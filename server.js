/* eslint no-console: 0 */
const path = require('path');
const express = require('express');
const compression = require('compression');
const app = express();

app.use(compression());
app.use(express.static(__dirname + '/public'));
app.get('*', function response(req, res) {
    res.sendFile(path.join(__dirname, 'public/index.html'));
});
app.get('*.js', function (req, res, next) {
    req.url = req.url + '.gz';
    res.set('Content-Encoding', 'gzip');
    next();
});

app.listen(3000, '127.0.0.1', function onStart(err) {
    if (err) {
        console.log(err);
    }
    console.info('==> ðŸŒŽ Listening on port %s. Open up http://0.0.0.0:%s/ in your browser.', 3000, 3000);
});
