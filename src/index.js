import "babel-polyfill";
import React from "../node_modules/react";
import ReactDOM from "../node_modules/react-dom";
import { Provider } from "react-redux";
import { store } from "./store";
import { router } from './router';

ReactDOM.render(
    <Provider store={store}>
        {router}
    </Provider>,
    document.getElementById('app')
);
