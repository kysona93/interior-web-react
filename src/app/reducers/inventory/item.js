import { combineReducers } from "redux";
import * as ITEM_TYPE from '../../actions/inventory/item';

function addItemReducer(state = {}, action) {
    switch (action.type) {
        case ITEM_TYPE.ADD_ITEM_SUCCESS:
            return action.response;
        case ITEM_TYPE.ADD_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllItemsReducer(state = {}, action) {
    switch (action.type) {
        case ITEM_TYPE.GET_ALL_ITEMS_SUCCESS:
            return action.response;
        case ITEM_TYPE.GET_ALL_ITEMS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getItemsReducer(state = {}, action) {
    switch (action.type) {
        case ITEM_TYPE.GET_ITEMS_SUCCESS:
            return action.response;
        case ITEM_TYPE.GET_ITEMS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateItemReducer(state = {}, action) {
    switch (action.type) {
        case ITEM_TYPE.UPDATE_ITEM_SUCCESS:
            return action.response;
        case ITEM_TYPE.UPDATE_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteItemReducer(state = {}, action) {
    switch (action.type) {
        case ITEM_TYPE.DELETE_ITEM_SUCCESS:
            return action.response;
        case ITEM_TYPE.DELETE_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const itemReducer = combineReducers({
    itemAdd: addItemReducer,
    itemsAll: getAllItemsReducer,
    items: getItemsReducer,
    itemUpdate: updateItemReducer,
    itemDelete: deleteItemReducer
});