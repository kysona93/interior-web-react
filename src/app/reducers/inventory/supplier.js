import { combineReducers } from "redux";
import * as SUPPLIER from './../../actions/inventory/supplier';

function getAllSupplierReducer(state = {}, action) {
    switch (action.type) {
        case SUPPLIER.GET_ALL_SUPPLIER_SUCCESS:
            return action.response;
        case SUPPLIER.GET_ALL_SUPPLIER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getSuppliersReducer(state = {}, action) {
    switch (action.type) {
        case SUPPLIER.GET_SUPPLIERS_SUCCESS:
            return action.response;
        case SUPPLIER.GET_SUPPLIERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addSupplierReducer(state = {}, action) {
    switch (action.type) {
        case SUPPLIER.ADD_SUPPLIER_SUCCESS:
            return action.response;
        case SUPPLIER.ADD_SUPPLIER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateSupplierReducer(state = {}, action) {
    switch (action.type) {
        case SUPPLIER.UPDATE_SUPPLIER_SUCCESS:
            return action.response;
        case SUPPLIER.UPDATE_SUPPLIER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteSupplierReducer(state = {}, action) {
    switch (action.type) {
        case SUPPLIER.DELETE_SUPPLIER_SUCCESS:
            return action.response;
        case SUPPLIER.DELETE_SUPPLIER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const supplierReducer = combineReducers({
    supplierAll: getAllSupplierReducer,
    getSuppliers: getSuppliersReducer,
    supplierAdd: addSupplierReducer,
    supplierUpdate: updateSupplierReducer,
    supplierDelete: deleteSupplierReducer
});