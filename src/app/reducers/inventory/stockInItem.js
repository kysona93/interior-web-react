import { combineReducers } from "redux";
import {
    ADD_STOCK_IN_ITEM_FAIL,
    ADD_STOCK_IN_ITEM_SUCCESS,
    GET_ALL_STOCK_IN_ITEMS_FAIL,
    GET_ALL_STOCK_IN_ITEMS_SUCCESS,
    UPDATE_STOCK_IN_ITEM_FAIL,
    UPDATE_STOCK_IN_ITEM_SUCCESS,
    DELETE_STOCK_IN_ITEM_FAIL,
    DELETE_STOCK_IN_ITEM_SUCCESS
} from '../../actions/inventory/stockInItem';

function addStockInItemReducer(state = {}, action) {
    switch (action.type) {
        case ADD_STOCK_IN_ITEM_SUCCESS:
            return action.response;
        case ADD_STOCK_IN_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllStockInItemsReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_STOCK_IN_ITEMS_SUCCESS:
            return action.response;
        case GET_ALL_STOCK_IN_ITEMS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateStockInItemReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_STOCK_IN_ITEM_SUCCESS:
            return action.response;
        case UPDATE_STOCK_IN_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteStockInItemReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_STOCK_IN_ITEM_SUCCESS:
            return action.response;
        case DELETE_STOCK_IN_ITEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const stockInItemReducer = combineReducers({
    addStockInItem: addStockInItemReducer,
    getAllStockInItems :getAllStockInItemsReducer,
    updateStockInItem: updateStockInItemReducer,
    deleteStockInItem: deleteStockInItemReducer
});