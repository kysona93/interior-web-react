import { combineReducers } from "redux";
import * as CATEGORY_ACTION from './../../actions/inventory/itemCategory';

function getItemCategoryByParentReducer(state = {}, action) {
    switch (action.type) {
        case CATEGORY_ACTION.GET_ITEM_CATEGORY_PARENT_SUCCESS:
            return action.response;
        case CATEGORY_ACTION.GET_ITEM_CATEGORY_PARENT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getItemSubCategoryByParentReducer(state = {}, action) {
    switch (action.type) {
        case CATEGORY_ACTION.GET_ITEM_SUB_CATEGORY_PARENT_SUCCESS:
            return action.response;
        case CATEGORY_ACTION.GET_ITEM_SUB_CATEGORY_PARENT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const ItemCategoryReducer = combineReducers({
    itemCategoryParent: getItemCategoryByParentReducer,
    itemSubCategoryParent: getItemSubCategoryByParentReducer,
});