import { combineReducers } from "redux";
import * as ACTION from './../../actions/inventory/connectionType';

function getConnectionTypesReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_CONNECTION_TYPES_SUCCESS:
            return action.response;
        case ACTION.GET_CONNECTION_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const connectionTypeReducer = combineReducers({
    connectionTypes: getConnectionTypesReducer,
});