import { combineReducers } from "redux";
import * as ACTION from '../../actions/inventory/purchaseKwh';

function addConnectionBuyReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.ADD_CONNECTION_BUY_SUCCESS:
            return action.response;
        case ACTION.ADD_CONNECTION_BUY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getConnectionBuysReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_CONNECTION_BUY_SUCCESS:
            return action.response;
        case ACTION.GET_CONNECTION_BUY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateConnectionBuyReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.UPDATE_CONNECTION_BUY_SUCCESS:
            return action.response;
        case ACTION.UPDATE_CONNECTION_BUY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteConnectionBuyReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.DELETE_CONNECTION_BUY_SUCCESS:
            return action.response;
        case ACTION.DELETE_CONNECTION_BUY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addPurchaseKwhReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.ADD_PURCHASE_KWH_SUCCESS:
            return action.response;
        case ACTION.ADD_PURCHASE_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPurchaseKwhsReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PURCHASE_KWH_SUCCESS:
            return action.response;
        case ACTION.GET_PURCHASE_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updatePurchaseKwhReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.UPDATE_PURCHASE_KWH_SUCCESS:
            return action.response;
        case ACTION.UPDATE_PURCHASE_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deletePurchaseKwhReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.DELETE_PURCHASE_KWH_SUCCESS:
            return action.response;
        case ACTION.DELETE_PURCHASE_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPurchaseKwhMonthlyReportsReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PURCHASE_KWH_MONTHLY_REPORT_SUCCESS:
            return action.response;
        case ACTION.GET_PURCHASE_KWH_MONTHLY_REPORT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const purchaseKwhReducer = combineReducers({
    connectionBuyAdd: addConnectionBuyReducer,
    connectionBuys: getConnectionBuysReducer,
    connectionBuyUpdate: updateConnectionBuyReducer,
    connectionBuyDelete: deleteConnectionBuyReducer,
    purchaseKwhAdd: addPurchaseKwhReducer,
    purchaseKwhs: getPurchaseKwhsReducer,
    purchaseKwhUpdate: updatePurchaseKwhReducer,
    purchaseKwhDelete: deletePurchaseKwhReducer,
    purchaseKwhMonthlyReports: getPurchaseKwhMonthlyReportsReducer
});