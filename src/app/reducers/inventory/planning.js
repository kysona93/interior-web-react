import { combineReducers } from "redux";
import * as ACTION from '../../actions/inventory/planning';

function addPlanningReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.ADD_PLANNING_SUCCESS:
            return action.response;
        case ACTION.ADD_PLANNING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPlanningsReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PLANNINGS_SUCCESS:
            return action.response;
        case ACTION.GET_PLANNINGS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPlanningNoReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PLANNING_NO_SUCCESS:
            return action.response;
        case ACTION.GET_PLANNING_NO_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updatePlanningReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.UPDATE_PLANNING_SUCCESS:
            return action.response;
        case ACTION.UPDATE_PLANNING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deletePlanningReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.DELETE_PLANNING_SUCCESS:
            return action.response;
        case ACTION.DELETE_PLANNING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPlanningsApprovedReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PLANNINGS_APPROVED_SUCCESS:
            return action.response.data;
        case ACTION.GET_PLANNINGS_APPROVED_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const planningReducer = combineReducers({
    planningAdd: addPlanningReducer,
    plannings: getPlanningsReducer,
    planningNo: getPlanningNoReducer,
    planningUpdate: updatePlanningReducer,
    planningDelete: deletePlanningReducer,
    planningsApproved: getPlanningsApprovedReducer
});