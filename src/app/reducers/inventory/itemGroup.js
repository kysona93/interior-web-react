import { combineReducers } from "redux";
import {
    ADD_ITEM_GROUP_FAIL,
    ADD_ITEM_GROUP_SUCCESS,
    GET_ALL_ITEM_GROUPS_FAIL,
    GET_ALL_ITEM_GROUPS_SUCCESS,
    UPDATE_ITEM_GROUP_SUCCESS,
    UPDATE_ITEM_GROUP_FAIL,
    DELETE_ITEM_GROUP_FAIL,
    DELETE_ITEM_GROUP_SUCCESS,
    GET_ALL_ITEM_GROUPS_LIST_SUCCESS,
    GET_ALL_ITEM_GROUPS_LIST_FAIL
} from './../../actions/inventory/itemGroup';

function addItemGroupReducer(state = {}, action) {
    switch (action.type) {
        case ADD_ITEM_GROUP_SUCCESS:
            return action.response;
        case ADD_ITEM_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function listAllItemGroupReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_ITEM_GROUPS_SUCCESS:
            return action.response;
        case GET_ALL_ITEM_GROUPS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateItemGroupReducer(state = {}, action){
    switch (action.type) {
        case UPDATE_ITEM_GROUP_SUCCESS:
            return action.response;
        case UPDATE_ITEM_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteItemGroupReducer(state = {}, action){
    switch (action.type) {
        case DELETE_ITEM_GROUP_SUCCESS:
            return action.response;
        case DELETE_ITEM_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getItemGroupsListReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_ITEM_GROUPS_LIST_SUCCESS:
            return action.response;
        case GET_ALL_ITEM_GROUPS_LIST_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const itemGroupReducer = combineReducers({
    addItemGroup: addItemGroupReducer,
    listAllItemGroup: listAllItemGroupReducer,
    updateItemGroup: updateItemGroupReducer,
    deleteItemGroup: deleteItemGroupReducer,
    ItemGroupsList: getItemGroupsListReducer
});