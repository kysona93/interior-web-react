import { combineReducers } from "redux";
import {
    ADD_UNIT_TYPE_SUCCESS,
    ADD_UNIT_TYPE_FAIL,
    GET_ALL_UNIT_TYPES_FAIL,
    GET_ALL_UNIT_TYPES_SUCCESS,
    UPDATE_UNIT_TYPE_FAIL,
    UPDATE_UNIT_TYPE_SUCCESS,
    DELETE_UNIT_TYPE_FAIL,
    DELETE_UNIT_TYPE_SUCCESS
} from './../../actions/inventory/unitType';

function addUnitTypeReducer(state = {}, action) {
    switch (action.type) {
        case ADD_UNIT_TYPE_SUCCESS:
            return action.response;
        case ADD_UNIT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function listAllUnitTypeReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_UNIT_TYPES_SUCCESS:
            return action.response;
        case GET_ALL_UNIT_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateUnitTypeReducer(state = {}, action){
    switch (action.type) {
        case UPDATE_UNIT_TYPE_SUCCESS:
            return action.response;
        case UPDATE_UNIT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteUnitTypeReducer(state = {}, action){
    switch (action.type) {
        case DELETE_UNIT_TYPE_SUCCESS:
            return action.response;
        case DELETE_UNIT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const unitTypeReducer = combineReducers({
    addUnitType: addUnitTypeReducer,
    listAllUnitType: listAllUnitTypeReducer,
    updateUnitType: updateUnitTypeReducer,
    deleteUnitType: deleteUnitTypeReducer
});