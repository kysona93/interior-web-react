import { combineReducers } from "redux";
import * as ACTION from '../../actions/inventory/request';

function addRequestReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.ADD_REQUEST_SUCCESS:
            return action.response;
        case ACTION.ADD_REQUEST_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getRequestsReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_REQUESTS_SUCCESS:
            return action.response;
        case ACTION.GET_REQUESTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getRequestNoReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_REQUEST_NO_SUCCESS:
            return action.response;
        case ACTION.GET_REQUEST_NO_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateRequestReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.UPDATE_REQUEST_SUCCESS:
            return action.response;
        case ACTION.UPDATE_REQUEST_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteRequestReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.DELETE_REQUEST_SUCCESS:
            return action.response;
        case ACTION.DELETE_REQUEST_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getRequestsApprovedReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_REQUESTS_APPROVED_SUCCESS:
            return action.response.data;
        case ACTION.GET_REQUESTS_APPROVED_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const requestReducer = combineReducers({
    requestAdd: addRequestReducer,
    requests: getRequestsReducer,
    requestNo: getRequestNoReducer,
    requestUpdate: updateRequestReducer,
    requestDelete: deleteRequestReducer,
    requestsApproved: getRequestsApprovedReducer
});