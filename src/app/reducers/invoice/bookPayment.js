import { combineReducers } from "redux";
import {
    ADD_CUSTOMER_BOOK_PAYMENT_FAIL,
    ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS,
    ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL,
    ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS
} from '../../actions/invoice/bookPayment';

function addCustomerBookPaymentReducer(state = {}, action) {
    switch (action.type) {
        case ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS:
            return action.response;
        case ADD_CUSTOMER_BOOK_PAYMENT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addCustomerBookDepreciationReducer(state = {}, action){
    switch (action.type) {
        case ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS:
            return action.response;
        case ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const customerBookPaymentReducer = combineReducers({
    addCustomerBookPayment: addCustomerBookPaymentReducer,
    addCustomerBookDepreciation: addCustomerBookDepreciationReducer
});