import { combineReducers } from "redux";
import * as GROUP_INVOICE_TYPE from './../../actions/invoice/groupInvoices';

function addGroupInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case GROUP_INVOICE_TYPE.ADD_GROUP_INVOICE_SUCCESS:
            return action.response;
        case GROUP_INVOICE_TYPE.ADD_GROUP_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllGroupInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GROUP_INVOICE_TYPE.GET_ALL_GROUP_INVOICE_SUCCESS:
            return action.response;
        case GROUP_INVOICE_TYPE.GET_ALL_GROUP_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateGroupInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case GROUP_INVOICE_TYPE.UPDATE_GROUP_INVOICE_SUCCESS:
            return action.response;
        case GROUP_INVOICE_TYPE.UPDATE_GROUP_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteGroupInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case GROUP_INVOICE_TYPE.DELETE_GROUP_INVOICE_SUCCESS:
            return action.response;
        case GROUP_INVOICE_TYPE.DELETE_GROUP_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const groupInvoiceReducer = combineReducers({
    groupInvoiceAll :getAllGroupInvoicesReducer,
    groupInvoiceAdd: addGroupInvoiceReducer,
    groupInvoiceUpdate: updateGroupInvoiceReducer,
    groupInvoiceDelete: deleteGroupInvoiceReducer
});