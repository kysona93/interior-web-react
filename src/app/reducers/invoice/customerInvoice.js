import { combineReducers } from "redux";
import {
    ADD_CUSTOMER_INVOICE_FAIL,
    ADD_CUSTOMER_INVOICE_SUCCESS,
    GET_ALL_CUSTOMER_INVOICES_FAIL,
    GET_ALL_CUSTOMER_INVOICES_SUCCESS,
    VOID_CUSTOMER_INVOICE_FAIL,
    VOID_CUSTOMER_INVOICE_SUCCESS,
    DELETE_CUSTOMER_INVOICE_FAIL,
    DELETE_CUSTOMER_INVOICE_SUCCESS,
    GET_ALL_CUSTOMER_INVOICE_NO_FAIL,
    GET_ALL_CUSTOMER_INVOICES_NO_SUCCESS,
    GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_FAIL,
    GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_SUCCESS,
    GET_INVOICE_INFORMATION_BY_ID_FAIL,
    GET_INVOICE_INFORMATION_BY_ID_SUCCESS,
    GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_FAIL,
    GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_SUCCESS,
    GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_FAIL,
    GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_SUCCESS,
    GET_ALL_LICENSE_INVOICES_NO_SUCCESS,
    GET_ALL_LICENSE_INVOICE_NO_FAIL,
    GET_INVOICE_DETAILS_RENT_POLE_SUCCESS,
    GET_INVOICE_DETAILS_RENT_POLE_FAIL
    } from './../../actions/invoice/customerInvoice';

function addCustomerInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case ADD_CUSTOMER_INVOICE_SUCCESS:
            return action.response;
        case ADD_CUSTOMER_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomerInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMER_INVOICES_SUCCESS:
            return action.response;
        case GET_ALL_CUSTOMER_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteCustomerInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_CUSTOMER_INVOICE_SUCCESS:
            return action.response;
        case DELETE_CUSTOMER_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function voidCustomerInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case VOID_CUSTOMER_INVOICE_SUCCESS:
            return action.response;
        case VOID_CUSTOMER_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomerInvoiceNoReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMER_INVOICES_NO_SUCCESS:
            return action.response;
        case GET_ALL_CUSTOMER_INVOICE_NO_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getAllLicenseInvoiceNoReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_LICENSE_INVOICES_NO_SUCCESS:
            return action.response;
        case GET_ALL_LICENSE_INVOICE_NO_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllInvoiceDetailsByInvoiceIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_SUCCESS:
            return action.response;
        case GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getInvoiceInformationByInvoiceIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_INVOICE_INFORMATION_BY_ID_SUCCESS:
            return action.response;
        case GET_INVOICE_INFORMATION_BY_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getHistoryInvoice12monthByCustomerIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_SUCCESS:
            return action.response;
        case GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllInvoiceDetailItemsByInvoiceIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_SUCCESS:
            return action.response;
        case GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getInvoiceDetailRentPoleReducer(state = {}, action) {
    switch (action.type) {
        case GET_INVOICE_DETAILS_RENT_POLE_SUCCESS:
            return action.response;
        case GET_INVOICE_DETAILS_RENT_POLE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const customerInvoicesReducer = combineReducers({
    addCustomerInvoice: addCustomerInvoiceReducer,
    getAllCustomerInvoices :getAllCustomerInvoicesReducer,
    deleteCustomerInvoice: deleteCustomerInvoiceReducer,
    voidCustomerInvoice: voidCustomerInvoiceReducer,
    getAllCustomerInvoiceNo: getAllCustomerInvoiceNoReducer,
    getAllInvoiceDetailsByInvoiceId: getAllInvoiceDetailsByInvoiceIdReducer,
    getInvoiceInformationByInvoiceId: getInvoiceInformationByInvoiceIdReducer,
    getHistoryInvoice12monthByCustomerId: getHistoryInvoice12monthByCustomerIdReducer,
    getAllInvoiceDetailItemsByInvoiceId: getAllInvoiceDetailItemsByInvoiceIdReducer,
    getAllLicenseInvoiceNo:getAllLicenseInvoiceNoReducer,
    getAllInvoiceRentPole: getInvoiceDetailRentPoleReducer
});