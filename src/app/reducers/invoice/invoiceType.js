import { combineReducers } from "redux";
import * as INVOICE_TYPE from './../../actions/invoice/invoiceType';

function getAllInvoiceTypeReducer(state = {}, action) {
    switch (action.type) {
        case INVOICE_TYPE.GET_ALL_INVOICE_TYPE_SUCCESS:
            return action.response;
        case INVOICE_TYPE.GET_ALL_INVOICE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addInvoiceTypeReducer(state = {}, action) {
    switch (action.type) {
        case INVOICE_TYPE.ADD_INVOICE_TYPE_SUCCESS:
            return action.response;
        case INVOICE_TYPE.ADD_INVOICE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateInvoiceTypeReducer(state = {}, action) {
    switch (action.type) {
        case INVOICE_TYPE.UPDATE_INVOICE_TYPE_SUCCESS:
            return action.response;
        case INVOICE_TYPE.UPDATE_INVOICE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteInvoiceTypeReducer(state = {}, action) {
    switch (action.type) {
        case INVOICE_TYPE.DELETE_INVOICE_TYPE_SUCCESS:
            return action.response;
        case INVOICE_TYPE.DELETE_INVOICE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const invoiceTypeReducer = combineReducers({
    invoiceTypeAll: getAllInvoiceTypeReducer,
    invoiceTypeAdd: addInvoiceTypeReducer,
    invoiceTypeUpdate: updateInvoiceTypeReducer,
    invoiceTypeDelete: deleteInvoiceTypeReducer
});