import { combineReducers } from "redux";
import {
    ADD_INVOICE_FAIL,
    ADD_INVOICE_SUCCESS,
    GET_ALL_INVOICES_FAIL,
    GET_ALL_INVOICES_SUCCESS,
    UPDATE_INVOICE_FAIL,
    UPDATE_INVOICE_SUCCESS,
    DELETE_INVOICE_FAIL,
    DELETE_INVOICE_SUCCESS,
    VOID_INVOICE_FAIL,
    VOID_INVOICE_SUCCESS
} from './../../actions/invoice/invoice';

function addInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case ADD_INVOICE_SUCCESS:
            return action.response;
        case ADD_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_INVOICES_SUCCESS:
            return action.response;
        case GET_ALL_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_INVOICE_SUCCESS:
            return action.response;
        case UPDATE_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_INVOICE_SUCCESS:
            return action.response;
        case DELETE_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function voidInvoiceReducer(state = {}, action) {
    switch (action.type) {
        case VOID_INVOICE_SUCCESS:
            return action.response;
        case VOID_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const invoicesReducer = combineReducers({
    addInvoice: addInvoiceReducer,
    getAllInvoices :getAllInvoicesReducer,
    updateInvoice: updateInvoiceReducer,
    deleteInvoice: deleteInvoiceReducer,
    voidInvoice: voidInvoiceReducer
});