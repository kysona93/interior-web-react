import { combineReducers } from "redux";
import {
    ADD_CASH_BOX_FAIL,
    ADD_CASH_BOX_SUCCESS,
    GET_ALL_CASH_BOXES_FAIL,
    GET_ALL_CASH_BOXES_SUCCESS,
    GET_ALL_USERS_CASH_BOXES_FAIL,
    GET_ALL_USERS_CASH_BOXES_SUCCESS,
    GET_CASH_BOXES_BY_USER_ID_SUCCESS,
    GET_CASH_BOXES_BY_USER_ID_FAIL
} from '../../actions/accounting/cashBox';

function addCashBoxReducer(state = {}, action) {
    switch (action.type) {
        case ADD_CASH_BOX_SUCCESS:
            return action.response;
        case ADD_CASH_BOX_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCashBoxesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CASH_BOXES_SUCCESS:
            return action.response;
        case GET_ALL_CASH_BOXES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllUsersCashBoxesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_USERS_CASH_BOXES_SUCCESS:
            return action.response;
        case GET_ALL_USERS_CASH_BOXES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCashBoxesByUserIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_CASH_BOXES_BY_USER_ID_SUCCESS:
            return action.response;
        case GET_CASH_BOXES_BY_USER_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const cashBoxesReducer = combineReducers({
    addCashBox: addCashBoxReducer,
    getAllCashBoxes: getAllCashBoxesReducer,
    getAllUsersCashBoxes: getAllUsersCashBoxesReducer,
    getAllCashBoxesByUserId: getAllCashBoxesByUserIdReducer
});