import { combineReducers } from "redux";
import * as PAYMENT_TITLE from './../../actions/expense/paymentLocation';

function getAllPaymentLocationReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TITLE.GET_ALL_PAYMENT_LOCATION_SUCCESS:
            return action.response;
        case PAYMENT_TITLE.GET_ALL_PAYMENT_LOCATION_FAIL:
            return action.response;
        default:
            return state;
    }
}

function addPaymentLocationReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TITLE.ADD_PAYMENT_LOCATION_SUCCESS:
            return action.response;
        case PAYMENT_TITLE.ADD_PAYMENT_LOCATION_FAIL:
            return action.response;
        default:
            return state;
    }
}

function updatePaymentLocationReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TITLE.UPDATE_PAYMENT_LOCATION_SUCCESS:
            return action.response;
        case PAYMENT_TITLE.UPDATE_PAYMENT_LOCATION_FAIL:
            return action.response;
        default:
            return state;
    }
}

function deletePaymentLocationReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TITLE.DELETE_PAYMENT_LOCATION_SUCCESS:
            return action.response;
        case PAYMENT_TITLE.DELETE_PAYMENT_LOCATION_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const paymentTitleReducer = combineReducers({
    paymentLocationAll: getAllPaymentLocationReducer,
    paymentLocationAdd: addPaymentLocationReducer,
    paymentLocationUpdate: updatePaymentLocationReducer,
    paymentLocationDelete: deletePaymentLocationReducer
});