import { combineReducers } from "redux";
import * as PAYMENT_TYPE from './../../actions/expense/paymentType';

function getAllPaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TYPE.GET_ALL_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case PAYMENT_TYPE.GET_ALL_PAYMENT_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function addPaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TYPE.ADD_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case PAYMENT_TYPE.ADD_PAYMENT_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function updatePaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TYPE.UPDATE_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case PAYMENT_TYPE.UPDATE_PAYMENT_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function deletePaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case PAYMENT_TYPE.DELETE_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case PAYMENT_TYPE.DELETE_PAYMENT_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const paymentTypeReducer = combineReducers({
    paymentTypeAll: getAllPaymentTypeReducer,
    paymentTypeAdd: addPaymentTypeReducer,
    paymentTypeUpdate: updatePaymentTypeReducer,
    paymentTypeDelete: deletePaymentTypeReducer
});