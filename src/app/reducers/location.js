import { combineReducers } from "redux";
import * as LOCATION_TYPE from '../actions/location';

const getAllLocationsReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.GET_ALL_LOCATIONS_SUCCESS:
            return action.response;
        case LOCATION_TYPE.GET_ALL_LOCATIONS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const addLocationReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.ADD_LOCATION_SUCCESS:
            return action.response;
        case LOCATION_TYPE.ADD_LOCATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateLocationReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.UPDATE_LOCATION_SUCCESS:
            return action.response;
        case LOCATION_TYPE.UPDATE_LOCATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteLocationReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.DELETE_LOCATION_SUCCESS:
            return action.response;
        case LOCATION_TYPE.DELETE_LOCATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
};


const getProvincesReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.GET_PROVINCES_SUCCESS:
            return action.response;
        case LOCATION_TYPE.GET_PROVINCES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getDistrictsReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.GET_DISTRICTS_SUCCESS:
            return action.response;
        case LOCATION_TYPE.GET_DISTRICTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getCommunesReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.GET_COMMUNES_SUCCESS:
            return action.response;
        case LOCATION_TYPE.GET_COMMUNES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getVillagesReducer = (state = {}, action) => {
    switch (action.type) {
        case LOCATION_TYPE.GET_VILLAGES_SUCCESS:
            return action.response;
        case LOCATION_TYPE.GET_VILLAGES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};



export const locationReducer = combineReducers({
    locationAll: getAllLocationsReducer,
    locationAdd: addLocationReducer,
    locationUpdate: updateLocationReducer,
    locationDelete: deleteLocationReducer,
    provinces: getProvincesReducer,
    districts: getDistrictsReducer,
    communes: getCommunesReducer,
    villages: getVillagesReducer
});