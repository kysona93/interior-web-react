import { combineReducers } from "redux";
import * as REPORT_REF from './../../actions/report/reportRef';

/* report ref 0 kw */
function getAllReportRefPostpaid0KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_POSTPAID_0_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_POSTPAID_0_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getAllReportRefPrepaid0KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_0_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_0_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* report ref 1 to 10 kw */
function getAllReportRefPostpaid1to10KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_POSTPAID_1_TO_10_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_POSTPAID_1_TO_10_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getAllReportRefPrepaid1to10KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_1_TO_10_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_1_TO_10_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* report ref 1 to 10 kw */
function getAllReportRefPostpaid11to50KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_POSTPAID_11_TO_50_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_POSTPAID_11_TO_50_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* report ref greater than 50 kw not sponsor */
function getAllReportRefPrepaidGreaterThan50KwNotSponsorReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
/* report ref greater than 50 kw sponsor */
function getAllReportRefPrepaidGreaterThan50KwSponsorReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
/* report ref greater than 50 kw residential */
function getAllReportRefPrepaidGreaterThan50KwResidentialReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getAllReportRefPrepaid11to50KwReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_REPORT_REF_PREPAID_11_TO_50_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_REPORT_REF_PREPAID_11_TO_50_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
const getHistory12MonthsReducer = (state ={}, action) => {
    switch(action.type){
        case REPORT_REF.GET_HISTORY_12_MONTHS_SUCCESS:
            return action.response;
        case REPORT_REF.GET_HISTORY_12_MONTHS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getHistory12MonthsBusinessReducer = (state ={}, action) => {
    switch(action.type){
        case REPORT_REF.GET_HISTORY_12_MONTHS_BUSINESS_SUCCESS:
            return action.response;
        case REPORT_REF.GET_HISTORY_12_MONTHS_BUSINESS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAdditionSubsidiesReducer = (state ={}, action) => {
    switch(action.type){
        case REPORT_REF.GET_ADDITION_SUBSIDIES_SUCCESS:
            return action.response;
        case REPORT_REF.GET_ADDITION_SUBSIDIES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAdditionSubsidiesBusinessReducer = (state ={}, action) => {
    switch(action.type){
        case REPORT_REF.GET_HISTORY_12_MONTHS_BUSINESS_SUCCESS:
            return action.response;
        case REPORT_REF.GET_HISTORY_12_MONTHS_BUSINESS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const reportRefReducer = combineReducers({
    getAllReportRefPostpaid0Kw: getAllReportRefPostpaid0KwReducer,
    getAllReportRefPrepaid0Kw: getAllReportRefPrepaid0KwReducer,

    getAllReportRefPostpaid1to10Kw: getAllReportRefPostpaid1to10KwReducer,
    getAllReportRefPrepaid1to10Kw: getAllReportRefPrepaid1to10KwReducer,

    getAllReportRefPostpaid11to50Kw: getAllReportRefPostpaid11to50KwReducer,
    getAllReportRefPrepaid11to50Kw: getAllReportRefPrepaid11to50KwReducer,

    getAllReportRefPrepaidGreaterThan50KwNotSponsor: getAllReportRefPrepaidGreaterThan50KwNotSponsorReducer,
    getAllReportRefPrepaidGreaterThan50KwSponsor: getAllReportRefPrepaidGreaterThan50KwSponsorReducer,
    getAllReportRefPrepaidGreaterThan50KwResidential: getAllReportRefPrepaidGreaterThan50KwResidentialReducer,
    history12Months: getHistory12MonthsReducer,
    history12MonthsBusiness: getHistory12MonthsBusinessReducer,
    additionSubsidies: getAdditionSubsidiesReducer,
    additionSubsidiesBusiness: getAdditionSubsidiesBusinessReducer
});