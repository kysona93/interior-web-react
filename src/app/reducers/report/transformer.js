import { combineReducers } from 'redux';
import * as REPORT_TYPE from './../../actions/report/transformer';

export const getTransformerReportsReducer = (state = {}, action) => {
    switch (action.type){
        case REPORT_TYPE.GET_TRANSFORMER_REPORTS_SUCCESS:
            return action.response;
        case REPORT_TYPE.GET_TRANSFORMER_REPORTS_FAIL:
            return action.error.response;
        default: return state;
    }
};

export const getPoleReportsReducer = (state = {}, action) => {
    switch (action.type){
        case REPORT_TYPE.GET_POLE_REPORTS_SUCCESS:
            return action.response;
        case REPORT_TYPE.GET_POLE_REPORTS_FAIL:
            return action.error.response;
        default: return state;
    }
};

export const transformerReportReducer = combineReducers({
    transformerReports: getTransformerReportsReducer,
    poleReports: getPoleReportsReducer
});