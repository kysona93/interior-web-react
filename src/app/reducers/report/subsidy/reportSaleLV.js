import { combineReducers } from "redux";
import * as REPORT_REF from './../../../actions/report/subsidy/reportSaleLV';

/**
 * report amount  subsidy sponser page 03 
 */
function getSubsidyPrepaidAmountSponserReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_SUCCESS:
            return action.response;
        case REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_FAIL:
            return action.error.response;
        default:
            return state;
    }
} 
function getSubsidyPrepaidAmountReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SUCCESS:
            return action.response;
        case REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* report subsidy page 2 */
function getReportSubsidySaleLV0to2000kwReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_2000_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_2000_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getReportSubsidySaleLV0to10kwReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_10_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_10_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getReportSubsidySaleLV11to50kwReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_SUBSIDY_SALE_LV_11_TO_50_KW_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_LV_11_TO_50_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getReportSubsidySaleLV51kwUpReducer(state={}, action){
    switch(action.type){
        case REPORT_REF.GET_SUBSIDY_SALE_LV_51_KW_UP_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_LV_51_KW_UP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const reportSubsidySaleLVReducer = combineReducers({
    getSubsidyPrepaidAmountSponser: getSubsidyPrepaidAmountSponserReducer,
    getSubsidyPrepaidAmount: getSubsidyPrepaidAmountReducer,

    getReportSubsidySaleLV0to2000kw: getReportSubsidySaleLV0to2000kwReducer,
    getReportSubsidySaleLV0to10kw: getReportSubsidySaleLV0to10kwReducer,
    getReportSubsidySaleLV11to50kw: getReportSubsidySaleLV11to50kwReducer,
    getReportSubsidySaleLV51kwUp: getReportSubsidySaleLV51kwUpReducer

});