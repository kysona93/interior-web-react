import { combineReducers } from "redux";
import * as REPORT_REF from './../../../actions/report/subsidy/reportSaleMV';


function getAllReportSubsidySaleMVLicenseReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_SUBSIDY_SALE_MV_LICENSE_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_MV_LICENSE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllReportSubsidySaleMVReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_SUBSIDY_SALE_MV_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_MV_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllReportSubsidySaleLVKW2001UPReducer(state = {}, action) {
    switch (action.type) {
        case REPORT_REF.GET_SUBSIDY_SALE_LV_KW_2001_UP_SUCCESS:
            return action.response;
        case REPORT_REF.GET_SUBSIDY_SALE_LV_KW_2001_UP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const reportSubsidySaleMVReducer = combineReducers({
    getAllReportSubsidySaleMVLicense: getAllReportSubsidySaleMVLicenseReducer,
    getAllReportSubsidySaleMV: getAllReportSubsidySaleMVReducer,
    getAllReportSubsidySaleLVKW2001UP: getAllReportSubsidySaleLVKW2001UPReducer
});