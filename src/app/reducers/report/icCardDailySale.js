import { combineReducers } from "redux";
import * as ACTION from './../../actions/report/icCardDailySale';

function getICCardDailySalesByCusIdReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_IC_CARD_DAILY_SALES_BY_CUS_ID_SUCCESS:
            return action.response;
        case ACTION.GET_IC_CARD_DAILY_SALES_BY_CUS_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getICCardDailySalesFilterReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_IC_CARD_DAILY_SALES_FILTER_SUCCESS:
            return action.response;
        case ACTION.GET_IC_CARD_DAILY_SALES_FILTER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPrepaidPowerReportsReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PREPAID_POWER_REPORTS_SUCCESS:
            return action.response;
        case ACTION.GET_PREPAID_POWER_REPORTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const icCardDailySaleReducer = combineReducers({
    icCardDailySalesByCusId: getICCardDailySalesByCusIdReducer,
    icCardDailySalesFilter: getICCardDailySalesFilterReducer,
    prepaidPowerReports: getPrepaidPowerReportsReducer,
});