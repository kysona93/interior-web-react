import { combineReducers } from "redux";
import {
    ADD_GROUP_REPORT_SUCCESS,
    ADD_GROUP_REPORT_FAIL,
    GET_ALL_GROUP_REPORTS_SUCCESS,
    GET_ALL_GROUP_REPORTS_FAIL,
    UPDATE_GROUP_REPORT_SUCCESS,
    UPDATE_GROUP_REPORT_FAIL,
    DELETE_GROUP_REPORT_SUCCESS,
    DELETE_GROUP_REPORT_FAIL
} from './../../actions/report/groupReport';

function addGroupReportReducer(state = {}, action) {
    switch (action.type) {
        case ADD_GROUP_REPORT_SUCCESS:
            return action.response;
        case ADD_GROUP_REPORT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function listAllGroupReportReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_GROUP_REPORTS_SUCCESS:
            return action.response;
        case GET_ALL_GROUP_REPORTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateGroupReportReducer(state = {}, action){
    switch (action.type) {
        case UPDATE_GROUP_REPORT_SUCCESS:
            return action.response;
        case UPDATE_GROUP_REPORT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteGroupReportReducer(state = {}, action){
    switch (action.type) {
        case DELETE_GROUP_REPORT_SUCCESS:
            return action.response;
        case DELETE_GROUP_REPORT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const groupReportReducer = combineReducers({
    addGroupReport: addGroupReportReducer,
    listAllGroupReport: listAllGroupReportReducer,
    updateGroupReport: updateGroupReportReducer,
    deleteGroupReport: deleteGroupReportReducer
});