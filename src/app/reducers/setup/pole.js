import { combineReducers } from "redux";
import * as POLE_TYPE from '../../actions/setup/pole';

function addPoleReducer(state = {}, action) {
    switch (action.type) {
        case POLE_TYPE.ADD_POLE_SUCCESS:
            return action.response;
        case POLE_TYPE.ADD_POLE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updatePoleReducer(state = {}, action) {
    switch (action.type) {
        case POLE_TYPE.UPDATE_POLE_SUCCESS:
            return action.response;
        case POLE_TYPE.UPDATE_POLE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deletePoleReducer(state = {}, action) {
    switch (action.type) {
        case POLE_TYPE.DELETE_POLE_SUCCESS:
            return action.response;
        case POLE_TYPE.DELETE_POLE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getPolesReducer(state = {}, action) {
    switch (action.type) {
        case POLE_TYPE.GET_POLES_SUCCESS:
            return action.response;
        case POLE_TYPE.GET_POLES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllPolesReducer(state = {}, action) {
    switch (action.type) {
        case POLE_TYPE.GET_ALL_POLES_SUCCESS:
            return action.response;
        case POLE_TYPE.GET_ALL_POLES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const poleReducer = combineReducers({
    poles: getPolesReducer,
    poleAll: getAllPolesReducer,
    poleAdd: addPoleReducer,
    poleUpdate: updatePoleReducer,
    poleDelete: deletePoleReducer
});