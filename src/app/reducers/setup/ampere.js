import { combineReducers } from "redux";
import * as  AMPERE_TYPE  from '../../actions/setup/ampere';

const addAmpereReducer = (state = {}, action) => {
    switch (action.type) {
        case AMPERE_TYPE.ADD_AMPERE_SUCCESS:
            return action.response;
        case AMPERE_TYPE.ADD_AMPERE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllAmperesReducer = (state = {}, action) => {
    switch (action.type) {
        case AMPERE_TYPE.GET_ALL_AMPERES_SUCCESS:
            return action.response;
        case AMPERE_TYPE.GET_ALL_AMPERES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateAmpereReducer = (state = {}, action) => {
    switch (action.type) {
        case AMPERE_TYPE.UPDATE_AMPERE_SUCCESS:
            return action.response;
        case AMPERE_TYPE.UPDATE_AMPERE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteAmpereReducer = (state = {}, action) => {
    switch (action.type) {
        case AMPERE_TYPE.DELETE_AMPERE_SUCCESS:
            return action.response;
        case AMPERE_TYPE.DELETE_AMPERE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const ampereReducer = combineReducers({
    ampereAll: getAllAmperesReducer,
    ampereAdd: addAmpereReducer,
    ampereUpdate: updateAmpereReducer,
    ampereDelete: deleteAmpereReducer
});