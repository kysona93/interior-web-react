import { combineReducers } from "redux";
import * as BANK_TYPE from './../../actions/setup/bank';

function addBankReducer(state = {}, action) {
    switch (action.type) {
        case BANK_TYPE.ADD_BANK_SUCCESS:
            return action.response;
        case BANK_TYPE.ADD_BANK_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllBankReducer(state = {}, action) {
    switch (action.type) {
        case BANK_TYPE.GET_ALL_BANKS_SUCCESS:
            return action.response;
        case BANK_TYPE.GET_ALL_BANKS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateBankReducer(state = {}, action) {
    switch (action.type) {
        case BANK_TYPE.UPDATE_BANK_SUCCESS:
            return action.response;
        case BANK_TYPE.UPDATE_BANK_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteBankReducer(state = {}, action) {
    switch (action.type) {
        case BANK_TYPE.DELETE_BANK_SUCCESS:
            return action.response;
        case BANK_TYPE.DELETE_BANK_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const bankReducer = combineReducers({
    bankAdd: addBankReducer,
    bankAll: getAllBankReducer,
    bankUpdate: updateBankReducer,
    bankDelete: deleteBankReducer
});