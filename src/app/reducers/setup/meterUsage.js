import { combineReducers } from "redux";
import {
    ADD_METER_USAGE_SUCCESS,
    ADD_METER_USAGE_FAIL,
    GET_ALL_METER_USAGES_SUCCESS,
    GET_ALL_METER_USAGES_FAIL,
    UPDATE_METER_USAGE_SUCCESS,
    UPDATE_METER_USAGE_FAIL,
    DELETE_METER_USAGE_SUCCESS,
    DELETE_METER_USAGE_FAIL
} from './../../actions/setup/meterUsage';


function addMeterUsageReducer(state = {}, action) {
    switch (action.type) {
        case ADD_METER_USAGE_SUCCESS:
            return action.response;
        case ADD_METER_USAGE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function listAllMeterUsagesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_METER_USAGES_SUCCESS:
            return action.response;
        case GET_ALL_METER_USAGES_FAIL:
            return action.response;
        default:
            return state;
    }
}

function updateMeterUsageReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_METER_USAGE_SUCCESS:
            return action.response;
        case UPDATE_METER_USAGE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function deleteMeterUsageReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_METER_USAGE_SUCCESS:
            return action.response;
        case DELETE_METER_USAGE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const meterUsageReducer = combineReducers({
    addMeterUsage: addMeterUsageReducer,
    listAllMeterUsages :listAllMeterUsagesReducer,
    updateMeterUsage: updateMeterUsageReducer,
    deleteMeterUsage: deleteMeterUsageReducer
});