import { combineReducers } from "redux";
import * as BREAKER_TYPE from '../../actions/setup/breaker';

function addBreakerReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE.ADD_BREAKER_SUCCESS:
            return action.response;
        case BREAKER_TYPE.ADD_BREAKER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateBreakerReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE.UPDATE_BREAKER_SUCCESS:
            return action.response;
        case BREAKER_TYPE.UPDATE_BREAKER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteBreakerReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE.DELETE_BREAKER_SUCCESS:
            return action.response;
        case BREAKER_TYPE.DELETE_BREAKER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getBreakersReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE.GET_BREAKERS_SUCCESS:
            return action.response;
        case BREAKER_TYPE.GET_BREAKERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllBreakersReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE.GET_ALL_BREAKERS_SUCCESS:
            return action.response;
        case BREAKER_TYPE.GET_ALL_BREAKERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const breakerReducer = combineReducers({
    breakers: getBreakersReducer,
    breakerAll: getAllBreakersReducer,
    breakerAdd: addBreakerReducer,
    breakerUpdate: updateBreakerReducer,
    breakerDelete: deleteBreakerReducer
});