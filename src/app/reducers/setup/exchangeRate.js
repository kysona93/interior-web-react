import { combineReducers } from "redux";
import {
    ADD_EXCHANGE_RATE_SUCCESS,
    ADD_EXCHANGE_RATE_FAIL,
    GET_ALL_EXCHANGE_RATE_SUCCESS,
    GET_ALL_EXCHANGE_RATE_FAIL,
    GET_EXCHANGE_RATE_SUCCESS,
    GET_EXCHANGE_RATE_FAIL,
    UPDATE_EXCHANGE_RATE_SUCCESS,
    UPDATE_EXCHANGE_RATE_FAIL,
    DELETE_EXCHANGE_RATE_SUCCESS,
    DELETE_EXCHANGE_RATE_FAIL
} from './../../actions/setup/exchangeRate';

/* ADD EXCHANGE RATE */
function addExchangeRateReducer(state = {}, action) {
    switch (action.type) {
        case ADD_EXCHANGE_RATE_SUCCESS:
            return action.response;
        case ADD_EXCHANGE_RATE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* LIST EXCHANGE RATE */
function listAllExchangeRateReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_EXCHANGE_RATE_SUCCESS:
            return action.response;
        case GET_ALL_EXCHANGE_RATE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* GET EXCHANGE RATE */
function getExchangeRateReducer(state = {}, action){
    switch (action.type) {
        case GET_EXCHANGE_RATE_SUCCESS:
            return action.response;
        case GET_EXCHANGE_RATE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* UPDATE EXCHANGE RATE */
function updateExchangeRateReducer(state = {}, action){
    switch (action.type) {
        case UPDATE_EXCHANGE_RATE_SUCCESS:
            return action.response;
        case UPDATE_EXCHANGE_RATE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* DELETE EXCHANGE RATE */
function deleteExchangeRateReducer(state = {}, action){
    switch (action.type) {
        case DELETE_EXCHANGE_RATE_SUCCESS:
            return action.response;
        case DELETE_EXCHANGE_RATE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const exchangeRateReducer = combineReducers({
    addExchangeRate: addExchangeRateReducer,
    listAllExchangeRate: listAllExchangeRateReducer,
    getExchangeRate: getExchangeRateReducer,
    updateExchangeRate: updateExchangeRateReducer,
    deleteExchangeRate: deleteExchangeRateReducer
});