import { combineReducers } from "redux";
import {
    ADD_SETTING_SUCCESS,
    ADD_SETTING_FAIL,
    GET_ALL_SETTING_SUCCESS,
    GET_ALL_SETTING_FAIL,
    UPDATE_SETTING_SUCCESS,
    UPDATE_SETTING_FAIL,
    DELETE_SETTING_SUCCESS,
    DELETE_SETTING_FAIL
} from './../../actions/setup/setting';

/* ADD SETTING */
function addSettingReducer(state = {}, action) {
    switch (action.type) {
        case ADD_SETTING_SUCCESS:
            return action.response;
        case ADD_SETTING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST SETTING */
function listSettingReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_SETTING_SUCCESS:
            return action.response;
        case GET_ALL_SETTING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* UPDATE SETTING */
function updateSettingReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_SETTING_SUCCESS:
            return action.response;
        case UPDATE_SETTING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* DELETE SETTING */
function deleteSettingReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_SETTING_SUCCESS:
            return action.response;
        case DELETE_SETTING_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const settingReducer = combineReducers({
    addSetting: addSettingReducer,
    listSetting: listSettingReducer,
    updateSetting: updateSettingReducer,
    deleteSetting: deleteSettingReducer
});