import { combineReducers } from "redux";
import * as TRANSFORMER_TYPE from '../../actions/setup/transformer';

/**
 * reducer add transformer
 * @param state
 * @param action
 * @returns {{}}
 */
function addTransformerReducer(state = {}, action) {
    switch (action.type) {
        case TRANSFORMER_TYPE.ADD_TRANSFORMER_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE.ADD_TRANSFORMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
/**
 * reducer update transformer
 * @param state
 * @param action
 * @returns {{}}
 */
function updateTransformerReducer(state = {}, action) {
    switch (action.type) {
        case TRANSFORMER_TYPE.UPDATE_TRANSFORMER_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE.UPDATE_TRANSFORMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/**
 * reducer delete transformer
 * @param state
 * @param action
 * @returns {{}}
 */
function deleteTransformerReducer(state = {}, action) {
    switch (action.type) {
        case TRANSFORMER_TYPE.DELETE_TRANSFORMER_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE.DELETE_TRANSFORMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/**
 * reducer get transformers
 * @param state
 * @param action
 * @returns {*}
 */
function getAllTransformersReducer(state = {}, action) {
    switch (action.type) {
        case TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllTransformersForKwhMonthlyReducer(state = {}, action) {
    switch (action.type) {
        case TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const transformerReducer = combineReducers({
    transformerAll: getAllTransformersReducer,
    transformerAdd: addTransformerReducer,
    transformerUpdate: updateTransformerReducer,
    transformerDelete: deleteTransformerReducer,
    getAllTransformersForKwhMonthly: getAllTransformersForKwhMonthlyReducer
});