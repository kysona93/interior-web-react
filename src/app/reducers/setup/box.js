import { combineReducers } from "redux";
import * as BOX_TYPE from '../../actions/setup/box';

export function addBoxReducer(state = {}, action) {
    switch (action.type) {
        case BOX_TYPE.ADD_BOX_SUCCESS:
            return action.response;
        case BOX_TYPE.ADD_BOX_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export function updateBoxReducer(state = {}, action) {
    switch (action.type) {
        case BOX_TYPE.UPDATE_BOX_SUCCESS:
            return action.response;
        case BOX_TYPE.UPDATE_BOX_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export function deleteBoxReducer(state = {}, action) {
    switch (action.type) {
        case BOX_TYPE.DELETE_BOX_SUCCESS:
            return action.response;
        case BOX_TYPE.DELETE_BOX_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export function getBoxesReducer(state = {}, action) {
    switch (action.type) {
        case BOX_TYPE.GET_BOXES_SUCCESS:
            return action.response;
        case BOX_TYPE.GET_BOXES_FAIL:
            return action.error.response;
        default:
            return state;
    }

}

export function getAllBoxesReducer(state = {}, action) {
    switch (action.type) {
        case BOX_TYPE.GET_ALL_BOXES_SUCCESS:
            return action.response;
        case BOX_TYPE.GET_ALL_BOXES_FAIL:
            return action.error.response;
        default:
            return state;
    }

}

export const boxReducer = combineReducers({
    boxes: getBoxesReducer,
    boxAll: getAllBoxesReducer,
    boxAdd: addBoxReducer,
    boxUpdate: updateBoxReducer,
    boxDelete: deleteBoxReducer
});