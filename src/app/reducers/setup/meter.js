import { combineReducers } from "redux";
import * as METER_TYPE from '../../actions/setup/meter';

function addMeterReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.ADD_METER_SUCCESS:
            return action.response;
        case METER_TYPE.ADD_METER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateMeterReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.UPDATE_METER_SUCCESS:
            return action.response;
        case METER_TYPE.UPDATE_METER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteMeterReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.DELETE_METER_SUCCESS:
            return action.response;
        case METER_TYPE.DELETE_METER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getMetersReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.GET_METERS_SUCCESS:
            return action.response;
        case METER_TYPE.GET_METERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllMetersReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.GET_ALL_METERS_SUCCESS:
            return action.response;
        case METER_TYPE.GET_ALL_METERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getMeterByMeterSerialReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.GET_METER_BY_METER_SERIAL_SUCCESS:
            return action.response;
        case METER_TYPE.GET_METER_BY_METER_SERIAL_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function reuseMeterPostPaidReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE.REUSE_METER_SUCCESS:
            return action.response;
        case METER_TYPE.REUSE_METER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const meterReducer = combineReducers({
    meters: getMetersReducer,
    meterAll: getAllMetersReducer,
    meterAdd: addMeterReducer,
    meterUpdate: updateMeterReducer,
    meterDelete: deleteMeterReducer,
    meterBySerial: getMeterByMeterSerialReducer,
    reuseMeter: reuseMeterPostPaidReducer
});