import { combineReducers } from "redux";
import {
    ADD_RATE_RANGE_FAIL,
    ADD_RATE_RANGE_SUCCESS,
    GET_ALL_RATE_RANGE_FAIL,
    GET_ALL_RATE_RANGE_SUCCESS,
    UPDATE_RATE_RANGE_FAIL,
    UPDATE_RATE_RANGE_SUCCESS,
    DELETE_RATE_RANGE_FAIL,
    DELETE_RATE_RANGE_SUCCESS
} from './../../actions/setup/rateRange';

function addRateRangeReducer(state = {}, action) {
    switch (action.type) {
        case ADD_RATE_RANGE_SUCCESS:
            return action.response;
        case ADD_RATE_RANGE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllRateRangesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_RATE_RANGE_SUCCESS:
            return action.response;
        case GET_ALL_RATE_RANGE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateRateRangeReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_RATE_RANGE_SUCCESS:
            return action.response;
        case UPDATE_RATE_RANGE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteRateRangeReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_RATE_RANGE_SUCCESS:
            return action.response;
        case DELETE_RATE_RANGE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const rateRangesReducer = combineReducers({
    addRateRange: addRateRangeReducer,
    getAllRateRanges :getAllRateRangesReducer,
    updateRateRange: updateRateRangeReducer,
    deleteRateRange: deleteRateRangeReducer
});