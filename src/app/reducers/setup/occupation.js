import { combineReducers } from "redux";
import * as OCCUPATION_TYPE from './../../actions/setup/occupation';

function addOccupationReducer(state = {}, action) {
    switch (action.type) {
        case OCCUPATION_TYPE.ADD_OCCUPATION_SUCCESS:
            return action.response;
        case OCCUPATION_TYPE.ADD_OCCUPATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllOccupationsReducer(state = {}, action){
    switch (action.type) {
        case OCCUPATION_TYPE.GET_ALL_OCCUPATIONS_SUCCESS:
            return action.response;
        case OCCUPATION_TYPE.Get_ALL_OCCUPATIONS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateOccupationReducer(state = {}, action){
    switch (action.type) {
        case OCCUPATION_TYPE.UPDATE_OCCUPATION_SUCCESS:
            return action.response;
        case OCCUPATION_TYPE.UPDATE_OCCUPATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteOccupationReducer(state = {}, action){
    switch (action.type) {
        case OCCUPATION_TYPE.DELETE_OCCUPATION_SUCCESS:
            return action.response;
        case OCCUPATION_TYPE.DELETE_OCCUPATION_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const occupationReducer = combineReducers({
    occupationAll: getAllOccupationsReducer,
    occupationAdd: addOccupationReducer,
    occupationUpdate :updateOccupationReducer,
    occupationDelete: deleteOccupationReducer
});