import { combineReducers } from "redux";
import * as CONSUMPTION_TYPE_TYPE from '../../../actions/setup/type/consumptionType';

const addConsumptionTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case CONSUMPTION_TYPE_TYPE.ADD_CONSUMPTION_TYPE_SUCCESS:
            return action.response;
        case CONSUMPTION_TYPE_TYPE.ADD_CONSUMPTION_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateConsumptionTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case CONSUMPTION_TYPE_TYPE.UPDATE_CONSUMPTION_TYPE_SUCCESS:
            return action.response;
        case CONSUMPTION_TYPE_TYPE.UPDATE_CONSUMPTION_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteConsumptionTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case CONSUMPTION_TYPE_TYPE.DELETE_CONSUMPTION_TYPE_SUCCESS:
            return action.response;
        case CONSUMPTION_TYPE_TYPE.DELETE_CONSUMPTION_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};


const getAllConsumptionTypesReducer = (state = {}, action) => {
    switch (action.type) {
        case CONSUMPTION_TYPE_TYPE.GET_ALL_CONSUMPTION_TYPES_SUCCESS:
            return action.response;
        case CONSUMPTION_TYPE_TYPE.GET_ALL_CONSUMPTION_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const consumptionTypeReducer = combineReducers({
    consumptionTypeAll: getAllConsumptionTypesReducer,
    consumptionTypeAdd: addConsumptionTypeReducer,
    consumptionTypeUpdate: updateConsumptionTypeReducer,
    consumptionTypeDelete: deleteConsumptionTypeReducer
});