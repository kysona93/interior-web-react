import { combineReducers } from "redux";
import * as BREAKER_TYPE_TYPE from './../../../actions/setup/type/breakerType';

function addBreakerTypeReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE_TYPE.ADD_BREAKER_TYPE_SUCCESS:
            return action.response;
        case BREAKER_TYPE_TYPE.ADD_BREAKER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateBreakerTypeReducer(state = {}, action){
    switch (action.type) {
        case BREAKER_TYPE_TYPE.UPDATE_BREAKER_TYPE_SUCCESS:
            return action.response;
        case BREAKER_TYPE_TYPE.UPDATE_BREAKER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteBreakerTypeReducer(state = {}, action){
    switch (action.type) {
        case BREAKER_TYPE_TYPE.DELETE_BREAKER_TYPE_SUCCESS:
            return action.response;
        case BREAKER_TYPE_TYPE.DELETE_BREAKER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllBreakerTypesReducer(state = {}, action) {
    switch (action.type) {
        case BREAKER_TYPE_TYPE.GET_ALL_BREAKER_TYPES_SUCCESS:
            return action.response;
        case BREAKER_TYPE_TYPE.GET_ALL_BREAKER_TYPES_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const breakerTypeReducer = combineReducers({
    breakerTypeAll: getAllBreakerTypesReducer,
    breakerTypeAdd: addBreakerTypeReducer,
    breakerTypeUpdate: updateBreakerTypeReducer,
    breakerTypeDelete: deleteBreakerTypeReducer
});