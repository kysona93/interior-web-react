import { combineReducers } from "redux";
import * as POLE_TYPE_TYPE from './../../../actions/setup/type/poleType';

const addPoleTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case POLE_TYPE_TYPE.ADD_POLE_TYPE_SUCCESS:
            return action.response;
        case POLE_TYPE_TYPE.ADD_POLE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updatePoleTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case POLE_TYPE_TYPE.UPDATE_POLE_TYPE_SUCCESS:
            return action.response;
        case POLE_TYPE_TYPE.UPDATE_POLE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deletePoleTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case POLE_TYPE_TYPE.DELETE_POLE_TYPE_SUCCESS:
            return action.response;
        case POLE_TYPE_TYPE.DELETE_POLE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllPoleTypesReducer = (state = {}, action) => {
    switch (action.type) {
        case POLE_TYPE_TYPE.GET_ALL_POLE_TYPES_SUCCESS:
            return action.response;
        case POLE_TYPE_TYPE.GET_ALL_POLE_TYPES_FAIL:
            return action.response;
        default:
            return state;
    }
};

export const poleTypeReducer = combineReducers({
    poleTypeAll: getAllPoleTypesReducer,
    poleTypeAdd: addPoleTypeReducer,
    poleTypeUpdate: updatePoleTypeReducer,
    poleTypeDelete: deletePoleTypeReducer
});