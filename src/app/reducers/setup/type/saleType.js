import { combineReducers } from "redux";
import * as SALE_TYPE from './../../../actions/setup/saleType';

/* ADD BOX TYPE */
function addSaleTypeReducer(state = {}, action) {
    switch (action.type) {
        case SALE_TYPE.ADD_SALE_TYPE_SUCCESS:
            return action.response;
        case SALE_TYPE.ADD_SALE_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* LIST BOX TYPE */
function getAllSaleTypeReducer(state = {}, action) {
    switch (action.type) {
        case SALE_TYPE.GET_ALL_SALE_TYPE_SUCCESS:
            return action.response;
        case SALE_TYPE.GET_ALL_SALE_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* UPDATE BOX TYPE */
function updateSaleTypeReducer(state = {}, action){
    switch (action.type) {
        case SALE_TYPE.UPDATE_SALE_TYPE_SUCCESS:
            return action.response;
        case SALE_TYPE.UPDATE_SALE_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

/* DELETE BOX TYPE */
function deleteSaleTypeReducer(state = {}, action){
    switch (action.type) {
        case SALE_TYPE.DELETE_SALE_TYPE_SUCCESS:
            return action.response;
        case SALE_TYPE.DELETE_SALE_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const SaleTypeReducer = combineReducers({
    addSaleType: addSaleTypeReducer,
    getAllSaleType: getAllSaleTypeReducer,
    updateSaleType: updateSaleTypeReducer,
    deleteSaleType: deleteSaleTypeReducer
});