import { combineReducers } from "redux";
import * as CUSTOMER_TYPE_TYPE from '../../../actions/setup/type/customerType';

const addCustomerTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE_TYPE.ADD_CUSTOMER_TYPE_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE_TYPE.ADD_CUSTOMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateCustomerTypeReducer = (state = {},action) => {
    switch (action.type) {
        case CUSTOMER_TYPE_TYPE.UPDATE_CUSTOMER_TYPE_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE_TYPE.UPDATE_CUSTOMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteCustomerTypeReducer = (state = {},action) => {
    switch (action.type) {
        case CUSTOMER_TYPE_TYPE.DELETE_CUSTOMER_TYPE_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE_TYPE.DELETE_CUSTOMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllCustomerTypesReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE_TYPE.GET_ALL_CUSTOMER_TYPES_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE_TYPE.GET_ALL_CUSTOMER_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }

};

export const customerTypeReducer = combineReducers({
    customerTypeAll: getAllCustomerTypesReducer,
    customerTypeAdd: addCustomerTypeReducer,
    customerTypeUpdate: updateCustomerTypeReducer,
    customerTypeDelete: deleteCustomerTypeReducer
});