import { combineReducers } from "redux";
import * as TRANSFORMER_TYPE_TYPE from './../../../actions/setup/type/transformerType';

const addTransformerTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case TRANSFORMER_TYPE_TYPE.ADD_TRANSFORMER_TYPE_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE_TYPE.ADD_TRANSFORMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateTransformerTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case TRANSFORMER_TYPE_TYPE.UPDATE_TRANSFORMER_TYPE_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE_TYPE.UPDATE_TRANSFORMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteTransformerTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case TRANSFORMER_TYPE_TYPE.DELETE_TRANSFORMER_TYPE_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE_TYPE.DELETE_TRANSFORMER_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllTransformerTypesReducer = (state = {}, action) => {
    switch (action.type) {
        case TRANSFORMER_TYPE_TYPE.GET_ALL_TRANSFORMER_TYPES_SUCCESS:
            return action.response;
        case TRANSFORMER_TYPE_TYPE.GET_ALL_TRANSFORMER_TYPES_FAIL:
            return action.response;
        default:
            return state;
    }
};

export const transformerTypeReducer = combineReducers({
    transformerTypeAll: getAllTransformerTypesReducer,
    transformerTypeAdd: addTransformerTypeReducer,
    transformerTypeUpdate: updateTransformerTypeReducer,
    transformerTypeDelete: deleteTransformerTypeReducer
});