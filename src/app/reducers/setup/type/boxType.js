import { combineReducers } from "redux";
import * as BOX_TYPE_TYPE from './../../../actions/setup/type/boxType';

const addBoxTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case BOX_TYPE_TYPE.ADD_BOX_TYPE_SUCCESS:
            return action.response;
        case BOX_TYPE_TYPE.ADD_BOX_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateBoxTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case BOX_TYPE_TYPE.UPDATE_BOX_TYPE_SUCCESS:
            return action.response;
        case BOX_TYPE_TYPE.UPDATE_BOX_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteBoxTypeReducer = (state = {}, action) => {
    switch (action.type) {
        case BOX_TYPE_TYPE.DELETE_BOX_TYPE_SUCCESS:
            return action.response;
        case BOX_TYPE_TYPE.DELETE_BOX_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllBoxTypesReducer = (state = {}, action) => {
    switch (action.type) {
        case BOX_TYPE_TYPE.GET_ALL_BOX_TYPES_SUCCESS:
            return action.response;
        case BOX_TYPE_TYPE.GET_ALL_BOX_TYPES_FAIL:
            return action.response;
        default:
            return state;
    }
};

export const boxTypeReducer = combineReducers({
    boxTypeAll: getAllBoxTypesReducer,
    boxTypeAdd: addBoxTypeReducer,
    boxTypeUpdate: updateBoxTypeReducer,
    boxTypeDelete: deleteBoxTypeReducer
});