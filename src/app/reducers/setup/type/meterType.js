import { combineReducers } from "redux";
import * as METER_TYPE_TYPE from './../../../actions/setup/type/meterType';

function addMeterTypeReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE_TYPE.ADD_METER_TYPE_SUCCESS:
            return action.response;
        case METER_TYPE_TYPE.ADD_METER_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function getAllMeterTypesReducer(state = {}, action) {
    switch (action.type) {
        case METER_TYPE_TYPE.GET_ALL_METER_TYPES_SUCCESS:
            return action.response;
        case METER_TYPE_TYPE.GET_ALL_METER_TYPES_FAIL:
            return action.response;
        default:
            return state;
    }
}

function updateMeterTypeReducer(state = {}, action){
    switch (action.type) {
        case METER_TYPE_TYPE.UPDATE_METER_TYPE_SUCCESS:
            return action.response;
        case METER_TYPE_TYPE.UPDATE_METER_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function deleteMeterTypeReducer(state = {}, action){
    switch (action.type) {
        case METER_TYPE_TYPE.DELETE_METER_TYPE_SUCCESS:
            return action.response;
        case METER_TYPE_TYPE.DELETE_METER_TYPE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const meterTypeReducer = combineReducers({
    meterTypeAll: getAllMeterTypesReducer,
    meterTypeAdd: addMeterTypeReducer,
    meterTypeUpdate: updateMeterTypeReducer,
    meterTypeDelete: deleteMeterTypeReducer
});