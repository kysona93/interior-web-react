import { combineReducers } from "redux";
import * as VOLTAGE_TYPE from './../../../actions/setup/type/voltageType';

function getAllVoltageTypesReducer(state = {}, action) {
    switch (action.type) {
        case VOLTAGE_TYPE.GET_ALL_VOLTAGE_TYPES_SUCCESS:
            return action.response;
        case VOLTAGE_TYPE.GET_ALL_VOLTAGE_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addVoltageTypeReducer(state = {}, action) {
    switch (action.type) {
        case VOLTAGE_TYPE.ADD_VOLTAGE_TYPE_SUCCESS:
            return action.response;
        case VOLTAGE_TYPE.ADD_VOLTAGE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateVoltageTypeReducer(state = {}, action) {
    switch (action.type) {
        case VOLTAGE_TYPE.UPDATE_VOLTAGE_TYPE_SUCCESS:
            return action.response;
        case VOLTAGE_TYPE.UPDATE_VOLTAGE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteVoltageTypeReducer(state = {}, action) {
    switch (action.type) {
        case VOLTAGE_TYPE.DELETE_VOLTAGE_TYPE_SUCCESS:
            return action.response;
        case VOLTAGE_TYPE.DELETE_VOLTAGE_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const voltageTypeReducer = combineReducers({
    voltageTypeAll: getAllVoltageTypesReducer,
    voltageTypeAdd: addVoltageTypeReducer,
    voltageTypeUpdate: updateVoltageTypeReducer,
    voltageTypeDelete: deleteVoltageTypeReducer
});