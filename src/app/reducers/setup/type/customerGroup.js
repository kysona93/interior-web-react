import { combineReducers } from "redux";
import * as CUSTOMER_GROUP from './../../../actions/setup/type/customerGroup';

function addCustomerGroupReducer(state = {}, action) {
    switch (action.type) {
        case CUSTOMER_GROUP.ADD_CUSTOMER_GROUP_SUCCESS:
            return action.response;
        case CUSTOMER_GROUP.ADD_CUSTOMER_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomerGroupReducer(state = {}, action) {
    switch (action.type) {
        case CUSTOMER_GROUP.GET_ALL_CUSTOMER_GROUPS_SUCCESS:
            return action.response;
        case CUSTOMER_GROUP.GET_ALL_CUSTOMER_GROUPS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateCustomerGroupReducer(state = {}, action){
    switch (action.type) {
        case CUSTOMER_GROUP.UPDATE_CUSTOMER_GROUP_SUCCESS:
            return action.response;
        case CUSTOMER_GROUP.UPDATE_CUSTOMER_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteCustomerGroupReducer(state = {}, action){
    switch (action.type) {
        case CUSTOMER_GROUP.DELETE_CUSTOMER_GROUP_SUCCESS:
            return action.response;
        case CUSTOMER_GROUP.DELETE_CUSTOMER_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const customerGroupReducer = combineReducers({
    customerGroupAll: getAllCustomerGroupReducer,
    customerGroupAdd: addCustomerGroupReducer,
    customerGroupUpdate: updateCustomerGroupReducer,
    customerGroupDelete: deleteCustomerGroupReducer
});