import { combineReducers } from "redux";
import * as  AREA_TYPE  from '../../actions/setup/area';

const addAreaReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.ADD_AREA_SUCCESS:
            return action.response;
        case AREA_TYPE.ADD_AREA_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllAreasReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.GET_ALL_AREAS_SUCCESS:
            return action.response;
        case AREA_TYPE.GET_ALL_AREAS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};
const getAreasReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.GET_AREAS_SUCCESS:
            return action.response;
        case AREA_TYPE.GET_AREAS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAreaReportsReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.GET_AREA_REPORTS_SUCCESS:
            return action.response;
        case AREA_TYPE.GET_AREA_REPORTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateAreaReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.UPDATE_AREA_SUCCESS:
            return action.response;
        case AREA_TYPE.UPDATE_AREA_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteAreaReducer = (state = {}, action) => {
    switch (action.type) {
        case AREA_TYPE.DELETE_AREA_SUCCESS:
            return action.response;
        case AREA_TYPE.DELETE_AREA_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const areaReducer = combineReducers({
    areaAll: getAllAreasReducer,
    areaGet: getAreasReducer,
    areaReports: getAreaReportsReducer,
    areaAdd: addAreaReducer,
    areaUpdate: updateAreaReducer,
    areaDelete: deleteAreaReducer
});