import { combineReducers } from "redux";
import {
    ADD_METER_PHASE_SUCCESS,
    ADD_METER_PHASE_FAIL,
    GET_ALL_METER_PHASE_SUCCESS,
    GET_ALL_METER_PHASE_FAIL,
    UPDATE_METER_PHASE_SUCCESS,
    UPDATE_METER_PHASE_FAIL,
    DELETE_METER_PHASE_SUCCESS,
    DELETE_METER_PHASE_FAIL
} from './../../actions/setup/meterPhase';


function addMeterPhaseReducer(state = {}, action) {
    switch (action.type) {
        case ADD_METER_PHASE_SUCCESS:
            return action.response;
        case ADD_METER_PHASE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function listAllMeterPhasesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_METER_PHASE_SUCCESS:
            return action.response;
        case GET_ALL_METER_PHASE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function updateMeterPhaseReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_METER_PHASE_SUCCESS:
            return action.response;
        case UPDATE_METER_PHASE_FAIL:
            return action.response;
        default:
            return state;
    }
}

function deleteMeterPhaseReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_METER_PHASE_SUCCESS:
            return action.response;
        case DELETE_METER_PHASE_FAIL:
            return action.response;
        default:
            return state;
    }
}

export const meterPhaseReducer = combineReducers({
    addMeterPhase: addMeterPhaseReducer,
    listAllMeterPhases :listAllMeterPhasesReducer,
    updateMeterPhase: updateMeterPhaseReducer,
    deleteMeterPhase: deleteMeterPhaseReducer
});