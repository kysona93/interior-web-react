import { combineReducers } from "redux";import {
    GET_ALL_JOBS_SUCCESS,
    GET_ALL_JOBS_FAIL
} from './../../actions/setup/job';

/* LIST ALL JOBS */
function listAllJobsReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_JOBS_SUCCESS:
            return action.response;
        case GET_ALL_JOBS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const jobReducer = combineReducers({
    listAllJobs: listAllJobsReducer
});