import { combineReducers } from "redux";
import * as RATE_TYPE from './../../actions/setup/rateKwh';

function addRatePerKhwReducer(state = {}, action) {
    switch (action.type) {
        case RATE_TYPE.ADD_RATE_PER_KWH_SUCCESS:
            return action.response;
        case RATE_TYPE.ADD_RATE_PER_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllRatePerKhwReducer(state = {}, action) {
    switch (action.type) {
        case RATE_TYPE.GET_ALL_RATE_PER_KWH_SUCCESS:
            return action.response;
        case RATE_TYPE.GET_ALL_RATE_PER_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateRatePerKhwReducer(state = {}, action) {
    switch (action.type) {
        case RATE_TYPE.UPDATE_RATE_PER_KWH_SUCCESS:
            return action.response;
        case RATE_TYPE.UPDATE_RATE_PER_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteRatePerKhwReducer(state = {}, action) {
    switch (action.type) {
        case RATE_TYPE.DELETE_RATE_PER_KWH_SUCCESS:
            return action.response;
        case RATE_TYPE.DELETE_RATE_PER_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const ratePerKwhReducer = combineReducers({
    ratePerKwhAll: getAllRatePerKhwReducer,
    ratePerKwhAdd: addRatePerKhwReducer,
    ratePerKwhUpdate: updateRatePerKhwReducer,
    ratePerKwhDelete: deleteRatePerKhwReducer
});