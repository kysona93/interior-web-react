import { combineReducers } from "redux";
import * as EMPLOYEE_TYPE from '../../actions/employee/employee';

function addEmployeeReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.ADD_EMPLOYEE_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.ADD_EMPLOYEE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateEmployeeReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.UPDATE_EMPLOYEE_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.UPDATE_EMPLOYEE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteEmployeeReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.DELETE_EMPLOYEE_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.DELETE_EMPLOYEE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getEmployeesReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.GET_EMPLOYEES_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.GET_EMPLOYEES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllEmployeesReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.GET_ALL_EMPLOYEES_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.GET_ALL_EMPLOYEES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}
function getAllInstallersReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.GET_ALL_INSTALLER_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.GET_ALL_INSTALLER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getInstallerReportsReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.GET_INSTALLER_REPORTS_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.GET_INSTALLER_REPORTS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getInstallingSchedulesReducer(state = {}, action) {
    switch (action.type) {
        case EMPLOYEE_TYPE.GET_INSTALLING_SCHEDULES_SUCCESS:
            return action.response;
        case EMPLOYEE_TYPE.GET_INSTALLING_SCHEDULES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const employeeReducer = combineReducers({
    employees: getEmployeesReducer,
    employeeAll: getAllEmployeesReducer,
    installerAll: getAllInstallersReducer,
    installerReports: getInstallerReportsReducer,
    installingSchedules: getInstallingSchedulesReducer,
    employeeAdd: addEmployeeReducer,
    employeeUpdate: updateEmployeeReducer,
    employeeDelete: deleteEmployeeReducer
});