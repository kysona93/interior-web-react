import { combineReducers } from "redux";
import { routerReducer } from "react-router-redux";
import { reducer as formReducer } from 'redux-form';

/** CUSTOMER TYPE */
import { customerTypeReducer } from './setup/type/customerType';
/* installer */
import { installerReducer } from './installation/installer';
/* occupation */
import { occupationReducer } from './setup/occupation';
/* job */
import { jobReducer } from './setup/job';
/* bank */
import { bankReducer } from './setup/bank';
/* exchange rate */
import { exchangeRateReducer } from './setup/exchangeRate';
/* rate per kwh */
import { ratePerKwhReducer } from './setup/rateKhw';
/* set up ampere */
import { ampereReducer } from './setup/ampere';
/* consumption type */
import { consumptionTypeReducer } from './setup/type/consumptionType';
/* setting */
import {settingReducer} from './setup/setting';
/* pole type */
import { poleTypeReducer } from './setup/type/poleType';
/* box type */
 import { boxTypeReducer } from './setup/type/boxType';
/* transformer type */
import {  transformerTypeReducer } from './setup/type/transformerType';
/* breaker type */
import { breakerTypeReducer } from './setup/type/breakerType';
/* meter type */
import { meterTypeReducer } from './setup/type/meterType';
/* meter phase */
import { meterPhaseReducer } from './setup/meterPhase';
/* meter usage */
import { meterUsageReducer } from './setup/meterUsage';
/* meter */
import { meterReducer } from './setup/meter';
/* billing item */
import { itemReducer } from './inventory/item';
/** user authentication */
import{ userAuthenticationReducer } from './authentication';
/* menu */
import { menuReducer } from './menu';
/* page */
import { pageReducer } from './page';
/** LOCATION **/
import { locationReducer } from './location';
/** AREA **/
import { areaReducer } from './setup/area';
/** TRANSFORMER **/
import {transformerReducer} from './setup/transformer';
/* report */
import { groupReportReducer } from './report/gropReport';
import { icCardDailySaleReducer } from './report/icCardDailySale';
import { reportRefReducer } from './report/reportRef';
import { reportSubsidySaleLVReducer } from './report/subsidy/reportSaleLV';
import { reportSubsidySaleMVReducer } from './report/subsidy/reportSaleMV';
/* inventory */
import { unitTypeReducer } from  './inventory/unitType';
import { itemGroupReducer } from './inventory/itemGroup';
import { stockInItemReducer } from './inventory/stockInItem';
import { ItemCategoryReducer } from './inventory/itemCategory';
import { planningReducer } from './inventory/planning';
import { requestReducer } from './inventory/request';
import { connectionTypeReducer } from './inventory/connectionType';
import { purchaseKwhReducer } from './inventory/purchaseKwh';
/** POLE **/
import {poleReducer} from './setup/pole';
/** BOX **/
import {boxReducer} from './setup/box';
/** voltage **/
import {voltageTypeReducer} from './setup/type/voltageType';
/** supplier **/
import {supplierReducer} from './inventory/supplier';
/* finance */
import { paymentTypesReducer } from './customer/finance/paymentType';
import { locationPaysReducer } from './customer/finance/locationPay';
/** invoice **/
import { groupInvoiceReducer } from './invoice/groupInvoices';
import {invoiceTypeReducer} from './invoice/invoiceType';
import {invoicesReducer} from './invoice/invoice';
import { customerInvoicesReducer } from './invoice/customerInvoice';
import { customerBookPaymentReducer } from './invoice/bookPayment';
/** EMPLOYEE **/
import {employeeReducer} from './employee/employee';
/* location */
import { installingReducer } from './customer/network/installation';
/* installation */
import { changeMeterReducer } from './customer/network/meter';
/** SALE_TYPE **/
import {SaleTypeReducer} from './setup/type/saleType';
/** CUSTOMER **/
import {customerReducer} from './customer/customer';
import {punishRatesReducer} from './transaction/punish/punishRate';
import {punishCustomersReducer} from './transaction/punish/punishCustomer';
/** CUSTOMER GROUP **/
import { customerGroupReducer } from  './setup/type/customerGroup';
/** BREAKER **/
import { breakerReducer } from './setup/breaker';
/* transaction */
import { invoicesPrintingReducer } from './transaction/invoicePrinting';
import { issueBillsReducer } from './transaction/issueBill';
import { quickPaymentsReducer } from './transaction/quickPayment';
import { recordKhwMonthlyReducer } from './transaction/recordKhwMonthly';
import { usageEntryReducer } from './transaction/usageEntry';
import { rateRangesReducer } from './setup/rateRange';
import { prepaidPowerReducer } from './transaction/prepaid/prepaidPower';
import { balancePrepaidKwhReducer } from './transaction/prepaid/balancePrepaidKhw';
import { returnInvoicesReducer } from './transaction/prepaid/returnInvoice';
import { lostKwhReducer } from './transaction/prepaid/lostKwh';
import { kwPerDayReducer } from './transaction/prepaid/kwPerDay';
/* accounting */
import { cashBoxesReducer } from './accounting/cashBox';

import { mailSettingsReducer } from './security/mailSettings';
import { stepWorkReducer } from './installation/stepwork/stepwork';
import { rentPoleReducer } from './customer/network/license';


/**
 * REPORT
 * @type {Reducer<S>}
 */
import { transformerReportReducer } from './report/transformer';

export const reducers = combineReducers({
    routing: routerReducer,
    form: formReducer,

    /* authentication */
    userAuthentication: userAuthenticationReducer,
    /* menu */
    menu: menuReducer,
    /* page */
    page: pageReducer,

    /** CUSTOMER TYPE */
    customerType: customerTypeReducer,

    /* installer */
    installer: installerReducer,
    /* occupation */
    occupation: occupationReducer,
    /* job */
    job: jobReducer,
    /* bank */
    bank: bankReducer,
    /* exchange rate */
    exchangeRate: exchangeRateReducer,
    /* rate per khw */
    ratePerKwh: ratePerKwhReducer,
    /* set up ampere */
    ampere: ampereReducer,
    /* consumption type */
    consumptionType: consumptionTypeReducer,
    /* setting */
    setting: settingReducer,
    /* pole type */
    poleType: poleTypeReducer,
    /* box type */
    boxType: boxTypeReducer,
    /* transformer type */
    transformerType: transformerTypeReducer,
    /* breaker type */
    breakerType: breakerTypeReducer,
    /* meter type */
    meterType: meterTypeReducer,
    /* meter phase */
    meterPhase : meterPhaseReducer,
    /* meter usage */
    meterUsage: meterUsageReducer,
    /** Inventory Item **/
    item: itemReducer,
    /* meter */
    meter: meterReducer,
    /** LOCATION **/
    location: locationReducer,
    /** AREA **/
    area: areaReducer,
    /** TRANSFORMER **/
    transformer: transformerReducer,
    /** POLE **/
    pole: poleReducer,
    /** BOX **/
    box: boxReducer,
    /** EMPLOYEE **/
    employee: employeeReducer,
    punishRates: punishRatesReducer,
    punishCustomers: punishCustomersReducer,
    /** CUSTOMER **/
    customer: customerReducer,
    /** CUSTOMER GROUP **/
    customerGroup: customerGroupReducer,
    /** BREAKER **/
    breaker: breakerReducer,
    /* report */
    groupReport : groupReportReducer,
    icCardDailySale : icCardDailySaleReducer,
    reportRef : reportRefReducer,
    reportSubsidySaleLV : reportSubsidySaleLVReducer,
    reportSubsidySaleMV : reportSubsidySaleMVReducer,
    /* inventory */
    unitType: unitTypeReducer,
    itemGroup: itemGroupReducer,
    stockInItem: stockInItemReducer,
    itemCategory: ItemCategoryReducer,
    planning: planningReducer,
    request: requestReducer,
    connectionType: connectionTypeReducer,
    purchaseKwh: purchaseKwhReducer,

    /* finance */
    paymentTypes: paymentTypesReducer,
    locationPays: locationPaysReducer,

    /* invoice */
    groupInvoice: groupInvoiceReducer,
    invoices: invoicesReducer,
    customerInvoices : customerInvoicesReducer,

    /** sale type **/
    saleType:SaleTypeReducer,
    customerBookPayment: customerBookPaymentReducer,

    /** network Management **/
    installing: installingReducer,
    stepWork: stepWorkReducer,
    changeMeter: changeMeterReducer,
    /** voltage **/
    voltageType: voltageTypeReducer,
    supplier:supplierReducer,
    invoiceType:invoiceTypeReducer,

    /* transaction */
    invoicesPrinting: invoicesPrintingReducer,
    issueBills: issueBillsReducer,
    quickPayments: quickPaymentsReducer,
    recordKhwMonthly: recordKhwMonthlyReducer,
    usageEntry: usageEntryReducer,
    prepaidPower: prepaidPowerReducer,
    balancePrepaidKwh: balancePrepaidKwhReducer,
    returnInvoices: returnInvoicesReducer,
    lostKwh: lostKwhReducer,
    kwPerDay: kwPerDayReducer,

    /** mail settings */
    mailSettings: mailSettingsReducer,
    
    rateRanges: rateRangesReducer,
    /* accounting */
    rentPole: rentPoleReducer,
    cashBoxes: cashBoxesReducer,
    /**
     * REPORT
     */
    transformerReport: transformerReportReducer
});