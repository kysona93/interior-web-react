import { combineReducers } from "redux";
import {
    GET_ALL_CUSTOMER_BILLS_FAIL,
    GET_ALL_CUSTOMER_BILLS_SUCCESS,
    ISSUE_BILL_CUSTOMERS_FAIL,
    ISSUE_BILL_CUSTOMERS_SUCCESS,
    GET_ALL_CUSTOMERS_PROBLEM_FAIL,
    GET_ALL_CUSTOMERS_PROBLEM_SUCCESS
} from '../../actions/transaction/issueBill';

function getAllCustomerBillsReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMER_BILLS_SUCCESS:
            return action.response;
        case GET_ALL_CUSTOMER_BILLS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function issueCustomerBillsReducer(state = {}, action) {
    switch (action.type) {
        case ISSUE_BILL_CUSTOMERS_SUCCESS:
            return action.response;
        case ISSUE_BILL_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomersProblemReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMERS_PROBLEM_SUCCESS:
            return action.response;
        case GET_ALL_CUSTOMERS_PROBLEM_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const issueBillsReducer = combineReducers({
    getAllCustomerBills: getAllCustomerBillsReducer,
    issueCustomerBills: issueCustomerBillsReducer,
    getAllCustomersProblem: getAllCustomersProblemReducer
});