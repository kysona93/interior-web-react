import { combineReducers } from "redux";
import {
    GET_ALL_RECORD_KWH_MONTHLY_SUCCESS,
    GET_ALL_RECORD_KWH_MONTHLY_FAIL
} from '../../actions/transaction/recordKhwMonthly';

function getAllRecordKhwMonthlyReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_RECORD_KWH_MONTHLY_SUCCESS:
            return action.response;
        case GET_ALL_RECORD_KWH_MONTHLY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const recordKhwMonthlyReducer = combineReducers({
    getAllRecordKhwMonthly: getAllRecordKhwMonthlyReducer
});