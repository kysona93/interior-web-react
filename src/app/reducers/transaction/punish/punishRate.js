import { combineReducers } from "redux";
import {
    ADD_PUNISH_RATE_FAIL,
    ADD_PUNISH_RATE_SUCCESS,
    GET_ALL_PUNISH_RATE_FAIL,
    GET_ALL_PUNISH_RATE_SUCCESS,
    UPDATE_PUNISH_RATE_FAIL,
    UPDATE_PUNISH_RATE_SUCCESS,
    DELETE_PUNISH_RATE_FAIL,
    DELETE_PUNISH_RATE_SUCCESS
} from '../../../actions/transaction/punish/punishRate';

function getAllPunishRatesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_PUNISH_RATE_SUCCESS:
            return action.response;
        case GET_ALL_PUNISH_RATE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addPunishRateReducer(state = {}, action) {
    switch (action.type) {
        case ADD_PUNISH_RATE_SUCCESS:
            return action.response;
        case ADD_PUNISH_RATE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updatePunishRateReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_PUNISH_RATE_SUCCESS:
            return action.response;
        case UPDATE_PUNISH_RATE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deletePunishRateReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_PUNISH_RATE_SUCCESS:
            return action.response;
        case DELETE_PUNISH_RATE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const punishRatesReducer = combineReducers({
    getAllPunishRates: getAllPunishRatesReducer,
    addPunishRate: addPunishRateReducer,
    updatePunishRate: updatePunishRateReducer,
    deletePunishRate: deletePunishRateReducer
});