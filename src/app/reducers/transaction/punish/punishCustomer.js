import { combineReducers } from "redux";
import {
    GET_ALL_PUNISH_INVOICES_FAIL,
    GET_ALL_PUNISH_INVOICES_SUCCESS,
    ADD_PUNISH_CUSTOMERS_FAIL,
    ADD_PUNISH_CUSTOMERS_SUCCESS,
    GET_ALL_PUNISH_CUSTOMERS_FAIL,
    GET_ALL_PUNISH_CUSTOMERS_SUCCESS,
    UPDATE_PUNISH_CUSTOMER_FAIL,
    UPDATE_PUNISH_CUSTOMER_SUCCESS,
    DELETE_PUNISH_CUSTOMER_FAIL,
    DELETE_PUNISH_CUSTOMER_SUCCESS
} from '../../../actions/transaction/punish/punishCustomer';

function getAllInvoicesPunishReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_PUNISH_INVOICES_SUCCESS:
            return action.response;
        case GET_ALL_PUNISH_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addCustomerPunishReducer(state = {}, action) {
    switch (action.type) {
        case ADD_PUNISH_CUSTOMERS_SUCCESS:
            return action.response;
        case ADD_PUNISH_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomersPunishReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_PUNISH_CUSTOMERS_SUCCESS:
            return action.response;
        case GET_ALL_PUNISH_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateCustomerPunishReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_PUNISH_CUSTOMER_SUCCESS:
            return action.response;
        case UPDATE_PUNISH_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteCustomerPunishReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_PUNISH_CUSTOMER_SUCCESS:
            return action.response;
        case DELETE_PUNISH_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const punishCustomersReducer = combineReducers({
    getAllInvoicesPunish: getAllInvoicesPunishReducer,
    addCustomerPunish: addCustomerPunishReducer,
    getAllCustomersPunish: getAllCustomersPunishReducer,
    updateCustomerPunish: updateCustomerPunishReducer,
    deleteCustomerPunish: deleteCustomerPunishReducer
});