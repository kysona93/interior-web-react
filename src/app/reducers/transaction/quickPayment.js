import { combineReducers } from "redux";
import {
    GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS,
    GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL,
    ADD_QUICK_PAID_PAYMENT_FAIL,
    ADD_QUICK_PAID_PAYMENT_SUCCESS,
    GET_INVOICE_BY_BARCODE_FAIL,
    GET_INVOICE_BY_BARCODE_SUCCESS
} from '../../actions/transaction/quickPayment';

function getAllInvoicesCustomerPaymentReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS:
            return action.response;
        case GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addQuickPaidPaymentReducer(state = {}, action) {
    switch (action.type) {
        case ADD_QUICK_PAID_PAYMENT_SUCCESS:
            return action.response;
        case ADD_QUICK_PAID_PAYMENT_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getInvoiceByBarcodeReducer(state = {}, action) {
    switch (action.type) {
        case GET_INVOICE_BY_BARCODE_SUCCESS:
            return action.response;
        case GET_INVOICE_BY_BARCODE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const quickPaymentsReducer = combineReducers({
    getAllInvoicesCustomerPayment: getAllInvoicesCustomerPaymentReducer,
    addQuickPaidPayment: addQuickPaidPaymentReducer,
    getInvoiceByBarcode: getInvoiceByBarcodeReducer
});