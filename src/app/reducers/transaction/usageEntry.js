import { combineReducers } from "redux";
import {
    GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL,
    GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS,
    IMPORT_EXCEL_METER_USAGE_RECORD_FAIL,
    IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS,
    ADD_METER_USAGE_RECORD_FAIL,
    ADD_METER_USAGE_RECORD_SUCCESS,
    GET_METERS_BY_CUSTOMER_ID_FAIL,
    GET_METERS_BY_CUSTOMER_ID_SUCCESS,
    GET_METER_USAGE_BY_METER_SERIAL_FAIL,
    GET_METER_USAGE_BY_METER_SERIAL_SUCCESS,
    UPDATE_METER_USAGE_BY_ID_FAIL,
    UPDATE_METER_USAGE_BY_ID_SUCCESS,
    VERIFY_METER_USAGE_INDIVIDUAL_FAIL,
    VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS,
    GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL,
    GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS,
    GET_METER_USAGE_PROBLEMS_FAIL,
    GET_METER_USAGE_PROBLEMS_SUCCESS
} from '../../actions/transaction/usageEntry';

function getCustomerInfoByMeterSerialReducer(state = {}, action) {
    switch (action.type) {
        case GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS:
            return action.response;
        case GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addMeterUsageRecordReducer(state = {}, action) {
    switch (action.type) {
        case ADD_METER_USAGE_RECORD_SUCCESS:
            return action.response;
        case ADD_METER_USAGE_RECORD_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function importExcelMeterUsageRecordReducer(state = {}, action) {
    switch (action.type) {
        case IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS:
            return action.response;
        case IMPORT_EXCEL_METER_USAGE_RECORD_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getMetersByCustomerIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_METERS_BY_CUSTOMER_ID_SUCCESS:
            return action.response;
        case GET_METERS_BY_CUSTOMER_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getMeterUsageBySerialReducer(state = {}, action) {
    switch (action.type) {
        case GET_METER_USAGE_BY_METER_SERIAL_SUCCESS:
            return action.response;
        case GET_METER_USAGE_BY_METER_SERIAL_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateMeterUsageByIdReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_METER_USAGE_BY_ID_SUCCESS:
            return action.response;
        case UPDATE_METER_USAGE_BY_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function verifyMeterUsageIndividualReducer(state = {}, action) {
    switch (action.type) {
        case VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS:
            return action.response;
        case VERIFY_METER_USAGE_INDIVIDUAL_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllCustomerIdsAndMeterSerialsReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS:
            return action.response;
        case GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getMeterUsageProblemsReducer(state = {}, action) {
    switch (action.type) {
        case GET_METER_USAGE_PROBLEMS_FAIL:
            return action.response;
        case GET_METER_USAGE_PROBLEMS_SUCCESS:
            return action.error.response;
        default:
            return state;
    }
}

export const usageEntryReducer = combineReducers({
    getCustomerInfoByMeterSerial: getCustomerInfoByMeterSerialReducer,
    addMeterUsageRecord: addMeterUsageRecordReducer,
    importExcelMeterUsageRecord: importExcelMeterUsageRecordReducer,
    getMetersByCustomerId: getMetersByCustomerIdReducer,
    getMeterUsageBySerial: getMeterUsageBySerialReducer,
    updateMeterUsageById: updateMeterUsageByIdReducer,
    verifyMeterUsageIndividual: verifyMeterUsageIndividualReducer,
    getAllCustomerIdsAndMeterSerials: getAllCustomerIdsAndMeterSerialsReducer,
    meterUsageProblems: getMeterUsageProblemsReducer
});