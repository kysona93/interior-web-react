import { combineReducers } from "redux";
import {
    GET_ALL_ISSUE_INVOICES_FAIL,
    GET_ALL_ISSUE_INVOICES_SUCCESS,
    GENERATE_ALL_INVOICE_FAIL,
    GENERATE_ALL_INVOICE_SUCCESS
} from '../../actions/transaction/invoicePrinting';

function getAllIssueInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_ISSUE_INVOICES_SUCCESS:
            return action.response;
        case GET_ALL_ISSUE_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function generateAllInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GENERATE_ALL_INVOICE_SUCCESS:
            return action.response;
        case GENERATE_ALL_INVOICE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const invoicesPrintingReducer = combineReducers({
    getAllIssueInvoices: getAllIssueInvoicesReducer,
    generateAllInvoices: generateAllInvoicesReducer
});