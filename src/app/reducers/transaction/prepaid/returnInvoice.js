import { combineReducers } from "redux";
import {
    ISSUE_BILL_PREPAID_SUCCESS,
    ISSUE_BILL_PREPAID_FAIL,
    GET_ALL_RETURN_INVOICES_SUCCESS,
    GET_ALL_RETURN_INVOICES_FAIL,
    GENERATE_RETURN_INVOICES_SUCCESS,
    GENERATE_RETURN_INVOICES_FAIL,
    GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL,
    GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS,
    UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL,
    UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS
} from '../../../actions/transaction/prepaid/returnInvoice';

function addIssueBillPrepaidReducer(state = {}, action) {
    switch (action.type) {
        case ISSUE_BILL_PREPAID_SUCCESS:
            return action.response;
        case ISSUE_BILL_PREPAID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllReturnInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_RETURN_INVOICES_SUCCESS:
            return action.response;
        case GET_ALL_RETURN_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function generateReturnInvoicesReducer(state = {}, action) {
    switch (action.type) {
        case GENERATE_RETURN_INVOICES_SUCCESS:
            return action.response;
        case GENERATE_RETURN_INVOICES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getReturnInvoiceByPrepaidIdReducer(state = {}, action) {
    switch (action.type) {
        case GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS:
            return action.response;
        case GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateReturnInvoiceByPrepaidIdReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS:
            return action.response;
        case UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const returnInvoicesReducer = combineReducers({
    addIssueBillPrepaid: addIssueBillPrepaidReducer,
    getAllReturnInvoices: getAllReturnInvoicesReducer,
    generateReturnInvoices: generateReturnInvoicesReducer,
    returnInvoiceByPrepaidId: getReturnInvoiceByPrepaidIdReducer,
    updateReturnInvoiceByPrepaidId: updateReturnInvoiceByPrepaidIdReducer
});