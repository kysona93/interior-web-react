import { combineReducers } from "redux";
import * as ACTION from '../../../actions/transaction/prepaid/prepaidPower';

function getPrepaidPowersReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.GET_PREPAID_POWERS_SUCCESS:
            return action.response;
        case ACTION.GET_PREPAID_POWERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addPrepaidPowerReducer(state = {}, action) {
    switch (action.type) {
        case ACTION.ADD_PREPAID_POWER_SUCCESS:
            return action.response;
        case ACTION.ADD_PREPAID_POWER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllPrepaidPowersReducer(state = {}, action){
    switch (action.type) {
        case ACTION.GET_ALL_PREPAID_POWERS_SUCCESS:
            return action.response;
        case ACTION.GET_ALL_PREPAID_POWERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllPrepaidEndPurchaseReducer(state = {}, action){
    switch (action.type) {
        case ACTION.GET_ALL_PREPAID_END_PURCHASES_SUCCESS:
            return action.response;
        case ACTION.GET_ALL_PREPAID_END_PURCHASES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const prepaidPowerReducer = combineReducers({
    prepaidPowers: getPrepaidPowersReducer,
    prepaidPowerAdd: addPrepaidPowerReducer,
    getAllPrepaidPowers: getAllPrepaidPowersReducer,
    getAllPrepaidEndPurchase: getAllPrepaidEndPurchaseReducer
});