import { combineReducers } from "redux";
import {
    GET_ALL_GENERATE_KW_PER_DAY_SUCCESS,
    GET_ALL_GENERATE_KW_PER_DAY_FAIL,
    GENERATE_KW_PER_DAY_SUCCESS,
    GENERATE_KW_PER_DAY_FAIL
} from '../../../actions/transaction/prepaid/kwPerDay';

function getAllGenerateKwPerDayReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_GENERATE_KW_PER_DAY_SUCCESS:
            return action.response;
        case GET_ALL_GENERATE_KW_PER_DAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function generateKwPerDayReducer(state = {}, action) {
    switch (action.type) {
        case GENERATE_KW_PER_DAY_SUCCESS:
            return action.response;
        case GENERATE_KW_PER_DAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const kwPerDayReducer = combineReducers({
    getAllGenerateKwPerDay: getAllGenerateKwPerDayReducer,
    generateKwPerDay: generateKwPerDayReducer
});