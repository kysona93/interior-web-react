import { combineReducers } from "redux";
import {
    ADD_BALANCE_PREPAID_KWH_SUCCESS,
    ADD_BALANCE_PREPAID_KWH_FAIL,
    GET_ALL_BALANCE_PREPAID_KWH_SUCCESS,
    GET_ALL_BALANCE_PREPAID_KWH_FAIL,
    GET_ALL_BILL_BALANCE_PREPAID_SUCCESS,
    GET_ALL_BILL_BALANCE_PREPAID_FAIL,
    VOID_BALANCE_PREPAID_KW_FAIL,
    VOID_BALANCE_PREPAID_KW_SUCCESS,
    GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL,
    GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS,
    SAVE_BALANCE_PREPAID_KWH_FAIL,
    SAVE_BALANCE_PREPAID_KWH_SUCCESS,
    LIST_BALANCE_PREPAID_KW_FAIL,
    LIST_BALANCE_PREPAID_KW_SUCCESS,
    UPDATE_BALANCE_PREPAID_KW_FAIL,
    UPDATE_BALANCE_PREPAID_KW_SUCCESS
} from '../../../actions/transaction/prepaid/balancePrepaidKwh';

function addBalancePrepaidKhwReducer(state = {}, action) {
    switch (action.type) {
        case ADD_BALANCE_PREPAID_KWH_SUCCESS:
            return action.response;
        case ADD_BALANCE_PREPAID_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllBalancePrepaidKhwReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_BALANCE_PREPAID_KWH_SUCCESS:
            return action.response;
        case GET_ALL_BALANCE_PREPAID_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllBillBalancePrepaidReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_BILL_BALANCE_PREPAID_SUCCESS:
            return action.response;
        case GET_ALL_BILL_BALANCE_PREPAID_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function voidBalancePrepaidReducer(state = {}, action) {
    switch (action.type) {
        case VOID_BALANCE_PREPAID_KW_SUCCESS:
            return action.response;
        case VOID_BALANCE_PREPAID_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllNewPrepaidCustomersReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS:
            return action.response;
        case GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function saveBalancePrepaidKwhReducer(state = {}, action) {
    switch (action.type) {
        case SAVE_BALANCE_PREPAID_KWH_SUCCESS:
            return action.response;
        case SAVE_BALANCE_PREPAID_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function listAllBalancePrepaidKwhReducer(state = {}, action) {
    switch (action.type) {
        case LIST_BALANCE_PREPAID_KW_SUCCESS:
            return action.response;
        case LIST_BALANCE_PREPAID_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateAllBalancePrepaidKwhReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_BALANCE_PREPAID_KW_SUCCESS:
            return action.response;
        case UPDATE_BALANCE_PREPAID_KW_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const balancePrepaidKwhReducer = combineReducers({
    addBalancePrepaidKhw: addBalancePrepaidKhwReducer,
    getAllBalancePrepaidKhw: getAllBalancePrepaidKhwReducer,
    getAllBillBalancePrepaid: getAllBillBalancePrepaidReducer,
    voidBalancePrepaid: voidBalancePrepaidReducer,
    getAllNewPrepaidCustomers: getAllNewPrepaidCustomersReducer,
    saveBalancePrepaidKwh: saveBalancePrepaidKwhReducer,

    listAllBalancePrepaidKwh: listAllBalancePrepaidKwhReducer,
    updateAllBalancePrepaidKwh: updateAllBalancePrepaidKwhReducer
});