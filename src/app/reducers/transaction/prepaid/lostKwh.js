import { combineReducers } from "redux";
import {
    ADD_LOST_KWH_FAIL,
    ADD_LOST_KWH_SUCCESS,
    GET_ALL_LOST_KWH_FAIL,
    GET_ALL_LOST_KWH_SUCCESS,
    GET_ALL_METER_IDS_SUCCESS,
    GET_ALL_METER_IDS_FAIL,
    GET_ALL_LOST_KWH_STATUS_FAIL,
    GET_ALL_LOST_KWH_STATUS_SUCCESS
} from '../../../actions/transaction/prepaid/lostKwh';

function addLostKhwReducer(state = {}, action) {
    switch (action.type) {
        case ADD_LOST_KWH_SUCCESS:
            return action.response;
        case ADD_LOST_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllLostKwhReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_LOST_KWH_SUCCESS:
            return action.response;
        case GET_ALL_LOST_KWH_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllMeterIdsReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_METER_IDS_SUCCESS:
            return action.response;
        case GET_ALL_METER_IDS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllLostKwhStatusReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_LOST_KWH_STATUS_SUCCESS:
            return action.response;
        case GET_ALL_LOST_KWH_STATUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const lostKwhReducer = combineReducers({
    addLostKhw: addLostKhwReducer,
    getAllLostKwh: getAllLostKwhReducer,
    getAllMeterIds: getAllMeterIdsReducer,
    getAllLostKwhStatus: getAllLostKwhStatusReducer
});