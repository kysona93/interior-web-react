import { combineReducers } from "redux";
import * as MAIL_SETTINGS_ACTION from './../../actions/security/mailSettings';

function getAllMailSettingsReducer(state = {}, action) {
    switch (action.type) {
        case MAIL_SETTINGS_ACTION.GET_ALL_MAIL_SETTINGS_SUCCESS:
            return action.response;
        case MAIL_SETTINGS_ACTION.GET_ALL_MAIL_SETTINGS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function addMailSettingsReducer(state = {}, action) {
    switch (action.type) {
        case MAIL_SETTINGS_ACTION.ADD_MAIL_SETTINGS_SUCCESS:
            return action.response;
        case MAIL_SETTINGS_ACTION.ADD_MAIL_SETTINGS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateMailSettingsReducer(state = {}, action) {
    switch (action.type) {
        case MAIL_SETTINGS_ACTION.UPDATE_MAIL_SETTINGS_SUCCESS:
            return action.response;
        case MAIL_SETTINGS_ACTION.UPDATE_MAIL_SETTINGS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteMailSettingsReducer(state = {}, action) {
    switch (action.type) {
        case MAIL_SETTINGS_ACTION.DELETE_MAIL_SETTINGS_SUCCESS:
            return action.response;
        case MAIL_SETTINGS_ACTION.DELETE_MAIL_SETTINGS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const mailSettingsReducer = combineReducers({
    mailSettingsAll: getAllMailSettingsReducer,
    mailSettingsAdd: addMailSettingsReducer,
    mailSettingsUpdate: updateMailSettingsReducer,
    mailSettingsDelete: deleteMailSettingsReducer
});