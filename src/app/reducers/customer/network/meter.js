import {combineReducers} from "redux";
import * as METER from './../../../actions/customer/network/meter';

function updateChangeMeterReducer(state = {}, action){
    switch (action.type){
        case METER.UPDATE_CHANGE_METER_SUCCESS:
            return action.response;
        case METER.UPDATE_CHANGE_METER_FAIL:
            return action.error.data;
        default :
            return state;
    }
}

function addItemMvReduce(state = {} , action){
    switch (action.type){
        case METER.ADD_ITEM_MV_SUCCESS:
            return action.response;
        case METER.ADD_ITEM_MV_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function updateItemMvReduce(state = {} , action){
    switch (action.type){
        case METER.UPDATE_ITEM_MV_SUCCESS:
            return action.response;
        case METER.UPDATE_ITEM_MV_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function deleteItemMvReduce(state = {} , action){
    switch (action.type){
        case METER.DELETE_ITEMS_MV_SUCCESS:
            return action.response;
        case METER.DELETE_ITEMS_MV_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getItemsMvReduce(state = {} , action){
    switch (action.type){
        case METER.GET_ITEMS_MV_SUCCESS:
            return action.response;
        case METER.GET_ITEMS_MV_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getAllItemsMvReduce(state = {} , action){
    switch (action.type){
        case METER.GET_ALL_ITEMS_MV_SUCCESS:
            return action.response;
        case METER.GET_ALL_ITEMS_MV_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getAllItemsMvSummaryReduce(state = {} , action){
    switch (action.type){
        case METER.GET_ITEMS_MV_SUMMARY_SUCCESS:
            return action.response;
        case METER.GET_ITEMS_MV_SUMMARY_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getConnectionPointReduce(state = {} , action){
    switch (action.type){
        case METER.GET_CONNECTION_POINT_SUCCESS:
            return action.response;
        case METER.GET_CONNECTION_POINT_FAIL:
            return action.error.response;
        default :
            return state;
    }
}

function updateTransferMeterReduce(state = {} , action){
    switch (action.type){
        case METER.UPDATE_TRANSFORMER_METER_SUCCESS:
            return action.response;
        case METER.UPDATE_TRANSFORMER_METER_FAIL:
            return action.error.response;
        default :
            return state;
    }
}

function getMeterHistoryReduce(state = {} , action){
    switch (action.type){
        case METER.GET_METER_HISTORY_SUCCESS:
            return action.response;
        case METER.GET_METER_HISTORY_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getAllMeterReduce(state = {} , action){
    switch (action.type){
        case METER.GET_FREE_METERS_SUCCESS:
            return action.response;
        case METER.GET_FREE_METERS_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getUsedMetersReduce(state = {} , action){
    switch (action.type){
        case METER.GET_USED_METERS_SUCCESS:
            return action.response;
        case METER.GET_USED_METERS_FAIL:
            return action.response;
        default :
            return state;
    }
}
export const changeMeterReducer = combineReducers({
    meterUpdate: updateChangeMeterReducer,
    transferUpdate:updateTransferMeterReduce,
    meterHistoryAll:getMeterHistoryReduce,
    meterFree:getAllMeterReduce,
    meterUsage:getUsedMetersReduce,
    itemMvAdd: addItemMvReduce,
    itemMVUpdate:updateItemMvReduce,
    itemMvDelete: deleteItemMvReduce,
    itemMvGet: getItemsMvReduce,
    itemMvGetAll: getAllItemsMvReduce,
    itemMvSummaryGet: getAllItemsMvSummaryReduce,
    connectionPointGet: getConnectionPointReduce
});