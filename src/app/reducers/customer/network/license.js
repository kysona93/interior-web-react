import {combineReducers} from 'redux';
import * as RENT from './../../../actions/customer/network/license';

function addRentPoleReducer(state = {}, action){
    switch (action.type){
        case RENT.ADD_RENT_POLE_SUCCESS:
            return action.response;
        case RENT.ADD_RENT_POLE_FAIL:
            return action.error;
        default :
            return state;
    }
}
function updateRentPoleReducer(state = {}, action){
    switch (action.type){
        case RENT.UPDATE_RENT_POLE_SUCCESS:
            return action.response;
        case RENT.UPDATE_RENT_POLE_FAIL:
            return action.error;
        default :
            return state;
    }
}
function deleteRentPoleReducer(state = {}, action){
    switch (action.type){
        case RENT.DELETE_RENT_POLE_SUCCESS:
            return action.response;
        case RENT.DELETE_RENT_POLE_FAIL:
            return action.error;
        default :
            return state;
    }
}
function getRentPoleReducer(state = {}, action){
    switch (action.type){
        case RENT.GET_RENT_POLE_SUCCESS:
            return action.response;
        case RENT.GET_RENT_POLE_FAIL:
            return action.error;
        default :
            return state;
    }
}
function getAllRentPoleReducer(state = {}, action){
    switch (action.type){
        case RENT.GET_ALL_RENT_POLE_SUCCESS:
            return action.response;
        case RENT.GET_ALL_RENT_POLE_FAIL:
            return action.error;
        default :
            return state;
    }
}
/** Rent Part **/
function addRentPartReducer(state = {}, action){
    switch (action.type){
        case RENT.ADD_RENT_PART_SUCCESS:
            return action.response;
        case RENT.ADD_RENT_PART_FAIL:
            return action.error;
        default :
            return state;
    }
}

function updateRentPartReducer(state = {}, action){
    switch (action.type){
        case RENT.UPDATE_RENT_PART_SUCCESS:
            return action.response;
        case RENT.UPDATE_RENT_PART_FAIL:
            return action.error;
        default :
            return state;
    }
}

function deleteRentPartReducer(state = {}, action){
    switch (action.type){
        case RENT.DELETE_RENT_PART_SUCCESS:
            return action.response;
        case RENT.DELETE_RENT_PART_FAIL:
            return action.error;
        default :
            return state;
    }
}

function getRentPartReducer(state = {}, action){
    switch (action.type){
        case RENT.GET_RENT_PART_SUCCESS:
            return action.response;
        case RENT.GET_RENT_PART_FAIL:
            return action.error;
        default :
            return state;
    }
}

function getAllRentPartReducer(state = {}, action){
    switch (action.type){
        case RENT.GET_ALL_RENT_PART_SUCCESS:
            return action.response;
        case RENT.GET_ALL_RENT_PART_FAIL:
            return action.error;
        default :
            return state;
    }
}
export const rentPoleReducer = combineReducers({
    rentPartAdd: addRentPartReducer,
    rentPartUpdate: updateRentPartReducer,
    rentPartDelete: deleteRentPartReducer,
    rentPartGet: getRentPartReducer,
    rentPartGetAll: getAllRentPartReducer,
    rentPoleAdd: addRentPoleReducer,
    rentPoleUpdate: updateRentPoleReducer,
    rentPoleDelete: deleteRentPoleReducer,
    rentPoleGet: getRentPoleReducer,
    rentPoleGetAll: getAllRentPoleReducer
});