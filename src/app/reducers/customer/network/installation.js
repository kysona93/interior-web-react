import { combineReducers } from "redux";
import * as INSTALLING from '../../../actions/customer/network/installation';

function getInstallingReducer(state= {} , action){
    switch (action.type){
        case INSTALLING.GET_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.GET_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }

}
function getAllInstallingReducer(state= {} , action){
    switch (action.type){
        case INSTALLING.GET_ALL_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.GET_ALL_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }

}
function getScheduleInstallingReducer(state= {} , action){
    switch (action.type){
        case INSTALLING.GET_SCHEDULE_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.GET_SCHEDULE_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }

}
function updateInstallingReducer(state = {} , action){
    switch (action.type){
        case INSTALLING.UPDATE_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.UPDATE_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }
}

function deleteInstallingReducer(state = {} , action){
    switch (action.type){
        case INSTALLING.DELETE_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.DELETE_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function addInstallingReducer(state = {} , action){
    switch (action.type){
        case INSTALLING.ADD_INSTALLING_SUCCESS:
            return action.response;
        case INSTALLING.ADD_INSTALLING_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function addGenerateFormRequestReducer(state = {} , action){
    switch (action.type){
        case INSTALLING.ADD_GENERATE_FORM_REQUEST_SUCCESS:
            return action.response;
        case INSTALLING.ADD_GENERATE_FORM_REQUEST_FAIL:
            return action.error.response;
        default :
            return state;
    }
}
function getAllGenerateFormRequestReducer(state = {} , action){
    switch (action.type){
        case INSTALLING.GET_ALL_GENERATE_FORM_REQUEST_SUCCESS:
            return action.response;
        case INSTALLING.GET_ALL_GENERATE_FORM_REQUEST_FAIL:
            return action.error.response;
        default :
            return state;
    }
}

export const installingReducer = combineReducers({
    installingGet: getInstallingReducer,
    installingAll: getAllInstallingReducer,
    installingGetSchedule:getScheduleInstallingReducer,
    installingUpdate: updateInstallingReducer,
    installingDelete : deleteInstallingReducer,
    installingAdd : addInstallingReducer,
    formRequestAdd: addGenerateFormRequestReducer,
    formRequestGetAll: getAllGenerateFormRequestReducer
});