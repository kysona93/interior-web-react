import { combineReducers } from "redux";
import * as CUSTOMER_TYPE from '../../actions/customer/customer';

const addCustomerReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.ADD_CUSTOMER_SUCCESS:
            console.log("RE " + JSON.stringify(action.response));
            return action.response;
        case CUSTOMER_TYPE.ADD_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateCustomerReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.UPDATE_CUSTOMER_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE.UPDATE_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteCustomerReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.DELETE_CUSTOMER_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE.DELETE_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getCustomerReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.GET_CUSTOMER_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE.GET_CUSTOMER_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getCustomersReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.GET_CUSTOMERS_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE.GET_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getAllCustomersReducer = (state = {}, action) => {
    switch (action.type) {
        case CUSTOMER_TYPE.GET_ALL_CUSTOMERS_SUCCESS:
            return action.response;
        case CUSTOMER_TYPE.GET_ALL_CUSTOMERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const customerReducer = combineReducers({
    customer: getCustomerReducer,
    customers: getCustomersReducer,
    customerAll: getAllCustomersReducer,
    customerAdd: addCustomerReducer,
    customerUpdate: updateCustomerReducer,
    customerDelete: deleteCustomerReducer
});