import { combineReducers } from "redux";
import {
    ADD_PAYMENT_TYPE_FAIL,
    ADD_PAYMENT_TYPE_SUCCESS,
    GET_ALL_PAYMENT_TYPES_FAIL,
    GET_ALL_PAYMENT_TYPES_SUCCESS,
    UPDATE_PAYMENT_TYPE_FAIL,
    UPDATE_PAYMENT_TYPE_SUCCESS,
    DELETE_PAYMENT_TYPE_FAIL,
    DELETE_PAYMENT_TYPE_SUCCESS
} from './../../../actions/customer/finance/paymentType';

function addPaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case ADD_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case ADD_PAYMENT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllPaymentTypesReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_PAYMENT_TYPES_SUCCESS:
            return action.response;
        case GET_ALL_PAYMENT_TYPES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updatePaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case UPDATE_PAYMENT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deletePaymentTypeReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_PAYMENT_TYPE_SUCCESS:
            return action.response;
        case DELETE_PAYMENT_TYPE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const paymentTypesReducer = combineReducers({
    addPaymentType: addPaymentTypeReducer,
    getAllPaymentTypes :getAllPaymentTypesReducer,
    updatePaymentType: updatePaymentTypeReducer,
    deletePaymentType: deletePaymentTypeReducer
});