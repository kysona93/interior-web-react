import { combineReducers } from "redux";
import {
    ADD_LOCATION_PAY_FAIL,
    ADD_LOCATION_PAY_SUCCESS,
    GET_ALL_LOCATION_PAY_FAIL,
    GET_ALL_LOCATION_PAY_SUCCESS,
    UPDATE_LOCATION_PAY_FAIL,
    UPDATE_LOCATION_PAY_SUCCESS,
    DELETE_LOCATION_PAY_FAIL,
    DELETE_LOCATION_PAY_SUCCESS
} from './../../../actions/customer/finance/locationPay';

function addLocationPayReducer(state = {}, action) {
    switch (action.type) {
        case ADD_LOCATION_PAY_SUCCESS:
            return action.response;
        case ADD_LOCATION_PAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function getAllLocationPaysReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_LOCATION_PAY_SUCCESS:
            return action.response;
        case GET_ALL_LOCATION_PAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function updateLocationPayReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_LOCATION_PAY_SUCCESS:
            return action.response;
        case UPDATE_LOCATION_PAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

function deleteLocationPayReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_LOCATION_PAY_SUCCESS:
            return action.response;
        case DELETE_LOCATION_PAY_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const locationPaysReducer = combineReducers({
    addLocationPay: addLocationPayReducer,
    getAllLocationPays :getAllLocationPaysReducer,
    updateLocationPay: updateLocationPayReducer,
    deleteLocationPay: deleteLocationPayReducer
});