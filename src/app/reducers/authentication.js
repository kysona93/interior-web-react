import { combineReducers } from "redux";
import * as AUTH_ACTION from '../actions/authentication';

/* USER LOGIN */
function userLoginReducer(state = {}, action) {
    switch (action.type) {
        case AUTH_ACTION.USER_LOGIN_SUCCESS:
            return action.response;
        case AUTH_ACTION.USER_LOGIN_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* ADD USER */
function addUserReducer(state = {}, action) {
    switch (action.type) {
        case AUTH_ACTION.ADD_USER_SUCCESS:
            return action.response;
        case AUTH_ACTION.ADD_USER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST USERS */
function getUsersStatusReducer(state = {}, action) {
    switch (action.type) {
        case AUTH_ACTION.GET_USERS_STATUS_SUCCESS:
            return action.response;
        case AUTH_ACTION.GET_USERS_STATUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* GET USER */
function getUserReducer(state = {}, action) {
    switch (action.type) {
        case AUTH_ACTION.GET_USER_SUCCESS:
            return action.response;
        case AUTH_ACTION.GET_USER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* UPDATE USER */
function updateUserReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.UPDATE_USER_SUCCESS:
            return action.response;
        case AUTH_ACTION.UPDATE_USER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* DELETE USER */
function deleteUserReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.DELETE_USER_SUCCESS:
            return action.response;
        case AUTH_ACTION.DELETE_USER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}


/* ADD GROUP */
function addGroupReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.ADD_GROUP_SUCCESS:
            return action.response;
        case AUTH_ACTION.ADD_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST GROUPS */
function getGroupsStatusReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.GET_GROUPS_STATUS_SUCCESS:
            return action.response;
        case AUTH_ACTION.GET_GROUPS_STATUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* GET GROUP */
function getGroupReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.GET_GROUP_SUCCESS:
            return action.response;
        case AUTH_ACTION.GET_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* UPDATE GROUP */
function updateGroupReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.UPDATE_GROUP_SUCCESS:
            return action.response;
        case AUTH_ACTION.UPDATE_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* ENABLED GROUP */
function enabledGroupReducer(state = {}, action){
    switch (action.type) {
        case AUTH_ACTION.ENABLED_GROUP_SUCCESS:
            return action.response;
        case AUTH_ACTION.ENABLED_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const userAuthenticationReducer = combineReducers({
    userLogin: userLoginReducer,
    addUser: addUserReducer,
    getUsersStatus: getUsersStatusReducer,
    getUser: getUserReducer,
    updateUser: updateUserReducer,
    deleteUser: deleteUserReducer,
    addGroup: addGroupReducer,
    getGroupsStatus: getGroupsStatusReducer,
    getGroup: getGroupReducer,
    updateGroup: updateGroupReducer,
    enabledGroup: enabledGroupReducer
});
