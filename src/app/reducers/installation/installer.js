import { combineReducers } from "redux";
import {
    ADD_INSTALLER_SUCCESS,
    ADD_INSTALLER_FAIL,
    GET_ALL_INSTALLERS_SUCCESS,
    GET_ALL_INSTALLERS_FAIL,
    UPDATE_INSTALLER_SUCCESS,
    UPDATE_INSTALLER_FAIL,
    DELETE_INSTALLER_SUCCESS,
    DELETE_INSTALLER_FAIL
} from '../../actions/installation/installer';

/* ADD INSTALLER */
function addInstallerReducer(state = {}, action) {
    switch (action.type) {
        case ADD_INSTALLER_SUCCESS:
            return action.response;
        case ADD_INSTALLER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST ALL INSTALLER */
function getAllInstallersReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_INSTALLERS_SUCCESS:
            return action.response;
        case GET_ALL_INSTALLERS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* UPDATE INSTALLER */
function updateInstallerReducer(state = {}, action) {
    switch (action.type) {
        case UPDATE_INSTALLER_SUCCESS:
            return action.response;
        case UPDATE_INSTALLER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* DELETE INSTALLER */
function deleteInstallerReducer(state = {}, action) {
    switch (action.type) {
        case DELETE_INSTALLER_SUCCESS:
            return action.response;
        case DELETE_INSTALLER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const installerReducer = combineReducers({
    addInstaller: addInstallerReducer,
    getAllInstallers :getAllInstallersReducer,
    updateInstaller: updateInstallerReducer,
    deleteInstaller: deleteInstallerReducer
});