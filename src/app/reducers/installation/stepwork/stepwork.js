import { combineReducers } from "redux";
import * as STEP_WORK_TYPE from '../../../actions/installation/stepwork/stepwork';

const addStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.ADD_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.ADD_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const updateStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.UPDATE_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.UPDATE_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const checkStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.CHECK_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.CHECK_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const deleteStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.DELETE_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.DELETE_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.GET_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.GET_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getStepWorksReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.GET_STEP_WORKS_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.GET_STEP_WORKS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const getStepWorkDetailsReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.GET_STEP_WORK_DETAILS_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.GET_STEP_WORK_DETAILS_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const printStepWorkReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.PRINT_STEP_WORK_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.PRINT_STEP_WORK_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

const removeStepWorkDetailReducer = (state = {}, action) => {
    switch (action.type) {
        case STEP_WORK_TYPE.REMOVE_STEP_WORK_DETAIL_SUCCESS:
            return action.response;
        case STEP_WORK_TYPE.REMOVE_STEP_WORK_DETAIL_FAIL:
            return action.error.response;
        default:
            return state;
    }
};

export const stepWorkReducer = combineReducers({
    workAdd: addStepWorkReducer,
    workUpdate: updateStepWorkReducer,
    workCheck: checkStepWorkReducer,
    workDelete: deleteStepWorkReducer,
    work: getStepWorkReducer,
    works: getStepWorksReducer,
    workDetails: getStepWorkDetailsReducer,
    workPrint: printStepWorkReducer,
    workDetailRemove: removeStepWorkDetailReducer
});