import { combineReducers } from "redux";
import * as PAGE_ACTION from '../actions/page';

/* get pages by group */
function getPagesByGroupReducer(state = {}, action) {
    switch (action.type) {
        case PAGE_ACTION.GET_PAGES_BY_GROUP_SUCCESS:
            return action.response;
        case PAGE_ACTION.GET_PAGES_BY_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* get pages by user */
function getPagesByUserReducer(state = {}, action) {
    switch (action.type) {
        case PAGE_ACTION.GET_PAGES_BY_USER_SUCCESS:
            return action.response;
        case PAGE_ACTION.GET_PAGES_BY_USER_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* get pages by username */
function getPagesByUsernameReducer(state = {}, action) {
    switch (action.type) {
        case PAGE_ACTION.GET_PAGES_BY_USERNAME_SUCCESS:
            return action.response;
        case PAGE_ACTION.GET_PAGES_BY_USERNAME_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

export const pageReducer = combineReducers({
    getPagesByGroup: getPagesByGroupReducer,
    getPagesByUser: getPagesByUserReducer,
    getPagesByUsername: getPagesByUsernameReducer
});