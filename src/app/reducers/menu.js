import { combineReducers } from "redux";
import {
    GET_ALL_MENUS_SUCCESS,
    GET_ALL_MENUS_FAIL,
    GET_MENUS_SUCCESS,
    GET_MENUS_FAIL,
    GET_MENU_SUCCESS,
    GET_MENU_FAIL,
    UPDATE_MENU_SUCCESS,
    UPDATE_MENU_FAIL,
    DELETE_MENU_SUCCESS,
    DELETE_MENU_FAIL,
    LIST_ALL_PAGES_SUCCESS,
    LIST_ALL_PAGES_FAIL,
    LIST_PAGES_BY_MENUS_SUCCESS,
    LIST_PAGES_BY_MENUS_FAIL,
    ASSIGN_MENUS_SUCCESS,
    ASSIGN_MENUS_FAIL,
    GET_MENUS_BY_GROUP_SUCCESS,
    GET_MENUS_BY_GROUP_FAIL,
    GET_MENUS_LIST_AND_PAGE_SUCCESS,
    GET_MENUS_LIST_AND_PAGE_FAIL
} from '../actions/menu';

/* GET ALL MENUS */
function getAllMenusReducer(state = {}, action) {
    switch (action.type) {
        case GET_ALL_MENUS_SUCCESS:
            return action.response;
        case GET_ALL_MENUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* GET MENUS */
function getMenusReducer(state = {}, action){
    switch (action.type) {
        case GET_MENUS_SUCCESS:
            return action.response.response;
        case GET_MENUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* GET MENU DETAIL */
function getMenuDetailReducer(state = {}, action){
    switch (action.type) {
        case GET_MENU_SUCCESS:
            return action.response;
        case GET_MENU_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* UPDATE MENU */
function updateMenuReducer(state = {}, action){
    switch (action.type) {
        case UPDATE_MENU_SUCCESS:
            return action.response.response;
        case UPDATE_MENU_FAIL:
            return action.error.response.response;
        default:
            return state;
    }
}

/* DELETE MENU */
function deleteMenuReducer(state = {}, action){
    switch (action.type) {
        case DELETE_MENU_SUCCESS:
            return action.response.data;
        case DELETE_MENU_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST ALL PAGES */
function listAllPagesReducer(state = {}, action){
    switch (action.type) {
        case LIST_ALL_PAGES_SUCCESS:
            return action.response.data;
        case LIST_ALL_PAGES_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* LIST PAGES BY MENUS */
function listPagesByMenusReducer(state = {}, action){
    switch (action.type) {
        case LIST_PAGES_BY_MENUS_SUCCESS:
            return action.response.data;
        case LIST_PAGES_BY_MENUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* ASSIGN MENUS */
function assignMenusReducer(state = {}, action){
    switch (action.type) {
        case ASSIGN_MENUS_SUCCESS:
            return action.response;
        case ASSIGN_MENUS_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* ASSIGN MENUS */
function getMenusByGroupReducer(state = {}, action){
    switch (action.type) {
        case GET_MENUS_BY_GROUP_SUCCESS:
            return action.response;
        case GET_MENUS_BY_GROUP_FAIL:
            return action.error.response;
        default:
            return state;
    }
}

/* GET MENUS LIST AND PAGE */
function getMenusListAndPageReducer(state = {}, action){
    switch (action.type) {
        case GET_MENUS_LIST_AND_PAGE_SUCCESS:
            return action.response;
        case GET_MENUS_LIST_AND_PAGE_FAIL:
            return action.error.response;
        default:
            return state;
    }
}


export const menuReducer = combineReducers({
    getAllMenus: getAllMenusReducer,
    getMenus: getMenusReducer,
    getMenuDetail: getMenuDetailReducer,
    updateMenu: updateMenuReducer,
    deleteMenu: deleteMenuReducer,
    listAllPages: listAllPagesReducer,
    listPagesByMenus: listPagesByMenusReducer,
    assignMenus: assignMenusReducer,
    getMenusByGroup: getMenusByGroupReducer,
    getMenusListAndPage: getMenusListAndPageReducer
});
