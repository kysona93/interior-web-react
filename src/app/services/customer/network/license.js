import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function addRentPoleApi(action){
    return axios.post(`${API_ENDPOINT}rentPole`, JSON.stringify(action.rentPole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function updateRentPoleApi(action){
    return axios.put(`${API_ENDPOINT}rentPole`, JSON.stringify(action.rentPole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function deleteRentPoleApi(action){
    return axios.delete(`${API_ENDPOINT}rentPole`, JSON.stringify(action.rentPole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getRentPoleApi(action){
    return axios.get(`${API_ENDPOINT}rentPole/part/`+ action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getAllRentPoleApi(action){
    return axios.get(`${API_ENDPOINT}rentPole`, JSON.stringify(action.rentPole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

/******************************* rentPart ***************************/
export function addRentPartApi(action){
    return axios.post(`${API_ENDPOINT}rentPart`, JSON.stringify(action.rentPart), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function updateRentPartApi(action){
    return axios.put(`${API_ENDPOINT}rentPart`, JSON.stringify(action.rentPart), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function deleteRentPartApi(action){
    return axios.delete(`${API_ENDPOINT}rentPart/`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getRentPartApi(action){
    return axios.get(`${API_ENDPOINT}rentPart?licenseId=${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getAllRentPartApi(action){
    return axios.get(`${API_ENDPOINT}poles/area?id=`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
