import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';


export function getInstallingApi() {
    return axios.get(`${API_ENDPOINT}installing?id=`+1, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}
/* view all items installing */
export function getAllInstallingApi(action) {
    return axios.get(`${API_ENDPOINT}installing/`+action.id, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}
export function getScheduleInstallingApi() {
    return axios.get(`${API_ENDPOINT}installing`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}
export function updateInstallingApi(action) {
    console.log("API WORK : " + JSON.stringify(action.installing));
    return axios.put(`${API_ENDPOINT}installing`, JSON.stringify(action.installing), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function deleteInstallingApi(action) {
    return axios.delete(`${API_ENDPOINT}installing/`+ action.id, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function addInstallingApi(action) {
    console.log("Api : " + JSON.stringify(action.installing));
    return axios.post(`${API_ENDPOINT}installing`, JSON.stringify(action.installing), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}
export function addGenerateFormRequestApi(action) {
    console.log("Api : " + JSON.stringify(action.generate));
    return axios.post(`${API_ENDPOINT}formRequest`,JSON.stringify(action.generate), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}
export function getAllGenerateFormRequestApi() {
    return axios.get(`${API_ENDPOINT}formRequest`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

