import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function updateChangeMeterApi(action){
    return axios.post(`${API_ENDPOINT}meters/change`, JSON.stringify(action.changeMeter), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function addItemMvApi(action){
    return axios.post(`${API_ENDPOINT}mv`, action.mv, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllItemsMvApi(action){
    return axios.get(`${API_ENDPOINT}mv/all?name=${action.license.name}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getItemsMvApi(action){
    return axios.get(`${API_ENDPOINT}mv/?id=`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getItemsMvSummaryApi(action){
        return axios.get(`${API_ENDPOINT}mv/summarykwh?licenseId=${action.summary.licenseId}&startDate=${action.summary.startDate}&endDate=${action.summary.endDate}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getConnectionPointApi(action){
         return axios.get(`${API_ENDPOINT}mv/connectionpoints?licenseId=${action.connection.licenseId}&startDate=${action.connection.startDate}&endDate=${action.connection.endDate}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function deleteItemsMvApi(action){
    console.log("API",JSON.stringify(action));
    return axios.delete(`${API_ENDPOINT}mv/`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function updateItemMvApi(action){
    return axios.put(`${API_ENDPOINT}mv`, action.mv, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function updateTransformerApi(action){
    return axios.post(`${API_ENDPOINT}meters/transfer`, JSON.stringify(action.transfer), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
export function getMeterHistoryApi(action){
    console.log("api:",action.id);
    return axios.get(`${API_ENDPOINT}meters/cus_id?id=`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getFreeMetersApi(){
    return axios.get(`${API_ENDPOINT}meters?check=free`, AUTH_CONFIG)
        .then(response => ({ response: response.data}))
        .catch(error => ({ error }));

}
export function getUsedMetersApi(action){
    return axios.get(`${API_ENDPOINT}meters/customer/`+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data}))
        .catch(error => ({ error }));
}