import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../../header';

export function getAllPaymentTypeApi(){
    return axios.get(API_ENDPOINT+"payment/types", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function addPaymentTypeApi(action) {
    return axios.post(`${API_ENDPOINT}payment/types`,
        JSON.stringify(action.payment), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updatePaymentTypeApi(action){
    return axios.put(`${API_ENDPOINT}payment/types`,
        JSON.stringify(action.payment), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function deletePaymentTypeApi(action){
    return axios.delete(`${API_ENDPOINT}payment/types/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
