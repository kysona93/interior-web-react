import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../../header';

export function getAllLocationPayApi(){
    return axios.get(API_ENDPOINT+"payment/locations", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function addLocationPayApi(action) {
    return axios.post(`${API_ENDPOINT}payment/locations`,
        JSON.stringify(action.location), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updatePLocationPayApi(action){
    return axios.put(`${API_ENDPOINT}payment/locations`,
        JSON.stringify(action.location), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function deleteLocationPayApi(action){
    return axios.delete(`${API_ENDPOINT}payment/locations/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
