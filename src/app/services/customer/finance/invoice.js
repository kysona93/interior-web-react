import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function getAllCustomerInvoicesApi(action){
    return axios.get(API_ENDPOINT+"customer/invoices"
                    , AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function deleteCustomerInvoiceApi(action){
    return axios.delete(API_ENDPOINT+"customer/invoices/"+action.id, AUTH_CONFIG)
                         .then(response => ({ response }))
                         .catch(error => ({ error }));
}

export function getAllCustomerEDCInvoicesApi(action){
    return axios.get(API_ENDPOINT+"customer/invoices/edc"
                    , AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function getAllFinancialTransactionApi(action){
    return axios.get(API_ENDPOINT+"customer/invoices/financial"
                    , AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function updateFinancialTransactionApi(action){
    return axios.put(API_ENDPOINT+"customer/invoices/financial",
                    AUTH_CONFIG,
                    JSON.stringify(action.financial))
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}
