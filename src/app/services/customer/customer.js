import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export const addCustomerApi = (action) => {
    return axios.post(`${API_ENDPOINT}customers`, JSON.stringify(action.customer), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const updateCustomerApi = (action) => {
    return axios.put(`${API_ENDPOINT}customers`, JSON.stringify(action.customer), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const deleteCustomerApi = (action) => {
    return axios.delete(`${API_ENDPOINT}customers/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getCustomerApi = (action) => {
    return axios.get(`${API_ENDPOINT}customers/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getCustomersApi = (action) => {
    return axios.get(`${API_ENDPOINT}customers?type=${action.customer.type}&option=${action.customer.option}&search=${action.customer.search}&pageSize=${action.customer.limit}&nowPage=${action.customer.page}&orderBy=${action.customer.orderBy}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getAllCustomersApi = () => {
    return axios.get(`${API_ENDPOINT}customers`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};
