import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addRatePerKhwApi(action){
    return axios.post(API_ENDPOINT+"rate_per_kwh", JSON.stringify(action.rate), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllRatePerKhwApi(){
    console.log("Api work  ===> ");
    return axios.get(API_ENDPOINT+"rate_per_kwh", AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateRatePerKhwApi(action){
    return axios.put(API_ENDPOINT+"rate_per_kwh",JSON.stringify(action.rate), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteRatePerKhwApi(action){
    return axios.delete(API_ENDPOINT+"rate_per_kwh/"+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}