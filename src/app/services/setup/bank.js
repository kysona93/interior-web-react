import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addBankApi(action){
    return axios.post(API_ENDPOINT+"banks", JSON.stringify(action.bank), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllBanksApi(){
    return axios.get(API_ENDPOINT+"banks", AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateBankApi(action){
    return axios.put(API_ENDPOINT+"banks", JSON.stringify(action.bank), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteBankApi(action){
    return axios.delete(API_ENDPOINT+"banks/"+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}