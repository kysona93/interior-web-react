import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addTransformerApi(action){
    return axios.post(`${API_ENDPOINT}transformers`, JSON.stringify(action.transformer), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateTransformerApi(action) {
    return axios.put(`${API_ENDPOINT}transformers`, JSON.stringify(action.transformer), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteTransformerApi(action) {
    return axios.delete(`${API_ENDPOINT}transformers/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllTransformersApi(action){
    return axios.get(`${API_ENDPOINT}transformers?check=${action.transformer.check}&serial=${action.transformer.serial}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllTransformerForKwhMonthlyApi(action){
    return axios.get(`${API_ENDPOINT}transformers/kwh/monthly`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}