import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';
const ENDPOINT = `${API_ENDPOINT}areas`;

export function addAreaApi(action){
    return axios.post(ENDPOINT, JSON.stringify(action.area), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateAreaApi(action){
    return axios.put(ENDPOINT, JSON.stringify(action.area), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteAreaApi(action){
    return axios.delete(`${ENDPOINT}/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllAreasApi(){
    return axios.get(`${ENDPOINT}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAreaReportsApi(){
    return axios.get(`${ENDPOINT}/report`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAreasApi(){
    return axios.get(`${ENDPOINT}/all`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}