import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addRateRangeApi(action){
    return axios.post(API_ENDPOINT+"rateranges",
        JSON.stringify(action.rate),
        AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllRateRangesApi(action){
    return axios.get(API_ENDPOINT+"rateranges", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updateRateRangeApi(action){
    return axios.put(API_ENDPOINT+"rateranges",
        JSON.stringify(action.rate),
        AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function deleteRateRangeApi(action){
    return axios.delete(API_ENDPOINT+"rateranges/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}