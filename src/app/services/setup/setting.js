import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

/* ADD SETTING */
export function addSettingApi(action){
    return axios.post(API_ENDPOINT+"settings",
                    JSON.stringify(action.setting),
                    AUTH_CONFIG)
                    .then(response => ({ response: response.data }))
                    .catch(error => ({ error }));
}

/* LIST SETTING */
export function listSettingApi(){
    return axios.get(API_ENDPOINT+"settings", AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

/* UPDATE SETTING */
export function updateSettingApi(action){
    return axios.put(API_ENDPOINT+"settings",
                JSON.stringify(action.setting),
                AUTH_CONFIG)
                .then(response => ({ response: response.data }))
                .catch(error => ({ error }));
}

/* DELETE SETTING */
export function deleteSettingApi(action){
    return axios.delete(API_ENDPOINT+"settings/"+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}