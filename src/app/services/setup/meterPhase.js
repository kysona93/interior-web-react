import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

/* ADD INSTALLER */
export function addMeterPhaseApi(action){
    return axios.post(API_ENDPOINT+"meter/phase",
        JSON.stringify(action.phase),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* LIST ALL INSTALLERS */
export function listAllMeterPhasesApi(action){
    return axios.get(API_ENDPOINT+"meter/phase",
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* UPDATE INSTALLER */
export function updateMeterPhaseApi(action){
    return axios.put(API_ENDPOINT+"meter/phase",
        JSON.stringify(action.phase),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* DELETE INSTALLER */
export function deleteMeterPhaseApi(action){
    return axios.delete(API_ENDPOINT+"meter/phase/"+action.id,
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}