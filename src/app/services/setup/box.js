import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addBoxApi(action){
    return axios.post(`${API_ENDPOINT}boxes`, JSON.stringify(action.box), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateBoxApi(action) {
    return axios.put(`${API_ENDPOINT}boxes`, JSON.stringify(action.box), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteBoxApi(action) {
    return axios.delete(`${API_ENDPOINT}boxes/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getBoxesApi(action){
    return axios.get(`${API_ENDPOINT}boxes?serial=${action.box.serial}&id=${action.box.id}&pageSize=${action.box.limit}&nowPage=${action.box.page}&orderBy=${action.box.orderBy}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllBoxesApi(){
    return axios.get(`${API_ENDPOINT}boxes`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
