import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addBreakerApi(action){
    return axios.post(`${API_ENDPOINT}breakers`, JSON.stringify(action.breaker), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateBreakerApi(action) {
    return axios.put(`${API_ENDPOINT}breakers`, JSON.stringify(action.breaker), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteBreakerApi(action) {
    return axios.delete(`${API_ENDPOINT}breakers/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getBreakersApi(action){
    return axios.get(`${API_ENDPOINT}breakers/filter?serial=${action.breaker.serial}&typeId=${action.breaker.typeId}&pageSize=${action.breaker.limit}&nowPage=${action.breaker.page}&orderBy=${action.breaker.orderBy}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllBreakersApi(){
    return axios.get(`${API_ENDPOINT}breakers?check=free`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
