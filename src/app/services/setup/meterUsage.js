import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addMeterUsageApi(action){
    return axios.post(API_ENDPOINT+"meter/usages",
        JSON.stringify(action.usage),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

export function listAllMeterUsagesApi(action){
    return axios.get(API_ENDPOINT+"meter/usages",
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

export function updateMeterUsageApi(action){
    return axios.put(API_ENDPOINT+"meter/usages",
        JSON.stringify(action.usage),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

export function deleteMeterUsageApi(action){
    return axios.delete(API_ENDPOINT+"meter/usages/"+action.id,
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}