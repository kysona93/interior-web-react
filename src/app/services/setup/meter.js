import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addMeterApi(action){
    return axios.post(`${API_ENDPOINT}meters`, JSON.stringify(action.meter), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getMetersApi(action){
    return axios.get(`${API_ENDPOINT}meters/filter?serial=${action.meter.serial}&typeId=${action.meter.typeId}&pageSize=${action.meter.limit}&nowPage=${action.meter.page}&orderBy=${action.meter.orderBy}&orderType=${action.meter.orderType}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateMeterApi(action){
    return axios.put(`${API_ENDPOINT}meters`, JSON.stringify(action.meter), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteMeterApi(action){
    return axios.delete(`${API_ENDPOINT}meters/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getMeterByMeterSerialApi(action){
    return axios.get(`${API_ENDPOINT}meters/meter-serial/${action.meter.serial}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function reuseMeterPostPaidApi(action){
    return axios.put(`${API_ENDPOINT}meters/reuse`, JSON.stringify(action.meter), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
