import axios from 'axios';
import { CONFIG, AUTH_CONFIG , API_ENDPOINT, clearState } from './../header';

/* ADD SALE TYPE */
export function addSaleTypeApi(action){
    return axios.post(API_ENDPOINT+"sale/types",
                    JSON.stringify(action.sale),
                    AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status === 404){
                return error.response;

            }else if(error.response.status === 500) {
                //window.location.assign('/server/error')
            }else if(error.status === 401) {
                clearState();
            }else{

            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* LIST ALL SALE TYPE */
export function getAllSaleTypeApi(){
    return axios.get(API_ENDPOINT+"sale/types",
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* UPDATE SALE TYPE */
export function updateSaleTypeApi(action){
    return axios.put(API_ENDPOINT+"sale/types",
                    JSON.stringify(action.sale),
                    AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* DELETE SALE TYPE */
export function deleteSaleTypeApi(action){
    return axios.delete(API_ENDPOINT+"sale/types/"+action.id,
                        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}