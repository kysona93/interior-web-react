import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';
const ENDPOINT = `${API_ENDPOINT}amperes`;

export function addAmpereApi(action){
    return axios.post(ENDPOINT, JSON.stringify(action.ampere), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateAmpereApi(action){
    return axios.put(ENDPOINT, JSON.stringify(action.ampere), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteAmpereApi(action){
    return axios.delete(`${ENDPOINT}/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllAmperesApi(action){
    return axios.get(`${ENDPOINT}?phaseId=${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}