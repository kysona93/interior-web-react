import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

/* LIST ALL EXCHANGE RATE */
export function listAllJobsApi(action){
    return axios.get(`${API_ENDPOINT}jobtitles`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}