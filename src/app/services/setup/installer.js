import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

/* ADD INSTALLER */
export function addInstallerApi(action){
    return axios.post(API_ENDPOINT+"installers",
                        JSON.stringify(action.installer),
                        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* LIST ALL INSTALLERS */
export function listAllInstallersApi(action){
    return axios.get(API_ENDPOINT+"installers",
                AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* UPDATE INSTALLER */
export function updateInstallersApi(action){
    return axios.put(API_ENDPOINT+"installers",
                    JSON.stringify(action.installer),
                    AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* DELETE INSTALLER */
export function deleteInstallerApi(action){
    return axios.delete(API_ENDPOINT+"installers/"+action.id,
                AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}