import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../header';

 export function addOccupationApi(action){
     return axios.post(API_ENDPOINT+"occupations", JSON.stringify(action.occupation), AUTH_CONFIG)
         .then(response => ({response}))
         .catch(error => ({error}));
 }

 export function getAllOccupationsApi(){
     return axios.get(API_ENDPOINT+"occupations", AUTH_CONFIG)
         .then(response => ({response: response.data}))
         .catch(error => ({error}));
 }

 export function updateOccupationApi(action){
     return axios.put(API_ENDPOINT+"occupations", JSON.stringify(action.occupation), AUTH_CONFIG)
         .then(response => ({response}))
         .catch(error => ({error}));
 }

 export function deleteOccupationApi(action){
     return axios.delete(API_ENDPOINT+"occupations/"+action.id, AUTH_CONFIG)
         .then(response => ({response}))
         .catch(error => ({error}));
 }