import axios from 'axios';
import {AUTH_CONFIG,CONFIG, API_ENDPOINT} from './../header';

/* ADD EXCHANGE RATE */
export function addExchangeRateApi(action){
    return axios.post(API_ENDPOINT+"exchange_rate", JSON.stringify(action.exchange.data.exchangeRate), AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* LIST ALL EXCHANGE RATE */
export function listAllExchangeRateApi(action){
    return axios.get(API_ENDPOINT+"exchange_rate", AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* GET EXCHANGE RATE */
export function getExchangeRateApi(action){
    return axios.get(API_ENDPOINT+"exchange_rate", AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* UPDATE EXCHANGE RATE */
export function updateExchangeRateApi(action){
    return axios.put(API_ENDPOINT+"exchange_rate",
        JSON.stringify(action.exchange.data.exchangeRate), AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}

/* DELETE EXCHANGE RATE */
export function deleteExchangeRateApi(action){
    return axios.delete(API_ENDPOINT+"exchange_rate/"+ action.id,  AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
}