import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addPoleApi(action){
    return axios.post(`${API_ENDPOINT}poles`, JSON.stringify(action.pole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updatePoleApi(action) {
    return axios.put(`${API_ENDPOINT}poles`, JSON.stringify(action.pole), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deletePoleApi(action) {
    return axios.delete(`${API_ENDPOINT}poles/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getPolesApi(action){
    return axios.get(`${API_ENDPOINT}poles/filter?area=${action.pole.area}&serial=${action.pole.serial}&pageSize=${action.pole.limit}&nowPage=${action.pole.page}&orderBy=${action.pole.orderBy}&orderType=${action.pole.orderType}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllPolesApi(){
    return axios.get(`${API_ENDPOINT}poles`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
