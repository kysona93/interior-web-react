import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function addCustomerTypeApi(action){
    return axios.post(`${API_ENDPOINT}customer/types`, action.customer, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function getAllCustomerTypesApi(){
    return axios.get(`${API_ENDPOINT}customer/types`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function updateCustomerTypeApi(action) {
    return axios.put(`${API_ENDPOINT}customer/types`, JSON.stringify(action.customer), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function deleteCustomerTypeApi(action) {
    return axios.delete(`${API_ENDPOINT}customer/types/${action.id}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}