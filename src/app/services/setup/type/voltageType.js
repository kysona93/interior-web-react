import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT} from './../../header';
const ENDPOINT = `${API_ENDPOINT}voltage/types`;

export function addVoltageApi(action){
    return axios.post(ENDPOINT, JSON.stringify(action.voltage), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllVoltagesApi(){
    return axios.get(ENDPOINT, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateVoltageApi(action){
    return axios.put(ENDPOINT, JSON.stringify(action.voltage), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteVoltageApi(action){
    return axios.delete(`${ENDPOINT}/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}