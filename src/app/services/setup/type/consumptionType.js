import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function addConsumptionTypeApi(action){

    return axios.post(`${API_ENDPOINT}consumptions`,
        JSON.stringify(action.consumption), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function updateConsumptionTypeApi(action){
    return axios.put(`${API_ENDPOINT}consumptions`,
        JSON.stringify(action.consumption), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function deleteConsumptionTypeApi(action){
    return axios.delete(`${API_ENDPOINT}consumptions/${action.id}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function getAllConsumptionTypesApi(){
    return axios.get(`${API_ENDPOINT}consumptions`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}