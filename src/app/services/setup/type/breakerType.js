import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT} from './../../header';

export const addBreakerTypeApi = (action) => {
    return axios.post(API_ENDPOINT+"breaker/types", JSON.stringify(action.breaker), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};


export const getAllBreakerTypesApi = () => {
    return axios.get(API_ENDPOINT + "breaker/types", AUTH_CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            if (error.response.status) {
                if (error.response.status == 404) {
                    return error.response;
                    //clearState();
                } else if (error.response.status == 500) {
                    //window.location.assign('/server/error')
                } else {
                    //window.location.assign('/server/down')
                }
            } else {
                //window.location.assign('/server/down')
            }
        });
};

export const updateBreakerTypeApi = (action) => {
    return axios.put(API_ENDPOINT+"breaker/types",
        JSON.stringify(action.breaker),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const deleteBreakerTypeApi = (action) => {
    return axios.delete(API_ENDPOINT+"breaker/types/"+action.id,
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};