import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../../header';

export const getAllCustomerGroupsApi = (action) => {
    return axios.get(`${API_ENDPOINT}customer/sale/types?typeId=${action.id}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
};

export const addCustomerGroupApi = (action) => {
    return axios.post(`${API_ENDPOINT}customer/sale/types`, JSON.stringify(action.group), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
};

export const updateCustomerGroupApi = (action) => {
    return axios.put(`${API_ENDPOINT}customer/sale/types`, JSON.stringify(action.group), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
};

export const deleteCustomerGroupApi = (action) => {
    return axios.delete(`${API_ENDPOINT}customer/sale/types/${action.id}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
};