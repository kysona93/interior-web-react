import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../../header';

export function addTransformerTypeApi(action) {
    return axios.post(API_ENDPOINT + "transformer/types",
        JSON.stringify(action.transformer),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function updateTransformerTypeApi(action) {
    return axios.put(API_ENDPOINT + "transformer/types",
        JSON.stringify(action.transformer),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function deleteTransformerTypeApi(action) {
    return axios.delete(API_ENDPOINT + "transformer/types/" + action.id, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllTransformerTypesApi() {
    return axios.get(API_ENDPOINT + "transformer/types", AUTH_CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            if (error.response.status) {
                if (error.response.status == 404) {
                    return error.response;
                    //clearState();
                } else if (error.response.status == 500) {
                    //window.location.assign('/server/error')
                } else {
                    //window.location.assign('/server/down')
                }
            } else {
                //window.location.assign('/server/down')
            }
        });
}
