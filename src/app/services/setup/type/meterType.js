import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT} from './../../header';

export const addMeterTypeApi = (action) => {
    return axios.post(API_ENDPOINT+"meter/types",
        JSON.stringify(action.meter),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
};

export const updateMeterTypeApi = (action) => {
    return axios.put(API_ENDPOINT+"meter/types",
        JSON.stringify(action.meter),
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
};

export const deleteMeterTypeApi = (action) => {
    return axios.delete(API_ENDPOINT+"meter/types/"+action.id,
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 401){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
};

export const getAllMeterTypesApi = () => {
    return axios.get(API_ENDPOINT+"meter/types",
        AUTH_CONFIG
    ).then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
};