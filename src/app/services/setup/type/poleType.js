import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT} from './../../header';

export const addPoleTypeApi = (action) => {
    return axios.post(API_ENDPOINT+"pole/types", JSON.stringify(action.pole), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const updatePoleTypeApi = (action) => {
    return axios.put(API_ENDPOINT+"pole/types", JSON.stringify(action.pole), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const deletePoleTypeApi = (action) => {
    return axios.delete(API_ENDPOINT+"pole/types/"+action.id, AUTH_CONFIG )
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const getAllPoleTypesApi = () => {
    return axios.get(API_ENDPOINT+"pole/types", AUTH_CONFIG)
        .then(function (response) {
        return response.data;
    }).catch(function(error){
        if(error.response.status){
            if(error.response.status == 404){
                return error.response;
                //clearState();
            }else if(error.response.status == 500) {
                //window.location.assign('/server/error')
            }else {
                //window.location.assign('/server/down')
            }
        }else {
            //window.location.assign('/server/down')
        }
    });
};