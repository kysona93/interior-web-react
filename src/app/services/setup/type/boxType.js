import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../../header';

export const addBoxTypeApi = (action) => {
    return axios.post(API_ENDPOINT+"box/types", JSON.stringify(action.box), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const updateBoxTypeApi = (action) => {
    return axios.put(API_ENDPOINT+"box/types",
                    JSON.stringify(action.box),
                    AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const deleteBoxTypeApi = (action) => {
    return axios.delete(API_ENDPOINT+"box/types/"+action.id,
                        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
};

export const getAllBoxTypesApi = () => {
    return axios.get(API_ENDPOINT+"box/types", AUTH_CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            if (error.response.status) {
                if (error.response.status == 404) {
                    return error.response;
                    //clearState();
                } else if (error.response.status == 500) {
                    //window.location.assign('/server/error')
                } else {
                    //window.location.assign('/server/down')
                }
            } else {
                //window.location.assign('/server/down')
            }
        });
};
