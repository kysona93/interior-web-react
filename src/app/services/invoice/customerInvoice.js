import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addCustomerInvoiceApi(action){
    return axios.post(API_ENDPOINT+"invoices",
        JSON.stringify(action.invoice), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllCustomerInvoicesApi(action){
    return axios.get(API_ENDPOINT+"invoices/" +
            "customer/" + action.customerId
        , AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
export function getAllLicenseInvoicesApi(action){
    return axios.get(API_ENDPOINT+"invoices/" +
            "license/" + action.license.licenseId +
            "?pageSize=" + action.license.limit +
            "&nowPage=" + action.license.page +
            "&orderBy=" + action.license.orderBy +
            "&orderType=" + action.license.orderType
        , AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function deleteCustomerInvoiceApi(action){
    return axios.delete(API_ENDPOINT+"invoices/status/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function voidCustomerInvoiceApi(action){
    return axios.delete(API_ENDPOINT+"invoices/void/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllCustomerInvoiceNoApi(action){
    return axios.get(API_ENDPOINT+"invoices/ids/customer/" + action.customerId
        , AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllInvoiceDetailsByInvoiceIdApi(action){
    return axios.get(API_ENDPOINT+"invoice/details/invoice/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getInvoiceInformationByInvoiceIdApi(action){
    return axios.get(API_ENDPOINT+"invoices/invoiceId?invoiceId="+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getHistoryInvoice12monthByCustomerIdApi(action){
    return axios.get(API_ENDPOINT+"invoices/history?" +
                    "customerId=" + action.customer.customerId +
                    "&invoiceDate="+action.customer.issueDate
                    , AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function getAllInvoiceDetailItemsByInvoiceIdApi(action){
    return axios.get(API_ENDPOINT+"invoice/details/item/invoice/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
export function getInvoiceDetailRentPoleApi(action){
    return axios.get(API_ENDPOINT+"/invoice/details/rentPole/invoice/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}