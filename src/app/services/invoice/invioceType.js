import axios from 'axios';
import {AUTH_CONFIG, CONFIG, API_ENDPOINT} from './../header';

export function getAllInvoiceTypeApi(){
    return axios.get(API_ENDPOINT+"invoice/types", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function addInvoiceTypeApi(action) {
    return axios.post(`${API_ENDPOINT}invoice/types`, JSON.stringify(action.invoiceType), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updateInvoiceTypeApi(action){
    return axios.put(`${API_ENDPOINT}invoice/types`, JSON.stringify(action.invoiceType), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function deleteInvoiceTypeApi(action){
    return axios.delete(`${API_ENDPOINT}invoice/types/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
