import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addGroupInvoiceApi(action){
    return axios.post(API_ENDPOINT+"group/invoices", JSON.stringify(action.group), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllGroupInvoiceApi(){
    return axios.get(API_ENDPOINT+"group/invoices", AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateGroupInvoiceApi(action){
    return axios.put(API_ENDPOINT+"group/invoices", JSON.stringify(action.group), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({error}));
}

export function deleteGroupInvoiceApi(action){
    return axios.delete(API_ENDPOINT+"group/invoices/"+action.id, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}