import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addInvoiceApi(action){
    return axios.post(API_ENDPOINT+"invoices",
        JSON.stringify(action.invoice), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllInvoicesApi(action){
    return axios.get(API_ENDPOINT+"invoices", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updateInvoiceApi(action){
    return axios.put(API_ENDPOINT+"invoices",
        JSON.stringify(action.invoice), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({error}));
}

export function deleteInvoiceApi(action){
    return axios.delete(API_ENDPOINT+"invoices/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function voidInvoiceApi(action){
    return axios.put(API_ENDPOINT+"invoices/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}