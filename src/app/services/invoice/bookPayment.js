import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addCustomerBookPaymentApi(action){
    return axios.post(API_ENDPOINT+"payments/invoice",
                JSON.stringify(action.payment),
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}
