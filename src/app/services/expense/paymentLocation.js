import axios from 'axios';
import {AUTH_CONFIG, CONFIG, API_ENDPOINT} from './header';

export function getAllPaymentLocationApi(){
    return axios.get(`${API_ENDPOINT}payment/location`, CONFIG)
        .then(function (response) {
        return response.data;
        }).catch(function(error){
            console.log(error.message)
        });
}

export function addPaymentLocationApi(action) {
    console.log("API : " + JSON.stringify(action));
    return axios.post(`${API_ENDPOINT}payment/location`, JSON.stringify(action.payment), CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}

export function updatePaymentLocationApi(action){
    return axios.put(`${API_ENDPOINT}payment/location`, JSON.stringify(action.payment), CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}

export function deletePaymentLocationApi(action){
    console.log(JSON.stringify(action));
    return axios.delete(`${API_ENDPOINT}payment/location/${action.payment.id}`, CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}
