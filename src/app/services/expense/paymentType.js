import axios from 'axios';
import {AUTH_CONFIG, CONFIG, API_ENDPOINT} from './header';

export function getAllPaymentTypeApi(){
    return axios.get(`${API_ENDPOINT}payment/types`, CONFIG)
        .then(function (response) {
        return response.data;
        }).catch(function(error){
            console.log(error.message)
        });
}

export function addPaymentTypeApi(action) {
    console.log("API : " + JSON.stringify(action));
    return axios.post(`${API_ENDPOINT}payment/types`, JSON.stringify(action.payment), CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}

export function updatePaymentTypeApi(action){
    return axios.put(`${API_ENDPOINT}payment/types`, JSON.stringify(action.payment), CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}

export function deletePaymentTypeApi(action){
    console.log(JSON.stringify(action));
    return axios.delete(`${API_ENDPOINT}payment/types/${action.payment.id}`, CONFIG)
        .then(function (response) {
            return response.data;
        }).catch(function (error) {
            console.log(error)
        });
}
