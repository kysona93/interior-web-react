import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './header';

export function getAllLocationsApi(){
    return axios.get(`${API_ENDPOINT}locations?type=0&province=0&district=0&commune=0`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function addLocationApi(action) {
    return axios.post(`${API_ENDPOINT}locations`, JSON.stringify(action.location), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateLocationApi(action){
    return axios.put(`${API_ENDPOINT}locations`, JSON.stringify(action.location), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function deleteLocationApi(action){
    return axios.delete(`${API_ENDPOINT}locations/${action.location.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getProvincesApi(){
    return axios.get(`${API_ENDPOINT}locations?type=2&province=0&district=0&commune=0`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getDistrictsApi(action){
    return axios.get(`${API_ENDPOINT}locations?type=3&province=${action.location}&district=0&commune=0`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getCommunesApi(action){
    return axios.get(`${API_ENDPOINT}locations?type=4&province=0&district=${action.location}&commune=0`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getVillagesApi(action){
    return axios.get(`${API_ENDPOINT}locations?type=5&province=0&district=0&commune=${action.location}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
