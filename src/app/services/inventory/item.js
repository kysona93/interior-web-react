import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addItemApi(action){
    return axios.post(`${API_ENDPOINT}items`, JSON.stringify(action.item), AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function getItemsApi(action){
    return axios.get(`${API_ENDPOINT}items/list?groupItem=${action.item.group}&status=${action.item.status}&keyWords=${action.item.keyWords}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function getAllItemsApi(action){
    return axios.get(`${API_ENDPOINT}items?groupItem=${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

export function updateItemApi(action){
    return axios.put(`${API_ENDPOINT}items`, JSON.stringify(action.item), AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function deleteItemApi(action){
    return axios.delete(`${API_ENDPOINT}items/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}