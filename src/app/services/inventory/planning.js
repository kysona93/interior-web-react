import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addPlanningApi(action){
    return axios.post(`${API_ENDPOINT}plannings`,
        JSON.stringify(action.planning), AUTH_CONFIG)
            .then(response => ({ response }))
            .catch(error => ({ error }));
}

export function getPlanningsApi(action){
    return axios.get(`${API_ENDPOINT}plannings?`+
                    `year=${action.planning.year}&`+
                    `month=${action.planning.month}&`+
                    `type=${action.planning.type}`
                    , AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function getPlanningNoApi(action){
    return axios.get(`${API_ENDPOINT}plannings/last/planning-no`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function updatePlanningApi(action){
    return axios.put(`${API_ENDPOINT}plannings`,
                    JSON.stringify(action.planning), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({error}));
}

export function deletePlanningApi(action){
    return axios.delete(`${API_ENDPOINT}plannings/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getPlanningsApprovedApi(action){
    return axios.get(`${API_ENDPOINT}plannings/approved`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}