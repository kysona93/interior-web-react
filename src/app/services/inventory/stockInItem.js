import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addStockInItemApi(action){
    return axios.post(API_ENDPOINT+"stocks/in",
        JSON.stringify(action.stockIn), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllStockInItemsApi(action){
    console.log("API : ", JSON.stringify(action.stockIn))
    return axios.get(API_ENDPOINT+"stocks/in?"
                    + "pageSize=" + action.stockIn.limit
                    + "&nowPage=" + action.stockIn.page
                    , AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function updateStockInItemApi(action){
    return axios.put(API_ENDPOINT+"stocks/in",
        JSON.stringify(action.stockIn), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({error}));
}

export function deleteStockInItemApi(action){
    return axios.delete(API_ENDPOINT+"stocks/in/"+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}