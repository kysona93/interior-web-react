import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addRequestApi(action){
    return axios.post(`${API_ENDPOINT}requests`,
        JSON.stringify(action.request), AUTH_CONFIG)
            .then(response => ({ response }))
            .catch(error => ({ error }));
}

export function getRequestsApi(action){
    return axios.get(`${API_ENDPOINT}requests?year=${action.request.year}&month=${action.request.month}&type=${action.request.type}&status=${action.request.status}`, AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function getRequestNoApi(action){
    return axios.get(`${API_ENDPOINT}requests/last/request-no`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function updateRequestApi(action){
    return axios.put(`${API_ENDPOINT}requests`,
                    JSON.stringify(action.request), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({error}));
}

export function deleteRequestApi(action){
    return axios.delete(`${API_ENDPOINT}requests/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getRequestsApprovedApi(action){
    return axios.get(`${API_ENDPOINT}requests/approved`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}