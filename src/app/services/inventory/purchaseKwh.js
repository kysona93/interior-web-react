import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

export function addConnectionBuyApi(action){
    return axios.post(`${API_ENDPOINT}inventory/purchase-kwh/connection-buy`,
        JSON.stringify(action.purchaseKwh), AUTH_CONFIG)
            .then(response => ({ response }))
            .catch(error => ({ error }));
}

export function getConnectionBuysApi(action){
    return axios.get(`${API_ENDPOINT}inventory/purchase-kwh/connection-buy?`+
                    `pageSize=${action.purchaseKwh.limit}&`+
                    `nowPage=${action.purchaseKwh.page}&`+
                    `keyWords=${action.purchaseKwh.keyWords}`
                    , AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function updateConnectionBuyApi(action){
    return axios.put(`${API_ENDPOINT}inventory/purchase-kwh/connection-buy`,
                    JSON.stringify(action.purchaseKwh), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({error}));
}

export function deleteConnectionBuyApi(action){
    return axios.delete(`${API_ENDPOINT}inventory/purchase-kwh/connection-buy/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function addPurchaseKwhApi(action){
    return axios.post(`${API_ENDPOINT}inventory/purchase-kwh`,
        JSON.stringify(action.purchaseKwh), AUTH_CONFIG)
            .then(response => ({ response }))
            .catch(error => ({ error }));
}

export function getPurchaseKwhsApi(action){
    return axios.get(`${API_ENDPOINT}inventory/purchase-kwh?`+
                    `pageSize=${action.purchaseKwh.limit}&`+
                    `nowPage=${action.purchaseKwh.page}&`+
                    `year=${action.purchaseKwh.year}&`+
                    `month=${action.purchaseKwh.month}&`+
                    `keyWords=${action.purchaseKwh.keyWords}`
                    , AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}

export function updatePurchaseKwhApi(action){
    return axios.put(`${API_ENDPOINT}inventory/purchase-kwh`,
                    JSON.stringify(action.purchaseKwh), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({error}));
}

export function deletePurchaseKwhApi(action){
    return axios.delete(`${API_ENDPOINT}inventory/purchase-kwh/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getPurchaseKwhMonthlyReportsApi(action){
    return axios.get(`${API_ENDPOINT}inventory/purchase-kwh/monthly-report?`+
                    `year=${action.purchaseKwh.year}&`+
                    `month=${action.purchaseKwh.month}`
                    , AUTH_CONFIG)
        .then(response => ({ response: response }))
        .catch(error => ({ error }));
}