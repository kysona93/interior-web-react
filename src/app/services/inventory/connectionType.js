import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function getConnectionTypesApi(action){
    return axios.get(`${API_ENDPOINT}inventory/connection-type`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}
