import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT, clearState } from './../header';

export function addUnitTypeApi(action){
    return axios.post(API_ENDPOINT+"unit/types",
                JSON.stringify(action.unit),
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function listAllUnitTypesApi(){
    return axios.get(API_ENDPOINT+"unit/types", AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function updateUnitTypeApi(action){
    return axios.put(API_ENDPOINT+"unit/types",
                JSON.stringify(action.unit),
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function deleteUnitTypeApi(action){
    return axios.delete(API_ENDPOINT+"unit/types/"+action.id, AUTH_CONFIG)
                        .then(response => ({ response }))
                        .catch(error => ({ error }));
}