import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT, clearState } from './../header';

export function addItemGroupApi(action){
    return axios.post(API_ENDPOINT+"items/groups",
                    JSON.stringify(action.group),
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function listAllItemGroupsApi(action){
    return axios.get(API_ENDPOINT+"items/groups",
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function updateItemGroupApi(action){
    return axios.put(API_ENDPOINT+"items/groups",
                    JSON.stringify(action.group),
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function deleteItemGroupApi(action){
    return axios.delete(API_ENDPOINT+"items/groups/"+action.id,
                        AUTH_CONFIG)
                        .then(response => ({ response }))
                        .catch(error => ({ error }));
}

export function getItemGroupsListApi(action){
    return axios.get(API_ENDPOINT+"items/groups/list",
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}