import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function getAllSupplierApi(action){
    return axios.get(`${API_ENDPOINT}suppliers/list?pageSize=${action.supplier.limit}
                    &nowPage=${action.supplier.page}&keyWords=${action.supplier.keyword}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getSuppliersApi(action){
    return axios.get(`${API_ENDPOINT}suppliers?type=${action.supplier.type}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function addSupplierApi(action) {
    return axios.post(`${API_ENDPOINT}suppliers`, JSON.stringify(action.supplier), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function updateSupplierApi(action){
    return axios.put(`${API_ENDPOINT}suppliers`, JSON.stringify(action.supplier), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function deleteSupplierApi(action){
    return axios.delete(`${API_ENDPOINT}suppliers/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}
