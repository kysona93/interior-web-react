import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function getItemCategoryByParentApi(action){
    return axios.get(`${API_ENDPOINT}items/categories/parents/0`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getItemSubCategoryByParentApi(action){
    return axios.get(`${API_ENDPOINT}items/categories/parents/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}
