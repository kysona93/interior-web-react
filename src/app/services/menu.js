import axios from 'axios';
import {CONFIG, AUTH_CONFIG, API_ENDPOINT} from './header';

/* LIST ALL MENUS */
export function getAllMenusApi(action){
    return axios.get(`${API_ENDPOINT}menus`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}

/* LIST MENUS */
export function getMenusApi(action){
    return axios.get(`${API_ENDPOINT}menus/username`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* GET MENU DETAIL */
export function getMenuDetailApi(action){
    return axios.get(`${API_ENDPOINT}menus/`+action.id, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* UPDATE MENU */
export function updateMenuApi(action){
    return axios.put(`${API_ENDPOINT}menus`, JSON.stringify(action.menu), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* DELETE MENU */
export function apiDeleteMenu(action){
    return axios.delete(`${API_ENDPOINT}auth/menu/`+action.id, CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));

}

/* LIST ALL PAGES */
export function apiListAllPages(action){
    return axios.get(`${API_ENDPOINT}auth/pages`, CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* LIST PAGES BY MENUS */
export function apiListPagesByMenus(action){
    return axios.post(`${API_ENDPOINT}auth/menus/pages`, JSON.stringify(action.ids), CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}


/* ASSIGN MENU */
export function assignMenusApi(action){
    return axios.put(`${API_ENDPOINT}groups/assign.menus`, JSON.stringify(action.menu), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}


/* GET MENU BY GROUP */
export function getMenusByGroupApi(action){
    return axios.get(`${API_ENDPOINT}menus/group/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}

/* GET MENU LIST AND PAGE */
export function getMenusListAndPageApi(action){
    return axios.get(`${API_ENDPOINT}menus/list?pageSize=${action.menu.limit}
                    &nowPage=${action.menu.page}&keyWords=${action.menu.keyword}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}