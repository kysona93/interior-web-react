import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../../header';

// new service
export function getReportSubsidySaleMVLicenseApi(action){
    return axios.get(API_ENDPOINT+"report/ref/subsidy/sale/mv/license?" +
                "startDate=" + action.subsidy.startDate +
                "&endDate=" + action.subsidy.endDate,
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getReportSubsidySaleMVApi(action){
    return axios.get(API_ENDPOINT+"report/ref/subsidy/sale/mv?" +
                "startDate=" + action.subsidy.startDate +
                "&endDate=" + action.subsidy.endDate,
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getReportSubsidySaleLVApi(action) {
    return axios.get(API_ENDPOINT + "report/ref/subsidy/sale/lv?" +
        "startDate=" + action.subsidy.startDate +
        "&endDate=" + action.subsidy.endDate +
        "&consumptionId=" + action.subsidy.consumptionId +
        "&startUnit=" + action.subsidy.startUnit +
        "&endUnit=" + action.subsidy.endUnit +
        "&isUpper=" + action.subsidy.isUpper,
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}
