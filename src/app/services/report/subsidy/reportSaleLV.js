import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../../header';

/* report subsidy page 2 */
export function getReportSubsidySaleLVApi(action) {
    return axios.get(API_ENDPOINT + "report/ref/subsidy/sale/lv?" +
        "startDate=" + action.subsidy.startDate +
        "&endDate=" + action.subsidy.endDate +
        "&consumptionId=" + action.subsidy.consumptionId +
        "&startUnit=" + action.subsidy.startUnit +
        "&endUnit=" + action.subsidy.endUnit +
        "&isUpper=" + action.subsidy.isUpper,
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

/* report subsidy page 3 */
export function getSubsidyPrepaidAmountSponserApi(action){
    return axios.get(API_ENDPOINT+`returninvoices`, AUTH_CONFIG)
    .then(response =>({ response }))
    .catch(error => ({ error }));
}

export function getSubsidyPrepaidAmountApi(action){
    return axios.get(API_ENDPOINT+`returninvoices`, AUTH_CONFIG)
    .then(response =>({ response }))
    .catch(error => ({ error }));
}