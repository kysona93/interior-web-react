import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT, clearState } from './../header';

export function addGroupReportApi(action){
    return axios.post(API_ENDPOINT+"group/reports",
                    JSON.stringify(action.report),
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function listAllGroupReportsApi(action){
    return axios.get(API_ENDPOINT+"group/reports?status="+action.status,
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function updateGroupReportApi(action){
    return axios.put(API_ENDPOINT+"group/reports",
                    JSON.stringify(action.report),
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function deleteGroupReportApi(action){
    return axios.delete(API_ENDPOINT+"group/reports/"+action.id,
                        AUTH_CONFIG)
                        .then(response => ({ response }))
                        .catch(error => ({ error }));
}