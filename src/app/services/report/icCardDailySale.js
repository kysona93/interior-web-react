import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../header';

export function getICCardDailySalesByCusIdApi(action){
    return axios.get(`${API_ENDPOINT}prepaid/daily-sale/filter/customer?` 
                        + `month=${action.filter.month}`
                        + `&year=${action.filter.year}` 
                        + `&cusId=${action.filter.cusId}`,
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function getICCardDailySalesFilterApi(action){
    return axios.get(`${API_ENDPOINT}prepaid/daily-sale/filter?` 
                        + `start=${action.filter.start}`
                        + `&end=${action.filter.end}` 
                        + `&cusId=${action.filter.cusId}` 
                        + `&cusName=${action.filter.cusName}`
                        + `&meterNo=${action.filter.meterNo}`,
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function getPrepaidPowerReportsApi(action){
    return axios.get(`${API_ENDPOINT}prepaid/daily-sale/prepaid-power-reports?` 
                        + `sDate=${action.filter.sDate}`
                        + `&eDate=${action.filter.eDate}` 
                        + `&cusId=${action.filter.cusId}` 
                        + `&prepaidId=${action.filter.prepaidId}` 
                        + `&cusName=${action.filter.cusName}`
                        + `&meterNo=${action.filter.meterNo}`,
                    AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}