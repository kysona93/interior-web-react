import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../header';

export const getTransformerReportsApi = (action) => {
    return axios.get(`${API_ENDPOINT}/report/transformers?areaId=${action.transformer.areaId}&transId=${action.transformer.transId}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({ error }));
};

export const getPoleReportsApi = (action) => {
    return axios.get(`${API_ENDPOINT}/report/pole?areaId=${action.transformer.areaId}&transId=${action.transformer.transId}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({ error }));
};