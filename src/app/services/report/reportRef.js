import axios from 'axios';
import { AUTH_CONFIG , API_ENDPOINT } from './../header';

export function getReportRefPostpaidKwApi(action){
    return axios.get(API_ENDPOINT+"report/ref/residential/postpaid?" + // invoices/ref
        "startDate=" + action.customer.startDate +
        "&endDate=" + action.customer.endDate +
        "&startUnit=" + action.customer.startUnit +
        "&endUnit=" + action.customer.endUnit,
        AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getReportRefPrepaidKwApi(action){
    return axios.get(API_ENDPOINT+"report/ref/residential/prepaid?" +
            "startDate=" + action.customer.startDate +
            "&endDate=" + action.customer.endDate +
            "&startUnit=" + action.customer.startUnit +
            "&endUnit=" + action.customer.endUnit,
        AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getReportRefPrepaidGreaterThan50KwNotSponsorApi(action){
    return axios.get(API_ENDPOINT+`report/ref/51kwh?consumption=${action.customer.id}&date=${action.customer.date}`,AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getReportRefPrepaidGreaterThan50KwSponsorApi(action){
    return axios.get(API_ENDPOINT+"report/ref/51kwh/2",AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}
export function getReportRefPrepaidGreaterThan50KwResidentialApi(action){
    return axios.get(API_ENDPOINT+`report/ref/51kwh?consumption=${action.customer.id}&date=${action.customer.date}`,AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
export function getHistory12MonthsApi(action){
    return axios.get(`${API_ENDPOINT}report/ref/12months?consumption=${action.report.consumption}&id=${action.report.id}&date=${action.report.date}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }))
}

export function getAdditionSubsidiesApi(action){
    return axios.get(`${API_ENDPOINT}report/ref/addition-subsidy?consumption=${action.report.consumption}&date=${action.report.date}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }))
}