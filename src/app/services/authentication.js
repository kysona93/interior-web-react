import axios from 'axios';
import {CONFIG, AUTH_CONFIG, API_ENDPOINT} from './header';

/* USER LOGIN */
export function userLoginApi(action){
    return axios.post(`${API_ENDPOINT}auth/login`, JSON.stringify(action.user), CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* ADD USER */
export function AddUserApi(action){
    return axios.post(`${API_ENDPOINT}users`, JSON.stringify(action.user), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* LIST USERS */
export function getUsersStatusApi(action){
    return axios.get(`${API_ENDPOINT}users/list?pageSize=100&nowPage=1&status=${action.status}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* GET USER */
export function getUserApi(action){
    return axios.get(`${API_ENDPOINT}auth/user/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* UPDATE USER */
export function apiUpdateUser(action){
    console.log(action.user);
    return axios.put(`${API_ENDPOINT}users`, JSON.stringify(action.user), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* DELETE USER */
export function deleteUserApi(action){
    return axios.delete(`${API_ENDPOINT}users/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* ADD GROUP */
export function addGroupApi(action){
    return axios.post(`${API_ENDPOINT}groups`, JSON.stringify(action.group), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* LIST GROUPS */
export function getGroupsStatusApi(action){
    return axios.get(`${API_ENDPOINT}groups?enabled=${action.status}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* GET GROUP */
export function getGroupApi(action){
    return axios.get(`${API_ENDPOINT}groups/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* UPDATE GROUP */
export function updateGroupApi(action){
    return axios.put(`${API_ENDPOINT}groups`, JSON.stringify(action.group.data), AUTH_CONFIG
    )
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

/* ENABLED GROUP */
export function enabledGroupApi(action){
    return axios.put(`${API_ENDPOINT}groups/enabled-userGroup/${action.id}`, JSON.stringify(action), AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
