import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../../header';

export const addStepWorkApi = (action) => {
    return axios.post(`${API_ENDPOINT}step/works`, JSON.stringify(action.work), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const updateStepWorkApi = (action) => {
    return axios.put(`${API_ENDPOINT}step/works`, JSON.stringify(action.work), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const checkStepWorkApi = (action) => {
    return axios.put(`${API_ENDPOINT}step/works/check`, JSON.stringify(action.work), AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const deleteStepWorkApi = (action) => {
    return axios.delete(`${API_ENDPOINT}step/works/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getStepWorkApi = () => {
    return axios.get(`${API_ENDPOINT}step/work`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getStepWorksApi = (action) => {
    return axios.get(`${API_ENDPOINT}step/works?stepType=${action.work.type}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const getStepWorkDetailsApi = (action) => {
    return axios.get(`${API_ENDPOINT}step/work/details?stepId=${action.work.stepId}&year=${action.work.year}&workType=${action.work.workType}&installer=${action.work.installer}&status=${action.work.status}&pageSize=${action.work.pageSize}&nowPage=${action.work.nowPage}&orderBy=${action.work.orderBy}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};

export const printStepWorkApi = (action) => {
    return axios.get(`${API_ENDPOINT}step/works/${action.id}/print`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};


export const removeStepWorkDetailApi = (action) => {
    return axios.put(`${API_ENDPOINT}step/work/details/${action.id}/remove`, {}, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
};