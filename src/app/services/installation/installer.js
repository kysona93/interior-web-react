import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from '../header';

/* ADD INSTALLER */
export function addInstallerApi(action){
    return axios.post(API_ENDPOINT+"installers",
                        JSON.stringify(action.installer),
                        AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

/* LIST ALL INSTALLERS */
export function getAllInstallersApi(action){
    return axios.get(API_ENDPOINT+`employees/installer?name=${action.installer.name}&installDate=${action.installer.installDate}&status=${action.installer.status}`, AUTH_CONFIG)
                .then(response => ({ response: response.data }))
                .catch(error => ({ error }));
}

export function getScheduleInstallingApi(){
    return axios.get(API_ENDPOINT + "",AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({ error }));
}

/* UPDATE INSTALLER */
export function updateInstallersApi(action){
    return axios.put(API_ENDPOINT+"installers",
                    JSON.stringify(action.installer),
                    AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

/* DELETE INSTALLER */
export function deleteInstallerApi(action){
    return axios.delete(API_ENDPOINT+"installers/"+action.id,
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}