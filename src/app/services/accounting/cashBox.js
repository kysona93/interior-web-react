import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function getAllCashBoxesApi(){
    return axios.get(API_ENDPOINT+"cashboxes", AUTH_CONFIG)
                    .then(response => ({ response }))
                    .catch(error => ({ error }));
}

export function addCashBoxApi(action) {
    return axios.post(`${API_ENDPOINT}cashboxes`,
                JSON.stringify(action.box),
                AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function getAllUsersCashBoxesApi(){
    return axios.get(API_ENDPOINT+"users/all", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}

export function getAllCashBoxesByUserIdApi(action){
    return axios.get(API_ENDPOINT+"cashboxes/userId", AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }));
}
