import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function getAllMailSettingsApi(action){
    return axios.get(`${API_ENDPOINT}invoices/mail/receiver`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function addMailSettingsApi(action) {
    return axios.post(`${API_ENDPOINT}invoices/mail/receiver`, JSON.stringify(action.mailSettings), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function updateMailSettingsApi(action){
    return axios.put(`${API_ENDPOINT}invoices/mail/receiver`, JSON.stringify(action.mailSettings), AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}

export function deleteMailSettingApi(action){
    return axios.delete(`${API_ENDPOINT}invoices/mail/receiver/${action.id}`, AUTH_CONFIG)
                .then(response => ({ response }))
                .catch(error => ({ error }));
}
