import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT, FILE_CONFIG} from '../header';

export function getCustomerInfoByMeterSerialApi(action){
    return axios.get(`${API_ENDPOINT}invoices`,
        JSON.stringify(action.record), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function addMeterUsageRecordApi(action){
    return axios.post(`${API_ENDPOINT}meter/usages`,
        JSON.stringify(action.usage),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function importExcelMeterUsageRecordApi(action){
    return axios.post(`${API_ENDPOINT}meter/usages/import`,
                action.fileExcel, FILE_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getMetersByCustomerIdApi(action){
    return axios.get(API_ENDPOINT+"meters/customerId?id=" + action.customerId, AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function getMeterUsageByMeterSerialApi(action){
    return axios.get(`${API_ENDPOINT}meter/usages/meterserial?serial=${action.serial}`, AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function updateMeterUsageByIdApi(action){
    return axios.put(`${API_ENDPOINT}meter/usages`,
                JSON.stringify(action.usage), AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function verifyMeterUsageIndividualApi(action){
    return axios.get(API_ENDPOINT+"meter/usages/verify/individual?" +
                    "bookDate=" + action.usage.bookDate +
                    "&customerId="+action.usage.customerId
                    , AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

export function getAllCustomerIdsAndMeterSerialsApi(action){
    return axios.get(API_ENDPOINT+"meters/customerIds/meterSerials", AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function getMeterUsageProblemsApi(action){
    return axios.get(API_ENDPOINT+"meter/usages/import/problems", AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}
