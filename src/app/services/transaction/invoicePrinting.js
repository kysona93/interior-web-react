import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../header';

export function getAllIssueInvoicesApi(action){
    return axios.get(API_ENDPOINT+"invoices/printing?" +
                    "startDate=" + action.invoice.startDate +
                    "&endDate=" + action.invoice.endDate +
                    //"&invoiceType=" + action.invoice.invoiceType +
                    "&transformer=" + action.invoice.transformer +
                    "&customerId=" + action.invoice.customerId
                    , AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

export function generateAllInvoicesApi(action){
    console.log("action.invoices " + action.invoices);
    return axios.post(API_ENDPOINT+"invoices/postpaid/generate/pdf",
                JSON.stringify(action.invoices), AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}
