import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../header';

export function getRecordKwhMonthlyApi(action){
    return axios.get(API_ENDPOINT + "meter/usages/kwhmonthly?" +
                    "bookDate=" + action.record.bookDate +
                    "&meterTypeId=" + action.record.meterTypeId +
                    "&groupInvoiceId=" + action.record.groupInvoiceId +
                    "&transformerId=" + action.record.transformerId +
                    "&sortBy=" + action.record.sortBy +
                    "&sortType=" + action.record.sortType
                    , AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

