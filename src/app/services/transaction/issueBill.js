import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../header';

export function getAllCustomerBillsApi(action){
    return axios.get(API_ENDPOINT+ "meter/usages/customer?problem=" + action.problem, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function issueBillCustomersApi(action){
    return axios.post(`${API_ENDPOINT}invoices/issue/bills`,
        JSON.stringify(action.customers),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

// export function getAllCustomersProblemApi(action){
//     return axios.get(API_ENDPOINT+ "meter/usages/customers/problem?" +
//                     "meterTypeId=" + action.meterTypeId,
//                     AUTH_CONFIG)
//                     .then(response => ({response}))
//                     .catch(error => ({error}));
// }
