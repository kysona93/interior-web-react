import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../header';

export function getAllInvoicesCustomerPaymentApi(action){
    return axios.get(API_ENDPOINT+"invoices/payment?" +
                    "customerId=" + action.invoice.customerId +
                    "&invoiceNo=" + action.invoice.invoiceNo +
                    "&invoiceType=" + action.invoice.invoiceType
                    , AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

export function addQuickPaidPaymentApi(action){
    return axios.post(`${API_ENDPOINT}invoices/${action.id}`,
        JSON.stringify(action.payment),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getInvoiceByBarcodeApi(action){
    return axios.get(API_ENDPOINT+"invoices/barcode?barcode=" + action.barcode, AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}
