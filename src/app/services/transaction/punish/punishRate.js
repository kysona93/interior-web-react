import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from './../../header';

export function getAllPunishRatesApi(action){
    return axios.get(API_ENDPOINT+"punishrates", AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function addPunishRateApi(action){
    return axios.post(API_ENDPOINT+"punishrates",
        JSON.stringify(action.punish), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}


export function updatePunishRateApi(action){
    return axios.put(API_ENDPOINT+"punishrates",
        JSON.stringify(action.punish), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}


export function deletePunishRateApi(action){
    return axios.delete(API_ENDPOINT+"punishrates/"+action.id, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}
