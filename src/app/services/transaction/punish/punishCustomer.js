import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function getAllInvoicesPunishApi(action){
        return axios.get(API_ENDPOINT+"punish-customers/invoices?" +
            "currentDate=" + action.date
            , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function addCustomerPunishApi(action){
    return axios.post(API_ENDPOINT+"punish-customers",
        JSON.stringify(action.customer)
        , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllCustomersPunishRatesApi(action){
    return axios.get(API_ENDPOINT+"punish-customers?" +
        "customerId=" + action.customer.customerId +
        "&invoiceNo=" + action.customer.invoiceNo +
        "&name=" + action.customer.name
        , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function updateCustomerPunishApi(action){
    return axios.put(API_ENDPOINT+"punish-customers",
        JSON.stringify(action.customer), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function deleteCustomerPunishApi(action){
    return axios.delete(API_ENDPOINT+"punish-customers?id="+action.id, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}
