import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function addLostKwhApi(action){
    return axios.post(`${API_ENDPOINT}lostkwhs`,
        JSON.stringify(action.kwh), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllLostKwhApi(action){
    return axios.get(API_ENDPOINT+"lostkwhs?" +
                    "startDate=" + action.kwh.startDate +
                    "&endDate=" + action.kwh.endDate,
                    AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

export function getAllMeterIdsApi(action){
    return axios.get(API_ENDPOINT+"lostkwhs/meterids", AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllLostKwhStatusApi(action){
    return axios.get(API_ENDPOINT+"statuslost", AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}