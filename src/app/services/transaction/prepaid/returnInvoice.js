import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function issueBillPrepaidApi(action){
    return axios.post(`${API_ENDPOINT}returninvoices`,
        JSON.stringify(action.prepaid), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllReturnInvoicesApi(action){
    return axios.get(API_ENDPOINT+"returninvoices?" +
                "sDate=" + action.invoice.startDate +
                "&eDate=" + action.invoice.endDate +
                "&invoiceType=" + action.invoice.invoiceType +
                "&tranSerial=" + action.invoice.transformer +
                "&cusId=" + action.invoice.customerId,
                AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function generateAllReturnInvoicesApi(action){
    return axios.post(`${API_ENDPOINT}invoices/prepaid/generate/pdf`,
                    JSON.stringify(action.invoices), AUTH_CONFIG)
                    .then(response => ({response}))
                    .catch(error => ({error}));
}

export function getReturnInvoiceByPrepaidIdApi(action){
    return axios.get(`${API_ENDPOINT}returninvoices/prepaid/${action.prepaidId}`,
                AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function updateReturnInvoiceByPrepaidIdApi(action){
    return axios.put(`${API_ENDPOINT}returninvoices/prepaid/${action.prepaidId}`,
                    JSON.stringify(action), AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}