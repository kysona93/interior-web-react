import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function addBalancePrepaidKwhApi(action){
    return axios.post(`${API_ENDPOINT}balanceprepaidkwh`,
                JSON.stringify(action.balance), AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));

}

export function getAllBalancePrepaidKwhApi(action){
    return axios.get(API_ENDPOINT+"prepaid/power-sale/generate-balance?" +
                "prepaidId=" + action.balance.prepaidId +
                "&customerName=" + action.balance.customerName +
                "&meterSerial=" + action.balance.meterSerial +
                "&transformer="+ action.balance.transformer
                , AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function getAllBillBalancePrepaidApi(action){
    return axios.get(API_ENDPOINT+"balanceprepaidkwh", AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function voidBalancePrepaidApi(action){
    return axios.put(API_ENDPOINT+"balanceprepaidkwh?id=" + action.id, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllNewPrepaidCustomersApi(action){
    return axios.get(API_ENDPOINT+"customers/prepaid-new", AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function saveBalancePrepaidKwApi(action){
    return axios.post(`${API_ENDPOINT}balanceprepaidkwh/save`,
        JSON.stringify(action.balance), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function listAllBalancePrepaidKwApi(action){
    return axios.get(API_ENDPOINT+"balanceprepaidkwh/list-balances?" +
        "startDate=" + action.balance.startDate +
        "&endDate=" + action.balance.endDate +
        "&meterId=" + action.balance.meterId +
        "&prepaidId=" + action.balance.prepaidId
        , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function updateBalancePrepaidKwApi(action){
    return axios.put(`${API_ENDPOINT}balanceprepaidkwh/update`,
        JSON.stringify(action.balance)
        , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}