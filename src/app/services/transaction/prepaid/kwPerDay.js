import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function getAllGenerateKwPerDayApi(action){
    return axios.get(API_ENDPOINT+"prepaid/power-sale/generate-kw-per-day?" +
        "prepaidId=" + action.prepaid.prepaidId +
        "&customerName=" + action.prepaid.customerName +
        "&meterSerial=" + action.prepaid.meterSerial +
        "&transformer=" + action.prepaid.transformer
        , AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function generateKwPerDayApi(action){
    return axios.post(`${API_ENDPOINT}prepaid/power-sale/calculate-kwh`,
        JSON.stringify(action.kw), AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));

}
