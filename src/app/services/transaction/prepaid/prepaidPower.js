import axios from 'axios';
import { AUTH_CONFIG, API_ENDPOINT} from '../../header';

export function getPrepaidPowersApi(action){
    return axios.get(`${API_ENDPOINT}prepaid/power-sale?pageSize=${action.prepaid.limit}
    &nowPage=${action.prepaid.page}&date=${action.prepaid.date}`, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function addPrepaidPowerApi(action){
    return axios.post(`${API_ENDPOINT}prepaid/power-sale`,
        JSON.stringify(action.prepaid),
        AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getAllPrepaidPowerApi(action){
    return axios.get(API_ENDPOINT+"prepaid/power-sale/list?" +
                "startDate=" + action.prepaid.startDate +
                "&endDate=" + action.prepaid.endDate, AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}

export function getAllVerifyPrepaidEndPurchaseApi(action){
    return axios.get(API_ENDPOINT+"prepaid/power-sale/verify-end-purchases?" +
                "prepaidId=" + action.prepaid.prepaidId +
                "&customerName=" + action.prepaid.customerName +
                "&meterSerial=" + action.prepaid.meterSerial +
                "&transformer=" + action.prepaid.transformer
                , AUTH_CONFIG)
                .then(response => ({response}))
                .catch(error => ({error}));
}
