import axios from 'axios';
import {AUTH_CONFIG, API_ENDPOINT} from './../header';

export function addEmployeeApi(action){
    return axios.post(`${API_ENDPOINT}employees`,
        JSON.stringify(action.employee), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function updateEmployeeApi(action) {
    return axios.put(`${API_ENDPOINT}employees`, JSON.stringify(action.employee), AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function deleteEmployeeApi(action) {
    return axios.delete(`${API_ENDPOINT}employees/${action.id}`, AUTH_CONFIG)
        .then(response => ({response}))
        .catch(error => ({error}));
}

export function getEmployeesApi(action){
    return axios.get(`${API_ENDPOINT}employees/paging?name=${action.employee.name}&position=${action.employee.position}&pageSize=${action.employee.limit}&nowPage=${action.employee.page}&orderBy=${action.employee.orderBy}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

export function getAllEmployeesApi(action){
    return axios.get(`${API_ENDPOINT}employees/schedule?installer=${action.employee.installer}&transformer=${action.employee.transformer}&installFromDate=${action.employee.installFromDate}&installToDate=${action.employee.installToDate}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

/**
 * Function get installer with customer
 * @param action
 * @returns {Promise.<{response}>}
 */
export function getAllInstallersApi(action){
    return axios.get(API_ENDPOINT+`employees/installer?name=${action.installer.name}&installDate=${action.installer.installDate}&status=${action.installer.status}`, AUTH_CONFIG)
        .then(response => ({ response: response.data }))
        .catch(error => ({ error }));
}

/**
 * Function get installer with customer counting
 * @param action
 * @returns {Promise.<{response}>}
 */
export function getInstallerReportsApi(action){
    return axios.get(`${API_ENDPOINT}employees/installer/report?status=${action.installer.status}&installDate=${action.installer.installDate}`, AUTH_CONFIG)
        .then(response => ({response:response.data}))
        .catch(error => ({error}));
}

/**
 * Function get installation schedule
 * @param action
 * @returns {Promise.<{response}>}
 */
export function getInstallingSchedulesApi(action){
    return axios.get(`${API_ENDPOINT}employees/schedule?installer=${action.installer.installer}&transformer=${action.installer.transformer}&installFromDate=${action.installer.installFromDate}&installToDate=${action.installer.installToDate}&status=${action.installer.status}`, AUTH_CONFIG)
        .then(response => ({response: response.data}))
        .catch(error => ({error}));
}

