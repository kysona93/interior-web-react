import axios from 'axios';
import {CONFIG, AUTH_CONFIG, API_ENDPOINT} from './header';

/* get pages by group */
export function getPagesByGroupApi(action){
    return axios.get(`${API_ENDPOINT}pages/groups?groupId[]=${action.ids}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}

/* get pages by user */
export function getPagesByUserApi(action){
    return axios.get(`${API_ENDPOINT}pages/user/${action.id}`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}

/* get pages by username */
export function getPagesByUsernameApi(action){
    return axios.get(`${API_ENDPOINT}pages/username`, AUTH_CONFIG)
        .then(response => ({ response }))
        .catch(error => ({ error }))
}