export const remove = (list) => {
    list.filter(function (item, index, inputArray) {
        return inputArray.indexOf(item) == index;
    })
};

export const clean = (actual) => {
    let newArray = [];
    for (let i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
};


export const removeDuplicate = (list) => {
    let result = [];
    for(let i=0; i<list.length; i++){
        if(result.indexOf(list[i]) === -1){
            result.push(list[i])
        }
    }
    return result;
};

//Simple javascript
export const removeElements = (list1, list2) => {
    let a = [], diff = [];
    for (let i=0; i<list1.length; i++) {
        a[list1[i]] = true;
    }
    for (let i=0; i <list2.length; i++) {
        if (a[list2[i]]) {
            delete a[list2[i]];
        } else {
            a[list2[i]] = true;
        }
    }
    for (let k in a) {
        diff.push(k);
    }
    return diff;
};

//Es6
export const getLeftOutJoin = (list1, list2) => {
    return list2.filter(x => list1.indexOf(x) === -1);
};


//

export const compare = (list1, list2) => {
    return (list1.length === list2.length) && list1.every(function(element, index) {
        return element === list2[index];
    });
};

//Remove duplicate array of object
export const uniqueArrayObj = (list) => {
    return list.filter((thing, index, self) => self.findIndex((t) => {return t.id === thing.id }) === index);
};
