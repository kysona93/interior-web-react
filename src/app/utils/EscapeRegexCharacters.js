export const escapeRegexCharacters = (str) => {
    const escapedValue = str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    return new RegExp('^' + escapedValue, 'i');
};