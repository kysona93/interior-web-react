export const get12Months = (date=new Date().toDateString()) => {
    const ascMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    const descMonths = ["Dec", "Nov", "Oct", "Sep", "Aug", "Jul", "Jun", "May", "Apr", "Mar", "Feb", "Jan"];
    const today = new Date(date);
    const month = today.getMonth();
    let month0 = [];
    let month1 = [];
    for(let i = 0; i < month; i++) {
        month0.push({
            date: ascMonths[i] +'-'+ today.getFullYear(),
            power: 0
        });
    }
    for(let i = 0; i <= (11 - month); i++) {
        month1.push({
            date: descMonths[i] +'-'+ (today.getFullYear() - 1),
            power: 0
        });
    }
    return month0.concat(month1);
};

export const combineArrayObj = (old, last) => {
    for(let i = 0, l = old.length; i < l; i++) {
        for(let j = 0, ll = last.length; j < ll; j++) {
            if(old[i].date === last[j].date) {
                old.splice(i, 1, last[j]);
                break;
            }
        }
    }
   return old;
};