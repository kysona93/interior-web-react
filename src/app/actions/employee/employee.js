export const ADD_EMPLOYEE = "ADD_EMPLOYEE";
export const ADD_EMPLOYEE_SUCCESS = "ADD_EMPLOYEE_SUCCESS";
export const ADD_EMPLOYEE_FAIL = "ADD_EMPLOYEE_FAIL";

export const UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE";
export const UPDATE_EMPLOYEE_SUCCESS = "UPDATE_EMPLOYEE_SUCCESS";
export const UPDATE_EMPLOYEE_FAIL = "UPDATE_EMPLOYEE_FAIL";

export const DELETE_EMPLOYEE = "DELETE_EMPLOYEE";
export const DELETE_EMPLOYEE_SUCCESS = "DELETE_EMPLOYEE_SUCCESS";
export const DELETE_EMPLOYEE_FAIL = "DELETE_EMPLOYEE_FAIL";

export const GET_EMPLOYEES = "GET_EMPLOYEES";
export const GET_EMPLOYEES_SUCCESS = "GET_EMPLOYEES_SUCCESS";
export const GET_EMPLOYEES_FAIL = "GET_EMPLOYEES_FAIL";

export const GET_ALL_EMPLOYEES = "GET_ALL_EMPLOYEES";
export const GET_ALL_EMPLOYEES_SUCCESS = "GET_ALL_EMPLOYEES_SUCCESS";
export const GET_ALL_EMPLOYEES_FAIL = "GET_ALL_EMPLOYEES_FAIL";

export const GET_ALL_INSTALLER = "GET_ALL_INSTALLER";
export const GET_ALL_INSTALLER_SUCCESS = "GET_ALL_INSTALLER_SUCCESS";
export const GET_ALL_INSTALLER_FAIL = "GET_ALL_INSTALLER_FAIL";

export const GET_INSTALLER_REPORTS = "GET_INSTALLER_REPORTS";
export const GET_INSTALLER_REPORTS_SUCCESS = "GET_INSTALLER_REPORTS_SUCCESS";
export const GET_INSTALLER_REPORTS_FAIL = "GET_INSTALLER_REPORTS_FAIL";

export const GET_INSTALLING_SCHEDULES = "GET_INSTALLING_SCHEDULES";
export const GET_INSTALLING_SCHEDULES_SUCCESS = "GET_INSTALLING_SCHEDULES_SUCCESS";
export const GET_INSTALLING_SCHEDULES_FAIL = "GET_INSTALLING_SCHEDULES_FAIL";

export function addEmployeeAction(employee) {
    return {
        type : ADD_EMPLOYEE,
        employee: employee
    }
}

export function updateEmployeeAction(employee) {
    return {
        type : UPDATE_EMPLOYEE,
        employee: employee
    }
}

export function deleteEmployeeAction(id) {
    return {
        type : DELETE_EMPLOYEE,
        id: id
    }
}

export function getEmployeesAction(employee) {
    return {
        type : GET_EMPLOYEES,
        employee: employee
    }
}

export function getAllEmployeesAction(employee) {
    return {
        type : GET_ALL_EMPLOYEES,
        employee : employee
    }
}

export function getAllInstallersAction(installer){
    return{
        type: GET_ALL_INSTALLER,
        installer: installer
    }
}

export function getInstallerReportsAction(installer){
    return{
        type: GET_INSTALLER_REPORTS,
        installer: installer
    }
}

export function getInstallingSchedulesAction(installer){
    return{
        type: GET_INSTALLING_SCHEDULES,
        installer: installer
    }
}