export const ADD_CONNECTION_BUY = "ADD_CONNECTION_BUY";
export const ADD_CONNECTION_BUY_SUCCESS = "ADD_CONNECTION_BUY_SUCCESS";
export const ADD_CONNECTION_BUY_FAIL = "ADD_CONNECTION_BUY_FAIL";

export const GET_CONNECTION_BUY = "GET_CONNECTION_BUY";
export const GET_CONNECTION_BUY_SUCCESS = "GET_CONNECTION_BUY_SUCCESS";
export const GET_CONNECTION_BUY_FAIL = "GET_CONNECTION_BUY_FAIL";

export const UPDATE_CONNECTION_BUY = "UPDATE_CONNECTION_BUY";
export const UPDATE_CONNECTION_BUY_SUCCESS = "UPDATE_CONNECTION_BUY_SUCCESS";
export const UPDATE_CONNECTION_BUY_FAIL = "UPDATE_CONNECTION_BUY_FAIL";

export const DELETE_CONNECTION_BUY = "DELETE_CONNECTION_BUY";
export const DELETE_CONNECTION_BUY_SUCCESS = "DELETE_CONNECTION_BUY_SUCCESS";
export const DELETE_CONNECTION_BUY_FAIL = "DELETE_CONNECTION_BUY_FAIL";

export const ADD_PURCHASE_KWH = "ADD_PURCHASE_KWH";
export const ADD_PURCHASE_KWH_SUCCESS = "ADD_PURCHASE_KWH_SUCCESS";
export const ADD_PURCHASE_KWH_FAIL = "ADD_PURCHASE_KWH_FAIL";

export const GET_PURCHASE_KWH = "GET_PURCHASE_KWH";
export const GET_PURCHASE_KWH_SUCCESS = "GET_PURCHASE_KWH_SUCCESS";
export const GET_PURCHASE_KWH_FAIL = "GET_PURCHASE_KWH_FAIL";

export const UPDATE_PURCHASE_KWH = "UPDATE_PURCHASE_KWH";
export const UPDATE_PURCHASE_KWH_SUCCESS = "UPDATE_PURCHASE_KWH_SUCCESS";
export const UPDATE_PURCHASE_KWH_FAIL = "UPDATE_PURCHASE_KWH_FAIL";

export const DELETE_PURCHASE_KWH = "DELETE_PURCHASE_KWH";
export const DELETE_PURCHASE_KWH_SUCCESS = "DELETE_PURCHASE_KWH_SUCCESS";
export const DELETE_PURCHASE_KWH_FAIL = "DELETE_PURCHASE_KWH_FAIL";

export const GET_PURCHASE_KWH_MONTHLY_REPORT = "GET_PURCHASE_KWH_MONTHLY_REPORT";
export const GET_PURCHASE_KWH_MONTHLY_REPORT_SUCCESS = "GET_PURCHASE_KWH_MONTHLY_REPORT_SUCCESS";
export const GET_PURCHASE_KWH_MONTHLY_REPORT_FAIL = "GET_PURCHASE_KWH_MONTHLY_REPORT_FAIL";

export function addConnectionBuyAction(purchaseKwh){
    return {
        type: ADD_CONNECTION_BUY,
        purchaseKwh: purchaseKwh
    }
}

export function getConnectionBuysAction(purchaseKwh){
    return {
        type: GET_CONNECTION_BUY,
        purchaseKwh: purchaseKwh
    }
}

export function updateConnectionBuyAction(purchaseKwh){
    return {
        type: UPDATE_CONNECTION_BUY,
        purchaseKwh: purchaseKwh
    }
}

export function deleteConnectionBuyAction(id){
    return {
        type: DELETE_CONNECTION_BUY,
        id: id
    }
}

export function addPurchaseKwhAction(purchaseKwh){
    return {
        type: ADD_PURCHASE_KWH,
        purchaseKwh: purchaseKwh
    }
}

export function getPurchaseKwhsAction(purchaseKwh){
    return {
        type: GET_PURCHASE_KWH,
        purchaseKwh: purchaseKwh
    }
}

export function updatePurchaseKwhAction(purchaseKwh){
    return {
        type: UPDATE_PURCHASE_KWH,
        purchaseKwh: purchaseKwh
    }
}

export function deletePurchaseKwhAction(id){
    return {
        type: DELETE_PURCHASE_KWH,
        id: id
    }
}

export function getPurchaseKwhMonthlyReportsAction(purchaseKwh){
    return {
        type: GET_PURCHASE_KWH_MONTHLY_REPORT,
        purchaseKwh: purchaseKwh
    }
}