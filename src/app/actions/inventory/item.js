export const ADD_ITEM = "ADD_ITEM";
export const ADD_ITEM_SUCCESS = "ADD_ITEM_SUCCESS";
export const ADD_ITEM_FAIL = "ADD_ITEM_FAIL";

export const GET_ALL_ITEMS = "GET_ALL_ITEMS";
export const GET_ALL_ITEMS_SUCCESS = "GET_ALL_ITEMS_SUCCESS";
export const GET_ALL_ITEMS_FAIL = "GET_ALL_ITEMS_FAIL";

export const GET_ITEMS = "GET_ITEMS";
export const GET_ITEMS_SUCCESS = "GET_ITEMS_SUCCESS";
export const GET_ITEMS_FAIL = "GET_ITEMS_FAIL";

export const UPDATE_ITEM = "UPDATE_ITEM";
export const UPDATE_ITEM_SUCCESS = "UPDATE_ITEM_SUCCESS";
export const UPDATE_ITEM_FAIL = "UPDATE_ITEM_FAIL";

export const DELETE_ITEM = "DELETE_ITEM";
export const DELETE_ITEM_SUCCESS = "DELETE_ITEM_SUCCESS";
export const DELETE_ITEM_FAIL = "DELETE_ITEM_FAIL";

export function addItemAction(item){
    return {
        type: ADD_ITEM,
        item: item
    }
}

export function getAllItemsAction(id){
    return {
        type: GET_ALL_ITEMS,
        id: id
    }
}

export function getItemsAction(item){
    return {
        type: GET_ITEMS,
        item: item
    }
}

export function updateItemAction(item){
    return {
        type: UPDATE_ITEM,
        item: item
    }
}

export function deleteItemAction(id){
    return {
        type: DELETE_ITEM,
        id: id
    }
}