export const ADD_PLANNING = "ADD_PLANNING";
export const ADD_PLANNING_SUCCESS = "ADD_PLANNING_SUCCESS";
export const ADD_PLANNING_FAIL = "ADD_PLANNING_FAIL";

export const GET_PLANNINGS = "GET_PLANNINGS";
export const GET_PLANNINGS_SUCCESS = "GET_PLANNINGS_SUCCESS";
export const GET_PLANNINGS_FAIL = "GET_PLANNINGS_FAIL";

export const GET_PLANNING_NO = "GET_PLANNING_NO";
export const GET_PLANNING_NO_SUCCESS = "GET_PLANNING_NO_SUCCESS";
export const GET_PLANNING_NO_FAIL = "GET_PLANNING_NO_FAIL";

export const UPDATE_PLANNING = "UPDATE_PLANNING";
export const UPDATE_PLANNING_SUCCESS = "UPDATE_PLANNING_SUCCESS";
export const UPDATE_PLANNING_FAIL = "UPDATE_PLANNING_FAIL";

export const DELETE_PLANNING = "DELETE_PLANNING";
export const DELETE_PLANNING_SUCCESS = "DELETE_PLANNING_SUCCESS";
export const DELETE_PLANNING_FAIL = "DELETE_PLANNING_FAIL";

export const GET_PLANNINGS_APPROVED = "GET_PLANNINGS_APPROVED";
export const GET_PLANNINGS_APPROVED_SUCCESS = "GET_PLANNINGS_APPROVED_SUCCESS";
export const GET_PLANNINGS_APPROVED_FAIL = "GET_PLANNINGS_APPROVED_FAIL";

export function addPlanningAction(planning){
    return {
        type: ADD_PLANNING,
        planning: planning
    }
}

export function getPlanningsAction(planning){
    return {
        type: GET_PLANNINGS,
        planning: planning
    }
}

export function getPlanningNoAction(){
    return {
        type: GET_PLANNING_NO
    }
}

export function updatePlanningAction(planning){
    return {
        type: UPDATE_PLANNING,
        planning: planning
    }
}

export function deletePlanningAction(id){
    return {
        type: DELETE_PLANNING,
        id: id
    }
}

export function getPlanningsApprovedAction(){
    return {
        type: GET_PLANNINGS_APPROVED
    }
}