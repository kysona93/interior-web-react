export const ADD_REQUEST = "ADD_REQUEST";
export const ADD_REQUEST_SUCCESS = "ADD_REQUEST_SUCCESS";
export const ADD_REQUEST_FAIL = "ADD_REQUEST_FAIL";

export const GET_REQUESTS = "GET_REQUESTS";
export const GET_REQUESTS_SUCCESS = "GET_REQUESTS_SUCCESS";
export const GET_REQUESTS_FAIL = "GET_REQUESTS_FAIL";

export const GET_REQUEST_NO = "GET_REQUEST_NO";
export const GET_REQUEST_NO_SUCCESS = "GET_REQUEST_NO_SUCCESS";
export const GET_REQUEST_NO_FAIL = "GET_REQUEST_NO_FAIL";

export const UPDATE_REQUEST = "UPDATE_REQUEST";
export const UPDATE_REQUEST_SUCCESS = "UPDATE_REQUEST_SUCCESS";
export const UPDATE_REQUEST_FAIL = "UPDATE_REQUEST_FAIL";

export const DELETE_REQUEST = "DELETE_REQUEST";
export const DELETE_REQUEST_SUCCESS = "DELETE_REQUEST_SUCCESS";
export const DELETE_REQUEST_FAIL = "DELETE_REQUEST_FAIL";

export const GET_REQUESTS_APPROVED = "GET_REQUESTS_APPROVED";
export const GET_REQUESTS_APPROVED_SUCCESS = "GET_REQUESTS_APPROVED_SUCCESS";
export const GET_REQUESTS_APPROVED_FAIL = "GET_REQUESTS_APPROVED_FAIL";

export function addRequestAction(request){
    return {
        type: ADD_REQUEST,
        request: request
    }
}

export function getRequestsAction(request){
    return {
        type: GET_REQUESTS,
        request: request
    }
}

export function getRequestNoAction(){
    return {
        type: GET_REQUEST_NO
    }
}

export function updateRequestAction(request){
    return {
        type: UPDATE_REQUEST,
        request: request
    }
}

export function deleteRequestAction(id){
    return {
        type: DELETE_REQUEST,
        id: id
    }
}

export function getRequestsApprovedAction(){
    return {
        type: GET_REQUESTS_APPROVED
    }
}