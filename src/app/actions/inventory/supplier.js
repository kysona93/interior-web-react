export const ADD_SUPPLIER = "ADD_SUPPLIER";
export const ADD_SUPPLIER_SUCCESS = "ADD_SUPPLIER_SUCCESS";
export const ADD_SUPPLIER_FAIL = "ADD_SUPPLIER_FAIL";

export const GET_ALL_SUPPLIER = "GET_ALL_SUPPLIER";
export const GET_ALL_SUPPLIER_SUCCESS = "GET_ALL_SUPPLIER_SUCCESS";
export const GET_ALL_SUPPLIER_FAIL = "GET_ALL_SUPPLIER_FAIL";

export const UPDATE_SUPPLIER = "UPDATE_SUPPLIER";
export const UPDATE_SUPPLIER_SUCCESS = "UPDATE_SUPPLIER_SUCCESS";
export const UPDATE_SUPPLIER_FAIL = "UPDATE_SUPPLIER_FAIL";

export const DELETE_SUPPLIER = "DELETE_SUPPLIER";
export const DELETE_SUPPLIER_SUCCESS = "DELETE_SUPPLIER_SUCCESS";
export const DELETE_SUPPLIER_FAIL = "DELETE_SUPPLIER_FAIL";

export const GET_SUPPLIERS = "GET_SUPPLIERS";
export const GET_SUPPLIERS_SUCCESS = "GET_SUPPLIERS_SUCCESS";
export const GET_SUPPLIERS_FAIL = "GET_SUPPLIERS_FAIL";

/* ADD SUPPLIER */
export function addSupplierAction(supplier){
    return {
        type: ADD_SUPPLIER,
        supplier: supplier
    }
}

/* GET SUPPLIER */
export function getAllSupplierAction(supplier){
    return {
        type: GET_ALL_SUPPLIER,
        supplier: supplier
    }
}

export function getSuppliersAction(supplier){
    return {
        type: GET_SUPPLIERS,
        supplier: supplier
    }
}

/* UPDATE SUPPLIER */
export function updateSupplierAction(supplier){
    return {
        type: UPDATE_SUPPLIER,
        supplier: supplier
    }
}

/* DELETE SUPPLIER */
export function deleteSupplierAction(id){
    return {
        type: DELETE_SUPPLIER,
        id: id
    }
}