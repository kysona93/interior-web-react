export const ADD_UNIT_TYPE = "ADD_UNIT_TYPE";
export const ADD_UNIT_TYPE_SUCCESS = "ADD_UNIT_TYPE_SUCCESS";
export const ADD_UNIT_TYPE_FAIL = "ADD_UNIT_TYPE_FAIL";

export const GET_ALL_UNIT_TYPES = "GET_ALL_UNIT_TYPES";
export const GET_ALL_UNIT_TYPES_SUCCESS = "GET_ALL_UNIT_TYPES_SUCCESS";
export const GET_ALL_UNIT_TYPES_FAIL = "GET_ALL_UNIT_TYPES_FAIL";

export const UPDATE_UNIT_TYPE = "UPDATE_UNIT_TYPE";
export const UPDATE_UNIT_TYPE_SUCCESS = "UPDATE_UNIT_TYPE_SUCCESS";
export const UPDATE_UNIT_TYPE_FAIL = "UPDATE_UNIT_TYPE_FAIL";

export const DELETE_UNIT_TYPE = "DELETE_UNIT_TYPE";
export const DELETE_UNIT_TYPE_SUCCESS = "DELETE_UNIT_TYPE_SUCCESS";
export const DELETE_UNIT_TYPE_FAIL = "DELETE_UNIT_TYPE_FAIL";

export function addUnitTypeAction(unit){
    return {
        type: ADD_UNIT_TYPE,
        unit: unit
    }
}

export function listAllUnitTypesAction(){
    return {
        type: GET_ALL_UNIT_TYPES
    }
}

export function updateUnitTypeAction(unit){
    return {
        type: UPDATE_UNIT_TYPE,
        unit: unit
    }
}

export function deleteUnitTypeAction(id){
    return {
        type: DELETE_UNIT_TYPE,
        id: id
    }
}