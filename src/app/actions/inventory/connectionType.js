
export const GET_CONNECTION_TYPES = "GET_CONNECTION_TYPES";
export const GET_CONNECTION_TYPES_SUCCESS = "GET_CONNECTION_TYPES_SUCCESS";
export const GET_CONNECTION_TYPES_FAIL = "GET_CONNECTION_TYPES_FAIL";

export function getConnectionTypesAction(){
    return {
        type: GET_CONNECTION_TYPES
    }
}