export const ADD_STOCK_IN_ITEM = "ADD_STOCK_IN_ITEM";
export const ADD_STOCK_IN_ITEM_SUCCESS = "ADD_STOCK_IN_ITEM_SUCCESS";
export const ADD_STOCK_IN_ITEM_FAIL = "ADD_STOCK_IN_ITEM_FAIL";

export const GET_ALL_STOCK_IN_ITEMS = "GET_ALL_STOCK_IN_ITEMS";
export const GET_ALL_STOCK_IN_ITEMS_SUCCESS = "GET_ALL_STOCK_IN_ITEMS_SUCCESS";
export const GET_ALL_STOCK_IN_ITEMS_FAIL = "GET_ALL_STOCK_IN_ITEMS_FAIL";

export const UPDATE_STOCK_IN_ITEM = "UPDATE_STOCK_IN_ITEM";
export const UPDATE_STOCK_IN_ITEM_SUCCESS = "UPDATE_STOCK_IN_ITEM_SUCCESS";
export const UPDATE_STOCK_IN_ITEM_FAIL = "UPDATE_STOCK_IN_ITEM_FAIL";

export const DELETE_STOCK_IN_ITEM = "DELETE_STOCK_IN_ITEM";
export const DELETE_STOCK_IN_ITEM_SUCCESS = "DELETE_STOCK_IN_ITEM_SUCCESS";
export const DELETE_STOCK_IN_ITEM_FAIL = "DELETE_STOCK_IN_ITEM_FAIL";

export function addStockInItemAction(stockIn){
    return {
        type: ADD_STOCK_IN_ITEM,
        stockIn: stockIn
    }
}

export function getAllStockInItemsAction(stockIn){
    return {
        type: GET_ALL_STOCK_IN_ITEMS,
        stockIn: stockIn
    }
}

export function updateStockInItemAction(stockIn){
    return {
        type: UPDATE_STOCK_IN_ITEM,
        stockIn: stockIn
    }
}

export function deleteStockInItemAction(id){
    return {
        type: DELETE_STOCK_IN_ITEM,
        id: id
    }
}