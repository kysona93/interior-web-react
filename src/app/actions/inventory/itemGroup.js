export const ADD_ITEM_GROUP = "ADD_ITEM_GROUP";
export const ADD_ITEM_GROUP_SUCCESS = "ADD_ITEM_GROUP_SUCCESS";
export const ADD_ITEM_GROUP_FAIL = "ADD_ITEM_GROUP_FAIL";

export const GET_ALL_ITEM_GROUPS = "GET_ALL_ITEM_GROUPS";
export const GET_ALL_ITEM_GROUPS_SUCCESS = "GET_ALL_ITEM_GROUPS_SUCCESS";
export const GET_ALL_ITEM_GROUPS_FAIL = "GET_ALL_ITEM_GROUPS_FAIL";

export const UPDATE_ITEM_GROUP = "UPDATE_ITEM_GROUP";
export const UPDATE_ITEM_GROUP_SUCCESS = "UPDATE_ITEM_GROUP_SUCCESS";
export const UPDATE_ITEM_GROUP_FAIL = "UPDATE_ITEM_GROUP_FAIL";

export const DELETE_ITEM_GROUP = "DELETE_ITEM_GROUP";
export const DELETE_ITEM_GROUP_SUCCESS = "DELETE_ITEM_GROUP_SUCCESS";
export const DELETE_ITEM_GROUP_FAIL = "DELETE_ITEM_GROUP_FAIL";

export const GET_ALL_ITEM_GROUPS_LIST = "GET_ALL_ITEM_GROUPS_LIST";
export const GET_ALL_ITEM_GROUPS_LIST_SUCCESS = "GET_ALL_ITEM_GROUPS_LIST_SUCCESS";
export const GET_ALL_ITEM_GROUPS_LIST_FAIL = "GET_ALL_ITEM_GROUPS_LIST_FAIL";

export function addItemGroupAction(group){
    return {
        type : ADD_ITEM_GROUP,
        group: group
    }
}

export function listAllItemGroupAction(){
    return {
        type: GET_ALL_ITEM_GROUPS
    }
}

export function getItemGroupsListAction(status){
    return {
        type: GET_ALL_ITEM_GROUPS_LIST
    }
}

export function updateItemGroupAction(group){
    return {
        type: UPDATE_ITEM_GROUP,
        group: group
    }
}

export function deleteItemGroupAction(id){
    return {
        type: DELETE_ITEM_GROUP,
        id: id
    }
}