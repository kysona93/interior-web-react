export const GET_TRANSFORMER_REPORTS = "GET_TRANSFORMER_REPORTS";
export const GET_TRANSFORMER_REPORTS_SUCCESS = "GET_TRANSFORMER_REPORTS_SUCCESS";
export const GET_TRANSFORMER_REPORTS_FAIL = "GET_TRANSFORMER_REPORTS_FAIL";

export const GET_POLE_REPORTS = "GET_POLE_REPORTS";
export const GET_POLE_REPORTS_SUCCESS = "GET_POLE_REPORTS_SUCCESS";
export const GET_POLE_REPORTS_FAIL = "GET_POLE_REPORTS_FAIL";

export const getTransformerReportsAction = (transformer) => {
    return {
        type: GET_TRANSFORMER_REPORTS,
        transformer: transformer
    }
};

export const getPoleReportsAction = (pole) => {
    return {
        type: GET_POLE_REPORTS,
        pole: pole
    }
};