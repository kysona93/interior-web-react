export const ADD_GROUP_REPORT = "ADD_GROUP_REPORT";
export const ADD_GROUP_REPORT_SUCCESS = "ADD_GROUP_REPORT_SUCCESS";
export const ADD_GROUP_REPORT_FAIL = "ADD_GROUP_REPORT_FAIL";

export const GET_ALL_GROUP_REPORTS = "GET_ALL_GROUP_REPORTS";
export const GET_ALL_GROUP_REPORTS_SUCCESS = "GET_ALL_GROUP_REPORTS_SUCCESS";
export const GET_ALL_GROUP_REPORTS_FAIL = "GET_ALL_GROUP_REPORTS_FAIL";

export const UPDATE_GROUP_REPORT = "UPDATE_GROUP_REPORT";
export const UPDATE_GROUP_REPORT_SUCCESS = "UPDATE_GROUP_REPORT_SUCCESS";
export const UPDATE_GROUP_REPORT_FAIL = "UPDATE_GROUP_REPORT_FAIL";

export const DELETE_GROUP_REPORT = "DELETE_GROUP_REPORT";
export const DELETE_GROUP_REPORT_SUCCESS = "DELETE_GROUP_REPORT_SUCCESS";
export const DELETE_GROUP_REPORT_FAIL = "DELETE_GROUP_REPORT_FAIL";

export function addGroupReportAction(report){
    return {
        type: ADD_GROUP_REPORT,
        report: report
    }
}

export function listAllGroupReportsAction(status){
    return {
        type: GET_ALL_GROUP_REPORTS,
        status: status
    }
}

export function updateGroupReportAction(report){
    return {
        type: UPDATE_GROUP_REPORT,
        report: report
    }
}

export function deleteGroupReportAction(id){
    return {
        type: DELETE_GROUP_REPORT,
        id: id
    }
}