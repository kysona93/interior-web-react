/* report subsidy page 3 */
export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER";
export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_SUCCESS = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_SUCCESS";
export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_FAIL = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_FAIL";

export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID";
export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SUCCESS = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SUCCESS";
export const GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_FAIL = "GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_FAIL";

/* report subsidy page 2 */
export const GET_SUBSIDY_SALE_LV_0_TO_2000_KW = "GET_SUBSIDY_SALE_LV_0_TO_2000_KW";
export const GET_SUBSIDY_SALE_LV_0_TO_2000_KW_SUCCESS = "GET_SUBSIDY_SALE_LV_0_TO_2000_KW_SUCCESS";
export const GET_SUBSIDY_SALE_LV_0_TO_2000_KW_FAIL = "GET_SUBSIDY_SALE_LV_0_TO_2000_KW_FAIL";

export const GET_SUBSIDY_SALE_LV_0_TO_10_KW = "GET_SUBSIDY_SALE_LV_0_TO_10_KW";
export const GET_SUBSIDY_SALE_LV_0_TO_10_KW_SUCCESS = "GET_SUBSIDY_SALE_LV_0_TO_10_KW_SUCCESS";
export const GET_SUBSIDY_SALE_LV_0_TO_10_KW_FAIL = "GET_SUBSIDY_SALE_LV_0_TO_10_KW_FAIL";

export const GET_SUBSIDY_SALE_LV_11_TO_50_KW = "GET_SUBSIDY_SALE_LV_11_TO_50_KW";
export const GET_SUBSIDY_SALE_LV_11_TO_50_KW_SUCCESS = "GET_SUBSIDY_SALE_LV_11_TO_50_KW_SUCCESS";
export const GET_SUBSIDY_SALE_LV_11_TO_50_KW_FAIL = "GET_SUBSIDY_SALE_LV_11_TO_50_KW_FAIL";

export const GET_SUBSIDY_SALE_LV_51_KW_UP = "GET_SUBSIDY_SALE_LV_51_KW_UP";
export const GET_SUBSIDY_SALE_LV_51_KW_UP_SUCCESS = "GET_SUBSIDY_SALE_LV_51_KW_UP_SUCCESS";
export const GET_SUBSIDY_SALE_LV_51_KW_UP_FAIL = "GET_SUBSIDY_SALE_LV_51_KW_UP_FAIL";


/* report subsidy page 3 */
export function getSubsidyPrepaidAmountSponserAction(subsidy){
    return{
        type: GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER ,
        subsidy:subsidy
    }
}

export function getSubsidyPrepaidAmountAction(subsidy){
    return{
        type: GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID ,
        subsidy:subsidy
    }
}

/* report subsidy page 2 */
export function getReportSubsidySaleLV0to2000kwAction(subsidy){
    return {
        type : GET_SUBSIDY_SALE_LV_0_TO_2000_KW,
        subsidy: subsidy
    }
}

export function getReportSubsidySaleLV0to10kwAction(subsidy){
    return {
        type : GET_SUBSIDY_SALE_LV_0_TO_10_KW,
        subsidy: subsidy
    }
}

export function getReportSubsidySaleLV11to50kwAction(subsidy){
    return {
        type : GET_SUBSIDY_SALE_LV_11_TO_50_KW,
        subsidy: subsidy
    }
}

export function getReportSubsidySaleLV51kwUpAction(subsidy){
    return {
        type : GET_SUBSIDY_SALE_LV_51_KW_UP,
        subsidy: subsidy
    }
}