export const GET_SUBSIDY_SALE_MV_LICENSE = "GET_SUBSIDY_SALE_MV_LICENSE";
export const GET_SUBSIDY_SALE_MV_LICENSE_SUCCESS = "GET_SUBSIDY_SALE_MV_LICENSE_SUCCESS";
export const GET_SUBSIDY_SALE_MV_LICENSE_FAIL = "GET_SUBSIDY_SALE_MV_LICENSE_FAIL";

export const GET_SUBSIDY_SALE_MV = "GET_SUBSIDY_SALE_MV";
export const GET_SUBSIDY_SALE_MV_SUCCESS = "GET_SUBSIDY_SALE_MV_SUCCESS";
export const GET_SUBSIDY_SALE_MV_FAIL = "GET_SUBSIDY_SALE_MV_FAIL";

export const GET_SUBSIDY_SALE_LV_KW_2001_UP = "GET_SUBSIDY_SALE_LV_KW_2001_UP";
export const GET_SUBSIDY_SALE_LV_KW_2001_UP_SUCCESS = "GET_SUBSIDY_SALE_LV_KW_2001_UP_SUCCESS";
export const GET_SUBSIDY_SALE_LV_KW_2001_UP_FAIL = "GET_SUBSIDY_SALE_LV_KW_2001_UP_FAIL";

export function getReportSubsidySaleMVLicenseAction(subsidy){
    return {
        type :GET_SUBSIDY_SALE_MV_LICENSE,
        subsidy: subsidy
    }
}

export function getReportSubsidySaleMVAction(subsidy){
    return {
        type: GET_SUBSIDY_SALE_MV,
        subsidy: subsidy
    }
}

export function getReportSubsidySaleLVKw2001UPAction(subsidy){
    return {
        type: GET_SUBSIDY_SALE_LV_KW_2001_UP,
        subsidy: subsidy
    }
}

