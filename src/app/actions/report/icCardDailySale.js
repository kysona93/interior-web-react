export const GET_IC_CARD_DAILY_SALES_BY_CUS_ID = "GET_IC_CARD_DAILY_SALES_BY_CUS_ID";
export const GET_IC_CARD_DAILY_SALES_BY_CUS_ID_SUCCESS = "GET_IC_CARD_DAILY_SALES_BY_CUS_ID_SUCCESS";
export const GET_IC_CARD_DAILY_SALES_BY_CUS_ID_FAIL = "GET_IC_CARD_DAILY_SALES_BY_CUS_ID_FAIL";

export const GET_IC_CARD_DAILY_SALES_FILTER = "GET_IC_CARD_DAILY_SALES_FILTER";
export const GET_IC_CARD_DAILY_SALES_FILTER_SUCCESS = "GET_IC_CARD_DAILY_SALES_FILTER_SUCCESS";
export const GET_IC_CARD_DAILY_SALES_FILTER_FAIL = "GET_IC_CARD_DAILY_SALES_FILTER_FAIL";

export const GET_PREPAID_POWER_REPORTS = "GET_PREPAID_POWER_REPORTS";
export const GET_PREPAID_POWER_REPORTS_SUCCESS = "GET_PREPAID_POWER_REPORTS_SUCCESS";
export const GET_PREPAID_POWER_REPORTS_FAIL = "GET_PREPAID_POWER_REPORTS_FAIL";

export function getICCardDailySalesByCusIdAction(filter){
    return {
        type: GET_IC_CARD_DAILY_SALES_BY_CUS_ID,
        filter: filter
    }
}

export function getICCardDailySalesFilterAction(filter){
    return {
        type: GET_IC_CARD_DAILY_SALES_FILTER,
        filter: filter
    }
}

export function getPrepaidPowerReportsAction(filter){
    return {
        type: GET_PREPAID_POWER_REPORTS,
        filter: filter
    }
}