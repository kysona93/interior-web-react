export const GET_REPORT_REF_POSTPAID_0_KW = "GET_REPORT_REF_POSTPAID_0_KW";
export const GET_REPORT_REF_POSTPAID_0_KW_SUCCESS = "GET_REPORT_REF_POSTPAID_0_KW_SUCCESS";
export const GET_REPORT_REF_POSTPAID_0_KW_FAIL = "GET_REPORT_REF_POSTPAID_0_KW_FAIL";

export const GET_REPORT_REF_PREPAID_0_KW = "GET_REPORT_REF_PREPAID_0_KW";
export const GET_REPORT_REF_PREPAID_0_KW_SUCCESS = "GET_REPORT_REF_PREPAID_0_KW_SUCCESS";
export const GET_REPORT_REF_PREPAID_0_KW_FAIL = "GET_REPORT_REF_PREPAID_0_KW_FAIL";

export const GET_REPORT_REF_POSTPAID_1_TO_10_KW = "GET_REPORT_REF_POSTPAID_1_TO_10_KW";
export const GET_REPORT_REF_POSTPAID_1_TO_10_KW_SUCCESS = "GET_REPORT_REF_POSTPAID_1_TO_10_KW_SUCCESS";
export const GET_REPORT_REF_POSTPAID_1_TO_10_KW_FAIL = "GET_REPORT_REF_POSTPAID_1_TO_10_KW_FAIL";

export const GET_REPORT_REF_PREPAID_1_TO_10_KW = "GET_REPORT_REF_PREPAID_1_TO_10_KW";
export const GET_REPORT_REF_PREPAID_1_TO_10_KW_SUCCESS = "GET_REPORT_REF_PREPAID_1_TO_10_KW_SUCCESS";
export const GET_REPORT_REF_PREPAID_1_TO_10_KW_FAIL = "GET_REPORT_REF_PREPAID_1_TO_10_KW_FAIL";

export const GET_REPORT_REF_POSTPAID_11_TO_50_KW = "GET_REPORT_REF_POSTPAID_11_TO_50_KW";
export const GET_REPORT_REF_POSTPAID_11_TO_50_KW_SUCCESS = "GET_REPORT_REF_POSTPAID_11_TO_50_KW_SUCCESS";
export const GET_REPORT_REF_POSTPAID_11_TO_50_KW_FAIL = "GET_REPORT_REF_POSTPAID_11_TO_50_KW_FAIL";

export const GET_REPORT_REF_PREPAID_11_TO_50_KW = "GET_REPORT_REF_PREPAID_11_TO_50_KW";
export const GET_REPORT_REF_PREPAID_11_TO_50_KW_SUCCESS = "GET_REPORT_REF_PREPAID_11_TO_50_KW_SUCCESS";
export const GET_REPORT_REF_PREPAID_11_TO_50_KW_FAIL = "GET_REPORT_REF_PREPAID_11_TO_50_KW_FAIL";

export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_SUCCESS = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_SUCCESS";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_FAIL = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_FAIL";

export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_SUCCESS = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_SUCCESS";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_FAIL = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_FAIL";

export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_SUCCESS = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_SUCCESS";
export const GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_FAIL = "GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_FAIL";

export const GET_HISTORY_12_MONTHS ="GET_HISTORY_12_MONTHS";
export const GET_HISTORY_12_MONTHS_SUCCESS ="GET_HISTORY_12_MONTHS_SUCCESS";
export const GET_HISTORY_12_MONTHS_FAIL ="GET_HISTORY_12_MONTHS_FAIL";

export const GET_HISTORY_12_MONTHS_BUSINESS ="GET_HISTORY_12_MONTHS_BUSINESS";
export const GET_HISTORY_12_MONTHS_BUSINESS_SUCCESS ="GET_HISTORY_12_MONTHS_BUSINESS_SUCCESS";
export const GET_HISTORY_12_MONTHS_BUSINESS_FAIL ="GET_HISTORY_12_MONTHS_BUSINESS_FAIL";

export const GET_ADDITION_SUBSIDIES ="GET_ADDITION_SUBSIDIES";
export const GET_ADDITION_SUBSIDIES_SUCCESS ="GET_ADDITION_SUBSIDIES_SUCCESS";
export const GET_ADDITION_SUBSIDIES_FAIL ="GET_ADDITION_SUBSIDIES_FAIL";

export const GET_ADDITION_SUBSIDIES_BUSINESS ="GET_ADDITION_SUBSIDIES_BUSINESS";
export const GET_ADDITION_SUBSIDIES_BUSINESS_SUCCESS ="GET_ADDITION_SUBSIDIES_BUSINESS_SUCCESS";
export const GET_ADDITION_SUBSIDIES_BUSINESS_FAIL ="GET_ADDITION_SUBSIDIES_BUSINESS_FAIL";

/* report ref 0 kw */
export function getAllReportRefPostpaid0KwAction(customer){
    return {
        type: GET_REPORT_REF_POSTPAID_0_KW,
        customer: customer
    }
}
export function getAllReportRefPrepaid0KwAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_0_KW,
        customer: customer
    }
}

/* report ref 1 to 10 kw */
export function getAllReportRefPostpaid1to10KwAction(customer){
    return {
        type: GET_REPORT_REF_POSTPAID_1_TO_10_KW,
        customer: customer
    }
}
export function getAllReportRefPrepaid1to10KwAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_1_TO_10_KW,
        customer: customer
    }
}

/* report ref 11 to 150 kw */
export function getAllReportRefPostpaid11to50KwAction(customer){
    return {
        type: GET_REPORT_REF_POSTPAID_11_TO_50_KW,
        customer: customer
    }
}
export function getAllReportRefPrepaid11to50KwAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_11_TO_50_KW,
        customer: customer
    }
}
/* report ref greater than 50 kw not sponsor */
export function getAllReportRefPrepaidGreaterThan50KwNotSponsorAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR,
        customer: customer
    }
}
/* report ref greater than 50 kw sponsor */
export function getAllReportRefPrepaidGreaterThan50KwSponsorAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR,
        customer: customer
    }
}
/* report ref greater than 50 kw residential */
export function getAllReportRefPrepaidGreaterThan50KwResidentialAction(customer){
    return {
        type: GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL,
        customer: customer
    }
}

export const getHistory12MonthsAction = (report) => {
    return{
        type: GET_HISTORY_12_MONTHS,
        report: report
    }
};

export const getHistory12MonthsBusinessAction = (report) => {
    return{
        type: GET_HISTORY_12_MONTHS_BUSINESS,
        report: report
    }
};

export const getAdditionSubsidiesAction = (report) => {
    return{
        type: GET_ADDITION_SUBSIDIES,
        report: report
    }
};

export const getAdditionSubsidiesBusinessAction = (report) => {
    return{
        type: GET_ADDITION_SUBSIDIES_BUSINESS,
        report: report
    }
};