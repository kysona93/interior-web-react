export const ADD_LOST_KWH = "ADD_LOST_KWH";
export const ADD_LOST_KWH_SUCCESS = "ADD_LOST_KWH_SUCCESS";
export const ADD_LOST_KWH_FAIL = "ADD_LOST_KWH_FAIL";

export const GET_ALL_LOST_KWH = "GET_ALL_LOST_KWH";
export const GET_ALL_LOST_KWH_SUCCESS = "GET_ALL_LOST_KWH_SUCCESS";
export const GET_ALL_LOST_KWH_FAIL = "GET_ALL_LOST_KWH_FAIL";

export const GET_ALL_METER_IDS = "GET_ALL_METER_IDS";
export const GET_ALL_METER_IDS_SUCCESS = "GET_ALL_METER_IDS_SUCCESS";
export const GET_ALL_METER_IDS_FAIL = "GET_ALL_METER_IDS_FAIL";

export const GET_ALL_LOST_KWH_STATUS = "GET_ALL_LOST_KWH_STATUS";
export const GET_ALL_LOST_KWH_STATUS_SUCCESS = "GET_ALL_LOST_KWH_STATUS_SUCCESS";
export const GET_ALL_LOST_KWH_STATUS_FAIL = "GET_ALL_LOST_KWH_STATUS_FAIL";

export function addLostKwhAction(kwh){
    return {
        type: ADD_LOST_KWH,
        kwh: kwh
    }
}

export function getAllLostKwhAction(kwh){
    return {
        type: GET_ALL_LOST_KWH,
        kwh: kwh
    }
}

export function getAllMeterIdsAction(){
    return {
        type: GET_ALL_METER_IDS
    }
}

export function getAllLostKwhStatusAction(){
    return {
        type: GET_ALL_LOST_KWH_STATUS
    }
}
