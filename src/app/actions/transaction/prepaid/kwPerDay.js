export const GET_ALL_GENERATE_KW_PER_DAY = "GET_ALL_GENERATE_KW_PER_DAY";
export const GET_ALL_GENERATE_KW_PER_DAY_SUCCESS = "GET_ALL_GENERATE_KW_PER_DAY_SUCCESS";
export const GET_ALL_GENERATE_KW_PER_DAY_FAIL = "GET_ALL_GENERATE_KW_PER_DAY_FAIL";

export const GENERATE_KW_PER_DAY = "GENERATE_KW_PER_DAY";
export const GENERATE_KW_PER_DAY_SUCCESS = "GENERATE_KW_PER_DAY_SUCCESS";
export const GENERATE_KW_PER_DAY_FAIL = "GENERATE_KW_PER_DAY_FAIL";

export function getAllGenerateKwPerDayAction(prepaid){
    return {
        type : GET_ALL_GENERATE_KW_PER_DAY,
        prepaid: prepaid
    }
}

export function generateKwPerDayAction(kw){
    return {
        type: GENERATE_KW_PER_DAY,
        kw : kw
    }
}