export const ISSUE_BILL_PREPAID = "ISSUE_BILL_PREPAID";
export const ISSUE_BILL_PREPAID_SUCCESS = "ISSUE_BILL_PREPAID_SUCCESS";
export const ISSUE_BILL_PREPAID_FAIL = "ISSUE_BILL_PREPAID_FAIL";

export const GET_ALL_RETURN_INVOICES = "GET_ALL_RETURN_INVOICES";
export const GET_ALL_RETURN_INVOICES_SUCCESS = "GET_ALL_RETURN_INVOICES_SUCCESS";
export const GET_ALL_RETURN_INVOICES_FAIL = "GET_ALL_RETURN_INVOICES_FAIL";

export const GENERATE_RETURN_INVOICES = "GENERATE_RETURN_INVOICES";
export const GENERATE_RETURN_INVOICES_SUCCESS = "GENERATE_RETURN_INVOICES_SUCCESS";
export const GENERATE_RETURN_INVOICES_FAIL = "GENERATE_RETURN_INVOICES_FAIL";

export const GET_RETURN_INVOICE_BY_PREPAID_ID = "GET_RETURN_INVOICE_BY_PREPAID_ID";
export const GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS = "GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS";
export const GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL = "GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL";

export const UPDATE_RETURN_INVOICE_BY_PREPAID_ID = "UPDATE_RETURN_INVOICE_BY_PREPAID_ID";
export const UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS = "UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS";
export const UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL = "UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL";

export function issueBillPrepaidAction(prepaid){
    return {
        type: ISSUE_BILL_PREPAID,
        prepaid: prepaid
    }
}

export function getAllReturnInvoicesAction(invoice){
    return {
        type : GET_ALL_RETURN_INVOICES,
        invoice: invoice
    }
}

export function generateAllReturnInvoicesAction(invoices){
    return {
        type: GENERATE_RETURN_INVOICES,
        invoices: invoices
    }
}

export function getReturnInvoiceByPrepaidIdAction(prepaidId){
    return {
        type: GET_RETURN_INVOICE_BY_PREPAID_ID,
        prepaidId: prepaidId
    }
}

export function updateReturnInvoiceByPrepaidIdAction(prepaidId){
    return {
        type: UPDATE_RETURN_INVOICE_BY_PREPAID_ID,
        prepaidId: prepaidId
    }
}