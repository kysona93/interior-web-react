export const ADD_BALANCE_PREPAID_KWH = "ADD_BALANCE_PREPAID_KWH";
export const ADD_BALANCE_PREPAID_KWH_SUCCESS = "ADD_BALANCE_PREPAID_KWH_SUCCESS";
export const ADD_BALANCE_PREPAID_KWH_FAIL = "ADD_BALANCE_PREPAID_KWH_FAIL";

export const GET_ALL_BALANCE_PREPAID_KWH = "GET_ALL_BALANCE_PREPAID_KWH";
export const GET_ALL_BALANCE_PREPAID_KWH_SUCCESS = "GET_ALL_BALANCE_PREPAID_KWH_SUCCESS";
export const GET_ALL_BALANCE_PREPAID_KWH_FAIL = "GET_ALL_BALANCE_PREPAID_KWH_FAIL";

export const GET_ALL_BILL_BALANCE_PREPAID = "GET_ALL_BILL_BALANCE_PREPAID";
export const GET_ALL_BILL_BALANCE_PREPAID_SUCCESS = "GET_ALL_BILL_BALANCE_PREPAID_SUCCESS";
export const GET_ALL_BILL_BALANCE_PREPAID_FAIL = "GET_ALL_BILL_BALANCE_PREPAID_FAIL";

export const VOID_BALANCE_PREPAID_KW = "VOID_BALANCE_PREPAID_KW";
export const VOID_BALANCE_PREPAID_KW_SUCCESS = "VOID_BALANCE_PREPAID_KW_SUCCESS";
export const VOID_BALANCE_PREPAID_KW_FAIL = "VOID_BALANCE_PREPAID_KW_FAIL";

export const GET_ALL_NEW_PREPAID_CUSTOMERS = "GET_ALL_NEW_PREPAID_CUSTOMERS";
export const GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS = "GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS";
export const GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL = "GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL";

export const SAVE_BALANCE_PREPAID_KWH = "SAVE_BALANCE_PREPAID_KWH";
export const SAVE_BALANCE_PREPAID_KWH_SUCCESS = "SAVE_BALANCE_PREPAID_KWH_SUCCESS";
export const SAVE_BALANCE_PREPAID_KWH_FAIL = "SAVE_BALANCE_PREPAID_KWH_FAIL";

export const LIST_BALANCE_PREPAID_KW = "LIST_BALANCE_PREPAID_KW";
export const LIST_BALANCE_PREPAID_KW_SUCCESS = "LIST_BALANCE_PREPAID_KW_SUCCESS";
export const LIST_BALANCE_PREPAID_KW_FAIL = "LIST_BALANCE_PREPAID_KW_FAIL";

export const UPDATE_BALANCE_PREPAID_KW = "UPDATE_BALANCE_PREPAID_KW";
export const UPDATE_BALANCE_PREPAID_KW_SUCCESS = "UPDATE_BALANCE_PREPAID_KW_SUCCESS";
export const UPDATE_BALANCE_PREPAID_KW_FAIL = "UPDATE_BALANCE_PREPAID_KW_FAIL";

export function addBalancePrepaidKwhAction(balance){
    return {
        type : ADD_BALANCE_PREPAID_KWH,
        balance: balance
    }
}

export function getAllBalancePrepaidKwhAction(balance){
    return {
        type: GET_ALL_BALANCE_PREPAID_KWH,
        balance: balance
    }
}

export function getAllBillBalancePrepaidAction(){
    return {
        type: GET_ALL_BILL_BALANCE_PREPAID
    }
}

export function voidBalancePrepaidKwAction(id){
    return {
        type: VOID_BALANCE_PREPAID_KW,
        id: id
    }
}

export function getAllNewPrepaidCustomersAction(){
    return {
        type: GET_ALL_NEW_PREPAID_CUSTOMERS
    }
}

export function saveBalancePrepaidKwhAction(balance){
    return {
        type: SAVE_BALANCE_PREPAID_KWH,
        balance: balance
    }
}

export function listBalancePrepaidKwhAction(balance){
    return {
        type: LIST_BALANCE_PREPAID_KW,
        balance: balance
    }
}

export function updateBalancePrepaidKwhAction(balance){
    return {
        type: UPDATE_BALANCE_PREPAID_KW,
        balance: balance
    }
}