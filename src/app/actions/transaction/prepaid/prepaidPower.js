export const GET_PREPAID_POWERS = "GET_PREPAID_POWERS";
export const GET_PREPAID_POWERS_SUCCESS = "GET_PREPAID_POWERS_SUCCESS";
export const GET_PREPAID_POWERS_FAIL = "GET_PREPAID_POWERS_FAIL";

export const ADD_PREPAID_POWER = "ADD_PREPAID_POWER";
export const ADD_PREPAID_POWER_SUCCESS = "ADD_PREPAID_POWER_SUCCESS";
export const ADD_PREPAID_POWER_FAIL = "ADD_PREPAID_POWER_FAIL";

export const GET_ALL_PREPAID_POWERS = "GET_ALL_PREPAID_POWERS";
export const GET_ALL_PREPAID_POWERS_SUCCESS = "GET_ALL_PREPAID_POWERS_SUCCESS";
export const GET_ALL_PREPAID_POWERS_FAIL = "GET_ALL_PREPAID_POWERS_FAIL";

export const GET_ALL_PREPAID_END_PURCHASES = "GET_ALL_PREPAID_END_PURCHASES";
export const GET_ALL_PREPAID_END_PURCHASES_SUCCESS = "GET_ALL_PREPAID_END_PURCHASES_SUCCESS";
export const GET_ALL_PREPAID_END_PURCHASES_FAIL = "GET_ALL_PREPAID_END_PURCHASES_FAIL";

export function getPrepaidPowersAction(prepaid){
    return {
        type: GET_PREPAID_POWERS,
        prepaid: prepaid
    }
}

export function addPrepaidPowerAction(prepaid){
    return {
        type: ADD_PREPAID_POWER,
        prepaid: prepaid
    }
}

/* to generate into table balance kwh */
export function getAllPrepaidPowerAction(prepaid){
    return {
        type: GET_ALL_PREPAID_POWERS,
        prepaid: prepaid
    }
}

export function getAllPrepaidEndPurchasesAction(prepaid){
    return {
        type : GET_ALL_PREPAID_END_PURCHASES,
        prepaid: prepaid
    }
}

