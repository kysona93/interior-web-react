export const ADD_PUNISH_RATE = "ADD_PUNISH_RATE";
export const ADD_PUNISH_RATE_SUCCESS = "ADD_PUNISH_RATE_SUCCESS";
export const ADD_PUNISH_RATE_FAIL = "ADD_PUNISH_RATE_FAIL";

export const GET_ALL_PUNISH_RATE = "GET_ALL_PUNISH_RATE";
export const GET_ALL_PUNISH_RATE_SUCCESS = "GET_ALL_PUNISH_RATE_SUCCESS";
export const GET_ALL_PUNISH_RATE_FAIL = "GET_ALL_PUNISH_RATE_FAIL";

export const UPDATE_PUNISH_RATE = "UPDATE_PUNISH_RATE";
export const UPDATE_PUNISH_RATE_SUCCESS = "UPDATE_PUNISH_RATE_SUCCESS";
export const UPDATE_PUNISH_RATE_FAIL = "UPDATE_PUNISH_RATE_FAIL";

export const DELETE_PUNISH_RATE = "DELETE_PUNISH_RATE";
export const DELETE_PUNISH_RATE_SUCCESS = "DELETE_PUNISH_RATE_SUCCESS";
export const DELETE_PUNISH_RATE_FAIL = "DELETE_PUNISH_RATE_FAIL";

export function addPunishRateAction(punish){
    return {
        type: ADD_PUNISH_RATE,
        punish: punish
    }
}

export function getAllPunishRatesAction(){
    return {
        type: GET_ALL_PUNISH_RATE
    }
}

export function updatePunishRateAction(punish){
    return {
        type: UPDATE_PUNISH_RATE,
        punish: punish
    }
}

export function deletePunishRateAction(id){
    return {
        type: DELETE_PUNISH_RATE,
        id: id
    }
}