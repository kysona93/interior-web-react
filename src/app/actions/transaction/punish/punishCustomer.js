export const GET_ALL_PUNISH_INVOICES = "GET_ALL_PUNISH_INVOICES";
export const GET_ALL_PUNISH_INVOICES_SUCCESS = "GET_ALL_PUNISH_INVOICES_SUCCESS";
export const GET_ALL_PUNISH_INVOICES_FAIL = "GET_ALL_PUNISH_INVOICES_FAIL";

export const ADD_PUNISH_CUSTOMERS = "ADD_PUNISH_CUSTOMERS";
export const ADD_PUNISH_CUSTOMERS_SUCCESS = "ADD_PUNISH_CUSTOMERS_SUCCESS";
export const ADD_PUNISH_CUSTOMERS_FAIL = "ADD_PUNISH_CUSTOMERS_FAIL";

export const GET_ALL_PUNISH_CUSTOMERS = "GET_ALL_PUNISH_CUSTOMERS";
export const GET_ALL_PUNISH_CUSTOMERS_SUCCESS = "GET_ALL_PUNISH_CUSTOMERS_SUCCESS";
export const GET_ALL_PUNISH_CUSTOMERS_FAIL = "GET_ALL_PUNISH_CUSTOMERS_FAIL";

export const UPDATE_PUNISH_CUSTOMER = "UPDATE_PUNISH_CUSTOMER";
export const UPDATE_PUNISH_CUSTOMER_SUCCESS = "UPDATE_PUNISH_CUSTOMER_SUCCESS";
export const UPDATE_PUNISH_CUSTOMER_FAIL = "UPDATE_PUNISH_CUSTOMER_FAIL";

export const DELETE_PUNISH_CUSTOMER = "DELETE_PUNISH_CUSTOMER";
export const DELETE_PUNISH_CUSTOMER_SUCCESS = "DELETE_PUNISH_CUSTOMER_SUCCESS";
export const DELETE_PUNISH_CUSTOMER_FAIL = "DELETE_PUNISH_CUSTOMER_FAIL";

export function getAllInvoicesPunishAction(date){
    return {
        type: GET_ALL_PUNISH_INVOICES,
        date: date
    }
}

export function addCustomerPunishAction(customer){
    return {
        type: ADD_PUNISH_CUSTOMERS,
        customer: customer
    }
}

export function getAllCustomersPunishAction(customer){
    return {
        type: GET_ALL_PUNISH_CUSTOMERS,
        customer: customer
    }
}

export function updateCustomerPunishAction(customer){
    return {
        type: UPDATE_PUNISH_CUSTOMER,
        customer: customer
    }
}

export function deleteCustomerPunishAction(id){
    return {
        type: DELETE_PUNISH_CUSTOMER,
        id: id
    }
}
