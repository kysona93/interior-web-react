export const GET_ALL_RECORD_KWH_MONTHLY = "GET_ALL_RECORD_KWH_MONTHLY";
export const GET_ALL_RECORD_KWH_MONTHLY_SUCCESS = "GET_ALL_RECORD_KWH_MONTHLY_SUCCESS";
export const GET_ALL_RECORD_KWH_MONTHLY_FAIL = "GET_ALL_RECORD_KWH_MONTHLY_FAIL";

export function getAllRecordKhwMonthlyAction(record){
    return {
        type: GET_ALL_RECORD_KWH_MONTHLY,
        record: record
    }
}