export const GET_ALL_INVOICES_CUSTOMER_PAYMENT = "GET_ALL_INVOICES_CUSTOMER_PAYMENT";
export const GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS = "GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS";
export const GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL = "GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL";

export const ADD_QUICK_PAID_PAYMENT = "ADD_QUICK_PAID_PAYMENT";
export const ADD_QUICK_PAID_PAYMENT_SUCCESS = "ADD_QUICK_PAID_PAYMENT_SUCCESS";
export const ADD_QUICK_PAID_PAYMENT_FAIL = "ADD_QUICK_PAID_PAYMENT_FAIL";

export const GET_INVOICE_BY_BARCODE = "GET_INVOICE_BY_BARCODE";
export const GET_INVOICE_BY_BARCODE_SUCCESS = "GET_INVOICE_BY_BARCODE_SUCCESS";
export const GET_INVOICE_BY_BARCODE_FAIL = "GET_INVOICE_BY_BARCODE_FAIL";

export function getAllInvoicesCustomerPaymentAction(invoice){
    return {
        type: GET_ALL_INVOICES_CUSTOMER_PAYMENT,
        invoice: invoice
    }
}

export function addQuickPaidPaymentAction(payment){
    return {
        type: ADD_QUICK_PAID_PAYMENT,
        payment: payment
    }
}

export function getInvoiceByBarcodeAction(barcode){
    return {
        type :GET_INVOICE_BY_BARCODE,
        barcode: barcode
    }
}