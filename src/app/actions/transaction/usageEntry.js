export const ADD_METER_USAGE_RECORD = "ADD_METER_USAGE_RECORD";
export const ADD_METER_USAGE_RECORD_SUCCESS = "ADD_METER_USAGE_RECORD_SUCCESS";
export const ADD_METER_USAGE_RECORD_FAIL = "ADD_METER_USAGE_RECORD_FAIL";

export const IMPORT_EXCEL_METER_USAGE_RECORD = "IMPORT_EXCEL_METER_USAGE_RECORD";
export const IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS = "IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS";
export const IMPORT_EXCEL_METER_USAGE_RECORD_FAIL = "IMPORT_EXCEL_METER_USAGE_RECORD_FAIL";

export const GET_CUSTOMER_INFO_BY_METER_SERIAL = "GET_CUSTOMER_INFO_BY_METER_SERIAL";
export const GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS = "GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS";
export const GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL = "GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL";

export const GET_METERS_BY_CUSTOMER_ID = "GET_METERS_BY_CUSTOMER_ID";
export const GET_METERS_BY_CUSTOMER_ID_SUCCESS = "GET_METERS_BY_CUSTOMER_ID_SUCCESS";
export const GET_METERS_BY_CUSTOMER_ID_FAIL = "GET_METERS_BY_CUSTOMER_ID_FAIL";

export const GET_METER_USAGE_BY_METER_SERIAL = "GET_METER_USAGE_BY_METER_SERIAL";
export const GET_METER_USAGE_BY_METER_SERIAL_SUCCESS = "GET_METER_USAGE_BY_METER_SERIAL_SUCCESS";
export const GET_METER_USAGE_BY_METER_SERIAL_FAIL = "GET_METER_USAGE_BY_METER_SERIAL_FAIL";

export const UPDATE_METER_USAGE_BY_ID = "UPDATE_METER_USAGE_BY_ID";
export const UPDATE_METER_USAGE_BY_ID_SUCCESS = "UPDATE_METER_USAGE_BY_ID_SUCCESS";
export const UPDATE_METER_USAGE_BY_ID_FAIL = "UPDATE_METER_USAGE_BY_ID_FAIL";

export const VERIFY_METER_USAGE_INDIVIDUAL = "VERIFY_METER_USAGE_INDIVIDUAL";
export const VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS = "VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS";
export const VERIFY_METER_USAGE_INDIVIDUAL_FAIL = "VERIFY_METER_USAGE_INDIVIDUAL_FAIL";

export const GET_ALL_CUSTOMER_IDS_AND_SERIALS = "GET_ALL_CUSTOMER_IDS_AND_SERIALS";
export const GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS = "GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS";
export const GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL = "GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL";

export const GET_METER_USAGE_PROBLEMS = "GET_METER_USAGE_PROBLEMS";
export const GET_METER_USAGE_PROBLEMS_SUCCESS = "GET_METER_USAGE_PROBLEMS_SUCCESS";
export const GET_METER_USAGE_PROBLEMS_FAIL = "GET_METER_USAGE_PROBLEMS_FAIL";

export function addMeterUsageRecordAction(usage){
    return {
        type: ADD_METER_USAGE_RECORD,
        usage: usage
    }
}

export function importExcelMeterUsageRecordAction(fileExcel){
    return {
        type: IMPORT_EXCEL_METER_USAGE_RECORD,
        fileExcel: fileExcel
    }
}

export function getCustomerInfoByMeterSerialAction(serial){
    return {
        type: GET_CUSTOMER_INFO_BY_METER_SERIAL,
        serial: serial
    }
}

export function getMetersByCustomerIdAction(customerId){
    return {
        type: GET_METERS_BY_CUSTOMER_ID,
        customerId: customerId
    }
}

export function getMeterUsageByMeterSerial(serial){
    return {
        type: GET_METER_USAGE_BY_METER_SERIAL,
        serial: serial
    }
}

export function updateMeterUsageByIdAction(usage){
    return {
        type: UPDATE_METER_USAGE_BY_ID,
        usage: usage
    }
}

export function verifyMeterUsageIndividualAction(usage){
    return {
        type: VERIFY_METER_USAGE_INDIVIDUAL,
        usage: usage
    }
}

export function getAllCustomerIdsAndMeterSerialAction(){
    return {
        type: GET_ALL_CUSTOMER_IDS_AND_SERIALS
    }
}

export function getMeterUsageProblemsAction(){
    return {
        type: GET_METER_USAGE_PROBLEMS
    }
}