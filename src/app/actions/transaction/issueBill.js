export const GET_ALL_CUSTOMER_BILLS = "GET_ALL_CUSTOMER_BILLS";
export const GET_ALL_CUSTOMER_BILLS_SUCCESS = "GET_ALL_CUSTOMER_BILLS_SUCCESS";
export const GET_ALL_CUSTOMER_BILLS_FAIL = "GET_ALL_CUSTOMER_BILLS_FAIL";

export const ISSUE_BILL_CUSTOMERS = "ISSUE_BILL_CUSTOMERS";
export const ISSUE_BILL_CUSTOMERS_SUCCESS = "ISSUE_BILL_CUSTOMERS_SUCCESS";
export const ISSUE_BILL_CUSTOMERS_FAIL = "ISSUE_BILL_CUSTOMERS_FAIL";

export const GET_ALL_CUSTOMERS_PROBLEM = "GET_ALL_CUSTOMERS_PROBLEM";
export const GET_ALL_CUSTOMERS_PROBLEM_SUCCESS = "GET_ALL_CUSTOMERS_PROBLEM_SUCCESS";
export const GET_ALL_CUSTOMERS_PROBLEM_FAIL = "GET_ALL_CUSTOMERS_PROBLEM_FAIL";

export function getAllCustomerBillsAction(problem){
    return {
        type: GET_ALL_CUSTOMER_BILLS,
        problem: problem
    }
}

export function issueBillCustomerAction(customers){
    return {
        type: ISSUE_BILL_CUSTOMERS,
        customers: customers
    }
}

export function getAllCustomersProblemAction(problem){
    return {
        type: GET_ALL_CUSTOMERS_PROBLEM,
        problem: problem
    }
}