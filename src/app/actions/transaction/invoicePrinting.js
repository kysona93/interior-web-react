export const GET_ALL_ISSUE_INVOICES = "GET_ALL_ISSUE_INVOICES";
export const GET_ALL_ISSUE_INVOICES_SUCCESS = "GET_ALL_ISSUE_INVOICES_SUCCESS";
export const GET_ALL_ISSUE_INVOICES_FAIL = "GET_ALL_ISSUE_INVOICES_FAIL";

export const GENERATE_ALL_INVOICE = "GENERATE_ALL_INVOICE";
export const GENERATE_ALL_INVOICE_SUCCESS = "GENERATE_ALL_INVOICE_SUCCESS";
export const GENERATE_ALL_INVOICE_FAIL = "GENERATE_ALL_INVOICE_FAIL";

export function getAllIssueInvoicesAction(invoice){
    return {
        type: GET_ALL_ISSUE_INVOICES,
        invoice: invoice
    }
}

export function generateAllInvoiceAction(invoices){
    return {
        type: GENERATE_ALL_INVOICE,
        invoices: invoices
    }
}