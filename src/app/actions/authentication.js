export const USER_LOGIN = "USER_LOGIN";
export const USER_LOGIN_SUCCESS = "USER_LOGIN_SUCCESS";
export const USER_LOGIN_FAIL = "USER_LOGIN_FAIL";

export const ADD_USER = "ADD_USER";
export const ADD_USER_SUCCESS = "ADD_USER_SUCCESS";
export const ADD_USER_FAIL = "ADD_USER_FAIL";

export const GET_USERS_STATUS = "GET_USERS_STATUS";
export const GET_USERS_STATUS_SUCCESS = "GET_USERS_STATUS_SUCCESS";
export const GET_USERS_STATUS_FAIL  = "GET_USERS_STATUS_FAIL";

export const GET_USER = "GET_USER";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAIL = "GET_USER_FAIL";

export const UPDATE_USER = "UPDATE_USER";
export const UPDATE_USER_SUCCESS = "UPDATE_USER_SUCCESS";
export const UPDATE_USER_FAIL = "UPDATE_USER_FAIL";

export const DELETE_USER = "DELETE_USER";
export const DELETE_USER_SUCCESS = "DELETE_USER_SUCCESS";
export const DELETE_USER_FAIL = "DELETE_USER_FAIL";

export const ADD_GROUP = "ADD_GROUP";
export const ADD_GROUP_SUCCESS = "ADD_GROUP_SUCCESS";
export const ADD_GROUP_FAIL = "ADD_GROUP_FAIL";

export const GET_GROUPS_STATUS = "GET_GROUPS_STATUS";
export const GET_GROUPS_STATUS_SUCCESS = "GET_GROUPS_STATUS_SUCCESS";
export const GET_GROUPS_STATUS_FAIL = "GET_GROUPS_STATUS_FAIL";

export const GET_GROUP = "GET_GROUP";
export const GET_GROUP_SUCCESS = "GET_GROUP_SUCCESS";
export const GET_GROUP_FAIL = "GET_GROUP_FAIL";

export const UPDATE_GROUP = "UPDATE_GROUP";
export const UPDATE_GROUP_SUCCESS = "UPDATE_GROUP_SUCCESS";
export const UPDATE_GROUP_FAIL = "UPDATE_GROUP_FAIL";

export const ENABLED_GROUP = "ENABLED_GROUP";
export const ENABLED_GROUP_SUCCESS = "ENABLED_GROUP_SUCCESS";
export const ENABLED_GROUP_FAIL = "ENABLED_GROUP_FAIL";

/* USER LOGIN */
export function userLoginAction(user){
    return {
        type: USER_LOGIN,
        user: user
    }
}

/* ADD USER */
export function addUserAction(user){
    return {
        type: ADD_USER,
        user: user
    }
}

/* LIST USERS */
export function getUsersStatusAction(status){
    return {
        type: GET_USERS_STATUS,
        status: status
    }
}

/* GET USER */
export function getUserAction(id){
    return {
        type: GET_USER,
        id: id
    }
}

/* UPDATE USER */
export function updateUserAction(user){
    return {
        type: UPDATE_USER,
        user: user
    }
}

/* DELETE USER */
export function deleteUserAction(id){
    return {
        type: DELETE_USER,
        id: id
    }
}

/* ADD GROUP */
export function addGroupAction(group){
    return {
        type : ADD_GROUP,
        group: group
    }
}

/* LIST GROUPS */
export function getGroupsStatusAction(status){
    return {
        type: GET_GROUPS_STATUS,
        status: status
    }
}

/* GET GROUP */
export function getGroupAction(id){
    return {
        type: GET_GROUP,
        id: id
    }
}

/* UPDATE GROUP */
export function updateGroupAction(group){
    return {
        type: UPDATE_GROUP,
        group: group
    }
}

/** ENABLED GROUP */
export function enabledGroupAction(id){
    return {
        type: ENABLED_GROUP,
        id: id
    }
}
