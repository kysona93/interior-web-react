export const ADD_PAYMENT_LOCATION = "ADD_PAYMENT_LOCATION";
export const ADD_PAYMENT_LOCATION_SUCCESS = "ADD_PAYMENT_LOCATION_SUCCESS";
export const ADD_PAYMENT_LOCATION_FAIL = "ADD_PAYMENT_LOCATION_FAIL";

export const GET_ALL_PAYMENT_LOCATION = "GET_ALL_PAYMENT_LOCATION";
export const GET_ALL_PAYMENT_LOCATION_SUCCESS = "GET_ALL_PAYMENT_LOCATION_SUCCESS";
export const GET_ALL_PAYMENT_LOCATION_FAIL = "GET_ALL_PAYMENT_LOCATION_FAIL";

export const UPDATE_PAYMENT_LOCATION = "UPDATE_PAYMENT_LOCATION";
export const UPDATE_PAYMENT_LOCATION_SUCCESS = "UPDATE_PAYMENT_LOCATION_SUCCESS";
export const UPDATE_PAYMENT_LOCATION_FAIL = "UPDATE_PAYMENT_LOCATION_FAIL";

export const DELETE_PAYMENT_LOCATION = "DELETE_PAYMENT_LOCATION";
export const DELETE_PAYMENT_LOCATION_SUCCESS = "DELETE_PAYMENT_LOCATION_SUCCESS";
export const DELETE_PAYMENT_LOCATION_FAIL = "DELETE_PAYMENT_LOCATION_FAIL";

/* ADD PAYMENT LOCATION */
export function addPaymentLocationAction(payment){
    return {
        type: ADD_PAYMENT_LOCATION,
        payment: payment
    }
}

/* GET ALL PAYMENT LOCATION */
export function getAllPaymentTypesAction(){
    return {
        type: GET_ALL_PAYMENT_LOCATION
    }
}

/* UPDATE PAYMENT LOCATION */
export function updatePaymentLocationAction(payment){
    return {
        type: UPDATE_PAYMENT_LOCATION,
        payment: payment
    }
}

/* DELETE PAYMENT LOCATION */
export function deletePaymentLocationAction(id){
    return {
        type: DELETE_PAYMENT_LOCATION,
        id: id
    }
}