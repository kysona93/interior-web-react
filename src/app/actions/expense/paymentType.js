export const ADD_PAYMENT_TYPE = "ADD_PAYMENT_TYPE";
export const ADD_PAYMENT_TYPE_SUCCESS = "ADD_PAYMENT_TYPE_SUCCESS";
export const ADD_PAYMENT_TYPE_FAIL = "ADD_PAYMENT_TYPE_FAIL";

export const GET_ALL_PAYMENT_TYPE = "GET_ALL_PAYMENT_TYPE";
export const GET_ALL_PAYMENT_TYPE_SUCCESS = "GET_ALL_PAYMENT_TYPE_SUCCESS";
export const GET_ALL_PAYMENT_TYPE_FAIL = "GET_ALL_PAYMENT_TYPE_FAIL";

export const UPDATE_PAYMENT_TYPE = "UPDATE_PAYMENT_TYPE";
export const UPDATE_PAYMENT_TYPE_SUCCESS = "UPDATE_PAYMENT_TYPE_SUCCESS";
export const UPDATE_PAYMENT_TYPE_FAIL = "UPDATE_PAYMENT_TYPE_FAIL";

export const DELETE_PAYMENT_TYPE = "DELETE_PAYMENT_TYPE";
export const DELETE_PAYMENT_TYPE_SUCCESS = "DELETE_PAYMENT_TYPE_SUCCESS";
export const DELETE_PAYMENT_TYPE_FAIL = "DELETE_PAYMENT_TYPE_FAIL";

/* ADD PAYMENT TYPE */
export function addPaymentTypeAction(payment){
    return {
        type: ADD_PAYMENT_TYPE,
        payment: payment
    }
}

/* GET ALL PAYMENT TYPE */
export function getAllPaymentTypesAction(){
    return {
        type: GET_ALL_PAYMENT_TYPE
    }
}

/* UPDATE PAYMENT TYPE */
export function updatePaymentTypeAction(payment){
    return {
        type: UPDATE_PAYMENT_TYPE,
        payment: payment
    }
}

/* DELETE PAYMENT TYPE */
export function deletePaymentTypeAction(id){
    return {
        type: DELETE_PAYMENT_TYPE,
        id: id
    }
}