export const ADD_CUSTOMER = "ADD_CUSTOMER";
export const ADD_CUSTOMER_SUCCESS = "ADD_CUSTOMER_SUCCESS";
export const ADD_CUSTOMER_FAIL = "ADD_CUSTOMER_FAIL";

export const UPDATE_CUSTOMER = "UPDATE_CUSTOMER";
export const UPDATE_CUSTOMER_SUCCESS = "UPDATE_CUSTOMER_SUCCESS";
export const UPDATE_CUSTOMER_FAIL = "UPDATE_CUSTOMER_FAIL";

export const DELETE_CUSTOMER = "DELETE_CUSTOMER";
export const DELETE_CUSTOMER_SUCCESS = "DELETE_CUSTOMER_SUCCESS";
export const DELETE_CUSTOMER_FAIL = "DELETE_CUSTOMER_FAIL";

export const GET_CUSTOMER = "GET_CUSTOMER";
export const GET_CUSTOMER_SUCCESS = "GET_CUSTOMER_SUCCESS";
export const GET_CUSTOMER_FAIL = "GET_CUSTOMER_FAIL";

export const GET_CUSTOMERS = "GET_CUSTOMERS";
export const GET_CUSTOMERS_SUCCESS = "GET_CUSTOMERS_SUCCESS";
export const GET_CUSTOMERS_FAIL = "GET_CUSTOMERS_FAIL";

export const GET_ALL_CUSTOMERS = "GET_ALL_CUSTOMERS";
export const GET_ALL_CUSTOMERS_SUCCESS = "GET_ALL_CUSTOMERS_SUCCESS";
export const GET_ALL_CUSTOMERS_FAIL = "GET_ALL_CUSTOMERS_FAIL";

export const addCustomerAction = (customer) => {
    return {
        type : ADD_CUSTOMER,
        customer: customer
    }
};

export const updateCustomerAction = (customer) => {
    return {
        type : UPDATE_CUSTOMER,
        customer: customer
    }
};

export const deleteCustomerAction = (id) => {
    return {
        type : DELETE_CUSTOMER,
        id: id
    }
};

export const getCustomerAction = (id) => {
    return {
        type : GET_CUSTOMER,
        id: id
    }
};

export const getCustomersAction = (customer) => {
    return {
        type : GET_CUSTOMERS,
        customer: customer
    }
};

export const getAllCustomersAction = () => {
    return {
        type : GET_ALL_CUSTOMERS
    }
};