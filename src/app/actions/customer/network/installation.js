/** INSTALLING **/
export const GET_INSTALLING = "GET_INSTALLING";
export const GET_INSTALLING_SUCCESS = "GET_INSTALLING_SUCCESS";
export const GET_INSTALLING_FAIL= "GET_INSTALLING_FAIL";

export const GET_ALL_INSTALLING = "GET_ALL_INSTALLING";
export const GET_ALL_INSTALLING_SUCCESS = "GET_ALL_INSTALLING_SUCCESS";
export const GET_ALL_INSTALLING_FAIL= "GET_ALL_INSTALLING_FAIL";

export const GET_SCHEDULE_INSTALLING = "GET_SCHEDULE_INSTALLING";
export const GET_SCHEDULE_INSTALLING_SUCCESS = "GET_SCHEDULE_INSTALLING_SUCCESS";
export const GET_SCHEDULE_INSTALLING_FAIL = "GET_SCHEDULE_INSTALLING_FAIL";

export const UPDATE_INSTALLING = "UPDATE_INSTALLING";
export const UPDATE_INSTALLING_SUCCESS = "UPDATE_INSTALLING_SUCCESS";
export const UPDATE_INSTALLING_FAIL = "UPDATE_INSTALLING_FAIL";

export const DELETE_INSTALLING = "DELETE_INSTALLING";
export const DELETE_INSTALLING_SUCCESS = "DELETE_INSTALLING_SUCCESS";
export const DELETE_INSTALLING_FAIL = "DELETE_INSTALLING_FAIL";

export const ADD_INSTALLING = "ADD_INSTALLING";
export const ADD_INSTALLING_SUCCESS = "ADD_INSTALLING_SUCCESS";
export const ADD_INSTALLING_FAIL= "ADD_INSTALLING_FAIL";

export const ADD_GENERATE_FORM_REQUEST = "ADD_GENERATE_FORM_REQUEST";
export const ADD_GENERATE_FORM_REQUEST_SUCCESS = "ADD_GENERATE_FORM_REQUEST_SUCCESS";
export const ADD_GENERATE_FORM_REQUEST_FAIL= "ADD_GENERATE_FORM_REQUEST_FAIL";

export const GET_ALL_GENERATE_FORM_REQUEST = "GET_ALL_GENERATE_FORM_REQUEST";
export const GET_ALL_GENERATE_FORM_REQUEST_SUCCESS = "GET_ALL_GENERATE_FORM_REQUEST_SUCCESS";
export const GET_ALL_GENERATE_FORM_REQUEST_FAIL= "GET_ALL_GENERATE_FORM_REQUEST_FAIL";

export function addInstallingAction(installing){
    return {
        type: ADD_INSTALLING,
        installing: installing
    }
}
export function getInstallingAction(){
    return {  type: GET_INSTALLING }
}

/*view items installing*/
export function getAllInstallingAction(id){
    return { type: GET_ALL_INSTALLING, id:id }
}
export function getScheduleInstallingAction(){
    return { type: GET_SCHEDULE_INSTALLING }
}
export function updateInstallingAction(installing){
    return {
        type: UPDATE_INSTALLING,
        installing: installing
    }
}

export function deleteInstallingAction(id){
    return {
        type:DELETE_INSTALLING,
        id:id
    }
}
export function addGenerateFormRequestAction(generate){
    return {
        type:ADD_GENERATE_FORM_REQUEST,
        generate:generate
    }
}
export function getAllGenerateFormRequestAction(){
    return {
        type:GET_ALL_GENERATE_FORM_REQUEST
    }
}