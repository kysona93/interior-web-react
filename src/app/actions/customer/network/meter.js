/** CHANGE METER **/
export const UPDATE_CHANGE_METER = "UPDATE_CHANGE_METER";
export const UPDATE_CHANGE_METER_SUCCESS = "UPDATE_CHANGE_METER_SUCCESS";
export const UPDATE_CHANGE_METER_FAIL = "UPDATE_CHANGE_METER_FAIL";

export const GET_FREE_METERS = "GET_FREE_METERS";
export const GET_FREE_METERS_SUCCESS = "GET_FREE_METERS_SUCCESS";
export const GET_FREE_METERS_FAIL = "GET_FREE_METERS_FAIL";

export const GET_USED_METERS = "GET_USED_METERS";
export const GET_USED_METERS_SUCCESS = "GET_USED_METERS_SUCCESS";
export const GET_USED_METERS_FAIL = "GET_USED_METERS_FAIL";

export const ADD_ITEM_MV = "ADD_ITEM_MV";
export const ADD_ITEM_MV_SUCCESS = "ADD_ITEM_MV_SUCCESS";
export const ADD_ITEM_MV_FAIL = "ADD_ITEM_MV_FAIL";

export const UPDATE_ITEM_MV = "UPDATE_ITEM_MV";
export const UPDATE_ITEM_MV_SUCCESS = "UPDATE_ITEM_MV_SUCCESS";
export const UPDATE_ITEM_MV_FAIL = "UPDATE_ITEM_MV_FAIL";

export const GET_ITEMS_MV = "GET_ITEMS_MV";
export const GET_ITEMS_MV_SUCCESS = "GET_ITEMS_MV_SUCCESS";
export const GET_ITEMS_MV_FAIL = "GET_ITEMS_MV_FAIL";

export const GET_ALL_ITEMS_MV = "GET_ALL_ITEMS_MV";
export const GET_ALL_ITEMS_MV_SUCCESS = "GET_ALL_ITEMS_MV_SUCCESS";
export const GET_ALL_ITEMS_MV_FAIL = "GET_ALL_ITEMS_MV_FAIL";

export const DELETE_ITEMS_MV = "DELETE_ITEMS_MV";
export const DELETE_ITEMS_MV_SUCCESS = "DELETE_ITEMS_MV_SUCCESS";
export const DELETE_ITEMS_MV_FAIL = "DELETE_ITEMS_MV_FAIL";

export const GET_ITEMS_MV_SUMMARY = "GET_ITEMS_MV_SUMMARY";
export const GET_ITEMS_MV_SUMMARY_SUCCESS = "GET_ITEMS_MV_SUMMARY_SUCCESS";
export const GET_ITEMS_MV_SUMMARY_FAIL = "GET_ITEMS_MV_SUMMARY_FAIL";

export const GET_CONNECTION_POINT = "GET_CONNECTION_POINT";
export const GET_CONNECTION_POINT_SUCCESS = "GET_CONNECTION_POINT_SUCCESS";
export const GET_CONNECTION_POINT_FAIL = "GET_CONNECTION_POINT_FAIL";

export const UPDATE_TRANSFORMER_METER = "UPDATE_TRANSFORMER_METER";
export const UPDATE_TRANSFORMER_METER_SUCCESS = "UPDATE_TRANSFORMER_METER_SUCCESS";
export const UPDATE_TRANSFORMER_METER_FAIL = "UPDATE_TRANSFORMER_METER_FAIL";

export const GET_METER_HISTORY = "GET_METER_HISTORY";
export const GET_METER_HISTORY_SUCCESS = "GET_METER_HISTORY_SUCCESS";
export const GET_METER_HISTORY_FAIL = "GET_METER_HISTORY_FAIL";

export function updateChangeMeterAction(changeMeter) {
    return {
        type: UPDATE_CHANGE_METER,
        changeMeter: changeMeter
    }
}

export function addItemMvAction(mv) {
    return {
        type: ADD_ITEM_MV,
        mv: mv
    }
}

export function updateItemMvAction(mv){
    return {
        type: UPDATE_ITEM_MV,
        mv:mv
    }
}
export function deleteItemMvAction(id){
    return {
        type: DELETE_ITEMS_MV,
        id:id
    }
}

export function getItemsMvAction(id){
    return{
        type: GET_ITEMS_MV,
        id:id
    }
}
export function getAllItemsMvAction(license){
    return{
        type: GET_ALL_ITEMS_MV,
        license: license
    }
}

export function getItemMvSummaryAction(summary){
    return{
        type: GET_ITEMS_MV_SUMMARY,
        summary:summary
    }
}
export function getConnectionPointAction(connection){
    return{
        type: GET_CONNECTION_POINT,
        connection:connection
    }
}

export function updateTransformerMeterAction(transfer) {
    return {
        type: UPDATE_TRANSFORMER_METER,
        transfer: transfer
    }
}
export function getAllMeterHistoryAction(id) {
    return {
        type: GET_METER_HISTORY,
        id:id
    }
}
export function getFreeMetersAction() {
    return {
        type: GET_FREE_METERS
    }
}
export function getUsedMetersAction(id) {
    return {
        type: GET_USED_METERS,
        id:id
    }
}