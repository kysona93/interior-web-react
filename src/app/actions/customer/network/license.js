export const ADD_RENT_POLE = "ADD_RENT_POLE";
export const ADD_RENT_POLE_SUCCESS = "ADD_RENT_POLE_SUCCESS";
export const ADD_RENT_POLE_FAIL = "ADD_RENT_POLE_FAIL";

export const UPDATE_RENT_POLE = "UPDATE_RENT_POLE";
export const UPDATE_RENT_POLE_SUCCESS = "UPDATE_RENT_POLE_SUCCESS";
export const UPDATE_RENT_POLE_FAIL = "UPDATE_RENT_POLE_FAIL";

export const DELETE_RENT_POLE = "DELETE_RENT_POLE";
export const DELETE_RENT_POLE_SUCCESS = "DELETE_RENT_POLE_SUCCESS";
export const DELETE_RENT_POLE_FAIL = "DELETE_RENT_POLE_FAIL";

export const GET_RENT_POLE = "GET_RENT_POLE";
export const GET_RENT_POLE_SUCCESS = "GET_RENT_POLE_SUCCESS";
export const GET_RENT_POLE_FAIL = "GET_RENT_POLE_FAIL";

export const GET_ALL_RENT_POLE = "GET_ALL_RENT_POLE";
export const GET_ALL_RENT_POLE_SUCCESS = "GET_ALL_RENT_POLE_SUCCESS";
export const GET_ALL_RENT_POLE_FAIL = "GET_ALL_RENT_POLE_FAIL";
/** RENT PART **/
export const ADD_RENT_PART = "ADD_RENT_PART";
export const ADD_RENT_PART_SUCCESS = "ADD_RENT_PART_SUCCESS";
export const ADD_RENT_PART_FAIL = "ADD_RENT_PART_FAIL";

export const UPDATE_RENT_PART = "UPDATE_RENT_PART";
export const UPDATE_RENT_PART_SUCCESS = "UPDATE_RENT_PART_SUCCESS";
export const UPDATE_RENT_PART_FAIL = "UPDATE_RENT_PART_FAIL";

export const DELETE_RENT_PART = "DELETE_RENT_PART";
export const DELETE_RENT_PART_SUCCESS = "DELETE_RENT_PART_SUCCESS";
export const DELETE_RENT_PART_FAIL = "DELETE_RENT_PART_FAIL";

export const GET_RENT_PART = "GET_RENT_PART";
export const GET_RENT_PART_SUCCESS = "GET_RENT_PART_SUCCESS";
export const GET_RENT_PART_FAIL = "GET_RENT_PART_FAIL";

export const GET_ALL_RENT_PART = "GET_ALL_RENT_PART";
export const GET_ALL_RENT_PART_SUCCESS = "GET_ALL_RENT_PART_SUCCESS";
export const GET_ALL_RENT_PART_FAIL = "GET_ALL_RENT_PART_FAIL";

export function addRentPoleAction(rentPole){
    return{
        type: ADD_RENT_POLE,
        rentPole:rentPole
    }
}
export function updateRentPoleAction(rentPole){
    return{
        type: UPDATE_RENT_POLE,
        rentPole:rentPole
    }
}
export function deleteRentPoleAction(id){
    return{
        type: DELETE_RENT_POLE,
        id:id
    }
}
export function getRentPoleAction(id){
    return {
        type:GET_RENT_POLE,
        id:id
    }

}
export function getAllRentPoleAction(){
    return GET_ALL_RENT_POLE
}
/** rent part **/
export function addRentPartAction(rentPart){
    return{
        type: ADD_RENT_PART,
        rentPart:rentPart
    }
}
export function updateRentPartAction(rentPart){
    return{
        type: UPDATE_RENT_PART,
        rentPart: rentPart
    }
}
export function deleteRentPartAction(id){
    return{
        type: DELETE_RENT_PART,
        id:id

    }
}
export function getRentPartAction(id){
    return{
        type: GET_RENT_PART,
        id: id
    }
}
export function getAllRentPartAction(id){
    return{
        type: GET_ALL_RENT_PART,
        id:id
    }
}