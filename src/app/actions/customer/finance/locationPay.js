export const ADD_LOCATION_PAY = "ADD_LOCATION_PAY";
export const ADD_LOCATION_PAY_SUCCESS = "ADD_LOCATION_PAY_SUCCESS";
export const ADD_LOCATION_PAY_FAIL = "ADD_LOCATION_PAY_FAIL";

export const GET_ALL_LOCATION_PAY = "GET_ALL_LOCATION_PAY";
export const GET_ALL_LOCATION_PAY_SUCCESS = "GET_ALL_LOCATION_PAY_SUCCESS";
export const GET_ALL_LOCATION_PAY_FAIL = "GET_ALL_LOCATION_PAY_FAIL";

export const UPDATE_LOCATION_PAY = "UPDATE_LOCATION_PAY";
export const UPDATE_LOCATION_PAY_SUCCESS = "UPDATE_LOCATION_PAY_SUCCESS";
export const UPDATE_LOCATION_PAY_FAIL = "UPDATE_LOCATION_PAY_FAIL";

export const DELETE_LOCATION_PAY = "DELETE_LOCATION_PAY";
export const DELETE_LOCATION_PAY_SUCCESS = "DELETE_LOCATION_PAY_SUCCESS";
export const DELETE_LOCATION_PAY_FAIL = "DELETE_LOCATION_PAY_FAIL";

export function addLocationPayAction(location){
    return {
        type: ADD_LOCATION_PAY
    }
}

export function getAllLocationPayAction(){
    return {
        type: GET_ALL_LOCATION_PAY
    }
}

export function updateLocationPayAction(location){
    return {
        type: UPDATE_LOCATION_PAY,
        location: location
    }
}

export function deleteLocationPayAction(id){
    return {
        type: DELETE_LOCATION_PAY,
        id: id
    }
}