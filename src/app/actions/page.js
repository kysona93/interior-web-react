export const GET_PAGES_BY_GROUP = "GET_PAGES_BY_GROUP";
export const GET_PAGES_BY_GROUP_SUCCESS = "GET_PAGES_BY_GROUP_SUCCESS";
export const GET_PAGES_BY_GROUP_FAIL = "GET_PAGES_BY_GROUP_FAIL";

export const GET_PAGES_BY_USER = "GET_PAGES_BY_USER";
export const GET_PAGES_BY_USER_SUCCESS = "GET_PAGES_BY_USER_SUCCESS";
export const GET_PAGES_BY_USER_FAIL = "GET_PAGES_BY_USER_FAIL";

export const GET_PAGES_BY_USERNAME = "GET_PAGES_BY_USERNAME";
export const GET_PAGES_BY_USERNAME_SUCCESS = "GET_PAGES_BY_USERNAME_SUCCESS";
export const GET_PAGES_BY_USERNAME_FAIL = "GET_PAGES_BY_USERNAME_FAIL";

/* get pages by group id */
export function getPagesByGroupAction(ids){
    return{
        type: GET_PAGES_BY_GROUP,
        ids: ids
    }
}

/* get pages by user id */
export function getPagesByUserAction(id){
    return{
        type: GET_PAGES_BY_USER,
        id: id
    }
}

/* get pages by username */
export function getPagesByUsernameAction(){
    return{
        type: GET_PAGES_BY_USERNAME,
    }
}