export const ADD_INVOICE = "ADD_INVOICE";
export const ADD_INVOICE_SUCCESS = "ADD_INVOICE_SUCCESS";
export const ADD_INVOICE_FAIL = "ADD_INVOICE_FAIL";

export const GET_ALL_INVOICES = "GET_ALL_INVOICES";
export const GET_ALL_INVOICES_SUCCESS = "GET_ALL_INVOICES_SUCCESS";
export const GET_ALL_INVOICES_FAIL = "GET_ALL_INVOICES_FAIL";

export const GET_INVOICES = "GET_INVOICES";
export const GET_INVOICES_SUCCESS = "GET_INVOICES_SUCCESS";
export const GET_INVOICES_FAIL = "GET_INVOICES_FAIL";

export const UPDATE_INVOICE = "UPDATE_INVOICE";
export const UPDATE_INVOICE_SUCCESS = "UPDATE_INVOICE_SUCCESS";
export const UPDATE_INVOICE_FAIL = "UPDATE_INVOICE_FAIL";

export const DELETE_INVOICE = "DELETE_INVOICE";
export const DELETE_INVOICE_SUCCESS = "DELETE_INVOICE_SUCCESS";
export const DELETE_INVOICE_FAIL = "DELETE_INVOICE_FAIL";

export const VOID_INVOICE = "VOID_INVOICE";
export const VOID_INVOICE_SUCCESS = "VOID_INVOICE_SUCCESS";
export const VOID_INVOICE_FAIL = "VOID_INVOICE_FAIL";

export function addInvoiceAction(invoice){
    return {
        type: ADD_INVOICE,
        invoice: invoice
    }
}

export function getAllInvoicesAction(invoice){
    return {
        type: GET_ALL_INVOICES,
        invoice: invoice
    }
}
export function getInvoicesAction(invoice){
    return {
        type: GET_INVOICES,
        invoice: invoice
    }
}

export function updateInvoiceAction(invoice){
    return {
        type: UPDATE_INVOICE,
        invoice: invoice
    }
}

export function deleteInvoiceAction(id){
    return {
        type: DELETE_INVOICE,
        id: id
    }
}

export function voidInvoiceAction(id){
    return {
        type: VOID_INVOICE,
        id: id
    }
}
