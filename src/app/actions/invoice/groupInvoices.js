export const ADD_GROUP_INVOICE = "ADD_GROUP_INVOICE";
export const ADD_GROUP_INVOICE_SUCCESS = "ADD_GROUP_INVOICE_SUCCESS";
export const ADD_GROUP_INVOICE_FAIL = "ADD_GROUP_INVOICE_FAIL";

export const GET_ALL_GROUP_INVOICE = "GET_ALL_GROUP_INVOICE";
export const GET_ALL_GROUP_INVOICE_SUCCESS = "GET_ALL_GROUP_INVOICE_SUCCESS";
export const GET_ALL_GROUP_INVOICE_FAIL = "GET_ALL_GROUP_INVOICE_FAIL";

export const UPDATE_GROUP_INVOICE = "UPDATE_GROUP_INVOICE";
export const UPDATE_GROUP_INVOICE_SUCCESS = "UPDATE_GROUP_INVOICE_SUCCESS";
export const UPDATE_GROUP_INVOICE_FAIL = "UPDATE_GROUP_INVOICE_FAIL";

export const DELETE_GROUP_INVOICE = "DELETE_GROUP_INVOICE";
export const DELETE_GROUP_INVOICE_SUCCESS = "DELETE_GROUP_INVOICE_SUCCESS";
export const DELETE_GROUP_INVOICE_FAIL = "DELETE_GROUP_INVOICE_FAIL";

export function addGroupInvoiceAction(group){
    return {
        type: ADD_GROUP_INVOICE,
        group: group
    }
}

export function getAllGroupInvoiceAction(){
    return {
        type: GET_ALL_GROUP_INVOICE
    }
}

export function updateGroupInvoiceAction(group){
    return {
        type: UPDATE_GROUP_INVOICE,
        group: group
    }
}

export function deleteGroupInvoiceAction(id){
    return {
        type: DELETE_GROUP_INVOICE,
        id: id
    }
}