export const ADD_INVOICE_TYPE = "ADD_INVOICE_TYPE";
export const ADD_INVOICE_TYPE_SUCCESS = "ADD_INVOICE_TYPE_SUCCESS";
export const ADD_INVOICE_TYPE_FAIL = "ADD_INVOICE_TYPE_FAIL";

export const GET_ALL_INVOICE_TYPE = "GET_ALL_INVOICE_TYPE";
export const GET_ALL_INVOICE_TYPE_SUCCESS = "GET_ALL_INVOICE_TYPE_SUCCESS";
export const GET_ALL_INVOICE_TYPE_FAIL = "GET_ALL_INVOICE_TYPE_FAIL";

export const UPDATE_INVOICE_TYPE = "UPDATE_INVOICE_TYPE";
export const UPDATE_INVOICE_TYPE_SUCCESS = "UPDATE_INVOICE_TYPE_SUCCESS";
export const UPDATE_INVOICE_TYPE_FAIL = "UPDATE_INVOICE_TYPE_FAIL";

export const DELETE_INVOICE_TYPE = "DELETE_INVOICE_TYPE";
export const DELETE_INVOICE_TYPE_SUCCESS = "DELETE_INVOICE_TYPE_SUCCESS";
export const DELETE_INVOICE_TYPE_FAIL = "DELETE_INVOICE_TYPE_FAIL";

export function addInvoiceTypeAction(invoiceType){
    return {
        type: ADD_INVOICE_TYPE,
        invoiceType : invoiceType
    }
}

export function getAllInvoiceTypeAction(){
    return {
        type: GET_ALL_INVOICE_TYPE
    }
}

export function updateInvoiceTypeAction(invoiceType){
    return {
        type: UPDATE_INVOICE_TYPE,
        invoiceType: invoiceType
    }
}

export function deleteInvoiceTypeAction(id){
    return {
        type: DELETE_INVOICE_TYPE,
        id: id
    }
}

