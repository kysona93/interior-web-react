export const ADD_CUSTOMER_BOOK_PAYMENT = "ADD_CUSTOMER_BOOK_PAYMENT";
export const ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS = "ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS";
export const ADD_CUSTOMER_BOOK_PAYMENT_FAIL = "ADD_CUSTOMER_BOOK_PAYMENT_FAIL";

export const ADD_CUSTOMER_BOOK_DEPRECIATION = "ADD_CUSTOMER_BOOK_DEPRECIATION";
export const ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS = "ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS";
export const ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL = "ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL";

export function addCustomerBookPaymentAction(payment){
    return {
        type : ADD_CUSTOMER_BOOK_PAYMENT,
        payment: payment
    }
}

export function addCustomerBookDepreciationAction(depreciation){
    return {
        type: ADD_CUSTOMER_BOOK_DEPRECIATION,
        depreciation: depreciation
    }
}