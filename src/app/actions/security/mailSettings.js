export const ADD_MAIL_SETTINGS = "ADD_MAIL_SETTINGS";
export const ADD_MAIL_SETTINGS_SUCCESS = "ADD_MAIL_SETTINGS_SUCCESS";
export const ADD_MAIL_SETTINGS_FAIL = "ADD_MAIL_SETTINGS_FAIL";

export const GET_ALL_MAIL_SETTINGS = "GET_ALL_MAIL_SETTINGS";
export const GET_ALL_MAIL_SETTINGS_SUCCESS = "GET_ALL_MAIL_SETTINGS_SUCCESS";
export const GET_ALL_MAIL_SETTINGS_FAIL = "GET_ALL_MAIL_SETTINGS_FAIL";

export const UPDATE_MAIL_SETTINGS = "UPDATE_MAIL_SETTINGS";
export const UPDATE_MAIL_SETTINGS_SUCCESS = "UPDATE_MAIL_SETTINGS_SUCCESS";
export const UPDATE_MAIL_SETTINGS_FAIL = "UPDATE_MAIL_SETTINGS_FAIL";

export const DELETE_MAIL_SETTINGS = "DELETE_MAIL_SETTINGS";
export const DELETE_MAIL_SETTINGS_SUCCESS = "DELETE_MAIL_SETTINGS_SUCCESS";
export const DELETE_MAIL_SETTINGS_FAIL = "DELETE_MAIL_SETTINGS_FAIL";

/* ADD MAIL SETTINGS */
export function addMailSettingsAction(mailSettings) {
    return {
        type: ADD_MAIL_SETTINGS,
        mailSettings: mailSettings
    }
}

/* GET MAIL SETTINGS */
export function getAllMailSettingsAction() {
    return {
        type: GET_ALL_MAIL_SETTINGS
    }
}

/* UPDATE MAIL SETTINGS */
export function updateMailSettingsAction(mailSettings) {
    return {
        type: UPDATE_MAIL_SETTINGS,
        mailSettings: mailSettings
    }
}

/* DELETE MAIL SETTINGS */
export function deleteMailSettingsAction(id) {
    return {
        type: DELETE_MAIL_SETTINGS,
        id: id
    }
}