export const ADD_LOCATION = "ADD_LOCATION";
export const ADD_LOCATION_SUCCESS = "ADD_LOCATION_SUCCESS";
export const ADD_LOCATION_FAIL = "ADD_LOCATION_FAIL";

export const UPDATE_LOCATION = "UPDATE_LOCATION";
export const UPDATE_LOCATION_SUCCESS = "UPDATE_LOCATION_SUCCESS";
export const UPDATE_LOCATION_FAIL = "UPDATE_LOCATION_FAIL";

export const DELETE_LOCATION = "DELETE_LOCATION";
export const DELETE_LOCATION_SUCCESS = "DELETE_LOCATION_SUCCESS";
export const DELETE_LOCATION_FAIL = "DELETE_LOCATION_FAIL";

export const GET_ALL_LOCATIONS = "GET_ALL_LOCATIONS";
export const GET_ALL_LOCATIONS_SUCCESS = "GET_ALL_LOCATIONS_SUCCESS";
export const GET_ALL_LOCATIONS_FAIL = "GET_ALL_LOCATIONS_FAIL";

export const GET_PROVINCES = "GET_PROVINCES";
export const GET_PROVINCES_SUCCESS = "GET_PROVINCES_SUCCESS";
export const GET_PROVINCES_FAIL = "GET_PROVINCES_FAIL";

export const GET_DISTRICTS = "GET_DISTRICTS";
export const GET_DISTRICTS_SUCCESS = "GET_DISTRICTS_SUCCESS";
export const GET_DISTRICTS_FAIL = "GET_DISTRICTS_FAIL";

export const GET_COMMUNES = "GET_COMMUNES";
export const GET_COMMUNES_SUCCESS = "GET_COMMUNES_SUCCESS";
export const GET_COMMUNES_FAIL = "GET_COMMUNES_FAIL";

export const GET_VILLAGES = "GET_VILLAGES";
export const GET_VILLAGES_SUCCESS = "GET_VILLAGES_SUCCESS";
export const GET_VILLAGES_FAIL = "GET_VILLAGES_FAIL";


export const getAllLocationsAction = () => {
    return {
        type : GET_ALL_LOCATIONS
    }
};

export const addLocationAction = (location) => {
    return {
        type: ADD_LOCATION,
        location: location
    }
};

export const updateLocationAction = (location) => {
    return {
        type: UPDATE_LOCATION,
        location: location
    }
};

export const deleteLocationAction = (location) => {
    return {
        type: DELETE_LOCATION,
        location: location
    }
};


export const getProvincesAction = () => {
    return {
        type : GET_PROVINCES
    }
};

export const getDistrictsAction = (location) => {
    return {
        type : GET_DISTRICTS,
        location: location
    }
};

export const getCommunesAction = (location) => {
    return {
        type : GET_COMMUNES,
        location: location
    }
};

export const getVillagesAction = (location) => {
    return {
        type : GET_VILLAGES,
        location: location
    }
};