export const ADD_INSTALLER = "ADD_INSTALLER";
export const ADD_INSTALLER_SUCCESS = "ADD_INSTALLER_SUCCESS";
export const ADD_INSTALLER_FAIL = "ADD_INSTALLER_FAIL";

export const GET_ALL_INSTALLERS = "GET_ALL_INSTALLERS";
export const GET_ALL_INSTALLERS_SUCCESS = "GET_ALL_INSTALLERS_SUCCESS";
export const GET_ALL_INSTALLERS_FAIL = "GET_ALL_INSTALLERS_FAIL";

export const UPDATE_INSTALLER = "UPDATE_INSTALLER";
export const UPDATE_INSTALLER_SUCCESS = "UPDATE_INSTALLER_SUCCESS";
export const UPDATE_INSTALLER_FAIL = "UPDATE_INSTALLER_FAIL";

export const DELETE_INSTALLER = "DELETE_INSTALLER";
export const DELETE_INSTALLER_SUCCESS = "DELETE_INSTALLER_SUCCESS";
export const DELETE_INSTALLER_FAIL = "DELETE_INSTALLER_FAIL";

/* ADD INSTALLER */
export function addInstallerAction(installer){
    return {
        type: ADD_INSTALLER,
        installer : installer
    }
}

/* LIST ALL INSTALLERS */
export function getAllInstallersAction(installer){
    return {
        type: GET_ALL_INSTALLERS,
        installer: installer
    }
}

/* UPDATE INSTALLER */
export function updateInstallerAction(installer){
    return {
        type: UPDATE_INSTALLER,
        installer: installer
    }
}

/* DELETE INSTALLER */
export function deleteInstallerAction(id){
    return {
        type: DELETE_INSTALLER,
        id: id
    }
}

