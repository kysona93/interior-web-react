export const ADD_STEP_WORK = "ADD_STEP_WORK";
export const ADD_STEP_WORK_SUCCESS = "ADD_STEP_WORK_SUCCESS";
export const ADD_STEP_WORK_FAIL = "ADD_STEP_WORK_FAIL";

export const UPDATE_STEP_WORK = "UPDATE_STEP_WORK";
export const UPDATE_STEP_WORK_SUCCESS = "UPDATE_STEP_WORK_SUCCESS";
export const UPDATE_STEP_WORK_FAIL = "UPDATE_STEP_WORK_FAIL";

export const CHECK_STEP_WORK = "CHECK_STEP_WORK";
export const CHECK_STEP_WORK_SUCCESS = "CHECK_STEP_WORK_SUCCESS";
export const CHECK_STEP_WORK_FAIL = "CHECK_STEP_WORK_FAIL";

export const DELETE_STEP_WORK = "DELETE_STEP_WORK";
export const DELETE_STEP_WORK_SUCCESS = "DELETE_STEP_WORK_SUCCESS";
export const DELETE_STEP_WORK_FAIL = "DELETE_STEP_WORK_FAIL";

export const GET_STEP_WORK = "GET_STEP_WORK";
export const GET_STEP_WORK_SUCCESS = "GET_STEP_WORK_SUCCESS";
export const GET_STEP_WORK_FAIL = "GET_STEP_WORK_FAIL";

export const GET_STEP_WORKS = "GET_STEP_WORKS";
export const GET_STEP_WORKS_SUCCESS = "GET_STEP_WORKS_SUCCESS";
export const GET_STEP_WORKS_FAIL = "GET_STEP_WORKS_FAIL";

export const GET_STEP_WORK_DETAILS = "GET_STEP_WORK_DETAILS";
export const GET_STEP_WORK_DETAILS_SUCCESS = "GET_STEP_WORK_DETAILS_SUCCESS";
export const GET_STEP_WORK_DETAILS_FAIL = "GET_STEP_WORK_DETAILS_FAIL";

export const PRINT_STEP_WORK = "PRINT_STEP_WORK";
export const PRINT_STEP_WORK_SUCCESS = "PRINT_STEP_WORK_SUCCESS";
export const PRINT_STEP_WORK_FAIL = "PRINT_STEP_WORK_FAIL";

export const REMOVE_STEP_WORK_DETAIL = "REMOVE_STEP_WORK_DETAIL";
export const REMOVE_STEP_WORK_DETAIL_SUCCESS = "REMOVE_STEP_WORK_DETAIL_SUCCESS";
export const REMOVE_STEP_WORK_DETAIL_FAIL = "REMOVE_STEP_WORK_DETAIL_FAIL";

export const addStepWorkAction = (work) => {
    return {
        type: ADD_STEP_WORK,
        work: work
    }
};

export const updateStepWorkAction = (work) => {
    return {
        type: UPDATE_STEP_WORK,
        work: work
    }
};

export const checkStepWorkAction = (work) => {
    return {
        type: CHECK_STEP_WORK,
        work: work
    }
};

export const deleteStepWorkAction = (id) => {
    return {
        type: DELETE_STEP_WORK,
        id: id
    }
};

export const getStepWorkAction = () => {
    return {
        type: GET_STEP_WORK
    }
};

export const getStepWorksAction = (work) => {
    return {
        type: GET_STEP_WORKS,
        work: work
    }
};

export const getStepWorkDetailsAction = (work) => {
    return {
        type: GET_STEP_WORK_DETAILS,
        work: work
    }
};

export const printStepWorkAction = (id) => {
    return {
        type: PRINT_STEP_WORK,
        id: id
    }
};

export const removeStepWorkDetailAction = (id) => {
    return {
        type: REMOVE_STEP_WORK_DETAIL,
        id: id
    }
};