export const GET_ALL_CASH_BOXES = "GET_ALL_CASH_BOXES";
export const GET_ALL_CASH_BOXES_SUCCESS = "GET_ALL_CASH_BOXES_SUCCESS";
export const GET_ALL_CASH_BOXES_FAIL = "GET_ALL_CASH_BOXES_FAIL";

export const ADD_CASH_BOX = "ADD_CASH_BOX";
export const ADD_CASH_BOX_SUCCESS = "ADD_CASH_BOX_SUCCESS";
export const ADD_CASH_BOX_FAIL = "ADD_CASH_BOX_FAIL";

export const GET_ALL_USERS_CASH_BOXES = "GET_ALL_USERS_CASH_BOXES";
export const GET_ALL_USERS_CASH_BOXES_SUCCESS = "GET_ALL_USERS_CASH_BOXES_SUCCESS";
export const GET_ALL_USERS_CASH_BOXES_FAIL = "GET_ALL_USERS_CASH_BOXES_FAIL";

export const GET_CASH_BOXES_BY_USER_ID = "GET_CASH_BOXES_BY_USER_ID";
export const GET_CASH_BOXES_BY_USER_ID_SUCCESS = "GET_CASH_BOXES_BY_USER_ID_SUCCESS";
export const GET_CASH_BOXES_BY_USER_ID_FAIL = "GET_CASH_BOXES_BY_USER_ID_FAIL";

export function getAllCashBoxesAction(){
    return {
        type : GET_ALL_CASH_BOXES
    }
}

export function addCashBoxAction(box){
    return {
        type : ADD_CASH_BOX,
        box: box
    }
}

export function getAllUsersCashBoxAction(){
    return {
        type : GET_ALL_USERS_CASH_BOXES
    }
}

export function getAllCashBoxesByUserIdAction(){
    return {
        type: GET_CASH_BOXES_BY_USER_ID
    }
}