export const ADD_METER_USAGE = "ADD_METER_USAGE";
export const ADD_METER_USAGE_SUCCESS = "ADD_METER_USAGE_SUCCESS";
export const ADD_METER_USAGE_FAIL = "ADD_METER_USAGE_FAIL";

export const GET_ALL_METER_USAGES = "GET_ALL_METER_USAGES";
export const GET_ALL_METER_USAGES_SUCCESS = "GET_ALL_METER_USAGES_SUCCESS";
export const GET_ALL_METER_USAGES_FAIL = "GET_ALL_METER_USAGES_FAIL";

export const UPDATE_METER_USAGE = "UPDATE_METER_USAGE";
export const UPDATE_METER_USAGE_SUCCESS = "UPDATE_METER_USAGE_SUCCESS";
export const UPDATE_METER_USAGE_FAIL = "UPDATE_METER_USAGE_FAIL";

export const DELETE_METER_USAGE = "DELETE_METER_USAGE";
export const DELETE_METER_USAGE_SUCCESS = "DELETE_METER_USAGE_SUCCESS";
export const DELETE_METER_USAGE_FAIL = "DELETE_METER_USAGE_FAIL";


export function addMeterUsageAction(usage){
    return {
        type: ADD_METER_USAGE,
        usage: usage
    }
}

export function listMeterUsagesAction(){
    return {
        type: GET_ALL_METER_USAGES
    }
}

export function updateMeterUsageAction(usage){
    return {
        type: UPDATE_METER_USAGE,
        usage: usage
    }
}

export function deleteMeterUsageAction(id){
    return {
        type: DELETE_METER_USAGE,
        id: id
    }
}