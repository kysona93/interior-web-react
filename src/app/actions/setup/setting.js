export const ADD_SETTING = "ADD_SETTING";
export const ADD_SETTING_SUCCESS = "ADD_SETTING_SUCCESS";
export const ADD_SETTING_FAIL = "ADD_SETTING_FAIL";

export const GET_ALL_SETTING = "GET_ALL_SETTING";
export const GET_ALL_SETTING_SUCCESS = "GET_ALL_SETTING_SUCCESS";
export const GET_ALL_SETTING_FAIL = "GET_ALL_SETTING_FAIL";

export const UPDATE_SETTING = "UPDATE_SETTING";
export const UPDATE_SETTING_SUCCESS = "UPDATE_SETTING_SUCCESS";
export const UPDATE_SETTING_FAIL = "UPDATE_SETTING_FAIL";

export const DELETE_SETTING = "DELETE_SETTING";
export const DELETE_SETTING_SUCCESS = "DELETE_SETTING_SUCCESS";
export const DELETE_SETTING_FAIL = "DELETE_SETTING_FAIL";

/* ADD SETTING */
export function addSettingAction(setting){
    return {
        type: ADD_SETTING,
        setting: setting
    }
}

/* LIST ALL SETTING */
export function listSettingAction(){
    return {
        type : GET_ALL_SETTING
    }
}

/* UPDATE SETTING */
export function updateSettingAction(setting){
    return {
        type: UPDATE_SETTING,
        setting: setting
    }
}

/* DELETE SETTING */
export function deleteSettingAction(id){
    return {
        type: DELETE_SETTING,
        id: id
    }
}