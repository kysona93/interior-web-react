export const ADD_EXCHANGE_RATE = "ADD_EXCHANGE_RATE";
export const ADD_EXCHANGE_RATE_SUCCESS = "ADD_EXCHANGE_RATE_SUCCESS";
export const ADD_EXCHANGE_RATE_FAIL = "ADD_EXCHANGE_RATE_FAIL";

export const GET_ALL_EXCHANGE_RATE = "GET_ALL_EXCHANGE_RATE";
export const GET_ALL_EXCHANGE_RATE_SUCCESS = "GET_ALL_EXCHANGE_RATE_SUCCESS";
export const GET_ALL_EXCHANGE_RATE_FAIL = "GET_ALL_EXCHANGE_RATE_FAIL";

export const GET_EXCHANGE_RATE = "GET_EXCHANGE_RATE";
export const GET_EXCHANGE_RATE_SUCCESS = "GET_EXCHANGE_RATE_SUCCESS";
export const GET_EXCHANGE_RATE_FAIL = "GET_EXCHANGE_RATE_FAIL";

export const UPDATE_EXCHANGE_RATE = "UPDATE_EXCHANGE_RATE";
export const UPDATE_EXCHANGE_RATE_SUCCESS = "UPDATE_EXCHANGE_RATE_SUCCESS";
export const UPDATE_EXCHANGE_RATE_FAIL = "UPDATE_EXCHANGE_RATE_FAIL";

export const DELETE_EXCHANGE_RATE = "DELETE_EXCHANGE_RATE";
export const DELETE_EXCHANGE_RATE_SUCCESS = "DELETE_EXCHANGE_RATE_SUCCESS";
export const DELETE_EXCHANGE_RATE_FAIL = "DELETE_EXCHANGE_RATE_FAIL";

/* ADD EXCHANGE RATE */
export function addExchangeRateAction(exchange){
    return {
        type: ADD_EXCHANGE_RATE,
        exchange: exchange
    }
}

/* LIST ALL EXCHANGE RATE */
export function listAllExchangeRateAction(){
    return {
        type: GET_ALL_EXCHANGE_RATE
    }
}

/* GET EXCHANGE RATE */
export function getExchangeRateAction(id){
    return {
        type: GET_EXCHANGE_RATE,
        id: id
    }
}

/* UPDATE EXCHANGE RATE */
export function updateExchangeRateAction(exchange){
    return {
        type: UPDATE_EXCHANGE_RATE,
        exchange: exchange
    }
}

/* DELETE EXCHANGE RATE */
export function deleteExchangeRateAction(id){
    return {
        type: DELETE_EXCHANGE_RATE,
        id: id
    }
}