export const ADD_BOX = "ADD_BOX";
export const ADD_BOX_SUCCESS = "ADD_BOX_SUCCESS";
export const ADD_BOX_FAIL = "ADD_BOX_FAIL";

export const UPDATE_BOX = "UPDATE_BOX";
export const UPDATE_BOX_SUCCESS = "UPDATE_BOX_SUCCESS";
export const UPDATE_BOX_FAIL = "UPDATE_BOX_FAIL";

export const GET_BOXES = "GET_BOXES";
export const GET_BOXES_SUCCESS = "GET_BOXES_SUCCESS";
export const GET_BOXES_FAIL = "GET_BOXES_FAIL";

export const GET_ALL_BOXES = "GET_ALL_BOXES";
export const GET_ALL_BOXES_SUCCESS = "GET_ALL_BOXES_SUCCESS";
export const GET_ALL_BOXES_FAIL = "GET_ALL_BOXES_FAIL";

export const DELETE_BOX = "DELETE_BOX";
export const DELETE_BOX_SUCCESS = "DELETE_BOX_SUCCESS";
export const DELETE_BOX_FAIL = "DELETE_BOX_FAIL";

export function addBoxAction(box) {
    return {
        type : ADD_BOX,
        box: box
    }
}

export function updateBoxAction(box) {
    return {
        type : UPDATE_BOX,
        box: box
    }
}

export function deleteBoxAction(id) {
    return {
        type : DELETE_BOX,
        id: id
    }
}

export function getBoxesAction(box) {
    return {
        type : GET_BOXES,
        box: box
    }
}

export function getAllBoxesAction() {
    return {
        type : GET_ALL_BOXES
    }
}


