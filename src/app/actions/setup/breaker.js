export const ADD_BREAKER = "ADD_BREAKER";
export const ADD_BREAKER_SUCCESS = "ADD_BREAKER_SUCCESS";
export const ADD_BREAKER_FAIL = "ADD_BREAKER_FAIL";

export const UPDATE_BREAKER = "UPDATE_BREAKER";
export const UPDATE_BREAKER_SUCCESS = "UPDATE_BREAKER_SUCCESS";
export const UPDATE_BREAKER_FAIL = "UPDATE_BREAKER_FAIL";

export const DELETE_BREAKER = "DELETE_BREAKER";
export const DELETE_BREAKER_SUCCESS = "DELETE_BREAKER_SUCCESS";
export const DELETE_BREAKER_FAIL = "DELETE_BREAKER_FAIL";

export const GET_BREAKERS = "GET_BREAKERS";
export const GET_BREAKERS_SUCCESS = "GET_BREAKERS_SUCCESS";
export const GET_BREAKERS_FAIL = "GET_BREAKERS_FAIL";

export const GET_ALL_BREAKERS = "GET_ALL_BREAKERS";
export const GET_ALL_BREAKERS_SUCCESS = "GET_ALL_BREAKERS_SUCCESS";
export const GET_ALL_BREAKERS_FAIL = "GET_ALL_BREAKERS_FAIL";

export function addBreakerAction(breaker) {
    return {
        type : ADD_BREAKER,
        breaker: breaker
    }
}

export function updateBreakerAction(breaker) {
    return {
        type : UPDATE_BREAKER,
        breaker: breaker
    }
}

export function deleteBreakerAction(id) {
    return {
        type : DELETE_BREAKER,
        id: id
    }
}

export function getBreakersAction(breaker) {
    return {
        type : GET_BREAKERS,
        breaker: breaker
    }
}

export function getAllBreakersAction() {
    return {
        type : GET_ALL_BREAKERS
    }
}