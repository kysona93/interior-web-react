export const ADD_POLE_TYPE = "ADD_POLE_TYPE";
export const ADD_POLE_TYPE_SUCCESS = "ADD_POLE_TYPE_SUCCESS";
export const ADD_POLE_TYPE_FAIL = "ADD_POLE_TYPE_FAIL";

export const GET_ALL_POLE_TYPES = "GET_ALL_POLE_TYPES";
export const GET_ALL_POLE_TYPES_SUCCESS = "GET_ALL_POLE_TYPES_SUCCESS";
export const GET_ALL_POLE_TYPES_FAIL = "GET_ALL_POLE_TYPES_FAIL";

export const UPDATE_POLE_TYPE = "UPDATE_POLE_TYPE";
export const UPDATE_POLE_TYPE_SUCCESS = "UPDATE_POLE_TYPE_SUCCESS";
export const UPDATE_POLE_TYPE_FAIL = "UPDATE_POLE_TYPE_FAIL";

export const DELETE_POLE_TYPE = "DELETE_POLE_TYPE";
export const DELETE_POLE_TYPE_SUCCESS = "DELETE_POLE_TYPE_SUCCESS";
export const DELETE_POLE_TYPE_FAIL = "DELETE_POLE_TYPE_FAIL";

/* ADD POLE TYPE */
export function addPoleTypeAction(pole){
    return {
        type: ADD_POLE_TYPE,
        pole: pole
    }
}

/* LIST POLE TYPE */


/* UPDATE POLE TYPE */
export function updatePoleTypeAction(pole){
    return {
        type: UPDATE_POLE_TYPE,
        pole: pole
    }
}

/* DELETE POLE TYPE */
export function deletePoleTypeAction(id){
    return {
        type: DELETE_POLE_TYPE,
        id: id
    }
}

export function getAllPoleTypesAction(){
    return {
        type: GET_ALL_POLE_TYPES
    }
}