export const ADD_CONSUMPTION_TYPE = "ADD_CONSUMPTION_TYPE";
export const ADD_CONSUMPTION_TYPE_SUCCESS = "ADD_CONSUMPTION_TYPE_SUCCESS";
export const ADD_CONSUMPTION_TYPE_FAIL = "ADD_CONSUMPTION_TYPE_FAIL";

export const GET_ALL_CONSUMPTION_TYPES = "GET_ALL_CONSUMPTION_TYPES";
export const GET_ALL_CONSUMPTION_TYPES_SUCCESS = "GET_ALL_CONSUMPTION_TYPES_SUCCESS";
export const GET_ALL_CONSUMPTION_TYPES_FAIL = "GET_ALL_CONSUMPTION_TYPES_FAIL";

export const UPDATE_CONSUMPTION_TYPE = "UPDATE_CONSUMPTION_TYPE";
export const UPDATE_CONSUMPTION_TYPE_SUCCESS = "UPDATE_CONSUMPTION_TYPE_SUCCESS";
export const UPDATE_CONSUMPTION_TYPE_FAIL = "UPDATE_CONSUMPTION_TYPE_FAIL";

export const DELETE_CONSUMPTION_TYPE = "DELETE_CONSUMPTION_TYPE";
export const DELETE_CONSUMPTION_TYPE_SUCCESS = "DELETE_CONSUMPTION_TYPE_SUCCESS";
export const DELETE_CONSUMPTION_TYPE_FAIL = "DELETE_CONSUMPTION_TYPE_FAIL";

export function addConsumptionTypeAction(consumption){
    return {
        type : ADD_CONSUMPTION_TYPE,
        consumption: consumption
    }
}

export function getAllConsumptionTypesAction(){
    return {
        type : GET_ALL_CONSUMPTION_TYPES
    }
}

export function updateConsumptionTypeAction(consumption){
    return {
        type : UPDATE_CONSUMPTION_TYPE,
        consumption: consumption
    }
}

export function deleteConsumptionTypeAction(id){
    return {
        type : DELETE_CONSUMPTION_TYPE,
        id: id
    }
}