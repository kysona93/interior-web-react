export const ADD_METER_TYPE = "ADD_METER_TYPE";
export const ADD_METER_TYPE_SUCCESS = "ADD_METER_TYPE_SUCCESS";
export const ADD_METER_TYPE_FAIL = "ADD_METER_TYPE_FAIL";

export const GET_ALL_METER_TYPES = "GET_ALL_METER_TYPES";
export const GET_ALL_METER_TYPES_SUCCESS = "GET_ALL_METER_TYPES_SUCCESS";
export const GET_ALL_METER_TYPES_FAIL = "GET_ALL_METER_TYPES_FAIL";

export const UPDATE_METER_TYPE = "UPDATE_METER_TYPE";
export const UPDATE_METER_TYPE_SUCCESS = "UPDATE_METER_TYPE_SUCCESS";
export const UPDATE_METER_TYPE_FAIL = "UPDATE_METER_TYPE_FAIL";

export const DELETE_METER_TYPE = "DELETE_METER_TYPE";
export const DELETE_METER_TYPE_SUCCESS = "DELETE_METER_TYPE_SUCCESS";
export const DELETE_METER_TYPE_FAIL = "DELETE_METER_TYPE_FAIL";

export function addMeterTypeAction(meter){
    return {
        type: ADD_METER_TYPE,
        meter: meter
    }
}

export function getAllMeterTypesAction(){
    return {
        type: GET_ALL_METER_TYPES
    }
}

export function updateMeterTypeAction(meter){
    return {
        type: UPDATE_METER_TYPE,
        meter: meter
    }
}

export function deleteMeterTypeAction(id){
    return {
        type: DELETE_METER_TYPE,
        id: id
    }
}