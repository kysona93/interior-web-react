export const ADD_TRANSFORMER_TYPE = "ADD_TRANSFORMER_TYPE";
export const ADD_TRANSFORMER_TYPE_SUCCESS = "ADD_TRANSFORMER_TYPE_SUCCESS";
export const ADD_TRANSFORMER_TYPE_FAIL = "ADD_TRANSFORMER_TYPE_FAIL";

export const GET_ALL_TRANSFORMER_TYPES = "GET_ALL_TRANSFORMER_TYPES";
export const GET_ALL_TRANSFORMER_TYPES_SUCCESS = "GET_ALL_TRANSFORMER_TYPES_SUCCESS";
export const GET_ALL_TRANSFORMER_TYPES_FAIL = "GET_ALL_TRANSFORMER_TYPES_FAIL";

export const UPDATE_TRANSFORMER_TYPE = "UPDATE_TRANSFORMER_TYPE";
export const UPDATE_TRANSFORMER_TYPE_SUCCESS = "UPDATE_TRANSFORMER_TYPE_SUCCESS";
export const UPDATE_TRANSFORMER_TYPE_FAIL = "UPDATE_TRANSFORMER_TYPE_FAIL";

export const DELETE_TRANSFORMER_TYPE = "DELETE_TRANSFORMER_TYPE";
export const DELETE_TRANSFORMER_TYPE_SUCCESS = "DELETE_TRANSFORMER_TYPE_SUCCESS";
export const DELETE_TRANSFORMER_TYPE_FAIL = "DELETE_TRANSFORMER_TYPE_FAIL";

export function addTransformerTypeAction(transformer){
    return {
        type: ADD_TRANSFORMER_TYPE,
        transformer: transformer
    }
}

export function getAllTransformersTypesAction(){
    return {
        type: GET_ALL_TRANSFORMER_TYPES
    }
}

export function updateTransformerTypeAction(transformer){
    return {
        type: UPDATE_TRANSFORMER_TYPE,
        transformer: transformer
    }
}

export function deleteTransformerTypeAction(id){
    return {
        type: DELETE_TRANSFORMER_TYPE,
        id: id
    }
}