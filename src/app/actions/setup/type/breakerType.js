export const ADD_BREAKER_TYPE = "ADD_BREAKER_TYPE";
export const ADD_BREAKER_TYPE_SUCCESS = "ADD_BREAKER_TYPE_SUCCESS";
export const ADD_BREAKER_TYPE_FAIL = "ADD_BREAKER_TYPE_FAIL";

export const GET_ALL_BREAKER_TYPES = "GET_ALL_BREAKER_TYPES";
export const GET_ALL_BREAKER_TYPES_SUCCESS = "GET_ALL_BREAKER_TYPES_SUCCESS";
export const GET_ALL_BREAKER_TYPES_FAIL = "GET_ALL_BREAKER_TYPES_FAIL";

export const UPDATE_BREAKER_TYPE = "UPDATE_BREAKER_TYPE";
export const UPDATE_BREAKER_TYPE_SUCCESS = "UPDATE_BREAKER_TYPE_SUCCESS";
export const UPDATE_BREAKER_TYPE_FAIL = "UPDATE_BREAKER_TYPE_FAIL";

export const DELETE_BREAKER_TYPE = "DELETE_BREAKER_TYPE";
export const DELETE_BREAKER_TYPE_SUCCESS = "DELETE_BREAKER_TYPE_SUCCESS";
export const DELETE_BREAKER_TYPE_FAIL = "DELETE_BREAKER_TYPE_FAIL";

export function addBreakerTypeAction(breaker){
    return {
        type: ADD_BREAKER_TYPE,
        breaker: breaker
    }
}

export function updateBreakerTypeAction(breaker){
    return {
        type: UPDATE_BREAKER_TYPE,
        breaker: breaker
    }
}

export function deleteBreakerTypeAction(id){
    return {
        type: DELETE_BREAKER_TYPE,
        id: id
    }
}

export function getAllBreakerTypesAction(){
    return {
        type: GET_ALL_BREAKER_TYPES
    }
}