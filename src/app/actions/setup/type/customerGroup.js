export const ADD_CUSTOMER_GROUP = "ADD_CUSTOMER_GROUP";
export const ADD_CUSTOMER_GROUP_SUCCESS = "ADD_CUSTOMER_GROUP_SUCCESS";
export const ADD_CUSTOMER_GROUP_FAIL = "ADD_CUSTOMER_GROUP_FAIL";

export const GET_ALL_CUSTOMER_GROUPS = "GET_ALL_CUSTOMER_GROUPS";
export const GET_ALL_CUSTOMER_GROUPS_SUCCESS = "GET_ALL_CUSTOMER_GROUPS_SUCCESS";
export const GET_ALL_CUSTOMER_GROUPS_FAIL = "GET_ALL_CUSTOMER_GROUPS_FAIL";

export const UPDATE_CUSTOMER_GROUP = "UPDATE_CUSTOMER_GROUP";
export const UPDATE_CUSTOMER_GROUP_SUCCESS = "UPDATE_CUSTOMER_GROUP_SUCCESS";
export const UPDATE_CUSTOMER_GROUP_FAIL = "UPDATE_CUSTOMER_GROUP_FAIL";

export const DELETE_CUSTOMER_GROUP = "DELETE_CUSTOMER_GROUP";
export const DELETE_CUSTOMER_GROUP_SUCCESS = "DELETE_CUSTOMER_GROUP_SUCCESS";
export const DELETE_CUSTOMER_GROUP_FAIL = "DELETE_CUSTOMER_GROUP_FAIL";

export const addCustomerGroupAction = (group) => {
    return {
        type: ADD_CUSTOMER_GROUP,
        group: group
    }
};

export const getAllCustomerGroupsAction = (id) => {
    return {
        type: GET_ALL_CUSTOMER_GROUPS,
        id: id
    }
};

export const updateCustomerGroupAction = (group) => {
    return {
        type: UPDATE_CUSTOMER_GROUP,
        group: group
    }
};

export const deleteCustomerGroupAction = (id) => {
    return {
        type: DELETE_CUSTOMER_GROUP,
        id: id
    }
};