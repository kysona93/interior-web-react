export const ADD_VOLTAGE_TYPE = "ADD_VOLTAGE_TYPE";
export const ADD_VOLTAGE_TYPE_SUCCESS = "ADD_VOLTAGE_TYPE_SUCCESS";
export const ADD_VOLTAGE_TYPE_FAIL = "ADD_VOLTAGE_TYPE_FAIL";

export const GET_ALL_VOLTAGE_TYPES = "GET_ALL_VOLTAGE_TYPES";
export const GET_ALL_VOLTAGE_TYPES_SUCCESS = "GET_ALL_VOLTAGE_TYPES_SUCCESS";
export const GET_ALL_VOLTAGE_TYPES_FAIL = "GET_ALL_VOLTAGE_TYPES_FAIL";

export const UPDATE_VOLTAGE_TYPE = "UPDATE_VOLTAGE_TYPE";
export const UPDATE_VOLTAGE_TYPE_SUCCESS = "UPDATE_VOLTAGE_TYPE_SUCCESS";
export const UPDATE_VOLTAGE_TYPE_FAIL = "UPDATE_VOLTAGE_TYPE_FAIL";

export const DELETE_VOLTAGE_TYPE = "DELETE_VOLTAGE_TYPE";
export const DELETE_VOLTAGE_TYPE_SUCCESS = "DELETE_VOLTAGE_TYPE_SUCCESS";
export const DELETE_VOLTAGE_TYPE_FAIL = "DELETE_VOLTAGE_TYPE_FAIL";

export function addVoltageTypeAction(voltage){
    return {
        type: ADD_VOLTAGE_TYPE,
        voltage: voltage
    }
}

export function updateVoltageTypeAction(voltage){
    return {
        type: UPDATE_VOLTAGE_TYPE,
        voltage: voltage
    }
}

export function deleteVoltageTypeAction(id){
    return {
        type: DELETE_VOLTAGE_TYPE,
        id: id
    }
}

export function getAllVoltageTypesAction(){
    return {
        type: GET_ALL_VOLTAGE_TYPES
    }
}