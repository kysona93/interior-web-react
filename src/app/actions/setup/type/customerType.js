export const ADD_CUSTOMER_TYPE = "ADD_CUSTOMER_TYPE";
export const ADD_CUSTOMER_TYPE_SUCCESS = "ADD_CUSTOMER_TYPE_SUCCESS";
export const ADD_CUSTOMER_TYPE_FAIL = "ADD_CUSTOMER_TYPE_FAIL";

export const UPDATE_CUSTOMER_TYPE = "UPDATE_CUSTOMER_TYPE";
export const UPDATE_CUSTOMER_TYPE_SUCCESS = "UPDATE_CUSTOMER_TYPE_SUCCESS";
export const UPDATE_CUSTOMER_TYPE_FAIL = "UPDATE_CUSTOMER_TYPE_FAIL";

export const GET_ALL_CUSTOMER_TYPES = "GET_ALL_CUSTOMER_TYPES";
export const GET_ALL_CUSTOMER_TYPES_SUCCESS = "GET_ALL_CUSTOMER_TYPES_SUCCESS";
export const GET_ALL_CUSTOMER_TYPES_FAIL = "GET_ALL_CUSTOMER_TYPES_FAIL";

export const DELETE_CUSTOMER_TYPE="DELETE_CUSTOMER_TYPE";
export const DELETE_CUSTOMER_TYPE_SUCCESS = "DELETE_CUSTOMER_TYPE_SUCCESS";
export const DELETE_CUSTOMER_TYPE_FAIL = "DELETE_CUSTOMER_TYPE_FAIL";

export function addCustomerTypeAction(customer) {
    return {
        type : ADD_CUSTOMER_TYPE,
        customer: customer
    }
}

export function updateCustomerTypeAction(customer) {
    return {
        type : UPDATE_CUSTOMER_TYPE,
        customer:customer
    }
}

export function deleteCustomerTypeAction(id) {
    return {
        type : DELETE_CUSTOMER_TYPE,
        id: id
    }
}

export function getAllCustomerTypesAction() {
    return {
        type : GET_ALL_CUSTOMER_TYPES
    }
}
