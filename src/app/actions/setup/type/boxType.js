export const ADD_BOX_TYPE = "ADD_BOX_TYPE";
export const ADD_BOX_TYPE_SUCCESS = "ADD_BOX_TYPE_SUCCESS";
export const ADD_BOX_TYPE_FAIL = "ADD_BOX_TYPE_FAIL";

export const GET_ALL_BOX_TYPES = "GET_ALL_BOX_TYPES";
export const GET_ALL_BOX_TYPES_SUCCESS = "GET_ALL_BOX_TYPES_SUCCESS";
export const GET_ALL_BOX_TYPES_FAIL = "GET_ALL_BOX_TYPES_FAIL";

export const UPDATE_BOX_TYPE = "UPDATE_BOX_TYPE";
export const UPDATE_BOX_TYPE_SUCCESS = "UPDATE_BOX_TYPE_SUCCESS";
export const UPDATE_BOX_TYPE_FAIL = "UPDATE_BOX_TYPE_FAIL";

export const DELETE_BOX_TYPE = "DELETE_BOX_TYPE";
export const DELETE_BOX_TYPE_SUCCESS = "DELETE_BOX_TYPE_SUCCESS";
export const DELETE_BOX_TYPE_FAIL = "DELETE_BOX_TYPE_FAIL";

export function addBoxTypeAction(box){
    return {
        type: ADD_BOX_TYPE,
        box: box
    }
}

export function updateBoxTypeAction(box){
    return {
        type: UPDATE_BOX_TYPE,
        box: box
    }
}

export function deleteBoxTypeAction(id){
    return {
        type: DELETE_BOX_TYPE,
        id: id
    }
}

export function getAllBoxTypesAction(){
    return {
        type: GET_ALL_BOX_TYPES
    }
}