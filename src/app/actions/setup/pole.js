export const ADD_POLE = "ADD_POLE";
export const ADD_POLE_SUCCESS = "ADD_POLE_SUCCESS";
export const ADD_POLE_FAIL = "ADD_POLE_FAIL";

export const UPDATE_POLE = "UPDATE_POLE";
export const UPDATE_POLE_SUCCESS = "UPDATE_POLE_SUCCESS";
export const UPDATE_POLE_FAIL = "UPDATE_POLE_FAIL";

export const DELETE_POLE = "DELETE_POLE";
export const DELETE_POLE_SUCCESS = "DELETE_POLE_SUCCESS";
export const DELETE_POLE_FAIL = "DELETE_POLE_FAIL";

export const GET_POLES = "GET_POLES";
export const GET_POLES_SUCCESS = "GET_POLES_SUCCESS";
export const GET_POLES_FAIL = "GET_POLES_FAIL";

export const GET_ALL_POLES = "GET_ALL_POLES";
export const GET_ALL_POLES_SUCCESS = "GET_ALL_POLES_SUCCESS";
export const GET_ALL_POLES_FAIL = "GET_ALL_POLES_FAIL";

export function addPoleAction(pole) {
    return {
        type : ADD_POLE,
        pole: pole
    }
}

export function updatePoleAction(pole) {
    return {
        type : UPDATE_POLE,
        pole: pole
    }
}

export function deletePoleAction(id) {
    return {
        type : DELETE_POLE,
        id: id
    }
}

export function getPolesAction(pole) {
    return {
        type : GET_POLES,
        pole: pole
    }
}

export function getAllPolesAction() {
    return {
        type : GET_ALL_POLES
    }
}