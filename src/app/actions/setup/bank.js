export const ADD_BANK = "ADD_BANK";
export const ADD_BANK_SUCCESS = "ADD_BANK_SUCCESS";
export const ADD_BANK_FAIL = "ADD_BANK_FAIL";

export const GET_ALL_BANKS = "GET_ALL_BANKS";
export const GET_ALL_BANKS_SUCCESS = "GET_ALL_BANKS_SUCCESS";
export const GET_ALL_BANKS_FAIL = "GET_ALL_BANKS_FAIL";

export const UPDATE_BANK = "UPDATE_BANK";
export const UPDATE_BANK_SUCCESS = "UPDATE_BANK_SUCCESS";
export const UPDATE_BANK_FAIL = "UPDATE_BANK_FAIL";

export const DELETE_BANK = "DELETE_BANK";
export const DELETE_BANK_SUCCESS = "DELETE_BANK_SUCCESS";
export const DELETE_BANK_FAIL = "DELETE_BANK_FAIL";

export function addBankAction(bank){
    return {
        type: ADD_BANK,
        bank: bank
    }
}

export function getAllBanksAction(){
    return {
        type: GET_ALL_BANKS
    }
}

export function updateBankAction(bank){
    return {
        type: UPDATE_BANK,
        bank: bank
    }
}

export function deleteBankAction(id){
    return {
        type: DELETE_BANK,
        id: id
    }
}