export const ADD_TRANSFORMER = "ADD_TRANSFORMER";
export const ADD_TRANSFORMER_SUCCESS = "ADD_TRANSFORMER_SUCCESS";
export const ADD_TRANSFORMER_FAIL = "ADD_TRANSFORMER_FAIL";

export const UPDATE_TRANSFORMER = "UPDATE_TRANSFORMER";
export const UPDATE_TRANSFORMER_SUCCESS = "UPDATE_TRANSFORMER_SUCCESS";
export const UPDATE_TRANSFORMER_FAIL = "UPDATE_TRANSFORMER_FAIL";

export const GET_ALL_TRANSFORMERS = "GET_ALL_TRANSFORMERS";
export const GET_ALL_TRANSFORMERS_SUCCESS = "GET_ALL_TRANSFORMERS_SUCCESS";
export const GET_ALL_TRANSFORMERS_FAIL = "GET_ALL_TRANSFORMERS_FAIL";

export const DELETE_TRANSFORMER="DELETE_TRANSFORMER";
export const DELETE_TRANSFORMER_SUCCESS = "DELETE_TRANSFORMER_SUCCESS";
export const DELETE_TRANSFORMER_FAIL = "DELETE_TRANSFORMER_FAIL";

export const GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY = "GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY";
export const GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_SUCCESS = "GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_SUCCESS";
export const GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_FAIL = "GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_FAIL";


export function addTransformerAction(transformer) {
    return {
        type : ADD_TRANSFORMER,
        transformer: transformer
    }
}

export function updateTransformerAction(transformer) {
    return {
        type : UPDATE_TRANSFORMER,
        transformer: transformer
    }
}

export function deleteTransformerAction(id) {
    return {
        type : DELETE_TRANSFORMER,
        id: id
    }
}

export function getAllTransformersAction(transformer) {
    return {
        type : GET_ALL_TRANSFORMERS,
        transformer: transformer
    }
}

export function getAllTransformerForKhwMonthlyAction(){
    return {
        type : GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY
    }
}


