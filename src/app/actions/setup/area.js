export const ADD_AREA = "ADD_AREA";
export const ADD_AREA_SUCCESS = "ADD_AREA_SUCCESS";
export const ADD_AREA_FAIL = "ADD_AREA_FAIL";

export const GET_ALL_AREAS = "GET_ALL_AREAS";
export const GET_ALL_AREAS_SUCCESS = "GET_ALL_AREAS_SUCCESS";
export const GET_ALL_AREAS_FAIL = "GET_ALL_AREAS_FAIL";

export const GET_AREAS = "GET_AREAS";
export const GET_AREAS_SUCCESS = "GET_AREAS_SUCCESS";
export const GET_AREAS_FAIL = "GET_AREAS_FAIL";

export const GET_AREA_REPORTS = "GET_AREA_REPORTS";
export const GET_AREA_REPORTS_SUCCESS = "GET_AREA_REPORTS_SUCCESS";
export const GET_AREA_REPORTS_FAIL = "GET_AREA_REPORTS_FAIL";

export const UPDATE_AREA = "UPDATE_AREA";
export const UPDATE_AREA_SUCCESS = "UPDATE_AREA_SUCCESS";
export const UPDATE_AREA_FAIL = "UPDATE_AREA_FAIL";

export const DELETE_AREA = "DELETE_AREA";
export const DELETE_AREA_SUCCESS = "DELETE_AREA_SUCCESS";
export const DELETE_AREA_FAIL = "DELETE_AREA_FAIL";

export function addAreaAction(area){
    return {
        type: ADD_AREA,
        area: area
    }
}

export function getAllAreasAction(){
    return {
        type: GET_ALL_AREAS
    }
}

export function getAreaReportsAction(){
    return {
        type: GET_AREA_REPORTS
    }
}

export function updateAreaAction(area){
    return {
        type: UPDATE_AREA,
        area: area
    }
}

export function deleteAreaAction(id){
    return {
        type: DELETE_AREA,
        id: id
    }
}

export function getAreasAction(){
    return {
        type: GET_AREAS
    }
}