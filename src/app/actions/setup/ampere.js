export const ADD_AMPERE = "ADD_AMPERE";
export const ADD_AMPERE_SUCCESS = "ADD_AMPERE_SUCCESS";
export const ADD_AMPERE_FAIL = "ADD_AMPERE_FAIL";

export const GET_ALL_AMPERES = "GET_ALL_AMPERES";
export const GET_ALL_AMPERES_SUCCESS = "GET_ALL_AMPERES_SUCCESS";
export const GET_ALL_AMPERES_FAIL = "GET_ALL_AMPERES_FAIL";

export const UPDATE_AMPERE = "UPDATE_AMPERE";
export const UPDATE_AMPERE_SUCCESS = "UPDATE_AMPERE_SUCCESS";
export const UPDATE_AMPERE_FAIL = "UPDATE_AMPERE_FAIL";

export const DELETE_AMPERE = "DELETE_AMPERE";
export const DELETE_AMPERE_SUCCESS = "DELETE_AMPERE_SUCCESS";
export const DELETE_AMPERE_FAIL = "DELETE_AMPERE_FAIL";

export function addAmpereAction(ampere){
    return {
        type: ADD_AMPERE,
        ampere: ampere
    }
}

export function getAllAmperesAction(id){
    return {
        type: GET_ALL_AMPERES,
        id: id
    }
}

export function updateAmpereAction(ampere){
    return {
        type: UPDATE_AMPERE,
        ampere: ampere
    }
}

export function deleteAmpereAction(id){
    return {
        type: DELETE_AMPERE,
        id: id
    }
}