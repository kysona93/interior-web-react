export const ADD_OCCUPATION = "ADD_OCCUPATION";
export const ADD_OCCUPATION_SUCCESS = "ADD_OCCUPATION_SUCCESS";
export const ADD_OCCUPATION_FAIL = "ADD_OCCUPATION_FAIL";

export const GET_ALL_OCCUPATIONS = "GET_ALL_OCCUPATIONS";
export const GET_ALL_OCCUPATIONS_SUCCESS = "GET_ALL_OCCUPATIONS_SUCCESS";
export const Get_ALL_OCCUPATIONS_FAIL = "Get_ALL_OCCUPATIONS_FAIL";

export const UPDATE_OCCUPATION = "UPDATE_OCCUPATION";
export const UPDATE_OCCUPATION_SUCCESS = "UPDATE_OCCUPATION_SUCCESS";
export const UPDATE_OCCUPATION_FAIL = "UPDATE_OCCUPATION_FAIL";

export const DELETE_OCCUPATION = "DELETE_OCCUPATION";
export const DELETE_OCCUPATION_SUCCESS = "DELETE_OCCUPATION_SUCCESS";
export const DELETE_OCCUPATION_FAIL = "DELETE_OCCUPATION_FAIL";

export function addOccupationAction(occupation){
    return {
        type: ADD_OCCUPATION,
        occupation: occupation
    }
}

export function updateOccupationAction(occupation){
    return {
        type: UPDATE_OCCUPATION,
        occupation: occupation
    }
}

export function deleteOccupationAction(id){
    return {
        type: DELETE_OCCUPATION,
        id: id
    }
}


export function getAllOccupationsAction(){
    return {
        type: GET_ALL_OCCUPATIONS
    }
}