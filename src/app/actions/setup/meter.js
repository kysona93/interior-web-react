export const ADD_METER = "ADD_METER";
export const ADD_METER_SUCCESS = "ADD_METER_SUCCESS";
export const ADD_METER_FAIL = "ADD_METER_FAIL";

export const UPDATE_METER = "UPDATE_METER";
export const UPDATE_METER_SUCCESS = "UPDATE_METER_SUCCESS";
export const UPDATE_METER_FAIL = "UPDATE_METER_FAIL";

export const DELETE_METER = "DELETE_METER";
export const DELETE_METER_SUCCESS = "DELETE_METER_SUCCESS";
export const DELETE_METER_FAIL = "DELETE_METER_FAIL";

export const GET_METERS = "GET_METERS";
export const GET_METERS_SUCCESS = "GET_METERS_SUCCESS";
export const GET_METERS_FAIL = "GET_METERS_FAIL";

export const GET_ALL_METERS = "GET_ALL_METERS";
export const GET_ALL_METERS_SUCCESS = "GET_ALL_METERS_SUCCESS";
export const GET_ALL_METERS_FAIL = "GET_ALL_METERS_FAIL";

export const GET_METER_BY_METER_SERIAL = "GET_METER_BY_METER_SERIAL";
export const GET_METER_BY_METER_SERIAL_SUCCESS = "GET_METER_BY_METER_SERIAL_SUCCESS";
export const GET_METER_BY_METER_SERIAL_FAIL = "GET_METER_BY_METER_SERIAL_FAIL";

export const REUSE_METER = "REUSE_METER";
export const REUSE_METER_SUCCESS = "REUSE_METER_SUCCESS";
export const REUSE_METER_FAIL = "REUSE_METER_FAIL";

export function addMeterAction(meter) {
    return {
        type : ADD_METER,
        meter: meter
    }
}

export function updateMeterAction(meter) {
    return {
        type : UPDATE_METER,
        meter: meter
    }
}

export function deleteMeterAction(id) {
    return {
        type : DELETE_METER,
        id: id
    }
}

export function getMetersAction(meter) {
    return {
        type : GET_METERS,
        meter: meter
    }
}

export function getAllMetersAction() {
    return {
        type : GET_ALL_METERS
    }
}

export function getMeterByMeterSerialAction(meter) {
    return {
        type : GET_METER_BY_METER_SERIAL,
        meter: meter
    }
}


export function reuseMeterPostPaidAction(meter) {
    return {
        type : REUSE_METER,
        meter: meter
    }
}