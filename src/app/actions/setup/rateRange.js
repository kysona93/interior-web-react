export const ADD_RATE_RANGE = "ADD_RATE_RANGE";
export const ADD_RATE_RANGE_SUCCESS = "ADD_RATE_RANGE_SUCCESS";
export const ADD_RATE_RANGE_FAIL = "ADD_RATE_RANGE_FAIL";

export const GET_ALL_RATE_RANGE = "GET_ALL_RATE_RANGE";
export const GET_ALL_RATE_RANGE_SUCCESS = "GET_ALL_RATE_RANGE_SUCCESS";
export const GET_ALL_RATE_RANGE_FAIL = "GET_ALL_RATE_RANGE_FAIL";

export const UPDATE_RATE_RANGE = "UPDATE_RATE_RANGE";
export const UPDATE_RATE_RANGE_SUCCESS = "UPDATE_RATE_RANGE_SUCCESS";
export const UPDATE_RATE_RANGE_FAIL = "UPDATE_RATE_RANGE_FAIL";

export const DELETE_RATE_RANGE = "DELETE_RATE_RANGE";
export const DELETE_RATE_RANGE_SUCCESS = "DELETE_RATE_RANGE_SUCCESS";
export const DELETE_RATE_RANGE_FAIL = "DELETE_RATE_RANGE_FAIL";

export function addRateRangeAction(rate){
    return {
        type: ADD_RATE_RANGE,
        rate: rate
    }
}

export function getAllRateRangesAction(){
    return {
        type: GET_ALL_RATE_RANGE
    }
}

export function updateRateRangeAction(rate){
    return {
        type: UPDATE_RATE_RANGE,
        rate: rate
    }
}

export function deleteRateRangeAction(id){
    return {
        type: DELETE_RATE_RANGE,
        id: id
    }
}