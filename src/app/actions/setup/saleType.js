export const ADD_SALE_TYPE = "ADD_SALE_TYPE";
export const ADD_SALE_TYPE_SUCCESS = "ADD_SALE_TYPE_SUCCESS";
export const ADD_SALE_TYPE_FAIL = "ADD_SALE_TYPE_FAIL";

export const GET_ALL_SALE_TYPE = "GET_ALL_SALE_TYPE";
export const GET_ALL_SALE_TYPE_SUCCESS = "GET_ALL_SALE_TYPE_SUCCESS";
export const GET_ALL_SALE_TYPE_FAIL = "GET_ALL_SALE_TYPE_FAIL";

export const UPDATE_SALE_TYPE = "UPDATE_SALE_TYPE";
export const UPDATE_SALE_TYPE_SUCCESS = "UPDATE_SALE_TYPE_SUCCESS";
export const UPDATE_SALE_TYPE_FAIL = "UPDATE_SALE_TYPE_FAIL";

export const DELETE_SALE_TYPE = "DELETE_SALE_TYPE";
export const DELETE_SALE_TYPE_SUCCESS = "DELETE_SALE_TYPE_SUCCESS";
export const DELETE_SALE_TYPE_FAIL = "DELETE_SALE_TYPE_FAIL";

/* ADD SALE TYPE */
export function addSaleTypeAction(sale){
    return {
        type: ADD_SALE_TYPE,
        sale: sale
    }
}

/* LIST SALE TYPE */
export function getAllSaleTypesAction(){
    return {
        type: GET_ALL_SALE_TYPE
    }
}

/* UPDATE SALE TYPE */
export function updateSaleTypeAction(sale){
    return {
        type: UPDATE_SALE_TYPE,
        sale: sale
    }
}

/* DELETE SALE TYPE */
export function deleteSaleTypeAction(id){
    return {
        type: DELETE_SALE_TYPE,
        id: id
    }
}