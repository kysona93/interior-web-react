export const ADD_METER_PHASE = "ADD_METER_PHASE";
export const ADD_METER_PHASE_SUCCESS = "ADD_METER_PHASE_SUCCESS";
export const ADD_METER_PHASE_FAIL = "ADD_METER_PHASE_FAIL";

export const GET_ALL_METER_PHASE = "GET_ALL_METER_PHASE";
export const GET_ALL_METER_PHASE_SUCCESS = "GET_ALL_METER_PHASE_SUCCESS";
export const GET_ALL_METER_PHASE_FAIL = "GET_ALL_METER_PHASE_FAIL";

export const UPDATE_METER_PHASE = "UPDATE_METER_PHASE";
export const UPDATE_METER_PHASE_SUCCESS = "UPDATE_METER_PHASE_SUCCESS";
export const UPDATE_METER_PHASE_FAIL = "UPDATE_METER_PHASE_FAIL";

export const DELETE_METER_PHASE = "DELETE_METER_PHASE";
export const DELETE_METER_PHASE_SUCCESS = "DELETE_METER_PHASE_SUCCESS";
export const DELETE_METER_PHASE_FAIL = "DELETE_METER_PHASE_FAIL";

export function addMeterPhaseAction(phase){
    return {
        type: ADD_METER_PHASE,
        phase: phase
    }
}

export function listMeterPhasesAction(){
    return {
        type: GET_ALL_METER_PHASE
    }
}

export function updateMeterPhaseAction(phase){
    return {
        type: UPDATE_METER_PHASE,
        phase: phase
    }
}

export function deleteMeterPhaseAction(id){
    return {
        type: DELETE_METER_PHASE,
        id: id
    }
}