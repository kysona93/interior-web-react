export const ADD_RATE_PER_KWH = "ADD_RATE_PER_KWH";
export const ADD_RATE_PER_KWH_SUCCESS = "ADD_RATE_PER_KWH_SUCCESS";
export const ADD_RATE_PER_KWH_FAIL = "ADD_RATE_PER_KWH_FAIL";

export const GET_ALL_RATE_PER_KWH = "GET_ALL_RATE_PER_KWH";
export const GET_ALL_RATE_PER_KWH_SUCCESS = "GET_ALL_RATE_PER_KWH_SUCCESS";
export const GET_ALL_RATE_PER_KWH_FAIL = "GET_ALL_RATE_PER_KWH_FAIL";

export const UPDATE_RATE_PER_KWH = "UPDATE_RATE_PER_KWH";
export const UPDATE_RATE_PER_KWH_SUCCESS = "UPDATE_RATE_PER_KWH_SUCCESS";
export const UPDATE_RATE_PER_KWH_FAIL = "UPDATE_RATE_PER_KWH_FAIL";

export const DELETE_RATE_PER_KWH = "DELETE_RATE_PER_KWH";
export const DELETE_RATE_PER_KWH_SUCCESS = "DELETE_RATE_PER_KWH_SUCCESS";
export const DELETE_RATE_PER_KWH_FAIL = "DELETE_RATE_PER_KWH_FAIL";

export function addRatePerKwhAction(rate){
    return {
        type : ADD_RATE_PER_KWH,
        rate: rate
    }
}

export function getAllRatePerKwhAction(){
    return {
        type: GET_ALL_RATE_PER_KWH
    }
}

export function updateRatePerKwhAction(rate){
    return {
        type: UPDATE_RATE_PER_KWH,
        rate: rate
    }
}

export function deleteRatePerKwhAction(id){
    return {
        type: DELETE_RATE_PER_KWH,
        id: id
    }
}