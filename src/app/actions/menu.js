export const GET_ALL_MENUS = "GET_ALL_MENUS";
export const GET_ALL_MENUS_SUCCESS = "GET_ALL_MENUS_SUCCESS";
export const GET_ALL_MENUS_FAIL = "GET_ALL_MENUS_FAIL";

export const GET_MENUS = "GET_MENUS";
export const GET_MENUS_SUCCESS = "GET_MENUS_SUCCESS";
export const GET_MENUS_FAIL = "GET_MENUS_FAIL";

export const GET_MENU = "GET_MENU";
export const GET_MENU_SUCCESS = "GET_MENU_SUCCESS";
export const GET_MENU_FAIL= "GET_MENU_FAIL";

export const UPDATE_MENU = "UPDATE_MENU";
export const UPDATE_MENU_SUCCESS = "UPDATE_MENU_SUCCESS";
export const UPDATE_MENU_FAIL = "UPDATE_MENU_FAIL";

export const DELETE_MENU = "DELETE_MENU";
export const DELETE_MENU_SUCCESS = "DELETE_MENU_SUCCESS";
export const DELETE_MENU_FAIL = "DELETE_MENU_FAIL";

export const LIST_ALL_PAGES = "LIST_ALL_PAGES";
export const LIST_ALL_PAGES_SUCCESS = "LIST_ALL_PAGES_SUCCESS";
export const LIST_ALL_PAGES_FAIL = "LIST_ALL_PAGES_FAIL";

export const LIST_PAGES_BY_MENUS = "LIST_PAGES_BY_MENUS";
export const LIST_PAGES_BY_MENUS_SUCCESS = "LIST_PAGES_BY_MENUS_SUCCESS";
export const LIST_PAGES_BY_MENUS_FAIL = "LIST_PAGES_BY_MENUS_FAIL";

export const ASSIGN_MENUS = "ASSIGN_MENUS";
export const ASSIGN_MENUS_SUCCESS = "ASSIGN_MENUS_SUCCESS";
export const ASSIGN_MENUS_FAIL = "ASSIGN_MENUS_FAIL";

export const GET_MENUS_BY_GROUP = "GET_MENUS_BY_GROUP";
export const GET_MENUS_BY_GROUP_SUCCESS = "GET_MENUS_BY_GROUP_SUCCESS";
export const GET_MENUS_BY_GROUP_FAIL = "GET_MENUS_BY_GROUP_FAIL";

export const GET_MENUS_LIST_AND_PAGE = "GET_MENUS_LIST_AND_PAGE";
export const GET_MENUS_LIST_AND_PAGE_SUCCESS = "GET_MENUS_LIST_AND_PAGE_SUCCESS";
export const GET_MENUS_LIST_AND_PAGE_FAIL = "GET_MENUS_LIST_AND_PAGE_FAIL";


/* GET ALL MENUS */
export function getAllMenusAction(){
    return {
        type : GET_ALL_MENUS
    }
}

/* GET MENUS */
export function getMenuAction(){
    return {
        type: GET_MENUS
    }
}

/* GET MENU DETAIL */
export function getMenuDetailAction(id){
    return{
        type: GET_MENU,
        id: id
    }
}

/* UPDATE MENU */
export function updateMenuAction(menu){
    return {
        type: UPDATE_MENU,
        menu: menu
    }
}

/* DELETE MENU */
export function actionDeleteMenu(id){
    return {
        type: DELETE_MENU,
        id: id
    }
}

/* LIST ALL PAGES */
export function actionListAllPages(){
    return {
        type: LIST_ALL_PAGES
    }
}

/* LIST PAGES BY MENUS */
export function actionListPagesByMenus(ids){
    return {
        type: LIST_PAGES_BY_MENUS,
        ids: ids
    }
}


/* ASSIGN MENU */
export function assignMenusAction(menu){
    return {
        type: ASSIGN_MENUS,
        menu: menu
    }
}

/* GET MENUS BY GROUP */
export function getMenusByGroupAction(id){
    return{
        type: GET_MENUS_BY_GROUP,
        id: id
    }
}

/* GET MENUS LIST AND PAGE*/
export function getMenusListAndPageAction(menu){
    return {
        type: GET_MENUS_LIST_AND_PAGE,
        menu: menu
    }
}