import React from 'react';
import { connect } from 'react-redux';
import { saveAuth } from './../../../auth';
import { bindActionCreators } from 'redux';
import { Row, Col, Image, Panel } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { browserHistory } from 'react-router';
import { TextBox } from './../../components/forms/TextBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import { userLoginAction } from '../../actions/authentication';
import SweetAlert from 'sweetalert-react';
import './index.css';


class LogIn extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillReceiveProps(props){
        if(props.userLogin.status == 200){
            saveAuth(props.userLogin.data.token);
            window.location.href = "/app";
        }
        if(props.userLogin.status == 401){
           alert("Username or Password not correct!");
           return props.userLogin.status = 0;
        }
    }

    handleSubmit(values){
        let user={
            "username" : values.username,
            "password" : values.password
        };
        this.props.userLoginAction(user);
    }

    render(){
        const { handleSubmit, error, invalid, submitting } = this.props;
        return(
            <div className="wrap-login">
                <div className="login">
                    <div className="login-triangle"></div>
                    <h2 className="login-header">Log in</h2>
                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="login-container p">
                        <Field name="username" type="text" component={TextBox} label="Username"/>
                        <Field name="password" type="password" component={TextBox} label="Password"/>
                        <ButtonSubmit label="Login"/>
                    </form>
                </div>
            </div>
        )
    }
}
 LogIn = reduxForm({
    form: 'form_log_in',
     validate: (values) => {
         let regex_username = /[0-9a-zA-Z]{4,20}/;
         let regex_password = /[0-9a-zA-Z]{4,20}/;

         const errors = {};
         if (!regex_username.test(values.username) || values.username === undefined) {
             errors.username = 'Invalid username!';
         }
         if(!regex_password.test(values.password) || values.password === undefined){
             errors.password = 'Password is required at least 6 characters!';
         }
         return errors
     }
})(LogIn);
function mapStateToProps(state) {
    return {
        userLogin : state.userAuthentication.userLogin
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({userLoginAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(LogIn) ;