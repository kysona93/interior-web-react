import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Image, Panel } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from './../../components/forms/TextBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import { userLoginAction } from '../../actions/authentication';
import SweetAlert from 'sweetalert-react';
import './../../../../node_modules/sweetalert/dist/sweetalert.css';

class SignIn extends React.Component {
    constructor(props){
        super(props)
        this.state ={
            error : false
        };
    }

    componentWillReceiveProps(data){
        if(data.userLogin.status == 401){
            this.setState({error : true});
        }else{
            this.setState({error : false});
        }
        if(data.userLogin.token != undefined){
            window.location.href = "/app";
        }
    }

    handleSubmit(values){
        let user={
            "username" : values.username,
            "password" : values.password
        };
        sessionStorage.setItem("username",values.username);
        this.props.userLoginAction(user);
    }

    render(){
        const { handleSubmit, error, invalid, submitting } = this.props;
        return(
            <div>
                <br/><br/>
                <Row>
                    <Col xs={4} sm={4} md={4} lg={4}></Col>
                    <Col xs={4} sm={4} md={4} lg={4}>
                        <center>
                            <Image src="/icons/bnd_copyright.png" responsive />
                            <br/>
                            <p style={{color:'blue'}}>Copyright © 2010 BnD Solutions Co., Ltd.</p>
                            <p style={{color:'blue'}}>All Rights Reserved.</p>
                        </center>
                        <br/><br/>
                        <Panel header={"Login Form"} bsStyle="primary">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row className="wrap-row-form-login">
                                    <Col sm={12} xs={12} md={12} lg={12} className="login-page">
                                        <Field name="username" type="text" component={TextBox} label="Username"/>
                                    </Col>
                                </Row>
                                <Row className="wrap-row-form-login">
                                    <Col sm={12} xs={12} md={12} lg={12} className="login-page">
                                        <Field name="password" type="password" component={TextBox} label="Password"/>
                                    </Col>
                                </Row>
                                <Row className="wrap-row-form-login">
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page"></Col>
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page">
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Login" />
                                    </Col>
                                    <Col sm={4} xs={4} md={4} lg={4} className="login-page"></Col>
                                </Row>
                                </form>
                                <SweetAlert
                                    show={this.state.error}
                                    type="error"
                                    title="Something went wrong"
                                    text="Username or Password is incorrect!"
                                    confirmButtonColor="#ff5a00"
                                    onConfirm={() => { location.href = "/"; }}
                                />
                        </Panel>
                    </Col>
                    <Col xs={4} sm={4} md={4} lg={4}></Col>
                </Row>
            </div>
        )
    }
}

const FormSignIn = reduxForm({
    form: 'form_sign_in',
    validate: (values) => {
        let regex_username = /[0-9a-zA-Z]{4,20}/;
        let regex_password = /[0-9a-zA-Z]{4,20}/;

        const errors = {};
        if (!regex_username.test(values.username) || values.username == undefined) {
            errors.username = 'Invalid username!';
        }
        if(!regex_password.test(values.password) || values.password == undefined){
            errors.password = 'Password is required at least 6 characters!';
        }
        return errors
    }
})(SignIn);

function mapStateToProps(state) {
    sessionStorage.setItem("token",state.userAuthentication.userLogin.token);
    return {
        userLogin : state.userAuthentication.userLogin
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({userLoginAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(FormSignIn) ;