import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Form, FormGroup, Button, Label } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllMailSettingsAction, 
        addMailSettingsAction, 
        updateMailSettingsAction, 
        deleteMailSettingsAction } from './../../../actions/security/mailSettings';
import { canCreate, canUpdate, canDelete } from './../../../../auth';

let id = 0;

class ListMailSettings extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isEdit: false,
            label: 'Save',
            labelForm: 'Add Mail Receiver',
        }
    }

    componentWillMount(){
        this.props.getAllMailSettingsAction();
    }
    

    componentWillReceiveProps(newProps){
        // add mail
        if(newProps.mailSettingsAdd.status === 200){
            alert("Successfully add mail settings!");
            this.props.getAllMailSettingsAction();
            return newProps.mailSettingsAdd.status = 0;
        }
        if(newProps.mailSettingsAdd.status === 500 || newProps.mailSettingsAdd.status === 400){
            alert("Fail with add mail settings!");
            return newProps.mailSettingsAdd.status = 0;
        }
        // update mail
        if(newProps.mailSettingsUpdate.status === 200){
            alert("Successfully update mail settings!");
            this.props.getAllMailSettingsAction();
            return newProps.mailSettingsUpdate.status = 0;
        }
        if(newProps.mailSettingsUpdate.status === 500 || newProps.mailSettingsUpdate.status === 400){
            alert("Fail with update mail settings!");
            return newProps.mailSettingsUpdate.status = 0;
        }
        // delete mail
        if(newProps.mailSettingsDelete.status === 200){
            alert("Successfully delete mail settings!");
            this.props.getAllMailSettingsAction();
            return newProps.mailSettingsDelete.status = 0;
        }
        if(newProps.mailSettingsDelete.status === 500 || newProps.mailSettingsDelete.status === 400){
            alert("Fail with delete mail settings!");
            return newProps.mailSettingsDelete.status = 0;
        }
    }

    handleSubmit(values){
        if(!this.state.isEdit){
            // add
            let mail = {
                "name": values.name,
                "email": values.email
            };
            this.props.addMailSettingsAction(mail);
        } else {
            // edit
            let mail = {
                "id": id,
                "name": values.name,
                "email": values.email
            };
            this.props.updateMailSettingsAction(mail);
        }
    }

    handleEditMailReceiver(value) {
        id = value.id;
        this.setState({
            isEdit: true, 
            label: 'Edit', 
            labelForm: 'Edit Mail Receiver'
        });

        this.props.dispatch(initialize('form_mail_settings_add', value));
    }

    handleDeleteMailReceiver(id){
        if(confirm("Are you sure to delete mail receiver?") == true){
            this.props.deleteMailSettingsAction(id);
        }
    }

    handleCancel(){
        this.setState({isEdit: false, label: 'Save', labelForm: 'Add Mail Receiver'});
        this.props.dispatch(initialize('form_mail_settings_add', null));
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;

        return (
            <div className="margin_left_25 margin_top_minus_30">
                <Row>
                    <Col md={12}>
                        <h3 className="text-align-center">Mail Settings</h3>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={11}>
                        <div className="">
                            <Row>
                                <Col md={6}>
                                    {
                                        canCreate == true ?
                                            <div className="panel panel-default">
                                                <div className="panel-heading">
                                                    <h3 className="panel-title">{this.state.labelForm}</h3>
                                                </div>
                                                <div className="panel-body">
                                                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                                        <Row>
                                                            <Col md={4} lg={4} className="label-name">
                                                                <strong>Receiver Name <span className="label-require">*</span></strong>
                                                            </Col>
                                                            <Col md={8} lg={8}>
                                                                <Field 
                                                                    name="name" 
                                                                    type="text" 
                                                                    component={TextBox} 
                                                                    label="Receiver Name" 
                                                                    icon="fa fa-user-o"/>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={4} lg={4} className="label-name">
                                                                <strong>Email Address <span className="label-require">*</span></strong>
                                                            </Col>
                                                            <Col md={8} lg={8}>
                                                                <Field 
                                                                    name="email" 
                                                                    type="text" 
                                                                    component={TextBox} 
                                                                    label="Email Address" 
                                                                    icon="fa fa-at"/>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={3} className="pull-right col-padding-left-0">
                                                                <ButtonSubmit 
                                                                    error={error} 
                                                                    invalid={invalid} 
                                                                    submitting={submitting} 
                                                                    label={this.state.label} />
                                                            </Col>
                                                            {
                                                                this.state.label == "Edit" ?
                                                                    <Col md={3} className="pull-right">
                                                                        <Button className="bt-cancel" onClick={() => this.handleCancel()}>Cancel</Button>
                                                                    </Col>
                                                                : null
                                                            }
                                                        </Row>
                                                    </form>
                                                </div>
                                            </div>
                                        : null
                                    }
                                </Col>
                            </Row>
                            <br/>

                            <table className="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Email Address</th>
                                        <th>Receiver Name</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                { this.props.mailSettingsAll.status == 200 ?
                                    this.props.mailSettingsAll.data.items.map((mail, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{index+1}</td>
                                                <td>{mail.email}</td>
                                                <td>{mail.name}</td>
                                                <td className="text-center">
                                                    {
                                                        canUpdate == true ? 
                                                            <a onClick={() => this.handleEditMailReceiver(mail)} title="Edit" className='btn btn-info btn-xs'>
                                                                <span className="glyphicon glyphicon-edit"></span>
                                                            </a>
                                                        : null
                                                    }
                                                    {
                                                        canDelete == true ? 
                                                            <a onClick={() => this.handleDeleteMailReceiver(mail.id)} title="Delete" className='btn btn-danger btn-xs'>
                                                                <span className="glyphicon glyphicon-trash"></span>
                                                            </a>
                                                        : null
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    })
                                : <tr>
                                    <td colSpan={4} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                </tr> }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

ListMailSettings = reduxForm({
    form: 'form_mail_settings_add',
    validate: (values) => {
        let regex_name = /[a-zA-Z]{3,20}/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const errors = {};

        if (!regex_name.test(values.name) || values.name === undefined) {
            errors.name = 'Invalid Receiver Name!';
        }
        if (!regex_email.test(values.email) || values.email === undefined) {
            errors.email = 'Invalid Email Mail Receiver!';
        }
        return errors
    }
})(ListMailSettings);

function mapStateToProps(state) {
    return ({
        mailSettingsAll: state.mailSettings.mailSettingsAll,
        mailSettingsAdd: state.mailSettings.mailSettingsAdd,
        mailSettingsUpdate: state.mailSettings.mailSettingsUpdate,
        mailSettingsDelete: state.mailSettings.mailSettingsDelete
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllMailSettingsAction,
        addMailSettingsAction,
        updateMailSettingsAction,
        deleteMailSettingsAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMailSettings);