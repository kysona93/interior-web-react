import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllBillBalancePrepaidAction, voidBalancePrepaidKwAction } from './../../../actions/transaction/prepaid/balancePrepaidKwh';
import { issueBillPrepaidAction } from './../../../actions/transaction/prepaid/returnInvoice';
import moment from 'moment';
import ReactLoading from 'react-loading';

let allBills = [];
let issueData = [];

let issueDate = '';
let startDate = '';
let endDate = '';
class IssueBillPrepaid extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            problem: false,
            issueDate: null,
            startDate: null,
            endDate: null,
            issueCheckAll: false
        };
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleCheckboxIssueAll = this.handleCheckboxIssueAll.bind(this);
        this.handleIssueCheck = this.handleIssueCheck.bind(this);
    }

    componentWillMount(){
        this.props.getAllBillBalancePrepaidAction();
    }

    componentWillReceiveProps(data){
        if(data.getAllBillBalancePrepaid.status === 200){
            document.getElementById('loading').style.display = 'none';
            allBills = data.getAllBillBalancePrepaid.data.items;
        }
        if(data.getAllBillBalancePrepaid.status === 401 || data.getAllBillBalancePrepaid.status === 404 || data.getAllBillBalancePrepaid.status === 500){
            document.getElementById('loading').style.display = 'none';
        }

        if(data.addIssueBillPrepaid.status === 200){
            if(data.addIssueBillPrepaid.data.status === 200) {
                document.getElementById('issue-bill').style.display = 'none';
                alert("Successfully issued customer bills");
                this.setState({issueCheckAll: false});
                $('.icheck').prop('checked', false);
                issueData = [];
                allBills = [];
                this.props.getAllBillBalancePrepaidAction();
                data.addIssueBillPrepaid.status = 0;
            } else {
                alert("Fail with issue customer bills");
                data.addIssueBillPrepaid.status = 0;
            }
        }
        if(data.addIssueBillPrepaid.status === 401 || data.addIssueBillPrepaid.status === 404 || data.addIssueBillPrepaid.status === 500){
            document.getElementById('issue-bill').style.display = 'none';
            alert("Fail with issue customer bills");
            this.setState({issueCheckAll: false});
            $('.icheck').prop('checked', false);
            issueData = [];
            allBills = [];
            this.props.getAllBillBalancePrepaidAction();
            data.addIssueBillPrepaid.status = 0;
        }

        if(data.voidBalancePrepaid.status === 200){
            alert("Successfully voided balance!");
            data.voidBalancePrepaid.status = 0;
        }
        if(data.voidBalancePrepaid.status === 500){
            alert("Fail with voided balance!");
            data.voidBalancePrepaid.status = 0;
        }
    }

    handleCheckboxIssueAll(){
        this.setState({issueCheckAll: !this.state.issueCheckAll});
        if(this.state.issueCheckAll === false) {
            // issue check all
            issueData = [];
            allBills.forEach((element) =>{
                issueData.push({
                    "balanceId" : Number(element.id),
                    "customerId": Number(element.customerId),
                    "prepaidId": element.prepaidId,
                    "oldBalance" : Number(element.oldBalance),
                    "newBalance" : Number(element.lastBalance ),
                    "oldUsage" : Number(element.oldUsage),
                    "newUsage" : Number(element.newUsage),
                    "coefficient" : Number(element.coefficient),
                    "issueDate": '',
                    "startDate": '',
                    "endDate": ''
                });
            });
        } else {
            // issue uncheck all
            issueData = [];
        }
        console.log("issue all: ", issueData);
    }
    handleIssueCheck(event) {
        let index = issueData.findIndex(element => element.customerId === Number(event.target.value));
        if(index === -1){
            // add issue customer
            for(let i=0; i< allBills.length; i++){
                if(allBills[i].customerId === Number(event.target.value)){
                    issueData.push({
                        "balanceId" : Number(allBills[i].id),
                        "customerId": Number(allBills[i].customerId),
                        "prepaidId": allBills[i].prepaidId,
                        "oldBalance" : Number(allBills[i].oldBalance),
                        "newBalance" : Number(allBills[i].lastBalance ),
                        "oldUsage" : Number(allBills[i].oldUsage),
                        "newUsage" : Number(allBills[i].newUsage),
                        "coefficient" : Number(allBills[i].coefficient),
                        "issueDate": '',
                        "startDate": '',
                        "endDate": ''
                    });
                    break;
                }
            }
            console.log("check issue customer :", issueData);
        }else{
            // uncheck issue customer
            issueData.splice(index,1);
            this.setState({issueCheckAll: false});
            console.log("uncheck issue customer :", issueData);
        }
    }

    handleIssueDate(date){
        this.setState({issueDate: date});
        issueDate = moment(date).format("YYYY-MM-DD");
    }
    handleStartDate(date){
        this.setState({startDate: date});
        startDate = moment(date).format("YYYY-MM-DD");
    }
    handleEndDate(date){
        this.setState({endDate: date});
        endDate = moment(date).format("YYYY-MM-DD");
    }
    handleSubmit(){
        if (issueData.length <= 0) {
            alert("Please select issue checkbox!");
        } else {
            document.getElementById('issue-bill').style.display = 'block';
            for(let i=0;i < issueData.length; i++){
                issueData[i].issueDate = issueDate;
                issueData[i].startDate = startDate;
                issueData[i].endDate = endDate;
            }
            console.log("before submit", issueData);
            this.props.issueBillPrepaidAction(issueData);
        }
    }

    handleVoidBalance(id){
        this.props.voidBalancePrepaidKwAction(id);
    }

    /* helper function */
    handleRefresh(){
        location.href = "/app/transactions/prepaid/issue-bill-prepaid";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#icheck").click(function () {
                $(".icheck").prop('checked', $(this).prop('checked'));
            });
        });
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Panel header="Issue Bill Pre Paid" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Issue Date <span className="label-require">*</span></strong>
                                        <Field name="fromDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.issueDate}
                                               handleChange={this.handleIssueDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Start Date <span className="label-require">*</span></strong>
                                        <Field name="startDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.startDate}
                                               handleChange={this.handleStartDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>End Date <span className="label-require">*</span></strong>
                                        <Field name="endDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.endDate}
                                               handleChange={this.handleEndDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3}>
                                        <br/>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Issue Selected Customer(s)" />
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div id="issue-bill" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                    <div>
                        {/* list properly customers */}
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <div className="pull-right">
                                    <Button bsStyle="primary" bsSize="small" onClick={() => this.handleRefresh()}>Refresh</Button>
                                </div>
                                <table className="page-permision-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Issue{' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.issueCheckAll}
                                                onChange={this.handleCheckboxIssueAll}
                                                id="icheck"
                                            />
                                        </th>
                                        <th>Customer ID</th>
                                        <th>Customer Name</th>
                                        <th>Meter Serial</th>
                                        <th>Pre Paid ID</th>
                                        <th>Generate Date</th>
                                        <th>Old Balance</th>
                                        <th>New Balance</th>
                                        <th>Last Usage</th>
                                        <th>New Usage</th>
                                        <th>Usage</th>
                                        <th>Last Read Date</th>
                                        {/*<th>Action</th>*/}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.getAllBillBalancePrepaid.status === 200 ?
                                            this.props.getAllBillBalancePrepaid.data.items.map((bill, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>
                                                            <input
                                                                type="checkbox"
                                                                value={bill.customerId}
                                                                onChange={this.handleIssueCheck}
                                                                className="icheck"
                                                                id={"i"+bill.customerId}
                                                            />
                                                        </td>
                                                        <td>{bill.customerId}</td>
                                                        <td>{bill.customerName}</td>
                                                        <td>{bill.meterSerial}</td>
                                                        <td>{bill.prepaidId}</td>
                                                        <td>{moment(bill.generateDate).format("DD-MM-YYYY")}</td>
                                                        <td>{bill.oldBalance}</td>
                                                        <td>{bill.lastBalance}</td>
                                                        <td>{bill.oldUsage}</td>
                                                        <td>{bill.newUsage}</td>
                                                        <td>{bill.newUsage - bill.oldUsage}</td>
                                                        <td>{moment(bill.dateBalance).format("DD-MM-YYYY")}</td>
                                                        {/*<td>*/}
                                                            {/*<a onClick={()=> this.handleVoidBalance(bill.id)} href="#" className="btn btn-danger btn-xs">*/}
                                                                {/*<span className="glyphicon glyphicon-remove"> </span>*/}
                                                            {/*</a>*/}
                                                        {/*</td>*/}
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan={12} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>
            </div>
        )
    }

}

IssueBillPrepaid = reduxForm({
    form: 'form_issue_bill_prepaid',
    validate: (values) => {
        const errors = {};
        if (values.issueDate === undefined) {
            errors.issueDate = 'Issue Date is required!';
        }
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if (values.endDate === undefined) {
            errors.endDate = 'End Date is required!';
        }
        return errors
    }
})(IssueBillPrepaid);

function mapStateToProps(state) {
    //console.log("DATA :",state.balancePrepaidKwh.getAllBillBalancePrepaid);
    return {
        getAllBillBalancePrepaid: state.balancePrepaidKwh.getAllBillBalancePrepaid,
        addIssueBillPrepaid: state.returnInvoices.addIssueBillPrepaid,
        voidBalancePrepaid: state.balancePrepaidKwh.voidBalancePrepaid
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllBillBalancePrepaidAction, issueBillPrepaidAction, voidBalancePrepaidKwAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(IssueBillPrepaid);