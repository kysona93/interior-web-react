import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { addBalancePrepaidKwhAction , getAllBalancePrepaidKwhAction, getAllNewPrepaidCustomersAction } from '../../../actions/transaction/prepaid/balancePrepaidKwh';
import moment from 'moment';
import ReactLoading from 'react-loading';
import '../style.css';

let allKws = [];
let issueKws = [];
let generateDate = "";
let dateBalance = "";
let totalDays = 0;
class GenerateBalanceKwh extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            generateDate : null,
            issueCheckAll: false,
            newCustomer: false,
            labelBtn: 'List New Customers'
        };
        this.handleCheckboxGenerateAll = this.handleCheckboxGenerateAll.bind(this);
        this.handleGenerateCheck = this.handleGenerateCheck.bind(this);
        dateBalance = moment(new Date(new Date().getFullYear(), new Date().getMonth(), 0)).format("YYYY-MM-DD"); // at the end of month
        totalDays = new Date(new Date().getFullYear(), new Date().getMonth(), 0).getDate(); // get total days of current month
    }

    componentDidMount(){
        this.props.getAllBalancePrepaidKwhAction({
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        });
    }

    componentWillReceiveProps(data){
        if(data.getAllBalancePrepaidKhw.status === 200){
            document.getElementById('loading').style.display = 'none';
            allKws = data.getAllBalancePrepaidKhw.data.items;
        }
        if(data.getAllBalancePrepaidKhw.status === 401 || data.getAllBalancePrepaidKhw.status === 404 || data.getAllBalancePrepaidKhw.status === 500){
            document.getElementById('loading').style.display = 'none';
        }

        if(data.addBalancePrepaidKhw.status === 200){
            alert("Successfully generate balance kwh");
            location.href = "/app/transactions/prepaid/generate-balance";
            // allKws = [];
            // data.addBalancePrepaidKhw.status = 0;
            // this.setState({issueCheckAll: false});
            // $('.icheck').prop('checked', false);
            // this.props.dispatch(initialize("form_generate_balance_prepaid",null));
        }
        if(data.addBalancePrepaidKhw.status === 401 || data.addBalancePrepaidKhw.status === 404 || data.addBalancePrepaidKhw.status === 500){
            alert("Fail with generate balance kwh");
            this.setState({issueCheckAll: false});
            $('.icheck').prop('checked', false);
            data.addBalancePrepaidKhw.status = 0;
        }

        if(data.getAllNewPrepaidCustomers.status === 200 || data.getAllNewPrepaidCustomers.status === 500 || data.getAllNewPrepaidCustomers.status === 404){
            document.getElementById('new-customers').style.display = 'none';
        }
    }

    handleSubmit(values){
        document.getElementById('loading').style.display = 'block';
        let criteria = {
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        };
        if(values.prepaidId !== undefined) criteria.prepaidId = values.prepaidId;
        if(values.customerName !== undefined) criteria.customerName = values.customerName;
        if(values.meterSerial !== undefined) criteria.meterSerial = values.meterSerial;
        if(values.transformer !== undefined) criteria.transformer = values.transformer;
        this.props.getAllBalancePrepaidKwhAction(criteria);
    }

    handleCheckboxGenerateAll(){
        this.setState({issueCheckAll: !this.state.issueCheckAll});
        if(this.state.issueCheckAll === false) {
            issueKws =[];
            allKws.forEach((element) => {
                // found by medium
                if(element.newBalance === null){
                    console.log("customer id medium: ", element.id);
                    issueKws.push({
                        "customerId": element.id,
                        "generateDate": generateDate,
                        "meterId": element.meterId,
                        "dateBalance": dateBalance,
                        "oldBalance": element.lastBalance, // old balance, new balance from previous invoice
                        "lastBalance": 0, // (element.curPower - (element.oldKwPerDay*totalDays)).toFixed(2)
                        "oldUsage": element.newUsage,
                        "newUsage": (element.newUsage + (element.oldKwPerDay*totalDays)).toFixed(2), // element.kwhPerDay
                        "prepaidId": element.prepaidId
                    });
                } else {
                    // found by balances
                    console.log("customer id balance : ", element.id);
                    issueKws.push({
                        "customerId": element.id,
                        "generateDate": generateDate,
                        "meterId": element.meterId,
                        "dateBalance": dateBalance,
                        "oldBalance": element.lastBalance, // old balance, new balance from previous invoice
                        "lastBalance": element.newBalance,
                        "oldUsage": element.newUsage,
                        "newUsage": (element.newUsage + (element.curPower + element.lastBalance - element.newBalance)).toFixed(2), // (element.newUsage + (element.curPower + element.oldBalance - element.newBalance)).toFixed(2)
                        "prepaidId": element.prepaidId
                    });
                }
            });
        } else {
            // generate uncheck all
            issueKws = [];
        }
        console.log("generate all: ", issueKws);
    }

    handleGenerateCheck(event) {
        let index = issueKws.findIndex(element => element.customerId === Number(event.target.value));
        if(index === -1) {
            // add generate kw
            for (let i = 0; i < allKws.length; i++) {
                if (allKws[i].id === Number(event.target.value)) {
                    if (allKws[i].newBalance === null) {
                        // found by medium
                        issueKws.push({
                            "customerId": allKws[i].id,
                            "generateDate": generateDate,
                            "meterId": allKws[i].meterId,
                            "dateBalance": dateBalance,
                            "oldBalance": allKws[i].lastBalance, // old balance, new balance from previous invoice
                            "lastBalance": 0, // (allKws[i].curPower - (allKws[i].oldKwPerDay*totalDays)).toFixed(2)
                            "oldUsage": allKws[i].newUsage,
                            "newUsage": (allKws[i].newUsage + (allKws[i].oldKwPerDay*totalDays)).toFixed(2), // element.kwhPerDay
                            "prepaidId": allKws[i].prepaidId
                        });
                    } else {
                        // found by balances
                        issueKws.push({
                            "customerId": allKws[i].id,
                            "generateDate": generateDate,
                            "meterId": allKws[i].meterId,
                            "dateBalance": dateBalance,
                            "oldBalance": allKws[i].lastBalance, // old balance, new balance from previous invoice
                            "lastBalance": allKws[i].newBalance,
                            "oldUsage": allKws[i].newUsage,
                            "newUsage": (allKws[i].newUsage + (allKws[i].curPower + allKws[i].lastBalance - allKws[i].newBalance)).toFixed(2), // (element.newUsage + (element.curPower + element.oldBalance - element.newBalance)).toFixed(2)
                            "prepaidId": allKws[i].prepaidId
                        });
                    }
                    break;
                }
            }
            console.log("generate kw :", issueKws);
        } else{
                // uncheck generate kw
                $('#i' + event.target.value).prop('checked', false);
                issueKws.splice(index, 1);
                this.setState({issueCheckAll: false});
                console.log("uncheck generate kw :", issueKws);
        }
    }

    handleGenerateDate(date){
        this.setState({generateDate: date});
        generateDate = moment(date).format("YYYY-MM-DD");
    }
    handleGenerate(){
        if(issueKws.length > 0){
            if(generateDate === ""){
                alert("Please select generate date");
            }else{
                for(let i=0; i< issueKws.length; i++){
                    issueKws[i].generateDate = generateDate;
                }
                console.log("Before submit : ",issueKws);
                this.props.addBalancePrepaidKwhAction(issueKws);
            }
        }else{
            alert("No data!");
        }
    }

    /* helper functions */
    getHistory(arr){
        if(arr !== null && arr !== undefined){
            let items = JSON.parse(arr);
            if(items.length === 1 ){
                return items[0].power;
            }else if(items.length === 2){
                return items[0].power+"/"+items[1].power;
            }else if(items.length === 3){
                return items[0].power+items[1].power + items[2].power;
            }else{
                return 0;
            }
        }else {
            return "";
        }
    }
    formatStrings(str){
        if(str !== null && str !== undefined){
            let items = JSON.parse(str);
            if(items.length === 1){
                return items[0];
            }else if(items.length === 2){
                return items[0]+"/"+items[1];
            }else {
                return items[0]+"/"+items[1]+"/"+items[2];
            }
        }else {
            return "";
        }
    }
    handleRefresh(){
        location.href = "/app/transactions/prepaid/generate-balance";
    }

    listNewCustomers(){
        if(this.state.newCustomer === false){
            // new customer
            document.getElementById('new-customers').style.display = 'block';
            this.setState({labelBtn: 'List Old Customers'});
            this.setState({newCustomer: true});
            this.props.getAllNewPrepaidCustomersAction();
        }else{
            // old customer
            this.setState({labelBtn: 'List New Customers'});
            this.setState({newCustomer: false});
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let percent = 0;
        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#icheck").click(function () {
                $(".icheck").prop('checked', $(this).prop('checked'));
            });
        });
        return(
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12} >
                        <Link to="/app/transactions/prepaid/verify-end-purchase">
                            <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back
                        </Link>
                        <Panel header="Generate Balances" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <br/>
                                <div className="search-by">
                                   <Col md={12} lg={12} className="wrap-search-by">
                                       <div className="bg-h4">
                                         <h4>Search By</h4>
                                       </div>
                                       <Row>
                                           <Col md={6} lg={6}>
                                               <Row>
                                                   <Col md={5} lg={5} className="label-name">  <strong>Customer Name</strong></Col>
                                                   <Col md={6} lg={6} className="label-name">
                                                       <Field name="customerName" type="text" component={TextBox} label="customer name"/>
                                                   </Col>
                                               </Row>
                                               <Row>
                                                   <Col md={5} lg={5} className="label-name">  <strong>Prepaid ID</strong></Col>
                                                   <Col md={6} lg={6} className="label-name">
                                                       <Field name="prepaidId" type="text" component={TextBox} label="prepaid id"/>
                                                   </Col>
                                               </Row>
                                           </Col>
                                           <Col md={6} lg={6}>
                                               <Row>
                                                   <Col md={5} lg={5} className="label-name">  <strong>Meter No</strong></Col>
                                                   <Col md={6} lg={6} className="label-name">
                                                       <Field name="meterSerial" type="text" component={TextBox} label="meter no"/>
                                                   </Col>
                                               </Row>
                                               <Row>
                                                   <Col md={5} lg={5} className="label-name">  <strong>Transformer</strong></Col>
                                                   <Col md={6} lg={6} className="label-name">
                                                       <Field name="meterSerial" type="text" component={TextBox} label="meter no"/>
                                                   </Col>
                                               </Row>
                                           </Col>
                                       </Row>
                                       <div style={{marginRight: '50px'}} className="pull-right">
                                           <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                       </div>
                                   </Col>
                                   </div>
                                    <Col xs={12} md={12} lg={12}>
                                        <Row>
                                            <div className="pull-left">
                                                <p style={{marginLeft:'10px',marginTop:'10px'}}>Problems</p>
                                                <Row style={{marginLeft:'10px'}}>
                                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'yellow'}}></div></Col>
                                                    <Col md={2}><b>>20%</b></Col>
                                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'orange'}}></div></Col>
                                                    <Col md={2}><b>>30%</b></Col>
                                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'red'}}></div></Col>
                                                    <Col md={2}><b>>50%</b></Col>
                                                </Row>
                                            </div>
                                        </Row>
                                    </Col>
                                    <Row>
                                        <Col xs={12} md={12} lg={12}>
                                            <div className="pull-right">
                                                <Row>
                                                    <Col md={12} lg={12}>
                                                        <ul className="button-ul">
                                                            <li><Button className="custom-button" bsStyle="primary" onClick={() => this.handleRefresh()}>Refresh</Button></li>
                                                            <li><Button className="custom-button" bsStyle="primary" onClick={() => this.handleGenerate()}>Save</Button></li>
                                                            <li><Field name="generateDate" component={DateTimePicker} placeholder="Generate Date"
                                                                       defaultDate={this.state.generateDate}
                                                                       handleChange = {this.handleGenerateDate.bind(this)}/></li>
                                                            <li><strong>Date<span className="label-require">*</span></strong></li>
                                                        </ul>
                                                    </Col>
                                                </Row>
                                            </div>
                                        </Col>
                                    </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div id="new-customers" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Button bsStyle="info" onClick={() => this.listNewCustomers()}>{this.state.labelBtn}</Button>
                        {
                            this.state.newCustomer ?
                                <div>
                                    <br/>
                                    <ReactHTMLTableToExcel
                                        id="test-table-xls-button"
                                        className="download-table-xls-button"
                                        table="table-to-xls"
                                        filename="new-prepaid-customers"
                                        sheet="tablexls"
                                        buttonText="Export to XLS"/>
                                    <table className="page-permision-table" id="table-to-xls">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Customer ID</th>
                                            <th>Name</th>
                                            <th>Prepaid ID</th>
                                            <th>Meter Serial</th>
                                            <th>Coefficient</th>
                                            <th>Ampere</th>
                                            <th>Ampere Value</th>
                                            <th>Area Name</th>
                                            <th>Transformer</th>
                                            <th>Pole</th>
                                            <th>Box</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.getAllNewPrepaidCustomers.status === 200 ?
                                                this.props.getAllNewPrepaidCustomers.data.items.map((element,index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index+1}</td>
                                                            <td>{element.id}</td>
                                                            <td>{element.nameKh}</td>
                                                            <td>{element.prepaidId}</td>
                                                            <td>{element.meterSerial}</td>
                                                            <td>{element.coefficient}</td>
                                                            <td>{element.ampere}</td>
                                                            <td>{element.ampereValue}</td>
                                                            <td>{element.areaName}</td>
                                                            <td>{element.transSerial}</td>
                                                            <td>{element.poleSerial}</td>
                                                            <td>{element.boxSerial}</td>
                                                        </tr>
                                                    )
                                                })
                                                :
                                                <tr>
                                                    <td colSpan={12} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                                </tr>
                                        }
                                        </tbody>
                                    </table>
                                </div>
                                :
                                <div>
                                    <table className="page-permision-table">
                                        <thead>
                                        <tr>
                                            <th rowSpan={2}>
                                                <input
                                                    type="checkbox"
                                                    checked={this.state.issueCheckAll}
                                                    onChange={this.handleCheckboxGenerateAll}
                                                    id="icheck"
                                                />
                                            </th>
                                            <th rowSpan={2}>Customer ID</th>
                                            <th rowSpan={2}>Customer Name</th>
                                            <th rowSpan={2}>Meter Serial</th>
                                            <th rowSpan={2}>Pre Paid ID</th>
                                            <th rowSpan={2}>Purchase Count</th>
                                            <th rowSpan={2}>All Purchases</th>
                                            <th rowSpan={2}>Purchases Per Month</th>
                                            <th rowSpan={2}>Last Purchase Date</th>
                                            <th style={{width: '200px'}}>History Using Per Month</th>
                                            <th rowSpan={2}>Old Balance</th>
                                            <th rowSpan={2}>New Balance</th>
                                            <th rowSpan={2}>Kw Per Day</th>
                                            <th rowSpan={2}>Percent</th>
                                            <th rowSpan={2}>Transformer</th>
                                            <th rowSpan={2}>Box</th>
                                        </tr>
                                        <tr>
                                            <th style={{textAlign: 'center', width: '200px'}}>I | II | III</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            allKws.length > 0 ?
                                                allKws.map((element, index) => {
                                                    if(element.percentage < 0 ){ // problem customer
                                                        percent = Math.abs(element.percentage);
                                                        if(percent < 20){
                                                            return (
                                                                <tr key={index}>
                                                                    <td>
                                                                        <input
                                                                            type="checkbox"
                                                                            value={element.id}
                                                                            onChange={this.handleGenerateCheck}
                                                                            className="icheck"
                                                                            id={"i"+element.id}
                                                                        />
                                                                    </td>
                                                                    <td>{element.id}</td>
                                                                    <td>{element.nameEn}</td>
                                                                    <td>{element.meterSerial}</td>
                                                                    <td>{element.curPrepaidId}</td>
                                                                    <td>{element.count}</td>
                                                                    <td>{element.energy}</td>
                                                                    <td>{element.curPower}</td>
                                                                    <td>{element.lastPurchase}</td>
                                                                    <td>{this.getHistory(element.history)}</td>
                                                                    <td>{element.lastBalance}</td>
                                                                    <td>{element.newBalance}</td>
                                                                    <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                    <td>{element.percentage.toFixed(2)}</td>
                                                                    <td>{element.transSerial}</td>
                                                                    <td>{element.boxSerial}</td>
                                                                </tr>
                                                            )
                                                        } else if(percent >= 20 && percent < 30 ){
                                                            return (
                                                                <tr key={index} style = {{backgroundColor: 'yellow'}}>
                                                                    <td>
                                                                        <input
                                                                            type="checkbox"
                                                                            value={element.id}
                                                                            onChange={this.handleGenerateCheck}
                                                                            className="icheck"
                                                                            id={"i"+element.id}
                                                                        />
                                                                    </td>
                                                                    <td>{element.id}</td>
                                                                    <td>{element.nameEn}</td>
                                                                    <td>{element.meterSerial}</td>
                                                                    <td>{element.curPrepaidId}</td>
                                                                    <td>{element.count}</td>
                                                                    <td>{element.energy}</td>
                                                                    <td>{element.curPower}</td>
                                                                    <td>{element.lastPurchase}</td>
                                                                    <td>{this.getHistory(element.history)}</td>
                                                                    <td>{element.lastBalance}</td>
                                                                    <td>{element.newBalance}</td>
                                                                    <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                    <td>{element.percentage.toFixed(2)}</td>
                                                                    <td>{element.transSerial}</td>
                                                                    <td>{element.boxSerial}</td>
                                                                </tr>
                                                            )
                                                        }else if(percent >= 30 && percent < 50){
                                                            return (
                                                                <tr key={index} style = {{backgroundColor: 'orange'}}>
                                                                    <td>
                                                                        <input
                                                                            type="checkbox"
                                                                            value={element.id}
                                                                            onChange={this.handleGenerateCheck}
                                                                            className="icheck"
                                                                            id={"i"+element.id}
                                                                        />
                                                                    </td>3
                                                                    <td>{element.id}</td>
                                                                    <td>{element.nameEn}</td>
                                                                    <td>{element.meterSerial}</td>
                                                                    <td>{element.curPrepaidId}</td>
                                                                    <td>{element.count}</td>
                                                                    <td>{element.energy}</td>
                                                                    <td>{element.curPower}</td>
                                                                    <td>{element.lastPurchase}</td>
                                                                    <td>{this.getHistory(element.history)}</td>
                                                                    <td>{element.lastBalance}</td>
                                                                    <td>{element.newBalance}</td>
                                                                    <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                    <td>{element.percentage.toFixed(2)}</td>
                                                                    <td>{element.transSerial}</td>
                                                                    <td>{element.boxSerial}</td>
                                                                </tr>
                                                            )
                                                        }else { // > 50
                                                            return (
                                                                <tr key={index} style = {{backgroundColor: 'red'}}>
                                                                    <td>
                                                                        <input
                                                                            type="checkbox"
                                                                            value={element.id}
                                                                            onChange={this.handleGenerateCheck}
                                                                            className="icheck"
                                                                            id={"i"+element.id}
                                                                        />
                                                                    </td>
                                                                    <td>{element.id}</td>
                                                                    <td>{element.nameEn}</td>
                                                                    <td>{element.meterSerial}</td>
                                                                    <td>{element.curPrepaidId}</td>
                                                                    <td>{element.count}</td>
                                                                    <td>{element.energy}</td>
                                                                    <td>{element.curPower}</td>
                                                                    <td>{element.lastPurchase}</td>
                                                                    <td>{this.getHistory(element.history)}</td>
                                                                    <td>{element.lastBalance}</td>
                                                                    <td>{element.newBalance}</td>
                                                                    <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                    <td>{element.percentage.toFixed(2)}</td>
                                                                    <td>{element.transSerial}</td>
                                                                    <td>{element.boxSerial}</td>
                                                                </tr>
                                                            )
                                                        }
                                                    }else { // normally customer
                                                        return (
                                                            <tr key={index}>
                                                                <td>
                                                                    <input
                                                                        type="checkbox"
                                                                        value={element.id}
                                                                        onChange={this.handleGenerateCheck}
                                                                        className="icheck"
                                                                        id={"i"+element.id}
                                                                    />
                                                                </td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.curPrepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.curPower}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{this.getHistory(element.history)}</td>
                                                                <td>{element.lastBalance}</td>
                                                                <td>{element.newBalance}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                            </tr>
                                                        )
                                                    }
                                                })
                                                :
                                                <tr>
                                                    <td colSpan={17} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                                </tr>
                                        }
                                        </tbody>
                                    </table>
                                </div>
                        }
                    </Col>
                </Row>
            </div>
        )
    }
}


GenerateBalanceKwh = reduxForm({
    form: 'form_generate_balance_prepaid',
    validate: (values) => {
        const errors = {};
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if (values.endDate === undefined) {
            errors.issueDate = 'Issue Date is required!';
        }
        return errors
    }
})(GenerateBalanceKwh);

function mapStateToProps(state) {
    //console.log("DATA :", state.balancePrepaidKwh.getAllNewPrepaidCustomers);
    return {
        getAllBalancePrepaidKhw: state.balancePrepaidKwh.getAllBalancePrepaidKhw,
        addBalancePrepaidKhw: state.balancePrepaidKwh.addBalancePrepaidKhw,
        getAllNewPrepaidCustomers: state.balancePrepaidKwh.getAllNewPrepaidCustomers

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addBalancePrepaidKwhAction,getAllBalancePrepaidKwhAction, getAllNewPrepaidCustomersAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(GenerateBalanceKwh);