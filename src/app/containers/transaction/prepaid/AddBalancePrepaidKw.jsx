import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { saveBalancePrepaidKwhAction, listBalancePrepaidKwhAction, updateBalancePrepaidKwhAction } from '../../../actions/transaction/prepaid/balancePrepaidKwh';
import moment from 'moment';
import ReactLoading from 'react-loading';

let startDate = "";
let endDate = "";
let meterId = 0;
let prepaidId = "";
class AddBalancePrepaidKw extends React.Component {
    constructor(props){
        super(props);
        this.state={
            dateBalance: null,
            generateDate: null,
            startDate: null,
            endDate: null,
            labelButton: 'SAVE',
            isEdit: false,
            isList: false,
            labelList: 'List Balance'
        };
        this.clearText = this.clearText.bind(this);
        this.getMeterId = this.getMeterId.bind(this);
        this.getPrepaidId = this.getPrepaidId.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    componentWillReceiveProps(data){
        if(data.saveBalancePrepaidKwh.status === 200){
            alert("Successfully save new balance kwh.");
            data.saveBalancePrepaidKwh.status = 0;
            this.props.dispatch(initialize("form_save_balance",null));
        }
        if(data.saveBalancePrepaidKwh.status === 500){
            alert("Fail with save new balance kwh!");
            data.saveBalancePrepaidKwh.status = 0;
        }

        if(data.updateAllBalancePrepaidKwh.status === 200){
            this.setState({labelButton: 'SAVE'});
            this.setState({isEdit: false});
            this.setState({startDate: null});
            this.setState({endDate: null});
            alert("Successfully updated new balance kwh.");
            data.updateAllBalancePrepaidKwh.status = 0;
            this.props.dispatch(initialize("form_save_balance",null));
        }
        if(data.updateAllBalancePrepaidKwh.status === 500){
            this.setState({labelButton: 'SAVE'});
            this.setState({isEdit: false});
            this.setState({startDate: null});
            this.setState({endDate: null});
            alert("Fail with updated new balance kwh.");
            data.updateAllBalancePrepaidKwh.status = 0;
            this.props.dispatch(initialize("form_save_balance",null));
        }
    }

    getDateBalance(date){
        this.setState({dateBalance: date});
    }
    getGenerateDate(date) {
        this.setState({generateDate: date});
    }

    clearText(){
        this.setState({dateBalance: null});
        this.setState({generateDate: null});
        this.props.dispatch(initialize("form_save_balance",null));
    }

    handleSubmit(values){
        if(!this.state.isEdit){
            // save
            let data = {
                "meterId": values.meterId,
                "prepaidId": values.prepaidId,
                "dateBalance": values.dateBalance,
                "oldBalance": values.oldBalance,
                "lastBalance": values.lastBalance,
                "newUsage": values.newUsage,
                "oldUsage": values.oldUsage,
                "description": values.description,
                "generateDate": values.generateDate,
                "status": 0
            };
            this.props.saveBalancePrepaidKwhAction(data);
        }else{
           // edit
            let data = {
                "dateBalance": values.dateBalance,
                "description": values.description,
                "generateDate": values.generateDate,
                "id": values.id,
                "lastBalance": values.lastBalance,
                "meterId": values.meterId,
                "newUsage": values.newUsage,
                "oldBalance": values.oldBalance,
                "oldUsage": values.oldUsage,
                "prepaidId": values.prepaidId,
                "status": 0
            };
            this.props.updateBalancePrepaidKwhAction(data);
        }
    }
    /* list and filter balance prepaid kw */
    listBalance(){
        if(this.state.isList === true){
            this.setState({labelList: 'List Balance'});
            this.setState({isList: false});
        }else{
            this.setState({labelList: 'Hide List Balance'});
            this.setState({isList: true});
        }
    }
    getStartDate(date){
        this.setState({startDate: date});
        startDate = moment(date).format("YYYY-MM-DD");
    }
    getEndDate(date){
        this.setState({endDate: date});
        endDate = moment(date).format("YYYY-MM-DD");
    }
    getMeterId(event){
        meterId = event.target.value;
    }
    getPrepaidId(event){
        prepaidId = event.target.value;
    }
    handleSearch(){
        let criteria = {
            'startDate' : startDate,
            'endDate' : endDate,
            'meterId' : meterId,
            'prepaidId' : prepaidId
        };
        if(criteria.startDate === "" && criteria.endDate === ""){
            alert("Start Date and End Date are required!");
        }else {
            this.props.listBalancePrepaidKwhAction(criteria);
        }
    }
    handleEdit(balance){
        this.setState({ labelButton : 'Update'});
        this.setState({isEdit: true});
        let data = {
            "dateBalance": moment(balance.dateBalance),
            "description": balance.description,
            "generateDate": moment(balance.generateDate),
            "id": balance.id,
            "lastBalance": balance.lastBalance,
            "meterId": balance.meterId,
            "newUsage": balance.newUsage,
            "oldBalance": balance.oldBalance,
            "oldUsage": balance.oldUsage,
            "prepaidId": balance.prepaidId,
            "status": 0
        };
        this.setState({dateBalance: moment(balance.dateBalance)});
        this.setState({generateDate: moment(balance.generateDate)});
        this.props.dispatch(initialize("form_save_balance",data));
    }
    handleRefresh(){
        location.href = "/app/transactions/prepaid/save-balance";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Button bsStyle="info" onClick={() => this.listBalance()}>{this.state.labelList}</Button>
                        <br/><br/>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Save Balance For New Customers</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Meter ID <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="meterId" type="text" component={TextBox} label="Meter ID" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Prepaid ID <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="prepaidId" type="text" component={TextBox} label="Prepaid ID" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Date Balance <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="dateBalance" component={DateTimePicker} placeholder="Date Balance"
                                                           defaultDate={this.state.dateBalance} handleChange={this.getDateBalance.bind(this)}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Old Balance <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="oldBalance" type="text" component={TextBox} label="Old Balance" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Last Balance <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="lastBalance" type="text" component={TextBox} label="Last Balance" icon=""/>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Old Usage <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="oldUsage" type="text" component={TextBox} label="Old Usage" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>New Usage <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="newUsage" type="text" component={TextBox} label="New Usage" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Generate Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="generateDate" component={DateTimePicker} placeholder="Generate Date"
                                                           defaultDate={this.state.generateDate} handleChange={this.getGenerateDate.bind(this)}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="description" type="text" component={TextArea}
                                                           label="Description" icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="pull-right">
                                                    <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.labelButton} />
                                                </Col>
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button onClick={this.clearText}>Clear</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                {
                    this.state.isList ?
                        <div>
                            {/* list balance and update */}
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <div className="panel panel-default">
                                        <div className="panel-heading">
                                            <h3 className="panel-title">Filter Balance Prepaid Kws</h3>
                                        </div>
                                        <div className="panel-body">
                                            <Row>
                                                <Col md={3} lg={3}>
                                                    <strong>Start Date <span className="label-require">*</span></strong>
                                                    <Field name="startDate" component={DateTimePicker} placeholder="Date Balance"
                                                           defaultDate={this.state.startDate} handleChange={this.getStartDate.bind(this)}/>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <strong>End Date <span className="label-require">*</span></strong>
                                                    <Field name="endDate" component={DateTimePicker} placeholder="Date Balance"
                                                           defaultDate={this.state.endDate} handleChange={this.getEndDate.bind(this)}/>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <strong>Meter ID </strong>
                                                    <Field name="meterid" type="text" component={TextBox} label="Meter ID" icon=""
                                                           onChange = {this.getMeterId}
                                                    />
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <strong>Prepaid ID </strong>
                                                    <Field name="prepaidid" type="text" component={TextBox} label="Prepaid ID" icon=""
                                                           onChange = {this.getPrepaidId} />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button bsStyle = "primary" onClick={() => this.handleSearch()}>Search</Button>&nbsp;&nbsp;
                                                    <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} lg={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    <table className="page-permision-table">
                                        <thead>
                                        <tr>
                                            <th>Balance ID</th>
                                            <th>Meter ID</th>
                                            <th>Prepaid ID</th>
                                            <th>Generate Date</th>
                                            <th>Date Balance</th>
                                            <th>Old Balance</th>
                                            <th>Last Balance</th>
                                            <th>Old Usage</th>
                                            <th>New Usage</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.listAllBalancePrepaidKwh.status === 200?
                                                this.props.listAllBalancePrepaidKwh.data.items.map((balance, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{balance.id}</td>
                                                            <td>{balance.meterId}</td>
                                                            <td>{balance.prepaidId}</td>
                                                            <td>{moment(balance.generateDate).format("DD-MM-YYYY")}</td>
                                                            <td>{moment(balance.dateBalance).format("DD-MM-YYYY")}</td>
                                                            <td>{balance.oldBalance}</td>
                                                            <td>{balance.lastBalance}</td>
                                                            <td>{balance.oldUsage}</td>
                                                            <td>{balance.newUsage}</td>
                                                            <td>{balance.description}</td>
                                                            <td>
                                                                <a onClick={() => this.handleEdit(balance)} className="btn btn-info btn-xs">
                                                                    <span>Edit</span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                                :
                                                <tr>
                                                    <td colSpan={11} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                                </tr>
                                        }
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </div>
                        :
                        null
                }
            </div>
        )
    }
}


AddBalancePrepaidKw = reduxForm({
    form: 'form_save_balance',
    validate: (values) => {
        let regex_number =/^-?\d*(\.\d+)?$/;

        const errors = {};
        if (values.meterId === undefined) {
            errors.meterId = 'Meter ID is required!';
        }
        if (values.prepaidId === undefined) {
            errors.prepaidId = 'Prepaid ID is required and at least 4 character!';
        }
        if(values.dateBalance === undefined){
            errors.dateBalance = 'Date Balance is required!';
        }
        if (!regex_number.test(values.oldBalance) || values.oldBalance === undefined) {
            errors.oldBalance = 'Old Balance is invalid!';
        }
        if (!regex_number.test(values.lastBalance) || values.lastBalance === undefined) {
            errors.lastBalance = 'Last Balance is invalid!';
        }
        if (!regex_number.test(values.oldUsage) || values.oldUsage === undefined) {
            errors.oldUsage = 'Old Usage is invalid!';
        }
        if (!regex_number.test(values.newUsage) || values.newUsage === undefined) {
            errors.newUsage = 'New Usage is invalid!';
        }
        if(values.generateDate === undefined){
            errors.generateDate = "Generate Date is required!";
        }
        return errors;
    }
})(AddBalancePrepaidKw);

function mapStateToProps(state) {
    //console.log("DATA  : ", state.balancePrepaidKwh.listAllBalancePrepaidKwh);
    return {
        saveBalancePrepaidKwh: state.balancePrepaidKwh.saveBalancePrepaidKwh,
        listAllBalancePrepaidKwh: state.balancePrepaidKwh.listAllBalancePrepaidKwh,
        updateAllBalancePrepaidKwh: state.balancePrepaidKwh.updateAllBalancePrepaidKwh
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ saveBalancePrepaidKwhAction, listBalancePrepaidKwhAction, updateBalancePrepaidKwhAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddBalancePrepaidKw);