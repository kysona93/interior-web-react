import React from 'react';
import { Row, Col, Panel } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextArea } from './../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { addLostKwhAction, getAllLostKwhAction, getAllMeterIdsAction, getAllLostKwhStatusAction } from './../../../actions/transaction/prepaid/lostKwh';
import {Typeahead} from 'react-bootstrap-typeahead';
import moment from 'moment';

let serials = [];
let meterSerial = "";
class LostKwh extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            lostDate: null,
            lostStatus: [],
            customerId: 0
        }
    }

    componentWillMount(){
        this.props.getAllMeterIdsAction();
        this.props.getAllLostKwhStatusAction();
    }
    componentWillReceiveProps(data) {
        if(data.getAllMeterIds.status === 200){
            serials = data.getAllMeterIds.data.items;
        }

        if(data.getAllLostKwhStatus.status === 200){
            this.setState({lostStatus: data.getAllLostKwhStatus.data.items});
        }

        if(data.addLostKhw.status === 200){
            alert("Successfully with add lost kwh");
            this.setState({lostDate: null});
            this.props.dispatch(initialize("form_lost_kwh",null));
            data.addLostKhw.status = 0;
        }
        if(data.addLostKhw.status === 401 || data.addLostKhw.status === 404 || data.addLostKhw.status === 500){
            alert("Fail with add lost kwh");
            let initData = {
                "customerId": '',
                "customerName": '',
                "prepaidId": ''
            };
            this.props.dispatch(initialize("form_lost_kwh",initData));
            data.addLostKhw.status = 0;
        }
    }

    filterMeterSerial(values){
        if(values.length > 0){
            meterSerial = values[0].meterSerial;
            let initData = {
                "customerId": values[0].customerId,
                "customerName": values[0].nameKh,
                "prepaidId": values[0].prepaidId
            };
            this.props.dispatch(initialize("form_lost_kwh",initData));
        }else{
            let initData = {
                "customerId": '',
                "customerName": '',
                "prepaidId": ''
            };
            this.props.dispatch(initialize("form_lost_kwh",initData));
        }
    }
    handleLostDate(date){
        this.setState({lostDate: date});
    }
    handleSubmit(values){
        let data = {
            "createDate": moment(new Date()).format("YYYY-MM-DD"),
            "customerId": Number(values.customerId),
            "dateLost": values.dateLost,
            "description": values.description,
            "kw": Number(values.kw),
            "meterSerial": meterSerial,
            "prepaidId": values.prepaidId,
            "statusId": Number(values.statusId)
        };
        //console.log("Data submit :",data);
        this.props.addLostKwhAction(data);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Panel header="Form Add Lost Kwh" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={6} lg={6}>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Meter Serial <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Typeahead
                                                    labelKey="meterSerial"
                                                    options={serials}
                                                    // selected={serials.filter(s => s.meterSerial === this.state.customerId)}
                                                    placeholder="Meter serial ...."
                                                    onChange={this.filterMeterSerial.bind(this)}
                                                    clearButton
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Customer ID <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="customerId" type="text" component={TextBox} label="Customer ID"/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Customer Name <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="customerName" type="text" component={TextBox} label="Customer Name"/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Prepaid ID</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="prepaidId" type="text" component={TextBox} label="Prepaid ID"/>
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={6} lg={6}>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Lost For <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="statusId" type="select" component={SelectBox} placeholder="Select reason"
                                                       values={this.state.lostStatus} sortBy="name" icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>KW <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="kw" type="text" component={TextBox} label="KW Lost"/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Lost Date <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="dateLost" component={DateTimePicker} placeholder="Lost Date"
                                                       defaultDate={this.state.lostDate}
                                                       handleChange={this.handleLostDate.bind(this)}/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <strong>Description </strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-user-circle"/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={2} lg={2} className="pull-right">
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting}
                                                      label="Save" />
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
            </div>
        )
    }

}

LostKwh = reduxForm({
    form: 'form_lost_kwh',
    validate: (values) => {
        const errors = {};
        if (values.customerId === undefined) {
            errors.customerId = 'Customer ID is required!';
        }
        if (values.customerName === undefined) {
            errors.customerName = 'Customer Name is required!';
        }
        if (values.statusId === undefined) {
            errors.statusId = 'Lost For is required!';
        }
        if (values.dateLost === undefined) {
            errors.dateLost = 'Lost Date is required!';
        }
        if (values.kw === undefined) {
            errors.kw = 'KW lost is required!';
        }
        return errors
    }
})(LostKwh);

function mapStateToProps(state) {
    //console.log("DATA :",state.lostKwh.getAllLostKwhStatus);
    return {
        getAllLostKwhStatus: state.lostKwh.getAllLostKwhStatus,
        getAllMeterIds: state.lostKwh.getAllMeterIds,
        addLostKhw: state.lostKwh.addLostKhw,
        getAllLostKwh: state.lostKwh.getAllLostKwh
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addLostKwhAction, getAllLostKwhAction, getAllMeterIdsAction, getAllLostKwhStatusAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(LostKwh);