import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import SelectBox from '../../../components/forms/SelectBox';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllReturnInvoicesAction, generateAllReturnInvoicesAction } from './../../../actions/transaction/prepaid/returnInvoice';
import moment from 'moment';
import ReactLoading from 'react-loading';

function onAfterTableComplete() {
    //console.log('Table render complete.');
}
function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
let invoices = [];
let filter = {
    startDate : '',
    endDate: '',
    invoiceType: '',
    transformer: '',
    customerId: ''
};
class InvoicePrepaidPrinting extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startDate: null,
            endDate: null,
            invoiceTypes : [{id:1, name:'All Pre-paid'},{id:2, name:'Return Kwh <=10 and Kwh <= 50'}]
        };
        this.handleClear = this.handleClear.bind(this);
        this.handleGenerate = this.handleGenerate.bind(this);
    }

    componentWillReceiveProps(data){
        /* get all invoices to be generate */
        if(data.getAllReturnInvoices.status === 200){
            invoices = [];
            document.getElementById('loading').style.display = 'none';
            let items = data.getAllReturnInvoices.data.items;
            items.forEach((element) => {
                invoices.push({
                    "poleSerial": element.poleSerial,
                    "transformerSerial": element.transformerSerial,
                    "usingAmount": element.usingAmount,
                    "customerName": element.cusNameKh,
                    "issueDate": moment(element.issueDate).format("DD-MM-YYYY"),
                    "startBillingDate": moment(element.startBillingDate).format("DD-MM-YYYY"),
                    "unit": element.unit,
                    "price": element.price,
                    "customerId": element.customerId,
                    "endBillingDate": moment(element.endBillingDate).format("DD-MM-YYYY"),
                    "currency": element.currency,
                    "invoiceNo": element.invoiceNo,
                    "returnAmount": element.returnAmount
                });
            });
        }
        if(data.getAllReturnInvoices.status === 401 || data.getAllReturnInvoices.status === 404 || data.getAllReturnInvoices.status === 500){
            document.getElementById('loading').style.display = 'none';
            invoices = [];
        }
        /* generate invoice as PDF */
        if(data.generateReturnInvoices.status === 200) {
            if(data.generateReturnInvoices.data.status === 200) {
                alert("អ្នកបានធ្វើការបញ្ចូលនូវ ការបង្កើតវិក្កយបត្រដោយស្វ័យប្រវត្តរួចរាល់។ វានឹងជូនដំណឹងទៅដល់អ្នកពេលដែលដំណើរការបានបញ្ចប់ តាមរយៈអ៊ីម៉ែលរបស់អ្នក។");
                data.generateReturnInvoices.status = 0;
                invoices = [];
            } else {
                alert("ម៉ាស៊ីនមានក៉ហុស!");
                data.generateReturnInvoices.status = 0;
            }
        }
    }
    /* filter to search */
    handleStartDate(date){
        this.setState({startDate: date});
        filter.startDate = moment(date).format("YYYY-MM-DD");
    }
    handleEndDate(date){
        this.setState({endDate: date});
        filter.endDate = moment(date).format("YYYY-MM-DD");
    }

    selectInvoiceType(event){
        console.log(event.target.value)
        if(event.target.value === undefined || event.target.value === null){
            filter.invoiceType = '';
        }else {
            filter.invoiceType = event.target.value;
        }
    }
    handleSearch(values){
        document.getElementById('loading').style.display = 'block';
        if(values.invoiceType !== undefined){
            filter.invoiceType = values.invoiceType;
        }else {
            filter.invoiceType = '';
        }
        if(values.transformer !== undefined){
            filter.transformer = values.transformer;
        }else {
            filter.transformer = '';
        }
        if(values.customerId !== undefined){
            filter.customerId = values.customerId;
        }else{
            filter.customerId = '';
        }
        this.props.getAllReturnInvoicesAction(filter);
    }
    /* generate invoice to be PDF file */
    handleGenerate(){
        let arrInvoiceNo = [];
        if(invoices.length <= 0){
            alert("Please search to find invoices before generate to pdf.");
        }else{
            for(let i=0; i< invoices.length;i++){
                arrInvoiceNo.push(invoices[i].invoiceNo);
            }
        }
        console.log("data generate : ", arrInvoiceNo);
        this.props.generateAllReturnInvoicesAction(arrInvoiceNo);
    }
    /* helper functions */
    handleClear(){
        this.setState({startDate: null});
        this.setState({endDate: null});
        this.props.dispatch(initialize("form_invoice_prepaid_printing", {}));
        if(this.props.getAllReturnInvoices.status === 200)
            this.props.getAllReturnInvoices.data.items = [];
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <Panel header="Filter Invoices" bsStyle="info">
                                <form onSubmit={handleSubmit(this.handleSearch.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={2} lg={2} className="label-name">
                                            <strong>Issue Start Date <span className="label-require">*</span></strong>
                                            <Field name="startDate" component={DateTimePicker} placeholder="Issue start date"
                                                   defaultDate={this.state.startDate}
                                                   handleChange={this.handleStartDate.bind(this)}/>
                                        </Col>
                                        <Col md={2} lg={2} className="label-name">
                                            <strong>Issue End Date <span className="label-require">*</span></strong>
                                            <Field name="endDate" component={DateTimePicker} placeholder="Issue end date"
                                                   defaultDate={this.state.endDate}
                                                   handleChange={this.handleEndDate.bind(this)}/>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Invoice Type</strong>
                                            <Field name="invoiceType" type="select" component={SelectBox}
                                                   placeholder="Select Invoice Type"
                                                   onChange = {this.selectInvoiceType.bind(this)}
                                                   values={this.state.invoiceTypes}
                                                   sortBy="name" icon=""/>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Transformer</strong>
                                            <Field name="transformer" type="text" component={TextBox} label="Transformer serial"/>
                                        </Col>
                                        <Col md={2} lg={2} className="label-name">
                                            <strong>Customer ID</strong>
                                            <Field name="customerId" type="text" component={TextBox} label="Customer ID"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={7} lg={7}></Col>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                        </Col>
                                        <Col md={3} lg={3} className="pull-right">
                                            <Button bsStyle="primary" bsSize="small"
                                                    onClick={() => this.handleGenerate()}>Generate All</Button>{' '}
                                            <Button bsSize="small" onClick={() => this.handleClear()}>Clear</Button>
                                        </Col>
                                    </Row>
                                </form>
                            </Panel>
                        </Col>
                    </Row>
                </div>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <BootstrapTable
                                data={invoices} //list of records
                                trClassName={trClassNameFormat}  //apply style on cell
                                bodyStyle={{ fontSize:'10pt'}}
                                options={{
                                    sizePerPageList: [ 10, 20, 30, 40 , 50, invoices.length ],
                                    paginationShowsTotal: true,
                                    sortName: 'customerName',  // default sort column name
                                    sortOrder: 'asc',  // default sort order
                                    afterTableComplete: onAfterTableComplete // A hook for after table render complete.
                                }}
                                exportCSV
                                hover // apply hover style on row
                                pagination
                            >
                                <TableHeaderColumn dataField='customerId' width="100px" dataSort >Customer ID</TableHeaderColumn>
                                <TableHeaderColumn dataField='customerName' width="100px" dataSort>Name</TableHeaderColumn>
                                <TableHeaderColumn dataField='invoiceNo' width="100px" dataSort isKey autoValue>Invoice No</TableHeaderColumn>
                                <TableHeaderColumn dataField='issueDate' width="100px" dataSort>Issue Date</TableHeaderColumn>
                                <TableHeaderColumn dataField='startBillingDate' width="150px" dataSort>Start Billing Date</TableHeaderColumn>
                                <TableHeaderColumn dataField='endBillingDate' width="150px" dataSort>End Billing Date</TableHeaderColumn>
                                <TableHeaderColumn dataField='unit' width="150px" dataSort>Using Powers</TableHeaderColumn>
                                <TableHeaderColumn dataField='price' width="150px" dataSort>Price</TableHeaderColumn>
                                <TableHeaderColumn dataField='usingAmount' width="150px" dataSort>Using Amount</TableHeaderColumn>
                                <TableHeaderColumn dataField='returnAmount' width="150px" dataSort>Return Amount</TableHeaderColumn>
                                <TableHeaderColumn dataField='currency' width="150px" dataSort>Currency</TableHeaderColumn>
                                <TableHeaderColumn dataField='transformerSerial' width="100px" dataSort>Transformer</TableHeaderColumn>
                                <TableHeaderColumn dataField='poleSerial' width="100px" dataSort>Pole</TableHeaderColumn>
                            </BootstrapTable>
                        </Col>
                    </Row>
                </div>

            </div>
        )
    }

}

InvoicePrepaidPrinting = reduxForm({
    form: 'form_invoice_prepaid_printing',
    validate: (values) => {
        const errors = {};
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if(values.endDate === undefined){
            errors.endDate = "End Date is required!";
        }
        return errors
    }
})(InvoicePrepaidPrinting);

function mapStateToProps(state) {
    //console.log("DATA : ",state.returnInvoices.getAllReturnInvoices);
    return {
        getAllReturnInvoices: state.returnInvoices.getAllReturnInvoices,
        generateReturnInvoices: state.returnInvoices.generateReturnInvoices
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllReturnInvoicesAction, generateAllReturnInvoicesAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InvoicePrepaidPrinting);