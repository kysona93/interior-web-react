import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllGenerateKwPerDayAction } from './../../../actions/transaction/prepaid/kwPerDay';
import { generateKwPerDayAction } from './../../../actions/transaction/prepaid/kwPerDay';
import moment from 'moment';
import ReactLoading from 'react-loading';

let allKws = [];
let issueKws = [];
let generateDate = "";
class GenerateKwPerDay extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            generateDate: null,
            issueCheckAll: false
        };
        this.handleCheckboxGenerateAll = this.handleCheckboxGenerateAll.bind(this);
        this.handleGenerateCheck = this.handleGenerateCheck.bind(this);
    }

    componentWillMount(){
        let criteria = {
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        };
        this.props.getAllGenerateKwPerDayAction(criteria);
    }

    componentWillReceiveProps(data){
        if(data.getAllGenerateKwPerDay.status === 200){
            document.getElementById('loading').style.display = 'none';
            allKws = data.getAllGenerateKwPerDay.data.items;
        }
        if(data.getAllGenerateKwPerDay.status === 404 || data.getAllGenerateKwPerDay.status === 500){
            document.getElementById('loading').style.display = 'none';
        }

        if(data.generateKwPerDay.status === 200){
            document.getElementById('generate').style.display = 'none';
            alert("Successfully generate kw per day");
            location.href = "/app/transactions/prepaid/generate-kw-per-day";
        }
        if(data.generateKwPerDay.status === 404 || data.generateKwPerDay.status === 500){
            document.getElementById('generate').style.display = 'none';
            alert("Fail with generate kw per day!");
            data.generateKwPerDay.status = 0;
        }
    }

    handleGenerateDate(date){
        this.setState({generateDate: date});
        generateDate = moment(date).format("YYYY-MM-DD");
    }

    handleSubmit(values){
        document.getElementById('loading').style.display = 'block';
        let criteria = {
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        };
        if(values.prepaidId !== undefined) criteria.prepaidId = values.prepaidId;
        if(values.customerName !== undefined) criteria.customerName = values.customerName;
        if(values.meterSerial !== undefined) criteria.meterSerial = values.meterSerial;
        if(values.transformer !== undefined) criteria.transformer = values.transformer;
        this.props.getAllGenerateKwPerDayAction(criteria);
    }

    handleCheckboxGenerateAll(){
        this.setState({issueCheckAll: !this.state.issueCheckAll});
        if(this.state.issueCheckAll === false) {
            // generate check all
            issueKws =[];
            allKws.forEach((element) => {
                issueKws.push({
                    "calCalculatedDate": generateDate,
                    "customerId": Number(element.id),
                    "kwhUsing": Number(element.kwhPerDay.toFixed(2))
                });
            });
        } else {
            // generate uncheck all
            issueKws = [];
        }
        console.log("generate all: ", issueKws);
    }

    handleGenerateCheck(event) {
        let index = issueKws.findIndex(element => element.customerId === Number(event.target.value));
        if(index === -1){
            // add generate kw
            for(let i=0; i< allKws.length; i++){
                if(allKws[i].id === Number(event.target.value)){
                    issueKws.push({
                        "calCalculatedDate": generateDate,
                        "customerId": Number(allKws[i].id),
                        "kwhUsing": Number(allKws[i].kwhPerDay.toFixed(2))
                    });
                    break;
                }
            }
            console.log("generate kw :", issueKws);
        }else{
            // uncheck generate kw
            $('#d'+event.target.value).prop('checked', false);
            issueKws.splice(index,1);
            this.setState({issueCheckAll: false});
            console.log("uncheck generate kw :", issueKws);
        }
    }

    handleSave(){
        if(issueKws.length > 0){
            if(generateDate === ""){
                alert("Generate Day is required!");
            }else{
                document.getElementById('generate').style.display = 'block';
                for(let i=0;i < issueKws.length; i++){
                    issueKws[i].calCalculatedDate = generateDate;
                }
                console.log("data before submit : ", issueKws);
                this.props.generateKwPerDayAction(issueKws);
            }
        }else {
            alert("No data! Please select data at least one.");
        }
    }

    handleRefresh(){
        location.href = "/app/transactions/prepaid/generate-kw-per-day";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let percent = 0;
        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#icheck").click(function () {
                $(".icheck").prop('checked', $(this).prop('checked'));
            });
            $("#dcheck").click(function () {
                $(".dcheck").prop('checked', $(this).prop('checked'));
            });
        });
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Link to="/app/transactions/prepaid/verify-end-purchase">
                            <span className="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back
                        </Link>
                        <Panel header="Kw Per Day" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Customer Name</strong>
                                        <Field name="customerName" type="text" component={TextBox} label="customer name"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Prepaid ID</strong>
                                        <Field name="prepaidId" type="text" component={TextBox} label="prepaid id"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Meter No</strong>
                                        <Field name="meterSerial" type="text" component={TextBox} label="meter no"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Transformer</strong>
                                        <Field name="transformer" type="text" component={TextBox} label="transformer"/>
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <br/>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                    </Col>
                                    <Col md={2} lg={2} className="pull-right">
                                        <br/>
                                        <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div id="generate" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div>
                    <div className="pull-left">
                        <p>Overdue</p>
                        <Row>
                            <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'yellow'}}></div></Col>
                            <Col md={2}><b>>20%</b></Col>
                            <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'orange'}}></div></Col>
                            <Col md={2}><b>>30%</b></Col>
                            <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'red'}}></div></Col>
                            <Col md={2}><b>>50%</b></Col>
                        </Row>
                    </div>
                    <div className="pull-right">
                        <Row>
                            <Col md={6} lg={6} className="label-name">
                                <Field name="generateDate" component={DateTimePicker} placeholder="Generate Date"
                                       defaultDate={this.state.generateDate}
                                       handleChange = {this.handleGenerateDate.bind(this)}/>
                            </Col>
                            <Col md={6} lg={6}>
                                <Button bsStyle="success" style={{marginTop:'5px'}}
                                        onClick={() => this.handleSave()}>Save</Button>
                            </Col>
                        </Row>
                    </div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <div>
                                <ReactHTMLTableToExcel
                                    id="test-table-xls-button"
                                    className="download-table-xls-button"
                                    table="table-to-xls"
                                    filename="kw-per-day-problems"
                                    sheet="tablexls"
                                    buttonText="Export to XLS"/>
                                <table className="page-permision-table" id="table-to-xls">
                                    <thead>
                                    <tr>
                                        <th rowSpan={2}>
                                            <input
                                                type="checkbox"
                                                checked={this.state.issueCheckAll}
                                                onChange={this.handleCheckboxGenerateAll}
                                                id="icheck"
                                            />
                                        </th>
                                        <th rowSpan={2}>Customer ID</th>
                                        <th rowSpan={2}>Name</th>
                                        <th rowSpan={2}>Meter Serial</th>
                                        <th rowSpan={2}>Transformer</th>
                                        <th rowSpan={2}>Box</th>
                                        <th rowSpan={2}>Prepaid ID</th>
                                        <th rowSpan={2}>Purchase Count</th>
                                        <th rowSpan={2}>Purchase Energy</th>
                                        <th rowSpan={2}>Last Purchase</th>
                                        <th >History Using</th>
                                        <th rowSpan={2}>New Kw Per Day</th>
                                        <th rowSpan={2}>Percent</th>
                                    </tr>
                                    <tr>
                                        <th style={{textAlign: 'center'}}>I | II | III</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.getAllGenerateKwPerDay.status === 200 ?
                                            this.props.getAllGenerateKwPerDay.data.items.map((element, index) => {
                                                if(element.percentage < 0 ){
                                                    percent = Math.abs(element.percentage);
                                                    if(percent < 20){
                                                        return (
                                                            <tr key={index}>
                                                                <td>
                                                                    <input
                                                                        type="checkbox"
                                                                        value={element.id}
                                                                        onChange={this.handleGenerateCheck}
                                                                        className="icheck"
                                                                        id={"i"+element.id}
                                                                    />
                                                                </td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.energies.replace('{', '').replace('}', '')}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else if(percent >= 20 && percent < 30 ){
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'yellow'}}>
                                                                <td>
                                                                    <input
                                                                        type="checkbox"
                                                                        value={element.id}
                                                                        onChange={this.handleGenerateCheck}
                                                                        className="icheck"
                                                                        id={"i"+element.id}
                                                                    />
                                                                </td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.energies.replace('{', '').replace('}', '')}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else if(percent >= 30 && percent < 50){
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'orange'}}>
                                                                <td>
                                                                    <input
                                                                        type="checkbox"
                                                                        value={element.id}
                                                                        onChange={this.handleGenerateCheck}
                                                                        className="icheck"
                                                                        id={"i"+element.id}
                                                                    />
                                                                </td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.energies.replace('{', '').replace('}', '')}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else { // > 50
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'red'}}>
                                                                <td>
                                                                    <input
                                                                        type="checkbox"
                                                                        value={element.id}
                                                                        onChange={this.handleGenerateCheck}
                                                                        className="icheck"
                                                                        id={"i"+element.id}
                                                                    />
                                                                </td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.energies.replace('{', '').replace('}', '')}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }
                                                }else {
                                                    // properly usage
                                                    return (
                                                        <tr key={index}>
                                                            <td>
                                                                <input
                                                                    type="checkbox"
                                                                    value={element.id}
                                                                    onChange={this.handleGenerateCheck}
                                                                    className="icheck"
                                                                    id={"i"+element.id}
                                                                />
                                                            </td>
                                                            <td>{element.id}</td>
                                                            <td>{element.nameEn}</td>
                                                            <td>{element.meterSerial}</td>
                                                            <td>{element.transSerial}</td>
                                                            <td>{element.boxSerial}</td>
                                                            <td>{element.prepaidId}</td>
                                                            <td>{element.count}</td>
                                                            <td>{element.energy}</td>
                                                            <td>{element.lastPurchase}</td>
                                                            <td>{element.energies.replace('{', '').replace('}', '')}</td>
                                                            <td>{element.kwhPerDay.toFixed(2)}</td>
                                                            <td>{element.percentage.toFixed(2)}</td>
                                                        </tr>
                                                    )
                                                }
                                            })
                                            :
                                            <tr>
                                                <td colSpan={15} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}

GenerateKwPerDay = reduxForm({
    form: 'form_generate_kw_per_day',
    validate: (values) => {
        const errors = {};
        return errors
    }
})(GenerateKwPerDay);

function mapStateToProps(state) {
    //console.log("DATA :",state.kwPerDay.getAllGenerateKwPerDay);
    return {
        getAllGenerateKwPerDay: state.kwPerDay.getAllGenerateKwPerDay,
        generateKwPerDay: state.kwPerDay.generateKwPerDay
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllGenerateKwPerDayAction, generateKwPerDayAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(GenerateKwPerDay);