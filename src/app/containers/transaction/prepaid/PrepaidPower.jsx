import React from  'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col } from 'react-bootstrap';
import { addPrepaidPowerAction, getPrepaidPowersAction } from './../../../actions/transaction/prepaid/prepaidPower';
import moment from 'moment';
import Pagination from '../../../components/forms/Pagination';
import ProgressButton from 'react-progress-button';
import accounting from 'accounting';
import './../../../../../node_modules/react-progress-button/react-progress-button.css';
import '../style.css';

let pagination = {
    limit: 10,
    page: 1,
    date: '',
    total: 0
};
class PrepaidPower extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            buttonState: ''
        };

        this.handleClick = this.handleClick.bind(this);
    }

    componentWillMount() {
        pagination.date = moment(new Date()).format("YYYY-MM-DD");
        this.props.getPrepaidPowersAction(pagination);
    }

    componentWillReceiveProps(newProps){
        if(newProps.prepaidPowerAdd.status === 200){
            if(newProps.prepaidPowerAdd.data.status === 200){
                this.setState({buttonState: 'success'});

                setTimeout(() => {
                    alert("Successfully added new prepaid power!");
                }, 1000);

                this.props.getPrepaidPowersAction(pagination);
                newProps.prepaidPowerAdd.data.status = 0;
            } else {
                this.setState({buttonState: 'error'});
                alert("Fail with add prepaid power!");
                newProps.prepaidPowerAdd.data.status = 0;
            }
            newProps.prepaidPowerAdd.status = 0;
        }
        if(newProps.prepaidPowerAdd.status === 404 || newProps.prepaidPowerAdd.status === 500){
            this.setState({buttonState: 'error'});
            alert("Fail with add prepaid power!");
            newProps.prepaidPowerAdd.status = 0;
        }
    }

    handleClick () {
        this.setState({buttonState: 'loading'});
        this.props.addPrepaidPowerAction();
    }

    render() {
        return (
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <p>
                            Click on button <b><i className="fa fa-download" /> Update </b>
                            to import prepaid power from Prepaid System into Billing System. <br/>
                            Note: This proccess will be take a few minutes, please do not go back!
                        </p>
                    </Col>
                </Row>
                <Row>
                    <Col md={12}>
                        <ProgressButton onClick={this.handleClick} state={this.state.buttonState} id="button-success">
                        <i className="fa fa-download" />{' '}Update
                        </ProgressButton>
                    </Col>
                </Row>

                <Row>
                    <Col md={12}>
                        <br/>
                        <h4>Records updated on { pagination.date }</h4>

                        <table className="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Prepaid ID</th>
                                <th>Buy Date</th>
                                <th>Customer Name / Office</th>
                                <th>Buy Powers</th>
                                <th>Price</th>
                                <th>Power Cost</th>
                                <th>Note</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.prepaidPowers.status === 200 ?
                                this.props.prepaidPowers.data.items.list.map((prepaidPower, index) => {
                                    pagination.total = this.props.prepaidPowers.data.items.totalCount;
                                    return (
                                        <tr key={index}>
                                            <td>{prepaidPower.id}</td>
                                            <td>{prepaidPower.prepaidId}</td>
                                            <td>{moment(prepaidPower.buyDate).format('L')}</td>
                                            <td>
                                                {prepaidPower.customer !== null ? prepaidPower.customer.nameEn : null}
                                                {prepaidPower.customer !== null ? ' / '+prepaidPower.customer.office : null}
                                            </td>
                                            <td>{prepaidPower.buyPower}</td>
                                            <td>{accounting.formatMoney(prepaidPower.price, { symbol: "",  format: "%v %s" })}</td>
                                            <td>{accounting.formatMoney(prepaidPower.powerCost, { symbol: "",  format: "%v %s" })}</td>
                                            <td>{prepaidPower.note}</td>
                                        </tr>
                                    )
                                })
                                : <tr>
                                    <td colSpan={8} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                </tr> }
                            </tbody>
                        </table>

                        <Pagination
                            totalCount = {pagination.total}
                            items = {pagination}
                            callBackAction = {this.props.getPrepaidPowersAction}
                        />
                    </Col>
                </Row>

            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        prepaidPowerAdd: state.prepaidPower.prepaidPowerAdd,
        prepaidPowers: state.prepaidPower.prepaidPowers
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        addPrepaidPowerAction,
        getPrepaidPowersAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(PrepaidPower);