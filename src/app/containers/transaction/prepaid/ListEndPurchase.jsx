import React from 'react';
import { Link } from 'react-router';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllPrepaidEndPurchasesAction } from './../../../actions/transaction/prepaid/prepaidPower';
import ReactLoading from 'react-loading';

class ListEndPurchase extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            disabledBtn: true
        }
    }

    componentWillMount(){
        // disable buttons to prevent user click duration closed date
        let today = new Date().getDate();
        if(today >= 8 && today <= 14){
            this.setState({disabledBtn:false});
        }

        let criteria = {
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        };
        this.props.getAllPrepaidEndPurchasesAction(criteria);
    }

    componentWillReceiveProps(data){
        if(data.getAllPrepaidEndPurchase.status === 200 || data.getAllPrepaidEndPurchase.status === 500){
            document.getElementById('loading').style.display = 'none';
        }
    }

    handleSubmit(values){
        document.getElementById('loading').style.display = 'block';
        let criteria = {
            prepaidId: '',
            customerName: '',
            meterSerial: '',
            transformer: ''
        };
        if(values.prepaidId !== undefined) criteria.prepaidId = values.prepaidId.trim();
        if(values.customerName !== undefined) criteria.customerName = values.customerName.trim();
        if(values.meterSerial !== undefined) criteria.meterSerial = values.meterSerial.trim();
        if(values.transformer !== undefined) criteria.transformer = values.transformer.trim();
        this.props.getAllPrepaidEndPurchasesAction(criteria);
    }

    // helper functions
    getPrepaidId(str){
        if(str !== null && str !== ""){
            let item = str.replace('{','').replace('}','');
            let arr = item.split(",");
            return arr[0];
        }else{
            return "";
        }
    }
    handleRefresh(){
        location.href = "/app/transactions/prepaid/verify-end-purchase";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let percent = 0;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Panel header="End Purchase" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Prepaid ID</strong>
                                        <Field name="prepaidId" type="text" component={TextBox} label="prepaid id"/>
                                    </Col>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Customer Name</strong>
                                        <Field name="customerName" type="text" component={TextBox} label="customer name"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Meter No</strong>
                                        <Field name="meterSerial" type="text" component={TextBox} label="meter no"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Transformer</strong>
                                        <Field name="transformer" type="text" component={TextBox} label="transformer"/>
                                    </Col>
                                    <Col md={2} lg={2} className="label-name">
                                        <br/>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting}
                                                      label="Search" />
                                    </Col>
                                </Row>
                            </form>
                            <div className="pull-right">
                                <Button>Graph</Button> {' '}
                                <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                            </div>
                        </Panel>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <div className="pull-left">
                                <p>Overdue</p>
                                <Row>
                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'yellow'}}></div></Col>
                                    <Col md={2}><b>>20%</b></Col>
                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'orange'}}></div></Col>
                                    <Col md={2}><b>>30%</b></Col>
                                    <Col md={1}><div style={{width:'15px', height: '15px', backgroundColor:'red'}}></div></Col>
                                    <Col md={2}><b>>50%</b></Col>
                                </Row>
                            </div>
                            <div className="pull-right">
                                <Link to="/app/transactions/prepaid/generate-kw-per-day">
                                    <Button bsStyle="success" bsSize="small" disabled={this.state.disabledBtn}>Calculate Kw Per Day</Button>
                                </Link>
                                {' '}
                                <Link to="/app/transactions/prepaid/generate-balance">
                                    <Button bsStyle="success" bsSize="small" disabled={this.state.disabledBtn}>Generate Last Balance</Button>
                                </Link>
                            </div>
                            <div>
                                <table className="page-permision-table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Customer ID</th>
                                        <th>Name</th>
                                        <th>Meter Serial</th>
                                        <th>Transformer</th>
                                        <th>Box</th>
                                        <th>Prepaid ID</th>
                                        <th>Purchase Count</th>
                                        <th>Purchase Energy</th>
                                        <th>Last Purchase</th>
                                        <th>KW Per Day</th>
                                        <th>Percent</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.getAllPrepaidEndPurchase.status === 200 ?
                                            this.props.getAllPrepaidEndPurchase.data.items.map((element, index) => {
                                                if(element.percentage < 0 ){
                                                    percent = Math.abs(element.percentage);
                                                    if(percent < 20){
                                                        return (
                                                            <tr key={index}>
                                                                <td>{index+1}</td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else if(percent >= 20 && percent < 30 ){
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'yellow'}}>
                                                                <td>{index+1}</td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else if(percent >= 30 && percent < 50){
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'orange'}}>
                                                                <td>{index+1}</td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }else { // > 50
                                                        return (
                                                            <tr key={index} style = {{backgroundColor: 'red'}}>
                                                                <td>{index+1}</td>
                                                                <td>{element.id}</td>
                                                                <td>{element.nameEn}</td>
                                                                <td>{element.meterSerial}</td>
                                                                <td>{element.transSerial}</td>
                                                                <td>{element.boxSerial}</td>
                                                                <td>{element.prepaidId}</td>
                                                                <td>{element.count}</td>
                                                                <td>{element.energy}</td>
                                                                <td>{element.lastPurchase}</td>
                                                                <td>{element.kwhPerDay.toFixed(2)}</td>
                                                                <td>{element.percentage.toFixed(2)}</td>
                                                            </tr>
                                                        )
                                                    }
                                                }else {
                                                    // properly usage
                                                    return (
                                                        <tr key={index}>
                                                            <td>{index+1}</td>
                                                            <td>{element.id}</td>
                                                            <td>{element.nameEn}</td>
                                                            <td>{element.meterSerial}</td>
                                                            <td>{element.transSerial}</td>
                                                            <td>{element.boxSerial}</td>
                                                            <td>{element.prepaidId}</td>
                                                            <td>{element.count}</td>
                                                            <td>{element.energy}</td>
                                                            <td>{element.lastPurchase}</td>
                                                            <td>{element.kwhPerDay.toFixed(2)}</td>
                                                            <td>{element.percentage.toFixed(2)}</td>
                                                        </tr>
                                                    )
                                                }
                                            })
                                            :
                                            <tr>
                                                <td colSpan={15} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}

ListEndPurchase = reduxForm({
    form: 'form_list_end_purchases',
    validate: (values) => {
        const errors = {};
        return errors;
    }
})(ListEndPurchase);

function mapStateToProps(state) {
    //console.log("DATA :",state.prepaidPower.getAllPrepaidEndPurchase);
    return {
        getAllPrepaidEndPurchase: state.prepaidPower.getAllPrepaidEndPurchase
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllPrepaidEndPurchasesAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListEndPurchase);