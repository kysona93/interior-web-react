import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Alert, Table, Button } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getMeterUsageProblemsAction } from '../../../actions/transaction/usageEntry';
import moment from 'moment';

class SuccessImport extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            alertVisible: true
        }
    }

    componentWillMount(){
        this.props.getMeterUsageProblemsAction();
    }
    componentWillReceiveProps(data){
        
    }

    handleAlertDismiss() {
        this.setState({ alertVisible: false });
    }

    render(){
        return (
            <div>
                { this.state.alertVisible ?
                    <Alert bsStyle="success" onDismiss={this.handleAlertDismiss.bind(this)}>
                        <strong>Success!</strong> Successful import excel file.
                    </Alert>
                : null }
                <div>
                    <h4>List Meter Problems</h4>
                    <ReactHTMLTableToExcel
                        id="test-table-xls-button"
                        className="download-table-xls-button"
                        table="table-to-xls"
                        filename="meter_usage_problems"
                        sheet="tablexls"
                        buttonText="Export to XLS"/>
                    <Table id="table-to-xls" striped bordered condensed className="prepaid-res-report">
                        <thead>
                            <tr>
                                <th style={{width:'60px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>លេខរៀង <br/>(No)</th>
                                <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>កាលបរិច្ជេទកត់ត្រា <br/>(Record Date)</th>
                                <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>អតិថិជន <br/>​(Customer)</th>
                                <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>លេខកុងទ័រ <br/>​(Meter Serial)</th>
                                <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>អំណានចាស់ <br/>(Old Usage)</th>
                                <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>អំណានថ្មី <br/>(New Usage)</th>
                                <th hidden style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ថាមពលចាស់<br/>(Old Reactive)</th>
                                <th hidden style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ថាមពលថ្មី<br/>(New Reactive)</th>
                            </tr>
                        </thead>
                        <tbody>
                        { this.props.meterUsageProblems.status === 200 && 
                            this.props.meterUsageProblems.data.status !== 500 ? 
                            this.props.meterUsageProblems.data.items.map((i,index) => {
                                return(
                                    <tr key={index} className="text-align-center">
                                        <td>{index + 1}</td>
                                        <td>{moment(i.recordDate).format('DD-MM-YYYY')}</td>
                                        <td>{i.meter.customer.nameEn}</td>
                                        <td>{i.meter.meterSerial}</td>
                                        <td>{i.lastUsageRecord}</td>
                                        <td>{i.newUsageRecord}</td>
                                        <td hidden>{i.lastReactiveRecord}</td>
                                        <td hidden>{i.newReactiveRecord}</td>
                                    </tr>
                                )
                            })
                            :
                            <tr className="text-align-center">
                                <td colSpan={8}>There is no data</td>
                            </tr>
                        }
                        </tbody>
                    </Table>
                </div>
                <Button 
                    bsStyle="default" 
                    onClick={() => this.props.handleClose(false)}
                    style={{float: 'right'}}>
                    Close
                </Button>
            </div>
        )
    }

}

SuccessImport = reduxForm({
    form: 'form_success_import',
    validate: (values) => {
        const errors = {};
        return errors
    }
})(SuccessImport);

function mapStateToProps(state) {
    console.log(state.usageEntry.meterUsageProblems)
    return {
        meterUsageProblems: state.usageEntry.meterUsageProblems,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getMeterUsageProblemsAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(SuccessImport);