import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {connect} from 'react-redux';
import { Field, reduxForm,initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllCustomerBillsAction, issueBillCustomerAction, getAllCustomersProblemAction } from '../../../actions/transaction/issueBill';
import moment from 'moment';
import ReactLoading from 'react-loading';

let allBills = [];
let issueData = [];
let problem = 0;
let issueDate = '';
let startDate = '';
let endDate = '';
class IssueBill extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            problem: false,
            meterTypeId : 0,
            issueDate: null,
            startDate: null,
            endDate: null,
            issueCheckAll: false,
            declareCheckAll: false
        };
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleCheckboxIssueAll = this.handleCheckboxIssueAll.bind(this);
        this.handleIssueCheck = this.handleIssueCheck.bind(this);
        this.handleCheckDeclareAll = this.handleCheckDeclareAll.bind(this);
        this.handleDeclareCheck = this.handleDeclareCheck.bind(this);
        this.customerHaveProblem = this.customerHaveProblem.bind(this);
    }

    componentWillMount(){
        this.props.getAllCustomerBillsAction(problem);
    }

    componentWillUnmount() {
        allBills = []; issueData = [];
        if(this.props.getAllCustomerBills.status === 200) 
            this.props.getAllCustomerBills.data.items = [];
        if(this.props.getAllCustomersProblem.status === 200) 
            this.props.getAllCustomersProblem.data.items = [];

        this.props.dispatch(initialize("form_issue_bill", {}));
    }

    handleRefresh(){
        this.setState({
            issueDate: null,
            startDate: null,
            endDate: null
        });
        this.props.getAllCustomerBillsAction(problem);
    }

    componentWillReceiveProps(data){
        if(data.getAllCustomerBills.status === 200){
            allBills = data.getAllCustomerBills.data.items;
            console.log(allBills)
            if(allBills.length !== 0) {
                document.getElementById('loading').style.display = 'none';
            }
        }
        if(data.getAllCustomerBills.status === 401 || data.getAllCustomerBills.status === 404 || data.getAllCustomerBills.status === 500){
            document.getElementById('loading').style.display = 'none';
        }

        if(data.issueCustomerBills.status === 200){
            if(data.issueCustomerBills.data.status === 200) {
                document.getElementById('issue-bill').style.display = 'none';
                alert("Successfully issued customer bills");
                this.setState({issueCheckAll: false});
                $('.icheck').prop('checked', false);
                this.setState({declareCheckAll: false});
                $('.dcheck').prop('checked', false);
                this.setState({meterTypeId: 0});
                problem = 0;
                allBills = [];
                this.props.getAllCustomerBillsAction(problem);
                data.issueCustomerBills.status = 0;
            } else {
                alert("Fail with issue customer bills");
                data.issueCustomerBills.status = 0;
            }
        }
        if(data.issueCustomerBills.status === 401 || data.issueCustomerBills.status === 404 || data.issueCustomerBills.status === 500){
            document.getElementById('issue-bill').style.display = 'none';
            alert("Fail with issue customer bills");
            this.setState({issueCheckAll: false});
            $('.icheck').prop('checked', false);
            this.setState({declareCheckAll: false});
            $('.dcheck').prop('checked', false);
            this.setState({meterTypeId: 0});
            problem = 0;
            allBills = [];
            this.props.getAllCustomerBillsAction(problem);
            data.issueCustomerBills.status = 0;
        }
    }
    /* filter customers who has problems */
    customerHaveProblem(){
        this.setState({problem: true});
        problem = 1;
        this.props.getAllCustomersProblemAction(problem);
    }

    handleCheckboxIssueAll(){
        this.setState({issueCheckAll: !this.state.issueCheckAll});
        if(this.state.issueCheckAll === false) {
            issueData = [];
            // issue check all
            allBills.forEach((element) => {
                issueData.push({
                    "invoiceTypeId": 2, //Number(invoiceTypeId),
                    "customerId": Number(element.customerId),
                    "meterUsageId": element.meterUsageId,
                    "notDeclare" : 0,
                    "issueDate": '',
                    "startDate": '',
                    "endDate": ''
                    });
            });
        } else {
            // issue uncheck all
            issueData = [];
            // clear all notdeclear
            this.setState({declareCheckAll: false});
            $('.dcheck').prop('checked', false);
        }
        console.log("issue all: ", issueData);
    }
    handleIssueCheck(event) {
        let index = issueData.findIndex(element => element.customerId === Number(event.target.value));
        if(index === -1){
            // add issue customer
            for(let i=0; i< allBills.length; i++){
                if(allBills[i].customerId === Number(event.target.value)){
                    issueData.push({
                        "invoiceTypeId": 2, //Number(invoiceTypeId),
                        "customerId": Number(allBills[i].customerId),
                        "meterUsageId": allBills[i].meterUsageId,
                        "notDeclare" : 0,
                        "issueDate": '',
                        "startDate": '',
                        "endDate": ''
                    });
                    break;
                }
            }
            console.log("check issue customer :", issueData);
        }else{
            // uncheck issue customer
            issueData[index].notDeclare = 0;
            $('#d'+event.target.value).prop('checked', false);
            issueData.splice(index,1);
            this.setState({issueCheckAll: false});
            this.setState({declareCheckAll: false});
            console.log("uncheck issue customer :", issueData);
        }
    }
    handleCheckDeclareAll(event){
        if(this.state.issueCheckAll === false){
            // not check issue all
            alert("Please check issue all customers!");
            this.setState({declareCheckAll: false});
            $('.dcheck').prop('checked', false);
        }else{
            // check issue all
            this.setState({declareCheckAll: !this.state.declareCheckAll});
            if(this.state.declareCheckAll === false) {
                // check notdeclare all
                for(let i=0; i< issueData.length; i++) issueData[i].notDeclare = 1;
                console.log("check not declare all :",issueData);
            } else {
                // uncheck notdeclare all
                for(let i=0; i< issueData.length; i++) issueData[i].notDeclare = 0;
                console.log("uncheck declare all :",issueData);
            }
        }
    }
    handleDeclareCheck(event){
        if(issueData.length >= 1){
            let index = issueData.findIndex(element => element.customerId === Number(event.target.value));
            if(index !== -1){
                // check issue already
                if(issueData[index].notDeclare === 1){
                    // uncheck notdeclare
                    issueData[index].notDeclare = 0;
                    this.setState({declareCheckAll: false});
                    console.log("check isdeclare :", issueData);
                }else{
                    // check notdeclare
                    issueData[index].notDeclare = 1;
                    console.log("uncheck isdeclare :", issueData);
                }
            }else{
                // not yet check issue
                alert("Please check issue customer!");
                $('#d'+event.target.value).prop('checked', false);
            }
        }else{
            // not yet check issue customer
            alert("Please check issue customer!");
            this.setState({declareCheckAll: false});
            $('.dcheck').prop('checked', false);
        }
    }

    handleIssueDate(date){
        this.setState({issueDate: date});
        issueDate = moment(date).format("YYYY-MM-DD");
    }
    handleStartDate(date){
        this.setState({startDate: date});
        startDate = moment(date).format("YYYY-MM-DD");
    }
    handleEndDate(date){
        this.setState({endDate: date});
        endDate = moment(date).format("YYYY-MM-DD");
    }
    handleSubmit(){
        if (issueData.length <= 0) {
            alert("Please select issue checkbox!");
        } else {
            document.getElementById('issue-bill').style.display = 'block';
            for(let i=0;i < issueData.length; i++){
                issueData[i].issueDate = issueDate;
                issueData[i].startDate = startDate;
                issueData[i].endDate = endDate;
            }
            //console.log("before submit", issueData);
            this.props.issueBillCustomerAction(issueData);
        }
    }

    handleBack(){
        this.setState({problem: false});
        problem = 0;
        this.props.getAllCustomerBillsAction(problem);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#icheck").click(function () {
                $(".icheck").prop('checked', $(this).prop('checked'));
            });
            $("#dcheck").click(function () {
                $(".dcheck").prop('checked', $(this).prop('checked'));
            });
        });
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Panel header="Issue Bill Post Paid" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Issue Date <span className="label-require">*</span></strong>
                                        <Field name="fromDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.issueDate}
                                               handleChange={this.handleIssueDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Start Date <span className="label-require">*</span></strong>
                                        <Field name="startDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.startDate}
                                               handleChange={this.handleStartDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>End Date <span className="label-require">*</span></strong>
                                        <Field name="endDate" component={DateTimePicker} placeholder="Date Usage"
                                               defaultDate={this.state.endDate}
                                               handleChange={this.handleEndDate.bind(this)}/>
                                    </Col>
                                    <Col md={3} lg={3}>
                                        <br/>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting}
                                                      label="Issue Selected Customer(s)" />
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                {!this.state.problem ?
                    <Row>
                        <Col md={9} lg={9}></Col>
                        <Col md={3} lg={3}>
                            {/*<Button bsStyle="primary" bsSize="small" onClick={() => this.customerHaveProblem()}>Customer Have Problem</Button>{' '}*/}
                            <Button bsStyle="primary" bsSize="small" className="pull-right" onClick={() => this.handleRefresh()}>Refresh</Button>
                        </Col>
                    </Row>
                    : null
                }

                <div id="loading" style={{marginTop: 20, textAlign: 'center'}}>
                    {/*<ReactLoading type="spokes" color="#888888" className="margin-auto" />*/}
                    <i className="fa fa-refresh fa-spin fa-3x fa-fw" style={{color: '#777'}}></i>
                    <span className="sr-only">Loading...</span>
                </div>
                <div id="issue-bill" style={{display: 'none', marginTop: 20, textAlign: 'center'}}>
                    <i className="fa fa-refresh fa-spin fa-3x fa-fw" style={{color: '#777'}}></i>
                    <span className="sr-only">Loading...</span>
                </div>
                { !this.state.problem ?
                    <div>
                        {/* list properly customers */}
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <table className="page-permision-table">
                                    <thead>
                                    <tr>
                                        <th>
                                            Issue{' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.issueCheckAll}
                                                onChange={this.handleCheckboxIssueAll}
                                                id="icheck"
                                            />
                                        </th>
                                        <th>
                                            Not Declared{' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.declareCheckAll}
                                                onChange={this.handleCheckDeclareAll}
                                                id="dcheck"
                                            />
                                        </th>
                                        <th>Customer ID</th>
                                        <th>Customer Name</th>
                                        <th>Meter Serial</th>
                                        <th>Ampere</th>
                                        <th>Meter Type</th>
                                        <th>Last Usage</th>
                                        <th>New Usage</th>
                                        <th>Coefficient</th>
                                        <th>Usage</th>
                                        <th>Last Read Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.getAllCustomerBills.status === 200 ?
                                            this.props.getAllCustomerBills.data.items.map((bill, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>
                                                            <input
                                                                type="checkbox"
                                                                value={bill.customerId}
                                                                onChange={this.handleIssueCheck}
                                                                className="icheck"
                                                                id={"i"+bill.customerId}
                                                            />
                                                        </td>
                                                        <td>
                                                            <input
                                                                type="checkbox"
                                                                value={bill.customerId}
                                                                onChange={this.handleDeclareCheck}
                                                                className="dcheck"
                                                                id={"d"+bill.customerId}
                                                            />
                                                        </td>
                                                        <td>{bill.customerId}</td>
                                                        <td>{bill.nameKh}</td>
                                                        <td>{bill.meterSerial}</td>
                                                        <td>{bill.ampere}</td>
                                                        <td>{bill.meterType}</td>
                                                        <td>{bill.oldUsage}</td>
                                                        <td>{bill.newUsage}</td>
                                                        <td>{bill.coefficient}</td>
                                                        <td>{bill.usage}</td>
                                                        <td>{moment(bill.recordDate).format("DD-MM-YYYY")}</td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan={13} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>
                        :
                    <div>
                        {/* list customer who has problems */}
                        <Button bsStyle="primary" bsSize="small" className="pull-right"
                                onClick={() => this.handleBack()}>Back</Button>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <ReactHTMLTableToExcel
                                    id="test-table-xls-button"
                                    className="download-table-xls-button"
                                    table="table-to-xls"
                                    filename="invoices-problem"
                                    sheet="tablexls"
                                    buttonText="Export to XLS"/>
                                <table className="page-permision-table" id="table-to-xls">
                                    <thead>
                                    <tr>
                                        <th>
                                            Issue{' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.issueCheckAll}
                                                onChange={this.handleCheckboxIssueAll}
                                                id="icheck"
                                            />
                                        </th>
                                        <th>
                                            Not Declared{' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.declareCheckAll}
                                                onChange={this.handleCheckDeclareAll}
                                                id="dcheck"
                                            />
                                        </th>
                                        <th>Customer ID</th>
                                        <th>Customer Name</th>
                                        <th>Meter Serial</th>
                                        <th>Ampere</th>
                                        <th>Meter Type</th>
                                        <th>Last Usage</th>
                                        <th>New Usage</th>
                                        <th>Coefficient</th>
                                        <th>Usage</th>
                                        <th>Last Read Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.getAllCustomersProblem.status === 200 ?
                                            this.props.getAllCustomersProblem.data.items.map((bill, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td>
                                                            <input
                                                                type="checkbox"
                                                                value={bill.customerId}
                                                                onChange={this.handleIssueCheck}
                                                                className="icheck"
                                                                id={"i"+bill.customerId}
                                                            />
                                                        </td>
                                                        <td>
                                                            <input
                                                                type="checkbox"
                                                                value={bill.customerId}
                                                                onChange={this.handleDeclareCheck}
                                                                className="dcheck"
                                                                id={"d"+bill.customerId}
                                                            />
                                                        </td>
                                                        <td>{bill.customerId}</td>
                                                        <td>{bill.nameKh}</td>
                                                        <td>{bill.meterSerial}</td>
                                                        <td>{bill.ampere}</td>
                                                        <td>{bill.meterType}</td>
                                                        <td>{bill.oldUsage}</td>
                                                        <td>{bill.newUsage}</td>
                                                        <td>{bill.coefficient}</td>
                                                        <td>{bill.usage}</td>
                                                        <td>{moment(bill.recordDate).format("DD-MM-YYYY")}</td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan={13} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                            </Col>
                        </Row>
                    </div>

                }
            </div>
        )
    }

}

IssueBill = reduxForm({
    form: 'form_issue_bill',
    validate: (values) => {
        const errors = {};
        if (values.issueDate === undefined) {
            errors.issueDate = 'Issue Date is required!';
        }
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if (values.endDate === undefined) {
            errors.endDate = 'End Date is required!';
        }
        return errors
    }
})(IssueBill);

function mapStateToProps(state) {
    return {
        getAllCustomerBills: state.issueBills.getAllCustomerBills,
        getAllCustomersProblem: state.issueBills.getAllCustomersProblem,
        issueCustomerBills: state.issueBills.issueCustomerBills

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllCustomerBillsAction, issueBillCustomerAction, getAllCustomersProblemAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(IssueBill);