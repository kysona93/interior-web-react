import React from 'react';
import { Row, Col, Panel, Button } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllRecordKhwMonthlyAction } from '../../../actions/transaction/recordKhwMonthly';
import { getAllTransformerForKhwMonthlyAction } from '../../../actions/setup/transformer';
import { getAllMeterTypesAction } from '../../../actions/setup/type/meterType';
import { getAllGroupInvoiceAction } from './../../../actions/invoice/groupInvoices';
import moment from 'moment';
import ReactLoading from 'react-loading';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import './../dataTableStyle.css';
import './../../../../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css';
import './../../../../../node_modules/react-bootstrap-table/css/toastr.css';

function onAfterTableComplete() {
    //console.log('Table render complete.');
}
function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
let records = {
    bookDate: '',
    meterTypeId: 3, // 3 is post paid
    groupInvoiceId: 0,
    transformerId : 0,
    sortBy: 'id',
    sortType: 'ASC'
};
let arrRecords = [];
let arrDate = [];
let monthKh = '';
class RecordKhwMonthly extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            bookDate: null,
            transformers : [],
            meterType : [],
            settings: JSON.parse(localStorage.getItem('_setting'))
        };
    }

    componentWillMount(){
        this.props.getAllMeterTypesAction();
        this.props.getAllTransformerForKhwMonthlyAction();
        this.props.getAllGroupInvoiceAction();
        // let d = new Date();
        // records.bookDate = d.getFullYear() + "-" + (d.getMonth()+1) + "-" + d.getDate();
        // this.props.getAllRecordKhwMonthlyAction(records);
    }

    componentWillUnmount() {
        this.handleRefresh();
    }

    componentWillReceiveProps(data){
        /* meter types */
        if(data.meterTypeAll.items !== undefined){
            let arrMeterTypes = [];
            let items = data.meterTypeAll.items;
            if(items.length === arrMeterTypes.length){
                this.setState({invoiceTypes : arrMeterTypes});
            }else {
                items.forEach((element) => {
                    arrMeterTypes.push({
                        "id": element.id,
                        "name": element.meterType
                    });
                });
                this.setState({meterType : arrMeterTypes});
            }
        }
        /* get all transformers */
        if(data.getAllTransformersForKwhMonthly !== undefined){
            let arrTransformers = [];
            let items = data.getAllTransformersForKwhMonthly.items;
            if(items !== undefined){
                items.forEach((element) => {
                    arrTransformers.push({
                        "id": element.id,
                        "name": element.serial
                    });
                });
                this.setState({transformers : arrTransformers});
            }
        }
        if(data.getAllTransformersForKwhMonthly.status === 401 || data.getAllTransformersForKwhMonthly.status === 404 || data.getAllTransformersForKwhMonthly.status === 500){
            this.setState({transformers : []});
        }
        /* list all meters usage */
        if(data.getAllRecordKhwMonthly.status === 200){
            document.getElementById('loading').style.display = 'none';
            arrRecords = [];
            let items = data.getAllRecordKhwMonthly.data.items;
            let histories = '', fm = '', sm = '', tm = '';
            let lastUsages = '', lu1= '', lu2 = '', lu3 = '';
            let lastReactives = '', lr1= '', lr2 = '', lr3 = '';
            for(let i=0; i<items.length; i++){
                histories = items[i].usageHistory.replace('{', '').replace('}', '');
                lastUsages = items[i].lastUsageRecords.replace('{', '').replace('}', '');
                lastReactives = items[i].lastReactiveRecords.replace('{', '').replace('}', '');

                let index = histories.indexOf(",");
                if(index === -1){
                    // one meter
                    tm = histories.replace(/"/g ,''); sm = 0; fm = 0;
                } else{
                    // more than one meter
                    histories = histories.split(','); tm = histories[0]; sm = histories[1];
                    fm = histories[2] === undefined ? '0' : histories[2];
                }

                //TODO: function covert lastusage and lastreactive
                let index2 = lastUsages.indexOf(",");
                if (index2 === -1){
                    // one meter
                    lu1 = lastUsages.replace(/"/g ,''); lu2 = ''; lu3 = '';
                    lr1 = lastReactives.replace(/"/g ,''); lr2 = ''; lr3 = '';
                } else {
                    // more than one meter
                    lastUsages = lastUsages.split(','); 
                    lu1 = lastUsages[0].toString().includes(".") ? Number(lastUsages[0]).toFixed(1) : lastUsages[0];
                    lu2 = lastUsages[1].toString().includes(".") ? Number(lastUsages[1]).toFixed(1) : lastUsages[1];
                    lu3 = lastUsages[2] === undefined ? '' : 
                        lastUsages[2].toString().includes(".") ? Number(lastUsages[2]).toFixed(1) : lastUsages[2];

                    lastReactives = lastReactives.split(','); 
                    lr1 = lastReactives[0].toString().includes(".") ? Number(lastReactives[0]).toFixed(1) : lastReactives[0];
                    lr2 = lastReactives[1].toString().includes(".") ? Number(lastReactives[1]).toFixed(1) : lastReactives[1];
                    lr3 = lastReactives[2] === undefined ? '' :
                        lastReactives[2].toString().includes(".") ? Number(lastReactives[2]).toFixed(1) : lastReactives[2];
                }
                lastUsages = lu1 + 
                    (lu2 !== '' ? ' / ' + lu2 : '') + 
                    (lu3 !== '' ? ' / ' + lu3 : '');
                lastReactives = lr1 + 
                    (lr2 !== '' ? ' / ' + lr2 : '') + 
                    (lr3 !== '' ? ' / ' + lr3 : '');

                arrRecords.push({
                    "no": (i+1),
                    "ampereValue": items[i].ampereValue,
                    "lastReactiveRecords": lastReactives,
                    "newReactiveRecords": '',
                    "poleSerial": items[i].poleSerial,
                    "firstMonth": fm.toString().includes(".") ? Number(fm).toFixed(1) : fm,
                    "secondMonth": sm.toString().includes(".") ? Number(sm).toFixed(1) : sm,
                    "thirdMonth": tm.toString().includes(".") ? Number(tm).toFixed(1) : tm,
                    "customerName": items[i].customerName,
                    "lastUsageRecords": lastUsages,
                    "newUsageRecords": '',
                    "areaNameKh": items[i].areaNameKh,
                    "transformerSerial" : items[i].transformerSerial,
                    "locationId": items[i].locationId,
                    "poleAddress": items[i].poleAddress,
                    "meterSerials": items[i].meterSerials.replace('{', '').replace('}', '').replace(/"/g ,'').replace(/,/g ,'/'),
                    "boxSerial": items[i].boxSerial,
                    "coefficients": items[i].coefficients.replace('{', '').replace('}', '').replace(/"/g ,'').replace(/,/g ,'/'),
                    "customerId": items[i].id,
                    "others" : ''
                });
            }
        }
        if(data.getAllRecordKhwMonthly.status === 401 || data.getAllRecordKhwMonthly.status === 404 || data.getAllRecordKhwMonthly.status === 500){
            document.getElementById('loading').style.display = 'none';
            arrRecords = [];
        }
    }

    /* helper functions */
    handleRefresh(){
        this.setState({bookDate: null});
        arrRecords = []; arrDate = [];
        if(this.props.getAllRecordKhwMonthly.status === 200) 
            this.props.getAllRecordKhwMonthly.data.items = [];
        
        this.props.dispatch(initialize("form_record_kwh_monthly", {}));
    }
    /* handle print preview */
    static handlePrint(){
        let divToPrint = document.getElementById('printTable');
        divToPrint.style.display = "block";
        let htmlToPrint = '' +
            '<style type="text/css">' +
                'table {'+ 'border-collapse: collapse;}'+
                'tbody,tr,td {'+ 'border: 1px solid black;}'+
                'thead,tr,th {'+ 'border: 1px solid black;}'+
            '</style>';
        htmlToPrint += divToPrint.outerHTML;
        setTimeout(() => {
            let newWin = window.open("");
            newWin.document.write('<center>'+"<b>ក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី​ សឹលូសិន ខបភើរេសិន</b>"+'</center>');
            newWin.document.write('<center>'+"<b>អាស័យដ្ឋាន: ភូមិ ពោធិ៍2, ឃុំ ដូនកឹង, ស្រុក កំចាយមារ, ខេត្ត ព្រៃវែង</b>"+'</center>');
            newWin.document.write('<center>'+"<b>របាយការណ៏ ចុះស្រង់អំណានសំរាប់ ថ្ងៃ......ខែ..........ឆ្នាំ...........</b>"+'</center>');
            newWin.document.write('<p>Date: </p>');
            newWin.document.write('<p>Project No: Inter BP</p>');
            newWin.document.write('<p>ពីថ្ងៃខែ ...../........../......... ដល់ថ្ងៃខែ ...../........../.........</p>');
            newWin.document.write(htmlToPrint);
            newWin.document.write("<br/>");
            newWin.document.write("<span style='float:left'>ហត្ថលេខា:</span>");
            newWin.document.write("<span style='float:right'>ឈ្មោះជាង:</span>");
            newWin.print();
            newWin.close();
            divToPrint.style.display = "none";
        }, 100);
    }
    /* search blog */
    bookDatePayment(date){
        arrDate = [];
        let month;
        let year;
        this.setState({bookDate: date});
        records.bookDate = moment(date).format("YYYY-MM-DD");
        let bookDate = new Date(records.bookDate);
        let minusDate = new Date(bookDate.setMonth(bookDate.getMonth() - 3));
        month = (minusDate.getMonth()+1);
        year = minusDate.getFullYear();
        for(let i=1; i<=3; i++){
            arrDate.push({
                'month' : month,
                'year': year
            });
            month = month + 1;
            if(month > 12) {
                month = 1; year = year + 1;
            }
        }
        this.handleConvertMonthToKhmer(month);
    }

    selectMeterType(event){
        records.meterTypeId = Number(event.target.value);
    }
    selectTransformer(event){
        records.transformerId = Number(event.target.value);
    }
    handleSearch(values){
        if(values.groupInvoiceId === undefined || values.groupInvoiceId === null){
            records.groupInvoiceId = 0;
        } else records.groupInvoiceId = Number(values.groupInvoiceId)
        if(values.meterTypeId === undefined || values.meterTypeId === null){
            records.meterTypeId = 3;
        }
        if(values.transformerId === undefined || values.transformerId === null){
            records.transformerId = 0;
        }
        console.log(records)
        document.getElementById('loading').style.display = 'block';
        this.props.getAllRecordKhwMonthlyAction(records);
    }

    handleSort(field) {
        records.sortBy = field;
        records.sortType !== 'ASC' ? records.sortType = 'ASC' : records.sortType = 'DESC';
        this.props.getAllRecordKhwMonthlyAction(records);
    }

    handleConvertMonthToKhmer(monthNumber) {
        switch (monthNumber) {
            case 1:
                monthKh = "មករា";
                break;
            case 2:
                monthKh = "កុម្ភៈ";
                break;
            case 3:
                monthKh = "មីនា";
                break;
            case 4:
                monthKh = "មេសា";
                break;
            case 5:
                monthKh = "ឧសភា";
                break;
            case 6:
                monthKh = "មិថុនា";
                break;
            case 7:
                monthKh = "កក្កដា";
                break;
            case 8:
                monthKh = "សីហា";
                break;
            case 9:
                monthKh = "កញ្ញា";
                break;
            case 10:
                monthKh = "តុលា";
                break;
            case 11:
                monthKh = "វិច្ឆិកា";
                break;
            case 12:
                monthKh = "ធ្នូ";
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let groupInvoices = []
        // group invoice
        if(this.props.groupInvoiceAll.items !== undefined){
            this.props.groupInvoiceAll.items.map((group) => {
                groupInvoices.push({
                    id: group.id,
                    name: group.groupName
                })
            })
        } else {groupInvoices = []}
        return (
            <div>
                <div className="margin_left_25">
                    <Row>
                        <Col xs={10} sm={10} md={10} lg={10} className="pull-right">
                            <Panel header="Record Kwh Monthly" bsStyle="info">
                                <form onSubmit={handleSubmit(this.handleSearch.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col xs={4} sm={4} md={4} lg={4}>
                                            <strong>Book Date Payment <span className="label-require">*</span></strong>
                                            <Field name="bookDate" component={DateTimePicker} placeholder="From book date payment"
                                                   defaultDate={this.state.bookDate}
                                                   handleChange={this.bookDatePayment.bind(this)}/>
                                        </Col>
                                        <Col xs={3} sm={3} md={3} lg={3}>
                                            {/*<strong>Meter Type</strong>
                                            <Field name="meterTypeId" type="select" component={SelectBox}
                                                   placeholder="All meter types"
                                                   onChange = {this.selectMeterType.bind(this)}
                                                   values={this.state.meterType}
                                                   sortBy="name" icon=""/>
                                            */}
                                            <strong>Group Invoice</strong>
                                            <Field 
                                                name="groupInvoiceId" 
                                                type="select" 
                                                component={SelectBox} 
                                                placeholder="Please select" 
                                                values={groupInvoices} />
                                        </Col>
                                        <Col xs={3} sm={3} md={3} lg={3}>
                                            <strong>Transformer</strong>
                                            <Field name="transformerId" type="select" component={SelectBox}
                                                   placeholder="Please select"
                                                   onChange = {this.selectTransformer.bind(this)}
                                                   values={this.state.transformers}
                                                   sortBy="name" icon=""/>
                                        </Col>
                                        <Col md={2} lg={2} className="pull-right">
                                            <br/>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                        </Col>
                                    </Row>
                                </form>
                            </Panel>
                        </Col>
                    </Row>
                </div>
                <div id="loading" style={{display: 'none', marginTop: 20, textAlign: 'center'}}>
                    <i className="fa fa-refresh fa-spin fa-3x fa-fw" style={{color: '#777'}}></i>
                    <span className="sr-only">Loading...</span>
                </div>
                <div className="margin_left_25">
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="table-to-xls"
                                filename="record-khw-monthly"
                                sheet="tablexls"
                                buttonText="Export to XLS"/>
                            {' '}
                            <Button 
                                style={{padding: '9px'}}
                                onClick={() => this.handleRefresh()}><i className="fa fa-refresh"></i> Refresh</Button>
                            <table id="table-to-xls" className="list-smaller-table" style={{width: '100%'}}>
                                <thead hidden>
                                    <tr>
                                        <td className="bg-none" colSpan={17}>
                                            <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "10pt"}}>
                                                {this.state.settings.find(i => i.keyword === "OFFICE_TITLE_KH").value}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="bg-none" colSpan={17}>
                                            <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "10pt"}}>
                                                {this.state.settings.find(i => i.keyword === "OFFICE_ADDRESS").value}
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="bg-none" colSpan={17}>
                                            <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "10pt"}}>របាយការណ៍ ចុះស្រង់អំណានសំរាប់ ថ្ងៃ ……… ខែ {monthKh} ឆ្នាំ ២០១៧</p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td className="bg-none" colSpan={5}>
                                            <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "10pt"}}>ពីថ្ងៃ ខែ : ……… / ……… / ………</p>
                                        </td>
                                        <td className="bg-none" colSpan={5}>
                                            <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "10pt"}}>ដល់ថ្ងៃ ខែ : ……… / ……… / ………</p>
                                        </td>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>ល.រ</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}><a href="#" onClick={e =>{e.preventDefault(); this.handleSort('id')}} className="sort-by">អត្ថលេខ</a></th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>អតិថិជន</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}><a href="#" onClick={e =>{e.preventDefault(); this.handleSort('areaNameKh')}} className="sort-by">តំបន់</a></th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}><a href="#" onClick={e =>{e.preventDefault(); this.handleSort('poleSerial')}} className="sort-by">បង្គោល</a></th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>ប្រអប់</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>អំពែរ</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>លេខកុងទ័រ</th>
                                        <th colSpan="3" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>ប្រវត្តិប្រើប្រាស់៣ខែ</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>មេគុណ</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}}>អំណានចាស់</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}} hidden>ថាមពលចាស់</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}} hidden>អំណានថ្មី</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}} hidden>ថាមពល</th>
                                        <th rowSpan="2" style={{fontFamily: "Kh Siemreap", fontSize: "9pt"}} hidden>ផ្សេងៗ</th>
                                    </tr>
                                    <tr>
                                        <th>{arrDate[0] !== undefined ? arrDate[0].month+"-"+arrDate[0].year : null}</th>
                                        <th>{arrDate[1] !== undefined ? arrDate[1].month+"-"+arrDate[1].year : null}</th>
                                        <th>{arrDate[2] !== undefined ? arrDate[2].month+"-"+arrDate[2].year : null}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    arrRecords.length > 0 ?
                                        arrRecords.map((record, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index+1}</td>
                                                    <td>{record.customerId}</td>
                                                    <td>{record.customerName}</td>
                                                    <td>{record.areaNameKh}</td>
                                                    <td>{record.poleSerial}</td>
                                                    <td>{record.boxSerial}</td>
                                                    <td>{record.ampereValue}</td>
                                                    <td>{record.meterSerials}</td>
                                                    <td>{record.firstMonth}</td>
                                                    <td>{record.secondMonth}</td>
                                                    <td>{record.thirdMonth}</td>
                                                    <td>{record.coefficients.toString()}</td>
                                                    <td>{record.lastUsageRecords}</td>
                                                    <td hidden>{record.lastReactiveRecords}</td>
                                                    <td hidden></td>
                                                    <td hidden></td>
                                                    <td hidden></td>
                                                </tr>
                                            )
                                        })
                                    :
                                    <tr className="text-align-center">
                                        <td colSpan={17}>There is no data</td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                            {/*<BootstrapTable
                                data={arrRecords} //list of records
                                trClassName={trClassNameFormat}  //apply style on cell
                                bodyStyle={{ fontSize:'10pt'}}
                                options={{
                                    sizePerPageList: [ 10, 20, 30, 40 , 50, arrRecords.length ],
                                    paginationShowsTotal: true,
                                    sortName: 'customerName',  // default sort column name
                                    sortOrder: 'asc',  // default sort order
                                    afterTableComplete: onAfterTableComplete // A hook for after table render complete.
                                }}
                                exportCSV
                                hover // apply hover style on row
                                pagination
                            >
                                <TableHeaderColumn row='0' rowSpan='2' width='70px' dataField='no' dataSort isKey autoValue>ល.រ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='80px' dataField='customerId' dataSort >អត្ថលេខ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='150px' dataField='customerName' dataSort>អតិថិជន</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='100px' dataField='areaNameKh' dataSort>តំបន់</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='100px' dataField='transformerSerial' dataSort>ត្រង់ស្វូរម៉ាទ័រ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='70px' dataField='poleSerial' dataSort>បង្គោល</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='70px' dataField='boxSerial' dataSort>ប្រអប់</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='70px' dataField='ampereValue' dataSort>អំពែរ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='150px' dataField='meterSerials' dataSort>លេខកុងទ័រ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='150px' dataField='poleAddress' dataSort>អាស័យដ្ខាន</TableHeaderColumn>

                                <TableHeaderColumn row='0' colSpan='3' width='210'>ប្រវត្តិប្រើប្រាស់៣ខែ</TableHeaderColumn>
                                <TableHeaderColumn row='1' dataField='firstMonth' width='70px'>{arrDate[0] !== undefined ? arrDate[0].month+"-"+arrDate[0].year : null}</TableHeaderColumn>
                                <TableHeaderColumn row='1' dataField='secondMonth' width='70px'>{arrDate[1] !== undefined ? arrDate[1].month+"-"+arrDate[1].year : null}</TableHeaderColumn>
                                <TableHeaderColumn row='1' dataField='thirdMonth' width='70px'>{arrDate[2] !== undefined ? arrDate[2].month+"-"+arrDate[2].year : null}</TableHeaderColumn>

                                <TableHeaderColumn row='0' rowSpan='2' width='80px' dataField='coefficients' dataSort>មេគុណ</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='150px' dataField='lastUsageRecords' dataSort>អំណានចាស់</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='100px' dataField='newUsageRecords' dataSort>អំណានថ្មី</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='150px' dataField='lastReactiveRecords' dataSort>ថាមពលចាស់</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='100px' dataField='newReactiveRecords' dataSort>ថាមពលថ្មី</TableHeaderColumn>
                                <TableHeaderColumn row='0' rowSpan='2' width='80px' dataField='others' dataSort>ផ្សេងៗ</TableHeaderColumn>
                            </BootstrapTable>*/}
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }

}

RecordKhwMonthly = reduxForm({
    form: 'form_record_kwh_monthly',
    validate: (values) => {
        const errors = {};
        if (values.bookDate === undefined) {
            errors.bookDate = 'Book Date is required!';
        }
        return errors
    }
})(RecordKhwMonthly);

function mapStateToProps(state) {
    //console.log("DATA : ",state.transformer.getAllTransformersForKwhMonthly);
    return {
        meterTypeAll: state.meterType.meterTypeAll,
        groupInvoiceAll: state.groupInvoice.groupInvoiceAll,
        getAllTransformersForKwhMonthly: state.transformer.getAllTransformersForKwhMonthly,
        getAllRecordKhwMonthly: state.recordKhwMonthly.getAllRecordKhwMonthly
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllMeterTypesAction, 
        getAllGroupInvoiceAction,
        getAllRecordKhwMonthlyAction,
        getAllTransformerForKhwMonthlyAction 
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(RecordKhwMonthly);