import React from 'react';
import { Row, Col, Button, Panel, FormControl, FormGroup } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllInvoicesCustomerPaymentAction, getInvoiceByBarcodeAction } from '../../../actions/transaction/quickPayment';
import { getAllCashBoxesByUserIdAction } from '../../../actions/accounting/cashBox';
import { addCustomerBookPaymentAction } from '../../../actions/invoice/bookPayment';
// import { getAllLocationPayAction } from '../../../actions/customer/finance/locationPay';
import { getAllPaymentTypesAction } from '../../../actions/customer/finance/paymentType';
import { getAllInvoiceTypeAction } from '../../../actions/invoice/invoiceType';
import ReactLoading from 'react-loading';
import moment from 'moment';

let invoice = {
    invoiceType: 0,
    invoiceNo: '',
    customerId: 0
};
let cashBoxId = 0;
let invoiceId = 0;
let customerId = 0;
let paidDate = '';
let userAccounts = [];
class QuickPayment extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            paidDate: null,
            userAccounts: [],
            accountName: '',
            //locationPay: [],
            paymentType: [],
            paidRiel: true,
            paidUSD: true,
            invoiceTypes : []
        };
        this.handlePaid = this.handlePaid.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount(){
        this.setState({paidDate: moment(new Date())});
        // this.props.getAllLocationPayAction();
        this.props.getAllPaymentTypesAction();
        this.props.getAllInvoiceTypeAction();
        this.props.getAllCashBoxesByUserIdAction();
        this.props.getAllInvoicesCustomerPaymentAction(invoice);
    }

    componentWillUnmount() {
        userAccounts = [];
        this.setState({
            userAccounts: [],
            //locationPay: [],
            paymentType: []
        });
        if(this.props.getAllInvoicesCustomerPayment.status === 200) 
            this.props.getAllInvoicesCustomerPayment.data.items = [];
        this.props.dispatch(initialize("form_quick_payment", {}));
    }

    componentWillReceiveProps(data){
        /* user accounts */
        if(data.getAllCashBoxesByUserId.status === 200){
            userAccounts = data.getAllCashBoxesByUserId.data.items;
        }
        // /* location pay */
        // if(data.getAllLocationPays.status === 200){
        //     this.setState({locationPay : []});
        //     let arrLocations = [];
        //     let items = data.getAllLocationPays.data.items;
        //     items.forEach((element) => {
        //         arrLocations.push({
        //             "id": element.id,
        //             "name": element.location
        //         });
        //     });
        //     this.setState({locationPay : arrLocations});
        // }
        /* payment type */
        if(data.getAllPaymentTypes.status === 200){
            this.setState({paymentType : []});
            let arrPaymentTypes = [];
            let items = data.getAllPaymentTypes.data.items;
            items.forEach((element) => {
                arrPaymentTypes.push({
                    "id": element.id,
                    "name": element.paymentType
                });
            });
            this.setState({paymentType : arrPaymentTypes});
        }
        /* invoice type */
        if(data.invoiceTypeAll.status === 200) {
            this.setState({invoiceTypes: []});
            let items = data.invoiceTypeAll.data.items;
            let arrInvoiceTypes = [];
            items.forEach((element) => {
                arrInvoiceTypes.push({
                    "id": element.id,
                    "name": element.invoiceType
                });
            });
            this.setState({invoiceTypes: arrInvoiceTypes});
        }
        /* list all customers invoice */
        if(data.getAllInvoicesCustomerPayment.status === 200 || data.getAllInvoicesCustomerPayment.status === 404 || data.getAllInvoicesCustomerPayment.status === 500){
            document.getElementById('loading').style.display = 'none';
        }
        /* get invoice by barcode */
        if(data.getInvoiceByBarcode.status === 200){
            data.getInvoiceByBarcode.status = 0;
            let item = data.getInvoiceByBarcode.data.items[0];
            invoiceId = item.invoiceId;
            let temporary = {
                invoiceId : item.invoiceId,
                barcode : item.barcode,
                invoiceNo : item.invoiceNo,
                paidAmountRiel : item.unpaidAmountRiel,
                paidAmountUSD : item.unpaidAmountUSD
            };
            if(temporary.paidAmountRiel <= 0) this.setState({paidRiel: false});
            else this.setState({paidRiel: true});
            if(temporary.paidAmountUSD <= 0) this.setState({paidUSD: false});
            else this.setState({paidUSD: true});
            this.props.dispatch(initialize("form_quick_payment",temporary));
        }
        if(data.getInvoiceByBarcode.status === 404 || data.getInvoiceByBarcode.status === 500){
            data.getInvoiceByBarcode.status = 0;
            document.getElementById('barcode').value = "";
            document.getElementById("barcode").focus();
            alert("Invoice barcode not found!");
        }
        /* quick payment */
        if(data.addCustomerBookPayment.status === 200){
            alert("Successfully book payment.");
            data.addCustomerBookPayment.status = 0;
            document.getElementById('barcode').value = "";
            document.getElementById("barcode").focus();
            invoice.invoiceType = 0;
            invoice.invoiceNo = '';
            invoice.customerId = 0;
            this.props.getAllInvoicesCustomerPaymentAction(invoice);
            this.props.dispatch(initialize('form_quick_payment', null));
        }
        if(data.addCustomerBookPayment.status === 404 || data.addCustomerBookPayment.status === 500){
            document.getElementById('barcode').value = "";
            document.getElementById("barcode").focus();
            invoice.invoiceType = 0;
            invoice.invoiceNo = '';
            invoice.customerId = 0;
            this.props.getAllInvoicesCustomerPaymentAction(invoice);
            alert("Fail with book payment!");
        }
    }
    /* select cash balance */
    selectGLAccount(event){
        if(event.target.value === ""){
            cashBoxId = 0;
            this.setState({accountName: 'Unknown'});
        }else{
            for(let i=0; i< userAccounts.length; i++){
                if(userAccounts[i].id === Number(event.target.value)){
                    cashBoxId = userAccounts[i].id;
                    this.setState({accountName: userAccounts[i].name}); break;
                }
            }
        }
    }
    /* handle search */
    getCustomerIdText(event){
        invoice.customerId = event.target.value;
    }
    getInvoiceNoText(event){
        invoice.invoiceNo = event.target.value;
    }
    selectInvoiceType(event){
        if(event.target.value === undefined) invoice.invoiceType = 0;
        invoice.invoiceType = Number(event.target.value);
    }
    handleSearch(){
        document.getElementById('loading').style.display = 'block';
        this.props.getAllInvoicesCustomerPaymentAction(invoice);
    }
    /* helper functions */
    handleRefresh(){
        document.getElementById('barcode').value = "";
        document.getElementById("barcode").focus();
        this.props.dispatch(initialize("form_quick_payment",null));
        invoice.invoiceType = 0;
        invoice.invoiceNo = '';
        invoice.customerId = 0;
        this.props.getAllInvoicesCustomerPaymentAction(invoice);
    }
    /* pay invoice */
    handlePaidDate(date){
        this.setState({paidDate: date});
    }
    handlePaid(invoice){
        invoiceId = Number(invoice.invoiceId);
        customerId = Number(invoice.customerId);
        let data = {
            invoiceId : invoice.invoiceId,
            barcode : invoice.barcode,
            invoiceNo : invoice.invoiceNo,
            paidAmountRiel : invoice.unpaidAmountRiel,
            paidAmountUSD : invoice.unpaidAmountUSD
        };
        if(data.paidAmountRiel <= 0) this.setState({paidRiel: false});
        else this.setState({paidRiel: true});
        if(data.paidAmountUSD <= 0) this.setState({paidUSD: false});
        else this.setState({paidUSD: true});
        this.props.dispatch(initialize("form_quick_payment",data));
    }
    handleSubmit(values){
        if(cashBoxId !== 0){
            if(values.paidDate === undefined) {
                let today = new Date();
                paidDate = moment(today).format("YYYY-MM-DD");
            }else{
                paidDate = values.paidDate;
            }
            let bookPayment = {
                "cashBoxId" : cashBoxId,
                "customerId" : customerId,
                "invoiceId": invoiceId,
                "paidAmountRiel": Number(values.paidAmountRiel),
                "paidAmountUSD": Number(values.paidAmountUSD),
                "paidDate": paidDate,
                //"paymentLocationId": 0,
                "paymentTypeId": values.paymentTypeId,
                "status": 1
            };
            // define location id coz it is optional
            // if(values.paymentLocationId !== undefined){
            //     bookPayment.paymentLocationId = Number(values.paymentLocationId);
            // }

            //console.log("data :",bookPayment);
            this.props.addCustomerBookPaymentAction(bookPayment);
        }else{
            alert("Please select Account Number!");
        }
    }
    /* to scan barcode invoice */
    handleBarcode(event){
        if(event.keyCode === 13){
            event.preventDefault();
            this.props.getInvoiceByBarcodeAction(event.target.value);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div>
                <div className="margin_left_25">
                    <div>
                        <Row>
                            <Col md={2} lg={2} className="label-name">
                                <strong>Account Number <span className="label-require">*</span></strong>
                            </Col>
                            <Col md={3} lg={3}>
                                <FormGroup controlId="formControlsSelect">
                                    <FormControl componentClass="select" onChange={this.selectGLAccount.bind(this)}>
                                        <option value="">Please select account number</option>
                                        {
                                            this.props.getAllCashBoxesByUserId.status === 200 ?
                                                this.props.getAllCashBoxesByUserId.data.items.map((account,index) => {
                                                return (
                                                    <option key={index} value={account.id}
                                                    >{account.name}</option>
                                                )
                                                })
                                                :null
                                        }
                                    </FormControl>
                                </FormGroup>
                            </Col>
                            <Col md={7} lg={7} className="label-name"><b>is {this.state.accountName}</b></Col>
                        </Row>
                    </div>
                    <Panel header="Quick Payment" bsStyle="info">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={6} lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Barcode</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <input className="form-control" type="text" name="barcode" id = "barcode" placeholder="Scan barcode here"
                                                   onKeyDown={this.handleBarcode.bind(this)} />
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Invoice No <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="invoiceNo" type="text" component={TextBox} label="Invoice No" icon=""
                                                   disabled = {true}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Payment Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paymentTypeId" type="select" component={SelectBox} placeholder="Please Select Payment Type"
                                                   values={this.state.paymentType} sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    {/*<Row>*/}
                                        {/*<Col md={4} lg={4} className="label-name">*/}
                                            {/*<strong>Location</strong>*/}
                                        {/*</Col>*/}
                                        {/*<Col md={8} lg={8}>*/}
                                            {/*<Field name="paymentLocationId" type="select" component={SelectBox} placeholder="Select Location"*/}
                                                   {/*values={this.state.locationPay} sortBy="name" icon=""/>*/}
                                        {/*</Col>*/}
                                    {/*</Row>*/}
                                </Col>
                                <Col md={6} lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Payment Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidDate" component={DateTimePicker} placeholder="Pay Date"
                                                   defaultDate={this.state.paidDate} handleChange={this.handlePaidDate.bind(this)}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Amount Riel</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidAmountRiel" type="text" component={TextBox} label="Amount RIEL" icon=""
                                                   disabled = {this.state.paidRiel ? false : true}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Amount USD</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidAmountUSD" type="text" component={TextBox} label="Amount USD" icon=""
                                                   disabled = {this.state.paidUSD ? false : true}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>

                        </form>
                    </Panel>
                </div>
                <div className="margin_left_25">
                    <Row>
                        <Col md={3} lg={3}>
                            <strong>Customer ID </strong>
                            <Field name="customerId" type="text" component={TextBox} label="Invoice No" icon=""
                                   onChange={this.getCustomerIdText.bind(this)}
                            />
                        </Col>
                        <Col md={3} lg={3}>
                            <strong>Invoice No</strong>
                            <Field name="invoiceno" type="text" component={TextBox} label="Invoice No" icon=""
                                   onChange={this.getInvoiceNoText.bind(this)}
                            />
                        </Col>
                        <Col md={3} lg={3}>
                            <strong>Invoice Type</strong>
                            <Field name="invoiceType" type="select" component={SelectBox}
                                   placeholder="Select Invoice Type"
                                   onChange = {this.selectInvoiceType.bind(this)}
                                   values={this.state.invoiceTypes}
                                   sortBy="name" icon=""/>
                        </Col>
                        <Col md={3} lg={3} className="pull-right">
                            <br/>
                            <Button bsStyle="primary" onClick={() => this.handleSearch()}>Search</Button>&nbsp;&nbsp;
                            <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                        </Col>
                    </Row>
                </div>
                <div id="loading" style={{marginTop: 20, textAlign: 'center'}}>
                    <i className="fa fa-refresh fa-spin fa-3x fa-fw" style={{color: '#777'}}></i>
                    <span className="sr-only">Loading...</span>
                </div>
                <div className="margin_left_25">
                    <table className="page-permision-table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Invoice No</th>
                            <th>Barcode</th>
                            <th>Customer ID</th>
                            <th>Customer Name</th>
                            <th>Latin Name</th>
                            <th>Amount RIEL</th>
                            <th>Amount USD</th>
                            <th className="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.getAllInvoicesCustomerPayment.status === 200 ?
                                this.props.getAllInvoicesCustomerPayment.data.items.map((invoice, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{index+1}</td>
                                            <td>{invoice.invoiceNo}</td>
                                            <td>{invoice.barcode}</td>
                                            <td>{invoice.customerId}</td>
                                            <td>{invoice.customerName}</td>
                                            <td>{invoice.latinName}</td>
                                            <td>{invoice.unpaidAmountRiel}</td>
                                            <td>{invoice.unpaidAmountUSD}</td>
                                            <td>
                                                <a onClick={() => this.handlePaid(invoice)} className="btn btn-info btn-xs">
                                                    <span>Pay</span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={9} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                </tr>
                        }
                        </tbody>
                    </table>
                </div>

            </div>
        )
    }

}

QuickPayment = reduxForm({
    form: 'form_quick_payment',
    validate: (values) => {
        const errors = {};
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        if(!regex_price.test(values.paidAmountRiel) || values.paidAmountRiel === undefined){
            errors.paidAmountRiel = "Invalid amount as RIEL.";
        }
        if(!regex_price.test(values.paidAmountUSD) || values.paidAmountUSD === undefined){
            errors.paidAmountUSD = "Invalid amount as USD.";
        }
        if(values.invoiceId === undefined){
            errors.invoiceId = "Please select invoice no.";
        }
        if(values.paymentTypeId === undefined){
            errors.paymentTypeId = "Please select payment type.";
        }
        return errors
    }
})(QuickPayment);

function mapStateToProps(state) {
    //console.log("DATA : ",state.cashBoxes.getAllCashBoxesByUserId);
    return {
        getAllCashBoxesByUserId: state.cashBoxes.getAllCashBoxesByUserId,
        //getAllLocationPays: state.locationPays.getAllLocationPays,
        getAllPaymentTypes: state.paymentTypes.getAllPaymentTypes,
        invoiceTypeAll: state.invoiceType.invoiceTypeAll,
        getAllInvoicesCustomerPayment: state.quickPayments.getAllInvoicesCustomerPayment,
        getInvoiceByBarcode: state.quickPayments.getInvoiceByBarcode,
        addCustomerBookPayment: state.customerBookPayment.addCustomerBookPayment
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getInvoiceByBarcodeAction,
        getAllInvoicesCustomerPaymentAction,
        addCustomerBookPaymentAction,
        //getAllLocationPayAction,
        getAllPaymentTypesAction,
        getAllInvoiceTypeAction,
        getAllCashBoxesByUserIdAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(QuickPayment);