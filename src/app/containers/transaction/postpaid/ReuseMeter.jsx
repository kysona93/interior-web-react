import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, initialize, change, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Alert, Table, Button, Row, Col, Panel, } from 'react-bootstrap';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getMeterByMeterSerialAction, reuseMeterPostPaidAction } from '../../../actions/setup/meter';
import moment from 'moment';

class ReuseMeter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            reuseType: [
                { id: "REUSE_METER", name: "Re-use Meter" },
                { id: "RESET_METER", name: "Reset Meter" }
            ],
            reuseDate: null,
            isReuseMeter: true,
            meterId: 0,
            serial: '',
            meterSerial: '',
            meterRound: 1,
        };
    }

    componentWillMount(){
        this.props.dispatch(change('form_reuse_meter', 'reuseType', 'REUSE_METER'));
    }
    
    componentWillReceiveProps(newProps){
        if (newProps.meterBySerial.status === 200) {
            let obj = newProps.meterBySerial.data.items;
            if(obj !== undefined) {
                this.setState({ 
                    meterId: obj.id,
                    meterSerial: this.state.serial,
                    meterRound: obj.usageRound
                });
                this.props.dispatch(change('form_reuse_meter', 'oldUsage1', obj.endUsageRecord));
                this.props.dispatch(change('form_reuse_meter', 'oldUsage2', 0));
            }
        } else if (newProps.meterBySerial.status === 404) {
            alert('Meter Serial Not Found!');
            this.setState({ 
                meterSerial: '',
                meterRound: 1
            });
            this.props.dispatch(change('form_reuse_meter', 'oldUsage1', ''));
            this.props.dispatch(change('form_reuse_meter', 'oldUsage2', ''));
            newProps.meterBySerial.status = 0;
        }

        if(newProps.reuseMeter.status === 200){
            if(newProps.reuseMeter.data.status === 200){
                alert("Successfully re-use meter.");
                this.handleClear();
                newProps.reuseMeter.data.status = 0;
            } else {
                alert("Fail with re-use meter!");
                newProps.reuseMeter.data.status = 0;
            }
            newProps.reuseMeter.status = 0;
        }
        if(newProps.reuseMeter.status === 404 || newProps.reuseMeter.status === 500){
            alert("Fail with re-use meter!");
            newProps.reuseMeter.status = 0;
        }
    }

    handleReuseType(event) {
        if (event.target.value === "REUSE_METER")
            this.setState({ isReuseMeter: true });
        else if (event.target.value === "RESET_METER")
            this.setState({ isReuseMeter: false });
    }

    handleMeterSerial(event) {
        this.setState({ serial: event.target.value });
        this.props.getMeterByMeterSerialAction({ serial: event.target.value });
    }

    handleReuseDate(date) {
        this.setState({ reuseDate: date });
    }

    handleClear() {
        this.setState({ 
            meterSerial: '',
            meterRound: 1,
            reuseDate: null
        });
        this.props.dispatch(reset('form_reuse_meter'));
        this.props.meterBySerial.status = 0;
    }

    handleSubmit(values) {
        let object = {
            "id": this.state.meterId,
            "newReactive1": values.newReactive1,
            "newReactive2": values.newReactive2,
            "newUsage1": values.newUsage1,
            "newUsage2": values.newUsage2,
            "oldUsage1": values.oldUsage1,
            "oldUsage2": values.oldUsage2,
            "originalUsage": values.originalUsage,
            "reuseDate": this.state.reuseDate,
            "reuseType": values.reuseType
        }
        console.log('object ', object);
        this.props.reuseMeterPostPaidAction(object);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        
        return (
            <div className="margin_left_25">
                <Panel header="Re-use Meter" bsStyle="info">
                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                        <Row>
                            <Col md={3} lg={3} className="label-name">
                                <strong>Re-use Type<span className="label-require">*</span></strong>
                                <Field 
                                    name="reuseType" 
                                    type="select" 
                                    component={SelectBox} 
                                    placeholder="Select Re-use Type"
                                    onChange={this.handleReuseType.bind(this)}
                                    values={this.state.reuseType} 
                                    sortBy="name"/>
                            </Col>
                            <Col md={3} lg={3} className="label-name">
                                <strong>Meter Serial<span className="label-require">*</span></strong>
                                <Field 
                                    name="meterSerial" 
                                    type="text"
                                    onBlur={this.handleMeterSerial.bind(this)}
                                    onBlurResetsInput={false}
                                    component={TextBox} 
                                    label="Meter Serial"/>
                            </Col>
                            <Col md={3} lg={3} className="label-name">
                                <strong>Re-use Date<span className="label-require">*</span></strong>
                                <Field 
                                    name="reuseDate" 
                                    component={DateTimePicker} 
                                    placeholder="Re-use Date"
                                    defaultDate={this.state.reuseDate}
                                    handleChange={this.handleReuseDate.bind(this)}/>
                            </Col>
                        </Row>
                        { this.state.isReuseMeter === true ?
                            <Row>
                                <Col md={12} lg={12} >
                                    <fieldset>
                                        <legend>Meter {this.state.meterSerial} (Round {this.state.meterRound})</legend>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Old Usage<span className="label-require">*</span></strong>
                                            <Field 
                                                name="oldUsage1" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Old Usage"/>
                                        </Col>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>New Usage<span className="label-require">*</span></strong>
                                            <Field 
                                                name="newUsage1" 
                                                type="text" 
                                                component={TextBox} 
                                                label="New Usage"/>
                                        </Col>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>New Reactive<span className="label-require">*</span></strong>
                                            <Field 
                                                name="newReactive1" 
                                                type="text" 
                                                component={TextBox} 
                                                label="New Reactive"/>
                                        </Col>
                                    </fieldset>
                                    <fieldset>
                                        <legend>Meter {this.state.meterSerial} (Round {this.state.meterRound + 1})</legend>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Old Usage<span className="label-require">*</span></strong>
                                            <Field 
                                                name="oldUsage2" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Old Usage"/>
                                        </Col>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>New Usage<span className="label-require">*</span></strong>
                                            <Field 
                                                name="newUsage2" 
                                                type="text" 
                                                component={TextBox} 
                                                label="New Usage"/>
                                        </Col>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>New Reactive<span className="label-require">*</span></strong>
                                            <Field 
                                                name="newReactive2" 
                                                type="text" 
                                                component={TextBox} 
                                                label="New Reactive"/>
                                        </Col>
                                    </fieldset>
                                </Col>
                            </Row>
                            :
                            <Row>
                                <Col md={12} lg={12} >
                                    <fieldset>
                                        <legend>Reset Meter {this.state.meterSerial}</legend>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Original Meter Usage<span className="label-require">*</span></strong>
                                            <Field 
                                                name="originalUsage" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Original Usage"/>
                                        </Col>
                                    </fieldset>
                                </Col>
                            </Row>
                        }
                        <Row>
                            <Col mdOffset={8} sm={3} md={2}>
                                <ButtonSubmit 
                                    error={error} 
                                    invalid={invalid} 
                                    submitting={submitting} 
                                    label="Save" />
                            </Col>
                            <Col sm={3} md={2} className="col-padding-left-0">
                                <Button className="btn btn-default btn-block" 
                                    onClick={() => this.handleClear()}>
                                    Cancel
                                </Button>
                            </Col>
                        </Row>
                    </form>

                    { this.state.meterSerial !== '' ?
                        <Row>
                            <Col md={12} lg={12}>
                                <b>&nbsp;Re-use Meter Histories</b>
                                <table className="list-smaller-table">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Re-use Date</th>
                                            <th>Meter Serial</th>
                                            <th>Current Round</th>
                                            <th>Description</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.meterBySerial.status === 200 ?
                                            this.props.meterBySerial.data.items !== undefined ?
                                                this.props.meterBySerial.data.items.meterRoundHistories.map((i, index) => {
                                                    return(
                                                        <tr key={index}>
                                                            <td>{index + 1}</td>
                                                            <td>{moment(i.changeDate).format('DD-MM-YYYY')}</td>
                                                            <td>{this.props.meterBySerial.data.items.meterSerial}</td>
                                                            <td>{i.usageRound}</td>
                                                            <td>{i.description}</td>
                                                        </tr>
                                                    )
                                                })
                                                : null
                                            : null
                                        }
                                    </tbody>
                                </table>
                            </Col>
                        </Row> 
                    : null } 
                </Panel>
            </div>
        )
    }

}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_reuse_meter'));

ReuseMeter = reduxForm({
    form: 'form_reuse_meter',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{2,1024}/;
        const regex_number = /[0-9]{1,10}/;

        const errors = {};
        if (values.reuseType === undefined || values.reuseType === "") {
            errors.reuseType = 'Please select re-use type!';
        }
        if (!regex_text.test(values.meterSerial) || values.meterSerial === undefined) {
            errors.meterSerial = 'Please input at least 2 characters !!';
        }
        if (values.reuseDate === undefined || values.reuseDate === "" || values.reuseDate === null) {
            errors.reuseDate = 'Re-use Date is required!';
        }
        if (!regex_number.test(values.oldUsage1) || values.oldUsage1 === "") {
            errors.oldUsage1 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.oldUsage2) || values.oldUsage2 === "") {
            errors.oldUsage2 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.newUsage1) || values.newUsage1 === "") {
            errors.newUsage1 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.newUsage2) || values.newUsage2 === "") {
            errors.newUsage2 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.newReactive1) || values.newReactive1 === "") {
            errors.newReactive1 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.newReactive2) || values.newReactive2 === "") {
            errors.newReactive2 = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.originalUsage) || values.originalUsage === "") {
            errors.originalUsage = 'Please input only numeric !!';
        }
        return errors;
    }
})(ReuseMeter);

function mapStateToProps(state) {
    console.log(state.meter.reuseMeter)
    return {
        meterBySerial: state.meter.meterBySerial,
        reuseMeter: state.meter.reuseMeter
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getMeterByMeterSerialAction,
        reuseMeterPostPaidAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ReuseMeter);