import React from 'react';
import { Row, Col, Panel, Button, ControlLabel, Form, FormGroup, FormControl, Image  } from 'react-bootstrap';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import {Typeahead} from 'react-bootstrap-typeahead';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { addMeterUsageRecordAction, getMetersByCustomerIdAction, getMeterUsageByMeterSerial,
        updateMeterUsageByIdAction, importExcelMeterUsageRecordAction, verifyMeterUsageIndividualAction,
        getAllCustomerIdsAndMeterSerialAction } from '../../../actions/transaction/usageEntry';
import moment from 'moment';
import ReactLoading from 'react-loading';
import SuccessImport from './SuccessImport';

let serials = []; // for extracting into auto completed
let arrUsages = []; // for adding into usage entry
let usage = []; // to list in verify table meter usage
let meters = []; // to store user's meter
let lastRecords = []; // to store last month meter records

let customerId = 0; // search meter information by customer id
let meterSerial = ""; // search all meter serial by meter serial
let oldUsage = 0; // to verify customer problem
let newUsage = 0; // to verify customer problem

class UsageEntry extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            label: 'Add',
            labelForm: 'Add New Usage',
            meter1: '',
            recordDate: null,
            usageId: 0,
            usage : false,
            arrUsages: [],
            updated: false,
            error : false,
            isImportSuccess: false
        };
        this.searchCustomerID = this.searchCustomerID.bind(this);
        this.searchMeterSerial = this.searchMeterSerial.bind(this);
        this.getCustomerId = this.getCustomerId.bind(this);
        this.getMeterSerial = this.getMeterSerial.bind(this);
        this.uploadExcelFile = this.uploadExcelFile.bind(this);
        this.importUsage = this.importUsage.bind(this);
        this.handleBack = this.handleBack.bind(this);
        this.clearText = this.clearText.bind(this);
        this.saveUsage = this.saveUsage.bind(this);
    }

    componentWillMount(){
        this.props.getAllCustomerIdsAndMeterSerialAction();
    }
    
    handleClearValue() {
        this.props.getMeterUsageBySerial.status = 0;
        document.getElementById("myForm").reset();
    }

    componentWillUnmount() {
        this.handleClearValue();
    }

    componentWillReceiveProps(data){
        /* extract meter serial into auto completed */
        if(data.getAllCustomerIdsAndMeterSerials.status === 200){
            serials = data.getAllCustomerIdsAndMeterSerials.data.items;
        }
        /* when user select on auto completed and search by customer ID */
        if(data.getMetersByCustomerId.status === 200){
            meters = [];
            this.setState({meter1: ''});
            let items = data.getMetersByCustomerId.data.items;
            items.forEach((element) => {
                meters.push({
                    "id": element.id,
                    "serial": element.meterSerial
                });
            });
            // check to know how many meters which a customer have
            let initData = {
                meterSerial2: '',
                meterSerial3: '',
                newUsageRecord1 : '',
                newUsageRecord2 : '',
                newUsageRecord3 : '',
                newReactiveRecord1 : '',
                newReactiveRecord2 : '',
                newReactiveRecord3 : ''
            };
            if(meters.length === 1){
                this.setState({meter1: meters[0].serial});
            }else if(meters.length === 2){
                this.setState({meter1: meters[0].serial});
                initData.meterSerial2 = meters[1].serial;
            }else {
                this.setState({meter1: meters[0].serial});
                initData.meterSerial2 = meters[1].serial;
                initData.meterSerial3 = meters[2].serial;
            }
            this.props.dispatch(initialize("form_usage_entry",initData));
            data.getMetersByCustomerId.status = 0;
        }
        if(data.getMetersByCustomerId.status === 401 || data.getMetersByCustomerId.status === 404 || data.getMetersByCustomerId.status === 500){
            meters = [];
            this.setState({meter1: ''});
            this.props.dispatch(initialize("form_usage_entry",null));
            data.getMetersByCustomerId.status = 1;
            data.getMetersByCustomerId.data = undefined;
            alert("Customer Not Found!");
        }
        /* add meter usage entry */
        if(data.addMeterUsageRecord.status === 200){
            arrUsages = [];
            meters = [];
            customerId  = 0;
            meterSerial = '';
            this.setState({meter1: ''});
            this.setState({arrUsages: []});
            this.setState({error: false});
            this.setState({recordDate: null});
            this.props.dispatch(initialize('form_usage_entry', null));
            alert("Successfully add new usage");
            data.getMetersByCustomerId.status = 1;
            data.getMetersByCustomerId.data = undefined;
            data.addMeterUsageRecord.status = 0;
        }
        if(data.addMeterUsageRecord.status === 401 || data.addMeterUsageRecord.status === 500){
            meters = [];
            arrUsages = [];
            customerId  = 0;
            meterSerial = '';
            this.setState({meter1: ''});
            this.setState({arrUsages: []});
            this.setState({error: false});
            this.setState({recordDate: null});
            alert("Fail with add new usage!");
            data.getMetersByCustomerId.status = 1;
            data.getMetersByCustomerId.data = undefined;
            data.addMeterUsageRecord.status = 0;
        }
        /* import meter usage by excel */
        if(data.importExcelMeterUsageRecord.status === 200){
            if (data.importExcelMeterUsageRecord.data.status === 200)
                this.setState({isImportSuccess: true});
            else {
                if (data.importExcelMeterUsageRecord.data.message === "Remote operation failed: null") {
                    this.setState({isImportSuccess: true});
                } else
                    alert("Fail with import excel file! " + data.importExcelMeterUsageRecord.data.message)
            }
            document.getElementById('loading').style.display = 'none';
            data.importExcelMeterUsageRecord.status = 0;
        }
        if(data.importExcelMeterUsageRecord.status === 401 || data.importExcelMeterUsageRecord.status === 500){
            document.getElementById('loading').style.display = 'none';
            alert("Fail with import excel file");
            this.setState({usage: false});
        }
        /* update meter usage when user input with wrong information */
        if(data.updateMeterUsageById.status === 200){
            alert("Successfully updated meter usage");
            this.props.dispatch(initialize('form_usage_entry', null));
            this.setState({updated: false, label: 'Save', labelForm: 'Add New Usage', usageId: 0});
            data.updateMeterUsageById.status = 0;
            data.getMeterUsageBySerial.status = 0;
        }
        if(data.updateMeterUsageById.status === 401 || data.updateMeterUsageById.status === 500){
            this.setState({recordDate: null});
            this.props.dispatch(initialize('form_usage_entry', null));
            alert("Fail with update meter usage");
            this.setState({updated: false, label: 'Save', labelForm: 'Add New Usage', usageId: 0});
        }
        /* verify meter usage by individual user */
        if(data.verifyMeterUsageIndividual.status === 200){
            lastRecords = [];
            oldUsage = 0;
            usage = [];
            let item = data.verifyMeterUsageIndividual.data.items;
            // to sum all old usage amount
            item.forEach((element) => {
                oldUsage+= element.activePower;
            });
            usage.push({
                'customerId': item[0].customerId,
                'customerName': item[0].customerName,
                'meterSerials': meters[0].serial+(meters[1] !== undefined ? ("/"+meters[1].serial): '' )+(meters[2] !== undefined ? ("/"+meters[2].serial): ''),
                'newUsageRecords': '',
                'recordDate': ''
            });
            // to save all last month usage record info
            if(meters.length === 1){
                lastRecords.push({
                    'serial': item[0].meterSerial,
                    'lastRecord': item[0].newRecord
                });
            }else if(meters.length === 2){
                for(let i= 0; i<=1; i++){
                    lastRecords.push({
                        'serial': item[i].meterSerial,
                        'lastRecord': item[i].newRecord
                    });
                }
            }else {
                for(let i= 0; i<=2; i++){
                    lastRecords.push({
                        'serial': item[i].meterSerial,
                        'lastRecord': item[i].newRecord
                    });
                }
            }
            data.verifyMeterUsageIndividual.status = 0;
        }
        if(data.verifyMeterUsageIndividual.status === 401 || data.verifyMeterUsageIndividual.status === 404 || data.verifyMeterUsageIndividual.status === 500){
            lastRecords = [];
            oldUsage = 0;
            usage = [];
            alert("This user has no old usage!");
            data.verifyMeterUsageIndividual.status = 1;
        }
    }
    /* search meter information by customer ID or meter serial */
    getCustomerId(event){
        customerId = event.target.value;
    }
    searchCustomerID(){
        this.setState({recordDate: null});
        if(!(customerId === "" || customerId === undefined || customerId === null)){
            this.props.getMetersByCustomerIdAction(customerId);
        }else{
            this.props.getMetersByCustomerIdAction(0);
        }
    }
    filterMeterSerial(values){
        if(values.length > 0){
            this.setState({recordDate: null});
            customerId = values[0].customerId;
            this.props.getMetersByCustomerIdAction(customerId);
        }
    }
    /* search meter serial to update if something went wrong */
    getMeterSerial(event){
        meterSerial = event.target.value;
    }
    searchMeterSerial(){
        this.props.getMeterUsageByMeterSerial(meterSerial);
    }
    updateItem(data){
        this.setState({updated: true, label: 'Edit', labelForm: 'Edit Meter Usage', usageId: data.id});
        let newData = {
            "meterSerial1": data.meterSerial,
            "newUsageRecord1": data.newUsageRecord
        };
        this.props.dispatch(initialize('form_usage_entry', newData));
    }
    /* add meter usage information into list SAVE */
    handleDateUsage(date){
        this.setState({recordDate: date});
    }
    handleAdd(values){
        arrUsages = [];
        newUsage = 0;
        let active = '';
        // verify to find problem for meter usage
        let criteria = {
            'bookDate': values.recordDate,
            'customerId': customerId
        };
        console.log("criteria :", customerId);
        this.props.verifyMeterUsageIndividualAction(criteria);
        setTimeout(()=>{
            console.log("last record:", lastRecords);
            if(!this.state.updated){
                // save meter usage
                let usages = [];
                if(meters.length === 1){
                    console.log("one meters");
                    arrUsages = [
                        {
                            "meterId": Number(meters[0].id),
                            "meterSerial": this.state.meter1, //values.meterSerial1
                            "newUsageRecord": Number(values.newUsageRecord1),
                            "newReactiveRecord": Number(values.newReactiveRecord1),
                            "recordDate": values.recordDate
                        }
                    ];
                    active = values.newUsageRecord1;
                    for(let i=0; i< lastRecords.length; i++){
                        if(arrUsages[0].meterSerial === lastRecords[i].serial){
                            newUsage = Number(values.newUsageRecord1) - Number(lastRecords[i].lastRecord);
                            break;
                        }
                    }
                }else if(meters.length === 2){
                    console.log("two meters");
                    arrUsages = [
                        {
                            "meterId": Number(meters[0].id),
                            "meterSerial": this.state.meter1,
                            "newUsageRecord": Number(values.newUsageRecord1),
                            "newReactiveRecord": Number(values.newReactiveRecord1),
                            "recordDate": values.recordDate
                        },
                        {
                            "meterId": Number(meters[1].id),
                            "meterSerial": values.meterSerial2,
                            "newUsageRecord": Number(values.newUsageRecord2),
                            "newReactiveRecord": Number(values.newReactiveRecord2),
                            "recordDate": values.recordDate
                        }
                    ];
                    active = values.newUsageRecord1+"/"+values.newUsageRecord2;
                    for(let i=0; i< lastRecords.length; i++){
                        if(arrUsages[0].meterSerial === lastRecords[i].serial){
                            newUsage = newUsage + Number(values.newUsageRecord1) - Number(lastRecords[i].lastRecord);
                        }
                        if(arrUsages[1].meterSerial === lastRecords[i].serial){
                            newUsage += Number(values.newUsageRecord2) - Number(lastRecords[i].lastRecord);
                        }
                    }
                }else {
                    console.log("three meters");
                    arrUsages = [
                        {
                            "meterId": Number(meters[0].id),
                            "meterSerial": this.state.meter1,
                            "newUsageRecord": Number(values.newUsageRecord1),
                            "newReactiveRecord": Number(values.newReactiveRecord1),
                            "recordDate": values.recordDate
                        },
                        {
                            "meterId": Number(meters[1].id),
                            "meterSerial": values.meterSerial2,
                            "newUsageRecord": Number(values.newUsageRecord2),
                            "newReactiveRecord": Number(values.newReactiveRecord2),
                            "recordDate": values.recordDate
                        },
                        {
                            "meterId": Number(meters[2].id),
                            "meterSerial": values.meterSerial3,
                            "newUsageRecord": Number(values.newUsageRecord3),
                            "newReactiveRecord": Number(values.newReactiveRecord3),
                            "recordDate": values.recordDate
                        }
                    ];
                    active = values.newUsageRecord1+"/"+values.newUsageRecord2+"/"+values.newUsageRecord3;
                    for(let i=0; i< lastRecords.length; i++){
                        if(arrUsages[0].meterSerial === lastRecords[i].serial){
                            newUsage = newUsage + (Number(values.newUsageRecord1) - Number(lastRecords[i].lastRecord));
                        }
                        if(arrUsages[1].meterSerial === lastRecords[i].serial){
                            newUsage = newUsage + (Number(values.newUsageRecord2) - Number(lastRecords[i].lastRecord));
                        }
                        if(arrUsages[2].meterSerial === lastRecords[i].serial){
                            newUsage = newUsage + (Number(values.newUsageRecord3) - Number(lastRecords[i].lastRecord));
                        }
                    }
                }
                // verify meter usage to check the problem
                usage[0].newUsageRecords = active;
                usage[0].recordDate = moment(values.recordDate).format("DD-MM-YYYY");
                this.setState({arrUsages: usage});
                console.log("new usage: "+newUsage);
                console.log("old usage: "+oldUsage);
                let err = (newUsage/oldUsage)*100;
                if(err < 30) this.setState({error: true});
                else this.setState({error: false});
            }else{
                // update meter usage
                let meterUsage = {
                    "id": Number(this.state.usageId),
                    "newUsageRecord": Number(values.newUsageRecord1),
                    "newReactiveRecord": Number(values.newReactiveRecord1),
                    "recordDate": values.recordDate
                };
                this.props.updateMeterUsageByIdAction(meterUsage);
            }
        },200);
    }
    /* handle save meter usage */
    saveUsage(){
        console.log("Data before Save :",arrUsages);
        this.props.addMeterUsageRecordAction(arrUsages);
    }
    /* import meter usage by excel file */
    importUsage(){
        this.setState({usage: true});
    }
    handleBack(){
        this.setState({usage: false});
    }
    uploadExcelFile(){
        document.getElementById('loading').style.display = 'block';
        // Attach file
        let excelFile = document.getElementById('submitfile');
        let formData = new FormData();
        formData.append("file", excelFile.files[0]);
        this.props.importExcelMeterUsageRecordAction(formData);
    }
    /* helper functions */
    clearText(){
        this.handleClearValue();
        this.props.getMetersByCustomerId.status = 1;
        this.props.getMetersByCustomerId.data = undefined;
        this.setState({meter1: ''});
        this.setState({recordDate: null});
        this.setState({arrUsages: []});
        this.setState({error: false});
        let initData = {
            meterSerial2: '',
            meterSerial3: '',
            newUsageRecord1 : '',
            newUsageRecord2 : '',
            newUsageRecord3 : '',
            newReactiveRecord1 : '',
            newReactiveRecord2 : '',
            newReactiveRecord3 : ''
        };
        this.props.dispatch(initialize("form_usage_entry",initData));
    }

    //​ close success import
    handleCloseSuccessImport(value) {
        this.setState({ 
            usage: value,
            isImportSuccess: false
        });
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                {
                    this.state.usage ?
                        this.state.isImportSuccess === false ?
                            <div>
                                {/* upload excel file */}
                                <Row>
                                    <Col md={4} lg={4}>
                                        <Panel header="Import Usage Excel File" bsStyle="info">
                                            <form name="Upload" action="#" encType="multipart/form-data" method="post">
                                                <input type="file" name="submitfile" id="submitfile" /><br/>
                                                <Button bsStyle="primary" bsSize="small" onClick={() => this.uploadExcelFile()} >
                                                    Submit
                                                </Button>
                                                {' '}
                                                <Button bsStyle="default" bsSize="small" onClick={() => this.handleBack()} >
                                                    Back
                                                </Button>
                                            </form>
                                        </Panel>
                                    </Col>
                                </Row>
                                <div id="loading" style={{display: 'none', marginTop: 20}}>
                                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                </div>
                                <Row>
                                    <Col md={12} lg={12}>
                                        <Image src="/images/simple-meter-usage.png" thumbnail />
                                    </Col>
                                </Row>
                            </div>
                            : <SuccessImport handleClose={this.handleCloseSuccessImport.bind(this)} />
                        :
                        <div>
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <Row>
                                        <Col md={6} lg={6}>
                                            <Form inline>
                                                <FormGroup controlId="formInlineName">
                                                    <ControlLabel>Customer ID</ControlLabel>
                                                    {' '}
                                                    <FormControl name ="customerId" type="text" placeholder="Customer ID"
                                                        onChange={this.getCustomerId} />
                                                </FormGroup>
                                                {' '}
                                                <Button bsStyle="primary" bsSize="small" onClick={() => this.searchCustomerID()}>Search</Button>
                                            </Form>
                                        </Col>
                                        <Col md={8} lg={8}></Col>
                                    </Row>
                                    <table className="page-permision-table">
                                        <thead>
                                        <tr>
                                            <th>Customer ID</th>
                                            <th>Customer Name</th>
                                            <th>Meter Serial</th>
                                            <th>Meter Type</th>
                                            <th>Ampere</th>
                                            <th>Previous</th>
                                            <th>Customer Type</th>
                                            <th>Pole No</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.getMetersByCustomerId.data !== undefined || this.props.getMetersByCustomerId.status === 0?
                                                this.props.getMetersByCustomerId.data.items.map((meter, index) => {
                                                 return (
                                                     <tr key={index}>
                                                         <td>{meter.customer.id}</td>
                                                         <td>{meter.customer.nameEn}</td>
                                                         <td>{meter.meterSerial}</td>
                                                         <td>{meter.meterType.meterType}</td>
                                                         <td>{meter.customer.ampere.ampere}</td>
                                                         <td>{meter.endUsageRecord}</td>
                                                         <td>{meter.customer.usageType.name}</td>
                                                         <td>{meter.customer.box.pole.serial}</td>
                                                     </tr>
                                                 )
                                            })
                                         :
                                            <tr>
                                                <td colSpan={8} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                            </tr>
                                        }
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                            <br/>
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <Panel header="Meter Usage Record" bsStyle="info">
                                        <form onSubmit={handleSubmit(this.handleAdd.bind(this))} className="wrap-full-form">
                                            <Row>
                                                <Col xs={4} sm={4} md={4} lg={4}>
                                                    <ControlLabel>Meter</ControlLabel>
                                                    <Typeahead
                                                        labelKey="serial"
                                                        options={serials}
                                                        selected={serials.filter(s => s.serial === this.state.meter1)}
                                                        placeholder="Type meter serial...."
                                                        onChange={this.filterMeterSerial.bind(this)}
                                                        clearButton
                                                    />
                                                    <div style={{marginBottom: 15}}></div>
                                                    {/*<Field name="meterSerial1" type="text" component={TextBox} label="Meter 1"/>*/}
                                                    <Field name="meterSerial2" type="text" component={TextBox} noOverwriteOnInitialize={true} label="Meter 2" disabled={true}/>
                                                    <Field name="meterSerial3" type="text" component={TextBox} noOverwriteOnInitialize={true} label="Meter 3" disabled={true}/>
                                                </Col>
                                                <Col xs={4} sm={4} md={4} lg={4}>
                                                    <ControlLabel>Usage</ControlLabel>
                                                    <Field name="newUsageRecord1" type="text" component={TextBox} label="Usage for meter 1"/>
                                                    <Field name="newUsageRecord2" type="text" component={TextBox} label="Usage for meter 2"/>
                                                    <Field name="newUsageRecord3" type="text" component={TextBox} label="Usage for meter 3"/>
                                                </Col>
                                                <Col xs={4} sm={4} md={4} lg={4}>
                                                    <ControlLabel>Reactive</ControlLabel>
                                                    <Field name="newReactiveRecord1" type="text" component={TextBox} label="Reative for meter 1"/>
                                                    <Field name="newReactiveRecord2" type="text" component={TextBox} label="Reative for meter 2"/>
                                                    <Field name="newReactiveRecord3" type="text" component={TextBox} label="Reative for meter 3"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col xs={8} sm={8} md={8} lg={8}></Col>
                                                <Col xs={4} sm={4} md={4} lg={4}>
                                                    <strong>Date <span className="label-require">*</span></strong>
                                                    <Field name="recordDate" component={DateTimePicker} placeholder="Date Usage"
                                                           defaultDate={this.state.recordDate}
                                                           handleChange={this.handleDateUsage.bind(this)}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={7} lg={7}></Col>
                                                <Col md={3} lg={3}>
                                                    <Button bsStyle="primary" onClick={() => this.clearText()} >Clear</Button>{' '}
                                                    <Button bsStyle="primary" onClick={() => this.importUsage()} >Import Usage</Button>
                                                </Col>
                                                <Col md={2} lg={2} className="pull-right">
                                                    <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                                </Col>
                                            </Row>
                                        </form>
                                    </Panel>
                                </Col>
                            </Row>
                            {/* list customer usages to verify */}
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <Panel header="SAVE" bsStyle="info">
                                        <table className="page-permision-table">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Customer ID</th>
                                                <th>Customer Name</th>
                                                <th>Meter Serial</th>
                                                <th>New Usage</th>
                                                <th>Date</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            {
                                                this.state.arrUsages !== undefined ?
                                                    this.state.arrUsages.map((meter, index) => {
                                                        return (
                                                            <tr key={index}>
                                                                <td>{index+1}</td>
                                                                <td>{meter.customerId}</td>
                                                                <td>{meter.customerName}</td>
                                                                <td>{meter.meterSerials}</td>
                                                                <td>{meter.newUsageRecords}</td>
                                                                <td>{meter.recordDate}</td>
                                                            </tr>
                                                        )
                                                    })
                                                    :
                                                    <tr>
                                                        <td colSpan={7} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                                    </tr>
                                            }
                                            </tbody>
                                        </table>
                                        <br/>
                                        {/* error usage error */}
                                        {this.state.error ? <strong><span className="label-require">The usage of customer might has problem because the usage is very decrease from last month.</span></strong> : null }
                                        <div className="pull-right">
                                            <Button bsStyle="primary" bsSize="small" onClick={() => this.saveUsage()} >SAVE</Button>
                                        </div>
                                    </Panel>
                                </Col>
                            </Row>
                            <hr/>
                            {/* search meter serial to update */}
                            <Row>
                                <Col md={6} lg={6}>
                                    <Form id="myForm" inline>
                                        <FormGroup controlId="formInlineName">
                                            <ControlLabel>Meter Serial</ControlLabel>
                                            {' '}
                                            <FormControl name ="meterSerial" type="text" placeholder="Meter Serial"
                                                         onChange={this.getMeterSerial} />
                                        </FormGroup>
                                        {' '}
                                        <Button bsStyle="primary" bsSize="small"
                                                onClick={() => this.searchMeterSerial()}>Search</Button>
                                    </Form>
                                </Col>
                                <Col md={8} lg={8}></Col>
                            </Row>
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <table className="page-permision-table">
                                        <thead>
                                        <tr>
                                            <th>Meter Usage ID</th>
                                            <th>Meter Serial</th>
                                            <th>Previous</th>
                                            <th>New Usage</th>
                                            <th>Coefficients</th>
                                            <th>Usage</th>
                                            <th>Record Date</th>
                                            <th>Edit</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {
                                            this.props.getMeterUsageBySerial.status === 200 ?
                                                this.props.getMeterUsageBySerial.data.items.map((meter, index) => {
                                                    return (
                                                        <tr key={index}>
                                                            <td>{meter.id}</td>
                                                            <td>{meter.meterSerial}</td>
                                                            <td>{meter.lastUsageRecord}</td>
                                                            <td>{meter.newUsageRecord}</td>
                                                            <td>{meter.coefficient}</td>
                                                            <td>{meter.activePower}</td>
                                                            <td>{moment(meter.recordDate).format("DD-MM-YYYY")}</td>
                                                            <td>
                                                                <a className='btn btn-info btn-xs' onClick={() => this.updateItem(meter)}>
                                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                                :
                                                <tr>
                                                    <td colSpan={8} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                                </tr>
                                        }
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </div>
                }
            </div>
        )
    }

}

UsageEntry = reduxForm({
    form: 'form_usage_entry',
    validate: (values) => {
        let regex_serial = /^[0-9a-zA-Z]{1,15}$/;
        let regex_number = /^[0-9]{1,8}$/;

        const errors = {};
        if (values.recordDate === undefined) {
            errors.recordDate = 'Date is required!';
        }
        if(!regex_serial.test(values.meterSerial1) || values.meterSerial1 === undefined){
            errors.meterSerial1 = "Meter 1 is required!";
        }
        if(!regex_number.test(values.newUsageRecord1) || values.newUsageRecord1 === undefined){
            errors.newUsageRecord1 = "Usage 1 is required!";
        }
        if(values.meterSerial2 !== undefined || values.meterSerial2 === ""){
            if(values.newUsageRecord2 === undefined){
                errors.newUsageRecord2 = "Meter Usage 2 is required!"
            }
        }
        if(values.meterSerial3 !== undefined || values.meterSerial3 === ""){
            if(values.newUsageRecord3 === undefined){
                errors.newUsageRecord3 = "Meter Usage 3 is required!"
            }
        }

        return errors
    }
})(UsageEntry);

function mapStateToProps(state) {
    console.log("DATA : ",state.usageEntry.importExcelMeterUsageRecord);
    //console.log("DATA : ",state.usageEntry.getAllCustomerIdsAndMeterSerials);
    return {
        addMeterUsageRecord: state.usageEntry.addMeterUsageRecord,
        getMetersByCustomerId: state.usageEntry.getMetersByCustomerId,
        getMeterUsageBySerial: state.usageEntry.getMeterUsageBySerial,
        updateMeterUsageById: state.usageEntry.updateMeterUsageById,
        importExcelMeterUsageRecord: state.usageEntry.importExcelMeterUsageRecord,
        verifyMeterUsageIndividual: state.usageEntry.verifyMeterUsageIndividual,
        getAllCustomerIdsAndMeterSerials: state.usageEntry.getAllCustomerIdsAndMeterSerials,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        addMeterUsageRecordAction,
        getMetersByCustomerIdAction,
        getMeterUsageByMeterSerial,
        updateMeterUsageByIdAction,
        importExcelMeterUsageRecordAction,
        verifyMeterUsageIndividualAction,
        getAllCustomerIdsAndMeterSerialAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(UsageEntry);