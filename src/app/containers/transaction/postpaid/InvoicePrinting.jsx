import React from 'react';
import { Row, Col, Button, Panel } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import {connect} from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import SelectBox from '../../../components/forms/SelectBox';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllIssueInvoicesAction, generateAllInvoiceAction } from '../../../actions/transaction/invoicePrinting';
import moment from 'moment';
import ReactLoading from 'react-loading';

function onAfterTableComplete() {
    //console.log('Table render complete.');
}
function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
let invoices = [];
let filter = {
    startDate : '',
    endDate: '',
    //invoiceType: 0,
    transformer: '',
    customerId: 0
};

let invoiceType = '';
let startDate = '';
let endDate = '';
class InvoicePrinting extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startDate: null,
            endDate: null
            // invoiceTypes : [{id:1, name:'Pre-paid'}, {id:2, name:'Post-paid'},
            //                 {id:3, name:'Pre-paid and Post-paid'},{id:4, name:'Return Kwh <= 10'}]
        };
        this.handleClear = this.handleClear.bind(this);
        this.handleGenerate = this.handleGenerate.bind(this);
    }

    componentWillMount(){
        //this.props.getAllIssueInvoicesAction(filter);
    }
    componentWillUnmount() {
        this.handleClear();
    }

    componentWillReceiveProps(data){
        /* get all invoices to be generate */
        if(data.getAllIssueInvoices.status === 200){
            console.log("data",data.getAllIssueInvoices.data.items);
            invoices = [];
            document.getElementById('loading').style.display = 'none';
            let items = data.getAllIssueInvoices.data.items;
            items.forEach((element) => {
                invoices.push({
                    "startBillingDate": moment(element.startBillingDate).format("DD-MM-YYYY"),
                    "amountUSD": element.invoiceAmountUSD,
                    "amountRiel": element.invoiceAmountRiel,
                    "customerId": element.customerId,
                    "transformer": element.transformerSerial,
                    "pole": element.poleSerial,
                    "invoiceId": element.invoiceId,
                    "endBillingDate": moment(element.endBillingDate).format("DD-MM-YYYY"),
                    "unpaidAmountUSD": element.unpaidAmountUSD,
                    "invoiceNo": element.invoiceNo,
                    "issueDate": moment(element.issueDate).format("DD-MM-YYYY"),
                    "customerName": element.customerName,
                    "unpaidAmountRiel": element.unpaidAmountRiel
                });
            });
        }
        if(data.getAllIssueInvoices.status === 401 || data.getAllIssueInvoices.status === 404 || data.getAllIssueInvoices.status === 500){
            document.getElementById('loading').style.display = 'none';
            invoices = [];
        }
        /* generate invoice as PDF */
        if(data.generateAllInvoices.status === 200) {
            if(data.generateAllInvoices.data.status === 200) {

                this.props.getAllIssueInvoicesAction({
                                                        startDate : '',
                                                        endDate: '',
                                                        transformer: '',
                                                        customerId: 0
                                                    });
                                                    
                if(invoices.length > 0) {
                    alert("អ្នកបានធ្វើការបញ្ចូលនូវ ការបង្កើតវិក្កយបត្រដោយស្វ័យប្រវត្តរួចរាល់។ វានឹងជូនដំណឹងទៅដល់អ្នកពេលដែលដំណើរការបានបញ្ចប់ តាមរយៈអ៊ីម៉ែលរបស់អ្នក។");
                }
                this.props.dispatch(initialize('form_invoice_printing', {}));
                data.generateAllInvoices.status = 0;
            } else {
                alert("ម៉ាស៊ីនមានក៉ហុស!");
                data.generateAllInvoices.status = 0;
            }
        }
    }
    /* filter to search */
    handleStartDate(date){
        this.setState({startDate: date});
        startDate = moment(date).format("YYYY-MM-DD");
    }
    handleEndDate(date){
        this.setState({endDate: date});
        endDate = moment(date).format("YYYY-MM-DD");
    }

    // selectInvoiceType(event){
    //     console.log("invoice type", event.target.value);
    //     invoiceType = Number(event.target.value);
    // }
    handleSearch(values){
        document.getElementById('loading').style.display = 'block';
        filter.startDate = startDate;
        filter.endDate = endDate;
        // if(values.invoiceType !== undefined){
        //     filter.invoiceType = Number(invoiceType);
        // }else {
        //     filter.invoiceType = 0;
        // }
        if(values.transformer !== undefined){
            filter.transformer = values.transformer;
        }else {
            filter.transformer = "";
        }
        if(values.customerId !== undefined){
            filter.customerId = Number(values.customerId);
        }else{
            filter.customerId = 0;
        }
        this.props.getAllIssueInvoicesAction(filter);
    }
    /* generate invoice to be PDF file */
    handleGenerate(){
        let arrInvoiceNo = [];
        if(invoices.length <= 0){
            alert("It has no invoice to generate");
        } else{
            for(let i=0; i< invoices.length;i++){
                arrInvoiceNo.push(invoices[i].invoiceNo);
            }
        }
        console.log("data generate : ",arrInvoiceNo);
        this.props.generateAllInvoiceAction(arrInvoiceNo);
    }
    /* helper functions */
    handleClear(){
        // location.href = "/app/transactions/postpaid/invoice-printing";
        this.setState({
            startDate: null,
            endDate: null
        })
        if(this.props.getAllIssueInvoices.status === 200)
            this.props.getAllIssueInvoices.data.items = [];

        this.props.dispatch(initialize("form_invoice_printing", {}));
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <Panel header="Invoices Printing" bsStyle="info">
                                <form onSubmit={handleSubmit(this.handleSearch.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Issue Start Date <span className="label-require">*</span></strong>
                                            <Field name="startDate" component={DateTimePicker} placeholder="Issue start date"
                                                   defaultDate={this.state.startDate}
                                                   handleChange={this.handleStartDate.bind(this)}/>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Issue End Date <span className="label-require">*</span></strong>
                                            <Field name="endDate" component={DateTimePicker} placeholder="Issue end date"
                                                   defaultDate={this.state.endDate}
                                                   handleChange={this.handleEndDate.bind(this)}/>
                                        </Col>
                                        {/*<Col md={3} lg={3} className="label-name">
                                            <strong>Invoice Type</strong>
                                            <Field name="invoiceType" type="select" component={SelectBox}
                                                   placeholder="Select Invoice Type"
                                                   onChange = {this.selectInvoiceType.bind(this)}
                                                   values={this.state.invoiceTypes}
                                                   sortBy="name" icon=""/>
                                        </Col>*/}
                                        <Col md={2} lg={2} className="label-name">
                                            <strong>Transformer</strong>
                                            <Field name="transformer" type="text" component={TextBox} label="Transformer serial"/>
                                        </Col>
                                        <Col md={2} lg={2} className="label-name">
                                            <strong>Customer ID</strong>
                                            <Field name="customerId" type="text" component={TextBox} label="Customer ID"/>
                                        </Col>
                                        <Col md={2} lg={2} className="pull-right">
                                            <br/>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                        </Col>
                                    </Row>
                                </form>
                                <Row>
                                    <Col md={12} lg={12}>
                                        <div className="pull-right">
                                        <Button bsStyle="primary" bsSize="small"
                                                onClick={() => this.handleGenerate()}>Generate Invoices To PDF</Button>{' '}
                                        <Button bsSize="small" onClick={() => this.handleClear()}>Refresh</Button>
                                        </div>
                                    </Col>
                                </Row>
                            </Panel>
                        </Col>
                    </Row>
                </div>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                                <BootstrapTable
                                    data={invoices} //list of records
                                    trClassName={trClassNameFormat}  //apply style on cell
                                    bodyStyle={{ fontSize:'10pt'}}
                                    options={{
                                        sizePerPage: 20,
                                        sizePerPageList: [ 10, 20, 30, 40 , 50, invoices.length ],
                                        paginationShowsTotal: true,
                                        sortName: 'customerName',  // default sort column name
                                        sortOrder: 'asc',  // default sort order
                                        afterTableComplete: onAfterTableComplete // A hook for after table render complete.
                                    }}
                                    exportCSV
                                    hover // apply hover style on row
                                    pagination
                                >
                                    <TableHeaderColumn dataField='invoiceNo' width="100px" dataSort isKey autoValue>Invoice No</TableHeaderColumn>
                                    <TableHeaderColumn dataField='customerId' width="100px" dataSort >Customer ID</TableHeaderColumn>
                                    <TableHeaderColumn dataField='customerName' width="100px" dataSort>Name</TableHeaderColumn>
                                    <TableHeaderColumn dataField='issueDate' width="100px" dataSort>Issue Date</TableHeaderColumn>
                                    <TableHeaderColumn dataField='startBillingDate' width="150px" dataSort>Start Billing Date</TableHeaderColumn>
                                    <TableHeaderColumn dataField='endBillingDate' width="150px" dataSort>End Billing Date</TableHeaderColumn>
                                    <TableHeaderColumn dataField='amountRiel' width="150px" dataSort>Invoice Amount RIEL</TableHeaderColumn>
                                    <TableHeaderColumn dataField='amountUSD' width="150px" dataSort>Invoice Amount USD</TableHeaderColumn>
                                    <TableHeaderColumn dataField='unpaidAmountRiel' width="150px" dataSort>Unpaid Amount RIEL</TableHeaderColumn>
                                    <TableHeaderColumn dataField='unpaidAmountUSD' width="150px" dataSort>Unpaid Amount USD</TableHeaderColumn>
                                    <TableHeaderColumn dataField='transformer' width="100px" dataSort>Transformer</TableHeaderColumn>
                                    <TableHeaderColumn dataField='pole' width="100px" dataSort>Pole</TableHeaderColumn>
                                </BootstrapTable>
                        </Col>
                    </Row>
                </div>

            </div>
        )
    }

}

InvoicePrinting = reduxForm({
    form: 'form_invoice_printing',
    validate: (values) => {
        const errors = {};
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if(values.endDate === undefined){
            errors.endDate = "End Date is required!";
        }
        return errors
    }
})(InvoicePrinting);

function mapStateToProps(state) {
    //console.log("DATA : ",state.invoicesPrinting.state.generateAllInvoices);
    return {
        getAllIssueInvoices: state.invoicesPrinting.getAllIssueInvoices,
        generateAllInvoices: state.invoicesPrinting.generateAllInvoices
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllIssueInvoicesAction, generateAllInvoiceAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InvoicePrinting);