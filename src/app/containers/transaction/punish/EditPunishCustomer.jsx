import React from 'react';
import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getPunishCustomerByIdAction, updateCustomerPunishAction } from '../../../actions/transaction/punish/punishCustomer';
import moment from 'moment';

class EditPunishCustomer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            cutDate: null,
            reconnectedDate: null
        }
    }

    componentWillMount(){
        this.props.getPunishCustomerByIdAction(this.props.params.id);
    }

    componentWillReceiveProps(data){
        if(data.getCustomerPunishById.status === 200){
            let item = data.getCustomerPunishById.data.items[0];
            let punish = {
                "customerId": item.customerId,
                "cutReason": item.cutReason,
                "cutDate": item.cutDate,
                "cutBy": item.cutBy,
                "connectBy": item.connectBy,
                "reconnectDate": item.reconnectDate,
                "invoiceNo": item.invoiceNo
            };
            if(punish.cutDate === null){
                this.setState({cutDate: null});
            }else {
                this.setState({cutDate:moment(punish.cutDate)});
            }
            if(punish.reconnectDate === null){
                this.setState({reconnectedDate:null});
            }else {
                this.setState({reconnectedDate:moment(punish.reconnectDate)});
            }
            this.props.dispatch(initialize("form_edit_punish_customer",punish));
            data.getCustomerPunishById.status = 0;
        }

        if(data.updateCustomerPunish.status === 200){
            alert("Successfully updated new punish customer.");
            this.setState({cutDate: null});
            this.setState({reconnectedDate: null});
            this.props.dispatch(initialize("form_edit_punish_customer",null));
            data.updateCustomerPunish.status = 0;
            browserHistory.push("/app/customers/punish-customers");
        }
        if(data.updateCustomerPunish.status === 401 || data.updateCustomerPunish.status === 404 || data.updateCustomerPunish.status === 500){
            alert("Fail with add punish customer!");
        }
    }

    handleCutDate(date){
        this.setState({cutDate: date});
    }

    handleReconnectedDate(date){
        this.setState({reconnectedDate: date});
    }

    handleCancel(){
        browserHistory.push("/app/customers/punish-customers");
    }

    handleSubmit(values){
        let data = {
            "id" : Number(this.props.params.id),
            "cutDate": values.cutDate,
            "cutReason": values.cutReason,
            "cutBy": values.cutBy,
            "connectBy": values.connectBy,
            "reconnectDate": values.reconnectDate,
            "invoiceNo": "",
            "status": 1
        };
        if(values.invoiceNo === undefined || values.invoiceNo === "") {
            data.invoiceNo = "";
            alert("Invoice No is required!");
        }else {
            data.invoiceNo = values.invoiceNo.trim();
            this.props.updateCustomerPunishAction(data);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Edit Punish Customer</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Customer ID<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="customerId" type="text" component={TextBox} label="Customer ID" icon="" disabled/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Invoice No<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="invoiceNo" type="text" component={TextBox} label="Invoice No" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Cut Date</strong>
                                                </Col>
                                                <Col md={8} lg={8} className="label-name">
                                                    <Field name="cutDate" component={DateTimePicker} placeholder="Cut Date"
                                                           defaultDate={this.state.cutDate}
                                                           handleChange = {this.handleCutDate.bind(this)}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Cut Reason</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="cutReason" type="text" component={TextArea} label="Cut reason" />
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Cut By</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="cutBy" type="text" component={TextBox} label="Cut By" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Reconnected Date</strong>
                                                </Col>
                                                <Col md={8} lg={8} className="label-name">
                                                    <Field name="reconnectDate" component={DateTimePicker} placeholder="Reconnected Date"
                                                           defaultDate={this.state.reconnectedDate}
                                                           handleChange = {this.handleReconnectedDate.bind(this)}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Connected By</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="connectBy" type="text" component={TextBox} label="Connected By" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="pull-right">
                                                    <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="UPDATE" />
                                                </Col>
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button onClick={() => this.handleCancel()}>Cancel</Button>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

EditPunishCustomer = reduxForm({
    form: 'form_edit_punish_customer',
    validate: (values) => {
        const errors = {};
        if (values.customerId === undefined) {
            errors.customerId = 'Customer ID is required!';
        }
        if (values.punishRateId === undefined) {
            errors.punishRateId = 'Punish Type is required!';
        }
        return errors
    }
})(EditPunishCustomer);

function mapStateToProps(state) {
    //console.log("DATA : ",state.punishCustomers.getCustomerPunishById);
    return {
        getCustomerPunishById: state.punishCustomers.getCustomerPunishById,
        updateCustomerPunish: state.punishCustomers.updateCustomerPunish
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getPunishCustomerByIdAction, updateCustomerPunishAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditPunishCustomer);