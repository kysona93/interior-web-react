import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllInvoicesPunishAction, addCustomerPunishAction } from '../../../actions/transaction/punish/punishCustomer';
import moment from 'moment';
import ReactLoading from 'react-loading';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

let allInvoices = [];
let saveData = [];
class ListInvoicesToPunish extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            cutDate : null,
            saveCheckAll: false
        };
        this.handleCheckboxSaveAll = this.handleCheckboxSaveAll.bind(this);
        this.handleCheckboxSave = this.handleCheckboxSave.bind(this);
    }

    componentWillMount(){
        this.props.getAllInvoicesPunishAction(moment(new Date()).format("YYYY-MM-DD"));
    }

    componentWillReceiveProps(data) {
        if (data.getAllInvoicesPunish.status === 200) {
            allInvoices = data.getAllInvoicesPunish.data.items;
        }

        if(data.addCustomerPunish.status === 200){
            alert("Successfully added punish to customers.");
            data.addCustomerPunish.status = 0;
            location.href = "/app/transactions/postpaid/list-invoices-to-punish";
        }
        if(data.addCustomerPunish.status === 500){
            alert("Fail with add punish to customers!");
            data.addCustomerPunish.status = 0;
        }
    }

    handleCheckboxSaveAll(){
        this.setState({saveCheckAll: !this.state.saveCheckAll});
        if(this.state.saveCheckAll === false) {
            // check all
            saveData = [];
            allInvoices.forEach((element) => {
                saveData.push({
                    "customerId": element.customerId,
                    "invoiceId": element.invoiceId,
                    "cutDate": null
                });
            });
            console.log("check all: ", saveData);
        } else {
            // uncheck all
            saveData = [];
            console.log("uncheck all: ", saveData);
        }
    }

    handleCheckboxSave(event){
        console.log("invoice id: ",event.target.value);
        let index = saveData.findIndex(element => element.invoiceId === Number(event.target.value));
        if(index === -1){
            // check customer
            for(let i=0; i< allInvoices.length; i++){
                if(allInvoices[i].invoiceId === Number(event.target.value)){
                    saveData.push({
                        "customerId": allInvoices[i].customerId,
                        "invoiceId": allInvoices[i].invoiceId,
                        "cutDate": null
                    });
                    break;
                }
            }
            console.log("check customer :", saveData);
        }else{
            // uncheck customer
            saveData.splice(index,1);
            this.setState({saveCheckAll: false});
            console.log("uncheck customer :", saveData);
        }
    }

    getCutDate(date){
        this.setState({cutDate: date});
    }

    handleSubmit(values){
        document.getElementById('loading').style.display = 'block';
        for(let i=0;i < saveData.length; i++){
            saveData[i].cutDate = values.cutDate;
        }
        this.props.addCustomerPunishAction(saveData);
    }

    handleRefresh(){
        location.href = "/app/transactions/postpaid/list-invoices-to-punish";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#scheck").click(function () {
                $(".scheck").prop('checked', $(this).prop('checked'));
            });
        });
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6} className="pull-right">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">List Punish Invoices</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={7} lg={7} className="label-name">
                                            <strong>Cut Date</strong>
                                            <Field name="cutDate" component={DateTimePicker} placeholder="Cut Date"
                                                   defaultDate={this.state.cutDate}
                                                   handleChange = {this.getCutDate.bind(this)}/>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <br/>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="SAVE" />
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <br/>
                                            <Button onClick={() => this.handleRefresh()} className="pull-right">Refresh</Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div id="loading" style={{display: 'none', marginTop: 20}}>
                            <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                        </div>
                        <div className="custyle">
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="table-to-xls"
                                filename="list-cut-connections"
                                sheet="tablexls"
                                buttonText="Export to XLS"/>
                            <table className="table-striped page-permision-table" id="table-to-xls">
                                <thead hidden>
                                    <tr>
                                        <th colSpan={10} style={{fontSize: '16pt'}}>List Customers to cut connection</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th style={{width: '50px'}}>
                                            Save {' '}
                                            <input
                                                type="checkbox"
                                                checked={this.state.saveCheckAll}
                                                onChange={this.handleCheckboxSaveAll}
                                                id="scheck"
                                            />
                                        </th>
                                        <th style={{width: '30px'}}>No</th>
                                        <th style={{width: '80px'}}>Customer ID</th>
                                        <th style={{width: '80px'}}>Name</th>
                                        <th style={{width: '80px'}}>Phone</th>
                                        <th style={{width: '80px'}}>Invoice No</th>
                                        <th style={{width: '80px'}}>Area</th>
                                        <th style={{width: '80px'}}>Transformer</th>
                                        <th style={{width: '80px'}}>Pole</th>
                                        <th style={{width: '80px'}}>Box</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    this.props.getAllInvoicesPunish.status === 200 ?
                                        this.props.getAllInvoicesPunish.data.items.map((invoice, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>
                                                        <input
                                                            type="checkbox"
                                                            value={invoice.invoiceId}
                                                            onChange={this.handleCheckboxSave}
                                                            className="scheck"
                                                            id={"s" + invoice.invoiceId}
                                                        />
                                                    </td>
                                                    <td>{index + 1}</td>
                                                    <td>{invoice.customerId}</td>
                                                    <td>{invoice.name}</td>
                                                    <td>{invoice.phone}</td>
                                                    <td>{invoice.invoiceNo}</td>
                                                    <td>{invoice.area}</td>
                                                    <td>{invoice.transformer}</td>
                                                    <td>{invoice.pole}</td>
                                                    <td>{invoice.box}</td>
                                                </tr>
                                            )
                                        })
                                    :
                                    <tr>
                                        <td colSpan={10}><h3>RESULT NOT FOUND</h3></td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

ListInvoicesToPunish = reduxForm({
    form: 'form_list_punish_invoices',
    validate: (values) => {
        const errors = {};
        if (values.cutDate === undefined) {
            errors.cutDate = 'Cut Date is required!';
        }
        return errors
    }
})(ListInvoicesToPunish);

function mapStateToProps(state) {
    //console.log("DATA : ",state.punishCustomers.getAllInvoicesPunish)
    return {
        getAllInvoicesPunish: state.punishCustomers.getAllInvoicesPunish,
        addCustomerPunish: state.punishCustomers.addCustomerPunish

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllInvoicesPunishAction, addCustomerPunishAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListInvoicesToPunish);