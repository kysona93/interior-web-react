import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllCustomersPunishAction, updateCustomerPunishAction, deleteCustomerPunishAction } from '../../../actions/transaction/punish/punishCustomer';
import moment from 'moment';

let criteria = {
    invoiceNo : '',
    name: '',
    customerId: 0
};
let money = 0;
class ListPunishCustomer extends React.Component {
    constructor(props){
        super(props);
        this.deleteItem = this.deleteItem.bind(this);
        this.payConnection = this.payConnection.bind(this);
        this.getAmount = this.getAmount.bind(this);
    }

    componentWillMount(){
        this.props.getAllCustomersPunishAction(criteria);
    }

    componentWillReceiveProps(data){
        if(data.deleteCustomerPunish.status === 200){
            alert("Successfully deleted punish customer.");
            this.props.getAllCustomersPunishAction(criteria);
            data.deleteCustomerPunish.status = 0;
        }
        if(data.deleteCustomerPunish.status === 500){
            alert("Fail with delete punish customer.");
            data.deleteCustomerPunish.status = 0;
        }

        if(data.updateCustomerPunish.status === 200){
            alert("Successfully connect customer.");
            this.props.getAllCustomersPunishAction(criteria);
            data.updateCustomerPunish.status = 0;
        }
        if(data.updateCustomerPunish.status === 500){
            alert("Fail with connect customer!");
            data.updateCustomerPunish.status = 0;
        }
    }

    getAmount(event){
        money = event.target.value;
    }
    payConnection(punish){
        let data = {
            "amount": money,
            "id": punish.id,
            "customerId" : punish.customerId
        };
        this.props.updateCustomerPunishAction(data);
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this punish customer?") === true){
            this.props.deleteCustomerPunishAction(id);
        }
    }

    handleRefresh(){
        location.href = "/app/transactions/postpaid/list-punish-customers";
    }

    handleSubmit(values){
        if(values.invoiceNo !== undefined) criteria.invoiceNo = values.invoiceNo;
        else criteria.invoiceNo = "";
        if(values.customerName !== undefined) criteria.name = values.customerName;
        else criteria.name = "";
        if(values.customerId !== undefined) criteria.customerId = Number(values.customerId);
        else criteria.customerId = 0;
        //console.log("data : ",criteria);
        this.props.getAllCustomersPunishAction(criteria);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={10} sm={10} md={10} lg={10} className="pull-right">
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">List Punish Customer</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Invoice No</strong>
                                            <Field name="invoiceNo" type="text" component={TextBox} label="Invoice No" icon=""/>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Customer ID</strong>
                                            <Field name="customerId" type="text" component={TextBox} label="Customer ID" icon=""/>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Name</strong>
                                            <Field name="customerName" type="text" component={TextBox} label="Customer Name" icon=""/>
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <br/>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="SEARCH" />
                                        </Col>
                                        <Col md={1} lg={1}>
                                            <br/>
                                            <Button onClick={() => this.handleRefresh()} className="pull-right">Refresh</Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div className="custyle">
                            <table className="table-striped page-permision-table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Customer ID</th>
                                    <th>Name</th>
                                    <th>Invoice No</th>
                                    <th>Cut Date</th>
                                    <th>Cut Reason</th>
                                    <th>Area</th>
                                    <th>Pole</th>
                                    <th>Box</th>
                                    <th>Pay Money</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.props.getAllCustomersPunish.status === 200 ?
                                    <tbody>
                                    { this.props.getAllCustomersPunish.data.items.map((customer,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{index+1}</td>
                                                <td>{customer.customerId}</td>
                                                <td>{customer.customerName}</td>
                                                <td>{customer.invoiceNo}</td>
                                                <td>{customer.cutDate === null ? "": moment(customer.cutDate).format("DD-MM-YYYY")}</td>
                                                <td>{customer.cutReason}</td>
                                                <td>{customer.areaName}</td>
                                                <td>{customer.pole}</td>
                                                <td>{customer.box}</td>
                                                <td>
                                                    <Field name={"amount"+customer.id} type="text" component={TextBox}
                                                           label="Enter Amount" icon="" onChange={this.getAmount}
                                                    />
                                                </td>
                                                <td className="text-center">
                                                    <a onClick={()=> this.payConnection(customer)} href="#" className="btn btn-info btn-xs">
                                                        <span className="glyphicon glyphicon-ok"></span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(customer.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-trash"></span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={11}><h3>RESULT NOT FOUND</h3></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

ListPunishCustomer = reduxForm({
    form: 'form_list_punish_customers',
    validate: (values) => {
        const errors = {};
        if (values.name === undefined) {
            errors.name = 'Name is required!';
        }
        return errors
    }
})(ListPunishCustomer);

function mapStateToProps(state) {
    //console.log("DATA : ",state.punishCustomers.getAllCustomersPunish)
    return {
        getAllCustomersPunish: state.punishCustomers.getAllCustomersPunish,
        deleteCustomerPunish: state.punishCustomers.deleteCustomerPunish,
        updateCustomerPunish: state.punishCustomers.updateCustomerPunish
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllCustomersPunishAction, updateCustomerPunishAction, deleteCustomerPunishAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListPunishCustomer);