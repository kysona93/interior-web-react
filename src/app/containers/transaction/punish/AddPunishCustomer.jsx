import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { addCustomerPunishAction } from '../../../actions/transaction/punish/punishCustomer';
import moment from 'moment';

class AddPunishCustomer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            cutDate: null
        }
    }

    componentWillReceiveProps(data){
        if(data.addCustomerPunish.status === 200){
            alert("Successfully added new punish customer.");
            this.setState({cutDate: null});
            this.props.dispatch(initialize("form_add_punish_customer",null));
            data.addCustomerPunish.status = 0;
        }
        if(data.addCustomerPunish.status === 401 || data.addCustomerPunish.status === 404 || data.addCustomerPunish.status === 500){
            alert("Fail with add punish customer!");
            data.addCustomerPunish.status = 0;
        }
    }

    handleCutDate(date){
        this.setState({cutDate: date});
    }

    handleSubmit(values){
        let data = {
            "customerId": Number(values.customerId),
            "invoiceNo": values.invoiceNo.trim(),
            "cutDate": moment(values.cutDate).format("YYYY-MM-DD"),
            "cutReason": values.cutReason,
            "cutBy": values.cutBy,
            "status": 1
        };
        this.props.addCustomerPunishAction(data);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add Punish Customer</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Customer ID<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="customerId" type="text" component={TextBox} label="Customer ID" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Invoice No<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="invoiceNo" type="text" component={TextBox} label="Invoice No" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Cut Date</strong>
                                        </Col>
                                        <Col md={8} lg={8} className="label-name">
                                            <Field name="cutDate" component={DateTimePicker} placeholder="Cut Date"
                                                   defaultDate={this.state.cutDate}
                                                   handleChange = {this.handleCutDate.bind(this)}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Cut Reason</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="cutReason" type="text" component={TextArea} label="Cut reason" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Cut By</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="cutBy" type="text" component={TextBox} label="Cut By" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="SAVE" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

AddPunishCustomer = reduxForm({
    form: 'form_add_punish_customer',
    validate: (values) => {
        const errors = {};
        if (values.customerId === undefined) {
            errors.customerId = 'Customer ID is required!';
        }
        if (values.invoiceNo === undefined) {
            errors.invoiceNo = 'Invoice No is required!';
        }
        return errors
    }
})(AddPunishCustomer);

function mapStateToProps(state) {
    return {
        addCustomerPunish: state.punishCustomers.addCustomerPunish
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addCustomerPunishAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddPunishCustomer);