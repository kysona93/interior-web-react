import React from 'react';
import { Link } from 'react-router';
import { Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { getUsersStatusAction } from './../../actions/authentication';
import './../menu/index.css';
import { canUpdate } from './../../../auth';

class ListInactiveUsers extends React.Component {
    constructor(props){
        super(props)
    }

    componentWillMount(){
        this.props.getUsersStatusAction(0);
    }

    render(){
        return(
            <div>
                <Row>
                    <Col xs={12} sm={12} md={12}>
                        <div className="">
                            <table className="list-smaller-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Khmer Name</th>
                                    <th>English Name</th>
                                    <th>Username</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.getUsersStatus.status == 200 ?
                                    this.props.getUsersStatus.data.items.list.map((user, index) => {
                                        
                                        return (
                                            <tr key={index}>
                                                <td>{user.id}</td>
                                                <td>{user.employee.nameKh}</td>
                                                <td>{user.employee.nameEn}</td>
                                                <td>{user.username }</td>
                                                <td>{user.employee.phone }</td>
                                                <td>{user.email }</td>
                                                <td>{user.employee.address }</td>
                                                <td>{user.employee.description}</td>
                                                <td className="text-center">
                                                {
                                                    canUpdate == true ? 
                                                        <Link to={`/app/security/users/edit/${user.id}`} title="Edit" className='btn btn-info btn-xs'>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </Link>
                                                    : null
                                                }
                                                </td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={9} className="text-align-center">RESULT NOT FOUND</td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return ({
        getUsersStatus: state.userAuthentication.getUsersStatus
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getUsersStatusAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListInactiveUsers);