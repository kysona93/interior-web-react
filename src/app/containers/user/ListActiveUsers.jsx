import React from 'react';
import { Link } from 'react-router';
import { Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { getUsersStatusAction, deleteUserAction } from './../../actions/authentication';
import './../menu/index.css';
import { canUpdate } from './../../../auth';

class ListActiveUsers extends React.Component {
    constructor(props){
        super(props);
        this.disableUser = this.disableUser.bind(this);
    }

    componentWillMount(){
        this.props.getUsersStatusAction(1);
    }

    componentWillReceiveProps(data){
        if(data.disabledUser.status === 200){
            alert("Successfully disabled user.");
            this.props.getUsersStatusAction(1);
            return data.disabledUser.status = 0;
        }
        if(data.disabledUser.status === 500 || data.disabledUser.status === 400){
            alert("Fail with disable user!");
            return data.disabledUser.status = 0;
        }
    }

    disableUser(id){
        if(confirm("Are you sure to disable this user?") == true){
            this.props.deleteUserAction(id);
        }
    }

    render(){
        return(
            <div>
                <Row>
                    <Col xs={12} sm={12} md={12}>
                        <div className="">
                            <table className="list-smaller-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Khmer Name</th>
                                    <th>English Name</th>
                                    <th>Username</th>
                                    <th>Gender</th>
                                    <th>Job Title</th>
                                    <th>Location</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Address</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.getUsersStatus.status == 200 ?
                                    this.props.getUsersStatus.data.items.list.map((user, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{user.id}</td>
                                                <td>{user.employee.nameKh}</td>
                                                <td>{user.employee.nameEn}</td>
                                                <td>{user.username }</td>
                                                <td>{user.employee.gender}</td>
                                                <td>{user.jobTitle.jobTitle}</td>
                                                <td>{user.paymentLocation.location}</td>
                                                <td>{user.employee.phone }</td>
                                                <td>{user.email }</td>
                                                <td>{user.employee.address }</td>
                                                <td>{user.employee.description}</td>
                                                <td className="text-center">
                                                {
                                                    canUpdate == true ? 
                                                        <span>
                                                            <Link to={`/app/security/users/edit/${user.id}`} title="Edit" className='btn btn-info btn-xs'>
                                                                <span className="glyphicon glyphicon-edit"></span>
                                                            </Link> &nbsp;
                                                            <Link to="" title="Disable" className='btn btn-danger btn-xs' onClick={() => this.disableUser(user.id)}>
                                                                <span className="glyphicon glyphicon-ban-circle"></span>
                                                            </Link>
                                                        </span>
                                                    : null
                                                }
                                                </td>
                                            </tr>
                                        )
                                    })
                                    : <tr>
                                        <td colSpan={12} className="text-align-center">RESULT NOT FOUND</td>
                                    </tr> }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return ({
        getUsersStatus: state.userAuthentication.getUsersStatus,
        disabledUser: state.userAuthentication.deleteUser
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getUsersStatusAction, deleteUserAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListActiveUsers);