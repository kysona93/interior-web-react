import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Tabs, Tab } from 'react-bootstrap';
import './style.css';

class UserDashboard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            key: 1
        }
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentWillMount() {
        let currentUrl = window.location.pathname;
        if(currentUrl == "/app/security/users/inactive") {
            this.setState({ key: 2 })
        } else if(currentUrl == "/app/security/users/add") {
            this.setState({ key: 3 })
        }
    }

    handleSelect(key) {
        this.setState({key});
        if (key == 1) {
            browserHistory.push('/app/security/users');
        } else if (key == 2) {
            browserHistory.push('/app/security/users/inactive');
        } else if (key == 3) {
            browserHistory.push('/app/security/users/add');
        }
    }

    componentWillReceiveProps(newProps) {
        let currentUrl = window.location.pathname;
        if(currentUrl == "/app/security/users") {
            this.setState({ key: 1 })
        }
    }

    render(){
        return(
            <Tabs activeKey={this.state.key} onSelect={this.handleSelect} animation={false} id="controlled-tab-example" className="margin_left_25">
                <Tab eventKey={1} title="Active Users" />
                <Tab eventKey={2} title="Inactive Users" />
                <Tab eventKey={3} title="Add New User" />
                { this.props.children }
            </Tabs>
        )
    }
}

export default UserDashboard;