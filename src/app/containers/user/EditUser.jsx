import React from 'react';
import { connect } from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, FormGroup, Checkbox, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from './../../components/forms/TextBox';
import { TextArea } from './../../components/forms/TextArea';
import { CheckBox } from './../../components/forms/CheckBox';
import SelectBox from './../../components/forms/SelectBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import './../../components/forms/Styles.css';
import { getGroupsStatusAction, updateUserAction } from '../../actions/authentication';
import { listAllJobsAction } from './../../actions/setup/job';
import { getPagesByGroupAction, getPagesByUserAction } from './../../actions/page';
import { getAllLocationPayAction } from './../../actions/customer/finance/locationPay';
import ReactLoading from 'react-loading';

let arrIds = [];
let arrJobs = [];
let arrLocation = [];
let user = {};
let pageIds = [];

let rcheckArr = [];
let wcheckArr = [];
let ucheckArr = [];
let dcheckArr = [];

class EditUser extends React.Component {
    constructor(props){
        super(props);
        this.state={
            gender: [{id: "MALE", name: "Male"},{id: "FEMALE", name: "Female"}],
            status: [{id: 1, name: "Active"},{id: 0, name: "In Active"}],
            jobs: [],
            location: [],
            rcheckAll: false,
            wcheckAll: false,
            ucheckAll: false,
            dcheckAll: false,
            angleIcons: "fa-angle-right",
            allowChangePwd: false
        };
        this.handleSelectGroup = this.handleSelectGroup.bind(this);
        this.handleCheckboxRead = this.handleCheckboxRead.bind(this);
        this.handleCheckboxWrite = this.handleCheckboxWrite.bind(this);
        this.handleCheckboxUpdate = this.handleCheckboxUpdate.bind(this);
        this.handleCheckboxDelete = this.handleCheckboxDelete.bind(this);
        this.handleCheckboxReadAll = this.handleCheckboxReadAll.bind(this);
        this.handleCheckboxWriteAll = this.handleCheckboxWriteAll.bind(this);
        this.handleCheckboxDeleteAll = this.handleCheckboxDeleteAll.bind(this);
        this.handleCheckboxUpdateAll = this.handleCheckboxUpdateAll.bind(this);
    }

    componentWillMount(){
        this.props.getGroupsStatusAction(true);
        this.props.listAllJobsAction();
        this.props.getAllLocationPayAction();

        user.userGroupDetails.forEach(element => {
            arrIds.push(element.userGroup.id);
            setTimeout(function() {
                $('.gcheck[value="' + element.userGroup.id + '"]').prop('checked', true);
            }, 500);
        });
        
        this.props.getPagesByGroupAction(arrIds);
        this.props.getPagesByUserAction(user.id);
    }

    componentWillUnmount() {
        arrIds = [];
        arrLocation = [];
        this.props.getPagesByGroup.status = 404;
        this.props.getPagesByUser.status = 404;
        user = {};
    }

    componentWillReceiveProps(newProps){
        // push job title to list arrJobs
        if(newProps.listAllJobs.status == 200){
            let jobs = newProps.listAllJobs.data.items;
            if(jobs.length === arrJobs.length) {
                this.setState({jobs : arrJobs})
            } else {
                jobs.forEach((element) => {
                    arrJobs.push({
                        "id": element.id,
                        "name": element.jobTitle
                    });
                });
                this.setState({jobs : arrJobs})
            }
        }

        // push payment location to list arrLocation
        if(newProps.getAllLocationPays.status == 200){
            let location = newProps.getAllLocationPays.data.items;
            if (location.length === arrLocation.length) {
                this.setState({location : arrLocation});
            } else {
                arrLocation = [];
                location.forEach((element) => {
                    if(element.location.toLowerCase() !== "bank") {
                        arrLocation.push({
                            "id": element.id,
                            "name": element.location
                        });
                    }
                });
                this.setState({location : arrLocation});
            }
        }

        if(newProps.getPagesByGroup.status == 200){
            document.getElementById('loading').style.display = 'none';

            if(newProps.getPagesByGroup.data.status != 500) {
                var arrayPageIds = [];
                newProps.getPagesByGroup.data.items.forEach(element => {
                    arrayPageIds.push(element.page.id);
                });
                pageIds = arrayPageIds;
            } else pageIds = [];
        }

        //TODO: --- set time load checked exist pages permission fields ----
        //------------------------------------------------------------------
        setTimeout(function() {
            if(newProps.getPagesByUser.status == 200) {
                let array1 = [];
                let array2 = [];
                let array3 = [];
                let array4 = [];

                newProps.getPagesByUser.data.items.forEach(element => {
                    if(element.canRead == true) {
                        array1.push(element.page.id);
                        $('.rcheck[value="' + element.page.id + '"]').prop('checked', true);
                    }
                    if(element.canCreate == true) {
                        array2.push(element.page.id);
                        $('.wcheck[value="' + element.page.id + '"]').prop('checked', true);
                    }
                    if(element.canUpdate == true) {
                        array3.push(element.page.id);
                        $('.ucheck[value="' + element.page.id + '"]').prop('checked', true);
                    }
                    if(element.canDelete == true) {
                        array4.push(element.page.id);
                        $('.dcheck[value="' + element.page.id + '"]').prop('checked', true);
                    }
                });

                rcheckArr = array1;
                wcheckArr = array2;
                ucheckArr = array3;
                dcheckArr = array4;
            }
        }, 500);

        if(newProps.updateUser !== undefined) {
            if(newProps.updateUser.status === 200){
                alert("Successfully update user!");
                window.history.back();
                return newProps.updateUser.status = 0;
            }
            if(newProps.updateUser.status === 502 || newProps.updateUser.status === 400){
                if(confirm("Fail with update user!") === true){
                    return newProps.updateUser.status = 0;
                }
            }
            if(newProps.updateUser.status === 500){
                if(confirm("The username is already present in database!") === true){
                    return newProps.updateUser.status = 0;
                }
            }
        }
    }

    handleSelectGroup(event){
        document.getElementById('loading').style.display = 'block';

        let index = arrIds.indexOf(Number(event.target.value));
        if( index == -1){
            arrIds.push(Number(event.target.value));
        }else{
            arrIds.splice(index, 1)
        }

        // TODO: On event change clear parent/child checkbox. 
        // -------------------------------------------------
        if(event.target.value) {
            $('.rcheck').prop('checked', false);
            $('.wcheck').prop('checked', false);
            $('.ucheck').prop('checked', false);
            $('.dcheck').prop('checked', false);
            this.setState({rcheckAll: false});
            this.setState({wcheckAll: false});
            this.setState({ucheckAll: false});
            this.setState({dcheckAll: false});
            rcheckArr = [];
            wcheckArr = [];
            ucheckArr = [];
            dcheckArr = [];
            
            this.props.getPagesByUser.status = 404;
        }
        this.props.getPagesByGroupAction(arrIds);
    }

    handleSubmit(values){
        if (arrIds[0] == null) {
            alert('Please Select Group!');
        } else {
            let pagePermission = [];
            
            //TODO: ----- push checkbox value into list pageUserForm permission
            for(var i=0; i<pageIds.length; i++) {
                var canCreate = false;
                var canDelete = false;
                var canRead = false;
                var canUpdate = false;

                var rcheckId = rcheckArr.find(element => element == pageIds[i]);
                var wcheckId = wcheckArr.find(element => element == pageIds[i]);
                var ucheckId = ucheckArr.find(element => element == pageIds[i]);
                var dcheckId = dcheckArr.find(element => element == pageIds[i]);

                if(rcheckId != undefined) {
                    canRead = true;
                }
                if(wcheckId != undefined) {
                    canCreate = true;
                }
                if(ucheckId != undefined) {
                    canUpdate = true;
                }
                if(dcheckId != undefined) {
                    canDelete = true;
                }

                pagePermission.push({
                    "canCreate": canCreate,
                    "canDelete": canDelete,
                    "canRead": canRead,
                    "canUpdate": canUpdate,
                    "pageId": pageIds[i]
                })
            }

            if(this.state.allowChangePwd === false) {
                values.password = undefined;
            }

            if(values.password === undefined) {
                let newUser = {
                    "id": user.id,
                    "employeeForm": {
                        "address": values.address,
                        "description": values.description,
                        "email": values.email,
                        "gender": values.gender,
                        "nameEn": values.nameEn,
                        "nameKh": values.nameKh,
                        "phone": values.phone
                    },
                    "username": values.username,
                    "email": values.email,
                    "jobTitleId": Number(values.jobTitleId),
                    "paymentLocationId": Number(values.paymentLocationId),
                    "userGroupIds": arrIds,
                    "pageUserForm": pagePermission,
                    "status": values.status
                };
                this.props.updateUserAction(newUser);
            } else {
                let newUser = {
                    "id": user.id,
                    "employeeForm": {
                        "address": values.address,
                        "description": values.description,
                        "email": values.email,
                        "gender": values.gender,
                        "nameEn": values.nameEn,
                        "nameKh": values.nameKh,
                        "phone": values.phone
                    },
                    "username": values.username,
                    "password": values.password,
                    "email": values.email,
                    "jobTitleId": Number(values.jobTitleId),
                    "paymentLocationId": Number(values.paymentLocationId),
                    "userGroupIds": arrIds,
                    "pageUserForm": pagePermission,
                    "status": values.status
                };
                this.props.updateUserAction(newUser);
            }

        }
    }

    /**
     * handle checkbox for each group checked function
     * Read, Write, Update, Delete
     * @property {array of checkbox read permistion} rcheckArr
     * @property {array of checkbox write permistion} wcheckArr
     * @property {array of checkbox update permistion} ucheckArr
     * @property {array of checkbox delete permistion} dcheckArr
     */
    handleCheckboxReadAll() {
        this.setState({rcheckAll: !this.state.rcheckAll});
        if(this.state.rcheckAll == false) {
            rcheckArr = Array.from(new Set(pageIds));
        } else {
            rcheckArr = [];
        }
    }

    handleCheckboxWriteAll() {
        this.setState({wcheckAll: !this.state.wcheckAll});
        if(this.state.wcheckAll == false) {
            wcheckArr = Array.from(new Set(pageIds));
        } else {
            wcheckArr = [];
        }
    }

    handleCheckboxUpdateAll() {
        this.setState({ucheckAll: !this.state.ucheckAll});
        if(this.state.ucheckAll == false) {
            ucheckArr = Array.from(new Set(pageIds));
        } else {
            ucheckArr = [];
        }
    }

    handleCheckboxDeleteAll() {
        this.setState({dcheckAll: !this.state.dcheckAll});
        if(this.state.dcheckAll == false) {
            dcheckArr = Array.from(new Set(pageIds));
        } else {
            dcheckArr = [];
        }
    }

    /**
     * handle checkbox for each function
     * Read, Write, Update, Delete
     * @param {* target value from checked box } event
     */
    handleCheckboxRead(event) {
        let index = rcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            rcheckArr.push(Number(event.target.value));
        } else {
            rcheckArr.splice(index, 1)
        }
    }

    handleCheckboxWrite(event) {
        let index = wcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            wcheckArr.push(Number(event.target.value));
        } else {
            wcheckArr.splice(index, 1)
        }
    }

    handleCheckboxUpdate(event) {
        let index = ucheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            ucheckArr.push(Number(event.target.value));
        } else {
            ucheckArr.splice(index, 1)
        }
    }

    handleCheckboxDelete(event) {
        let index = dcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            dcheckArr.push(Number(event.target.value));
        } else {
            dcheckArr.splice(index, 1)
        }
    }

    handleChangePassword() {
        this.setState({allowChangePwd: !this.state.allowChangePwd});
        if(this.state.allowChangePwd === false) {
            this.setState({angleIcons: "fa-angle-down"});
        } else {
            this.setState({angleIcons: "fa-angle-right"});
        }
    }

    render(){
        const { handleSubmit, error, invalid, submitting } = this.props;

        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#rcheck").click(function () {
                $(".rcheck").prop('checked', $(this).prop('checked'));
            });
            $("#wcheck").click(function () {
                $(".wcheck").prop('checked', $(this).prop('checked'));
            });
            $("#ucheck").click(function () {
                $(".ucheck").prop('checked', $(this).prop('checked'));
            });
            $("#dcheck").click(function () {
                $(".dcheck").prop('checked', $(this).prop('checked'));
            });
        });

        return(
            <div className="margin_left_25">
                <Row>
                    <Col xs={6} sm={6} md={4}>
                        <h3>
                            <Link onClick={() => window.history.back()} className="a-no-link"><i className="fa fa-arrow-left" /></Link>
                            &nbsp;&nbsp;Edit User
                        </h3>
                        <hr/>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Khmer Name <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="nameKh" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Khmer Name" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>English Name <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="nameEn" 
                                        type="text" 
                                        component={TextBox} 
                                        label="English Name" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Username <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="username" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Username" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Gender <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="gender" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select gender" 
                                        values={this.state.gender} 
                                        sortBy="name" 
                                        icon="fa fa-shopping-bag"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Phone <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="phone" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Phone Number" 
                                        icon="fa fa-phone"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Email <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="email" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Email" 
                                        icon="fa fa-envelope-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Address <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="address" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Address" 
                                        icon="fa fa-home"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Job Title <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="jobTitleId" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select job title" 
                                        values={this.state.jobs} 
                                        sortBy="name" 
                                        icon="fa fa-shopping-bag"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Location <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="paymentLocationId" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select Location" 
                                        values={this.state.location} 
                                        sortBy="name" 
                                        icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="description" 
                                        type="text" 
                                        component={TextArea} 
                                        label="Description" 
                                        icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Status <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="status" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select status" 
                                        values={this.state.status} 
                                        sortBy="name" 
                                        icon="fa fa-hand-paper-o"/>
                                </Col>
                            </Row>

                            <Row>
                                <Col md={12}>
                                    <Row>
                                        <Col md={12}>
                                            <Link to="" onClick={() => this.handleChangePassword()} className="lnk-change-password">
                                                <h4><i className="fa fa-key"/> Change Password &emsp;<i className={"fa " + this.state.angleIcons}/></h4>
                                            </Link>
                                        </Col>
                                    </Row>
                                    { this.state.allowChangePwd === true ? 
                                        <div>
                                            <Row>
                                                <hr/>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>New Password <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="password" 
                                                        type="password" 
                                                        component={TextBox} 
                                                        label="New Password" 
                                                        icon="fa fa-key"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Confirm Password <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="confirm_password" 
                                                        type="password" 
                                                        component={TextBox} 
                                                        label="Confirm password" 
                                                        icon="fa fa-key"/>
                                                </Col>
                                            </Row>
                                        </div>
                                    : null }
                                </Col>
                            </Row>

                            <Row>
                                <Col mdOffset={6} md={3}>
                                    <ButtonSubmit 
                                        error={error} 
                                        invalid={invalid} 
                                        submitting={submitting} 
                                        label="Save" />
                                </Col>
                                <Col md={3} className="col-padding-left-0">
                                    <Button className="btn btn-default btn-block" 
                                        onClick={() => window.history.back()}>
                                        Cancel
                                    </Button>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                    {/* assign menu to group*/}
                    <Col xs={6} sm={6} md={7} className="margin_left_25">
                        <h3>Assign Group</h3>
                        <hr/>
                        <div style={{border:"1px solid #DDDDDD", padding: '10px 20px 0 20px', width: 300}}>
                            { this.props.getGroupsStatus.status == 200?
                                <FormGroup onChange={ this.handlehandleSelectGroup }>
                                    {
                                        this.props.getGroupsStatus.data.items.map((group, index) => {
                                            return(
                                                <div key={index}>
                                                    <input 
                                                        type="checkbox"
                                                        className="gcheck"
                                                        onChange={this.handleSelectGroup}
                                                        value={ group.id } /> 
                                                        &nbsp;&nbsp;{ group.groupName }
                                                </div>
                                            )
                                        })
                                    }
                                </FormGroup>
                            : <p>Groups Not Found</p> }
                        </div>

                        <div id="loading" style={{display: 'none', marginTop: 20}}>
                            <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                        </div>

                        <div>
                        { this.props.getPagesByGroup.status == 200 ? 
                            this.props.getPagesByGroup.data.status != 500 ?
                                <table className="page-permision-table">
                                    <thead>
                                        <tr>
                                            <th>Page Name</th>
                                            <th>Description</th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.rcheckAll}
                                                    onChange={this.handleCheckboxReadAll}
                                                    id="rcheck" /> Read
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.wcheckAll}
                                                    onChange={this.handleCheckboxWriteAll}
                                                    id="wcheck" /> Write
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.ucheckAll}
                                                    onChange={this.handleCheckboxUpdateAll}
                                                    id="ucheck" /> Update
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.dcheckAll}
                                                    onChange={this.handleCheckboxDeleteAll}
                                                    id="dcheck" /> Delete
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    { this.props.getPagesByGroup.data.items.map((item, index) => {
                                        return(
                                            <tr key={index}>
                                                <td>{item.page.pageName}</td>
                                                <td>{item.page.description}</td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxRead}
                                                        className="rcheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxWrite}
                                                        className="wcheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxUpdate}
                                                        className="ucheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxDelete}
                                                        className="dcheck" />
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    </tbody>
                                </table>
                            : null
                        : null }
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
EditUser = reduxForm({
    form: 'form_edit_user',
    validate: (values) => {
        let regex_name = /[a-zA-Z]{1,20}/;
        let regex_username = /^\S[0-9a-zA-Z]{4,20}$/;
        let regex_phone = /^0\d{8,9}$/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex_password = /^\S[0-9a-zA-Z]{6,20}$/;

        const errors = {};
        if (values.nameKh === undefined) {
            errors.nameKh = 'Firstname is required and at least 1 character!';
        }
        if (!regex_name.test(values.nameEn) || values.nameEn === undefined) {
            errors.nameEn = 'Lastname is required and at least 1 character!';
        }
        if (!regex_username.test(values.username) || values.username === undefined) {
            errors.username = 'Username is required and at least 4 characters without spaces!';
        }
        if (values.gender === undefined) {
            errors.gender = 'Gender is required!';
        }
        if (!regex_phone.test(values.phone) || values.phone === undefined) {
            errors.phone = 'Invalid phone!';
        }
        if (!regex_email.test(values.email) || values.email === undefined) {
            errors.email = 'Invalid email!';
        }
        if (values.address === undefined) {
            errors.address = 'Address is required!';
        }
        if (values.jobTitleId === undefined) {
            errors.jobTitle = 'Job Title is required!';
        }
        if (values.paymentLocationId === undefined) {
            errors.paymentLocationId = 'Location is required!';
        }
        if(values.enabled === undefined){
            errors.enabled = 'Status is required!';
        }
        if(!regex_password.test(values.password) || values.password === undefined){
            errors.password = 'Password is required at least 6 characters without spaces!';
        }
        if(values.confirm_password != values.password){
            errors.confirm_password = 'Confirm password is not match!';
        }
        return errors
    }
})(EditUser);

function mapStateToProps(state, own_props) {
    user = {
        "id": 0,
        "address": "",
        "description": "",
        "email": "",
        "gender": "",
        "nameEn": "",
        "nameKh": "",
        "phone": "",
        "username": "",
        "email": "",
        "jobTitleId": 0,
        "paymentLocationId": 0,
        "userGroupDetails": [],
        "status": 0
    };
   

    if (state.userAuthentication.getUsersStatus.status == 200) {
        let data = {};
        data = state.userAuthentication.getUsersStatus.data.items.list
                    .find(userItem => userItem.id == own_props.params.id ) || user;
        user = {
            "id": data.id,
            "address": data.employee.address,
            "description": data.employee.description,
            "email": data.employee.email,
            "gender": data.employee.gender,
            "nameEn": data.employee.nameEn,
            "nameKh": data.employee.nameKh,
            "phone": data.employee.phone,
            "username": data.username,
            "email": data.email,
            "jobTitleId": data.jobTitle.id,
            "paymentLocationId": data.paymentLocation.id,
            "userGroupDetails": data.userGroupDetails,
            "status": data.status
        };
    }

    return {
        getGroupsStatus: state.userAuthentication.getGroupsStatus,
        listAllJobs: state.job.listAllJobs,
        updateUser: state.userAuthentication.updateUser,
        getPagesByGroup: state.page.getPagesByGroup,
        getPagesByUser: state.page.getPagesByUser,
        getAllLocationPays: state.locationPays.getAllLocationPays,
        initialValues: user
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
                getGroupsStatusAction,
                listAllJobsAction,
                updateUserAction,
                getPagesByGroupAction,
                getPagesByUserAction,
                getAllLocationPayAction
        }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);