import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, FormGroup, Checkbox } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { TextBox } from './../../components/forms/TextBox';
import { TextArea } from './../../components/forms/TextArea';
import SelectBox from './../../components/forms/SelectBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import './../../components/forms/Styles.css';
import { getGroupsStatusAction, addUserAction } from '../../actions/authentication';
import { listAllJobsAction } from './../../actions/setup/job';
import { getPagesByGroupAction } from './../../actions/page';
import { getAllLocationPayAction } from './../../actions/customer/finance/locationPay';
import ReactLoading from 'react-loading';

let arrIds = [];
let arrJobs = [];
let arrLocation = [];

let pageIds = [];
let rcheckArr = [];
let wcheckArr = [];
let ucheckArr = [];
let dcheckArr = [];

class AddUser extends React.Component {
    constructor(props){
        super(props)
        this.state={
            gender: [{id: "MALE", name: "Male"},{id: "FEMALE", name: "Female"}],
            status: [{id: 1, name: "Active"},{id: 0, name: "In Active"}],
            jobs: [],
            location: [],
            rcheckAll: false,
            wcheckAll: false,
            ucheckAll: false,
            dcheckAll: false
        };
        this.handleSelectGroup = this.handleSelectGroup.bind(this);
        this.handleCheckboxRead = this.handleCheckboxRead.bind(this);
        this.handleCheckboxWrite = this.handleCheckboxWrite.bind(this);
        this.handleCheckboxUpdate = this.handleCheckboxUpdate.bind(this);
        this.handleCheckboxDelete = this.handleCheckboxDelete.bind(this);
        this.handleCheckboxReadAll = this.handleCheckboxReadAll.bind(this);
        this.handleCheckboxWriteAll = this.handleCheckboxWriteAll.bind(this);
        this.handleCheckboxDeleteAll = this.handleCheckboxDeleteAll.bind(this);
        this.handleCheckboxUpdateAll = this.handleCheckboxUpdateAll.bind(this);
    }

    componentWillMount(){
        this.props.getGroupsStatusAction(true);
        this.props.listAllJobsAction();
        this.props.getAllLocationPayAction();
    }

    componentWillUnmount() {
        arrIds = [];
        arrLocation = [];
        this.props.getPagesByGroup.status = 404;
    }

    componentWillReceiveProps(newProps){
        // push job title to list.
        if(newProps.listAllJobs.status == 200){
            let jobs = newProps.listAllJobs.data.items;
            if (jobs.length === arrJobs.length) {
                this.setState({jobs : arrJobs});
            } else {
                jobs.forEach((element) => {
                    arrJobs.push({
                        "id": element.id,
                        "name": element.jobTitle
                    });
                });
                this.setState({jobs : arrJobs});
            }
        }
        // push payment location to list
        if(newProps.getAllLocationPays.status == 200){
            let location = newProps.getAllLocationPays.data.items;
            if (location.length === arrLocation.length) {
                this.setState({location : arrLocation});
            } else {
                arrLocation = [];
                location.forEach((element) => {
                    if(element.location.toLowerCase() !== "bank") {
                        arrLocation.push({
                            "id": element.id,
                            "name": element.location
                        });
                    }
                });
                this.setState({location : arrLocation});
            }
        }

        if(newProps.addUser != undefined) {
            if(newProps.addUser.status === 200){
                alert("Successfully added new user");
                browserHistory.push("/app/security/users");
                return newProps.addUser.status = 0;
            }
            if(newProps.addUser.status === 502 || newProps.addUser.status === 400){
                if(confirm("Fail with adding new user") === true){
                    browserHistory.push("/app/security/users");
                    return newProps.addUser.status = 0;
                }
            }
            if(newProps.addUser.status === 500){
                if(confirm("The username is already present in database!") === true){
                    return newProps.addUser.status = 0;
                }
            }
        }
        if(newProps.getPagesByGroup != undefined) {
            document.getElementById('loading').style.display = 'none';

            if(newProps.getPagesByGroup.status == 200){
                if(newProps.getPagesByGroup.data.status != 500) {
                    var arrayPageIds = [];
                    newProps.getPagesByGroup.data.items.forEach((element) => {
                        arrayPageIds.push(element.page.id);
                    });
                    pageIds = arrayPageIds;
                } else pageIds = [];
            }
        }
    }

    /**
     * handle checked group
     * @param {* onChange get group user Id} event
     * arrIds: array of id groups.
     */
    handleSelectGroup(event){
        document.getElementById('loading').style.display = 'block';

        let index = arrIds.indexOf(Number(event.target.value));
        if( index == -1){
            arrIds.push(Number(event.target.value));
        } else arrIds.splice(index, 1)
        
        // TODO: On event change clear parent/child checkbox. 
        // -------------------------------------------------
        if(event.target.value) {
            $('.rcheck').prop('checked', false);
            $('.wcheck').prop('checked', false);
            $('.ucheck').prop('checked', false);
            $('.dcheck').prop('checked', false);
            this.setState({rcheckAll: false});
            this.setState({wcheckAll: false});
            this.setState({ucheckAll: false});
            this.setState({dcheckAll: false});
            rcheckArr = [];
            wcheckArr = [];
            ucheckArr = [];
            dcheckArr = [];
            
        }
        this.props.getPagesByGroupAction(arrIds);
    }

    /**
     * handle checkbox for each group checked function
     * Read, Write, Update, Delete
     * @property {array of checkbox read permistion} rcheckArr
     * @property {array of checkbox write permistion} wcheckArr
     * @property {array of checkbox update permistion} ucheckArr
     * @property {array of checkbox delete permistion} dcheckArr
     */
    handleCheckboxReadAll() {
        this.setState({rcheckAll: !this.state.rcheckAll});
        if(this.state.rcheckAll == false) {
            rcheckArr = Array.from(new Set(pageIds));
        } else {
            rcheckArr = [];
        }
    }

    handleCheckboxWriteAll() {
        this.setState({wcheckAll: !this.state.wcheckAll});
        if(this.state.wcheckAll == false) {
            wcheckArr = Array.from(new Set(pageIds));
        } else {
            wcheckArr = [];
        }
    }

    handleCheckboxUpdateAll() {
        this.setState({ucheckAll: !this.state.ucheckAll});
        if(this.state.ucheckAll == false) {
            ucheckArr = Array.from(new Set(pageIds));
        } else {
            ucheckArr = [];
        }
    }

    handleCheckboxDeleteAll() {
        this.setState({dcheckAll: !this.state.dcheckAll});
        if(this.state.dcheckAll == false) {
            dcheckArr = Array.from(new Set(pageIds));
        } else {
            dcheckArr = [];
        }
    }

    /**
     * handle checkbox for each function
     * Read, Write, Update, Delete
     * @param {* target value from checked box } event
     */
    handleCheckboxRead(event) {
        let index = rcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            rcheckArr.push(Number(event.target.value));
        } else {
            rcheckArr.splice(index, 1)
        }
    }

    handleCheckboxWrite(event) {
        let index = wcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            wcheckArr.push(Number(event.target.value));
        } else {
            wcheckArr.splice(index, 1)
        }
    }

    handleCheckboxUpdate(event) {
        let index = ucheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            ucheckArr.push(Number(event.target.value));
        } else {
            ucheckArr.splice(index, 1)
        }
    }

    handleCheckboxDelete(event) {
        let index = dcheckArr.indexOf(Number(event.target.value));
        if( index == -1){
            dcheckArr.push(Number(event.target.value));
        } else {
            dcheckArr.splice(index, 1)
        }
    }

    /**
     * handle submit form insert user.
     * @param {*values of input} values 
     */
    handleSubmit(values){
        if (arrIds.length <= 0) {
            alert("Please select groups!");
        } else {
            let pagePermission = [];
            
            //TODO: ----- push checkbox value into list pageUserForm permission
            for(var i=0; i<pageIds.length; i++) {
                var canCreate = false;
                var canDelete = false;
                var canRead = false;
                var canUpdate = false;

                var rcheckId = rcheckArr.find(element => element == pageIds[i]);
                var wcheckId = wcheckArr.find(element => element == pageIds[i]);
                var ucheckId = ucheckArr.find(element => element == pageIds[i]);
                var dcheckId = dcheckArr.find(element => element == pageIds[i]);

                if(rcheckId != undefined) {
                    canRead = true;
                }
                if(wcheckId != undefined) {
                    canCreate = true;
                }
                if(ucheckId != undefined) {
                    canUpdate = true;
                }
                if(dcheckId != undefined) {
                    canDelete = true;
                }

                pagePermission.push({
                    "canCreate": canCreate,
                    "canDelete": canDelete,
                    "canRead": canRead,
                    "canUpdate": canUpdate,
                    "pageId": pageIds[i]
                })
            }

            let user = {
                "employeeForm": {
                    "address": values.address,
                    "description": values.description,
                    "email": values.email,
                    "gender": values.gender,
                    "nameEn": values.nameEn,
                    "nameKh": values.nameKh,
                    "phone": values.phone
                },
                "username": values.username,
                "email": values.email,
                "jobTitleId": Number(values.jobTitleId),
                "paymentLocationId": Number(values.paymentLocationId),
                "password": values.password,
                "userGroupIds": arrIds,
                "pageUserForm": pagePermission,
                "status": values.status
            };
            this.props.addUserAction(user);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;

        //TODO: ----------- group checked parent boxs. -----------
        $( document ).ready(function() {
            $("#rcheck").click(function () {
                $(".rcheck").prop('checked', $(this).prop('checked'));
            });
            $("#wcheck").click(function () {
                $(".wcheck").prop('checked', $(this).prop('checked'));
            });
            $("#ucheck").click(function () {
                $(".ucheck").prop('checked', $(this).prop('checked'));
            });
            $("#dcheck").click(function () {
                $(".dcheck").prop('checked', $(this).prop('checked'));
            });
        });

        return(
            <div className="margin_left_25">
                <Row>
                    <Col xs={6} sm={6} md={4}>
                        <h3>Add New User</h3>
                        <hr/>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Khmer Name <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="nameKh" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Khmer Name" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>English Name <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="nameEn" 
                                        type="text" 
                                        component={TextBox} 
                                        label="English Name" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Username <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="username" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Username" 
                                        icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Gender <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="gender" 
                                        type="select" 
                                        component={SelectBox}
                                        placeholder="Please select gender" 
                                        values={this.state.gender} 
                                        sortBy="name" 
                                        icon="fa fa-shopping-bag"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Phone <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="phone" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Phone Number" 
                                        icon="fa fa-phone"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Email <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="email" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Email" 
                                        icon="fa fa-envelope-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Address <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="address" 
                                        type="text" 
                                        component={TextBox} 
                                        label="Address" 
                                        icon="fa fa-home"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Job Title <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="jobTitleId" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select job title" 
                                        values={this.state.jobs} 
                                        sortBy="name" 
                                        icon="fa fa-shopping-bag"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Location <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="paymentLocationId" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select location" 
                                        values={this.state.location} 
                                        sortBy="name" 
                                        icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Description</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="description" 
                                        type="text" 
                                        component={TextArea} 
                                        label="Description" 
                                        icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Password <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="password" 
                                        type="password" 
                                        component={TextBox} 
                                        label="Password" 
                                        icon="fa fa-key"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Confirm Password <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="confirm_password" 
                                        type="password" 
                                        component={TextBox} 
                                        label="Confirm password" 
                                        icon="fa fa-key"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Status <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field 
                                        name="status" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select status" 
                                        values={this.state.status} 
                                        sortBy="name" 
                                        icon="fa fa-hand-paper-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit 
                                        error={error} 
                                        invalid={invalid} 
                                        submitting={submitting} 
                                        label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </Col>
                    {/* assign group and page to user */}
                    <Col xs={6} sm={6} md={7} className="margin_left_25">
                        <h3>Assign Group</h3>
                        <hr/>
                        <div style={{border:"1px solid #DDDDDD", padding: '0 20px', width: 300}}>
                            { this.props.getGroupsStatus.status == 200?
                                <FormGroup onChange={ this.handleSelectGroup }>
                                    {
                                        this.props.getGroupsStatus.data.items.map((group, index) => {
                                            return(
                                                <Checkbox value={group.id} key={index}>{group.groupName}</Checkbox>
                                            )
                                        })
                                    }
                                </FormGroup>
                            : <p>Groups Not Found</p> }
                        </div>

                        <div id="loading" style={{display: 'none', marginTop: 20}}>
                            <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                        </div>

                        <div>
                        { this.props.getPagesByGroup.status == 200 ? 
                            this.props.getPagesByGroup.data.status != 500 ?
                                <table className="page-permision-table">
                                    <thead>
                                        <tr>
                                            <th>Page Name</th>
                                            <th>Description</th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.rcheckAll}
                                                    onChange={this.handleCheckboxReadAll}
                                                    id="rcheck" /> Read
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.wcheckAll}
                                                    onChange={this.handleCheckboxWriteAll}
                                                    id="wcheck" /> Write
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.ucheckAll}
                                                    onChange={this.handleCheckboxUpdateAll}
                                                    id="ucheck" /> Update
                                            </th>
                                            <th><input 
                                                    type="checkbox"
                                                    checked={this.state.dcheckAll}
                                                    onChange={this.handleCheckboxDeleteAll}
                                                    id="dcheck" /> Delete
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    { this.props.getPagesByGroup.data.items.map((item, index) => {
                                        return(
                                            <tr key={index}>
                                                <td>{item.page.pageName}</td>
                                                <td>{item.page.description}</td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxRead}
                                                        className="rcheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxWrite}
                                                        className="wcheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxUpdate}
                                                        className="ucheck" />
                                                </td>
                                                <td><input 
                                                        type="checkbox"
                                                        value={item.page.id}
                                                        onChange={this.handleCheckboxDelete}
                                                        className="dcheck" />
                                                </td>
                                            </tr>
                                        );
                                    })}
                                    </tbody>
                                </table>
                            : <p>Pages Not Found</p>
                        : null }
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
AddUser = reduxForm({
    form: 'form_add_user',
    validate: (values) => {
        let regex_name = /[a-zA-Z]{1,20}/;
        let regex_username = /^\S[0-9a-zA-Z]{4,20}$/;
        let regex_phone = /^0\d{8,9}$/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex_password = /^\S[0-9a-zA-Z]{6,20}$/;

        const errors = {};
        if (values.nameKh === undefined) {
            errors.nameKh = 'Khmer Name is required and at least 1 character!';
        }
        if (!regex_name.test(values.nameEn) || values.nameEn === undefined) {
            errors.nameEn = 'English Name is required and at least 1 character!';
        }
        if (!regex_username.test(values.username) || values.username === undefined) {
            errors.username = 'Username is required and at least 4 characters without spaces!';
        }
        if (values.gender === undefined) {
            errors.gender = 'Gender is required!';
        }
        if (!regex_phone.test(values.phone) || values.phone === undefined) {
            errors.phone = 'Invalid phone!';
        }
        if (!regex_email.test(values.email) || values.email === undefined) {
            errors.email = 'Invalid email!';
        }
        if (values.address === undefined) {
            errors.address = 'Address is required!';
        }
        if (values.jobTitleId === undefined) {
            errors.jobTitleId = 'Job Title is required!';
        }
        if (values.paymentLocationId === undefined) {
            errors.paymentLocationId = 'Location is required!';
        }
        if(!regex_password.test(values.password) || values.password === undefined){
            errors.password = 'Password is required at least 6 characters!';
        }
        if(!regex_password.test(values.confirm_password) || values.confirm_password === undefined){
            errors.confirm_password = 'Confirm password is required at least 6 characters without spaces!';
        }
        if(values.confirm_password != values.password){
            errors.confirm_password = 'Confirm password is not match!';
        }
        if(values.status === undefined){
            errors.status = 'Status is required!';
        }
        return errors
    }
})(AddUser);

function mapStateToProps(state) {
    return {
        addUser : state.userAuthentication.addUser,
        listAllJobs: state.job.listAllJobs,
        getGroupsStatus: state.userAuthentication.getGroupsStatus,
        getPagesByGroup: state.page.getPagesByGroup,
        getAllLocationPays: state.locationPays.getAllLocationPays
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
                listAllJobsAction, 
                getGroupsStatusAction, 
                addUserAction, 
                getPagesByGroupAction,
                getAllLocationPayAction
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);