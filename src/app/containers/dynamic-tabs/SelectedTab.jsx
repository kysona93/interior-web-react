import React from '../../../../node_modules/react';
import { browserHistory } from 'react-router';

export const selectedTab = (key) => {


    /*const SETUP = "/app/administration/setup/";
    switch (key) {
        /!** LOCATION **!/
        case `${SETUP}locations`: return browserHistory.push(`${SETUP}locations`);
        /!** POLE **!/
        case `${SETUP}poles` : return browserHistory.push(`${SETUP}poles`);
        case `${SETUP}poles/add` : return browserHistory.push(`${SETUP}poles/add`);
        /!** BOX **!/
        case `${SETUP}boxes` : return browserHistory.push(`${SETUP}boxes`);
        case `${SETUP}boxes/add` : return browserHistory.push(`${SETUP}boxes/add`);
        /!** TRANSFORMER **!/
        case `${SETUP}transformers` : return browserHistory.push(`${SETUP}transformers`);
        case `${SETUP}transformers/add` : return browserHistory.push(`${SETUP}transformers/add`);
        /!** BREAKER **!/
        case `${SETUP}breakers` : return browserHistory.push(`${SETUP}breakers`);
        case `${SETUP}breakers/add` : return browserHistory.push(`${SETUP}breakers/add`);
        /!** METER **!/
        case `${SETUP}meters` : return browserHistory.push(`${SETUP}meters`);
        case `${SETUP}meters/add` : return browserHistory.push(`${SETUP}meters/add`);
        /!** AMPERE **!/
        case `${SETUP}amperes` : return browserHistory.push(`${SETUP}amperes`);
        /!** CUSTOMER GROUP **!/
        case `${SETUP}customer-group` : return browserHistory.push(`${SETUP}customer-group`);
        /!** RATE PER KWH **!/
        case `${SETUP}rate-per-kwh` : return browserHistory.push(`${SETUP}rate-per-kwh`);
        /!** OCCUPATION **!/
        case `${SETUP}occupations` : return browserHistory.push(`${SETUP}occupations`);
        /!** TYPES **!/
        case `${SETUP}types/box` : return browserHistory.push(`${SETUP}types/box`);
        case `${SETUP}types/transformer` : return browserHistory.push(`${SETUP}types/transformer`);
        case `${SETUP}types/pole` : return browserHistory.push(`${SETUP}types/pole`);
        case `${SETUP}types/breaker` : return browserHistory.push(`${SETUP}types/breaker`);
        case `${SETUP}types/meter` : return browserHistory.push(`${SETUP}types/meter`);
        case `${SETUP}types/consumption` : return browserHistory.push(`${SETUP}types/consumption`);
        case `${SETUP}types/customer` : return browserHistory.push(`${SETUP}types/customer`);
        case `${SETUP}types/sale` : return browserHistory.push(`${SETUP}types/sale`);
        case `${SETUP}types/voltage` : return browserHistory.push(`${SETUP}types/voltage`);
        /!** REGISTRATION **!/
        case '/app/customers/registration/add' : return browserHistory.push('/app/customers/registration/add');
        case '/app/customers/registration/edit' : return browserHistory.push('/app/customers/registration/edit');
        case '/app/customers/registration/list' : return browserHistory.push('/app/customers/registration/list');
        /!** INVENTORY *!/
        case "/app/inventory/supplier/list" : return browserHistory.push('/app/inventory/supplier/list');
        case "/app/inventory/supplier/add" : return browserHistory.push('/app/inventory/supplier/list');
        /!** SECURITY *!/
        case "/app/security/users" : return browserHistory.push('/app/security/users');
        case "/app/security/groups" : return browserHistory.push('/app/security/groups');
        case "/app/security/menus" : return browserHistory.push('/app/security/menus');
        case "/app/security/menus/assign" : return browserHistory.push('/app/security/menus/assign');
        /!** HOME **!/
        default : return browserHistory.push('/app');
    }*/
    browserHistory.push(key);
};