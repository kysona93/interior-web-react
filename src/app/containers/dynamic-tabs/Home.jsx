import React from '../../../../node_modules/react';
import { push } from 'react-router-redux';
import { store } from "../../../store.js";
import '../index.css';

export default class Home extends React.Component {
    constructor(props){
        super(props);
    }

    handleNewTab(link, name){
        if(link !== null && link !== undefined && link !== ""){
            this.props.tab(
                link,
                name
            );
            store.dispatch(push(link));
        }
    }

    render(){
        return(
            <div>
                <div className="row">
                    <div className="col-md-8 col-lg-12">
                        <div className="row">
                           <div className="col-xs-1 col-md-1"> </div>
                            <div className="col-xs-3 col-md-3">
                                <a onClick={() => this.handleNewTab("/app/customers/registration/add", "Customer Registration")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/customerlist.png"/></span> <br/>Customer Registration</a>
                                <a onClick={() => this.handleNewTab("/app/administration/setup/installers", "Installer")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/installation.png"/> </span> <br/>Installer</a>
                                <a onClick={() => this.handleNewTab("/app/customers/registration/add", "Customer Filter List")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/customer-list.png"/> </span> <br/>Customer Filter List</a>
                            </div>
                            <div className="col-xs-1 col-md-1"> </div>
                            <div className="col-xs-3 col-md-3">
                                <a onClick={() => this.handleNewTab("/app/customers/registration/add", "New Customer")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/register2.png"/> </span> <br/>New Customer</a>
                                <a onClick={() => this.handleNewTab("/app/administration/setup/poles/add", "Pole")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/pole.png"/> </span> <br/>Pole</a>
                                <a onClick={() => this.handleNewTab("/app/installation/installers/install-schedule", "Installation")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/installtion-schedual.png"/> </span> <br/>Installer Schedule</a>
                            </div>
                            <div className="col-xs-1 col-md-1"> </div>
                            <div className="col-xs-3 col-md-3">
                                <a onClick={() => this.handleNewTab("/app/customers/registration/add", "Print Invoice")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/printinvoice.png"/> </span> <br/>Print Invoice</a>
                                <a onClick={() => this.handleNewTab("/app/administration/setup/boxes/add", "Box")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/meter2.png"/> </span> <br/>Box</a>
                                <a onClick={() => this.handleNewTab("/app/administration/setup/transformers/add", "Transformer")} className="btn btn-primary home btn-lg" role="button"><span className="icon-home"><img src="/icons/transformers.png"/> </span> <br/>Transformer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}