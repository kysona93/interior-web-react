import React from '../../../../node_modules/react';
import {Tab} from 'react-draggable-tab';

const home = (
    <Tab key={'/app'} title={'Home'} disableClose={true}>
        <div></div>
    </Tab>);

const other = (key, title) => {
  return (
      (<Tab key={key} title={title}>
          <div></div>
      </Tab>)
  )
};

export const defaultTabs = (pathname, param) => {
    const SETUP = "/app/administration/setup/";
    const type  = param.type || 'fussy';
    const option = param.opt || 'id';
    const search = param.search || '';
    const name = param.name || '';
    const id = param.id || '';
    switch (pathname) {
        /** LOCATION **/
        case `${SETUP}locations`: return([home,other(pathname, "Location")]);
        /** POLE **/
        case `${SETUP}poles` : return([home,other(pathname, "List Poles")]);
        case `${SETUP}poles/add` : return([home,other(pathname, "Add Pole")]);
        /** BOX **/
        case `${SETUP}boxes` : return([home,other(pathname, "List Boxes")]);
        case `${SETUP}boxes/add` : return([home,other(pathname, "Add Box")]);
        /** TRANSFORMER **/
        case `${SETUP}transformers` : return([home,other(pathname, "List Transformers")]);
        case `${SETUP}transformers/add` : return([home,other(pathname, "Add Transformer")]);
        /** BREAKER **/
        case `${SETUP}breakers` : return([home,other(pathname, "List Breakers")]);
        case `${SETUP}breakers/add` : return([home,other(pathname, "Add Breaker")]);
        /** METER **/
        case `${SETUP}meters` : return([home,other(pathname, "List Meters")]);
        case `${SETUP}meters/add` : return([home,other(pathname, "Add Meter")]);
        /** INSTALLER **/
        case `${SETUP}installers` : return([home,other(pathname, "List Installer")]);
        /** AMPERE **/
        case `${SETUP}amperes` : return([home,other(pathname, "Amperes")]);
        /** OCCUPATION **/
        case `${SETUP}occupations` : return([home,other(pathname, "Occupation")]);
        /** CUSTOMER GROUP **/
        case `${SETUP}customer-group` : return([home,other(pathname, "Customer Group")]);
        /** RATE PER KWH **/
        case `${SETUP}rate-per-kwh` : return([home,other(pathname, "Rate Per Kwh")]);
        /** TYPES **/
        case `${SETUP}types/box` : return([home,other(pathname, "Box Type")]);
        case `${SETUP}types/transformer` : return([home,other(pathname, "Transformer Type")]);
        case `${SETUP}types/pole` : return([home,other(pathname, "Pole Type")]);
        case `${SETUP}types/breaker` : return ([home,other(pathname, "Breaker Type")]);
        case `${SETUP}types/meter` : return ([home,other(pathname, "Meter Type")]);
        case `${SETUP}types/consumption` : return ([home,other(pathname, "Consumption Type")]);
        case `${SETUP}types/customer` : return ([home,other(pathname, "customer Type")]);
        case `${SETUP}types/sale` : return ([home,other(pathname, "Sale Type")]);
        case `${SETUP}types/voltage` : return ([home,other(pathname, "Voltage Type")]);
        /** CUSTOMER **/
        case "/app/customers" : return ([home, other(`${pathname}?type=${type}&opt=${option}&search=${search}`, `Search(${search})`)]);
        case "/app/customers/info" : return ([home, other(`${pathname}?id=${id}&name=${name}`, `${name}`)]);
        case '/app/customers/registration/add' : return([home, other(pathname, "Customer Registration")]);
        /** INVENTORY */
        case "/app/inventory/supplier/list" : return ([home, other(pathname, "Supplier Management")]);
        case "/app/inventory/supplier/add" : return ([home, other(pathname, "Add Supplier")]);

        /** SECURITY */
        case "/app/security/users" : return ([home, other(pathname, "User Management")]);
        case "/app/security/groups" : return ([home, other(pathname, "Group Management")]);
        case "/app/security/menus" : return ([home, other(pathname, "Menu Management")]);
        case "/app/security/menus/assign" : return ([home, other(pathname, "Assign Menus")]);
        /** HOME **/
        default :return([home]);
    }
};