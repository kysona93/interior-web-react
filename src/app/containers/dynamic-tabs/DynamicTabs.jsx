import React from '../../../../node_modules/react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import {Tabs} from 'react-draggable-tab';
import { selectedTab } from './SelectedTab';
import { getCustomerAction } from './../../actions/customer/customer';
import './tab.css';

const tabsClassNames = {
    tabWrapper: 'myWrapper',
    tabBar: 'myTabBar',
    tabBarAfter: 'myTabBarAfter',
    tab:      'myTab',
    tabTitle: 'myTabTitle',
    tabCloseIcon: 'tabCloseIcon',
    tabBefore: 'myTabBefore',
    tabAfter: 'myTabAfter'
};

const tabsStyles = {
    tabWrapper: {marginTop: '10px'},
    tabBar: {},
    tab:{},
    tabTitle: {},
    tabCloseIcon: {},
    tabBefore: {},
    tabAfter: {}
};

class DynamicTabs extends React.Component {
    constructor(props) {
        super(props);
    }

    handleTabSelect(e, key, currentTabs) {
        if(key.indexOf("app/customers/info?id=") > 0){
            const k = key.replace("/app/customers/info?id=", "").split("&")[0];
            this.props.getCustomerAction(k);
        }
        this.props.selectTab(key, currentTabs);
        selectedTab(key);
    }

    handleTabClose(e, key, currentTabs) {
        this.props.closeTab(currentTabs);
    }

    render() {
        return (
            <div>
                <Tabs
                    tabsClassNames={tabsClassNames}
                    tabsStyles={tabsStyles}
                    selectedTab={this.props.activeTab ? this.props.activeTab : this.props.defaultTab}
                    onTabSelect={this.handleTabSelect.bind(this)}
                    onTabClose={this.handleTabClose.bind(this)}
                    tabs={this.props.tabs}
                />
            </div>
        )
    }
}

function mapStateToProps(state) {
    //console.log("customer", state.customer.customer);
    return {
        customer: state.customer.customer
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getCustomerAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(DynamicTabs);