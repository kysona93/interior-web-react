import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addGroupInvoiceAction, getAllGroupInvoiceAction, updateGroupInvoiceAction, deleteGroupInvoiceAction } from './../../../actions/invoice/groupInvoices';

class GroupInvoice extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            status: [{id: 1, name: "Active"},{id: 0, name: "In Active"}],
            label: 'Save',
            labelForm: 'New Group Invoice',
            groupId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.getAllGroupInvoiceAction();
    }

    componentWillReceiveProps(data){
        if(data.groupInvoiceAdd.status === 200){
            alert("Successfully added new group invoice.");
            this.props.getAllGroupInvoiceAction();
            this.props.dispatch(initialize("form_add_group_invoice",null));
            data.groupInvoiceAdd.status = 0;
        }
        if(data.groupInvoiceAdd.status === (400 || 404) || data.groupInvoiceAdd.status === (500 || 502)){
            alert("Fail with add group invoice!");
            data.groupInvoiceAdd.status = 0;
        }

        if(data.groupInvoiceUpdate.status === 200 ){
            alert("Successfully updated group invoice.");
            this.props.getAllGroupInvoiceAction();
            this.props.dispatch(initialize("form_add_group_invoice",null));
            data.groupInvoiceUpdate.status = 0;
        }
        if(data.groupInvoiceUpdate.status === (400 || 404) || data.groupInvoiceUpdate.status === (500 || 502)){
            alert("Fail with update group invoice!");
            data.groupInvoiceUpdate.status = 0;
        }

        if(data.groupInvoiceDelete.status === 200){
            alert("Successfully deleted this group invoice.");
            this.props.getAllGroupInvoiceAction();
            data.groupInvoiceDelete.status = 0;
        }
        if(data.groupInvoiceDelete.status === (400 || 404) || data.groupInvoiceDelete.status === (500 || 502)){
            alert("Fail with delete this group invoice!");
            data.groupInvoiceDelete.status = 0;
        }
    }

    handleBack(){
        this.props.initialize("form_add_group_invoice", null);
        this.setState({
            update: false,
            label: 'Save',
            labelForm: 'New Group Invoice'
        });
    }

    updateItem(data){
        this.setState({
            update: true,
            label: 'Edit',
            labelForm: 'Edit Group Invoice',
            groupId: data.id
        });
        this.props.dispatch(initialize('form_add_group_invoice', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this group invoice?") === true){
            this.props.deleteGroupInvoiceAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addGroupInvoiceAction({
                description: values.description,
                dueDay: Number(values.dueDay),
                groupName: values.groupName.trim()
            });
        }else{
            this.props.updateGroupInvoiceAction({
                id : this.state.groupId,
                description: values.description,
                dueDay: Number(values.dueDay),
                groupName: values.groupName.trim()
            });
        }
        this.handleBack();
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={7} sm={7} md={7} lg={7}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Group Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="groupName" type="text" component={TextBox} label="Group Name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Due Days <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="dueDay" type="text" component={TextBox} label="Due Days" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label !== "Edit" ? null :
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                            </Col>
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col lg={12}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Group Name</th>
                                    <th>Due Days</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.groupInvoiceAll.items !== undefined ?
                                    this.props.groupInvoiceAll.items.map((group,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{group.id}</td>
                                                <td>{group.groupName}</td>
                                                <td>{group.dueDay}</td>
                                                <td>{group.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs'
                                                       onClick={() => this.updateItem(group)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(group.id)}
                                                       className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

GroupInvoice = reduxForm({
    form: 'form_add_group_invoice',
    validate: (values) => {
        let regex_number = /[0-9]{1,4}/;
        const errors = {};
        if ( values.groupName === undefined) {
            errors.groupName = 'Group Name is required!';
        }
        if (!regex_number.test(values.dueDay) || values.dueDay === undefined) {
            errors.dueDay = 'Due Days accept only number!';
        }
        return errors;
    }
})(GroupInvoice);

function mapStateToProps(state) {
    //console.log("DATA : ",state.groupInvoice.groupInvoiceAll);
    return {
        groupInvoiceAdd: state.groupInvoice.groupInvoiceAdd,
        groupInvoiceAll :state.groupInvoice.groupInvoiceAll,
        groupInvoiceUpdate: state.groupInvoice.groupInvoiceUpdate,
        groupInvoiceDelete: state.groupInvoice.groupInvoiceDelete,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addGroupInvoiceAction, getAllGroupInvoiceAction, updateGroupInvoiceAction, deleteGroupInvoiceAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(GroupInvoice);