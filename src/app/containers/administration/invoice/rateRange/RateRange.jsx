import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addRateRangeAction, getAllRateRangesAction, updateRateRangeAction, deleteRateRangeAction } from './../../../../actions/setup/rateRange';

class RateRange extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            createDate: null,
            update: false,
            label: 'Save',
            labelForm: 'New Rate Range',
            rateId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.getAllRateRangesAction();
    }

    componentWillReceiveProps(data){
        if(data.addRateRange.status === 200){
            alert("Successfully added new Rate Range.");
            this.handleBack();
            this.props.getAllRateRangesAction();
            data.addRateRange.status = 0;
        }
        if(data.addRateRange.status === 401 || data.addRateRange.status === 500){
            alert("Fail with add Rate Range!");
        }

        if(data.updateRateRange.status === 200 ){
            alert("Successfully updated Rate Range.");
            this.handleBack();
            this.props.getAllRateRangesAction();
            data.updateRateRange.status = 0;
        }
        if(data.updateRateRange.status === 401 || data.updateRateRange.status === 404 || data.updateRateRange.status === 500){
            alert("Fail with update Rate Range!");
        }

        if(data.deleteRateRange.status === 200){
            alert("Successfully deleted this Rate Range.");
            this.props.getAllRateRangesAction();
            data.deleteRateRange.status = 0;
        }
        if(data.deleteRateRange.status === 401 || data.deleteRateRange.status === 404 || data.deleteRateRange.status === 500){
            alert("Fail with delete this Rate Range!");
        }
    }

    handleBack(){
        this.setState({update: false, label: 'Save', labelForm: 'New Rate Range'});
        this.props.dispatch(initialize('form_rate_range', null));
    }

    updateItem(data){
        console.log(data);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Rate Range', rateId: data.id});
        this.props.dispatch(initialize('form_rate_range', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this Rate Range?") === true){
            this.props.deleteRateRangeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addRateRangeAction({
                "startUsage": Number(values.startUsage),
                "endUsage": Number(values.endUsage),
                "rateRiel": Number(values.rateRiel),
                "rateUSD": Number(values.rateUSD)
            });
        }else{
            this.props.updateRateRangeAction({
                "id": this.state.rateId,
                "startUsage": Number(values.startUsage),
                "endUsage": Number(values.endUsage),
                "rateRiel": Number(values.rateRiel),
                "rateUSD": Number(values.rateUSD)
            });
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Start Usage <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="startUsage" type="text" component={TextBox} label="Start usage" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>End Usage <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="endUsage" type="text" component={TextBox} label="End usage" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Rate(RIEL) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="rateRiel" type="text" component={TextBox} label="Rate in RIEL currency" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Rate(USD) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="rateUSD" type="text" component={TextBox} label="Rate in USD currency" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={2} lg={2} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <table className="page-permision-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Start Usage</th>
                                <th>End Usage</th>
                                <th>Rate(RIEL)</th>
                                <th>Rate(USD)</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            { this.props.getAllRateRanges.status === 200 ?
                                <tbody>
                                {
                                    this.props.getAllRateRanges.data.items.map((rate,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{rate.id}</td>
                                                <td>{rate.startUsage}</td>
                                                <td>{rate.endUsage}</td>
                                                <td>{rate.rateRiel}</td>
                                                <td>{rate.rateUSD}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(rate)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(rate.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                                :
                                <tbody>
                                <tr>
                                    <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                </tr>
                                </tbody>
                            }
                        </table>
                    </Col>
                </Row>
            </div>
        )
    }

}

RateRange = reduxForm({
    form: 'form_rate_range',
    validate: (values) => {
        const errors = {};
        let regex_number = /[0-9]{1,10}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        if (!regex_number.test(values.startUsage) || values.startUsage === undefined) {
            errors.startUsage = 'Invalid start usage!';
        }
        if (!regex_number.test(values.endUsage) || values.endUsage === undefined) {
            errors.endUsage = 'Invalid end usage!';
        }
        if (!regex_price.test(values.rateRiel) || values.rateRiel === undefined) {
            errors.rateRiel = 'Invalid rate in riel currency!';
        }
        if (!regex_price.test(values.rateUSD) || values.rateUSD === undefined) {
            errors.rateUSD = 'Invalid rate in USD currency!';
        }
        return errors;
    }
})(RateRange);

function mapStateToProps(state) {
    // console.log("data  :", state.rateRanges.addRateRange);
    return {
        addRateRange: state.rateRanges.addRateRange,
        getAllRateRanges: state.rateRanges.getAllRateRanges,
        updateRateRange :state.rateRanges.updateRateRange,
        deleteRateRange: state.rateRanges.deleteRateRange
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addRateRangeAction, getAllRateRangesAction, updateRateRangeAction, deleteRateRangeAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(RateRange);