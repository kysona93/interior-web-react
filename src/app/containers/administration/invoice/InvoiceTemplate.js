import React from  'react';
import './index.css';

export default class InvoiceTemplate extends React.Component{
    render(){
        return(
            <div className="container">
                <div className="wrapp-conten">
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 header-invoice">
                            <div className="header-logo">
                                <img src="/icons/logo-bvc-header.png"/>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12">
                            <div className="col-xs-4 col-sm-4 barcode"><img src="/icons/barcode.png"/></div>
                            <div className="col-xs-8 col-sm-8 invoice-title"><img src="/icons/invoice-bvc.png"/></div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-6 col-sm-6">
                            <div className="table-cus-info">
                                <div className="cutomer-info">
                                    <div className="col-xs-6 col-sm-6"><p>វិក្កយ្របត្តិលេខ :</p></div><div className="col-xs-6 col-sm-6"><p>0000001</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>អត្រលេខ :</p></div><div className="col-xs-6 col-sm-6"><p>100</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះ អតិថិជន :</p></div><div className="col-xs-6 col-sm-6"><p>ហេង ម៉េងហុង</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>អាស័ដ្ធាន ទទួលវិក័យប័ត្រ:</p></div><div className="col-xs-6 col-sm-6 location"><p>ភូមិត្បូង ឃុំគរ ស្រុក ត្បូង ឃ្មុំ ខេតុ ត្បូង ឃ្មុំ</p></div>
                                </div>
                                <img src="/icons/customer-info-bvc.png"/>
                                <br/>
                            </div>
                        </div>

                        <div className="col-xs-6 col-sm-6 content-text">
                            <div className="col-xs-6 col-sm-6"><p>ទីតាំង ប្រើប្រាស់ចរន្ត :</p></div><div className="col-xs-6 col-sm-6"><p>T-0001</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញ វិក្កយបត្រ :</p></div><div className="col-xs-6 col-sm-6"><p>03/01/2017</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ចាប់ពីថ្ងៃ :</p></div><div className="col-xs-6 col-sm-6"><p>01/12/2017</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ដល់ ថ្ងៃទី  :</p></div><div className="col-xs-6 col-sm-6"><p>01/12/2017</p></div>
                        </div>
                    </div>
                    <div className="master-content">
                        <img src="/icons/b-account.jpg"/>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12">
                                <div className="col-xs-6 col-sm-6">
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះគណនី ធនាគា (ACCOUNT NAME)  :</p></div><div className="col-xs-6 col-sm-6"><p><b>BVC POWER DEVELOPMENT CO.,LTD</b></p></div>
                                    <div className="col-xs-6 col-sm-6"><p>លេខគណនី ធនាគា :</p></div><div className="col-xs-6 col-sm-6"><p><b>00083625</b></p></div>
                                </div>
                                <div className="col-xs-6 col-sm-6">
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះ ធនាគា (BANK NAME) :</p></div><div className="col-xs-6 col-sm-6"><p><b>ធនាគា ABA</b></p></div>
                                    <div className="col-xs-6 col-sm-6"><p>រូបិយប័ណ្ណ(CURRENCY) :</p></div><div className="col-xs-6 col-sm-6"><p><b>ដុល្លា</b></p></div>
                                </div>
                            </div>
                        </div>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12">
                                <table className="table invoice">
                                    <thead className="thead-inverse">
                                    <tr>
                                        <th>
                                            <p>នាឡិកាស្ទង់</p>
                                            <p>METER</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានចាស់</p>
                                            <p>PREVIOUS</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានថ្នី</p>
                                            <p>CURRENT</p>
                                        </th>
                                        <th>
                                            <p>មេគុណ</p>
                                            <p>MULTIPLIER</p>
                                        </th>
                                        <th>
                                            <p>ការប្រើប្រាស់ ថាមពល</p>
                                            <p>CONSUMPTION</p>
                                        </th>
                                        <th>
                                            <p>តម្លៃឯកតា</p>
                                            <p>RATE</p>
                                        </th>
                                        <th>
                                            <p>តម្លៃ សរុប</p>
                                            <p>VALUE</p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Balance brought forward</td>
                                        <td>1234567</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><p>Enalty for reactive energt</p></td>
                                        <td><p>123456</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>100001</p></td>
                                        <td><p>00000</p></td>
                                        <td><p>999999</p></td>
                                        <td><p>10</p></td>
                                        <td><p>99999</p></td>
                                        <td><p>0.10</p></td>
                                        <td><p>R  9999.99</p></td>
                                    </tr>
                                    <tr>
                                        <td><p>100001</p></td>
                                        <td><p>00000</p></td>
                                        <td><p>999999</p></td>
                                        <td><p>@10</p></td>
                                        <td><p>99999</p></td>
                                        <td><p>0.001</p></td>
                                        <td><p>R  9999.99</p></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div className="col-xs-12 col-sm-12 invoice">
                                    <center><h1 className="invoice">វិក្កយបត្រ INVOICE</h1></center>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 lase-usage">
                                <div className="panel panel-default usage">
                                    <div className="col-xs-12 col-sm-12 history-usage"><p>ប្រវត្តិនៃការ ប្រើប្រាស់ អគ្គិសនី រយះពេល ១២ ខែកន្លងមក</p></div>
                                    <div className="panel-body">
                                        <ul>
                                            <li>01/01/2017</li>
                                            <li>01/02/2017</li>
                                            <li>01/03/2017</li>
                                            <li>01/04/2017</li>
                                        </ul>
                                        <ul>
                                            <li>11 គ</li>
                                            <li>12 គ</li>
                                            <li>13 គ</li>
                                            <li>14 គ</li>
                                        </ul>
                                        <ul>
                                            <li>01/05/2017</li>
                                            <li>01/06/2017</li>
                                            <li>01/07/2017</li>
                                            <li>01/08/2017</li>
                                        </ul>
                                        <ul>
                                            <li>11 គ</li>
                                            <li>12 គ</li>
                                            <li>13 គ</li>
                                            <li>14 គ</li>
                                        </ul>
                                        <ul>
                                            <li>01/09/2017</li>
                                            <li>01/10/2017</li>
                                            <li>01/11/2017</li>
                                            <li>01/12/2017</li>
                                        </ul>
                                        <ul>
                                            <li>11 គ</li>
                                            <li>12 គ</li>
                                            <li>13 គ</li>
                                            <li>14 គ</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default usage">
                                    <div className="panel-body">
                                        <div className="col-xs-12 col-sm-12 note1"><p>ប្រសិនជាអ្នក ប្រើប្រាស់ មានចម្ងល់ ឬ ចង់ តវា សូមទំនាក់ទំនង ដោយផ្ទាល់ ជាមួយ ភ្នាក់ទទួល ខុសត្រូវ របស់សាខា។</p></div>
                                        <div className="col-xs-12 col-sm-12 note1"><p>អគ្គិសនីក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឺលូសិន ខបភើរេសិន រក្សាសិទ្ធិមិនដោះស្រាយជូនតាមរយះ ការបង្ហោះបញ្ហា
                                            លើប្រព័ន្ធសារ ពត៏មានសង្គម ជាលក្ខណៈ បុគ្គលនោះទេ​ ។</p></div>
                                        <div className="col-xs-12 col-sm-12 note1"><p>អ្នកចុះផ្សាយមិនមានការទទួលខុសត្រូវ ដែលប៉ះពាល់ដល់ កិត្តិយសរបស់ អគ្គិសនី អ៊ិនធើ ប៊ីភី សឺលូសិន ខបភើរេសិន
                                            អាចប្រឈមមុខជាមួយច្បាប់ដែលពាក់ពាន់ ជាធរមាន ។</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default price">
                                    <p>លុយដែល ត្រូវផាកខែមុន (Punished Money)</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default price1">
                                    <p>0</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default money">
                                    <p>ថ្ងៃ ផុត កំណត់ (Expiry Date)</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default money1">
                                    <p>14/05/2016</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="panel-body">
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; ហួសពីថ្ងៃផុតកំណត់បង់ប្រាក់ដែលបានកំណត់ នេះ ក្នុងករណី អតិថិជនមិនបានបង់ប្រាក់ អគ្គិសនី អ៊ិនធើ ប៊ីភី សឺលូសិន ខបភើរេសិន និងផ្អាក
                                            ការផ្គត់ថាមពលជូន</p></div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; ការផ្ជាប់ចរន្ត ជូនវិញ លុះត្រាតែអតិថិជន បានបង់ប្រាក់បំណុលសរុប ក្នុងវិក្កយបត្រនេះ​ និង​ថ្លៃផាកពិន័យ ក្នុងមួយថ្ងៃ ០.០៧ ភាគរយ រួចរាល់ហើយ</p></div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; សូមអញ្ជើញមក បង់ប្រាក់ នៅបេឡាអគ្គិសនី ឬ គណនីធនាគារបស់ ក្រុមហ៊ុន ។</p></div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; រាល់ការបង់ប្រាក់ តាមរយៈ ធនាគារ អតិធិជន គឺជាអ្នកទទួលខុសត្រូវ លើថ្លៃសេវា របស់ធនាគា ។</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>បំណល់សរុប (Amount)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p>R 4700.00</p></div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>ថ្ងៃបង់ប្រាក់ (Paid Date)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p>0</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <hr/>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <img src="/icons/barcode.png"/>
                                    <div className="panel-body">
                                        <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញ វិក្កយបត្រ </p></div><div className="col-xs-6 col-sm-6"><p>: 01/08/2017</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>វិក្កយបត្រលេខ </p></div><div className="col-xs-6 col-sm-6"><p>: 10000</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>ឈ្មោះ អតិថិជន </p></div><div className="col-xs-6 col-sm-6"><p>: ហេង ម៉េងហុង</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>អត្តលេខអតិថិជន</p></div><div className="col-xs-6 col-sm-6 location"><p>: 0001</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>បំណល់សរុប (Amount)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p>R 4700.00</p></div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>ថ្ងៃបង់ប្រាក់ (Paid Date)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        )
    }
}