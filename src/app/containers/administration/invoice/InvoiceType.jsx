import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from './../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { addInvoiceTypeAction, updateInvoiceTypeAction, deleteInvoiceTypeAction, getAllInvoiceTypeAction } from './../../../actions/invoice/invoiceType';

let invoiceId = 0;
class InvoiceType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Invoice Type'
        };
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentWillMount(){
        this.props.getAllInvoiceTypeAction();
    }

    componentWillReceiveProps(data){
        if(data.invoiceTypeAdd.status === 200){
            alert("Successfully added new invoice type.");
            this.props.getAllInvoiceTypeAction();
            data.invoiceTypeAdd.status = 0;
        }
        if(data.invoiceTypeAdd.status === 500){
            alert("Fail with add invoice type!");
            data.invoiceTypeAdd.status = 0;
        }

        if(data.invoiceTypeUpdate.status === 200){
            alert("Successfully updated invoice type.");
            this.setState({label: 'Save'});
            this.setState({labelForm: 'Add New invoice Type'});
            this.setState({update: false});
            this.props.getAllInvoiceTypeAction();
            data.invoiceTypeUpdate.status = 0;
        }
        if(data.invoiceTypeUpdate.status === 500){
            alert("Fail with update invoice type!");
            data.invoiceTypeUpdate.status = 0;
        }

        if(data.invoiceTypeDelete.status === 200){
            alert("Successfully deleted this invoice type.");
            this.props.getAllInvoiceTypeAction();
            data.invoiceTypeDelete.status = 0;
        }
        if(data.invoiceTypeDelete.status === 500){
            alert("Fail with delete this invoice type!");
            data.invoiceTypeDelete.status = 0;
        }
    }

    updateItem(data){
        invoiceId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Invoice Type'});
        this.props.dispatch(initialize('form_add_invoice_type', data)
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this invoice type?") === true){
            this.props.deleteInvoiceTypeAction(id);
        }
    }

    handleSubmit(values){
        this.setState({alert: true});
        if(!this.state.update){
            // add
            let invoiceType = {
                "invoiceType": values.invoiceType,
                "description": values.description

            };
            this.props.addInvoiceTypeAction(invoiceType);

        }else{
            // edit
            let invoiceType = {
                "id": invoiceId,
                "invoiceType": values.invoiceType,
                "description": values.description
            };
            this.props.updateInvoiceTypeAction(invoiceType);
        }
       this.props.initialize("form_add_invoice_type", null)
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Invoice Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="invoiceType" type="text" component={TextBox} label="Invoice type" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() =>  this.props.initialize("form_add_invoice_type", null)}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Invoice Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.invoiceTypeAll.status === 200 ?
                                        <tbody>
                                        { this.props.invoiceTypeAll.data.items.map((invoice,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{invoice.id}</td>
                                                    <td>{invoice.invoiceType}</td>
                                                    <td>{invoice.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(invoice)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(invoice.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

InvoiceType = reduxForm({
    form: 'form_add_invoice_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.invoiceType) || values.invoiceType === undefined) {
            errors.invoiceType = 'Invoice Type is required and at least 2 character!';
        }
        return errors;
    }
})(InvoiceType);

function mapStateToProps(state) {
    //console.log("DATA : ",state.invoiceType.invoiceTypeAll);
    return {
        invoiceTypeAll: state.invoiceType.invoiceTypeAll,
        invoiceTypeAdd: state.invoiceType.invoiceTypeAdd,
        invoiceTypeUpdate: state.invoiceType.invoiceTypeUpdate,
        invoiceTypeDelete: state.invoiceType.invoiceTypeDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addInvoiceTypeAction, updateInvoiceTypeAction, deleteInvoiceTypeAction, getAllInvoiceTypeAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InvoiceType);