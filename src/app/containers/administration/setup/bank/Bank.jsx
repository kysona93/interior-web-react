import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addBankAction, getAllBanksAction, updateBankAction, deleteBankAction } from './../../../../actions/setup/bank';

class Bank extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Bank',
            bankId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllBanksAction();
    }

    componentWillReceiveProps(data){
        if(data.bankAdd.status === 200){
            alert("Successfully added new Bank !!");
            this.handleCancel();
            this.props.getAllBanksAction();
        } else if(data.bankAdd.status === (400 || 404) || data.bankAdd.status === (500 || 502)){
            alert(data.bankAdd.status + " Error !! \nFail with add Bank !!\n" + data.bankAdd.data.message);
        } else if(data.bankAdd.status === 401){
            alert("Please Login !!")
        }

        if(data.bankUpdate.status === 200 ){
            alert("Successfully updated bank.");
            this.handleCancel();
            this.props.getAllBanksAction();
        } else if(data.bankUpdate.status === (400 || 404) || data.bankUpdate.status === (500 || 502)){
            alert(data.bankUpdate.status + " Error !! \nFail with add bank !!\n" + data.bankUpdate.message);
        } else if(data.bankUpdate.status === 401){
            alert("Please Login !!")
        }

        if(data.bankDelete.status === 200){
            alert("Successfully deleted Bank !!");
            this.props.getAllBanksAction();
        }else if(data.bankDelete.status === (400 || 404) || data.bankDelete.status === (500 || 502)){
            alert(data.bankDelete.status + " Error !! \nFail with deleted Bank !!\n" + data.bankDelete.message);
        } else if(data.bankDelete.status === 401){
            alert("Please Login !!")
        }
        data.bankAdd.status = 0;
        data.bankUpdate.status = 0;
        data.bankDelete.status = 0;
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Bank'});
        this.props.dispatch(initialize('form_add_bank', null));
    }


    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Bank', bankId: data.id});
        this.props.dispatch(initialize('form_add_bank', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this Bank?") === true){
            this.props.deleteBankAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addBankAction({
                bankNo: values.bankNo.trim(),
                bankNameEn: values.bankNameEn.trim(),
                bankNameKh: values.bankNameKh.trim(),
                currency: values.currency.trim()
            });
        }else{
            this.props.updateBankAction({
                id: Number(this.state.bankId),
                bankNo: values.bankNo.trim(),
                bankNameEn: values.bankNameEn.trim(),
                bankNameKh: values.bankNameKh.trim(),
                currency: values.currency.trim()
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Khmer Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="bankNameKh" type="text" component={TextBox} label="Khmer Name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>English Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="bankNameEn" type="text" component={TextBox} label="English Name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Bank No <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="bankNo" type="text" component={TextBox} label="Bank No"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Currency <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="currency" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 'Riel', name: 'Riel'}, {id: 'USD', name: 'USD'}]} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label === "Edit" ?
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Khmer Name</th>
                                    <th>English Name</th>
                                    <th>Bank No</th>
                                    <th>Currency</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                    <tbody>
                                    { this.props.bankAll.items !== undefined ?
                                        this.props.bankAll.items.length > 0 ?
                                            this.props.bankAll.items.map((bank,index)=>{
                                                return(
                                                    <tr key={index}>
                                                        <td>{bank.id}</td>
                                                        <td>{bank.bankNameKh}</td>
                                                        <td>{bank.bankNameEn}</td>
                                                        <td>{bank.bankNo}</td>
                                                        <td>{bank.currency}</td>
                                                        <td className="text-center">
                                                            <a className='btn btn-info btn-xs' onClick={() => this.updateItem(bank)}>
                                                                <span className="glyphicon glyphicon-edit"> </span>
                                                            </a>
                                                            &nbsp;
                                                            <a onClick={()=> this.deleteItem(bank.id)} className="btn btn-danger btn-xs">
                                                                <span className="glyphicon glyphicon-remove"> </span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                            </tr>
                                            :
                                        <tr>
                                            <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>

                                    }
                                    </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

Bank = reduxForm({
    form: 'form_add_bank',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,50}/;

        const errors = {};
        if (!regex_name.test(values.bankNameEn)) {
            errors.bankNameEn = 'Please input at least 2 character !!';
        }
        if (values.bankNameKh.length < 2) {
            errors.bankNameKh = 'Please input at least 2 character !!';
        }
        if (!regex_name.test(values.bankNo)) {
            errors.bankNo = 'Please input at least 2 character !!';
        }
        if (values.currency === "") {
            errors.currency = 'Please select at currency !!';
        }
        return errors;
    }
})(Bank);

function mapStateToProps(state) {
    return {
        bankAdd: state.bank.bankAdd,
        bankAll: state.bank.bankAll,
        bankUpdate: state.bank.bankUpdate,
        bankDelete: state.bank.bankDelete,
        initialValues: {
            bankNameKh: '',
            bankNameEn: '',
            bankNo: '',
            currency: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addBankAction, getAllBanksAction, updateBankAction, deleteBankAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Bank);