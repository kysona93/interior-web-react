import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm, change } from 'redux-form';
import { Row, Col, Table } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import '../../../../components/forms/Styles.css';
import EditPole from './../pole/Edit';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import Pagination from '../../../../components/forms/Pagination';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllAreasAction } from './../../../../actions/setup/area';
import { getAllTransformersAction } from './../../../../actions/setup/transformer';
import { getPolesAction, deletePoleAction } from './../../../../actions/setup/pole';
import moment from 'moment';

let pole = {
    area: 0,
    serial: "",
    limit: 10,
    page: 1,
    orderBy: "id",
    orderType: "desc"
};

class ListPole extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            pole: {},
            activePage: 1,
            areas: [],
            transformers: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handlePaging = this.handlePaging.bind(this);
        this.handleShowPage = this.handleShowPage.bind(this);
        this.handleSelectArea = this.handleSelectArea.bind(this);
        this.handleSelectTransformer = this.handleSelectTransformer.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    componentWillMount(){
        this.props.getPolesAction(pole);
        this.props.getAllAreasAction({
            area: '',
            serial: ''
        });
        this.props.getAllTransformersAction({
            check: 0,
            serial: ""
        });
    }

    componentWillReceiveProps(data){
        if(data.poles.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.poles.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.poleDelete.status === 200){
            alert("Successfully deleted Pole !!");
            this.props.getPolesAction(pole);
            this.props.getAllAreasAction({
                area: '',
                serial: ''
            });
            data.poleDelete.status = 0;
        } else if(data.poleDelete.status === (400 || 404) || data.poleDelete.status === (500 || 502)){
            alert(data.poleDelete.status + " Error !!\nFail !! Pole have not been deleted !!\n" + data.poleDelete.message);
            data.poleDelete.status = 0;
        } else if(data.poleDelete.status === 401){
            alert("Please login !!")
        }
        let areas = [];
        let transformers = [];
        if(data.areaAll.items !== undefined){
            data.areaAll.items.map((area) => {
                areas.push({
                    id: area.id,
                    name: area.areaNameEn
                })
            })
        } else areas = [];
        if(data.transformerAll.items !== undefined){
            data.transformerAll.items.map((trans) => {
                transformers.push({
                    id: trans.id,
                    name: trans.serial
                })
            })
        } else transformers = [];
        this.setState({areas: areas, transformers: transformers});
    }

    handleSelectArea(e){
        if(this.props.areaAll.items !== undefined){
            const area = this.props.areaAll.items.filter(area => area.id === Number(e.target.value))[0];
            if(area !== undefined){
                this.props.dispatch(change("form_filter_pole", "transformer", area.transId));
            } else this.props.dispatch(change("form_filter_pole", "transformer", null));
        }
    }

    handleSelectTransformer(e){
        if(this.props.transformerAll.items !== undefined){
            const trans = this.props.transformerAll.items.filter(trans => trans.id === Number(e.target.value))[0];
            if(trans !== undefined){
                this.props.dispatch(change("form_filter_pole", "area", trans.areaId));
            } else this.props.dispatch(change("form_filter_pole", "area", null));
        }
    }

    openUpdate(pole){
        this.setState({update: true, pole: pole})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getPolesAction(pole);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this pole from database?") === true){
            this.props.deletePoleAction(id);
        }
    }

    handleSubmit(value){
        pole = {
            area: value.area !== "" ? Number(value.area) : 0,
            serial: value.serial,
            limit: 10,
            page: 1,
            orderBy: "id",
            orderType: "desc"
        };
        this.props.getPolesAction(pole);
    }

    handleShowPage(e){
        pole.nowPage = 1;
        pole.pageSize = e.target.value !== '' ? Number(e.target.value) : 10;
        this.props.getPolesAction(pole);
    }

    handlePaging(eventKey) {
        this.setState({activePage: eventKey});
        pole.nowPage = eventKey;
        this.props.getPolesAction(pole);
    }

    static handleItem(total) {
        if (total <= pole.pageSize) {
            return 1
        } else if (total % pole.pageSize === 0) {
            return total / pole.pageSize
        } else if (total % pole.pageSize > 0) {
            return parseInt(total/pole.pageSize) + 1
        }
    }

    render(){
        const poles = this.props.poles.items;
        const {handleSubmit, error, submitting} = this.props;
        let total = 0;
        return (
            <div className="container-fluid">
                <h3 style={{color: '#1c91d0', marginTop: '-0.7%', fontWeight: 'bold'}}>POLE REPORT</h3>
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={12}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">AREA : </p></td>
                                            <td><Field name="area" type="select" onChange={this.handleSelectArea} component={SelectBox} placeholder="Please select ..." values={this.state.areas} sortBy="name" /></td>
                                            <td><p className="search-installer">TRANSFORMER : </p></td>
                                            <td><Field name="transformer" type="select" onChange={this.handleSelectTransformer} component={SelectBox} placeholder="Please select ..." values={this.state.transformers} sortBy="name" /></td>
                                            <td><p className="search-installer">POLE SERIAL : </p></td>
                                            <td><Field name="serial" type="text" component={TextBox} label="Pole serial"/></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>
                        <Table condensed bordered hover>
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Serial</th>
                                <th>Pole Type</th>
                                <th>Area/Transformer</th>
                                <th>Box(es)</th>
                                <th>Created Date</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th>Address</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { poles !== undefined ?
                                poles.list.length === 0 ?
                                    <tr>
                                        <td colSpan="10">
                                            <h3 style={{textAlign: 'center'}}>RESULT NOT FOUND!</h3>
                                        </td>
                                    </tr>
                                    :
                                    poles.list.map((pole,index)=>{
                                        total = poles.totalCount;
                                        return(
                                            <tr key={index} className="text-center">
                                                <td>{pole.id}</td>
                                                <td>{pole.serial}</td>
                                                <td>{pole.typeName}</td>
                                                <td>{pole.areaNameEn + '/' + pole.transSerial}</td>
                                                <td>{pole.boxes}</td>
                                                <td>{moment(pole.created).format("YYYY-MM-DD")}</td>
                                                <td>{pole.latitude}</td>
                                                <td>{pole.longitude}</td>
                                                <td style={{textAlign: 'left'}}>{pole.villageKh + ", " + pole.communeKh + ", " + pole.districtKh + ", " + pole.provinceKh}</td>
                                                <td>
                                                    <a onClick={() => this.openUpdate(pole)} className='btn btn-info btn-xs'>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={() => this.handleDelete(pole.id)} className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })
                                :
                                <tr>
                                    <td colSpan="10">
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        <Pagination
                            totalCount={total}
                            items={pole}
                            callBackAction={this.props.getPolesAction}
                        />
                    </Row>
                    :
                    <EditPole closeUpdate={this.closeUpdate} areas={this.props.areaAll.items} pole={this.state.pole} />
                }
            </div>
        )
    }
}

ListPole = reduxForm({
    form: 'form_filter_pole'
})(ListPole);

function mapStateToProps(state) {
    return {
        areaAll: state.area.areaAll,
        transformerAll: state.transformer.transformerAll,
        poles: state.pole.poles,
        poleDelete: state.pole.poleDelete,
        initialValues: {
            area: '',
            serial: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllAreasAction, getAllTransformersAction, getPolesAction, deletePoleAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPole);