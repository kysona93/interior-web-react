import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../../actions/location';
import { getAllPoleTypesAction } from './../../../../actions/setup/type/poleType';
import { updatePoleAction } from './../../../../actions/setup/pole';
import moment from 'moment';

class EditPole extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            created: this.props.pole.created !== null ? moment(this.props.pole.created) : null,
            expired: this.props.pole.expired !== null ? moment(this.props.pole.expired) : null,
            areas: [],
            types: [],
            provinces: [],
            districts: [],
            communes: [],
            villages: []
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({created: date});
    }

    handleExpireDate(date) {
        this.setState({expired: date});
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSubmit(value){
        this.props.updatePoleAction({
            createdDate: value.created || this.state.created,
            description: value.description,
            expireDate: value.expired || this.state.expired,
            height: value.height,
            id: Number(this.props.pole.id),
            locationId: Number(value.village),
            poleTypeId: Number(value.typeId),
            serial: value.serial.trim(),
            areaId: Number(value.areaId),
            x: Number(value.latitude),
            y: Number(value.longitude)
        });
    }

    componentWillMount(){
        this.props.getProvincesAction();
        this.props.getDistrictsAction(this.props.pole !== undefined ? this.props.pole.province : 0);
        this.props.getCommunesAction(this.props.pole !== undefined ? this.props.pole.district : 0);
        this.props.getVillagesAction(this.props.pole !== undefined ? this.props.pole.commune : 0);
        this.props.getAllPoleTypesAction();
    }

    componentDidMount(){
        this.props.dispatch(initialize('form_edit_pole', this.props.pole));
        this.props.dispatch(change('form_edit_pole', 'country', "Cambodia"));
    }

    componentWillReceiveProps(data){
        let areas = [],
            types = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [];
        if(this.props.areas !== undefined){
            this.props.areas.map((area) => {
                areas.push({
                    id: area.id,
                    name: area.areaNameEn
                })
            })
        } else {areas = [];}
        if(data.poleTypeAll.items !== undefined){
            data.poleTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.poleType
                })
            })
        } else {types = [];}
        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];
        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];
        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];
        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];
        this.setState({
            areas: areas,
            types: types,
            provinces: provinces,
            districts: districts,
            communes: communes,
            villages: villages
        });

        if(data.poleUpdate.status === 200){
            alert("Successful update Pole !!");
            this.closeUpdate();
        } else if(data.poleUpdate.status === (400 || 404) || data.poleUpdate.status === (500 || 502)){
            alert(data.poleUpdate.status + " Error !!\nFail with update Pole !! \n" + data.poleUpdate.message);
            this.closeUpdate();
        } else if(data.poleUpdate.status === 401){
            alert("Please login !!")
        }
        data.poleUpdate.status = 0;
    }

    closeUpdate(){
        this.props.closeUpdate(false);
        this.props.dispatch(initialize('form_edit_pole', null));
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Edit Pole</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Area <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="areaId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.areas} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.types} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Height </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="height" type="text" component={TextBox} label="Pole height" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Created Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="created" component={DateTimePicker} placeholder="Created date" defaultDate={this.state.created} handleChange={this.handleCreatedDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Expire Date </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="expired" component={DateTimePicker} placeholder="Expire date" defaultDate={this.state.expired} handleChange={this.handleExpireDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Latitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="latitude" type="text" component={TextBox} label="Latitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Longitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="longitude" type="text" component={TextBox} label="Longitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Country</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="country" type="text" component={TextBox} label="Country" disabled={true} icon="fa fa-map-marker" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Province <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>District <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Commune <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Village <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="village" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={7} lgOffset={7} md={2} lg={2}>
                                    <Button bsStyle="primary" style={{width: '115%'}} onClick={this.closeUpdate}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                </Col>
                                <Col md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
EditPole = reduxForm({
    form: 'form_edit_pole',
    validate: (values) => {
        const errors = {};
        if (values.serial.length < 2) {
            errors.serial = 'Please input at least 2 characters !!'
        }
        if (values.areaId === undefined || values.area === "" ) {
            errors.areaId = "Please select area !!";
        }
        if (values.typeId === undefined || values.typeId === "" ) {
            errors.typeId = "Please select pole type !!";
        }
        if (values.province === "0" || values.province === "" ) {
            errors.province = "Please select province !!";
        }
        if (values.district === "0" || values.district === "" ) {
            errors.district = "Please select district !!";
        }
        if (values.commune === "0" || values.commune === "" ) {
            errors.commune = "Please select commune !!";
        }
        if (values.village === "0" || values.village === "" ) {
            errors.village = "Please select village !!";
        }
        if (values.created === undefined || values.created === "") {
            errors.created = 'Please input created date !!'
        }
        return errors
    },
})(EditPole);

function mapStateToProps(state) {
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        poleTypeAll: state.poleType.poleTypeAll,
        poleUpdate: state.pole.poleUpdate,
        initialValues: {
            country: 1,
            province: '0',
            district: '0',
            commune: '0',
            village: '0',
            serial: '',
            areaId: '',
            typeId: '',
            height: '',
            latitude: '0',
            longitude: '0',
            description: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction,
        getAllPoleTypesAction,
        updatePoleAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPole);