import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../../actions/location';
import { getAllAreasAction } from './../../../../actions/setup/area';
import { getAllPoleTypesAction } from './../../../../actions/setup/type/poleType';
import { addPoleAction } from './../../../../actions/setup/pole';

class AddPole extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createdDate: null,
            expireDate: null,
            areas: [],
            types: [],
            provinces: [],
            districts: [],
            communes: [],
            villages: []
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({createdDate: date});
    }

    handleExpireDate(date) {
        this.setState({expireDate: date});
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSubmit(value){
        this.props.addPoleAction({
            createdDate: value.createdDate,
            description: value.description.trim(),
            expireDate: value.expireDate || null,
            height: value.height.trim(),
            id: 0,
            locationId: Number(value.village),
            poleTypeId: Number(value.typeId),
            serial: value.serial.trim(),
            areaId: Number(value.area),
            x: Number(value.latitude),
            y: Number(value.longitude)
        });
    }

    componentWillMount(){
        this.props.getProvincesAction();
        this.props.getAllAreasAction({
            area: '',
            serial: ''
        });
        this.props.getAllPoleTypesAction();
    }

    componentWillReceiveProps(data){
        let areas = [],
            types = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [];
        if(data.areaAll.items !== undefined){
            data.areaAll.items.map((area) => {
                areas.push({
                    id: area.id,
                    name: area.areaNameEn
                })
            })
        }else {areas = [];}
        if(data.poleTypeAll.items !== undefined){
            data.poleTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.poleType
                })
            })
        }else {types = [];}
        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];
        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];
        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];
        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];
        this.setState({
            areas: areas,
            types: types,
            provinces: provinces,
            districts: districts,
            communes: communes,
            villages: villages
        });

        if(data.poleAdd.status === 200){
            alert("Successful add new Pole !!");
            this.props.dispatch(initialize('form_add_pole', this.props.pole));
            data.poleAdd.status = 0;
        } else if(data.poleAdd.status === (400 || 404) || data.poleAdd.status === (500 || 502)){
            alert(data.poleAdd.status + " Error !!\nFail with add new Pole !! \n" + data.poleAdd.message);
            data.poleAdd.status = 0;
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;

        return (
            <div className="col-md-12">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Pole</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Area (Transformer) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="area" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.areas} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.types} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Height </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="height" type="text" component={TextBox} label="Pole height" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Created Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="createdDate" component={DateTimePicker} placeholder="Created date" defaultDate={this.state.createdDate} handleChange={this.handleCreatedDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Expire Date </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="expireDate" component={DateTimePicker} placeholder="Expire date" defaultDate={this.state.expireDate} handleChange={this.handleExpireDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Latitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="latitude" type="text" component={TextBox} label="Latitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Longitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="longitude" type="text" component={TextBox} label="Longitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Country</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="country" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Cambodia"}]} disabled={true} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Province <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>District <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Commune <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Village <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="village" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

AddPole  = reduxForm({
    form: 'form_add_pole',
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{2,100}/;
        const errors = {};
        if (!regex_text.test(values.serial)) {
            errors.serial = 'Please input at least 2 characters !!'
        }
        if (values.area === (undefined || "") ) {
            errors.area = "Please select transformer !!";
        }
        if (values.typeId === (undefined || "")) {
            errors.typeId = "Please select pole type !!";
        }
        if (values.province === ("0" || "")) {
            errors.province = "Please select province !!";
        }
        if (values.district === ("0" || "")) {
            errors.district = "Please select district !!";
        }
        if (values.commune === "0" || values.commune === "" ) {
            errors.commune = "Please select commune !!";
        }
        if (values.village === "0" || values.village === "" ) {
            errors.village = "Please select village !!";
        }
        if (values.createdDate === undefined) {
            errors.createdDate = 'Please input created date !!'
        }
        return errors
    },
})(AddPole);

function mapStateToProps(state) {
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        areaAll: state.area.areaAll,
        poleTypeAll: state.poleType.poleTypeAll,
        poleAdd: state.pole.poleAdd,
        initialValues: {
            country: '1',
            province: '0',
            district: '0',
            commune: '0',
            village: '0',
            serial: '',
            area: '',
            typeId: '',
            height: '',
            latitude: '0',
            longitude: '0',
            description: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction,
        getAllAreasAction,
        getAllPoleTypesAction,
        addPoleAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPole);