import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';

import { getAllLocationsAction, addLocationAction, updateLocationAction, deleteLocationAction } from '../../../../actions/location';
import LocationList from './List';
import './Styles.css';

let provinces = [];
let districts = [];
let communes = [];
let villages = [];
class AddLocation extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            types:[
                {id: 1, name: "Country"},
                {id: 2, name: "Province"},
                {id: 3, name: "District"},
                {id: 4, name: "Commune"},
                {id: 5, name: "Village"},
            ],
            selectCountry: true,
            selectProvince: true,
            selectDistrict: true,
            selectCommune: true,
            province: "",
            check: 1
        };
        this.handleSelectType = this.handleSelectType.bind(this);
        this.handleSelectCountry = this.handleSelectCountry.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
    }

    handleSelectType(event){
        this.props.dispatch(reset('form_add_location'));
        provinces = [];
        districts = [];
        communes = [];
        villages = [];
        const type = Number(event.target.value);
        if(type === 2){
            this.setState({
                selectCountry: false,
                selectProvince: true,
                selectDistrict: true,
                selectCommune: true
            });
        }else if(type === 3){
            this.setState({
                selectCountry: false,
                selectProvince: false,
                selectDistrict: true,
                selectCommune: true
            });
        }else if(type === 4){
            this.setState({
                selectCountry: false,
                selectProvince: false,
                selectDistrict: false,
                selectCommune: true
            });
        }else if(type === 5){
            this.setState({
                selectCountry: false,
                selectProvince: false,
                selectDistrict: false,
                selectCommune: false
            });
        }else{
            this.setState({
                selectCountry: true,
                selectProvince: true,
                selectDistrict: true,
                selectCommune: true
            });
        }
    }

    handleSelectCountry(event){
        this.setState({check: 2});
        if(this.props.locations.items != undefined){
            const locations = this.props.locations.items.filter(location => location.country == event.target.value && location.province != 0 && location.district == 0 && location.commune == 0 && location.village == 0);
            for(let location of locations){
                provinces.push({
                    id: location.province,
                    name: location.name
                })
            }
        }else {
            provinces = [];
        }
    }

    handleSelectProvince(event){
        this.setState({check: 3});
        this.setState({province: event.target.value});
        districts = [];
        if(this.props.locations.items != undefined){
            const locations = this.props.locations.items.filter(location => location.province == event.target.value && location.district != 0 && location.commune == 0 && location.village == 0);
            for(let location of locations){
                districts.push({
                    id: location.district,
                    name: location.name
                })
            }
        }else {
            districts = [];
        }
    }

    handleSelectDistrict(event){
        this.setState({check: 4});
        communes = [];
        if(this.props.locations.items !== undefined){
            const locations = this.props.locations.items.filter(location => location.province === this.state.province && location.district === event.target.value && location.commune !== 0 && location.village === 0);
            for(let location of locations){
                communes.push({
                    id: location.commune,
                    name: location.name
                })
            }
        } else {
            communes = [];
        }
    }

    handleSelectCommune(event){
        this.setState({check: 5});
        if(this.props.locations.items !== undefined){
            villages = [];
            const locations = this.props.locations.items.filter(location => location.province === this.state.province && location.commune === event.target.value && location.village !== 0);
            console.log("L : " + typeof locations !== "objects");
            if(locations.length > 0){
                for(let location of locations){
                    villages.push({
                        id: location.village,
                        name: location.name
                    })
                }
            }
        }else {
            villages = [];
        }
    }

    handleSubmit(value){
        let location = {
            nameKh: value.nameKh.trim(),
            name: value.nameEn.trim(),
            locationTypeId: Number(value.locationTypeId),
            country: Number(1),
            province: Number(value.province),
            district: Number(value.district),
            commune: Number(value.commune),
            village: Number(value.village)
        };

        if(this.state.check === 2){
            location.province = Number(provinces.length + 1);
        } else if(this.state.check  === 3){
            location.district = Number(districts.length + 1);
        } else if(this.state.check  === 4){
            location.commune = Number(communes.length + 1);
        } else if(this.state.check  === 5){
            location.village = Number(villages.length + 1);
        } else {
            location = {
                nameKh: value.nameKh.trim(),
                name: value.nameEn.trim(),
                locationTypeId: Number(value.locationTypeId),
                country: Number(1),
                province: Number(value.province),
                district: Number(value.district),
                commune: Number(value.commune),
                village: Number(value.village)
            };
        }

        this.props.addLocationAction(location);
        this.props.dispatch(reset('form_add_location'));
    }

    componentWillMount(){
        this.props.getAllLocationsAction();
    }

    componentWillReceiveProps(data){
        if(data.locationAdd.status === 200){
            alert("Successful add new Location !!");
            this.props.getAllLocationsAction();
            data.locationAdd.status = 0;
        } else if(data.locationAdd.status === (400 || 404) || data.locationAdd.status === (500 || 502)){
            alert(data.locationAdd.status + " Error !!\nFail with add new Location !! \n" + data.locationAdd.data.message);
            data.locationAdd.status = 0;
        }
    }

    render() {
        const {handleSubmit, error, invalid, submitting} = this.props;
        const getData = () => {
            if(this.state.check === 2){
                return provinces;
            } else if(this.state.check  === 3){
                return districts;
            } else if(this.state.check  === 4){
                return communes;
            } else if(this.state.check  === 5){
                return villages;
            } else {
                return [];
            }
        };

        return (
            <div className="container-fluid">
                <Row>
                    <Col lg={6}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Type <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="locationTypeId" type="select" onChange={this.handleSelectType} component={SelectBox} placeholder="Please select ..." values={this.state.types} sortBy="name" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Name (Khmer) <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="nameKh" type="text" component={TextBox} label="ឈ្មោះ ..." icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Name (English) <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="nameEn" type="text" component={TextBox} label="Name ..." icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Country</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="country" type="select" onChange={this.handleSelectCountry} component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Cambodia"}]} disabled={this.state.selectCountry} sortBy="name" icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Province</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={provinces} disabled={this.state.selectProvince} sortBy="name" icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>District</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={districts} disabled={this.state.selectDistrict} sortBy="name" icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Commune</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={communes} disabled={this.state.selectCommune} sortBy="name" icon="fa fa-map-marker"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={4} lg={4}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </Col>
                   <Col lg={6}>
                        <div style={{marginTop: '-10px'}}>
                            <div className="m-title-header custom-header">
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <h4 className="product-title">ទីតាំងដែលមានរួចហើយ / Locations existed</h4>
                                </div>
                                <hr className="line-title" style={{ clear:"both" }}/>
                            </div>
                            <div className="wrap-location">
                                <div className="row row-location">
                                    { getData()[0] == null || getData()[0] == undefined ? null :
                                        getData().map((location, index) => {
                                            return(
                                                <div key={index} className="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                                    <ul>
                                                        <li style={{ fontFamily: 'khmer', fontWeight: 'normal',fontSize: '100%',lineHeight: 1.5}}>&nbsp;{location.name}</li>
                                                    </ul>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <LocationList locations={this.props.locations.items !== undefined ? this.props.locations.items : null} />
                    </Col>
                </Row>
            </div>
        )
    }
}
const FormAddLocation = reduxForm({
    form: 'form_add_location',
    validate: (values) => {
        const errors = {};
        if (values.nameKh.length < 2) {
            errors.nameKh = 'Please enter at least 4 characters !!'
        }
        if (values.nameEn.length < 2) {
            errors.nameEn = 'Please enter at least 4 characters !!'
        }

        if (values.locationTypeId == undefined) {
            errors.locationTypeId = "Please select your location type !!";
        }

        return errors
    },
})(AddLocation);

function mapStateToProps(state) {
    return({
        locations: state.location.locationAll,
        locationAdd: state.location.locationAdd,
        initialValues: {
            nameKh: '',
            nameEn: '',
            country: '0',
            province: '0',
            district: '0',
            commune: '0',
            village: '0'
        }
    })
}

function mapDispatchToProps(dispatch) {

    return bindActionCreators({getAllLocationsAction, addLocationAction, updateLocationAction, deleteLocationAction}, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(FormAddLocation);