import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Styles.css';
import {updateLocationAction, deleteLocationAction} from '../../../../actions/location';

function onRowSelect(row, isSelected) {
    //console.log(row);
   // console.log(`selected: ${isSelected}`);
}

function onSelectAll(isSelected) {
    //console.log(`is select all: ${isSelected}`);
}

function onAfterTableComplete() {
    //console.log('Table render complete.');
}


function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
function nameValidator(value) {
    if (!value) {
        return 'Job Name is required!';
    } else if (value.length < 2) {
        return 'Job Name length must great 3 char';
    }
    return true;
}

class LocationList extends React.Component {
    constructor(props){
        super(props);
        this.onAfterSaveCell = this.onAfterSaveCell.bind(this);
        this.onDeleteRow = this.onDeleteRow.bind(this);

    }

    onAfterSaveCell(row, cellName, cellValue) {
        this.props.updateLocationAction({
            id: Number(row.id),
            nameKh: row.nameKh.trim(),
            name: row.name.trim(),
            locationTypeId: Number(row.typeId),
            country: Number(1),
            province: Number(row.province),
            district: Number(row.district),
            commune: Number(row.commune),
            village: Number(row.village)
        });
    }

    onDeleteRow(rows) {
        for (let i = 0; i < rows.length; i++) {
            this.props.deleteLocationAction({id: rows[i]})
        }
    }

    componentWillReceiveProps(data){
        if(data.locationDelete.status === 200){
            location.href = '/app/locations';
        }
    }

    render() {
        return (
            <BootstrapTable
                data={this.props.locations} //list of records
                trClassName={trClassNameFormat}  //apply style on cell
                headerStyle={{background: "#d5dee5"}}
                selectRow={{
                    mode: 'checkbox',
                    clickToSelect: true,
                    selected: [],
                    bgColor: 'rgb(238, 193, 213)',
                    onSelect: onRowSelect,
                    onSelectAll: onSelectAll
                }} //apply function to select record
                cellEdit={{
                    mode: 'click',
                    blurToSave: true,
                    afterSaveCell: this.onAfterSaveCell
                }} // apply function to edit cell
                options={{
                    paginationShowsTotal: true,
                    sortName: 'id',  // default sort column name
                    sortOrder: 'desc',  // default sort order
                    afterTableComplete: onAfterTableComplete, // A hook for after table render complete.
                    onDeleteRow: this.onDeleteRow
                }} //
                //deleteRow // apply button delete on the top right
                search // apply text-box search on the top right
                hover // apply hover style on row
                pagination // apply pagination
            >
                <TableHeaderColumn dataField='id' width="7%" dataAlign='center' dataSort isKey autoValue>ID</TableHeaderColumn>
                <TableHeaderColumn dataField='typeName' width="10%" className='good' dataSort >TYPE</TableHeaderColumn>
                <TableHeaderColumn dataField='nameKh' width="14%" className='good' dataSort editable={ { type: 'textarea', validator: nameValidator } }>ឈ្មោះ</TableHeaderColumn>
                <TableHeaderColumn dataField='name' width="14%" className='good' dataSort editable={ { type: 'textarea', validator: nameValidator } }>NAME</TableHeaderColumn>
                <TableHeaderColumn dataField='country' width="10%" dataAlign='center'>COUNTRY</TableHeaderColumn>
                <TableHeaderColumn dataField='province' width="10%" dataAlign='center'>PROVINCE</TableHeaderColumn>
                <TableHeaderColumn dataField='district' width="10%" dataAlign='center'>DISTRICT</TableHeaderColumn>
                <TableHeaderColumn dataField='commune' width="10%" dataAlign='center'>COMMUNE</TableHeaderColumn>
                <TableHeaderColumn dataField='village' width="10%" dataAlign='center'>VILLAGE</TableHeaderColumn>
            </BootstrapTable>
        );
    }
}

function mapStateToProps(state) {
    return({
        locationUpdate: state.location.locationUpdate,
        locationDelete: state.location.locationDelete
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({updateLocationAction, deleteLocationAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationList);