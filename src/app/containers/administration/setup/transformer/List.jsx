import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { Row, Col, Table } from 'react-bootstrap';
import { TextBox } from '../../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import EditTransformer from './Edit';
import { getAllTransformersAction, deleteTransformerAction } from './../../../../actions/setup/transformer';

class ListTransformer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            transformer: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    componentWillMount(){
        this.props.getAllTransformersAction({
            check: 0,
            serial: ""
        });
    }

    openUpdate(transformer){
        this.setState({update: true, transformer: transformer})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getAllTransformersAction({
            check: 0,
            serial: ""
        });
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this Transformer?") === true){
            this.props.deleteTransformerAction(id);
        }
    }

    componentWillReceiveProps(data){

        if(data.transformerAll.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.transformerAll.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.transformerDelete.status === 200){
            alert("Transformer have been deleted !!");
            this.props.getAllTransformersAction({
                check: 0,
                serial: ""
            });
        } else if(data.transformerDelete.status === (400 && 404) || data.transformerDelete.status === (500 && 502)){
            alert(data.transformerDelete.status + " Error !! \nFail !! Transformer have not been deleted !!\n" + data.transformerDelete.message);
        } else if(data.transformerDelete.status === 401){
            alert("Please login !!");
        }
        data.transformerDelete.status = 0;
    }

    handleSubmit(value){
        this.props.getAllTransformersAction({
            check: 0,
            serial: value.serial.trim()
        });
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        <h3 style={{ color: '#1c91d0', position: 'absolute', marginTop: '0.7%', fontWeight: 'bold'}}>TRANSFORMER REPORT</h3>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={5} lg={7}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">TRANSFORMER SERIAL : </p></td>
                                            <td><Field name="serial" type="text" component={TextBox} label="Transformer serial"/></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>
                        <Table responsive condensed bordered hover>
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Transformer</th>
                                <th>Type</th>
                                <th>Ratio of Voltage</th>
                                <th>Latitude</th>
                                <th>Longitude</th>
                                <th>Address</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.transformerAll.items !== undefined ?
                                this.props.transformerAll.items.map((trans, index)=>{
                                    return(
                                        <tr key={index} className="text-center">
                                            <td>{trans.id}</td>
                                            <td>{trans.serial}</td>
                                            <td>{trans.typeName}</td>
                                            <td>{trans.kv}</td>
                                            <td>{trans.latitude}</td>
                                            <td>{trans.longitude}</td>
                                            <td style={{textAlign: 'left'}}>{trans.villageKh + ", " + trans.communeKh + ", " + trans.districtKh + ", " + trans.provinceKh}</td>
                                            <td>
                                                <a onClick={() => this.openUpdate(trans)} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>
                                                &nbsp;
                                                <a onClick={() => this.handleDelete(trans.id)} className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })
                                    :
                                <tr>
                                    <td colSpan="9" className="text-center">
                                        <h3>RESULT NOT FOUND!</h3>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                    </Row>
                    :
                    <EditTransformer closeUpdate={this.closeUpdate} transformer={this.state.transformer} />
                }
            </div>
        )
    }
}

ListTransformer = reduxForm({
    form: 'form_filter_transformer'
})(ListTransformer);

function mapStateToProps(state) {
    return {
        transformerAll: state.transformer.transformerAll,
        transformerDelete: state.transformer.transformerDelete,
        initialValues: {
            area: '',
            serial: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllTransformersAction, deleteTransformerAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTransformer);