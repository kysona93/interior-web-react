import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../../actions/location';
import { getAllTransformersTypesAction } from './../../../../actions/setup/type/transformerType';
import { addTransformerAction } from './../../../../actions/setup/transformer';

class AddTransformer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createdDate: null,
            expireDate: null,
            types: [],
            provinces: [],
            districts: [],
            communes: [],
            villages: []
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({createdDate: date});
    }

    handleExpireDate(date) {
        this.setState({expireDate: date});
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSubmit(value){
        this.props.addTransformerAction({
            address: value.address.trim(),
            cg: value.cg.trim(),
            createdDate: value.createdDate,
            description: value.description.trim(),
            expireDate: value.expireDate || null,
            id: 0,
            impedance: value.impedance.trim(),
            kv: value.kv.trim(),
            locationId: Number(value.village),
            manufacturer: value.manufacturer.trim(),
            noLoadCurrent: value.noLoadCurrent.trim(),
            noLoadLoss: value.noLoadLoss.trim(),
            serial: value.serial.trim(),
            transformerDimension: value.dimension.trim(),
            transformerTypeId: Number(value.transformerType),
            transformerWeight: value.transformerWeight.trim(),
            wc: value.wc.trim(),
            weightActivePart: value.activePart.trim(),
            x: Number(value.latitude),
            y: Number(value.longitude)
        });
    }

    componentWillMount(){
        this.props.getProvincesAction();
        this.props.getAllTransformersTypesAction();
    }

    componentWillReceiveProps(data){
        let types = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [];
        if(data.transformerTypeAll.items !== undefined){
            data.transformerTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.transformerType
                })
            })
        } else types = [];
        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];
        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];
        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];
        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];
        this.setState({
            types: types,
            provinces: provinces,
            districts: districts,
            communes: communes,
            villages: villages
        });

        if(data.transformerAdd.status === 200){
            alert("Successful add new Transformer !");
            this.props.dispatch(initialize('form_add_transformer', null));
            data.transformerAdd.status = 0;
        } else if(data.transformerAdd.status === (400 || 404) || data.transformerAdd.status === (500 || 502)){
            alert(data.transformerAdd.status + " Error !!\nFail with add new Transformer !! \n" + data.transformerAdd.message);
            data.transformerAdd.status = 0;
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="col-md-12">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Transformer</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="transformerType" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.types} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Weight active part </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="activePart" type="text" component={TextBox} label="Weight active part" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Weight </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="transformerWeight" type="text" component={TextBox} label="Transformer weight" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Dimension </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="dimension" type="text" component={TextBox} label="Transformer dimension" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>KV </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="kv" type="text" component={TextBox} label="KV" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>CG </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="cg" type="text" component={TextBox} label="CG" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>WC </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="wc" type="text" component={TextBox} label="WC" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>No.Load Current </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="noLoadCurrent" type="text" component={TextBox} label="No. load current" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>No.Load Loss </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="noLoadLoss" type="text" component={TextBox} label="No. load loss" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Manufacturer </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="manufacturer" type="text" component={TextBox} label="Manufacturer" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Impedance </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="impedance" type="text" component={TextBox} label="Impedance" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Country</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="country" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Cambodia"}]} disabled={true} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Province <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>District <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Commune <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Village <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="village" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Latitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="latitude" type="text" component={TextBox} label="Latitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Longitude </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="longitude" type="text" component={TextBox} label="Longitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Created Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="createdDate" component={DateTimePicker} placeholder="Created date" defaultDate={this.state.createdDate} handleChange={this.handleCreatedDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Expire Date </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="expireDate" component={DateTimePicker} placeholder="Expire date" defaultDate={this.state.expireDate} handleChange={this.handleExpireDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Address </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="address" type="text" component={TextArea} label="Address" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

const FormAddTransformer = reduxForm({
    form: 'form_add_transformer',
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{1,100}/;

        const errors = {};
        if (!regex_text.test(values.serial)) {
            errors.serial = 'Please input at least 1 character !!'
        }
        if (values.transformerType === "") {
            errors.transformerType = "Please select transformer type !!";
        }
        if (values.province === ("0" || "")) {
            errors.province = "Please select province !!";
        }
        if (values.district === ("0" || "")) {
            errors.district = "Please select district !!";
        }
        if (values.commune === ("0" || "")) {
            errors.commune = "Please select district !!";
        }
        if (values.village === ("0" || "")) {
            errors.village = "Please select village !!";
        }
        if (values.createdDate === (undefined || "" || null)) {
            errors.createdDate = 'Please input created date !!'
        }

        return errors
    },
})(AddTransformer);

function mapStateToProps(state) {
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        transformerTypeAll: state.transformerType.transformerTypeAll,
        transformerAdd: state.transformer.transformerAdd,
        initialValues: {
            nameKh: '',
            nameEn: '',
            country: '1',
            province: '0',
            district: '0',
            commune: '0',
            village: '0',
            address: "",
            cg: "",
            description: "",
            dimension: "",
            impedance: "",
            kv: "",
            locationId: 1,
            manufacturer: "",
            noLoadCurrent: "",
            noLoadLoss: "",
            serial: "",
            transformerType: "",
            transformerWeight: "",
            wc: "",
            activePart: "",
            latitude: '0',
            longitude: '0'
        }
    })
}

function mapDispatchToProps(dispatch) {

    return bindActionCreators({
        getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction,
        getAllTransformersTypesAction,
        addTransformerAction
    }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(FormAddTransformer);