import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../../actions/location';
import { getAllTransformersTypesAction } from './../../../../actions/setup/type/transformerType';
import { updateTransformerAction } from './../../../../actions/setup/transformer';
import moment from 'moment';

class EditTransformer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            created: this.props.transformer.created !== null ? moment(this.props.transformer.created) : null,
            expired: this.props.transformer.expired !== null ? moment(this.props.transformer.expired) : null,
            types: [],
            provinces: [],
            districts: [],
            communes: [],
            villages: []
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({created: date});
    }

    handleExpireDate(date) {
        this.setState({expired: date});
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSubmit(value){
        this.props.updateTransformerAction({
            address: value.address,
            cg: value.cg,
            createdDate: value.created || this.state.created,
            expireDate: value.expired || this.state.expired,
            description: value.description,
            id: Number(this.props.transformer.id),
            impedance: value.impedance,
            kv: value.kv,
            locationId: Number(value.village),
            manufacturer: value.manufacturer,
            noLoadCurrent: value.current,
            noLoadLoss: value.loss,
            serial: value.serial.trim(),
            transformerDimension: value.dimension,
            transformerTypeId: Number(value.typeId),
            transformerWeight: value.weight,
            wc: value.wc,
            weightActivePart: value.part,
            x: Number(value.latitude),
            y: Number(value.longitude)
        });
    }

    componentWillMount(){
        this.props.getProvincesAction();
        this.props.getDistrictsAction(this.props.transformer !== undefined ? this.props.transformer.province : 0);
        this.props.getCommunesAction(this.props.transformer !== undefined ? this.props.transformer.district : 0);
        this.props.getVillagesAction(this.props.transformer !== undefined ? this.props.transformer.commune : 0);
        this.props.getAllTransformersTypesAction();
    }

    componentDidMount(){
        this.props.dispatch(initialize('form_edit_transformer', this.props.transformer));
        this.props.dispatch(change('form_edit_transformer', 'country', "Cambodia"));
    }

    componentWillReceiveProps(data){
        let types = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [];
        if(data.transformerTypeAll.items !== undefined){
            data.transformerTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.transformerType
                })
            })
        } else types = [];
        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];
        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];
        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];
        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];

        this.setState({types: types, provinces: provinces, districts: districts, communes: communes, villages: villages});
        if(data.transformerUpdate.status === 200){
            alert("Successful update Transformer !!");
            this.closeUpdate();
        } else if(data.transformerUpdate.status === (400 && 404) || data.transformerUpdate.status === (500 && 502)){
            alert(data.transformerUpdate.status + " Error !! \nFail update Transformer !!\n" + data.transformerUpdate.message);
            this.closeUpdate();
        } else if(data.transformerUpdate.status === 401){
            alert("Please Login !!")
        }
        data.transformerUpdate.status = 0;
    }

    closeUpdate(){
        this.props.closeUpdate(false);
        this.props.dispatch(initialize('form_edit_transformer', null));
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h4 style={{ color: '#1c91d0', fontWeight: 'bold'}}>EDIT TRANSFORMER</h4>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.types} sortBy="name" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Weight active part </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="part" type="text" component={TextBox} label="Weight active part" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Weight </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="weight" type="text" component={TextBox} label="Transformer weight" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Transformer Dimension </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="dimension" type="text" component={TextBox} label="Transformer dimension" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>KV </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="kv" type="text" component={TextBox} label="KV" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>CG </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="cg" type="text" component={TextBox} label="CG" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>WC </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="wc" type="text" component={TextBox} label="WC" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>No.Load Current </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="current" type="text" component={TextBox} label="No. load current" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>No.Load Loss </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="loss" type="text" component={TextBox} label="No. load loss" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Manufacturer </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="manufacturer" type="text" component={TextBox} label="Manufacturer" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Impedance </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="impedance" type="text" component={TextBox} label="Impedance" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Country</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="country" type="text" component={TextBox} label="Country" disabled={true} icon="fa fa-map-marker" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Province <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>District <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Commune <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Village <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="village" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" icon="fa fa-map-marker"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Latitude</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="latitude" type="text" component={TextBox} label="Latitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Longitude</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="longitude" type="text" component={TextBox} label="Longitude" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Created Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="created" component={DateTimePicker} placeholder="Created date" defaultDate={this.state.created} handleChange={this.handleCreatedDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Expire Date</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="expired" component={DateTimePicker} placeholder="Expire date" defaultDate={this.state.expired} handleChange={this.handleExpireDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Address</strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="address" type="text" component={TextArea} label="Address" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Description</strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={2} lg={2}>
                                    <Button bsStyle="primary" style={{width: '100%'}} onClick={this.closeUpdate.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                </Col>
                                <Col md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
EditTransformer = reduxForm({
    form: 'form_edit_transformer',
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{1,100}/;
        const errors = {};
        if (!regex_text.test(values.serial)) {
            errors.serial = 'Please input at least 1 character !!'
        }
        if (values.transformerType === "") {
            errors.transformerType = "Please select transformer type !!";
        }
        if (values.province === ("0" || "")) {
            errors.province = "Please select province !!";
        }
        if (values.district === ("0" || "")) {
            errors.district = "Please select district !!";
        }
        if (values.commune === "0" || values.commune === "" ) {
            errors.commune = "Please select commune !!";
        }
        if (values.village === "0" || values.village === "" ) {
            errors.village = "Please select village !!";
        }
        return errors
    },
})(EditTransformer);

function mapStateToProps(state) {
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        transformerTypeAll: state.transformerType.transformerTypeAll,
        transformerUpdate: state.transformer.transformerUpdate,
        initialValues: {
            country: 1,
            province: '0',
            district: '0',
            commune: '0',
            village: '0',
            address: "",
            cg: "",
            description: "",
            dimension: "",
            impedance: "",
            kv: "",
            manufacturer: "",
            current: "",
            loss: "",
            serial: "",
            typeId: '',
            weight: "",
            wc: "",
            part: "",
            latitude: '0',
            longitude: '0'
        }
    })
}

function mapDispatchToProps(dispatch) {

    return bindActionCreators({
        getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction,
        getAllTransformersTypesAction,
        updateTransformerAction
    }, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(EditTransformer);