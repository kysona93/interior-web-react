import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { updateMeterAction } from './../../../../actions/setup/meter';

class EditMeter extends React.Component {
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    closeUpdate(){
        this.props.dispatch(initialize('form_edit_meter', null));
        this.props.closeUpdate(false);
    }

    componentWillMount(){
        this.props.dispatch(initialize('form_edit_meter', this.props.meter));
    }

    componentWillReceiveProps(data){
        if(data.meterUpdate.status === 200){
            alert("Meter have been updated !!");
            this.closeUpdate();
        } else if(data.meterUpdate.status === (400 || 404) || data.meterUpdate.status === (500 || 502)){
            alert(data.meterUpdate.status + " Error !!\n Meter have not been updated !!\n" + data.meterUpdate.message);
            this.closeUpdate();
        } else if(data.meterUpdate.status === 401){
            alert("Please login !!");
            this.closeUpdate();
        }
        data.meterUpdate.status = 0;
    }

    handleSubmit(value){
        this.props.updateMeterAction({
            id: Number(this.props.meter.id),
            brand: value.brand.trim(),
            coefficient: Number(value.coefficient),
            depreciation: Number(value.depreciation),
            endUsageRecord: Number(value.endUsageRecord),
            makeIn: Number(value.makeIn),
            maxAmpere: Number(value.maxAmpere),
            meterPhaseId: Number(value.phaseId),
            meterSerial: value.meterSerial.trim(),
            meterTypeId: Number(value.typeId),
            minAmpere: Number(value.minAmpere),
            originalUsageRecord: Number(value.originalUsageRecord),
            status: Number(value.status)
        });
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return(
            <div className="col-lg-12">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Edit Meter</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Serial</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterSerial" type="text" component={TextBox} label="Meter Serial" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Phase</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phaseId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[{id: 1, name: "1PH"}, {id: 2, name: "3PH"}]} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Type</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.props.types} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Min Ampere</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="minAmpere" type="text" component={TextBox} label="Min Ampere" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Max Ampere</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="maxAmpere" type="text" component={TextBox} label="Max Ampere" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Manufacturer</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="brand" type="text" component={TextBox} label="Manufacturer" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Make in Year</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="makeIn" type="text" component={TextBox} label="Make in Year" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Original Usage Record</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="originalUsageRecord" type="text" component={TextBox} label="Original Usage Record" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>End Usage Record</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="endUsageRecord" type="text" component={TextBox} label="End Usage Record" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Depreciation</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="depreciation" type="text" component={TextBox} label="Depreciation" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Coefficient</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="coefficient" type="text" component={TextBox} label="Coefficient" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="status" type="select" component={SelectBox} placeholder="Status" values={[{id: 1, name: "Active"},{id: 0, name: "Stopped"}]} sortBy="name" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={8} lg={8}>
                                    <span className="label-require">Note: All fields are required *</span>
                                </Col>

                                <Col md={2} lg={2}>
                                    <Button bsStyle="primary" style={{width: '115%'}} onClick={this.closeUpdate.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                </Col>
                                <Col md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

EditMeter = reduxForm({
    form: 'form_edit_meter',
    validate: (values) => {
        let regex_text = /[0-9a-zA-Z]{2,1024}/;
        let regex_number = /[0-9]{1,10}/;

        const errors = {};
        if (!regex_text.test(values.brand) || values.brand === undefined) {
            errors.brand = 'Brand is required!';
        }
        if (!regex_number.test(values.coefficient) || values.coefficient === undefined) {
            errors.coefficient = 'Coefficient is required and accept only numeric!';
        }
        if (!regex_number.test(values.depreciation) || values.depreciation === undefined) {
            errors.depreciation = 'Depreciation is required and accept only numeric!';
        }
        if (!regex_number.test(values.makeIn) || values.makeIn === undefined) {
            errors.makeIn = 'Make in year is required and accept only numeric!';
        }
        if (!regex_number.test(values.maxAmpere) || values.maxAmpere === undefined) {
            errors.maxAmpere = 'Max Ampere is required and accept only numeric!';
        }
        if (!regex_number.test(values.minAmpere) || values.minAmpere === undefined) {
            errors.minAmpere = 'Min Ampere is required and accept only numeric!';
        }
        if (values.meterPhaseId === undefined) {
            errors.meterPhaseId = 'Meter Phase is required!';
        }
        if (!regex_text.test(values.meterSerial) || values.meterSerial === undefined) {
            errors.meterSerial = 'Meter Serial is required!';
        }
        if (values.meterTypeId === undefined) {
            errors.meterTypeId = 'Meter Type is required!';
        }
        if (!regex_number.test(values.originalUsageRecord) || values.originalUsageRecord === undefined) {
            errors.originalUsageRecord = 'Original usage record is required and accept only numeric!';
        }
        if (!regex_number.test(values.endUsageRecord) || values.endUsageRecord === undefined) {
            errors.endUsageRecord = 'End usage record is required and accept only numeric!';
        }
        if(values.billingItemId === undefined){
            errors.billingItemId = "Billing Item is required!";
        }
        if (values.status === undefined) {
            errors.status = 'Status is required and accept only numeric!';
        }
        return errors;
    }
})(EditMeter);

function mapStateToProps(state) {
    return {
        meterUpdate: state.meter.meterUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ updateMeterAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditMeter);