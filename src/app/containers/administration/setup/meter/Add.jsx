import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addMeterAction } from './../../../../actions/setup/meter';
import { getAllMeterTypesAction } from './../../../../actions/setup/type/meterType';

class AddMeter extends React.Component {
    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentWillMount(){
        this.props.getAllMeterTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.meterAdd.status === 200){
            alert("Successful add new meter !!");
            data.meterAdd.status = 0;
        }
        if(data.meterAdd.status === 404 || data.meterAdd.status === 500){
            alert("Fail add meter, make sure meter serial must be unique !!");
            data.meterAdd.status = 0;
        }
    }

    handleSubmit(values){
        this.props.addMeterAction({
            brand: values.brand.trim(),
            coefficient: Number(values.coefficient),
            depreciation: Number(values.depreciation),
            endUsageRecord: Number(values.endUsageRecord),
            makeIn: Number(values.makeIn),
            maxAmpere: Number(values.maxAmpere),
            meterPhaseId: Number(values.meterPhaseId),
            meterSerial: values.meterSerial.trim(),
            meterTypeId: Number(values.meterTypeId),
            minAmpere: Number(values.minAmpere),
            originalUsageRecord: Number(values.originalUsageRecord),
            status: Number(values.status)
        });
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        let meterTypes = [];
        if(this.props.meterTypeAll.items !== undefined){
            this.props.meterTypeAll.items.map((type) => {
                meterTypes.push({
                    id: type.id,
                    name: type.meterType
                })
            })
        }else {meterTypes = [];}

        return(
            <div className="col-lg-12 container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Meter</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Serial</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterSerial" type="text" component={TextBox} label="Meter Serial" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Phase</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterPhaseId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[{id: 1, name: "1PH"}, {id: 2, name: "3PH"}]} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Type</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterTypeId" type="select" component={SelectBox} placeholder="Please select ..." values={meterTypes} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Min Ampere</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="minAmpere" type="text" component={TextBox} label="Min Ampere" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Max Ampere</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="maxAmpere" type="text" component={TextBox} label="Max Ampere" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Manufacturer</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="brand" type="text" component={TextBox} label="Manufacturer" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Make in Year</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="makeIn" type="text" component={TextBox} label="Make in Year" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Original Usage Record</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="originalUsageRecord" type="text" component={TextBox} label="Original Usage Record" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>End Usage Record</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="endUsageRecord" type="text" component={TextBox} label="End Usage Record" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Depreciation</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="depreciation" type="text" component={TextBox} label="Depreciation" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Coefficient</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="coefficient" type="text" component={TextBox} label="Coefficient" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="status" type="select" component={SelectBox} placeholder="Status" values={[{id: 1, name: "Active"},{id: 0, name: "Stopped"}]} sortBy="name" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={10} lg={10}>
                                    <span className="label-require">Note: All fields are required *</span>
                                </Col>
                                <Col md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_meter'));

AddMeter = reduxForm({
    form: 'form_add_meter',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{2,1024}/;
        const regex_number = /[0-9]{1,10}/;

        const errors = {};
        if (!regex_text.test(values.brand)) {
            errors.brand = 'Please input at least 2 characters !!';
        }
        if (!regex_number.test(values.coefficient) || values.coefficient === "") {
            errors.coefficient = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.depreciation) || values.depreciation === "") {
            errors.depreciation = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.makeIn) || values.makeIn === "") {
            errors.makeIn = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.maxAmpere) || values.maxAmpere === "") {
            errors.maxAmpere = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.minAmpere) || values.minAmpere === "") {
            errors.minAmpere = 'Please input only numeric !!';
        }
        if (values.meterPhaseId === "") {
            errors.meterPhaseId = 'Please select meter phase !!';
        }
        if (!regex_text.test(values.meterSerial)) {
            errors.meterSerial = 'Please input at least 2 characters !!';
        }
        if (values.meterTypeId === "") {
            errors.meterTypeId = 'Please select meter type !!';
        }
        if (!regex_number.test(values.originalUsageRecord) || values.originalUsageRecord === "") {
            errors.originalUsageRecord = 'Please input only numeric !!';
        }
        if (!regex_number.test(values.endUsageRecord) || values.endUsageRecord === "") {
            errors.endUsageRecord = 'Please input only numeric !!';
        }
        if (values.status === "") {
            errors.status = 'Please select status !!';
        }

        return errors;
    }
})(AddMeter);

function mapStateToProps(state) {
    return {
        meterTypeAll : state.meterType.meterTypeAll,
        meterAdd: state.meter.meterAdd,
        initialValues: {
            brand: '',
            coefficient: '',
            depreciation: '',
            endUsageRecord: '',
            makeIn: '',
            maxAmpere: '',
            meterPhaseId: '',
            meterSerial: '',
            meterTypeId: '',
            minAmpere: '',
            originalUsageRecord: '',
            status: '1'
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addMeterAction, getAllMeterTypesAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddMeter);