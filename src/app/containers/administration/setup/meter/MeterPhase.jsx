import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addMeterPhaseAction, listMeterPhasesAction, updateMeterPhaseAction, deleteMeterPhaseAction } from './../../../../actions/setup/meterPhase';

let phaseId = 0;
class MeterPhase extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Meter Phase'
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.listMeterPhasesAction();
    }

    componentWillReceiveProps(data){
        if(data.addMeterPhase.status === 200){
            if(confirm("Successfully added new meter phase.") === true){
                this.props.listMeterPhasesAction();
                data.addMeterPhase.status = 0;
            }else{
                this.props.listMeterPhasesAction();
                data.addMeterPhase.status = 0;
            }
        }
        if(data.addMeterPhase.status === 404 || data.addMeterPhase.status === 500){
            alert("Fail with add meter phase!");
        }

        if(data.updateMeterPhase.status === 200 ){
            alert("Successfully updated meter phase.");
            this.setState({label: 'Save'});
            this.setState({update: false});
            this.props.listMeterPhasesAction();
            data.updateMeterPhase.status = 0;
        }
        if(data.updateMeterPhase.status === 400 || data.updateMeterPhase.status === 500){
            alert("Fail with update meter phase!");
        }

        if(data.deleteMeterPhase.status === 200){
            alert("Successfully deleted this meter phase.");
            this.props.listMeterPhasesAction();
            data.deleteMeterPhase.status = 0;
        }
        if(data.deleteMeterPhase.status === 400 || data.deleteMeterPhase.status === 500){
            alert("Fail with delete this meter phase!");
        }
    }

    handleBack(){
        location.href = "/app/meters/meter-phase";
    }

    updateItem(data){
        phaseId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Meter Phase'});
        this.props.dispatch(initialize(
            'form_add_meter_phase',
            data,
            ['meterPhase', 'description', 'status'])
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this meter phase from database?") === true){
            this.props.deleteMeterPhaseAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add box type
            let meterPhase = {
                "description": values.description,
                "meterPhase": values.meterPhase
            };
            this.props.addMeterPhaseAction(meterPhase);
        }else{
            // edit box type
            let meterPhase = {
                "id": phaseId,
                "description": values.description,
                "meterPhase": values.meterPhase
            };
            this.props.updateMeterPhaseAction(meterPhase);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Phase <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterPhase" type="text" component={TextBox} label="Meter Phase" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Meter Phase</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllMeterPhases.items != undefined ?
                                        <tbody>
                                        { this.props.listAllMeterPhases.items.map((meter,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{meter.id}</td>
                                                    <td>{meter.meterPhase}</td>
                                                    <td>{meter.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(meter)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(meter.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>
            </div>
        )
    }
}

MeterPhase = reduxForm({
    form: 'form_add_meter_phase',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.meterPhase) || values.meterPhase === undefined) {
            errors.meterPhase = 'Meter Phase is required and at least 2 character!';
        }
        return errors;
    }
})(MeterPhase);

function mapStateToProps(state) {
    //console.log("data ", state.meterPhase.listAllMeterPhases)
    return {
        addMeterPhase: state.meterPhase.addMeterPhase,
        listAllMeterPhases: state.meterPhase.listAllMeterPhases,
        updateMeterPhase: state.meterPhase.updateMeterPhase,
        deleteMeterPhase: state.meterPhase.deleteMeterPhase
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addMeterPhaseAction, listMeterPhasesAction, updateMeterPhaseAction, deleteMeterPhaseAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MeterPhase);