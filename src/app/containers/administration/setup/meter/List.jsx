import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Row, Col, Table } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import '../../../../components/forms/Styles.css';
import EditMeter from './../meter/Edit';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import Pagination from '../../../../components/forms/Pagination';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getMetersAction, deleteMeterAction } from './../../../../actions/setup/meter';
import { getAllMeterTypesAction } from './../../../../actions/setup/type/meterType';

let meter = {
    serial: "",
    typeId: 0,
    limit: 10,
    page: 1,
    orderBy: "id",
    orderType: "desc"
};

class ListMeter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            types: [],
            meter: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
        //this.handleSort = this.handleSort.bind(this);
    }

    componentWillMount(){
        this.props.getAllMeterTypesAction();
        this.props.getMetersAction(meter);
    }

    openUpdate(meter){
        this.setState({update: true, meter: meter})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getMetersAction(meter);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this meter?") === true){
            this.props.deleteMeterAction(id);
        }
    }

    handleSubmit(value){
        meter.typeId = value.typeId !== undefined ? Number(value.typeId) : 0;
        meter.serial = value.serial.trim();
        meter.page = 1;
        this.props.getMetersAction(meter);
    }

    componentWillReceiveProps(data){
        let types = [];
        if(data.meterTypeAll.items !== undefined){
            data.meterTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.meterType
                })
            });
            this.setState({types: types});
        } else {types = [];}

        if(data.meters.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.meters.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.meterDelete.status === 200){
            alert("Meter have been deleted !!");
            this.props.getMetersAction(meter);
        } else if(data.meterDelete.status === (400 || 404) || data.meterDelete.status === (500 || 502)){
            alert(data.meterDelete.status + " Error !!\n Meter have not been deleted !!\n" + data.meterDelete.message);
        } else if(data.meterDelete.status === 401){
            alert("Please login !!")
        }
        data.meterDelete.status = 0;
    }

    handleSort(value){
        meter.orderBy = value;
        if(meter.orderType === "desc"){
            meter.orderType = "asc";
        } else{
            meter.orderType = "desc";
        }
        this.props.getMetersAction(meter);
    }

    render(){
        const meters = this.props.meters.items;
        const {handleSubmit, error, submitting} = this.props;
        let total = 0;
        return (
            <div className="col-lg-12 container-fluid">
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        <h3 style={{ color: '#1c91d0', position: 'absolute', marginTop: '0.7%', fontWeight: 'bold'}}>METER REPORT</h3>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={3} lg={9}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">TYPE : </p></td>
                                            <td><Field name="typeId" type="select" component={SelectBox} placeholder="Meter type..." values={this.state.types} sortBy="name" /></td>
                                            <td><p className="search-installer">NAME : </p></td>
                                            <td><Field name="serial" type="text" component={TextBox} label="Meter serial..."/></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>

                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>
                        <Table responsive condensed bordered hover>
                            <thead >
                            <tr>
                                <th><a onClick={() => this.handleSort("id")}>ID</a></th>
                                <th><a onClick={() => this.handleSort("meterSerial")}>Serial</a></th>
                                <th>Type</th>
                                <th>Phase</th>
                                <th>Ampere</th>
                                <th>Coefficient</th>
                                <th>Depreciation</th>
                                <th>Status</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { meters !== undefined ?
                                meters.list.map((meter, index)=>{
                                    total = meters.totalCount;
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{meter.id}</td>
                                            <td className="text-center">{meter.meterSerial}</td>
                                            <td className="text-center">{meter.typeName}</td>
                                            <td className="text-center">{meter.meterPhase}</td>
                                            <td className="text-center">{meter.minAmpere + "-" + meter.maxAmpere + " A"}</td>
                                            <td className="text-center">{meter.coefficient}</td>
                                            <td className="text-center">{meter.depreciation}</td>
                                            <td className="text-center">{meter.status === 1 ? "Active" : "Stopped"}</td>
                                            <td className="text-center">
                                                {meter.status === 1 ?
                                                    <div>
                                                        <a onClick={() => this.openUpdate(meter)} className='btn btn-info btn-xs'>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={() => this.handleDelete(meter.id)} className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </div>
                                                    :
                                                    <div>
                                                        <a onClick={() => this.openUpdate(meter)} className='btn btn-info btn-xs'>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={() => this.handleDelete(meter.id)} className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </div>
                                                }
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan="9">
                                        <h3 style={{textAlign: 'center'}}>RESULT NOT FOUND!</h3>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        <Pagination
                            totalCount={total}
                            items={meter}
                            callBackAction={this.props.getMetersAction}
                        />
                    </Row>
                    :
                    <EditMeter closeUpdate={this.closeUpdate} types={this.state.types} meter={this.state.meter} />
                }
            </div>
        )
    }
}

ListMeter = reduxForm({
    form: 'form_filter_meter'
})(ListMeter);

function mapStateToProps(state) {
    return {
        meterTypeAll: state.meterType.meterTypeAll,
        meters: state.meter.meters,
        meterDelete: state.meter.meterDelete,
        initialValues: {
            serial: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllMeterTypesAction, getMetersAction, deleteMeterAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListMeter);