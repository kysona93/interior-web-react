import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { DateTimePicker } from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import './../../index.css';
import { addMeterUsageAction, listMeterUsagesAction, updateMeterUsageAction, deleteMeterUsageAction } from './../../../../actions/setup/meterUsage';
import moment from 'moment';

let usageId = 0;
class MeterUsage extends React.Component {
    constructor(props){
        super(props);
        this.state={
            update: false,
            label: 'Save',
            labelForm: 'Add New Meter Usage',
            startDate: null
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }


    componentWillMount(){
        this.props.listMeterUsagesAction();
    }

    componentWillReceiveProps(data){
        if(data.addMeterUsage.status === 200){
            if(confirm("Successfully added new meter usage.") === true){
                this.props.listMeterUsagesAction();
                this.setState({startDate: null});
                data.addMeterUsage.status = 0;
            }else{
                this.props.listMeterUsagesAction();
                data.addMeterUsage.status = 0;
            }
        }
        if(data.addMeterUsage.status === 404 || data.addMeterUsage.status === 500){
            alert("Fail with add meter usage!");
        }

        if(data.updateMeterUsage.status === 200 ){
            alert("Successfully updated meter usage.");
            this.setState({label: 'Save'});
            this.setState({update: false});
            this.props.listMeterUsagesAction();
            data.updateMeterUsage.status = 0;
            this.setState({labelForm: 'Add New Meter Usage'});
        }
        if(data.updateMeterUsage.status === 400 || data.updateMeterUsage.status === 500){
            alert("Fail with update meter usage!");
        }

        if(data.deleteMeterUsage.status === 200){
            alert("Successfully deleted this meter usage.");
            this.props.listMeterUsagesAction();
            data.deleteMeterUsage.status = 0;
        }
        if(data.deleteMeterUsage.status === 400 || data.deleteMeterUsage.status === 500){
            alert("Fail with delete this meter usage!");
        }
    }

    handleBack(){
        location.href = "/app/meters/meter-usage";
    }

    updateItem(data){
        usageId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Meter Usage'});
        this.props.dispatch(initialize(
            'form_add_meter_usage',
            data,
            ['meterSerial', 'lastUsageRecord', 'newUsageRecord', 'recordDate'])
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this meter usage?") === true){
            this.props.deleteMeterUsageAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add
            let meterUsage = {
                "lastUsageRecord": Number(values.lastUsageRecord),
                "meterSerial": values.meterSerial,
                "newUsageRecord": Number(values.newUsageRecord),
                "recordDate": values.recordDate,
                "status": 1
            };
            this.props.addMeterUsageAction(meterUsage);
        }else{
            // edit
            let meterUsage = {
                "id": usageId,
                "lastUsageRecord": Number(values.lastUsageRecord),
                "meterSerial": values.meterSerial,
                "newUsageRecord": Number(values.newUsageRecord),
                "recordDate": values.recordDate,
                "status": 1
            };
            this.props.updateMeterUsageAction(meterUsage);
        }

    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div>
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterSerial" type="text" component={TextBox} label="Meter Serial" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Last Usage Record <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="lastUsageRecord" type="text" component={TextBox} label="Last Usage Record" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>End Usage Record <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="newUsageRecord" type="text" component={TextBox} label="End Usage Record" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Record Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="recordDate" component={DateTimePicker} placeholder="Record Date"
                                                   defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Meter Serial</th>
                                    <th>Last Usage Record</th>
                                    <th>End Usage Record</th>
                                    <th>Record Date</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllMeterUsages.items != undefined ?
                                        <tbody>
                                        { this.props.listAllMeterUsages.items.map((usage,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{usage.id}</td>
                                                    <td>{usage.meterSerial}</td>
                                                    <td>{usage.lastUsageRecord}</td>
                                                    <td>{usage.newUsageRecord}</td>
                                                    <td>{moment(usage.recordDate).format('MMMM Do YYYY')}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(usage)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(usage.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={10}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

            </div>
        )
    }
}

MeterUsage = reduxForm({
    form: 'form_add_meter_usage',
    validate: (values) => {
        let regex_text = /[0-9a-zA-Z]{2,500}/;
        let regex_number = /[0-9]{1,6}/;

        const errors = {};
        if (!regex_text.test(values.meterSerial) || values.meterSerial === undefined) {
            errors.meterSerial = 'Meter Serial is required and at least 2 character!';
        }
        if (!regex_number.test(values.lastUsageRecord) || values.lastUsageRecord === undefined) {
            errors.lastUsageRecord = 'Last Usage Record is required and at least 4 character!';
        }
        if (!regex_number.test(values.newUsageRecord) || values.newUsageRecord === undefined) {
            errors.newUsageRecord = 'New Usage Record is invalid!';
        }
        if (values.recordDate === undefined) {
            errors.recordDate = 'Record Date is required!';
        }
        return errors
    }
})(MeterUsage);

function mapStateToProps(state) {
    return {
        addMeterUsage: state.meterUsage.addMeterUsage,
        listAllMeterUsages :state.meterUsage.listAllMeterUsages,
        updateMeterUsage: state.meterUsage.updateMeterUsage,
        deleteMeterUsage: state.meterUsage.deleteMeterUsage,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addMeterUsageAction, listMeterUsagesAction, updateMeterUsageAction, deleteMeterUsageAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MeterUsage);