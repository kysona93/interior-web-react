import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table } from 'react-bootstrap';
import { Field, reduxForm} from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getItemsMvAction,getItemMvSummaryAction } from './../../../../actions/customer/network/meter';
import { getRentPoleAction } from './../../../../actions/customer/network/license';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import './../index.css';
import moment from 'moment';
import { loadLicense } from '../../../../localstorages/localstorage';

let summary ={
    licenseId : loadLicense() === undefined ? 0 : loadLicense().id,
    endDate:0
};

class SummaryKw extends Component{
    constructor(props){
        super(props);
        this.state = {
            startDate:null,
            endDate: null
        };
        this.handleSubmit =this.handleSubmit.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleEndDate = this.handleEndDate.bind(this);
    }

    handleStartDate(date){
        this.setState({ startDate: date })
    }
    handleEndDate(date){
        this.setState({ endDate: date })
    }
    componentDidMount(){
        this.props.getItemMvSummaryAction(summary);
    }

    componentWillReceiveProps(data){
    }
    handleSubmit(value){
        summary.licenseId =loadLicense().id;
        summary.startDate=value.startDate;
        summary.endDate= value.endDate;
        this.props.getItemMvSummaryAction(summary);
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                    <Row>
                        <Col md={6} lg={6} className="pull-right">
                            <Row>
                                <Col md={5} lg={5}>
                                    <Field name="startDate" component={DateTimePicker}
                                           placeholder="start Date"
                                           defaultDate={this.state.startDate}
                                           handleChange={this.handleStartDate}/>
                                </Col>
                                <Col md={5} lg={5}>
                                    <Field name="endDate" component={DateTimePicker}
                                           placeholder="end Date"
                                           defaultDate={this.state.endDate}
                                           handleChange={this.handleEndDate}/>
                                </Col>
                                <Col  md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Search"/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                <div className="row">
                    <Table responsive condensed bordered hover className="table-list-mv" id="summary-point-to-xls">
                        <thead >
                        <tr>
                            <th>ID</th>
                            <th>Connection Point</th>
                            <th>Serial</th>
                            <th>Connection Type</th>
                            <th>Pole</th>
                            <th>Previous</th>
                            <th>New Usage</th>
                            <th>Issue Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.itemMvSummaryGet.items != undefined ?
                                this.props.itemMvSummaryGet.items.map((license,index) =>{
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{license.customerId}</td>
                                            <td className="text-center">{license.nameKh}</td>
                                            <td className="text-center">{license.poleSerial}</td>
                                            <td className="text-center">{license.nameKh}</td>
                                            <td className="text-center">{license.nameKh}</td>
                                            <td className="text-center">{license.meterUsageLastUsageRecord}</td>
                                            <td className="text-center">{license.meterUsageNewUsageRecord}</td>
                                            <td className="text-center">{moment(license.meterUsageRecordDate).format("YYYY-MM-DD")}</td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={11}><h2>RESULT NOT FOUND</h2></td>
                                </tr>
                        }
                        </tbody>
                    </Table>
                </div>
                <Row>
                    <ul className="button-style">
                        {/**
                         * <li><Button className="my-custom" bsStyle="primary">&nbsp;Print</Button></li>
                        */}
                        <li><ReactHTMLTableToExcel
                            className="my-custom-export"
                            table="summary-point-to-xls"
                            filename="summary-kw"
                            sheet="summary-point"
                            buttonText="Export"/></li>
                    </ul>
                </Row>
            </form>
            </div>
        )
    }
}
SummaryKw = reduxForm({
    form: 'form_add_rent_pole'
})(SummaryKw);

function mapStateToProps(state) {
    return {
        itemMvSummaryGet: state.changeMeter.itemMvSummaryGet
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getRentPoleAction,getItemsMvAction ,getItemMvSummaryAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(SummaryKw);