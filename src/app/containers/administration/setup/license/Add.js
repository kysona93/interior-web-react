import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table, Pagination,Button, FormGroup, ControlLabel} from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import {browserHistory} from 'react-router';
import { bindActionCreators } from 'redux';
import { TextBox } from './../../../../components/forms/TextBox';
import { TextArea } from './../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker } from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from './../../../../components/forms/ButtonSubmit';
import { loadCustomer } from './../../../../localstorages/localstorage';
import {getEmployeesAction} from './../../../../actions/employee/employee';
import { addItemMvAction, updateItemMvAction, getItemsMvAction,deleteItemMvAction } from './../../../../actions/customer/network/meter';
import EditItemMv from './EditLicense';
import moment from 'moment';

class AddItemMv extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            update: false,
            gander: [{id: "MALE", name: "Male"}, {id: "FEMALE", name: "FeMale"}],
            licenseType: [{id: "CT/VT", name: "CT/VT"}],
            startDate: null,
            mv: {}
        };
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
    }

    componentWillReceiveProps(data) {
        if (data.itemMvAdd.status === 200) {
            alert("success add ItemMV");
            this.props.initialize("form_add_item_mv", null);
            data.itemMvAdd.status = 0;
            browserHistory.push("/app/administration/setup/licenses");
        } else if (data.itemMvAdd.status === 404 || data.itemMvAdd.status === 500) {
            alert("add ItemMv Fail !!")
        }
    }

    deleteItem(id) {
        if (confirm("Are you sure want to delete ItemMV?") === true) {
            alert(JSON.stringify(this.props.deleteItemMvAction(id)));
            this.props.getItemsMvAction();
        }
    }

    componentWillMount() {
        this.props.getItemsMvAction();
    }

    handleSubmit(values) {
            this.props.addItemMvAction({
                licenseNameKh: values.licenseNameKh,
                licenseNameEn: values.licenseNameEn,
                licenseNumber: values.licenseNumber,
                registerDate: values.registerDate,
                region: values.region,
                address: values.address,
                licenseType: values.licenseType,
                owner: values.owner,
                sex: values.sex,
                phone: values.phone,
                email: values.email,
                id: 0
            })
    }

    openUpdate(mv) {
        this.setState({update: true, mv: mv})
    }

    closeUpdate(value) {
        this.setState({update: value});
        this.props.getItemsMvAction(loadCustomer().id);
    }

    handleRegisterDate(date) {
        this.setState({startDate: date})
    }

    render() {
        const {handleSubmit, error, invalid, submitting} = this.props;
        const itemMvGets = this.props.itemMvGet.items;
        let employees = [];
        if (this.props.employees.items !== undefined) {
            this.props.employees.items.list.map((emp) => {
                employees.push({
                    id: emp.id,
                    name: emp.nameKh
                })
            });
        }
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={6} lg={6}>
                        <Button className="my-custom pull-left" bsStyle="primary" onClick={ ()=>{ window.history.back() }}>&nbsp;Back</Button>
                    </Col>
                </Row>
                <br/>
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Add License</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>licenseNameKh : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="licenseNameKh" type="text" component={TextBox}
                                                   label="license Name Kh"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>licenseNameEn : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="licenseNameEn" type="text" component={TextBox}
                                                   label="license Name En"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>license No : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="licenseNumber" type="text" component={TextBox}
                                                   label="license No"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>licenseType : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="licenseType" type="select" component={SelectBox}
                                                   placeholder="licenseType"
                                                   values={this.state.licenseType} sortBy="name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Region : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="region" type="text" component={TextBox} label="Region"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Address : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="address" type="text" component={TextBox} label="address"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Register Date : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="registerDate" component={DateTimePicker}
                                                   placeholder="Install Date"
                                                   defaultDate={this.state.startDate}
                                                   handleChange={this.handleRegisterDate}/>
                                        </Col>
                                    </Row>
                                </Col>

                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> owner : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="owner" type="text" component={TextBox} label="owner"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Gander : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="sex" type="select" component={SelectBox} placeholder="gander"
                                                   values={this.state.gander} sortBy="name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Phone : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phone" type="text" component={TextBox} label="phone"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Email : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="email" type="text" component={TextBox} label="email"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="រក្សាទុក/Save"/>
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

AddItemMv = reduxForm({
    form: 'form_add_item_mv',
    validate: (values) => {
        const errors = {};
        if(values.licenseNameKh === "" || values.licenseNameKh == undefined){
            errors.licenseNameKh = "Field Required !!"
        }
        if(values.licenseNameEn === "" || values.licenseNameEn == undefined){
            errors.licenseNameEn = "Field Required !!"
        }
        if(values.licenseNumber === "" || values.licenseNumber == undefined){
            errors.licenseNumber = "Field Required !!"
        }
        if(values.registerDate === "" || values.registerDate == undefined){
            errors.registerDate = "Field Required !!"
        }
        if(values.licenseType === "" || values.licenseType == undefined){
            errors.licenseType = "Field Required !!"
        }
        if(values.owner === "" || values.owner == undefined){
            errors.owner = "Field Required !!"
        }
        if(values.phone === "" || values.phone == undefined){
            errors.phone = "Field Required !!"
        }
        if(values.region === "" || values.region == undefined){
            errors.region = "Field Required !!"
        }

        return errors
    }
})(AddItemMv);
function mapStateToProps(state) {
    return {
        itemMvAdd: state.changeMeter.itemMvAdd,
        employees: state.employee.employees,
        itemMvGet: state.changeMeter.itemMvGet,
        itemMvDelete: state.changeMeter.itemMvDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addItemMvAction, updateItemMvAction, getItemsMvAction, deleteItemMvAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddItemMv);