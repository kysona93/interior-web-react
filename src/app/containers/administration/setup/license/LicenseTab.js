import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import EditItemMv from './EditLicense';
class LicenseTab extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const renderLicenseInf = () =>{
            return(
                <tbody>
                <tr>
                    <td>LicenseNumber: test</td>
                    <td>LicenseNameKh: test</td>
                    <td>LicenseNameEn: test</td>
                    <td>NumberOfPoint: test</td>
                </tr>
                </tbody>
            )
        };
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <ul className="nav nav-tabs search-tab" role="tablist">
                                <li role="presentation" className="active"><a href="#customer-info" aria-controls="customer-info" role="tab" data-toggle="tab">License Information</a></li>
                                <li role="presentation"><a href="#network-management" aria-controls="network" role="tab" data-toggle="tab">Summary KWH</a></li>
                                <li role="presentation"><a href="#finance" aria-controls="finance" role="tab" data-toggle="tab">Finance</a></li>
                                <li role="presentation"><a href="#user-history" aria-controls="history" role="tab" data-toggle="tab">Rent Pole</a></li>
                                <li role="presentation"><a href="#user-graph" aria-controls="usergraph" role="tab" data-toggle="tab">ConnectionPoint</a></li>
                                <li role="presentation"><a href="#audit-trial" aria-controls="audittrial" role="tab" data-toggle="tab">Audit Trial</a></li>
                            </ul>
                            <div className="search-tab tab-content">
                                <div role="tabpanel" className="tab-pane active" id="customer-info">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="panel panel-default">
                                            <div className="panel-heading">
                                                <h3 className="panel-title"><span style={{color: 'blue', fontSize: '20px'}}>Customer Information</span></h3>

                                            </div>
                                            <div className="panel-body">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="network-management">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <h2>Network</h2>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="finance">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <h2>Finance</h2>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="user-history">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <h2>User History</h2>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="user-graph">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                <tbody>
                                                <tr>
                                                    <td>CustomerId:</td>
                                                    <td>0001</td>
                                                    <td>Last Record:</td>
                                                    <td>0</td>
                                                    <td>Meter Info:</td>
                                                    <td>1007150906 , 1Ph</td>
                                                    <td>Status:</td>
                                                    <td>Using</td>
                                                    <td>Type</td>
                                                    <td>Pre-paid (IC)</td>
                                                </tr>
                                                <tr>
                                                    <td>CustomerName:</td>
                                                    <td>Sok Dara</td>
                                                    <td>Remaining Usage:</td>
                                                    <td>0</td>
                                                    <td>Breaker:</td>
                                                    <td>Bk 0001</td>
                                                    <td>Register Date:</td>
                                                    <td>01/01/2017</td>
                                                    <td>Deposit:</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <h2>Graph</h2>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="audit-trial">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                <tbody>
                                                <tr>
                                                    <td>CustomerId:</td>
                                                    <td>0001</td>
                                                    <td>Last Record:</td>
                                                    <td>0</td>
                                                    <td>Meter Info:</td>
                                                    <td>1007150906 , 1Ph</td>
                                                    <td>Status:</td>
                                                    <td>Using</td>
                                                    <td>Type</td>
                                                    <td>Pre-paid (IC)</td>
                                                </tr>
                                                <tr>
                                                    <td>CustomerName:</td>
                                                    <td>Sok Dara</td>
                                                    <td>Remaining Usage:</td>
                                                    <td>0</td>
                                                    <td>Breaker:</td>
                                                    <td>Bk 0001</td>
                                                    <td>Register Date:</td>
                                                    <td>01/01/2017</td>
                                                    <td>Deposit:</td>
                                                    <td>0</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <h2>Audit</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
            </div>
        )
    }
}
export default LicenseTab;