import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table, Button , FormGroup, ControlLabel } from 'react-bootstrap';
import { Field, reduxForm, reset,initialize } from 'redux-form';
import { browserHistory } from 'react-router'
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllItemsMvAction,getItemsMvAction } from './../../../../actions/customer/network/meter';
import { getRentPoleAction,getRentPartAction } from './../../../../actions/customer/network/license';
import './../index.css';
import RentPart from './../../../../../app/containers/administration/setup/license/RentPart';
import moment from 'moment';
import { loadLicense} from '../../../../localstorages/localstorage';

class RentPole extends Component {

    constructor(props){
        super(props);
        this.state = {
            showRentPart: false,
            openRentPart:false
        };
    }
    componentWillMount(){
        this.props.getRentPartAction(loadLicense().id);
    }

    closeRentPart(value){this.setState({ openRentPart:value })}

    render(){
        return (
            <div className="container-fluid">
                {
                    !this.state.openRentPart ?
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <h3 className="panel-title">Rent Pole</h3>
                                    </div>
                                    <div className="panel-body">
                                        <Table responsive condensed bordered hover className="table-list-mv">
                                            <thead >
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Parth</th>
                                                    <th>Start Pole</th>
                                                    <th>End Pole</th>
                                                    <th>Total Pole</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            { this.props.rentPartGet.items !== undefined ?
                                                this.props.rentPartGet.items.map((pole, index) => {
                                                    return(
                                                        <tr key={index}>
                                                            <td className="text-center"><a onClick={ ()=> {browserHistory.push('/app/administration/setup/licenses/view/'+pole.rentPartId)}}>{pole.rentPartId}</a> </td>
                                                            <td className="text-center"></td>
                                                            <td className="text-center"></td>
                                                            <td className="text-center"></td>
                                                            <td className="text-center">{pole.totalPole}</td>
                                                        </tr>
                                                    )
                                                })
                                                :
                                                <tr>
                                                    <td colSpan="8">
                                                        <center><h3>RESULT NOT FOUND!</h3></center>
                                                    </td>
                                                </tr>
                                            }
                                            </tbody>
                                        </Table>
                                        <Row>
                                            <ul className="button-style">
                                                <li><Button className="my-custom" bsStyle="primary" onClick={ ()=> {this.setState({showRentPart: true , openRentPart:true})} }>&nbsp;Add New</Button></li>
                                                <li><Button className="my-custom" bsStyle="primary">&nbsp;Print</Button></li>
                                                <li><Button className="my-custom" bsStyle="primary">&nbsp;Export</Button></li>
                                            </ul>
                                        </Row>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                        :
                        <RentPart closeRentPart={ this.closeRentPart.bind(this) }/>
                }
            </div>
        )
    }
}
RentPole = reduxForm({
    form: 'form_add_rent_pole',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        return errors
    },
})(RentPole);

function mapStateToProps(state) {
    return {
        rentPartGet: state.rentPole.rentPartGet
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getRentPoleAction,getItemsMvAction,getRentPartAction },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(RentPole);