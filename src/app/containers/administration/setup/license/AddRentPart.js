import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset,initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllItemsMvAction } from './../../../../actions/customer/network/meter';
import { addRentPartAction,deleteRentPartAction,updateRentPartAction,getAllRentPartAction } from './../../../../actions/customer/network/license';

class AddRentPart extends Component{
    constructor(props){
        super(props);
        this.state = {
            licenseId: 0,
            update: false,
            label: 'Save',
            labelForm: 'New RentPart',
            createdDate: null,
            expireDate: null
        };
        this.handleCreateDate=this.handleCreateDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreateDate(date){
        this.setState({createdDate: date})
    }
    handleExpireDate(date){
        this.setState({expireDate: date})
    }

    componentWillMount(){
        this.props.getAllItemsMvAction();
        this.props.getAllRentPartAction();
    }
    updateItems(data){
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Rent Part', licenseId: data.rentPartId});
        this.props.dispatch(initialize('form_add_rent_part', data));
    }
    deleteItem(id){
        if(confirm("Are you sure want to delete this Rent Part?") === true){
            alert(JSON.stringify(
                this.props.deleteRentPartAction(id)
            ))
        }
    }
    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Rent Part'});
        this.props.dispatch(initialize('form_add_rent_part', null));
    }
    handleSubmit(value){
        if(!this.state.update){
            this.props.addRentPartAction({
                description: value.description,
                endPole: value.startPole,
                licenseId: value.licenseId,
                startPole: value.startPole,
                totalPole: value.totalPole
            })
        }else{
            this.props.updateRentPartAction({
                id:Number(this.state.licenseId),
                description: value.description,
                endPole: value.startPole,
                licenseId: value.licenseId,
                startPole: value.startPole,
                totalPole: value.totalPole
            });
        }
    }

    componentWillReceiveProps(data){
        if (data.rentPartAdd.status === 200) {
            alert("Successful add new RentPart !!");
            this.props.dispatch(reset('form_add_rent_part'));
            this.props.getAllRentPartAction();
        } else if(data.rentPartAdd.status === (400 || 404) || data.rentPartAdd.status === (500 || 502)){
            alert(data.rentPartAdd.status + " Error !! \nFail with add RentPart !!\n" + data.rentPartAdd.data.message);
        } else if(data.rentPartAdd.status === 401){
            alert("Please Login !!")
        }
        data.rentPartAdd.status = 0;
        if(data.rentPartUpdate.status === 200 ){
            alert("Successfully updated Rent Part.");
            this.handleCancel();
            this.props.getAllRentPartAction();
            data.rentPartUpdate.status = 0;
        }
        if(data.rentPartUpdate.status === 400 || data.rentPartUpdate.status === 500){
            alert("Fail with update Rent Part!");
        }

        if(data.rentPartDelete.status === 200){
            alert("Successfully deleted this Rent Part.");
            this.props.getAllRentPartAction();
            data.rentPartDelete.status = 0;
        }
        if(data.rentPartDelete.status === 404 || data.rentPartDelete.status === 500){
            alert("Fail with delete this Rent Part!");
        }
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;
        let licenses = [];
        if(this.props.itemMvGetAll.items !== undefined){
            this.props.itemMvGetAll.items.map((license)=>{
                licenses.push({
                    id:license.id,
                    name:license.licenseNameKh
                })
            })
        }

        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Start Pole <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="startPole" type="text" component={TextBox} label="start pole" icon="fa fa-key"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>End Pole <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="endPole" type="text" component={TextBox} label="end pole"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Total Pole <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="totalPole" type="text" component={TextBox} label="total pole"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>LicenstName<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={5} lg={5}>
                                            <Field name="licenseId" type="select" component={SelectBox} placeholder="Please select..." values={licenses} sortBy="name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-user-circle"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                            <ButtonSubmit error={error} submitting={submitting} label="រក្សាទុក/Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Start Pole</th>
                                    <th>End Pole</th>
                                    <th>Total Pole</th>
                                    <th>LicenseName</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.rentPartGetAll.items != undefined ?
                                        <tbody>
                                        { this.props.rentPartGetAll.items.map((rentPart, index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{rentPart.rentPartId}</td>
                                                    <td>{rentPart.startPole}</td>
                                                    <td>{rentPart.endPole}</td>
                                                    <td>{rentPart.totalPole}</td>
                                                    <td>{rentPart.licenseNameKh}</td>
                                                    <td>{rentPart.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItems(rentPart)}>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(rentPart.rentPartId)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
AddRentPart = reduxForm({
    form: 'form_add_rent_part',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        if (values.startPole === undefined || values.startPole === "" ) {
            errors.startPole = "Please select startPole !!";
        }
        if (values.endPole === undefined || values.endPole === "" ) {
            errors.endPole = "Please select endPole !!";
        }
        if (values.totalPole === undefined || values.totalPole === "" ) {
            errors.totalPole = "Please select totalPole !!";
        }
        if (values.licenseId === undefined || values.licenseId === "" ) {
            errors.licenseId = "Please select licenseId !!";
        }
        return errors
    },
})(AddRentPart);

function mapStateToProps(state) {
    console.log("ddkd",state.rentPole.rentPartGetAll);
    return {
        itemMvGetAll: state.changeMeter.itemMvGetAll,
        rentPartAdd: state.rentPole.rentPartAdd,
        rentPartGetAll: state.rentPole.rentPartGetAll,
        rentPartUpdate: state.rentPole.rentPartUpdate,
        rentPartDelete: state.rentPole.rentPartDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllItemsMvAction, addRentPartAction, getAllRentPartAction,updateRentPartAction, deleteRentPartAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(AddRentPart);