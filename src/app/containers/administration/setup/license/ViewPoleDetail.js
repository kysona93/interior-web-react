import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table, Button , FormGroup, ControlLabel } from 'react-bootstrap';
import { Field, reduxForm, reset,initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllItemsMvAction,getItemsMvAction } from './../../../../actions/customer/network/meter';
import { getRentPoleAction,getRentPartAction } from './../../../../actions/customer/network/license';
import './../index.css';
import RentPart from './../../../../../app/containers/administration/setup/license/RentPart';
import moment from 'moment';

let rentPartId = 0;
class RentPole extends Component{
    constructor(props){
        super(props);
        this.state = {
        };

    }
    componentDidMount(){
            this.props.getRentPoleAction(this.props.params.id);
    }

    closeRentPart(value){this.setState({ openRentPart:value })}

    render(){
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">View Pole Of Part</h3>
                            </div>
                            <div className="panel-body">
                                <Table responsive condensed bordered hover className="table-list-mv">
                                    <thead >
                                    <tr>
                                        <th>PoleId</th>
                                        <th>Pole Serial</th>
                                        <th>Date Rent</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { this.props.rentPoleGet.items !== undefined ?
                                        this.props.rentPoleGet.items.map((pole, index) => {
                                            return(
                                                <tr key={index}>
                                                    <td className="text-center">{pole.id} </td>
                                                    <td className="text-center">{pole.serial}</td>
                                                    <td className="text-center">{moment(pole.dateRent).format("YYYY-MM-DD")}</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan="8">
                                                <center><h3>RESULT NOT FOUND!</h3></center>
                                            </td>
                                        </tr>
                                    }
                                    </tbody>
                                </Table>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <ul className="button-style">
                        <li><Button className="my-custom" bsStyle="primary" onClick={ ()=>{window.history.back();} }>&nbsp;Back</Button></li>
                    </ul>
                </Row>

            </div>
        )
    }
}
RentPole = reduxForm({
    form: 'form_add_rent_pole',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        return errors
    },
})(RentPole);

function mapStateToProps(state) {
    //console.log("===>", state.rentPole.rentPoleGet);
    return {
        rentPoleGet: state.rentPole.rentPoleGet,
        rentPartGet: state.rentPole.rentPartGet,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getRentPoleAction,getItemsMvAction ,getRentPartAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(RentPole);