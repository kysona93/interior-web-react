import React from 'react';
import {connect} from 'react-redux';
import {browserHistory} from 'react-router';
import { Row, Col, Table, Pagination, FormGroup, ControlLabel,Button} from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from './../../../../components/forms/TextBox';
import { TextArea } from './../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker } from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from './../../../../components/forms/ButtonSubmit';
import { loadCustomer } from './../../../../localstorages/localstorage';
import {getEmployeesAction} from './../../../../actions/employee/employee';
import { addItemMvAction, updateItemMvAction, getItemsMvAction,getAllItemsMvAction,deleteItemMvAction } from './../../../../actions/customer/network/meter';
import EditItemMv from './EditLicense';
import LicenseTab from './LicenseTab';
import { saveLicense,loadLicense } from '../../../../localstorages/localstorage';
import './../index.css';
import moment from 'moment';
let license = {
    name:''
};
class ListLicense extends React.Component{
    constructor(props){
        super(props);
        this.state={
            update: false,
            gander: [{id: "Male", name: "Male"},{id: "FeMale", name: "FeMale"}],
            licenseType: [{id: "CT/VT", name: "CT/VT"}],
            startDate: null,
            mv : {}
        };
        this.FormSubmit = this.FormSubmit.bind(this);
        this.handleRegisterDate=this.handleRegisterDate.bind(this);
    }
    FormSubmit(value){
        license.name= value.name.trim();
        this.props.getAllItemsMvAction(license);
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete ItemMV?") === true){
            alert(JSON.stringify(this.props.deleteItemMvAction(id)));
            this.props.getItemsMvAction();
        }
    }
    componentWillMount(){
        this.props.getAllItemsMvAction(license);
    }

    openUpdate(mv){
        this.setState({ update: true,  mv: mv });
        if(mv !== undefined){
            saveLicense(mv);
        }
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getItemsMvAction();
    }

    handleRegisterDate(date){this.setState({startDate:date})}
    render(){
        const {handleSubmit, error, submitting} = this.props;
        return(
            <div className="container-fluid" style={{marginLeft:'20px'}}>
                {
                    !this.state.update ?
                        <div>
                            <div>
                                <form onSubmit={handleSubmit(this.FormSubmit)} className="wrap-full-form">
                                    <Row>
                                        <Col lgOffset={8} lg={4} className="license">
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>LicenseName</strong>
                                                </Col>
                                                <Col md={5} lg={5}>
                                                    <Field name="name" type="text" component={TextBox} label="Name" />
                                                </Col>
                                                <Col lg={4}>
                                                    <ButtonSubmit error={error} submitting={submitting} label="Search" />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                                <Row>
                                    <Col xs={6} lg={6}> </Col>
                                    <Col xs={6} lg={6}>
                                        <Button className="my-custom pull-right add-license" bsStyle="primary" onClick={ ()=>{ browserHistory.push('/app/administration/setup/licenses/add') }}>&nbsp;Add New</Button>
                                    </Col>
                                </Row>
                                <Row>
                                    <Table responsive condensed bordered hover className="table-list-mv">
                                        <thead >
                                        <tr>
                                            <th>ID</th>
                                            <th>LicenseNameKh</th>
                                            <th>LicenseNameEn</th>
                                            <th>Owner</th>
                                            <th>Gander</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>LicenseNumber</th>
                                            <th>RegisterDate</th>
                                            <th>Region</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        { this.props.itemMvGetAll.items !== undefined ?
                                            this.props.itemMvGetAll.items.map((mv, index) => {
                                                return(
                                                    <tr key={index}>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.id}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}><a onClick ={ () => this.openUpdate(mv) }>{mv.licenseNameKh}</a></td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}><a onClick ={ () => this.openUpdate(mv) }>{mv.licenseNameEn}</a></td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.owner}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.sex}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.phone}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.email}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.address}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.licenseNumber}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{moment(mv.registerDate).format("YYYY-MM-DD")}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>{mv.region}</td>
                                                        <td style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>
                                                            <a onClick ={ () => this.openUpdate(mv) } className='btn btn-info btn-xs'>
                                                                <span className="glyphicon glyphicon-edit"> </span>
                                                            </a>
                                                            &nbsp;
                                                            <a onClick={ () => this.deleteItem(mv.id)} className="btn btn-danger btn-xs">
                                                                <span className="glyphicon glyphicon-remove"> </span>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan="8">
                                                    <center><h3>RESULT NOT FOUND!</h3></center>
                                                </td>
                                            </tr>
                                        }
                                        </tbody>
                                    </Table>
                                </Row>
                        </div>
                        :
                        <EditItemMv closeUpdate={this.closeUpdate.bind(this)} mv={this.state.mv}/>
                }
            </div>
        )
    }
}
ListLicense = reduxForm({
    form: 'form_license'
})(ListLicense);

function mapStateToProps(state) {
    return {
        itemMvGetAll: state.changeMeter.itemMvGetAll,
        itemMvGet: state.changeMeter.itemMvGet,
        itemMvDelete: state.changeMeter.itemMvDelete,
        initialValues: {
            name: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addItemMvAction, updateItemMvAction, getItemsMvAction, deleteItemMvAction,getAllItemsMvAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListLicense);