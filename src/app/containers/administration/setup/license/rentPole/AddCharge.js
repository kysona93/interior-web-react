import React from 'react';
import _ from 'lodash';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../../components/forms/TextBox';
import { DateTimePicker } from '../../../../../components/forms/DateTimePicker';
import { TextArea } from '../../../../../components/forms/TextArea';
import SelectBox from '../../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../../components/forms/ButtonSubmit';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { getAllItemsAction } from '../../../../../actions/inventory/item';
import { getAllInvoiceTypeAction } from './../../../../../actions/invoice/invoiceType';
import { addCustomerInvoiceAction } from './../../../../../actions/invoice/customerInvoice';
import { loadCustomer } from '../../../../../localstorages/localstorage';
import { loadLicense} from '../../../../../localstorages/localstorage';
import ReactLoading from 'react-loading';
import moment from 'moment';

class AddCharge extends React.Component {
    constructor(props){
        super(props);
        this.state={
            itemId: 0,
            items: [],
            itemDetails: [],
            itemCharged: [],
            currency: '',
            invoiceTypes: [],
            invoiceTypeId: 0,
            issueDate: null
        };
        this.handleSelectItem = this.handleSelectItem.bind(this);
        this.handleIssueDate = this.handleIssueDate.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.handleSelectInvoiceType = this.handleSelectInvoiceType.bind(this);
    }

    componentWillMount(){
        this.props.getAllItemsAction('');
        this.props.getAllInvoiceTypeAction();
    }

    componentWillReceiveProps(data){
        // invoice type
        let invoiceTypes = [];
        if(data.invoiceTypeAll.status === 200){
            data.invoiceTypeAll.data.items.map((element) => {
                invoiceTypes.push({
                    id: element.id,
                    name: element.invoiceType
                });
            });
        } else invoiceTypes = [];
        this.setState({invoiceTypes : invoiceTypes});
        if(data.addCustomerInvoice.status === 200){
            document.getElementById('loading').style.display = 'none';
            alert("Successfully issued invoice.");
            data.addCustomerInvoice.status = 0;
            // clear old data
            this.setState({itemCharged: [], itemDetails: [], invoiceTypes: [], currency: "", invoiceTypeId: 0, issueDate: null});
        }
        if(data.addCustomerInvoice.status === 404 || data.addCustomerInvoice.status === 500){
            document.getElementById('loading').style.display = 'none';
            alert("Fail with issue invoice!");
        }
    }

    handleSelectInvoiceType(event){
        this.setState({invoiceTypeId: Number(event.target.value)});
    }

    handleIssueDate(date) {
        this.setState({issueDate: date});
    }

    deleteItem(id){
        if(confirm("Are you sure want to remove this item?") === true){
            this.setState({
                itemCharged: this.state.itemCharged.filter(item => item.id !== id),
                itemDetails: this.state.itemDetails.filter(item => item.itemId !== id)
            });
        }
    }

    handleSelectItem(id){
        let charge = {
            itemId: '',
            unit: '',
            unitPrice: '',
            amount: '',
            description: ''
        };
        if(this.props.itemsAll.items !== undefined){
            const item = this.props.itemsAll.items.find(i => i.id === Number(id));
            if(item !== undefined){
                switch (loadCustomer().consumptionEn){
                    case "Residential":
                        charge.unitPrice = item.price1;
                        break;
                    case "Business":
                        charge.unitPrice = item.price2;
                        break;
                    default:
                        charge.unitPrice = item.price3;
                        break;
                }
                charge.unit = 1;
                charge.description = item.itemNameEn;
                this.setState({currency: item.currency, itemId: item.id})
            }
        }
        this.props.dispatch(initialize('form_add_charge', charge));
    }

    handleAddItems(values){
        alert(JSON.stringify(
            values
        ));
        let item = {
            id : Number(this.state.itemId),
            itemName: '',
            customerType: loadCustomer().consumptionEn,
            unit: Number(values.unit),
            currency: this.state.currency,
            unitPrice: Number(values.unitPrice),
            description: values.description,
            subTotalUSD: 0,
            subTotalRIEL: 0
        };
        if(this.state.currency === "USD"){
            item.subTotalUSD = item.unit * item.unitPrice;
        } else{
            item.subTotalRIEL = item.unit * item.unitPrice;
        }
        if(this.props.itemsAll.items !== undefined) {
            const item = this.props.itemsAll.items.find(i => i.id === Number(values.itemId));
            if (item !== undefined) {
                item.itemName = item.itemNameEn;
            }
        }
        let itemDetail = {
            itemId : Number(this.state.itemId),
            unit: item.unit,
            unitPrice: item.unitPrice,
            amountRiel: item.subTotalRIEL,
            amountUSD: item.subTotalUSD,
            description: item.description,
            workType: values.workType,
            status: 1
        };
        this.setState({itemCharged: this.state.itemCharged.concat(item), itemDetails: this.state.itemDetails.concat(itemDetail)});
        this.props.reset('form_add_charge');
    }

    handleIssueItem(){
        if(this.state.itemCharged.length <= 0 ){
            alert("Please add items first !");
        }else{
            if(this.state.issueDate === null || this.state.invoiceTypeId === 0){
                alert("Please select Issue Date, Invoice Type and Work Type !!")
            }else{
                const date = new Date(this.state.issueDate);
                const endBillingDate = date.setMonth(date.getMonth() + 1);
                let item = {
                    invoiceTypeId: Number(this.state.invoiceTypeId),
                    description: "First connection",
                    issueDate: moment(this.state.issueDate).format("YYYY-MM-DD"),
                    startBillingDate: moment(this.state.issueDate).format("YYYY-MM-DD"),
                    endBillingDate: moment(endBillingDate).format("YYYY-MM-DD"),
                    invoiceAmountRiel: 0,
                    invoiceAmountUSD: 0,
                    paidAmountRiel: 0,
                    paidAmountUSD: 0,
                    unpaidAmountRiel: 0,
                    unpaidAmountUSD: 0,
                    isVoid: 0,
                    status: 1,
                    invoiceDetailForms: this.state.itemDetails,
                    customerId :0,
                    licenseId:loadLicense().id
                };
                for(let i=0; i < this.state.itemCharged.length; i++){
                    item.invoiceAmountRiel = item.invoiceAmountRiel + this.state.itemCharged[i].subTotalRIEL;
                    item.invoiceAmountUSD = item.invoiceAmountUSD + this.state.itemCharged[i].subTotalUSD;
                }
                item.unpaidAmountUSD = item.invoiceAmountUSD;
                item.unpaidAmountRiel = item.invoiceAmountRiel;
                console.log("final : ", item);
                document.getElementById('loading').style.display = 'block';
                this.props.addCustomerInvoiceAction(item);
            }
        }
    }

    render(){
        let totalRIEL = 0, totalUSD = 0;
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Add Charge for License: {loadLicense().id}</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleAddItems.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Item Charged <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { this.handleSelectItem(_.map(selected, "id")) }}
                                                labelKey="itemNameEn"
                                                options={this.props.itemsAll.items || []}
                                                placeholder="Please select ..."
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Quantity <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="unit" type="text" component={TextBox} label="Quantity" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Unit Price <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={6} lg={6}>
                                            <Field name="unitPrice" type="text" component={TextBox} label="Unit price" icon="fa fa-money"/>
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <strong>{this.state.currency}</strong>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Work Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="workType" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[
                                                       {id: "Rent Pole", name: "Rent Pole"},
                                                       {id: "Other", name: "Other"}
                                                   ]}
                                                   sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label="Add Item" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
                <hr/>
                <Row>
                    <Col xs={3} sm={3} md={3} lg={3}>
                        <strong>Issue Date <span className="label-require">*</span></strong>
                        <Field name="issueDate" component={DateTimePicker} placeholder="Issue Date"
                               defaultDate={this.state.issueDate} handleChange={this.handleIssueDate}/>
                    </Col>
                    <Col xs={3} sm={3} md={3} lg={3}>
                        <strong>Invoice Type <span className="label-require">*</span></strong>
                        <Field name="invoiceType" type="select" component={SelectBox} placeholder="Please select ..."
                               onChange = {this.handleSelectInvoiceType}
                               values={this.state.invoiceTypes}
                               sortBy="name" />
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <Row>
                    <Col lg={12}>
                        <div className="custyle">
                            <table className="page-permision-table">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Item Charged</th>
                                    <th>Customer Type</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Description</th>
                                    <th>Sub Total USD</th>
                                    <th>Sub Total RIEL</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.state.itemCharged.length > 0 ?
                                    <tbody>
                                    { this.state.itemCharged.map((item, index)=>{
                                        totalRIEL = totalRIEL + item.subTotalRIEL;
                                        totalUSD = totalUSD + item.subTotalUSD;
                                        return(
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{item.itemName}</td>
                                                <td>{item.customerType}</td>
                                                <td>{item.unit}</td>
                                                <td>{item.unitPrice}</td>
                                                <td>{item.description}</td>
                                                <td>{item.subTotalUSD}</td>
                                                <td>{item.subTotalRIEL}</td>
                                                <td className="text-center">
                                                    <a onClick={()=> this.deleteItem(item.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    <tr>
                                        <td colSpan={6}><b>Total</b></td>
                                        <td><b>{'$ ' + totalUSD}</b></td>
                                        <td><b>{totalRIEL + ' RIEL'}</b></td>
                                        <td> </td>
                                    </tr>
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={9}><h2>NO DATA</h2></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
                <br/>
                <Row>
                    <Col lgOffset={10} xs={2} sm={2} md={2} lg={2}>
                        <Button bsStyle="primary" onClick={() => this.handleIssueItem()}>Issue Item(s)</Button>
                    </Col>
                </Row>
            </div>
        )
    }
}


AddCharge = reduxForm({
    form: 'form_add_charge',
    validate: (values) => {
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if (values.itemId === undefined) {
            errors.itemId = 'Item is required !';
        }
        if(!regex_price.test(values.unitPrice) || values.unitPrice === null){
            errors.unitPrice = "Invalid unit price !";
        }
        if(!regex_number.test(values.unit)){
            errors.unit = "Invalid quantity !";
        }
        if (values.workType === undefined) {
            errors.workType = 'Work Type is required !';
        }
        return errors
    }
})(AddCharge);

function mapStateToProps(state) {
    //console.log("DATA : ", state.item.itemsAll);
    return {
        itemsAll: state.item.itemsAll,
        invoiceTypeAll: state.invoiceType.invoiceTypeAll,
        addCustomerInvoice: state.customerInvoices.addCustomerInvoice
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllInvoiceTypeAction, getAllItemsAction, addCustomerInvoiceAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddCharge);