import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, Table } from 'react-bootstrap';
import { getInvoiceDetailRentPoleAction } from './../../../../../actions/invoice/customerInvoice';
import moment from 'moment';

let detail = [];
class PrintInvoiceAddCharge extends React.Component {
    constructor(props){
        super(props);
    }

    componentDidMount(){
        this.props.getInvoiceDetailRentPoleAction(this.props.params.invoiceId)
    }

    componentWillReceiveProps(data){
        if(data.getAllInvoiceRentPole.status === 200){
            detail = data.getAllInvoiceRentPole.data.items[0];
            setInterval(function(){
                window.print();
            }, 500);
        }
        if(data.getAllInvoiceRentPole.status === 401 || data.getAllInvoiceRentPole.status === 404 || data.getAllInvoiceRentPole.status === 500){
            detail = [];
        }
    }

    render(){
        let amountRiel = 0;
        let amountUSD = 0;
        return (
            <div style={{margin: '25px'}}>
                <h2 style={{textAlign: 'center'}}>ក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឹលូសិន ខបភើរេសិន</h2>
                <br/>

                <p>លេខអត្តសញ្ញាណកម្ម អតប(VAT TIN): K002-901501127</p>
                <p>អាស័យដ្ធាន៖ ភូមិពោធិ២ ឃុំដូនកឹង ស្រុកំចាយមារ ខេត្តព្រៃវែង</p>
                <p>ទូរសព្ទ័លេខ៖ 043 63 63 186, 077 54 54 54, 097 91 71 934</p>
                <div style={{textAlign: 'center'}}>
                    <h3 style={{fontWeight: 'bold'}}>វិក័យបត្រ៖</h3>
                    <h3 style={{fontWeight: 'bold'}}>INVOICE:</h3>
                </div>

                <Row>
                    <Col md={4} lg={4} className="pull-left">
                        <p>ក្រុមហ៊ុនឬអតិថិជន៖ {detail.customerName}</p>
                        <p>Company name/Customer :</p>
                        <p>ទូរស័ព្ទលេខ៖ {detail.phone}</p>
                        <p>Phone Number:</p>
                    </Col>
                    <Col md={5} lg={5}></Col>
                    <Col md={3} lg={3} className="pull-right">
                        <p>លេខរៀងវិក័យបត្រ៖{detail.invoiceNo}</p>
                        <p>Invoice No:</p>
                        <p>កាលបរិច្ឆេត៖ {moment(detail.issueDate).format("DD-MM-YYYY")}</p>
                        <p>Date: </p>
                    </Col>
                </Row>
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <Table striped bordered condensed className="normal-invoice">
                                <thead>
                                <tr>
                                    <th>ល.រ</th>
                                    <th>បរិយាយ</th>
                                    <th>បរិមាណ</th>
                                    <th>តំលៃឯកតា</th>
                                    <th>សរុប</th>
                                </tr>
                                </thead>
                                {
                                    this.props.getAllInvoiceRentPole.status === 200?
                                        <tbody>
                                        {
                                            this.props.getAllInvoiceRentPole.data.items.map((detail, index) => {
                                                amountRiel = amountRiel + detail.amountRiel;
                                                amountUSD = amountUSD + detail.amountUSD;
                                                return (
                                                    <tr key={index}>
                                                        <td>{index+1}</td>
                                                        <td>{detail.itemNameKh}</td>
                                                        <td>{detail.unit}</td>
                                                        <td>{detail.currency === 'USD' ? ('$ ' + detail.unitPrice):('៛  ' + detail.unitPrice) }</td>
                                                        <td>{detail.currency === 'USD' ? ("$ "+detail.amountUSD): ("៛  "+detail.amountRiel)}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        <tr>
                                            <td colSpan={4}><p style={{fontWeight: 'bold', float: 'right'}}>Total(Vat Included)</p></td>
                                            <td colSpan={4}><p style={{fontWeight: 'bold'}}>{amountRiel + "៛ និង​ $ " + amountUSD}</p></td>
                                        </tr>
                                        </tbody>
                                        :
                                        null
                                }
                            </Table>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={4} lg={4} className="pull-left">
                            <center>
                                <hr style={{fontWeight: 'bold'}}/>
                                <p>ហត្ថលេខានិងឈ្មោះអតិថិជន</p>
                                <p>Customer's Signature and Name</p>
                            </center>
                        </Col>
                        <Col md={4} lg={4}></Col>
                        <Col md={4} lg={4} className="pull-right">
                            <center>
                                <hr style={{fontWeight: 'bold'}}/>
                                <p>ហត្ថលេខានិងឈ្មោះអ្នកលក់</p>
                                <p>Seller's Signature and Name</p>
                            </center>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    //console.log("---", state.customerInvoices.getAllInvoiceRentPole);
    return {
        getAllInvoiceRentPole: state.customerInvoices.getAllInvoiceRentPole
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getInvoiceDetailRentPoleAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PrintInvoiceAddCharge);