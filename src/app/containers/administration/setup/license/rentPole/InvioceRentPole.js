import React from 'react';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import { Row, Col, Pagination } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { getAllLicenseInvoiceNoAction, voidCustomerInvoiceAction } from './../../../../../actions/invoice/customerInvoice';
import { loadLicense } from '../../../../../localstorages/localstorage';

class InvoiceRentPole extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            activePage: 1
        };
        this.voidInvoice = this.voidInvoice.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handlePrint = this.handlePrint.bind(this);
    }

    componentDidMount(){
        this.props.getAllLicenseInvoiceNoAction({
            page : 1,
            limit : 10,
            licenseId : loadLicense().id !== undefined ? loadLicense().id : '',
            orderBy : "issueDate",
            orderType : "desc"
        });
    }

    componentWillReceiveProps(data){
        if(data.voidCustomerInvoice.status === 200){
            alert("Successfully void this invoice.");
            this.props.getAllLicenseInvoiceNoAction({
                page : 1,
                limit : 10,
                licenseId : loadLicense().id !== undefined ? loadLicense().id : '',
                orderBy : "issueDate",
                orderType : "desc"
            });
            data.voidCustomerInvoice.status = 0;
        }
        if(data.voidCustomerInvoice.status === 400 || data.voidCustomerInvoice.status === 500){
            alert("Fail with void this invoice!");
        }
    }

    handlePrint(invoiceId, issueDate, typeInvoice){
        if(typeInvoice === "ITEM"){
            window.open("/print-rent-pole-invoice/"+invoiceId);
        }else{
        }
    }

    handleSelect(eventKey) {
        this.setState({
            activePage: eventKey
        });
        criteria.page = eventKey;
        this.props.listAllBillingItemsAction(billing);
    }

    static handleItem(total) {
        if (total <= 10) {
            return 1
        } else if (total % 10 == 0) {
            return total / 10
        } else if (total % 10 > 0) {
            return parseInt(total/10) + 1
        }
    }

    voidInvoice(id){
        if(confirm("Are you sure want to void this invoice?") === true){
            this.props.voidCustomerInvoiceAction(id);
        }
    }

    render(){
        let total = 0;
        return(
            <div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="">
                            <table className="list-smaller-table">
                                <thead>
                                <tr>
                                    <th>Invoice No</th>
                                    <th>Issue Date</th>
                                    <th>Start Billing Date</th>
                                    <th>End Billing Date</th>
                                    <th>Amount(RIEL)</th>
                                    <th>Amount(USD)</th>
                                    <th>Invoice Amount(RIEL)</th>
                                    <th>Invoice Amount(USD)</th>
                                    <th>Brought Forward Balance(RIEL)</th>
                                    <th>Brought Forward Balance(USD)</th>
                                    <th>Unpaid Amount(RIEL)</th>
                                    <th>Unpaid Amount(USD)</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.getAllLicenseInvoiceNo.status === 200 ?
                                        <tbody>
                                        { this.props.getAllLicenseInvoiceNo.data.items.list.map((invoice,index)=>{
                                          total = this.props.getAllLicenseInvoiceNo.data.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td><Link onClick={() => this.handlePrint(invoice.invoiceId, moment(invoice.issueDate).format("YYYY-MM-DD") ,invoice.typeInvoice)}>{invoice.invoiceNo}</Link></td>
                                                        <td>{moment(invoice.issueDate).format("YYYY-MM-DD")}</td>
                                                        <td>{moment(invoice.startBillingDate).format("YYYY-MM-DD")}</td>
                                                        <td>{moment(invoice.endBillingDate).format("YYYY-MM-DD")}</td>
                                                        <td>{invoice.amountRiel}</td>
                                                        <td>{invoice.amountUSD}</td>
                                                        <td>{invoice.invoiceAmountRiel}</td>
                                                        <td>{invoice.invoiceAmountUSD}</td>
                                                        <td>{invoice.broughtForwardBalanceRiel}</td>
                                                        <td>{invoice.broughtForwardBalanceUSD}</td>
                                                        <td>{invoice.unpaidAmountRiel}</td>
                                                        <td>{invoice.unpaidAmountUSD}</td>
                                                        <td className="text-center">
                                                            {
                                                                invoice.isVoid === 0 ?
                                                                    <a onClick={()=> this.voidInvoice(invoice.invoiceId)} href="#" className="btn btn-warning btn-xs">
                                                                        <span>Void</span>
                                                                    </a>
                                                                    :
                                                                    <h5 style={{color: 'red', fontWeight: 'bold'}}>Broken</h5>

                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            }
                                        )}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan = {13}><h5 style={{textAlign: 'center'}}>RESULT NOT FOUND </h5></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                            {total <= 10 ? null
                                :
                                <Pagination style={{ float: 'right'}}
                                            prev
                                            next
                                            first
                                            last
                                            ellipsis
                                            boundaryLinks
                                            items={ListCustomerInvoice.handleItem(total)}
                                            maxButtons={5}
                                            activePage={this.state.activePage}
                                            onSelect={this.handleSelect}
                                />
                            }
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

function mapStateToProps(state) {
    //console.log("DATA : ", state.customerInvoices.getAllLicenseInvoiceNo);
    return {
        voidCustomerInvoice: state.customerInvoices.voidCustomerInvoice,
        getAllLicenseInvoiceNo: state.customerInvoices.getAllLicenseInvoiceNo
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllLicenseInvoiceNoAction,voidCustomerInvoiceAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InvoiceRentPole);