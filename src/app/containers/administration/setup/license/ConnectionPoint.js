import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table, Button , FormGroup, ControlLabel } from 'react-bootstrap';
import { Field, reduxForm, reset,initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getConnectionPointAction } from './../../../../actions/customer/network/meter';
import { getRentPoleAction,getRentPartAction } from './../../../../actions/customer/network/license';
import './../index.css';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import RentPart from './../../../../../app/containers/administration/setup/license/RentPart';
import moment from 'moment';

let connection = {
    licenseId : 2,
    startDate: 0,
    endDate: 0

};
class ConnectionPoint extends Component{
    constructor(props){
        super(props);
        this.state = {
            startDate:null,
            endDate: null
        };
        this.handleSubmit =this.handleSubmit.bind(this);
        this.handleStartDate = this.handleStartDate.bind(this);
        this.handleEndDate = this.handleEndDate.bind(this);
    }

    handleStartDate(date){
        this.setState({ startDate: date })
    }
    handleEndDate(date){
        this.setState({ endDate: date })
    }
    componentWillMount(){
        //this.props.getConnectionPointAction(connection)
    }

    componentWillReceiveProps(data){
    }
    handleSubmit(value){
        connection.licenseId =2;
        connection.startDate=value.startDate;
        connection.endDate= value.endDate;
        this.props.getConnectionPointAction(connection)
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                    <Row>
                        <Col md={6} lg={6} className="pull-right">
                            <Row>
                                <Col md={5} lg={5}>
                                    <Field name="startDate" component={DateTimePicker}
                                           placeholder="start Date"
                                           defaultDate={this.state.startDate}
                                           handleChange={this.handleStartDate}/>
                                </Col>
                                <Col md={5} lg={5}>
                                    <Field name="endDate" component={DateTimePicker}
                                           placeholder="end Date"
                                           defaultDate={this.state.endDate}
                                           handleChange={this.handleEndDate}/>
                                </Col>
                                <Col  md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Search"/>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                <div className="row">
                    <Table responsive condensed bordered hover className="table-list-mv" id="connection-point-to-xls">
                        <thead >
                            <tr>
                                <th>ID</th>
                                <th>Village</th>
                                <th>Serial</th>
                                <th>RegisterDate</th>
                                <th>Pole</th>
                                <th>Lat</th>
                                <th>Long</th>
                                <th>Ampere</th>
                            </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.connectionPointGet.items != undefined ?
                                this.props.connectionPointGet.items.map((connection,index) =>{
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{connection.customerId}</td>
                                            <td className="text-center">{connection.areaKh}</td>
                                            <td className="text-center">{connection.meterSerial}</td>
                                            <td className="text-center">{moment(connection.registerDate).format("YYYY-MM-DD")}</td>
                                            <td className="text-center">{connection.poleSerial}</td>
                                            <td className="text-center">{connection.lat}</td>
                                            <td className="text-center">{connection.log}</td>
                                            <td className="text-center">{connection.ampere}</td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={11}><h2>RESULT NOT FOUND</h2></td>
                                </tr>
                        }
                        </tbody>
                    </Table>
                </div>
                <Row>
                    <ul className="button-style">
                       {/**
                        *  <li><Button className="my-custom" bsStyle="primary">&nbsp;Print</Button></li>
                        */}
                        <li><ReactHTMLTableToExcel
                            className="my-custom-export"
                            table="connection-point-to-xls"
                            filename="connection-Point"
                            sheet="connection-point"
                            buttonText="Export"/></li>
                    </ul>
                </Row>
                </form>
            </div>
        )
    }
}
ConnectionPoint = reduxForm({
    form: 'form_add_rent_pole',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        return errors
    },
})(ConnectionPoint);

function mapStateToProps(state) {
    //console.log("===>", state.changeMeter.connectionPointGet);
    return {
        connectionPointGet: state.changeMeter.connectionPointGet
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getConnectionPointAction },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(ConnectionPoint);