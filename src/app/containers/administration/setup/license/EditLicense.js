import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Table, Button , FormGroup, ControlLabel} from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { loadCustomer } from '../../../../localstorages/localstorage';
import {getEmployeesAction} from '../../../../actions/employee/employee';
import { updateItemMvAction, getItemsMvAction, getItemMvSummaryAction } from '../../../../actions/customer/network/meter';
import SummaryKw from './SummaryKw';
import RentPole from './../../../../containers/administration/setup/license/RentPole';
import InvoiceRentPole from '../../../../../app/containers/administration/setup/license/rentPole/InvioceRentPole';
import ConnectionPoint from './ConnectionPoint';
import moment from 'moment';
import AddCharge from './rentPole/AddCharge';
import { loadLicense} from '../../../../localstorages/localstorage';
class EditItemMv extends React.Component{
    constructor(props){
        super(props);
        this.state={
             tabIndex: 0 ,
            gander: [{id: "Male", name: "Male"}, {id: "FeMale", name: "FeMale"}],
            licenseType: [{id: "CT/VT", name: "CT/VT"}],
            startDate: moment(this.props.mv.installDate)
        };
        this.handleRegisterDate=this.handleRegisterDate.bind(this);
    }

    componentWillMount(){
        this.props.getItemMvSummaryAction(loadLicense().id);
        this.props.getItemsMvAction(loadLicense().id);
        this.props.dispatch(initialize('form_edit_item_mv', this.props.mv));

    }
    componentWillReceiveProps(data){
        if(data.itemMVUpdate.status === 200){
            alert("success update ItemMV");
            this.closeUpdate();
            this.props.initialize("form_edit_item_mv", this.props.mv);
            data.itemMVUpdate.status = 0
        }else if(data.itemMVUpdate.status === 404 || data.itemMVUpdate.status === 500){
            alert("update ItemMv Fail !!")
        }
    }

    handleSubmit(values){
            this.props.updateItemMvAction({
                id: Number(this.props.mv.id),
                licenseNameKh: values.licenseNameKh,
                licenseNameEn: values.licenseNameEn,
                licenseNumber: values.licenseNumber,
                registerDate: values.registerDate,
                region: values.region,
                address: values.address,
                licenseType: values.licenseType,
                owner: values.owner,
                sex: values.sex,
                phone: values.phone,
                email: values.email
            });
    }
    closeUpdate(){
        this.props.closeUpdate(false);
    }

    handleRegisterDate(date){this.setState({startDate:date})}
    render(){
        const itemMvGet = this.props.itemMvGet;
        const {handleSubmit, error, invalid, submitting} = this.props;
        let employees = [];
        if(this.props.employees.items !== undefined){
            this.props.employees.items.list.map((emp) => {
                employees.push({
                    name: emp.nameKh,
                    id: emp.id
                })
            });
        }
        const renderLicenseInf=()=>{
            return(
                itemMvGet.items !== undefined ?
                <tbody>
                <tr>
                    <td>LicenseNumber: {itemMvGet.items.licenseNumber}</td>
                    <td>LicenseNameKh: {itemMvGet.items.licenseNameKh}</td>
                    <td>LicenseNameEn: {itemMvGet.items.licenseNameEn}</td>
                    <td>NumberOfPoint: {itemMvGet.items.totalCustomer}</td>
                </tr>
                </tbody>
                :null
            )
        };

        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <ul className="nav nav-tabs search-tab" role="tablist">
                                <li role="presentation" className="active"><a href="#customer-info" aria-controls="customer-info" role="tab" data-toggle="tab">License Information</a></li>
                                <li role="presentation"><a href="#network-management" aria-controls="network" role="tab" data-toggle="tab">Summary KWH</a></li>
                                <li role="presentation"><a href="#finance" aria-controls="finance" role="tab" data-toggle="tab">Finance</a></li>
                                <li role="presentation"><a href="#user-history" aria-controls="history" role="tab" data-toggle="tab">Rent Pole</a></li>
                                <li role="presentation"><a href="#user-graph" aria-controls="usergraph" role="tab" data-toggle="tab">ConnectionPoint</a></li>
                               {/**
                                * <li role="presentation"><a href="#audit-trial" aria-controls="audittrial" role="tab" data-toggle="tab">Audit Trial</a></li>
                                */} 
                            </ul>
                            <div className="search-tab tab-content">
                                <div role="tabpanel" className="tab-pane active" id="customer-info">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="panel panel-default">
                                            <div className="panel-heading">
                                                <h3 className="panel-title"><span style={{color: 'blue', fontSize: '20px'}}>Customer Information</span></h3>
                                            </div>
                                            <div className="panel-body">
                                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                                    <Row>
                                                        <Col lg={6}>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>licenseNameKh : <span className="label-require">*</span></strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="licenseNameKh" type="text" component={TextBox}
                                                                           label="license Name Kh"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>licenseNameEn : <span className="label-require">*</span></strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="licenseNameEn" type="text" component={TextBox}
                                                                           label="license Name En"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>license No : <span className="label-require">*</span></strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="licenseNumber" type="text" component={TextBox}
                                                                           label="license No"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>licenseType : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="licenseType" type="select" component={SelectBox}
                                                                           placeholder="licenseType"
                                                                           values={this.state.licenseType} sortBy="name"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong> Region : <span className="label-require">*</span></strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="region" type="text" component={TextBox} label="Region"/>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong> Address : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="address" type="text" component={TextBox} label="address"/>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>Register Date : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="registerDate" component={DateTimePicker}
                                                                           placeholder="Install Date"
                                                                           defaultDate={this.state.startDate}
                                                                           handleChange={this.handleRegisterDate}/>
                                                                </Col>
                                                            </Row>
                                                        </Col>

                                                        <Col lg={6}>

                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong> owner : <span className="label-require">*</span></strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="owner" type="text" component={TextBox} label="owner"/>
                                                                </Col>
                                                            </Row>

                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong> Gander : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="sex" type="select" component={SelectBox} placeholder="gander"
                                                                           values={this.state.gander} sortBy="name"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>Phone : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="phone" type="text" component={TextBox} label="phone"/>
                                                                </Col>
                                                            </Row>
                                                            <Row>
                                                                <Col md={4} lg={4} className="label-name">
                                                                    <strong>Email : </strong>
                                                                </Col>
                                                                <Col md={8} lg={8}>
                                                                    <Field name="email" type="text" component={TextBox} label="email"/>
                                                                </Col>
                                                            </Row>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col mdOffset={6} lgOffset={6} md={3} lg={3}>
                                                            <Button bsStyle="primary" onClick={this.closeUpdate.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                                        </Col>
                                                        <Col  md={3} lg={3}>
                                                            <ButtonSubmit error={error} submitting={submitting} label="រក្សាទុក/Save"/>
                                                        </Col>
                                                    </Row>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="network-management">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <SummaryKw/>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="finance">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="panel with-nav-tabs panel-default installing">
                                                <div className="panel-heading installing">
                                                    <ul className="nav nav-tabs installing">
                                                        <li className="active"><a href="#tabinvoice" data-toggle="tab">Invoices</a></li>
                                                        <li className="dropdown">
                                                            <a href="#" data-toggle="dropdown">Payment<span className="caret"></span></a>
                                                            <ul className="dropdown-menu" role="menu">
                                                                <li><a href="#tabbookpayment" data-toggle="tab">Book Payment</a></li>
                                                                <li><a href="#tabbookdepreciation" data-toggle="tab">Book Depreciation</a></li>
                                                            </ul>
                                                        </li>
                                                        <li><a href="#tabaddcharge" data-toggle="tab">Add Charge</a></li>
                                                    </ul>
                                                </div>
                                                <div className="panel-body">
                                                    <div className="tab-content">
                                                        <div className="tab-pane fade in active" id="tabinvoice">
                                                                <InvoiceRentPole/>
                                                        </div>
                                                        <div className="tab-pane fade" id="tabbookpayment">
                                                            Book Payment
                                                        </div>

                                                        <div className="tab-pane fade" id="tabbookdepreciation">
                                                            Book Depricate
                                                        </div>
                                                        <div className="tab-pane fade" id="tabaddcharge">
                                                           <AddCharge/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="user-history">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <RentPole />
                                </div>
                                <div role="tabpanel" className="tab-pane" id="user-graph">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                   <div className="row">
                                       <div className="col-xs-12 col-sm-12 col-md-12">
                                       </div>
                                   </div>
                                    <ConnectionPoint/>
                                </div>
                                <div role="tabpanel" className="tab-pane" id="audit-trial">
                                    <div className="row customer-information">
                                        <br/>
                                        <div className="col-xs-12 col-sm-12 col-md-12">
                                            <table className="table table-inverse customer-information">
                                                {renderLicenseInf()}
                                            </table>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <Table responsive condensed bordered hover className="table-list-mv">
                                            <thead >
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Context</th>
                                                    <th>DateTime</th>
                                                    <th>Description</th>
                                                    <th>UserName</th>
                                                    <th>IpAddress</th>
                                                    <th>HostName</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                    <td className="text-center"> </td>
                                                </tr>
                                            </tbody>
                                        </Table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
EditItemMv = reduxForm({
    form: 'form_edit_item_mv',
    validate: (values) => {
        const errors = {};
        if(values.licenseName === "" || values.licenseName == undefined){
            errors.licenseName = "Field Required !!"
        }
        if(values.installBy === "" || values.installBy == undefined){
            errors.installBy = "Field Required !!"
        }
        if(values.installDate === "" || values.installDate == undefined){
            errors.installDate = "Field Required !!"
        }
        if(values.typeOfConnection === "" || values.typeOfConnection == undefined){
            errors.typeOfConnection = "Field Required !!"
        }
        if(values.capacity === "" || values.capacity == undefined){
            errors.capacity = "Field Required !!"
        }
        if(values.region === "" || values.region == undefined){
            errors.region = "Field Required !!"
        }

        return errors
    }
})(EditItemMv);

function mapStateToProps(state) {
    //console.log("mv:",state.changeMeter.itemMvGet);
    return {
        itemMVUpdate: state.changeMeter.itemMVUpdate,
        itemMvSummaryGet: state.changeMeter.itemMvSummaryGet,
        itemMvGet: state.changeMeter.itemMvGet,
        employees: state.employee.employees
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ updateItemMvAction, getItemsMvAction,getItemMvSummaryAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditItemMv);