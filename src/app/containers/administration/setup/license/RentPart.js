import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Row, Col,Button } from 'react-bootstrap';
import { Field, reduxForm, reset,initialize } from 'redux-form';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllItemsMvAction,getItemsMvAction,getItemMvSummaryAction } from './../../../../actions/customer/network/meter';
import { addRentPartAction,deleteRentPartAction,updateRentPartAction,getAllRentPartAction } from './../../../../actions/customer/network/license';
import { getAreasAction } from './../../../../actions/setup/area';
import moment from 'moment';
import { loadLicense} from '../../../../localstorages/localstorage';

function onRowSelect(row, isSelected) {
    if(isSelected){
        poles = poles.concat(row);
    } else poles = poles.filter(pole => pole.id !== row.id);
    //console.log(poles.length)
}

function onSelectAll(isSelected,rows) {
    poles = [];
    if(isSelected){
        poles= rows;
    }else poles= [];
   //console.log(poles.length);
}

function onAfterTableComplete() {
    //console.log('Table render complete.');
}


function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}



let newPoles= [];
let poles =[];
let dateRent = '';
let licenseId = 0;
class RentPart extends Component{
    constructor(props){
        super(props);
        this.state = {
            licenseId: 0,
            areaId: 0,
            update: false,
            label: 'Save',
            labelForm: 'New RentPart',
            dateRent: null,
            newPoles: [],
            poles:[]
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleRentDate(date){
        this.setState({dateRent: date});
        dateRent = moment(date).format("YYYY-MM-DD");
    }

    componentWillMount(){
        this.props.getAllItemsMvAction();
        this.props.getAllRentPartAction(0);
        //this.props.getRentPartAction();
        this.props.getAreasAction();
    }
    updateItems(data){
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Rent Part', licenseId: data.rentPartId});
        this.props.dispatch(initialize('form_add_rent_part', data));
    }
    deleteItem(id){
        if(confirm("Are you sure want to delete this Rent Part?") === true){
            alert(JSON.stringify(
                this.props.deleteRentPartAction(id)
            ))
        }
    }
    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Rent Part'});
        this.props.dispatch(initialize('form_add_rent_part', null));
    }
    handleSubmit(value){
        if(this.state.newPoles.length > 0){
            let items = [];
            this.state.newPoles.forEach((element) => {
                items.push({
                    dateRent : dateRent,
                    licenseId : loadLicense().id,
                    poleId: element.id
                })
            });
            this.props.addRentPartAction(items);
        }
    }

    componentWillReceiveProps(data){

        if (data.rentPartAdd.status === 200) {
            alert("Successful add new RentPart !!");
            this.props.dispatch(reset('form_add_rent_part'));
            location.href = "/app/administration/setup/licenses";
        } else if(data.rentPartAdd.status === (400 || 404) || data.rentPartAdd.status === (500 || 502)){
            alert(data.rentPartAdd.status + " Error !! \nFail with add RentPart !!\n" + data.rentPartAdd.data.message);
        } else if(data.rentPartAdd.status === 401){
            alert("Please Login !!")
        }
        data.rentPartAdd.status = 0;
        if(data.rentPartUpdate.status === 200 ){
            alert("Successfully updated Rent Part.");
            this.handleCancel();
            this.props.getAllRentPartAction();
            data.rentPartUpdate.status = 0;
        }
        if(data.rentPartUpdate.status === 400 || data.rentPartUpdate.status === 500){
            alert("Fail with update Rent Part!");
        }

        if(data.rentPartDelete.status === 200){
            alert("Successfully deleted this Rent Part.");
            this.props.getAllRentPartAction();
            data.rentPartDelete.status = 0;
        }
        if(data.rentPartDelete.status === 404 || data.rentPartDelete.status === 500){
            alert("Fail with delete this Rent Part!");
        }
    }
    handleAdd(){
        if(poles.length <= 0){
            alert("Please select pole !")
        } else {
            poles.forEach((element) => {
                newPoles.push({
                    serial: element.serial,
                    id:element.poleId
                });
            });
        }
        this.setState({newPoles: newPoles, poles:poles});
        poles = [];
    }

    handleRemove(id){
        this.setState({newPoles: this.state.newPoles.filter(item => item.id !== id)});
    }

    selectArea(event){
        if(event.target.value > 0){
            this.props.getAllRentPartAction(event.target.value);
        }else {
            this.props.getAllRentPartAction(0);
        }
    }

    closeRentPart(){
        this.props.closeRentPart(false);
    }
    render(){
        const { handleSubmit, error, submitting} = this.props;
        let areas = [];
        if(this.props.areaGet.items !== undefined){
            this.props.areaGet.items.map((area)=>{
                areas.push({
                    id:area.id,
                    name:area.areaNameKh
                })
            })
        }

        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Add Rent Part</strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Area Name<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={5} lg={5}>
                                            <Field name="area" type="select" component={SelectBox} placeholder="Please select..." values={areas} sortBy="name"
                                            onChange={this.selectArea.bind(this)} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <BootstrapTable
                                                data={this.props.rentPartGetAll.items} //list of records
                                                trClassName={trClassNameFormat}  //apply style on cell
                                                headerStyle={{background: "#d5dee5"}}
                                                selectRow={{
                                                mode: 'checkbox',
                                                clickToSelect: true,
                                                selected: [],
                                                bgColor: 'rgb(238, 193, 213)',
                                                onSelect: onRowSelect,
                                                onSelectAll: onSelectAll
                                            }} //apply function to select record
                                                cellEdit={{
                                                mode: 'click',
                                                blurToSave: true,
                                                afterSaveCell: this.onAfterSaveCell
                                            }} // apply function to edit cell
                                                options={{
                                                paginationShowsTotal: false,
                                                sortName: 'poleId',  // default sort column name
                                                sortOrder: 'desc',  // default sort order
                                                onDeleteRow: this.onDeleteRow
                                            }} //
                                                hover // apply hover style on row
                                            >
                                                <TableHeaderColumn dataField='poleId' width="7%" dataAlign='center' dataSort isKey autoValue>ID</TableHeaderColumn>
                                                <TableHeaderColumn dataField='serial' width="10%" className='good' dataSort >Pole Serial</TableHeaderColumn>
                                                <TableHeaderColumn dataField='poleHeight' width="14%" className='good' dataSort>Pole Height</TableHeaderColumn>
                                                <TableHeaderColumn dataField='latitude' width="14%" className='good' dataSort>Latitude</TableHeaderColumn>
                                                <TableHeaderColumn dataField='longitude' width="10%" dataAlign='center'>Longitude</TableHeaderColumn>

                                            </BootstrapTable>
                                            <Button bsStyle="primary" style={{float: 'right'}} onClick={this.handleAdd.bind(this)}>add</Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs={12} sm={12} md={12} lg={12}>
                                            <div>
                                                <table className="table table-bordered table-striped installer">
                                                    <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Pole Serial</th>
                                                        <th>Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody style={{textAlign: 'center'}}>
                                                    { this.state.newPoles === undefined ?
                                                        <tr>
                                                            <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                                        </tr>
                                                        :
                                                        this.state.newPoles.map((pole, index) => {
                                                            return(
                                                                <tr key={index}>
                                                                    <td>{pole.id}</td>
                                                                    <td>{pole.serial}</td>
                                                                    <td>
                                                                        <a onClick={() => this.handleRemove(pole.id)}>
                                                                            <i className="fa fa-minus-circle fa-lg text-danger"> </i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                    }
                                                    </tbody>
                                                </table>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <br/>
                                        <Col  md={8} lg={8} className="label-name"></Col>
                                        <Col  md={2} lg={2} className="label-name">
                                            <strong>Register Date : </strong>
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <Field name="dateRent" component={DateTimePicker}
                                                   placeholder="Install Date"
                                                   defaultDate={this.state.dateRent}
                                                   handleChange={this.handleRentDate.bind(this)}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <ul className="button-style">
                                            <li><ButtonSubmit error={error} submitting={submitting} label="Save"/></li>
                                            <li><Button className="my-custom" bsStyle="primary" onClick={this.closeRentPart.bind(this)}>&nbsp;Back</Button></li>
                                        </ul>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
RentPart = reduxForm({
    form: 'form_add_rent_part',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};

        if(values.dateRent === undefined){
            errors.dateRent = "Date Rent is required!";
        }

        return errors
    },
})(RentPart);

function mapStateToProps(state) {
    return {
        areaGet: state.area.areaGet,
        itemMvGet: state.changeMeter.itemMvGet,
        itemMvSummaryGet: state.changeMeter.itemMvSummaryGet,
        itemMvGetAll: state.changeMeter.itemMvGetAll,
        rentPartAdd: state.rentPole.rentPartAdd,
        rentPartGetAll: state.rentPole.rentPartGetAll,
        rentPartUpdate: state.rentPole.rentPartUpdate,
        rentPartDelete: state.rentPole.rentPartDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllItemsMvAction,
        getItemsMvAction,
        addRentPartAction,
        getAllRentPartAction,
        updateRentPartAction,
        deleteRentPartAction,
        getItemMvSummaryAction,
        getAreasAction
    },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(RentPart);