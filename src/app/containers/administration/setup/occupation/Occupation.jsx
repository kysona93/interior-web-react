import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import './../../index.css';
import { addOccupationAction, getAllOccupationsAction, updateOccupationAction, deleteOccupationAction } from './../../../../actions/setup/occupation';

class Occupation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Occupation',
            occId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.getAllOccupationsAction();
    }

    componentWillReceiveProps(data){
        if(data.occupationAdd.status === 200){
            alert("Successfully added new occupation.");
            this.handleBack();
            this.props.getAllOccupationsAction();
            data.occupationAdd.status = 0;
        }
        if(data.occupationAdd.status === 500){
            alert("Fail with add occupation!");
        }

        if(data.occupationUpdate.status === 200 ){
            alert("Successfully updated occupation.");
            this.handleBack();
            this.props.getAllOccupationsAction();
            data.occupationUpdate.status = 0;
        }
        if(data.occupationUpdate.status === 500){
            alert("Fail with update occupation!");
        }

        if(data.occupationDelete.status === 200){
            alert("Successfully deleted this occupation.");
            this.props.getAllOccupationsAction();
            data.occupationDelete.status = 0;
        }
        if(data.occupationDelete.status === 500){
            alert("Fail with delete this occupation!");
        }
    }

    handleBack(){
        this.setState({update: false, label: 'Save', labelForm: 'New Occupation'});
        this.props.dispatch(initialize('form_add_occupation', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Occupation', occId: data.id});
        this.props.dispatch(initialize('form_add_occupation', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this occupation?") === true){
            this.props.deleteOccupationAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            let data = {
                'occupationKh': values.occupationKh.trim(),
                'occupationEn': values.occupationEn.trim(),
                'description' : ""
            };
            if(values.description === "" || values.description === undefined) data.description = "description";
            else data.description = values.description;
            this.props.addOccupationAction(data);
        }else{
            let data = {
                'id': this.state.occId,
                'occupationKh': values.occupationKh.trim(),
                'occupationEn': values.occupationEn.trim(),
                'description' : ""
            };
            if(values.description === "" || values.description === undefined) data.description = "description";
            else data.description = values.description;
            this.props.updateOccupationAction(data);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Occupation (Khmer) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="occupationKh" type="text" component={TextBox} label="Occupation (Khmer)" icon="fa fa-suitcase"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Occupation (English) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="occupationEn" type="text" component={TextBox} label="Occupation (English)" icon="fa fa-suitcase"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div>
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Occupation (Khmer)</th>
                                    <th>Occupation (English)</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.props.occupationAll.items !== undefined ?
                                    <tbody>
                                    { this.props.occupationAll.items.map((occupation,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{occupation.id}</td>
                                                <td>{occupation.occupationKh}</td>
                                                <td>{occupation.occupationEn}</td>
                                                <td>{occupation.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(occupation)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(occupation.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={5}><h2>RESULT NOT FOUND</h2></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

Occupation = reduxForm({
    form: 'form_add_occupation',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;
        const errors = {};
        if (values.occupationKh.length < 2) {
            errors.occupationKh = 'Please input at least 2 character !!';
        }
        if (!regex_name.test(values.occupationEn)) {
            errors.occupationEn = 'Please input at least 2 character !!';
        }
        return errors;
    }
})(Occupation);

function mapStateToProps(state) {
    return {
        occupationAdd: state.occupation.occupationAdd,
        occupationAll: state.occupation.occupationAll,
        occupationUpdate :state.occupation.occupationUpdate,
        occupationDelete: state.occupation.occupationDelete
        ,
        initialValues: {
            occupationKh: "",
            occupationEn: "",
            description: ""
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addOccupationAction, getAllOccupationsAction, updateOccupationAction, deleteOccupationAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Occupation);