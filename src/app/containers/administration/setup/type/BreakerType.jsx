import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addBreakerTypeAction, getAllBreakerTypesAction, updateBreakerTypeAction, deleteBreakerTypeAction } from './../../../../actions/setup/type/breakerType';

class BreakerType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Breaker Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllBreakerTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.breakerTypeAdd.status === 200){
            alert("Successfully added new breaker type.");
            this.handleCancel();
            this.props.getAllBreakerTypesAction();
            data.breakerTypeAdd.status = 0;
        }

        if(data.breakerTypeAdd.status === 500){
            alert("Fail with add breaker type!");
        }

        if(data.breakerTypeUpdate.status === 200 ){
            alert("Successfully updated breaker type.");
            this.handleCancel();
            this.props.getAllBreakerTypesAction();
            data.breakerTypeUpdate.status = 0;
        }
        if( data.breakerTypeUpdate.status === 500){
            alert("Fail with update breaker type!");
        }

        if(data.breakerTypeDelete.status === 200){
            alert("Successfully deleted this breaker type.");
            this.props.getAllBreakerTypesAction();
            data.breakerTypeDelete.status = 0;
        }
        if(data.breakerTypeDelete.status === 500){
            alert("Fail with delete this breaker type!");
            data.breakerTypeDelete.status = 0;
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Breaker Type'});
        this.props.dispatch(initialize('form_add_breaker_type', null));
    }


    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Breaker Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_breaker_type', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this breaker type?") === true){
            this.props.deleteBreakerTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addBreakerTypeAction({
                description: values.description,
                breakerType: values.breakerType.trim()
            });
        }else{
            this.props.updateBreakerTypeAction({
                id: Number(this.state.typeId),
                description: values.description,
                breakerType: values.breakerType.trim()
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Breaker Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="breakerType" type="text" component={TextBox} label="Breaker type"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label == "Edit" ?
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Breaker Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.breakerTypeAll.items != undefined ?
                                        <tbody>
                                        { this.props.breakerTypeAll.items.map((breaker,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{breaker.id}</td>
                                                    <td>{breaker.breakerType}</td>
                                                    <td>{breaker.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(breaker)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(breaker.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>
            </div>
        )
    }
}

BreakerType = reduxForm({
    form: 'form_add_breaker_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.breakerType) || values.breakerType === undefined) {
            errors.breakerType = 'Box Type is required and at least 2 character!';
        }
        return errors;
    }
})(BreakerType);

function mapStateToProps(state) {
    return {
        breakerTypeAdd: state.breakerType.breakerTypeAdd,
        breakerTypeAll: state.breakerType.breakerTypeAll,
        breakerTypeUpdate: state.breakerType.breakerTypeUpdate,
        breakerTypeDelete: state.breakerType.breakerTypeDelete,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addBreakerTypeAction, getAllBreakerTypesAction, updateBreakerTypeAction, deleteBreakerTypeAction  },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(BreakerType);