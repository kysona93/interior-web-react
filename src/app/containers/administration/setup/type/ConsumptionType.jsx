import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import '../index.css';
import { getAllConsumptionTypesAction, addConsumptionTypeAction, updateConsumptionTypeAction, deleteConsumptionTypeAction } from '../../../../actions/setup/type/consumptionType';

class ConsumptionType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Consumption Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllConsumptionTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.consumptionTypeAdd.status === 200){
            alert("Successfully added new consumption type.");
            this.handleCancel();
            this.props.getAllConsumptionTypesAction();
            data.consumptionTypeAdd.status = 0;
        }
        if(data.consumptionTypeAdd.status === 404 || data.consumptionTypeAdd.status === 500){
            alert("Fail with add consumption type!");
        }
        if(data.consumptionTypeUpdate.status === 200 ){
            alert("Successfully updated consumption type.");
            this.handleCancel();
            this.props.getAllConsumptionTypesAction();
            data.consumptionTypeUpdate.status = 0;
        }
        if(data.consumptionTypeUpdate.status === 400 || data.consumptionTypeUpdate.status === 500){
            alert("Fail with update consumption type!");
        }
        if(data.consumptionTypeDelete.status === 200){
            alert("Successfully deleted this consumption type.");
            this.props.getAllConsumptionTypesAction();
            data.consumptionTypeDelete.status = 0;
        }
        if(data.consumptionTypeDelete.status === 400 || data.consumptionTypeDelete.status === 500){
            alert("Fail with delete this consumption type!");
        }
    }

    handleCancel(){
        this.setState({update: true, label: 'Save', labelForm: 'New Consumption Type'});
        this.props.dispatch(initialize('form_add_consumption_type', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Consumption Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_consumption_type', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this consumption type?") === true){
            this.props.deleteConsumptionTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addConsumptionTypeAction({
                consumptionEn: values.consumptionEn.trim(),
                consumptionKh: values.consumptionKh.trim(),
                description: values.description,
                kwhPerMonth: Number(values.kwhPerMonth)
            });
        }else{
            this.props.updateConsumptionTypeAction({
                id: Number(this.state.typeId),
                consumptionEn: values.consumptionEn.trim(),
                consumptionKh: values.consumptionKh.trim(),
                description: values.description,
                kwhPerMonth: Number(values.kwhPerMonth)
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Consumption Type (Khmer) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="consumptionKh" type="text" component={TextBox} label="Consumption type khmer" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Consumption Type (English) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="consumptionEn" type="text" component={TextBox} label="Consumption type english" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Amount<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="kwhPerMonth" type="text" component={TextBox} label="KWH per month" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label === "Edit" ?
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={() => this.handleCancel()}>Cancel</Button>
                                            </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Consumption (Khmer)</th>
                                    <th>Consumption (English)</th>
                                    <th>Kwh Per Month</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>

                                { this.props.consumptionTypeAll.items !== undefined ?
                                    <tbody>
                                    { this.props.consumptionTypeAll.items.map((consumption, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{consumption.id}</td>
                                                <td>{consumption.consumptionKh}</td>
                                                <td>{consumption.consumptionEn}</td>
                                                <td>{consumption.kwhPerMonth}</td>
                                                <td>{consumption.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(consumption)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(consumption.id)} className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                        <tr>
                                            <td colSpan={6}> RESULT NOT FOUND </td>
                                        </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

ConsumptionType = reduxForm({
    form: 'form_add_consumption_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;
        let regex_amount = /[0-9]{0,10}/;

        const errors = {};
        if (values.consumptionKh === undefined || values.consumptionKh === "") {
            errors.consumptionKh = 'Field is required at least 2 character!';
        }
        if (!regex_name.test(values.consumptionEn)) {
            errors.consumptionEn = 'Field is required at least 2 character!';
        }
        if (!regex_amount.test(values.kwhPerMonth)) {
            errors.kwhPerMonth = 'Kwh per month is required and accept only numeric!';
        }
        return errors;
    }
})(ConsumptionType);

function mapStateToProps(state) {
    return {
        consumptionTypeAdd : state.consumptionType.consumptionTypeAdd,
        consumptionTypeUpdate: state.consumptionType.consumptionTypeUpdate,
        consumptionTypeDelete: state.consumptionType.consumptionTypeDelete,
        consumptionTypeAll: state.consumptionType.consumptionTypeAll,
        initialValues: {
            consumptionEn: "",
            consumptionKh: "",
            description: "",
            kwhPerMonth: ""
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllConsumptionTypesAction, addConsumptionTypeAction, updateConsumptionTypeAction, deleteConsumptionTypeAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ConsumptionType);