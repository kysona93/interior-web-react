import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addPoleTypeAction, updatePoleTypeAction, deletePoleTypeAction, getAllPoleTypesAction } from './../../../../actions/setup/type/poleType';

class PoleType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm : 'New Pole Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllPoleTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.poleTypeAdd.status === 200){
            alert("Successfully added new pole type !!");
            this.handleCancel();
            this.props.getAllPoleTypesAction();
            data.poleTypeAdd.status = 0;
        }
        if(data.poleTypeAdd.status === 500){
            alert("Fail with add pole type !!");
        }

        if(data.poleTypeUpdate.status === 200 ){
            alert("Successfully updated pole type !!");
            this.handleCancel();
            this.props.getAllPoleTypesAction();
            data.poleTypeUpdate.status = 0;
        }
        if(data.poleTypeUpdate.status === 500){
            alert("Fail with update pole type !!");
        }

        if(data.poleTypeDelete.status === 200){
            alert("Successfully deleted this pole type !!");
            this.props.getAllPoleTypesAction();
            data.poleTypeDelete.status = 0;
        }
        if(data.poleTypeDelete.status === 500){
            alert("Fail with delete this pole type !!");
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Pole Type'});
        this.props.dispatch(initialize('form_add_pole_type', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Pole Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_pole_type', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this pole type?") === true){
            this.props.deletePoleTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addPoleTypeAction({
                description: values.description,
                poleType: values.poleType.trim()
            });
        }else{
            this.props.updatePoleTypeAction({
                id: Number(this.state.typeId),
                description: values.description,
                poleType: values.poleType.trim()
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Pole Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="poleType" type="text" component={TextBox} label="Pole type"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col  xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Pole Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.props.poleTypeAll.items != undefined ?
                                    <tbody>
                                    { this.props.poleTypeAll.items.map((pole,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{pole.id}</td>
                                                <td>{pole.poleType}</td>
                                                <td>{pole.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(pole)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(pole.id)} className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

PoleType = reduxForm({
    form: 'form_add_pole_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.poleType) || values.poleType === undefined) {
            errors.poleType = 'Pole Type is required and at least 2 character!';
        }
        return errors;
    }
})(PoleType);

function mapStateToProps(state) {
    return {
        poleTypeAdd: state.poleType.poleTypeAdd,
        poleTypeAll: state.poleType.poleTypeAll,
        poleTypeUpdate: state.poleType.poleTypeUpdate,
        poleTypeDelete: state.poleType.poleTypeDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addPoleTypeAction, updatePoleTypeAction, deletePoleTypeAction, getAllPoleTypesAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PoleType);