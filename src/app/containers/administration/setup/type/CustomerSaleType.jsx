import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { getAllCustomerGroupsAction, addCustomerGroupAction, updateCustomerGroupAction, deleteCustomerGroupAction } from './../../../../actions/setup/type/customerGroup';
import { getAllCustomerTypesAction } from './../../../../actions/setup/type/customerType';

class CustomerSaleType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm : 'New Customer Sale Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Customer Sale Type'});
        this.props.dispatch(initialize('form_add_customer_group', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Customer Sale Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_customer_group', data)
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this customer group?") === true){
            this.props.deleteCustomerGroupAction(id);
        }
    }

    componentWillMount(){
        this.props.getAllCustomerGroupsAction(0);
        this.props.getAllCustomerTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.customerGroupAdd.status === 200){
            alert("Successfully added new customer Sale Type !!");
            this.handleCancel();
            this.props.getAllCustomerGroupsAction(0);
            data.customerGroupAdd.status = 0;
        }
        if(data.customerGroupAdd.status === (404 || 502) || data.customerGroupAdd.status === 500){
            alert("Fail with add customer Sale Type !!");
        }
        if(data.customerGroupUpdate.status === 200 ){
            alert("Successfully updated customer Sale Type !!");
            this.handleCancel();
            this.props.getAllCustomerGroupsAction(0);
            data.customerGroupUpdate.status = 0;
        }
        if(data.customerGroupUpdate.status === (404 || 502) || data.customerGroupUpdate.status === 500){
            alert("Fail with update customer Sale Type !!");
        }

        if(data.customerGroupDelete.status === 200){
            alert("Successfully deleted this customer Sale Type !!");
            this.props.getAllCustomerGroupsAction(0);
            data.customerGroupDelete.status = 0;
        }
        if(data.customerGroupDelete.status === (404 || 502) || data.customerGroupDelete.status === 500){
            alert("Fail with delete this customer Sale Type!");
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addCustomerGroupAction({
                customerTypeId: Number(values.customerTypeId),
                customerSaleTypeKh: values.customerSaleTypeKh.trim(),
                customerSaleTypeEn: values.customerSaleTypeEn.trim(),
                description: values.description.trim()
            });
        }else{
               this.props.updateCustomerGroupAction({
                   id: Number(this.state.typeId),
                   customerTypeId: Number(values.customerTypeId),
                   customerSaleTypeKh: values.customerSaleTypeKh.trim(),
                   customerSaleTypeEn: values.customerSaleTypeEn.trim(),
                   description: values.description.trim()
               });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        let customerTypes = [];
        if(this.props.customerTypeAll.items !== undefined){
            this.props.customerTypeAll.items.map((type) => {
                customerTypes.push({
                    id: type.id,
                    name: type.customerTypeEn
                })
            })
        }else customerTypes = [];
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{this.state.labelForm}</h3>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Sale Type (Khmer)<span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="customerSaleTypeKh" type="text" component={TextBox} label="Customer Sale Type (Khmer)" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Sale Type (English)<span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="customerSaleTypeEn" type="text" component={TextBox} label="Customer Sale Type (English)" />
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Customer Type <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="customerTypeId" type="select" component={SelectBox} placeholder="Please select ..." values={customerTypes} />
                                    </Col>
                                </Row>

                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Description </strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="description" type="text" component={TextArea} label="Description" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={2} lg={2} className="pull-right">
                                        <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                    </Col>
                                    { this.state.label === "Edit" ?
                                        <Col md={3} lg={3} className="pull-right">
                                            <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                        </Col>
                                        : null
                                    }
                                </Row>
                            </form>
                        </div>
                    </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Customer Sale Type (Khmer)</th>
                                    <th>Customer Sale Type (English)</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.props.customerGroupAll.items !== undefined ?
                                    <tbody>
                                    { this.props.customerGroupAll.items.map((customer,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{customer.id}</td>
                                                <td>{customer.customerSaleTypeKh}</td>
                                                <td>{customer.customerSaleTypeEn}</td>
                                                <td>{customer.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(customer)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(customer.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={5}><h2>RESULT NOT FOUND</h2></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

CustomerSaleType = reduxForm({
    form: 'form_add_customer_group',
    validate: (values) => {
        const errors = {};
        if (values.customerSaleTypeKh ==="" || values.customerSaleTypeKh === undefined) {
            errors.customerSaleTypeKh = 'Pole Type is required!';
        }
        if ((values.customerSaleTypeEn ==="") || values.customerSaleTypeEn === undefined) {
            errors.customerSaleTypeEn = 'Pole Type is required!';
        }
        return errors;
    }
})(CustomerSaleType);

function mapStateToProps(state) {
    return {
        customerTypeAll: state.customerType.customerTypeAll,
        customerGroupAdd: state.customerGroup.customerGroupAdd,
        customerGroupUpdate: state.customerGroup.customerGroupUpdate,
        customerGroupDelete: state.customerGroup.customerGroupDelete,
        customerGroupAll: state.customerGroup.customerGroupAll
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllCustomerTypesAction,
        addCustomerGroupAction,
        updateCustomerGroupAction,
        deleteCustomerGroupAction,
        getAllCustomerGroupsAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(CustomerSaleType);