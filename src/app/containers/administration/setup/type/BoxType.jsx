import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addBoxTypeAction, getAllBoxTypesAction, updateBoxTypeAction, deleteBoxTypeAction } from './../../../../actions/setup/type/boxType';

class BoxType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Box Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllBoxTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.boxTypeAdd.status === 200){
            alert("Successfully added new box type.");
            this.handleCancel();
            this.props.getAllBoxTypesAction();
            data.boxTypeAdd.status = 0;
        }
        if(data.boxTypeAdd.status === 500){
            alert("Fail with add box type!");
        }

        if(data.boxTypeUpdate.status === 200 ){
            alert("Successfully updated box type.");
            this.handleCancel();
            this.props.getAllBoxTypesAction();
            data.boxTypeUpdate.status = 0;
        }
        if(data.boxTypeUpdate.status === 500){
            alert("Fail with update box type!");
        }

        if(data.boxTypeDelete.status === 200){
            alert("Successfully deleted this box type.");
            this.props.getAllBoxTypesAction();
            data.boxTypeDelete.status = 0;
        }
        if(data.boxTypeDelete.status === 500){
            alert("Fail with delete this box type!");
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Box Type'});
        this.props.dispatch(initialize('form_add_box_type', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Box Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_box_type', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this box type?") === true){
            this.props.deleteBoxTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addBoxTypeAction({
                description: values.description,
                boxType: values.boxType.trim()
            });
        }else{
            this.props.updateBoxTypeAction({
                id: Number(this.state.typeId),
                description: values.description,
                boxType: values.boxType.trim()
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Box Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="boxType" type="text" component={TextBox} label="Box type" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label === "Edit" ?
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Box Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.boxTypeAll.items != undefined ?
                                        <tbody>
                                        { this.props.boxTypeAll.items.map((box, index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{box.id}</td>
                                                    <td>{box.boxType}</td>
                                                    <td>{box.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(box)}>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(box.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

BoxType = reduxForm({
    form: 'form_add_box_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.boxType) || values.boxType === undefined) {
            errors.boxType = 'Box Type is required and at least 2 character!';
        }
        return errors;
    }
})(BoxType);

function mapStateToProps(state) {
    return {
        boxTypeAdd: state.boxType.boxTypeAdd,
        boxTypeAll: state.boxType.boxTypeAll,
        boxTypeUpdate: state.boxType.boxTypeUpdate,
        boxTypeDelete: state.boxType.boxTypeDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addBoxTypeAction, getAllBoxTypesAction, updateBoxTypeAction, deleteBoxTypeAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(BoxType);