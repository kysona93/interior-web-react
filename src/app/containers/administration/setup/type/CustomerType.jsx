import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { getAllCustomerTypesAction, addCustomerTypeAction, updateCustomerTypeAction, deleteCustomerTypeAction } from './../../../../actions/setup/type/customerType';

class CustomerType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Customer Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllCustomerTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.customerTypeAdd.status === 200){
            alert("Successfully added new customer type.");
            this.handleCancel();
            this.props.getAllCustomerTypesAction();
            data.customerTypeAdd.status = 0;
        }
        if(data.customerTypeAdd.status === 404 || data.customerTypeAdd.status === 500 || data.customerTypeAdd.status === 502){
            alert("Fail with add customer type!");
        }

        if(data.customerTypeUpdate.status === 200 ){
            alert("Successfully updated customer type !!");
            this.handleCancel();
            this.props.getAllCustomerTypesAction();
            data.customerTypeUpdate.status = 0;
        }
        if(data.customerTypeUpdate.status === 404 || data.customerTypeUpdate.status === 500 || data.customerTypeUpdate.status === 502){
            alert("Fail with update customer type!");
        }

        if(data.customerTypeDelete.status === 200){
            alert("Successfully deleted this customer type.");
            this.props.getAllCustomerTypesAction();
            data.customerTypeDelete.status = 0;
        }
        if(data.customerTypeDelete.status === 404 || data.customerTypeDelete.status === 500 || data.customerTypeDelete.status === 502){
            alert(data.customerTypeDelete.status + " Error !! \nFail with delete this customer type!");
        }
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Customer Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_customer_type', data));
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Customer Type'});
        this.props.dispatch(initialize('form_add_customer_type', null));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this customer type?") === true){
            this.props.deleteCustomerTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addCustomerTypeAction({
                description: values.description,
                customerTypeEn: values.customerTypeEn.trim(),
                customerTypeKh: values.customerTypeKh.trim()
            });
        }else{
            this.props.updateCustomerTypeAction({
                id: Number(this.state.typeId),
                description: values.description,
                customerTypeEn: values.customerTypeEn.trim(),
                customerTypeKh: values.customerTypeKh.trim()
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        const customerTypes = this.props.customerTypeAll.items;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Customer Type (Khmer) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="customerTypeKh" type="text" component={TextBox} label="Customer type (Khmer)"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Customer Type (English) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="customerTypeEn" type="text" component={TextBox} label="Customer type (English)"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label !== "Edit" ? null :
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Customer Type (Khmer)</th>
                                    <th>Customer Type (English)</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { customerTypes !== undefined ?
                                    <tbody>
                                    { customerTypes.map((customer, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{customer.id}</td>
                                                <td>{customer.customerTypeKh}</td>
                                                <td>{customer.customerTypeEn}</td>
                                                <td>{customer.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(customer)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a className="btn btn-danger btn-xs" onClick={()=> this.deleteItem(customer.id)}>
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                        <tr>
                                            <td colSpan={5}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

CustomerType = reduxForm({
    form: 'form_add_customer_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;
        const errors = {};

        if (!regex_name.test(values.customerTypeEn)) {
            errors.customerTypeEn = 'Field is required and at least 2 character!';
        }
        if (values.customerTypeKh.length < 2) {
            errors.customerTypeKh = 'Field is required and at least 2 character!';
        }
        if (values.groupId === (undefined || "")) {
            errors.groupId = "Please select customer group !!";
        }
        return errors;
    }
})(CustomerType);

function mapStateToProps(state) {
    return {
        customerTypeAdd: state.customerType.customerTypeAdd,
        customerTypeAll: state.customerType.customerTypeAll,
        customerTypeUpdate: state.customerType.customerTypeUpdate,
        customerTypeDelete: state.customerType.customerTypeDelete,
        initialValues: {
            description: "",
            customerTypeEn: "",
            customerTypeKh: ""
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllCustomerTypesAction,
        addCustomerTypeAction,
        updateCustomerTypeAction,
        deleteCustomerTypeAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(CustomerType);