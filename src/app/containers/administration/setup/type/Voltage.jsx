import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addVoltageTypeAction, updateVoltageTypeAction, deleteVoltageTypeAction, getAllVoltageTypesAction } from './../../../../actions/setup/type/voltageType';

class VoltageType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Voltage Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllVoltageTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.voltageTypeAdd.status === 200){
            alert("Successfully added new voltage type.");
            this.handleCancel();
            this.props.getAllVoltageTypesAction();
            data.voltageTypeAdd.status = 0;
        }
        if(data.voltageTypeAdd.status === (404 || 502) || data.voltageTypeAdd.status === 500){
            alert("Fail with add voltage type!");
        }
        if(data.voltageTypeUpdate.status === 200 ){
            alert("Successfully updated voltage type.");
            this.handleCancel();
            this.props.getAllVoltageTypesAction();
            data.voltageTypeUpdate.status = 0;
        }
        if(data.voltageTypeUpdate.status === (404 || 502) || data.voltageTypeUpdate.status === 500){
            alert("Fail with update voltage type!");
        }

        if(data.voltageTypeDelete.status === 200){
            alert("Successfully deleted this voltage type.");
            this.props.getAllVoltageTypesAction();
            data.voltageTypeDelete.status = 0;
        }
        if(data.voltageTypeDelete.status === (404 || 502) || data.voltageTypeDelete.status === 500){
            alert("Fail with delete this voltage type!");
        }
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Voltage Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_voltage_type', data));
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Voltage Type'});
        this.props.dispatch(initialize('form_add_voltage_type', null));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this voltage type?") === true){
            this.props.deleteVoltageTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addVoltageTypeAction({
                description: values.description,
                voltageType: values.voltageType,
                voltage: Number(values.voltage)
            });
        }else{
            this.props.updateVoltageTypeAction({
                id: this.state.typeId,
                description: values.description,
                voltageType: values.voltageType,
                voltage: Number(values.voltage)
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Voltage Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="voltageType" type="text" component={TextBox} label="voltage type" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Voltage value <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="voltage" type="text" component={TextBox} label="Voltage value" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label === "Edit" ?
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Voltage Type</th>
                                    <th>Value</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.voltageTypeAll.items !== undefined ?
                                        <tbody>
                                        { this.props.voltageTypeAll.items.map((voltage,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{voltage.id}</td>
                                                    <td>{voltage.voltageType}</td>
                                                    <td>{voltage.voltage}</td>
                                                    <td>{voltage.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(voltage)}>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(voltage.id)} className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

VoltageType = reduxForm({
    form: 'form_add_voltage_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;
        let regex_num = /^\d+$/;
        const errors = {};
        if (!regex_name.test(values.voltageType) || values.voltageType === undefined) {
            errors.voltageType = 'Voltage Type is required and at least 2 character !';
        }
        if (!regex_num.test(values.voltage)) {
            errors.voltage = 'Voltage is required and numeric only !';
        }
        return errors;
    }
})(VoltageType);

function mapStateToProps(state) {
    return {
        voltageTypeAll: state.voltageType.voltageTypeAll,
        voltageTypeAdd: state.voltageType.voltageTypeAdd,
        voltageTypeUpdate: state.voltageType.voltageTypeUpdate,
        voltageTypeDelete: state.voltageType.voltageTypeDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addVoltageTypeAction, updateVoltageTypeAction, deleteVoltageTypeAction, getAllVoltageTypesAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(VoltageType);