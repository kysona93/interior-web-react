import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { getAllMeterTypesAction, addMeterTypeAction, updateMeterTypeAction, deleteMeterTypeAction } from './../../../../actions/setup/type/meterType';

class MeterType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Meter Type',
            typeId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllMeterTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.meterTypeAdd.status === 200){
            alert("Successfully added new meter type.");
            this.handleCancel();
            this.props.getAllMeterTypesAction();
            data.meterTypeAdd.status = 0;
        }
        if(data.meterTypeAdd.status === 404 || data.meterTypeAdd.status === 500){
            alert("Fail with add meter type!");
        }

        if(data.meterTypeUpdate.status === 200 ){
            alert("Successfully updated meter type !!");
            this.handleCancel();
            this.props.getAllMeterTypesAction();
            data.meterTypeUpdate.status = 0;
        }
        if(data.meterTypeUpdate.status === 400 || data.meterTypeUpdate.status === 500){
            alert("Fail with update meter type!");
        }

        if(data.meterTypeDelete.status === 200){
            alert("Successfully deleted this meter type.");
            this.props.getAllMeterTypesAction();
            data.meterTypeDelete.status = 0;
        }
        if(data.meterTypeDelete.status === 400 || data.meterTypeDelete.status === 500){
            alert("Fail with delete this meter type!");
        }
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Meter Type', typeId: data.id});
        this.props.dispatch(initialize('form_add_meter_type', data));
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Meter Type'});
        this.props.dispatch(initialize('form_add_meter_type', null));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this meter type?") === true){
            this.props.deleteMeterTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addMeterTypeAction({
                description: values.description,
                meterType: values.meterType
            });
        }else{
            this.props.updateMeterTypeAction({
                id: Number(this.state.typeId),
                description: values.description,
                meterType: values.meterType
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="meterType" type="text" component={TextBox} label="Meter type"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label !== "Edit" ? null :
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Meter Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.meterTypeAll.items != undefined ?
                                        <tbody>
                                        { this.props.meterTypeAll.items.map((meter,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{meter.id}</td>
                                                    <td>{meter.meterType}</td>
                                                    <td>{meter.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(meter)}>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a className="btn btn-danger btn-xs" onClick={()=> this.deleteItem(meter.id)}>
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

MeterType = reduxForm({
    form: 'form_add_meter_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.meterType) || values.meterType === undefined) {
            errors.meterType = 'Meter Type is required and at least 2 character!';
        }
        return errors;
    }
})(MeterType);

function mapStateToProps(state) {
    //console.log("Data",state.meterType.meterTypeAll);
    return {
        meterTypeAdd: state.meterType.meterTypeAdd,
        meterTypeAll: state.meterType.meterTypeAll,
        meterTypeUpdate: state.meterType.meterTypeUpdate,
        meterTypeDelete: state.meterType.meterTypeDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllMeterTypesAction, addMeterTypeAction, updateMeterTypeAction, deleteMeterTypeAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MeterType);