import React from '../../../../../../node_modules/react';
import _ from '../../../../../../node_modules/lodash';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllPolesAction } from './../../../../actions/setup/pole';
import { getAllBoxTypesAction } from './../../../../actions/setup/type/boxType';
import { addBoxAction } from '../../../../actions/setup/box';

class Box extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            createdDate: null,
            expireDate: null,
            types: [],
            poleId: 0
        };
        this.handleCreateDate=this.handleCreateDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreateDate(date){
        this.setState({createdDate: date})
    }
    handleExpireDate(date){
        this.setState({expireDate: date})
    }

    componentWillMount(){
        this.props.getAllPolesAction();
        this.props.getAllBoxTypesAction();
    }

    handleSubmit(value){
        this.props.addBoxAction({
            boxTypeId: Number(value.typeId),
            createdDate: value.createdDate,
            description: value.description.trim(),
            expireDate: value.expireDate || null,
            id: 0,
            poleId: Number(this.state.poleId),
            room: Number(value.room),
            serial: value.serial.trim()
        });
    }

    componentWillReceiveProps(data){
        let types = [];
        if(this.props.boxTypeAll.items !== undefined){
            this.props.boxTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.boxType
                })
            });
            this.setState({types: types});
        } else types = [];

        if (data.boxAdd.status === 200) {
            alert("Successful add new Box !!");
            this.props.dispatch(reset('form_add_box'));
            this.setState({createdDate: null, expireDate: null});
        } else if(data.boxAdd.status === (400 || 404) || data.boxAdd.status === (500 || 502)){
            alert(data.boxAdd.status + " Error !! \nFail with add Box !!\n" + data.boxAdd.message);
        } else if(data.boxAdd.status === 401){
            alert("Please Login !!")
        }
        data.boxAdd.status = 0;
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;

        return (
            <div className="container-fluid">
                <div className="col-md-7 col-lg-7">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">NEW BOX</h3>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Serial <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={9} lg={9}>
                                        <Field name="serial" type="text" component={TextBox} label="Box serial..." />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Create Date <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={9} lg={9}>
                                        <Field name="createdDate" component={DateTimePicker} defaultDate={this.state.createdDate}  handleChange={this.handleCreateDate} placeholder="Create date" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Expire Date</strong>
                                    </Col>
                                    <Col md={9} lg={9}>
                                        <Field name="expireDate" component={DateTimePicker} defaultDate={this.state.expireDate}  handleChange={this.handleExpireDate} placeholder="Expire date" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Pole No <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={5} lg={5}>
                                        <Typeahead
                                            clearButton
                                            onChange={selected => {this.setState({poleId: _.map(selected, "id")[0]})}}
                                            labelKey="serial"
                                            options={this.props.poleAll.items || []}
                                            placeholder="Pole..."
                                        />
                                    </Col>
                                </Row>
                                <Row style={{marginTop: '13px'}}>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Box Type <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={5} lg={5}>
                                        <Field name="typeId" type="select" component={SelectBox} placeholder="Please select box type" values={this.state.types} sortBy="name"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Room No <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={9} lg={9}>
                                        <Field name="room" type="text" component={TextBox} label="Room no"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={3} lg={3} className="label-name">
                                        <strong>Description</strong>
                                    </Col>
                                    <Col md={9} lg={9}>
                                        <Field name="description" type="text" component={TextArea} label="Description" />
                                    </Col>
                                </Row>
                                <Row>
                                    <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                        <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                    </Col>
                                </Row>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
Box = reduxForm({
    form: 'form_add_box',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        if (values.serial.length < 2) {
            errors.serial = 'Please input at least 2 characters !!'
        }
        if (values.poleId === undefined || values.poleId === "" ) {
            errors.poleId = "Please select pole !!";
        }
        if (values.typeId === undefined || values.typeId === "" ) {
            errors.typeId = "Please select box type !!";
        }
        if (!regex_number.test(values.room) || values.room === "" ) {
            errors.room = "Please input number only !!";
        }
        if (values.createdDate === undefined || values.createdDate === "") {
            errors.createdDate = 'Please input created date !!'
        }
        return errors
    },
})(Box);

function mapStateToProps(state) {
    return {
        poleAll: state.pole.poleAll,
        boxTypeAll: state.boxType.boxTypeAll,
        boxAdd: state.box.boxAdd,
        initialValues: {
            serial: '',
            typeId: '',
            poleId: '',
            room: ''
        }

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllPolesAction, getAllBoxTypesAction, addBoxAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(Box);