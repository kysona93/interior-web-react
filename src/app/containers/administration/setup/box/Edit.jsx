import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from './../../../../components/forms/SelectBox';
import { DateTimePicker }  from './../../../../components/forms/DateTimePicker';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllPolesAction } from './../../../../actions/setup/pole';
import { getAllBoxTypesAction } from './../../../../actions/setup/type/boxType';
import { updateBoxAction } from '../../../../actions/setup/box';
import moment from 'moment';

class EditBox extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            createdDate: this.props.box.createdDate !== null ? moment(this.props.box.createdDate) : null,
            expireDate: this.props.box.expireDate !== null ? moment(this.props.box.expireDate) : null,
            types: [],
            poleId: 0
        };
        this.handleCreateDate=this.handleCreateDate.bind(this);
        this.handleExpireDate = this.handleExpireDate.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    handleCreateDate(date){
        this.setState({createdDate: date})
    }

    handleExpireDate(date){
        this.setState({expireDate: date})
    }

    componentWillMount(){
        this.props.getAllPolesAction();
        this.props.getAllBoxTypesAction();
        this.props.dispatch(initialize('form_edit_box', this.props.box));
    }

    handleSubmit(value){
        this.props.updateBoxAction({
            boxTypeId: Number(value.typeId),
            createdDate: value.createdDate || this.state.createdDate,
            description: value.description.trim(),
            expireDate: value.expireDate || this.state.expireDate,
            id: Number(this.props.box.id),
            poleId: Number(this.state.poleId) > 0 ? Number(this.state.poleId) : this.props.box.poleId,
            room: Number(value.room),
            serial: value.serial.trim()
        });
    }

    componentWillReceiveProps(data){
        let types = [];
        if(this.props.boxTypeAll.items !== undefined){
            this.props.boxTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.boxType
                })
            });
            this.setState({types: types});
        } else types = [];

        if(data.boxUpdate.status === 200){
            alert("Successful update box !!");
            this.closeUpdate();
        }
        if(data.boxUpdate.status === (400 || 404) || data.boxUpdate.status === (500 || 502)){
            alert("Fail update box !!\n" + data.boxUpdate.message);
            this.closeUpdate();
        }
        if(data.boxUpdate.status === 401){
            browserHistory.push("/sign-in")
        }
        data.boxUpdate.status = 0;
    }

    closeUpdate(){
        this.setState({startDate: null, expireDate: null});
        this.props.dispatch(initialize('form_edit_box', null));
        this.props.closeUpdate(false);
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;

        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">New Box</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Serial <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="serial" type="text" component={TextBox} label="Box serial..." />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Create Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="createdDate" component={DateTimePicker} defaultDate={this.state.createdDate}  handleChange={this.handleCreateDate} placeholder="Created date" icon="fa fa-money"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Expire Date</strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="expireDate" component={DateTimePicker} defaultDate={this.state.expireDate}  handleChange={this.handleExpireDate} placeholder="Expire date" icon="fa fa-money"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Pole No <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={5} lg={5}>
                                            { this.props.poleAll.items !== undefined ?
                                                <Typeahead
                                                    clearButton
                                                    selected={this.props.poleAll.items.filter(pole => pole.id === this.props.box.poleId)}
                                                    onChange={selected => {this.setState({poleId: _.map(selected, "id")[0]})}}
                                                    labelKey="serial"
                                                    options={this.props.poleAll.items}
                                                    placeholder="Pole..."
                                                />
                                                : null
                                            }
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Box Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={5} lg={5}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Box type..." values={this.state.types} sortBy="name"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Room No <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="room" type="text" component={TextBox} label="Room no"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={9} lg={9}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={6} lgOffset={6} md={3} lg={3}>
                                            <Button bsStyle="primary" style={{width: '115%'}} onClick={this.closeUpdate}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
EditBox = reduxForm({
    form: 'form_edit_box',
    validate: (values) => {
        let regex_number = /^[1-9]\d{0,1}$/;
        const errors = {};
        if (values.serial.length < 2) {
            errors.serial = 'Please input at least 2 characters !!'
        }
        if (values.poleId === undefined || values.poleId === "" ) {
            errors.poleId = "Please select pole !!";
        }
        if (values.typeId === undefined || values.typeId === "" ) {
            errors.typeId = "Please select box type !!";
        }
        if (!regex_number.test(values.room) || values.room === "" ) {
            errors.room = "Please input number only !!";
        }
        return errors
    },
})(EditBox);

function mapStateToProps(state) {
    return {
        poleAll: state.pole.poleAll,
        boxTypeAll: state.boxType.boxTypeAll,
        boxUpdate: state.box.boxUpdate,
        initialValues: {
            serial: '',
            typeId: '',
            poleId: '',
            room: ''
        }

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllPolesAction, getAllBoxTypesAction, updateBoxAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(EditBox);