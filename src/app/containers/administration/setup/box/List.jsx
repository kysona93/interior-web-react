import React,{ Component } from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { Row, Col, Table } from 'react-bootstrap';
import { TextBox } from '../../../../components/forms/TextBox';
import Pagination from '../../../../components/forms/Pagination';
import { bindActionCreators } from 'redux';
import { getBoxesAction, deleteBoxAction } from './../../../../actions/setup/box';
import moment from 'moment';
import EditBox from './Edit';

let box = {
    serial: '',
    id: 0,
    limit: 10,
    page: 1,
    orderBy: "id"
};

class ListBox extends Component{
    constructor(props){
        super(props);
        this.state = {
            update: false,
            box: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    openUpdate(box){
        this.setState({update: true, box: box})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getBoxesAction(box);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this box from database?") === true){
            this.props.deleteBoxAction(id);
        }
    }

    componentWillMount(){
        this.props.getBoxesAction(box);
    }

    handleSubmit(value){
        box.serial = value.serial;
        box.page = 1;
        this.props.getBoxesAction(box);
    }

    componentWillReceiveProps(data){
        if(data.boxes.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.boxes.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.boxDelete.status === 200){
            alert("Successfully deleted box !!");
            this.props.getBoxesAction(box);
            data.boxDelete.status = 0;
        }
        if(data.boxDelete.status === (400 && 404) || data.boxDelete.status === (500 && 502)){
            data.boxDelete.status = 0;
            alert("Fail with delete box !!");
        }
    }

    render(){
        const boxes = this.props.boxes.items;
        let total = 0;
        const {handleSubmit, error, submitting} = this.props;
        return(
            <div className="container-fluid">
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        <h3 style={{ color: '#1c91d0', position: 'absolute', marginTop: '0.7%', fontWeight: 'bold'}}>BOX REPORT</h3>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={5} lg={7}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">BOX SERIAL : </p></td>
                                            <td><Field name="serial" type="text" component={TextBox} label="Box serial"/></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>
                        <Table responsive condensed bordered hover>
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Type</th>
                                <th>Serial</th>
                                <th>Pole</th>
                                <th>Room</th>
                                <th>Created Date</th>
                                <th>Description</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { boxes !== undefined ?
                                boxes.list.map((box, index) => {
                                    total = boxes.totalCount;
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{box.id}</td>
                                            <td className="text-center">{box.typeName}</td>
                                            <td className="text-center">{box.serial}</td>
                                            <td className="text-center">{box.poleSerial}</td>
                                            <td className="text-center">{box.room}</td>
                                            <td className="text-center">{moment(box.createdDate).format("YYYY-MM-DD")}</td>
                                            <td className="text-center">{box.description}</td>
                                            <td className="text-center">
                                                <a onClick={() => this.openUpdate(box)} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>
                                                &nbsp;
                                                <a onClick={() => this.handleDelete(box.id)} className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan="8">
                                        <h3 style={{textAlign: 'center'}}>RESULT NOT FOUND!</h3>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        <Pagination
                            totalCount={total}
                            items={box}
                            callBackAction={this.props.getBoxesAction}
                        />
                    </Row>
                    :
                    <EditBox closeUpdate={this.closeUpdate} box={this.state.box} />
                }
            </div>
        )
    }
}
ListBox = reduxForm({
    form: 'form_filter_boxes'
})(ListBox);
function mapSateToProps(state) {
    return {
        boxes: state.box.boxes,
        boxDelete: state.box.boxDelete,
        initialValues: {
            serial: ''
        }
    }
}
function mapDispatchToProps(dispatch) {
    return bindActionCreators({getBoxesAction, deleteBoxAction},dispatch)
}
export default connect(mapSateToProps,mapDispatchToProps) (ListBox);