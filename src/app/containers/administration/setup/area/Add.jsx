import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllTransformersAction } from './../../../../actions/setup/transformer';
import { getAllAreasAction, addAreaAction, updateAreaAction, deleteAreaAction } from './../../../../actions/setup/area';

class AddArea extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'NEW AREA',
            id: 0,
            transformers: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleSubmit(value){
        if(value.transId !== ""){
            if(!this.state.update){
                this.props.addAreaAction({
                    areaNameKh: value.areaNameKh.trim(),
                    areaNameEn: value.areaNameEn.trim(),
                    areaType: "Has Transformer",
                    description: value.description,
                    transformerId: Number(value.transId)
                });
            } else {
                this.props.updateAreaAction({
                    id: Number(this.state.id),
                    areaNameKh: value.areaNameKh.trim(),
                    areaNameEn: value.areaNameEn.trim(),
                    areaType: "Has Transformer",
                    description: value.description,
                    transformerId: Number(value.transId)
                });
            }
        } else {
            if(!this.state.update){
                this.props.addAreaAction({
                    areaNameKh: value.areaNameKh.trim(),
                    areaNameEn: value.areaNameEn.trim(),
                    areaType: "No Transformer",
                    description: value.description
                });
            } else{
                this.props.updateAreaAction({
                    id: Number(this.state.id),
                    areaNameKh: value.areaNameKh.trim(),
                    areaNameEn: value.areaNameEn.trim(),
                    areaType: "No Transformer",
                    description: value.description
                });
            }
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'NEW AREA'});
        this.props.dispatch(initialize('form_add_area', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'EDIT AREA', id: data.id});
        this.props.dispatch(initialize('form_add_area', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this Area?") === true){
            this.props.deleteAreaAction(id);
        }
    }

    componentWillMount(){
        this.props.getAllTransformersAction({
            check: 1,
            serial: ""
        });
        this.props.getAllAreasAction();
    }

    componentWillReceiveProps(data){
        let transformers = [];
        if(data.transformerAll.items !== undefined){
            data.transformerAll.items.map((transformer) => {
                transformers.push({
                    id: transformer.id,
                    name: transformer.serial
                })
            })
        } else {transformers = [];}
        this.setState({transformers: transformers});

        if(data.areaAdd.status === 200) {
            alert("Successful added new Area !!");
            this.handleCancel();
            this.props.getAllTransformersAction({
                check: 1,
                serial: ""
            });
            this.props.getAllAreasAction();
        } else if(data.areaAdd.status === (400 || 404) || data.areaAdd.status === (500 || 502)){
            alert(data.areaAdd.status + " Error !!\nFail with add new Area !! \n" + data.areaAdd.message);
        }

        if(data.areaUpdate.status === 200 ){
            alert("Successfully updated Area !!");
            this.handleCancel();
            this.props.getAllTransformersAction({
                check: 1,
                serial: ""
            });
            this.props.getAllAreasAction();
        } else if(data.areaUpdate.status === (400 || 404) || data.areaUpdate.status === (500 || 502)){
            alert(data.areaUpdate.status + " Error !!\nFail with update Area !! \n" + data.areaUpdate.message);
        }

        if(data.areaDelete.status === 200){
            alert("Successfully deleted Area !!");
            this.handleCancel();
            this.props.getAllTransformersAction({
                check: 1,
                serial: ""
            });
            this.props.getAllAreasAction();
        } else if(data.areaDelete.status === (400 || 404) || data.areaDelete.status === (500 || 502)){
            alert(data.areaDelete.status + " Error !!\nFail with delete Area !! \n" + data.areaDelete.message);
        }
        data.areaAdd.status = 0;
        data.areaUpdate.status = 0;
        data.areaDelete.status = 0;
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        const areas = this.props.areaAll.items;

        return (
            <div className="container-fluid">
                <div className="col-md-7 col-lg-7">
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title">{this.state.labelForm}</h3>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                                <Row>
                                    <Col lg={12}>
                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Area Name Khmer <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="areaNameKh" type="text" component={TextBox} label="Area Name Khmer" />
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Area Name English <span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="areaNameEn" type="text" component={TextBox} label="Area Name English" />
                                            </Col>
                                        </Row>

                                        { !this.state.update ? null :
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Current transformer</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="serial" type="select" component={TextBox} disabled={true} />
                                                </Col>
                                            </Row>
                                        }

                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Transformer</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="transId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.transformers} />
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Description </strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="description" type="text" component={TextArea} label="Description" />
                                            </Col>
                                        </Row>

                                        <Row>
                                            <Col md={2} lg={2} className="pull-right">
                                                <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                            </Col>
                                            { this.state.label === "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                                </Col>
                                                : null
                                            }
                                        </Row>
                                    </Col>
                                </Row>
                            </form>
                        </div>
                    </div>
                </div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="custyle table-responsive">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Area Khmer</th>
                                    <th>Area English</th>
                                    <th>Area Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    { areas !== undefined ?
                                        areas.map((area, index) => {
                                            return(
                                                <tr key={index}>
                                                    <td>{area.id}</td>
                                                    <td>{area.areaNameKh}</td>
                                                    <td>{area.areaNameEn}</td>
                                                    <td>{area.areaType}</td>
                                                    <td>{area.description}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(area)}>
                                                            <span className="glyphicon glyphicon-edit"> </span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(area.id)} className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"> </span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

AddArea = reduxForm({
    form: 'form_add_area',
    validate: (values) => {
        const regex_text = /[0-9a-zA-Z]{2,100}/;
        const errors = {};
        if (!regex_text.test(values.areaNameEn)) {
            errors.areaNameEn = 'Please input at least 2 characters !!'
        }
        if (values.areaNameKh === "" || values.areaNameKh.length < 2) {
            errors.areaNameKh = "Please input at least 2 characters !!";
        }

        return errors
    },
})(AddArea);

function mapStateToProps(state) {

    return({
        transformerAll: state.transformer.transformerAll,
        areaAll: state.area.areaAll,
        areaAdd: state.area.areaAdd,
        areaUpdate: state.area.areaUpdate,
        areaDelete: state.area.areaDelete,
        initialValues: {
            areaNameKh: '',
            areaNameEn: '',
            areaType: '',
            description: '',
            transId: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getAllTransformersAction, getAllAreasAction, addAreaAction, updateAreaAction, deleteAreaAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddArea);