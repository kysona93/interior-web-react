import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { addPunishRateAction, getAllPunishRatesAction, updatePunishRateAction, deletePunishRateAction } from '../../../../actions/transaction/punish/punishRate';

class PunishRate extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Punish Rate',
            rateId: 0
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.getAllPunishRatesAction();
    }

    componentWillReceiveProps(data){
        if(data.addPunishRate.status === 200){
            alert("Successfully added new punish rate.");
            this.props.getAllPunishRatesAction();
            this.handleBack();
            data.addPunishRate.status = 0;
        }
        if(data.addPunishRate.status === 502 || data.addPunishRate.status === 500){
            alert("Fail with add punish rate!");
            data.addPunishRate.status = 0;
        }

        if(data.updatePunishRate.status === 200 ){
            alert("Successfully updated punish rate.");
            this.props.getAllPunishRatesAction();
            this.handleBack();
            data.updatePunishRate.status = 0;
        }
        if(data.updatePunishRate.status === 404 || data.updatePunishRate.status === 500){
            alert("Fail with update punish rate!");
        }

        if(data.deletePunishRate.status === 200){
            alert("Successfully deleted this punish rate.");
            this.props.getAllPunishRatesAction();
            data.deletePunishRate.status = 0;
        }
        if(data.deletePunishRate.status === 404 || data.deletePunishRate.status === 500){
            alert("Fail with delete this punish rate!");
        }
    }

    handleBack(){
        this.setState({update: false, label: 'Save', labelForm: 'New Punish Rate'});
        this.props.dispatch(initialize('form_add_punish_rate', null));
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Punish Rate', rateId: data.id});
        this.props.dispatch(initialize('form_add_punish_rate', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this punish rate?") === true){
            this.props.deletePunishRateAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addPunishRateAction(
                {
                    "description": values.description,
                    "name": values.name.trim(),
                    "rate": values.rate
                }
            );
        }else{
            this.props.updatePunishRateAction({
                 id: this.state.rateId,
                "description": values.description,
                "name": values.name.trim(),
                "rate": values.rate
            });
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;

        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="name" type="text" component={TextBox} label="Name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Rate<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="rate" type="text" component={TextBox} label="Rate" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label === "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Rate</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                { this.props.getAllPunishRates.status === 200 ?
                                    <tbody>
                                    { this.props.getAllPunishRates.data.items.map((rate,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{rate.id}</td>
                                                <td>{rate.name}</td>
                                                <td>{rate.rate}</td>
                                                <td>{rate.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(rate)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(rate.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr>
                                        <td colSpan={5}><h3>RESULT NOT FOUND</h3></td>
                                    </tr>
                                    </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

PunishRate = reduxForm({
    form: 'form_add_punish_rate',
    validate: (values) => {
        const errors = {};
        if (values.name === undefined) {
            errors.name = 'Name is required!';
        }
        if (values.rate === undefined) {
            errors.rate = 'Rate is required!';
        }
        return errors
    }
})(PunishRate);

function mapStateToProps(state) {
    return {
        getAllPunishRates: state.punishRates.getAllPunishRates,
        addPunishRate: state.punishRates.addPunishRate,
        updatePunishRate :state.punishRates.updatePunishRate,
        deletePunishRate: state.punishRates.deletePunishRate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addPunishRateAction, getAllPunishRatesAction, updatePunishRateAction, deletePunishRateAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PunishRate);