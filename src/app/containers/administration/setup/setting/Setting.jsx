import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import { addSettingAction, listSettingAction, updateSettingAction, deleteSettingAction } from './../../../../actions/setup/setting';

let settingId = 0;
class Setting extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Setting'
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.listSettingAction();
    }

    componentWillReceiveProps(data){
        if(data.addSetting.status === 200){
            alert("Successfully added new setting.");
            this.props.dispatch(initialize("form_add_setting",null));
            this.props.listSettingAction();
            data.addSetting.status = 0;
        }
        if(data.addSetting.status === 404 || data.addSetting.status === 500){
            alert("Fail with add setting!");
        }

        if(data.updateSetting.status === 200 ){
            alert("Successfully updated setting.");
            this.props.listSettingAction();
            this.setState({label: 'Save'});
            this.setState({labelForm: 'Add New Setting'});
            this.setState({update: false});
            this.props.dispatch(initialize("form_add_setting",null));
            data.updateSetting.status = 0;
        }
        if(data.updateSetting.status === 400 || data.updateSetting.status === 500){
            alert("Fail with update setting!");
        }

        if(data.deleteSetting.status === 200){
            alert("Successfully deleted this setting.");
            this.props.listSettingAction();
            data.deleteSetting.status = 0;
        }
        if(data.deleteSetting.status === 400 || data.deleteSetting.status === 500){
            alert("Fail with delete this setting!");
        }
    }

    handleBack(){
        location.href = "/app/administration/setup/setting";
    }

    updateItem(data){
        window.scrollTo(0, 0);
        settingId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Setting'});
        this.props.dispatch(initialize(
            'form_add_setting',
            data,
            ['name', 'keyword', 'value', 'description', 'status'])
        );
    }

    deleteItem(id){
        console.log("ID: ", id);
        if(confirm("Are you sure want to delete this setting?") === true){
            this.props.deleteSettingAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add box type
            let setting = {
                "description": values.description,
                "keyword": values.keyword,
                "name": values.name,
                "value": values.value
            };
            this.props.addSettingAction(setting);
        }else{
            // edit box type
            let setting = {
                "id" : settingId,
                "description": values.description,
                "keyword": values.keyword,
                "name": values.name,
                "value": values.value
            };
            this.props.updateSettingAction(setting);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="name" type="text" component={TextBox} label="Name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Keyword <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="keyword" type="text" component={TextBox} label="Keyword" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Value <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="value" type="text" component={TextBox} label="Value" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label === "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div>
                            <table className="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Keyword</th>
                                    <th>Value</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.listSetting.items !== undefined ?
                                    this.props.listSetting.items.map((setting, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{setting.id}</td>
                                                <td>{setting.name}</td>
                                                <td>{setting.keyword}</td>
                                                <td>{setting.value}</td>
                                                <td>{setting.description}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem(setting)}>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={()=> this.deleteItem(setting.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

Setting = reduxForm({
    form: 'form_add_setting',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.name) || values.name === undefined) {
            errors.name = 'Name is required and at least 2 character!';
        }
        if (!regex_name.test(values.keyword) || values.keyword === undefined) {
            errors.keyword = 'Keyword is required and at least 2 character!';
        }
        if (!regex_name.test(values.value) || values.value === undefined) {
            errors.value = 'Value is required and at least 2 character!';
        }
        return errors;
    }
})(Setting);

function mapStateToProps(state) {
    return {
        addSetting: state.setting.addSetting,
        listSetting: state.setting.listSetting,
        updateSetting: state.setting.updateSetting,
        deleteSetting: state.setting.deleteSetting,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addSettingAction, listSettingAction, updateSettingAction, deleteSettingAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Setting);