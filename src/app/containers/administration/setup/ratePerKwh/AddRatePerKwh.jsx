import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import SelectBox from '../../../../components/forms/SelectBox';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { addRatePerKwhAction, getAllRatePerKwhAction, updateRatePerKwhAction, deleteRatePerKwhAction } from '../../../../actions/setup/rateKwh';
import '../../../../components/forms/Styles.css';
import '../index.css';

class AddRatePerKwh extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'New Rate Per Kwh',
            rateId: 0,
            currency:[{id: 'RIEL', name: 'RIEL'}, {id: 'USD', name: 'USD'}]
        };
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount(){
        this.props.getAllRatePerKwhAction();
    }

    componentWillReceiveProps(data){
        //Addition Rate Per Kwh
        if(data.ratePerKwhAdd.status === 200){
            alert("Successfully added new rate per khw.");
            this.handleCancel();
        }
        if(data.ratePerKwhAdd.status === 404 || data.ratePerKwhAdd.status === 500 || data.ratePerKwhAdd.status === 502){
            alert(data.ratePerKwhAdd.status + " Error !!\nFail with add new rate per kwh.");
        }
        //Update Rate Per Kwh
        if(data.ratePerKwhUpdate.status === 200 ){
            alert("Successfully updated rate per khw.");
            this.handleCancel();
        }
        if(data.ratePerKwhUpdate.status === 404 || data.ratePerKwhUpdate.status === 500 || data.ratePerKwhUpdate.status === 502){
            alert(data.ratePerKwhUpdate.status + " Error !!\nFail with update rate per khw.");
        }
        //Delete Rate Per Kwh
        if(data.ratePerKwhDelete.status === 200){
            alert("Successfully deleted this rate per khw.");
            this.props.getAllRatePerKwhAction();

        } else if(data.ratePerKwhDelete.status === 404 || data.ratePerKwhDelete.status === 500 || data.ratePerKwhDelete.status === 502){
            alert(data.ratePerKwhDelete.status + " Error !!\nFail with delete rate per khw.");
        }
        data.ratePerKwhAdd.status = 0;
        data.ratePerKwhUpdate.status = 0;
        data.ratePerKwhDelete.status = 0;
    }

    handleSubmit(value){
        if(!this.state.update){
            this.props.addRatePerKwhAction({
                rate: Number(value.rate),
                currency:value.currency,
                description: value.description
            });
        }else{
            this.props.updateRatePerKwhAction({
                id:Number(this.state.rateId),
                rate: Number(value.rate),
                currency:value.currency,
                description: value.description
            });
        }
        this.handleCancel();
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, label: 'Edit', labelForm: 'Edit Rate Per Kwh', rateId: data.id });
        this.props.dispatch(initialize('form_add_rate_per_kwh', data));
    }

    deleteItem(id){
        if(confirm("Are you sure to delete this rate per kwh?") === true){
            this.props.deleteRatePerKwhAction(id);
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Rate Per Kwh'});
        this.props.dispatch(initialize('form_add_rate_per_kwh', null));
        this.props.getAllRatePerKwhAction();
    }

    render(){
        const { handleSubmit, error, submitting} = this.props;
        const ratePerKwh = this.props.ratePerKwhAll.items;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Rate <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={4} lg={4}>
                                            <Field name="rate" type="text" component={TextBox} label="Rate" icon="fa fa-money" />
                                        </Col>
                                        <Col md={4} lg={4}>
                                            <Field name="currency" type="select"
                                                   component={SelectBox}
                                                   placeholder="select currency ..."
                                                   values={this.state.currency} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3} className="pull-right">
                                            <ButtonSubmit error={error} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        { this.state.label !== "Edit" ? null :
                                            <Col md={3} lg={3} className="pull-right">
                                                <Button className="bt-cancel" onClick={this.handleCancel}>Cancel</Button>
                                            </Col>
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <div>
                        <table className="table table-bordered table-striped custab">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Rate​</th>
                                <th>Currency</th>
                                <th>Description</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            { ratePerKwh !== undefined ?
                                <tbody>
                                { ratePerKwh.map((rate, index)=>{
                                    return(
                                        <tr key={index}>
                                            <td>{rate.id}</td>
                                            <td>{rate.rate}</td>
                                            <td>{rate.currency}</td>
                                            <td>{rate.description}</td>
                                            <td className="text-center">
                                                <a className='btn btn-info btn-xs' onClick={() => this.updateItem(rate)}>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>
                                                <a onClick={()=> this.deleteItem(rate.id)} href="#" className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                                :
                                <tbody>
                                    <tr><td colSpan={5}><h3>RESULT NOT FOUND</h3></td></tr>
                                </tbody>
                            }
                        </table>
                    </div>
                </Row>
            </div>
        )
    }
}

AddRatePerKwh = reduxForm({
    form: 'form_add_rate_per_kwh',
    validate: (values) => {
        const regex_number = /[0-9]{1,10}/;
        const errors = {};
        if (!regex_number.test(values.rate) || values.rate === undefined) {
            errors.rate = 'Please input number only !!'
        }
        if (values.currency === undefined) {
            errors.currency = 'Please Select Currency !!'
        }
        return errors
    }
})(AddRatePerKwh);

function mapStateToProps(state) {

    return {
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        ratePerKwhAdd: state.ratePerKwh.ratePerKwhAdd,
        ratePerKwhUpdate: state.ratePerKwh.ratePerKwhUpdate,
        ratePerKwhDelete: state.ratePerKwh.ratePerKwhDelete

    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addRatePerKwhAction, getAllRatePerKwhAction, updateRatePerKwhAction, deleteRatePerKwhAction },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(AddRatePerKwh);