import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { Row, Col, Table} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import '../../../../components/forms/Styles.css';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import Pagination from '../../../../components/forms/Pagination';
import EditBreaker from './Edit';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllBreakerTypesAction } from './../../../../actions/setup/type/breakerType';
import { getBreakersAction, deleteBreakerAction } from './../../../../actions/setup/breaker';

let breaker = {
    typeId: 0,
    serial: "",
    limit: 10,
    page: 1,
    orderBy: "id"
};

class ListBreaker extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            types: [],
            breaker: {}
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    componentWillMount(){
        this.props.getBreakersAction(breaker);
        this.props.getAllBreakerTypesAction();
    }

    componentWillReceiveProps(data){
        let types = [];
        if(data.breakerTypeAll.items !== undefined){
            data.breakerTypeAll.items.map((type) => {
                types.push({
                    id: type.id,
                    name: type.breakerType
                })
            });
            this.setState({types: types});
        } else { types = [];}

        if(data.breakers.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.breakers.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.breakerDelete.status === 200){
            alert("Successful update breaker !!");
            this.props.getBreakersAction(breaker);
            data.breakerDelete.status = 0;
        }
        if(data.breakerDelete.status === (400 || 404) || data.breakerDelete.status === (500 || 502)){
            alert(data.breakerDelete.status + " Error !! \nFail update breaker !!\n" + data.breakerDelete.message);
            this.props.getBreakersAction(breaker);
            data.breakerDelete.status = 0;
        }
    }

    openUpdate(breaker){
        this.setState({update: true, breaker: breaker})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getBreakersAction(breaker);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this breaker?") === true){
            this.props.deleteBreakerAction(id);
        }
    }

    handleSubmit(value){
        breaker.serial = value.serial.trim();
        breaker.typeId = Number(value.typeId);
        breaker.page = 1;
        this.props.getBreakersAction(breaker);
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        const breakers = this.props.breakers.items;
        let total = 0;
        return (
            <div className="container-fluid">
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        {/*<form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={7} lg={4}>
                                    <Row>
                                        <Col md={5} lg={5} className="label-name">
                                            <strong>Breaker Type</strong>
                                        </Col>
                                        <Col md={7} lg={7}>
                                            <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={breakerTypes} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={5} lg={5} className="label-name">
                                            <strong>Breaker Serial</strong>
                                        </Col>
                                        <Col md={7} lg={7}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={1}>
                                    <Row>
                                        <Col lg={12} style={{marginTop: "20px"}}>
                                            <ButtonSubmit error={error} submitting={submitting} label="Search" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </form>*/}

                        <h3 style={{ color: '#1c91d0', position: 'absolute', marginTop: '0.7%', fontWeight: 'bold'}}>BREAKER REPORT</h3>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={3} lg={9}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">TYPE : </p></td>
                                            <td><Field name="typeId" type="select" component={SelectBox} placeholder="Breaker type..." values={this.state.types} sortBy="name" /></td>
                                            <td><p className="search-installer">SERIAL : </p></td>
                                            <td><Field name="serial" type="text" component={TextBox} label="Breaker serial..." /></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>

                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>

                        <Table responsive condensed bordered hover>
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Serial</th>
                                <th>Type</th>
                                <th>Ampere</th>
                                <th>Manufacturer</th>
                                <th>Description</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { breakers !== undefined ?
                                breakers.list.map((breaker, index)=>{
                                    total = breakers.totalCount;
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{breaker.id}</td>
                                            <td className="text-center">{breaker.serial}</td>
                                            <td className="text-center">{breaker.typeName}</td>
                                            <td className="text-center">{breaker.ampere}</td>
                                            <td className="text-center">{breaker.manufacturer}</td>
                                            <td className="text-center">{breaker.description}</td>
                                            <td className="text-center">
                                                <a onClick={() => this.openUpdate(breaker)} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>
                                                &nbsp;
                                                <a onClick={() => this.handleDelete(breaker.id)} className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan="7">
                                        <h3 style={{textAlign: 'center'}}>RESULT NOT FOUND!</h3>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        <Pagination
                            totalCount={total}
                            items={breaker}
                            callBackAction={this.props.getBreakersAction}
                        />
                    </Row>
                    :
                    <EditBreaker closeUpdate={this.closeUpdate} types={this.state.types} breaker={this.state.breaker} />
                }
            </div>
        )
    }
}

ListBreaker = reduxForm({
    form: 'form_filter_breaker'
})(ListBreaker);

function mapStateToProps(state) {
    //console.log(state.breaker.breakers);
    return {
        breakerTypeAll: state.breakerType.breakerTypeAll,
        breakers: state.breaker.breakers,
        breakerDelete: state.breaker.breakerDelete,
        initialValues: {
            typeId: '',
            serial: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllBreakerTypesAction, getBreakersAction, deleteBreakerAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListBreaker);