import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { getAllBreakerTypesAction } from './../../../../actions/setup/type/breakerType';
import { addBreakerAction } from './../../../../actions/setup/breaker';

class AddBreaker extends React.Component {
    constructor(props) {
        super(props);
    }

    handleSubmit(value){
        this.props.addBreakerAction({
            ampere: Number(value.ampere),
            breakerSerial: value.breakerSerial.trim(),
            breakerTypeId: Number(value.typeId),
            description: value.description,
            manufacturer: value.manufacturer
        });
        this.props.reset();
    }

    componentWillMount(){
        this.props.getAllBreakerTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.breakerAdd.status === 200){
            alert("Successful add new breaker !!");
            data.breakerAdd.status = 0;
        } else if(data.breakerAdd.status === (400 || 404) || data.breakerAdd.status === (500 || 502)){
            alert(data.breakerAdd.status + " Error !! \nFail add breaker !!\n" + data.breakerAdd.message);
            this.closeUpdate();
            data.breakerAdd.status = 0;
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        let breakerTypes = [];
        if(this.props.breakerTypeAll.items !== undefined){
            this.props.breakerTypeAll.items.map((type) => {
                breakerTypes.push({
                    id: type.id,
                    name: type.breakerType
                })
            })
        }else {
            breakerTypes = [];
        }

        return (
            <div className="col-lg-6 container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Breaker</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Breaker Serial <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="breakerSerial" type="text" component={TextBox} label="Serial number" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Breaker Type <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={breakerTypes} sortBy="name" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Ampere</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="ampere" type="text" component={TextBox} label="Ampere" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Manufacturer</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="manufacturer" type="text" component={TextBox} label="Manufacturer" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

AddBreaker  = reduxForm({
    form: 'form_add_breaker',
    validate: (values) => {
        let regex_text = /[0-9a-zA-Z]{2,1024}/;
        let regex_number = /[0-9]{1,10}/;
        const errors = {};
        if (!regex_text.test(values.breakerSerial)) {
            errors.breakerSerial = 'Please input at least 2 characters !!'
        }
        if (values.typeId === "" ) {
            errors.typeId = "Please select breaker type !!";
        }
        if (!regex_text.test(values.manufacturer)) {
            errors.manufacturer = 'Please input at least 2 characters !!'
        }
        if (!regex_number.test(values.ampere)) {
            errors.ampere = 'Please input number only !!'
        }
        return errors
    },
})(AddBreaker);

function mapStateToProps(state) {
    return({
        breakerTypeAll: state.breakerType.breakerTypeAll,
        breakerAdd: state.breaker.breakerAdd,
        initialValues: {
            breakerSerial: '',
            typeId: '',
            ampere: '',
            manufacturer: '',
            description: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getAllBreakerTypesAction, addBreakerAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddBreaker);