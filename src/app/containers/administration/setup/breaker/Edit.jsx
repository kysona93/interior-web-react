import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { updateBreakerAction } from './../../../../actions/setup/breaker';

class EditBreaker extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    closeUpdate(){
        this.props.dispatch(initialize('form_edit_breaker', null));
        this.props.closeUpdate(false);
    }

    handleSubmit(value){
        this.props.updateBreakerAction({
            id: Number(this.props.breaker.id),
            ampere: Number(value.ampere),
            breakerSerial: value.serial.trim(),
            breakerTypeId: Number(value.typeId),
            description: value.description,
            manufacturer: value.manufacturer
        });
    }

    componentWillMount(){
        this.props.dispatch(initialize('form_edit_breaker', this.props.breaker));
    }

    componentWillReceiveProps(data){
        if(data.breakerUpdate.status === 200){
            alert("Successful update breaker !!");
            this.closeUpdate();
            data.breakerUpdate.status = 0;
        } else if(data.breakerUpdate.status === (400 || 404) || data.breakerUpdate.status === (500 || 502)){
            alert(data.breakerUpdate.status + " Error !! \nFail update breaker !!\n" + data.breakerUpdate.message);
            this.closeUpdate();
            data.breakerUpdate.status = 0;
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;

        return (
            <div className="col-lg-6 container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Edit Breaker</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Breaker Serial <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="serial" type="text" component={TextBox} label="Serial number" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Breaker Type <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.props.types} sortBy="name" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Ampere</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="ampere" type="text" component={TextBox} label="Ampere" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Manufacturer</strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="manufacturer" type="text" component={TextBox} label="Manufacturer" />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="description" type="text" component={TextArea} label="Description" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={6} lgOffset={6} md={3} lg={3}>
                                    <Button bsStyle="primary" style={{width: '115%'}} onClick={this.closeUpdate}>
                                        <i className="fa fa-angle-double-left">&nbsp;Back</i>
                                    </Button>
                                </Col>
                                <Col md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

EditBreaker  = reduxForm({
    form: 'form_edit_breaker',
    validate: (values) => {
        let regex_text = /[0-9a-zA-Z]{2,1024}/;
        let regex_number = /[0-9]{1,10}/;
        const errors = {};
        if (!regex_text.test(values.breakerSerial)) {
            errors.breakerSerial = 'Please input at least 2 characters !!'
        }
        if (values.typeId === "" ) {
            errors.typeId = "Please select breaker type !!";
        }
        if (!regex_text.test(values.manufacturer)) {
            errors.manufacturer = 'Please input at least 2 characters !!'
        }
        if (!regex_number.test(values.ampere)) {
            errors.ampere = 'Please input number only !!'
        }
        return errors
    },
})(EditBreaker);

function mapStateToProps(state) {
    return({
        breakerUpdate: state.breaker.breakerUpdate,
        initialValues: {
            breakerSerial: '',
            typeId: '',
            ampere: '',
            manufacturer: '',
            description: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({updateBreakerAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EditBreaker);