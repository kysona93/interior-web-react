import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm} from 'redux-form';
import { Row, Col, Table, Pagination, FormGroup, ControlLabel} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import '../../../../components/forms/Styles.css';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import EditEmployee from '../../../../containers/administration/setup/employee/Edit';
import moment from 'moment';
import {getEmployeesAction, deleteEmployeeAction} from '../../../../actions/employee/employee';
import './../index.css';

let employee = {
    position: 'installer',
    name: '',
    pageSize: 10,
    nowPage: 1,
    orderBy: "id"
};

class ListInstaller extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            employee: {},
            activePage: 1
        };
        this.FormSubmit = this.FormSubmit.bind(this);
        this.handlePaging = this.handlePaging.bind(this);
        this.handleShowPage = this.handleShowPage.bind(this);
    }

    componentWillMount(){
        this.props.getEmployeesAction(employee);
    }

    openUpdate(employee){
        this.setState({update: true, employee: employee})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getEmployeesAction(employee);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this employee from database?") === true){
            this.props.deleteEmployeeAction(id);
        }
    }

    FormSubmit(value){
        employee.position = value.position.trim();
        employee.name = value.name.trim();
        employee.nowPage = 1;
        this.props.getEmployeesAction(employee);
    }

    handleShowPage(e){
        employee.nowPage = 1;
        employee.pageSize = e.target.value !== '' ? Number(e.target.value) : 10;
        this.props.getEmployeesAction(employee);
    }

    handlePaging(eventKey) {
        this.setState({activePage: eventKey});
        employee.nowPage = eventKey;
        this.props.getEmployeesAction(employee);
    }

    static handleItem(total) {
        if (total <= employee.pageSize) {
            return 1
        } else if (total % employee.pageSize == 0) {
            return total / employee.pageSize
        } else if (total % employee.pageSize > 0) {
            return parseInt(total/employee.pageSize) + 1
        }
    }

    componentWillReceiveProps(data){
        if(data.employeeDelete.status == 200){
            alert("successful to delete !!");
            this.props.getEmployeesAction(employee);
        }else{}
    }

    render(){
        const employees = this.props.employees.items;
        const {handleSubmit, error, submitting} = this.props;
        let total = 0;
        return (
            <div className="container-fluid">
                {!this.state.update ?
                    <div>
                        <div>
                            <form onSubmit={handleSubmit(this.FormSubmit)} className="wrap-full-form">
                                <Row>
                                    <Col lgOffset={8} lg={3}>
                                        <Row>
                                            <Col md={5} lg={5} className="label-name">
                                                <strong>Position</strong>
                                            </Col>
                                            <Col md={7} lg={7}>
                                                <Field name="position" type="text" component={TextBox} label="Position" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={5} lg={5} className="label-name">
                                                <strong>Name</strong>
                                            </Col>
                                            <Col md={7} lg={7}>
                                                <Field name="name" type="text" component={TextBox} label="Name" />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col lg={1}>
                                        <Row>
                                            <Col lg={12} style={{marginTop: "20px"}}>
                                                <ButtonSubmit error={error} submitting={submitting} label="Search" />
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </form>
                        </div>
                    <Row style={{margin: '0', padding: '0'}}>
                        <Table responsive condensed bordered hover className="list-installer">
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Khmer Name</th>
                                <th>Latin Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Position</th>
                                <th>Address</th>
                                <th>Register Date</th>
                                <th>Status</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { employees !== undefined ?
                                employees.list.length === 0 ?
                                    <tr>
                                        <td colSpan="11">
                                            <center><h3>RESULT NOT FOUND!</h3></center>
                                        </td>
                                    </tr>
                                    :
                                    employees.list.map((employee, index)=>{
                                        total = employees.totalCount;
                                        return(
                                            <tr key={index}>
                                                <td className="text-center">{employee.id}</td>
                                                <td className="text-center">{employee.nameKh}</td>
                                                <td className="text-center">{employee.nameEn}</td>
                                                <td className="text-center">{employee.phone}</td>
                                                <td className="text-center">{employee.email}</td>
                                                <td className="text-center">{employee.gender == 1 ? 'Male' : 'Female'}</td>
                                                <td className="text-center">{employee.positionKh}/{employee.positionEn}</td>
                                                <td className="text-center">{employee.address}</td>
                                                <td className="text-center">{moment(employee.createDate).format("YYYY-MM-DD")}</td>
                                                <td className="text-center">{employee.status == 1 ? 'Active' : 'Inactive'}</td>
                                                <td className="text-center">
                                                    <a onClick={() => this.openUpdate({
                                                        id: employee.id,
                                                        nameKh: employee.nameKh,
                                                        nameEn: employee.nameEn,
                                                        phone: employee.phone,
                                                        email: employee.email,
                                                        gender: employee.gender == 1 ? 'MALE' : 'FEMALE',
                                                        positionKh: employee.positionKh,
                                                        positionEn: employee.positionEn,
                                                        description: employee.description,
                                                        address: employee.address,
                                                        createDate: employee.createDate !== null ? moment(employee.createDate).format("YYYY-MM-DD") : null,
                                                        expireDate: employee.expireDate !== null ? moment(employee.expireDate).format("YYYY-MM-DD"): null
                                                    })} className='btn btn-info btn-xs'>
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    &nbsp;
                                                    <a onClick={() => this.handleDelete(employee.id)} className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })
                                :
                                <tr>
                                    <td colSpan="11">
                                        <center><h3>RESULT NOT FOUND!</h3></center>
                                    </td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        {total <= employee.pageSize ? null
                            :
                            <div>
                                <Row>
                                    <Col lgOffset={9} lg={1}>
                                        <FormGroup controlId="formControlsSelect">
                                            <ControlLabel> </ControlLabel>
                                            <Field name="showPage" type="select" onChange={this.handleShowPage} component={SelectBox} placeholder="Show page" values={[{id: 10, name: "10"}, {id: 25, name: "25"}, {id: 50, name: "50"}, {id: 100, name: "100"}]} icon="fa fa-keyboard-o"/>
                                        </FormGroup>
                                    </Col>
                                    <Col lg={2}>
                                        <Pagination style={{ float: 'right'}}
                                                    prev
                                                    next
                                                    first
                                                    last
                                                    ellipsis
                                                    boundaryLinks
                                                    items={ListEmployee.handleItem(total)}
                                                    maxButtons={5}
                                                    activePage={this.state.activePage}
                                                    onSelect={this.handlePaging}
                                        />
                                    </Col>
                                </Row>
                            </div>
                        }
                    </Row>
                </div>
                    :
                    <EditEmployee closeUpdate={this.closeUpdate.bind(this)} employee={this.state.employee} />
                }
            </div>
        )
    }
}

ListInstaller = reduxForm({
    form: 'form_filter_employee'
})(ListInstaller);

function mapStateToProps(state) {
    return {
        employees: state.employee.employees,
        employeeDelete: state.employee.employeeDelete,
        initialValues: {
            position: '',
            name: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getEmployeesAction, deleteEmployeeAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListInstaller);