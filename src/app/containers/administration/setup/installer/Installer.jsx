import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import '../../../../components/forms/Styles.css';
import './../../index.css';
import moment from 'moment';
import { addInstallerAction, getAllInstallersAction, updateInstallerAction, deleteInstallerAction } from '../../../../actions/installation/installer';

let installerId = 0;
class Installer extends React.Component {
    constructor(props){
        super(props);
        this.state={
            gender: [{id: 1, name: "Male"},{id: 2, name: "Female"}],
            status: [{id: true, name: "Active"},{id: false, name: "In Active"}],
            update: false,
            label: 'Save',
            labelForm: 'Add New Installer',
            startDate: null
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }

    componentWillMount(){
        this.props.getAllInstallersAction();
    }

    componentWillReceiveProps(data){
        if(data.addInstaller.status === 200){
            if(confirm("Successfully added new installer.") === true){
                this.props.getAllInstallersAction();
                data.addInstaller.status = 0;
            }else{
                this.props.getAllInstallersAction();
                data.addInstaller.status = 0;
            }
        }
        if(data.addInstaller.status === 404 || data.addInstaller.status === 500){
            alert("Fail with add installer!");
        }

        if(data.updateInstaller.status === 200 ){
            alert("Successfully updated installer.");
            this.setState({label: 'Save'});
            this.setState({update: false});
            this.props.getAllInstallersAction();
            data.updateInstaller.status = 0;
            this.setState({labelForm: 'Add New Installer'});
        }
        if(data.updateInstaller.status === 400 || data.updateInstaller.status === 500){
            alert("Fail with update installer!");
        }

        if(data.deleteInstaller.status === 200){
            alert("Successfully deleted this installer.");
            this.props.getAllInstallersAction();
            data.deleteInstaller.status = 0;
        }
        if(data.deleteInstaller.status === 400 || data.deleteInstaller.status === 500){
            alert("Fail with delete this installer!");
        }
    }

    handleBack(){
        location.href = "/app/installers/installer";
    }

    updateItem(data){
        installerId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Installer'});
        this.props.dispatch(initialize(
            'form_add_installer',
            data,
            ['nameKh', 'nameEn', 'gender', 'phone', 'email', 'registerDate', 'description', 'enabled' ])
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this installer?") === true){
            this.props.deleteInstallerAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add
            let installer = {
                "description": values.description,
                "email": values.email,
                "enabled": values.enabled,
                "gender": values.gender,
                "nameEn": values.nameEn,
                "nameKh": values.nameKh,
                "phone": values.phone,
                "registerDate": values.registerDate
            };
            this.props.addInstallerAction(installer);
        }else{
            // edit
            let installer = {
                "description": values.description,
                "email": values.email,
                "enabled": values.enabled,
                "gender": values.gender,
                "id" : installerId,
                "nameEn": values.nameEn,
                "nameKh": values.nameKh,
                "phone": values.phone,
                "registerDate": values.registerDate
            };
            this.props.updateInstallerAction(installer);
        }

    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div>
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Khmer Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameKh" type="text" component={TextBox} label="Khmer Name" icon="fa fa-user-circle"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>English Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameEn" type="text" component={TextBox} label="English Name" icon="fa fa-user-circle"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Gender <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="gender" type="select" component={SelectBox} placeholder="Please select gender" values={this.state.gender} sortBy="name" icon="fa fa-user-circle"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Phone <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phone" type="text" component={TextBox} label="Phone" icon="fa fa-phone"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Email <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="email" type="text" component={TextBox} label="Email" icon="fa fa-envelope-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Register Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="registerDate" component={DateTimePicker} placeholder="Register Date"
                                                   defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea}
                                                   label="Description" icon="fa fa-user-circle"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="enabled" type="select" component={SelectBox} placeholder="Please select status" values={this.state.status} sortBy="name" icon="fa fa-hand-paper-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }

                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

                <Row>
                    <Col xs={1} sm={1} md={1} lg={1}></Col>
                    <Col xs={11} sm={11} md={11} lg={11}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Khmer Name</th>
                                    <th>English Name</th>
                                    <th>Gender</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Register Date</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllInstallers.items != undefined ?
                                        <tbody>
                                        { this.props.listAllInstallers.items.map((installer,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{installer.id}</td>
                                                    <td>{installer.nameKh}</td>
                                                    <td>{installer.nameEn}</td>
                                                    <td>{installer.gender == 1 ? 'Male' : 'Female'}</td>
                                                    <td>{installer.phone}</td>
                                                    <td>{installer.email}</td>
                                                    <td>{moment(installer.registerDate).format("DD-MM-YYYY")}</td>
                                                    <td>{installer.description}</td>
                                                    <td>{installer.enabled ? "Active" : "In Active"}</td>
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(installer)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(installer.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={10}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                </Row>

            </div>
        )
    }
}


Installer = reduxForm({
    form: 'form_add_installer',
    validate: (values) => {
        let regex_name = /[a-zA-Z]{4,20}/;
        let regex_phone = /^0\d{8,9}$/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        const errors = {};
        if (values.nameKh === undefined) {
            errors.nameKh = 'Khmer name is required!';
        }
        if (values.nameEn === undefined) {
            errors.nameEn = 'English name is required and at least 4 character!';
        }
        if(values.gender === undefined){
            errors.gender = 'Status is required!';
        }
        if (!regex_phone.test(values.phone) || values.phone === undefined) {
            errors.phone = 'Phone number is invalid!';
        }
        if (!regex_name.test(values.email) || values.email === undefined) {
            errors.email = 'Email is invalid!';
        }
        if(values.registerDate === undefined){
            errors.registerDate = "Register date is required!";
        }
        if(values.enabled === undefined){
            errors.enabled = 'Status is required!';
        }
        return errors
    }
})(Installer);

function mapStateToProps(state) {
    console.log("all installer",state.installer.listAllInstallers);
    return {
        addInstaller: state.installer.addInstaller,
        listAllInstallers :state.installer.listAllInstallers,
        updateInstaller: state.installer.updateInstaller,
        deleteInstaller: state.installer.deleteInstaller
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addInstallerAction, getAllInstallersAction, updateInstallerAction, deleteInstallerAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Installer);