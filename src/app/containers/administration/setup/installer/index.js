import React from "react";
import {Router, Route, IndexRoute} from "react-router";
import Installer from './Installer.jsx';
import ListInstaller from './ListInstaller';


export const installer = (
    <Route path="installer" >
        <IndexRoute component={Installer}/>
        <Route path="list" component={ListInstaller} />
    </Route>
);


