import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { TextBox } from '../../../../../components/forms/TextBox';
import SelectBox from '../../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../../components/forms/ButtonSubmit';
import { getAllCustomerTypeAction,updateCustomerTypeAction } from '../../../../../actions/setup/type/customerType';
import '../../index.css';

class UpdateCustomerType extends React.Component{
    constructor(props){
        super(props);
    }

    handleSubmit(value){
        alert(JSON.stringify(value));
        console.log("params",this.props.params.id);
        this.props.updateCustomerTypeAction({
            data: {
                token: "",
                customerType: {
                    id:this.props.params.id,
                    customerType: Number(value.customerType),
                    descriptionKh: value.nameKh,
                    descriptionEn: value.nameEn
                }
            }
        });
        location.href="/type/add-customer";
    }
    render(){
        const { handleSubmit,error,invalid,submitting } = this.props;
        return(
            <div className="container">
                <Row>
                    <Col lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Update Customer Type</h3>
                                <div className="go-to">
                                    <a className="pull-right" href="/type/add-customer"><i className="fa fa-home" aria-hidden="true"></i>go home</a>
                                </div>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Customer Type KH<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameKh" type="text" component={TextBox} label="ឈ្មោះ ..." icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Customer Type En: <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameEn" type="text" component={TextBox} label="Name ..." icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>ប្រភេទ (Type) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="customerType" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Normal"},{id: 2, name: "Business"}]} sortBy="name" icon="fa fa-user-plus"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} lgOffset={8} md={4} lg={4}>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="រក្សាទុក/Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}
UpdateCustomerType = reduxForm({
    form: 'form_add_customer_type',
    validate: (value) => {
        let regex_name = /[a-zA-Z]{4,20}/;
        const errors = {};
        if (!regex_name.test(value.nameKh) || value.nameKh == undefined) {
            errors.khname = 'Khmer name is required and at least 4 character!';
        }
        if (!regex_name.test(value.nameEn) || value.nameEn == undefined) {
            errors.enname = 'English name is required and at least 4 character!';
        }
        return errors
    }

})(UpdateCustomerType);
function mapStateToProps(state,own_props) {
    let customerType = {
        customerType: '',
        nameKh: '',
        nameEn: ''
    };
    let data = {};
    if (state.getAllCustomerType.items !== undefined) {
        console.log("data",data);
        data = state.getAllCustomerType.items.find(cus => cus.id === own_props.params.id) ||customerType;
        customerType = {
            nameKh: data.nameKh,
            nameEn: data.nameEn,
            customerType: data.customerType,
        };
    }
    console.log("data",customerType);
    return ({
        updateCustomerType:state.updateCustomerType,
        initialValues:customerType
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllCustomerTypeAction,updateCustomerTypeAction },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(UpdateCustomerType);
