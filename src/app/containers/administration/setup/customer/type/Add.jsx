import React from 'react';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { TextBox } from '../../../../../components/forms/TextBox';
import SelectBox from '../../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../../components/forms/ButtonSubmit';
import { addCustomerTypeAction, getAllCustomerTypeAction, deleteCustomerTypeAction, updateCustomerTypeAction } from '../../../../../actions/setup/type/customerType';
import '../../index.css';

let id = 0;
class AddCustomerType extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            update: false
        }
    }
    handleSubmit(value){
        if(!this.state.update){
            this.props.addCustomerTypeAction({
                data: {
                    token: "",
                    customerType: {
                        customerType: Number(value.customerType),
                        descriptionKh: value.nameKh,
                        descriptionEn: value.nameEn
                    }
                }
            });
        }else {
            this.props.updateCustomerTypeAction({
                data: {
                    token: "",
                    customerType: {
                        id: id,
                        customerType: Number(value.customerType),
                        descriptionKh: value.nameKh,
                        descriptionEn: value.nameEn
                    }
                }
            });
        }
        this.props.dispatch(reset('form_add_customer_type'));
        location.href=('/app/type/add-customer');
    }
    updateItem(data){
        id = data.id;
        this.setState({update: true});
        this.props.dispatch(initialize('form_add_customer_type', data, ['nameKh', 'nameEn', 'customerType']));
    }

    deleteItem(id){
                 let a = confirm("Are you sure to delete this customer type?");
                    if(a){
                        this.props.deleteCustomerTypeAction(id);
                        location.href=('/app/type/add-customer');
                        return true;
                    }else{
                        return false;
                    }
                }
    componentWillMount(){
        this.props.getAllCustomerTypeAction();
    }

    render(){
        const { handleSubmit,error,invalid,submitting } = this.props;
        return(

            <div className="container-fluid">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6}>
                         <div className="panel panel-default">
                             <div className="panel-heading">
                                 <h3 className="panel-title">Add New Customer Type</h3>
                             </div>
                             <div className="panel-body">
                                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Customer Type KH<span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="nameKh" type="text" component={TextBox} label="ឈ្មោះ ..." icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Customer Type En: <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="nameEn" type="text" component={TextBox} label="Name ..." icon="fa fa-keyboard-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>ប្រភេទ (Type) <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="customerType" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Normal"},{id: 2, name: "Business"}]} sortBy="name" icon="fa fa-user-plus"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={4} lg={4}>
                                    <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="រក្សាទុក/Save" />
                                </Col>
                            </Row>
                        </form>
                             </div>
                         </div>
                    </Col>
                </Row>
                <Col xs={6} sm={6} md={6} lg={6}>
                    <div className="custyle">
                        <table className="table table-bordered table-striped custab">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer type Kh</th>
                                <th>Customer Type EN</th>
                                <th>Parent</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                             {
                                this.props.getAllCustomerType.items !== undefined ?
                            <tbody>
                            { this.props.getAllCustomerType.items.map((cus,index)=>{
                                return(
                                    <tr key={index}>
                                        <td>{index+1}</td>
                                        <td>{cus.descriptionKh}</td>
                                        <td>{cus.descriptionEn}</td>
                                        <td>{cus.customerType === 1 ? "Normal" : "Business"}</td>
                                        <td className="text-center">
                                            <a className='btn btn-info btn-xs' onClick={() => this.updateItem({id: cus.id, nameKh: cus.descriptionKh, nameEn: cus.descriptionEn, customerType: cus.customerType})}>{/*href={"/type/update-customer/"+cus.id}*/}
                                                <span className="glyphicon glyphicon-edit">Edit</span>
                                            </a>
                                            <a onClick={()=> this.deleteItem(cus.id)} href="#" className="btn btn-danger btn-xs">
                                                <span className="glyphicon glyphicon-remove">Delete</span>
                                            </a>
                                        </td>
                                    </tr>
                                )
                            })}
                            </tbody>
                             :null
                            }
                        </table>
                    </div>
                </Col>
            </div>
        )
    }
}
 AddCustomerType = reduxForm({
    form: 'form_add_customer_type',
     validate: (value) => {
         let regex_name = /[a-zA-Z]{4,20}/;
         const errors = {};
         if (value.nameKh === undefined) {
             errors.nameKh = 'field require ';
         }
         if (!regex_name.test(value.nameEn) || value.nameEn === undefined) {
             errors.nameEn = 'English name is required and at least 4 character!';
         }
         if(value.customerType === undefined){
             errors.customerType='please select customer type';
         }
         return errors
     }

})(AddCustomerType);
function mapStateToProps(state) {
    // console.log("state",JSON.stringify(state.getAllCustomerType));
    return ({
        getAllCustomerType:state.getAllCustomerType,
        addCustomerType:state.addCustomerType,
        deleteCustomerType:state.deleteCustomerType
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addCustomerTypeAction,getAllCustomerTypeAction,deleteCustomerTypeAction,updateCustomerTypeAction},dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(AddCustomerType);
