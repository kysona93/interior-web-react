import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col ,Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import { addExchangeRateAction,listAllExchangeRateAction,updateExchangeRateAction,deleteExchangeRateAction } from '../../../../actions/setup/exchangeRate';
import '../../../../components/forms/Styles.css';
import '../index.css';

class AddExchangeRate extends React.Component {
    constructor(props){
        super(props);
        this.state={
            defaultt: [{id: 1, name: "Dollar(USD)"},{id: 2, name: "Riels(R)"}],
            exchangeTo: [{id: 1, name: "Dollar(USD)"},{id: 2, name: "Riels(R)"}],
            update: false,
            id: 0,
            label: 'Save',
            labelForm: 'New Box Type'
        };
    }

    componentWillMount(){
        this.props.listAllExchangeRateAction();
    }

    componentWillReceiveProps(data){
        if(data.addExchangeRate.status === 200){
            alert("Successfully added exchange rate.");
            this.props.listAllExchangeRateAction();
            data.addExchangeRate.status = 0;
        }
        if(data.addExchangeRate.status === 401 || data.addExchangeRate.status === 500){
            alert("Fail with add exchange rate.");
            data.addExchangeRate.status = 0;
            this.props.listAllExchangeRateAction();
        }

        if(data.updateExchangeRate.status === 200){
            alert("Successfully updated exchange rate.");
             this.props.listAllExchangeRateAction();
             data.updateExchangeRate.status = 0;
        }
        if(data.updateExchangeRate.status === 401 || data.updateExchangeRate.status === 500){
            alert("Fail with update exchange rate.");
            this.props.listAllExchangeRateAction();
        }

        if(data.deleteExchangeRate.status === 200){
            alert("Successfully deleted exchange rate.");
            this.props.listAllExchangeRateAction();
            data.deleteExchangeRate.status = 0;
        }
        if(data.deleteExchangeRate.status === 401 || data.deleteExchangeRate.status === 500){
            alert("Fail with delete exchange rate.");
            this.props.listAllExchangeRateAction();
        }
    }

    updateItem(data){
        window.scrollTo(0, 0);
        this.setState({update: true, id : data.id, label: 'Update'});
        this.props.dispatch(initialize('form_add_exchange_rate', data, ['fromCurrencyCode', 'fromCurrencyValue','toCurrencyCode','exchangeValue']));
    }
    deleteItem(id){
        if(confirm("Are you sure to delete this ExchangeRate?") === true) {
            this.props.deleteExchangeRateAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            this.props.addExchangeRateAction({
                data: {
                    exchangeRate: {
                        fromCurrencyCode: Number(values.defaultt),
                        fromCurrencyValue:Number(values.defaultValue),
                        toCurrencyCode:Number(values.exchangeTo),
                        toCurrencyValue:Number(values.exchangeValue)
                    }
                }
            });
        }else {
            this.props.updateExchangeRateAction({
                data: {
                    exchangeRate: {
                        id:Number(this.state.id),
                        fromCurrencyCode: Number(values.defaultt),
                        fromCurrencyValue:Number(values.defaultValue),
                        toCurrencyCode:Number(values.exchangeTo),
                        toCurrencyValue:Number(values.exchangeValue)
                    }
                }
            });
        }
        this.props.initialize('form_add_exchange_rate', null);
    }

    render(){
        const {handleSubmit, error, invalid, pristine, reset, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={6} sm={6} md={6} lg={6}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add New Exchange Rate </h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Default <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="defaultt" type="select" component={SelectBox} placeholder="Please select default" values={this.state.defaultt} sortBy="name" icon="fa fa-money"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Default Value <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="defaultValue" type="text" component={TextBox} label="Default Value" icon="fa fa-money"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Exchange To <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="exchangeTo" type="select" component={SelectBox} placeholder="Please select exchange to" values={this.state.exchangeTo} sortBy="name" icon="fa fa-money"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Exchange Value <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="exchangeValue" type="text" component={TextBox} label="Exchange Value" icon="fa fa-money"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4}> </Col>
                                        <Col md={4} lg={4}>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        <Col  md={4} lg={4}>
                                            <Button className="btnClear btn btn-primary btn-block"  type="submit" disabled={pristine || submitting} onClick={reset}>Clear</Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Col xs={6} sm={6} md={6} lg={6}>
                    <div className="custyle">
                        <table className="table table-bordered table-striped custab">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Currency</th>
                                <th>Value</th>
                                <th>Currency</th>
                                <th>Value</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            {
                                this.props.listAllExchangeRate.items !== undefined ?
                                    <tbody>
                                    { this.props.listAllExchangeRate.items.map((exchange,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{index+1}</td>
                                                <td>{exchange.fromCurrencyCode === 1 ? "Dollar(USD)" : "Riels(R)"}</td>
                                                <td>{exchange.fromCurrencyValue}</td>
                                                <td>{exchange.toCurrencyCode  === 1 ? "Dollar(USD)" : "Riels(R)"}</td>
                                                <td>{exchange.toCurrencyValue}</td>
                                                <td className="text-center">
                                                    <a className='btn btn-info btn-xs' onClick={() => this.updateItem({id:exchange.id,defaultt:exchange.fromCurrencyCode,defaultValue:exchange.fromCurrencyValue,exchangeTo:exchange.toCurrencyCode,exchangeValue:exchange.toCurrencyValue})}>{/*href={"/type/update-customer/"+cus.id}*/}
                                                        <span className="glyphicon glyphicon-edit"> </span>
                                                    </a>
                                                    <a onClick={()=> this.deleteItem(exchange.id)} href="#" className="btn btn-danger btn-xs">
                                                        <span className="glyphicon glyphicon-remove"> </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                    </tbody>
                                    :null
                            }
                        </table>
                    </div>
                </Col>
            </div>
        )
    }
}

AddExchangeRate = reduxForm({
    form: 'form_add_exchange_rate',
    validate: (value) => {
        let regex_currency = /[0-9]{1,10}/;

        const errors = {};
        if (value.defaultt === undefined) {
            errors.defaultt = 'Default is required!';
        }
        if (!regex_currency.test(value.defaultValue) || value.defaultValue === undefined) {
            errors.defaultValue = 'Default Value is required and at least 1 numeric!';
        }
        if(value.exchangeTo === undefined){
            errors.exchangeTo = 'Exchange To is required!';
        }
        if (!regex_currency.test(value.exchangeValue) || value.exchangeValue === undefined) {
            errors.exchangeValue = 'Exchange value is required and at least 1 numeric!';
        }
        return errors
    }
})(AddExchangeRate);

function mapStateToProps(state) {
    // console.log( "Data : ",state.exchangeRate.listAllExchangeRate);
    return {
        addExchangeRate:state.exchangeRate.addExchangeRate,
        listAllExchangeRate:state.exchangeRate.listAllExchangeRate,
        updateExchangeRate: state.exchangeRate.updateExchangeRate,
        deleteExchangeRate: state.exchangeRate.deleteExchangeRate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addExchangeRateAction,listAllExchangeRateAction,updateExchangeRateAction,deleteExchangeRateAction  },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddExchangeRate);