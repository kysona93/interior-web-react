import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import {updateEmployeeAction} from '../../../../actions/employee/employee';
import moment from 'moment';

class EditEmployee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createDate: moment(this.props.employee.createDate),
            stopDate: this.props.employee.stopDate !== null ? moment(this.props.employee.stopDate) : null,
        };
        this.handleCreateDate = this.handleCreateDate.bind(this);
        this.handleStopDate = this.handleStopDate.bind(this);
    }

    handleCreateDate(date) {
        this.setState({createDate: date});
    }

    handleStopDate(date) {
        this.setState({stopDate: date});
    }

    componentWillMount(){
        this.props.dispatch(initialize('form_edit_employee', this.props.employee));
    }

    handleSubmit(value){
        this.props.updateEmployeeAction({
            id: Number(this.props.employee.id),
            address: value.address,
            createDate: value.createDate,
            stopDate: value.stopDate,
            description: value.description,
            email: value.email,
            gender: value.gender,
            nameEn: value.nameEn.trim(),
            nameKh: value.nameKh.trim(),
            phone: value.phone,
            positionEn: value.positionEn,
            positionKh: value.positionKh
        });
    }

    componentWillReceiveProps(data){
        if(data.employeeUpdate.status === 200){
            alert("Successful to update Employee !!");
            this.closeUpdate();
        } else if(data.employeeUpdate.status === (400 || 404) || data.employeeUpdate.status === (500 || 502)){
            alert(data.employeeUpdate.status + " Error !! \nFail add new Employee !!\n" + data.employeeUpdate.message);
            this.closeUpdate();
        } else if(data.employeeUpdate.status === 401){
            alert("Please Login !!");
        }
        data.employeeUpdate.status = 0;
    }


    closeUpdate(){
        this.props.closeUpdate(false);
        this.props.dispatch(initialize('form_edit_employee', null));
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Edit Employee</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameKh" type="text" component={TextBox} label="Name (Khmer)" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameEn" type="text" component={TextBox} label="Name (English)" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Phone <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phone" type="text" component={TextBox} label="Phone number" icon="fa fa-phone"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Email <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="email" type="text" component={TextBox} label="Email" icon="fa fa-envelope-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Gender <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="gender" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 'MALE', name: 'MALE'},{id: 'FEMALE', name: 'FEMALE'}]} sortBy="name" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Create Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="createDate" component={DateTimePicker} placeholder="Create date" defaultDate={this.state.createDate} handleChange={this.handleCreateDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Stop Date </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="stopDate" component={DateTimePicker} placeholder="Stop date" defaultDate={this.state.stopDate} handleChange={this.handleStopDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Position (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="positionKh" type="text" component={TextBox} label="Position (Khmer)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Position (EN) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="positionEn" type="text" component={TextBox} label="Position (English)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} height={210} label="Description of position" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Address </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="address" type="text" component={TextArea} label="Address" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={2} lg={2}>
                                    <Button bsStyle="primary" style={{width: '100%'}} onClick={this.closeUpdate.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                </Col>
                                <Col md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

EditEmployee = reduxForm({
    form: 'form_edit_employee'
})(EditEmployee);

function mapStateToProps(state) {
    return({
        employeeUpdate: state.employee.employeeUpdate
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({updateEmployeeAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EditEmployee);