import React from '../../../../../../node_modules/react';
import {connect} from 'react-redux';
import { Field, reduxForm} from 'redux-form';
import { Row, Col, Table} from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import '../../../../components/forms/Styles.css';
import { TextBox } from '../../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import EditEmployee from '../../../../containers/administration/setup/employee/Edit';
import Pagination from '../../../../components/forms/Pagination';
import moment from 'moment';
import {getEmployeesAction, deleteEmployeeAction} from '../../../../actions/employee/employee';

let employee = {
    position: '',
    name: '',
    limit: 10,
    page: 1,
    orderBy: "id"
};

class ListEmployee extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            employee: {},
            activePage: 1,
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
    }

    componentWillMount(){
        this.props.getEmployeesAction(employee);
    }

    openUpdate(employee){
        this.setState({update: true, employee: employee})
    }

    closeUpdate(value){
        this.setState({update: value});
        this.props.getEmployeesAction(employee);
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this employee from database?") === true){
            this.props.deleteEmployeeAction(id);
        }
    }

    handleSubmit(value){
        employee.position = value.position.trim();
        employee.name = value.name.trim();
        employee.page = 1;
        this.props.getEmployeesAction(employee);
    }

    componentWillReceiveProps(data){
        if(data.employees.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.employees.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        if(data.employeeDelete.status === 200){
            alert("Employee have been deleted !!");
            this.props.getEmployeesAction(employee);
        } else if(data.employeeDelete.status === (400 && 404) || data.employeeDelete.status === (500 && 502)){
            alert(data.employeeDelete.status + " Error !! \nFail !! Employee have not been deleted !!\n" + data.employeeDelete.message);
        } else if(data.employeeDelete.status === 401){
            alert("Please login !!");
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        let total = 0;
        return (
            <div className="container-fluid">
                {!this.state.update ?
                    <Row style={{margin: '0', padding: '0'}}>
                        <h3 style={{ color: '#1c91d0', position: 'absolute', marginTop: '0.7%', fontWeight: 'bold'}}>EMPLOYEE REPORT</h3>
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lgOffset={3} lg={9}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">POSITION : </p></td>
                                            <td><Field name="position" type="text" component={TextBox} label="Position..."/></td>
                                            <td><p className="search-installer">NAME : </p></td>
                                            <td><Field name="name" type="text" component={TextBox} label="Name..."/></td>
                                            <td><ButtonSubmit className="search-installer" error={error} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>

                        <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                            <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                            <span className="sr-only">Loading...</span>
                        </div>

                        <Table responsive condensed bordered hover>
                            <thead >
                            <tr>
                                <th>ID</th>
                                <th>Khmer Name</th>
                                <th>Latin Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Position</th>
                                <th>Address</th>
                                <th>Register Date</th>
                                <th>Status</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.employees.items !== undefined ?
                                this.props.employees.items.list.map((employee, index)=> {
                                    total = this.props.employees.items.totalCount;
                                    return(
                                        <tr key={index}>
                                            <td className="text-center">{employee.id}</td>
                                            <td className="text-center">{employee.nameKh}</td>
                                            <td className="text-center">{employee.nameEn}</td>
                                            <td className="text-center">{employee.phone}</td>
                                            <td className="text-center">{employee.email}</td>
                                            <td className="text-center">{employee.gender}</td>
                                            <td className="text-center">{employee.positionKh}/{employee.positionEn}</td>
                                            <td className="text-center">{employee.address}</td>
                                            <td className="text-center">{moment(employee.createDate).format("YYYY-MM-DD")}</td>
                                            <td className="text-center">{employee.status === 1 ? 'Active' : 'Inactive'}</td>
                                            <td className="text-center">
                                                <a onClick={() => this.openUpdate(employee)} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>
                                                &nbsp;
                                                <a onClick={() => this.handleDelete(employee.id)} className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={11}><h3 style={{textAlign: 'center'}}>RESULT NOT FOUND</h3></td>
                                </tr>
                            }
                            </tbody>
                        </Table>
                        <Pagination
                            totalCount={total}
                            items={employee}
                            callBackAction={this.props.getEmployeesAction}
                        />
                    </Row>
                    :
                    <EditEmployee closeUpdate={this.closeUpdate} employee={this.state.employee} />
                }
            </div>
        )
    }
}

ListEmployee = reduxForm({
    form: 'form_filter_employee'
})(ListEmployee);

function mapStateToProps(state) {
    return {
        employees: state.employee.employees,
        employeeDelete: state.employee.employeeDelete,
        initialValues: {
            position: '',
            name: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getEmployeesAction, deleteEmployeeAction},dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListEmployee);