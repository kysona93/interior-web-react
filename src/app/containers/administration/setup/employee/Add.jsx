import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import SelectBox from '../../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import {addEmployeeAction} from '../../../../actions/employee/employee';

class AddEmployee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createDate: null,
            stopDate: null
        };
        this.handleCreateDate = this.handleCreateDate.bind(this);
        this.handleStopDate = this.handleStopDate.bind(this);
    }

    handleCreateDate(date) {
        this.setState({createDate: date});
    }

    handleStopDate(date) {
        this.setState({stopDate: date});
    }

    handleSubmit(value){
        this.props.addEmployeeAction({
            address: value.address.trim(),
            createDate: value.createDate,
            stopDate: value.stopDate,
            description: value.description.trim(),
            email: value.email,
            gender: value.gender,
            nameEn: value.nameEn.trim(),
            nameKh: value.nameKh.trim(),
            phone: value.phone,
            positionEn: value.positionEn.trim(),
            positionKh: value.positionKh.trim()
        });
        this.props.dispatch(reset('form_add_employee'));
        this.setState({createDate: null, stopDate: null});
    }

    componentWillReceiveProps(data){
        if(data.employeeAdd.status === 200){
            alert("Successful add new Employee !!")
        } else if(data.employeeAdd.status === (400 || 404) || data.employeeAdd.status === (500 || 502)){
            alert(data.employeeAdd.status + " Error !! \nFail add new Employee !!\n" + data.employeeAdd.message);
        } else if(data.employeeAdd.status === 401){
            alert("Please Login !!");
        }
        data.employeeAdd.status = 0;
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="col-md-12 col-lg-12">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Employee</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameKh" type="text" component={TextBox} label="Name (Khmer)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameEn" type="text" component={TextBox} label="Name (English)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Phone <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phone" type="text" component={TextBox} label="Phone number" icon="fa fa-phone"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Email <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="email" type="text" component={TextBox} label="Email" icon="fa fa-envelope-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Gender <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="gender" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 'MALE', name: 'Male'},{id: 'FEMALE', name: 'Female'}]} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Create Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="createDate" component={DateTimePicker} placeholder="Create date" defaultDate={this.state.createDate} handleChange={this.handleCreateDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Stop Date </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="stopDate" component={DateTimePicker} placeholder="Stop date" defaultDate={this.state.stopDate} handleChange={this.handleStopDate}/>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Position (KH) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="positionKh" type="text" component={TextBox} label="Position (Khmer)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Position (EN) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="positionEn" type="text" component={TextBox} label="Position (English)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} height={210} label="Description of position" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={2} lg={2} className="label-name">
                                    <strong>Address </strong>
                                </Col>
                                <Col md={10} lg={10}>
                                    <Field name="address" type="text" component={TextArea} label="Address" />
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

AddEmployee  = reduxForm({
    form: 'form_add_employee',
    validate: (values) => {
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex_phone = /^0\d{8,9}$/;
        const errors = {};
        if (values.nameKh.length < 2) {
            errors.nameKh = 'Please input at least 2 characters !!';
        }
        if (values.nameEn.length < 2) {
            errors.nameEn = 'Please input at least 2 characters !!';
        }
        if(!regex_phone.test(values.phone)){
            errors.phone = 'Phone is invalid !!';
        }
        if(!regex_email.test(values.email)){
            errors.email = 'Email is invalid !!';
        }
        if (values.gender === undefined || values.gender === "" ) {
            errors.gender = "Please select gender !!";
        }
        if (values.positionKh.length < 2) {
            errors.positionKh = 'Please input at least 2 characters !!';
        }
        if (values.positionEn.length < 2) {
            errors.positionEn = 'Please input at least 2 characters !!';
        }
        if (values.createDate === undefined || values.createDate === "" || values.createDate === null) {
            errors.createDate = 'Please input create date !!'
        }
        if (values.address.length < 2) {
            errors.address = 'Please input at least 2 characters !!';
        }
        return errors
    },
})(AddEmployee);

function mapStateToProps(state) {
    return({
        employeeAdd: state.employee.employeeAdd,
        initialValues: {
            nameKh: '',
            nameEn: '',
            positionKh: '',
            positionEn: '',
            description: '',
            address: ''
        }
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({addEmployeeAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEmployee);