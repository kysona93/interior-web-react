import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Col, Row, FormGroup } from 'react-bootstrap';
import SelectBox from './../../components/forms/SelectBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import { Field, reduxForm, reset } from 'redux-form';
import { getGroupsStatusAction } from '../../actions/authentication';
import { getAllMenusAction, assignMenusAction, getMenusByGroupAction } from './../../actions/menu';

let arrGroups = [];
let arrIds = [];
let groupIds = [];

let parentId1 = "";
let parentId2 = "";
let parentId3 = "";
let parentIndex1 = "";
let parentIndex2 = "";
let parentIndex3 = "";

class AssignMenu extends React.Component {
    constructor(props){
        super(props);
        this.state={
            groups: [],
            isChecked: false
        };
        
        this.toggleChange = this.toggleChange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.handleSelectGroup = this.handleSelectGroup.bind(this);
    }

    componentWillMount(){
        this.props.getGroupsStatusAction(true);
        this.props.getAllMenusAction();
    }

    componentWillReceiveProps(newProps){
        if(newProps.getGroupsStatus.status == 200){
            let groups = newProps.getGroupsStatus.data.items;
            if(groups.length === arrGroups.length){
                this.setState({groups : arrGroups})
           }else {
                let array = [];
                groups.forEach((element) => {
                    array.push({
                        "id": element.id,
                        "name": element.groupName
                    });
                });

                arrGroups = array;
                this.setState({groups : arrGroups});
            }
        }
        
        if(newProps.assignMenus.response != undefined) {
            if(newProps.assignMenus.response.status == 200) {
                alert("Successfully assign menu!");
                $(".check").prop("checked", false);
                return newProps.assignMenus.response.status = 0;
            } 
            if(newProps.assignMenus.response.status == 400 
                    || newProps.assignMenus.response.status == 500) {
                alert("Fail assign menu!");
                return newProps.assignMenus.response.status = 0;
            }
        }

        if(newProps.getMenusByGroup != undefined) {
            if(newProps.getMenusByGroup.status == 200) {
                arrIds = Array.from(new Set(groupIds));

                var array1 = arrIds;
                var array2 = [];
                var index;

                //TODO: ------ keep elements from compare filter group
                newProps.getMenusByGroup.data.items.forEach((element) => {
                    array2.push(element.menu.id);
                    $('input:checkbox[value="' + element.menu.id + '"]').prop('checked', true);
                });

                for (var i=0; i<array2.length; i++) {
                    index = array1.indexOf(array2[i]);
                    if (index > -1) {
                        array1.splice(index, 1);
                    }
                }
                //TODO: ------ remove old elements from compare filter group
                array1.forEach(element => {
                    $('input:checkbox[value="' + element + '"]').prop('checked', false);
                })

                arrIds = array2;
            }
            if(newProps.getMenusByGroup.status == 404) {
                $(".check").prop("checked", false)
                arrIds = [];
            }
        }
    }

    handleCheckboxChange() {
        this.setState({isChecked: !this.state.isChecked});
        if(this.state.isChecked == false) {
            arrIds = Array.from(new Set(groupIds));
        } else {
            arrIds = [];
        }
    }

    toggleChange(event) {
        let index = arrIds.indexOf(Number(event.target.value));
        if( index == -1){
            arrIds.push(Number(event.target.value));
        } else {
            arrIds.splice(index, 1)
        }
    }

    handleSubmit(values) {
        let menu = {
            "groupId": Number(values.group),
            "menuIds": arrIds
        };

        if(menu.menuIds.length == 0) {
            alert("Please select menu!")
        } else {
            this.props.assignMenusAction(menu);
        }
        this.setState({isChecked: false});
        
    }

    handleSelectGroup(event) {
        if(event.target.value != "") {
            this.props.getMenusByGroupAction(event.target.value);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        
        $( document ).ready(function() {
            $("#checkAll").click(function () {
                $(".check").prop('checked', $(this).prop('checked'));
            });
        });

        return(
            <div className="margin_left_25 margin_top_minus_30">
                <Row>
                    <Col md={12}>
                        <h3>Assign Menu</h3>
                    </Col>
                </Row>
                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
                <Row>
                    <Col md={6}>
                        <Row>
                            <Col md={6}>
                                { this.props.getGroupsStatus.status == 200 ? 
                                    <Field name="group" 
                                        type="select" 
                                        component={SelectBox} 
                                        placeholder="Please select group" 
                                        values={this.state.groups} 
                                        sortBy="name" 
                                        onChange={this.handleSelectGroup}
                                        icon="fa fa-user"/>
                                : null }
                            </Col>
                        </Row>
                        <Row>
                            <Col md={12}>
                                <div className="menu-scroll-box">
                                    <ul>
                                        <li>
                                            <FormGroup onChange={ this.toggleChange }>
                                            <input 
                                                type="checkbox"
                                                checked={this.state.isChecked} 
                                                onChange={this.handleCheckboxChange}
                                                id="checkAll" />
                                                Check / uncheck all
                                                <ul>
                                                    { this.props.getMenus.status == 200 ?
                                                        this.props.getMenus.data.items.map((menu, index) => {
                                                            parentId1 = menu.id;
                                                            return (
                                                                menu.parentId === 0 && menu.isLeaf === 0 ?
                                                                    <li key={index}>
                                                                        <input 
                                                                            type="checkbox"
                                                                            className="check"
                                                                            value={ menu.id } />
                                                                            { menu.menuName }
                                                                        <ul>
                                                                            { this.props.getMenus.data.items.map((submenu1, subindex1) => {
                                                                                parentId2 = submenu1.id;
                                                                                    return (
                                                                                        parentId1 === submenu1.parentId ?
                                                                                            <li key={subindex1}>
                                                                                                <input 
                                                                                                    type="checkbox"
                                                                                                    className="check"
                                                                                                    value={ submenu1.id } />
                                                                                                    { submenu1.menuName }
                                                                                                <ul>
                                                                                                    { this.props.getMenus.data.items.map((submenu2, subindex2) => {
                                                                                                        parentId3 = submenu2.id;
                                                                                                            return (
                                                                                                                parentId2 === submenu2.parentId ?
                                                                                                                    <li key={subindex2}>
                                                                                                                        <input 
                                                                                                                            type="checkbox"
                                                                                                                            className="check"
                                                                                                                            value={ submenu2.id } />
                                                                                                                            { submenu2.menuName }
                                                                                                                        <ul>
                                                                                                                            { this.props.getMenus.data.items.map((submenu3, subindex3) => {
                                                                                                                                    return (
                                                                                                                                        parentId3 === submenu3.parentId ?
                                                                                                                                            <li key={subindex3}>
                                                                                                                                                <input 
                                                                                                                                                    type="checkbox"
                                                                                                                                                    className="check"
                                                                                                                                                    value={ submenu3.id } />
                                                                                                                                                    { submenu3.menuName }
                                                                                                                                            </li>
                                                                                                                                        : null
                                                                                                                                    )
                                                                                                                                })
                                                                                                                            }
                                                                                                                        </ul>
                                                                                                                    </li>
                                                                                                                : null
                                                                                                            )
                                                                                                        })
                                                                                                    }
                                                                                                </ul>
                                                                                            </li>
                                                                                        : null
                                                                                    )
                                                                                })
                                                                            }
                                                                        </ul>
                                                                    </li>
                                                                : null
                                                            )
                                                        })
                                                    : null }
                                                </ul>
                                        </FormGroup>
                                        </li>
                                    </ul>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col mdOffset={9} md={3}>
                                <ButtonSubmit 
                                    error={error} 
                                    invalid={invalid} 
                                    submitting={submitting} 
                                    label="Save" />
                            </Col>
                        </Row>
                    </Col>
                </Row>
                </form>
            </div>
        )
    }

}

const afterSubmit = (result, dispatch) =>
    dispatch(
        reset('form_assign_menu')
    );

AssignMenu = reduxForm({
    form: 'form_assign_menu',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        const errors = {};
        if (values.group === undefined) {
            errors.group = 'Group is required!';
        }
        return errors
    }
})(AssignMenu);

function mapStateToProps(state) {
    if(state.menu.getAllMenus.status == 200){
        state.menu.getAllMenus.data.items.forEach((element) => {
            groupIds.push(Number(element.id));
        });
    }
    return {
        getGroupsStatus: state.userAuthentication.getGroupsStatus,
        getMenus : state.menu.getAllMenus,
        assignMenus: state.menu.assignMenus,
        getMenusByGroup: state.menu.getMenusByGroup
    }
    
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getGroupsStatusAction, 
        getAllMenusAction,
        assignMenusAction,
        getMenusByGroupAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignMenu) ;