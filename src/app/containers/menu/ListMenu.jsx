import React from 'react';
import { Link } from 'react-router';
import { Row, Col, Form, FormGroup } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../components/forms/TextBox';
import { ButtonSubmit } from '../../components/forms/ButtonSubmit';
import { getMenusListAndPageAction } from './../../actions/menu';
import Pagination from '../../components/forms/Pagination';
import './index.css';
import { canUpdate } from './../../../auth';

let menu = {
    limit: 10,
    page: 1,
    keyword: ''
}

class ListMenu extends React.Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.getMenusListAndPageAction(menu);
    }

    handleSubmit(values){
        if(values.keyword !== undefined){
            menu.keyword = values.keyword;
        } else {
            menu.keyword = "";
        }
        this.props.getMenusListAndPageAction(menu);
    }
    
    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25 margin_top_minus_30">
                <Row>
                    <Col md={12}>
                        <h3 className="text-align-center">Menu Management</h3>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={11}>
                        <div className="">
                            <Form inline className="pull-right" 
                                    onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
                                <Field 
                                    name="keyword" 
                                    type="text" 
                                    component={TextBox} 
                                    label="Search keyword"
                                    className="keysearch"
                                    icon="fa fa-search"/>
                            </Form>

                            <table className="list-smaller-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Khmer Name</th>
                                        <th>English Name</th>
                                        <th>Parent ID</th>
                                        <th>Link</th>
                                        <th>Icon</th>
                                        <th>Is Leaf</th>
                                        <th>Order Number</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                { this.props.getMenusListAndPage.status == 200 ?
                                    this.props.getMenusListAndPage.data.items.list.map((menu, index) => {
                                        total = this.props.getMenusListAndPage.data.items.totalCount;
                                        return (
                                            <tr key={index}>
                                                <td>{menu.id}</td>
                                                <td>{menu.menuNameKh}</td>
                                                <td>{menu.menuName}</td>
                                                <td>{menu.parentId }</td>
                                                <td>{menu.link }</td>
                                                <td>{menu.icon }</td>
                                                <td>{menu.isLeaf }</td>
                                                <td>{menu.orderNum}</td>
                                                <td className="text-center">
                                                {
                                                    canUpdate == true ?
                                                        <Link to={`/app/security/menus/edit/${menu.id}`} title="Edit" className='btn btn-info btn-xs'>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </Link>
                                                    : null
                                                }
                                                </td>
                                            </tr>
                                        )
                                    })
                                : <tr>
                                    <td colSpan={9} className="text-align-center">RESULT NOT FOUND</td>
                                </tr> }
                                </tbody>
                            </table>
                            <br/>
                            <Pagination 
                                totalCount = {total}
                                items = {menu}
                                callBackAction = {this.props.getMenusListAndPageAction}
                            />

                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

ListMenu = reduxForm({
    form: 'form_menu_list',
    validate: (values) => {
        let regex_text = /[0-9a-zA-Z]{2,500}/;
        let regex_number = /[0-9]{1,6}/;

        const errors = {};
        if (!regex_text.test(values.meterSerial)) {
            errors.serial = 'Meter Serial is invalid!';
        }
        return errors
    }
})(ListMenu);

function mapStateToProps(state) {
    return ({
        getMenusListAndPage: state.menu.getMenusListAndPage
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getMenusListAndPageAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListMenu);