import React from 'react';
import { Row, Col, Panel } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { Link, browserHistory } from 'react-router';
import { bindActionCreators } from 'redux';
import {connect} from 'react-redux';
import { TextBox } from './../../components/forms/TextBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import { updateMenuAction } from './../../actions/menu';    

class EditMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            selectValue: ''
        }

        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(data){
        if(data.updateMenu.status === 200){
            alert(data.updateMenu.data.message);
            window.history.back();
            return data.updateMenu.status = 0;
        }
        if(data.updateMenu.status === 400 || data.updateMenu.status === 500){
            alert("Fail with update menu!");
            return data.updateMenu.status = 0;
        }
    }

    handleSubmit(values){
        let menu = {
            menuName: values.menuName,
            menuNameKh: values.menuNameKh,
            icon: this.state.selectValue,
            id: this.props.params.id
        };

        this.props.updateMenuAction(menu);
    }

    handleChange(e) {
        this.setState({selectValue: e.target.value});
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={5} lg={5}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <Row>
                                    <Col md={1}>
                                        <Link to="#" onClick={() => window.history.back()}><i className="fa fa-arrow-left" /></Link>
                                    </Col>
                                    <Col md={8}>
                                        <b>Edit Menu</b>
                                    </Col>
                                </Row>
                                
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Khmer Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="menuNameKh" type="text" component={TextBox} label="Khmner Name" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>English Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="menuName" type="text" component={TextBox} label="English Name" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Icon</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <select 
                                                style={{fontFamily: 'FontAwesome'}} 
                                                className="form-control" 
                                                value={this.state.selectValue} 
                                                onChange={this.handleChange} >

                                                <option value="">Please Select Icon</option>
                                                <option value="fa-user">&#xf007; &emsp; fa-user</option>
                                                <option value="fa-align-left">&#xf036; &emsp; fa-align-left</option>
                                                <option value="fa-align-right">&#xf038; &emsp; fa-align-right</option>
                                                <option value="fa-amazon">&#xf270; &emsp; fa-amazon</option>
                                                <option value="fa-ambulance">&#xf0f9; &emsp; fa-ambulance</option>
                                                <option value="fa-anchor">&#xf13d; &emsp; fa-anchor</option>
                                                <option value="fa-android">&#xf17b; &emsp; fa-android</option>
                                                <option value="fa-angellist">&#xf209; &emsp; fa-angellist</option>
                                                <option value="fa-angle-double-down">&#xf103; &emsp; fa-angle-double-down</option>
                                                <option value="fa-angle-double-left">&#xf100; &emsp; fa-angle-double-left</option>
                                                <option value="fa-angle-double-right">&#xf101; &emsp; fa-angle-double-right</option>
                                                <option value="fa-angle-double-up">&#xf102; &emsp; fa-angle-double-up</option>
                                                <option value="fa-angle-left">&#xf104; &emsp; fa-angle-left</option>
                                                <option value="fa-angle-right">&#xf105; &emsp; fa-angle-right</option>
                                                <option value="fa-angle-up">&#xf106; &emsp; fa-angle-up</option>
                                                <option value="fa-apple">&#xf179; &emsp; fa-apple</option>
                                                <option value="fa-archive">&#xf187; &emsp; fa-archive</option>
                                                <option value="fa-area-chart">&#xf1fe; &emsp; fa-area-chart</option>
                                                <option value="fa-arrow-circle-down">&#xf0ab; &emsp; fa-arrow-circle-down</option>
                                                <option value="fa-arrow-circle-left">&#xf0a8; &emsp; fa-arrow-circle-left</option>
                                                <option value="fa-arrow-circle-o-down">&#xf01a; &emsp; fa-arrow-circle-o-down</option>
                                                <option value="fa-arrow-circle-o-left">&#xf190; &emsp; fa-arrow-circle-o-left</option>
                                                <option value="fa-arrow-circle-o-right">&#xf18e; &emsp; fa-arrow-circle-o-right</option>
                                                <option value="fa-arrow-circle-o-up">&#xf01b; &emsp; fa-arrow-circle-o-up</option>
                                                <option value="fa-arrow-circle-right">&#xf0a9; &emsp; fa-arrow-circle-right</option>
                                                <option value="fa-arrow-circle-up">&#xf0aa; &emsp; fa-arrow-circle-up</option>
                                                <option value="fa-arrow-down">&#xf063; &emsp; fa-arrow-down</option>
                                                <option value="fa-arrow-left">&#xf060; &emsp; fa-arrow-left</option>
                                                <option value="fa-arrow-right">&#xf061; &emsp; fa-arrow-right</option>
                                                <option value="fa-arrow-up">&#xf062; &emsp; fa-arrow-up</option>
                                                <option value="fa-arrows">&#xf047; &emsp; fa-arrows</option>
                                                <option value="fa-arrows-alt">&#xf0b2; &emsp; fa-arrows-alt</option>
                                                <option value="fa-arrows-h">&#xf07e; &emsp; fa-arrows-h</option>
                                                <option value="fa-arrows-v">&#xf07d; &emsp; fa-arrows-v</option>
                                                <option value="fa-asterisk">&#xf069; &emsp; fa-asterisk</option>
                                                <option value="fa-at">&#xf1fa; &emsp; fa-at</option>
                                                <option value="fa-automobile">&#xf1b9; &emsp; fa-automobile</option>
                                                <option value="fa-backward">&#xf04a; &emsp; fa-backward</option>
                                                <option value="fa-balance-scale">&#xf24e; &emsp; fa-balance-scale</option>
                                                <option value="fa-ban">&#xf05e; &emsp; fa-ban</option>
                                                <option value="fa-bank">&#xf19c; &emsp; fa-bank</option>
                                                <option value="fa-bar-chart">&#xf080; &emsp; fa-bar-chart</option>
                                                <option value="fa-bar-chart-o">&#xf080; &emsp; fa-bar-chart-o</option>
                                                <option value="fa-battery-full">&#xf240; &emsp; fa-battery-full</option>
                                                <option value="fa-beer">&#xf0fc; &emsp; fa-beer</option>
                                                <option value="fa-behance">&#xf1b4; &emsp; fa-behance</option>
                                                <option value="fa-behance-square">&#xf1b5; &emsp; fa-behance-square</option>
                                                <option value="fa-bell">&#xf0f3; &emsp; fa-bell</option>
                                                <option value="fa-bell-o">&#xf0a2; &emsp; fa-bell-o</option>
                                                <option value="fa-bell-slash">&#xf1f6; &emsp; fa-bell-slash</option>
                                                <option value="fa-bell-slash-o">&#xf1f7; &emsp; fa-bell-slash-o</option>
                                                <option value="fa-bicycle">&#xf206; &emsp; fa-bicycle</option>
                                                <option value="fa-binoculars">&#xf1e5; &emsp; fa-binoculars</option>
                                                <option value="fa-birthday-cake">&#xf1fd; &emsp; fa-birthday-cake</option>
                                                <option value="fa-bitbucket">&#xf171; &emsp; fa-bitbucket</option>
                                                <option value="fa-bitbucket-square">&#xf172; &emsp; fa-bitbucket-square</option>
                                                <option value="fa-bitcoin">&#xf15a; &emsp; fa-bitcoin</option>
                                                <option value="fa-black-tie">&#xf27e; &emsp; fa-black-tie</option>
                                                <option value="fa-bold">&#xf032; &emsp; fa-bold</option>
                                                <option value="fa-bolt">&#xf0e7; &emsp; fa-bolt</option>
                                                <option value="fa-bomb">&#xf1e2; &emsp; fa-bomb</option>
                                                <option value="fa-book">&#xf02d; &emsp; fa-book</option>
                                                <option value="fa-bookmark">&#xf02e; &emsp; fa-bookmark</option>
                                                <option value="fa-bookmark-o">&#xf097; &emsp; fa-bookmark-o</option>
                                                <option value="fa-briefcase">&#xf0b1; &emsp; fa-briefcase</option>
                                                <option value="fa-btc">&#xf15a; &emsp; fa-btc</option>
                                                <option value="fa-bug">&#xf188; &emsp; fa-bug</option>
                                                <option value="fa-building">&#xf1ad; &emsp; fa-building</option>
                                                <option value="fa-building-o">&#xf0f7; &emsp; fa-building-o</option>
                                                <option value="fa-bullhorn">&#xf0a1; &emsp; fa-bullhorn</option>
                                                <option value="fa-bullseye">&#xf140; &emsp; fa-bullseye</option>
                                                <option value="fa-bus">&#xf207; &emsp; fa-bus</option>
                                                <option value="fa-cab">&#xf1ba; &emsp; fa-cab</option>
                                                <option value="fa-calendar">&#xf073; &emsp; fa-calendar</option>
                                                <option value="fa-camera">&#xf030; &emsp; fa-camera</option>
                                                <option value="fa-car">&#xf1b9; &emsp; fa-car</option>
                                                <option value="fa-caret-up">&#xf0d8; &emsp; fa-caret-up</option>
                                                <option value="fa-cart-plus">&#xf217; &emsp; fa-cart-plus</option>
                                                <option value="fa-cc">&#xf20a; &emsp; fa-cc</option>
                                                <option value="fa-cc-amex">&#xf1f3; &emsp; fa-cc-amex</option>
                                                <option value="fa-cc-jcb">&#xf24b; &emsp; fa-cc-jcb</option>
                                                <option value="fa-cc-paypal">&#xf1f4; &emsp; fa-cc-paypal</option>
                                                <option value="fa-cc-stripe">&#xf1f5; &emsp; fa-cc-stripe</option>
                                                <option value="fa-cc-visa">&#xf1f0; &emsp; fa-cc-visa</option>
                                                <option value="fa-chain">&#xf0c1; &emsp; fa-chain</option>
                                                <option value="fa-check">&#xf00c; &emsp; fa-check</option>
                                                <option value="fa-chevron-left">&#xf053; &emsp; fa-chevron-left</option>
                                                <option value="fa-chevron-right">&#xf054; &emsp; fa-chevron-right</option>
                                                <option value="fa-chevron-up">&#xf077; &emsp; fa-chevron-up</option>
                                                <option value="fa-child">&#xf1ae; &emsp; fa-child</option>
                                                <option value="fa-chrome">&#xf268; &emsp; fa-chrome</option>
                                                <option value="fa-circle">&#xf111; &emsp; fa-circle</option>
                                                <option value="fa-circle-o">&#xf10c; &emsp; fa-circle-o</option>
                                                <option value="fa-circle-o-notch">&#xf1ce; &emsp; fa-circle-o-notch</option>
                                                <option value="fa-circle-thin">&#xf1db; &emsp; fa-circle-thin</option>
                                                <option value="fa-clipboard">&#xf0ea; &emsp; fa-clipboard</option>
                                                <option value="fa-clock-o">&#xf017; &emsp; fa-clock-o</option>
                                                <option value="fa-clone">&#xf24d; &emsp; fa-clone</option>
                                                <option value="fa-close">&#xf00d; &emsp; fa-close</option>
                                                <option value="fa-cloud">&#xf0c2; &emsp; fa-cloud</option>
                                                <option value="fa-cloud-download">&#xf0ed; &emsp; fa-cloud-download</option>
                                                <option value="fa-cloud-upload">&#xf0ee; &emsp; fa-cloud-upload</option>
                                                <option value="fa-cny">&#xf157; &emsp; fa-cny</option>
                                                <option value="fa-code">&#xf121; &emsp; fa-code</option>
                                                <option value="fa-code-fork">&#xf126; &emsp; fa-code-fork</option>
                                                <option value="fa-codepen">&#xf1cb; &emsp; fa-codepen</option>
                                                <option value="fa-coffee">&#xf0f4; &emsp; fa-coffee</option>
                                                <option value="fa-cog">&#xf013; &emsp; fa-cog</option>
                                                <option value="fa-cogs">&#xf085; &emsp; fa-cogs</option>
                                                <option value="fa-columns">&#xf0db; &emsp; fa-columns</option>
                                                <option value="fa-comment">&#xf075; &emsp; fa-comment</option>
                                                <option value="fa-comment-o">&#xf0e5; &emsp; fa-comment-o</option>
                                                <option value="fa-commenting">&#xf27a; &emsp; fa-commenting</option>
                                                <option value="fa-commenting-o">&#xf27b; &emsp; fa-commenting-o</option>
                                                <option value="fa-comments">&#xf086; &emsp; fa-comments</option>
                                                <option value="fa-comments-o">&#xf0e6; &emsp; fa-comments-o</option>
                                                <option value="fa-compass">&#xf14e; &emsp; fa-compass</option>
                                                <option value="fa-compress">&#xf066; &emsp; fa-compress</option>
                                                <option value="fa-connectdevelop">&#xf20e; &emsp; fa-connectdevelop</option>
                                                <option value="fa-contao">&#xf26d; &emsp; fa-contao</option>
                                                <option value="fa-copy">&#xf0c5; &emsp; fa-copy</option>
                                                <option value="fa-copyright">&#xf1f9; &emsp; fa-copyright</option>
                                                <option value="fa-creative-commons">&#xf25e; &emsp; fa-creative-commons</option>
                                                <option value="fa-credit-card">&#xf09d; &emsp; fa-credit-card</option>
                                                <option value="fa-crop">&#xf125; &emsp; fa-crop</option>
                                                <option value="fa-crosshairs">&#xf05b; &emsp; fa-crosshairs</option>
                                                <option value="fa-css3">&#xf13c; &emsp; fa-css3</option>
                                                <option value="fa-cube">&#xf1b2; &emsp; fa-cube</option>
                                                <option value="fa-cubes">&#xf1b3; &emsp; fa-cubes</option>
                                                <option value="fa-cut">&#xf0c4; &emsp; fa-cut</option>
                                                <option value="fa-cutlery">&#xf0f5; &emsp; fa-cutlery</option>
                                                <option value="fa-dashboard">&#xf0e4; &emsp; fa-dashboard</option>
                                                <option value="fa-dashcube">&#xf210; &emsp; fa-dashcube</option>
                                                <option value="fa-database">&#xf1c0; &emsp; fa-database</option>
                                                <option value="fa-dedent">&#xf03b; &emsp; fa-dedent</option>
                                                <option value="fa-delicious">&#xf1a5; &emsp; fa-delicious</option>
                                                <option value="fa-desktop">&#xf108; &emsp; fa-desktop</option>
                                                <option value="fa-deviantart">&#xf1bd; &emsp; fa-deviantart</option>
                                                <option value="fa-diamond">&#xf219; &emsp; fa-diamond</option>
                                                <option value="fa-digg">&#xf1a6; &emsp; fa-digg</option>
                                                <option value="fa-dollar">&#xf155; &emsp; fa-dollar</option>
                                                <option value="fa-download">&#xf019; &emsp; fa-download</option>
                                                <option value="fa-dribbble">&#xf17d; &emsp; fa-dribbble</option>
                                                <option value="fa-dropbox">&#xf16b; &emsp; fa-dropbox</option>
                                                <option value="fa-drupal">&#xf1a9; &emsp; fa-drupal</option>
                                                <option value="fa-edit">&#xf044; &emsp; fa-edit</option>
                                                <option value="fa-eject">&#xf052; &emsp; fa-eject</option>
                                                <option value="fa-ellipsis-h">&#xf141; &emsp; fa-ellipsis-h</option>
                                                <option value="fa-ellipsis-v">&#xf142; &emsp; fa-ellipsis-v</option>
                                                <option value="fa-empire">&#xf1d1; &emsp; fa-empire</option>
                                                <option value="fa-envelope">&#xf0e0; &emsp; fa-envelope</option>
                                                <option value="fa-envelope-o">&#xf003; &emsp; fa-envelope-o</option>
                                                <option value="fa-eur">&#xf153; &emsp; fa-eur</option>
                                                <option value="fa-euro">&#xf153; &emsp; fa-euro</option>
                                                <option value="fa-exchange">&#xf0ec; &emsp; fa-exchange</option>
                                                <option value="fa-exclamation">&#xf12a; &emsp; fa-exclamation</option>
                                                <option value="fa-exclamation-circle">&#xf06a; &emsp; fa-exclamation-circle</option>
                                                <option value="fa-exclamation-triangle">&#xf071; &emsp; fa-exclamation-triangle</option>
                                                <option value="fa-expand">&#xf065; &emsp; fa-expand</option>
                                                <option value="fa-expeditedssl">&#xf23e; &emsp; fa-expeditedssl</option>
                                                <option value="fa-external-link">&#xf08e; &emsp; fa-external-link</option>
                                                <option value="fa-external-link-square">&#xf14c; &emsp; fa-external-link-square</option>
                                                <option value="fa-eye">&#xf06e; &emsp; fa-eye</option>
                                                <option value="fa-eye-slash">&#xf070; &emsp; fa-eye-slash</option>
                                                <option value="fa-eyedropper">&#xf1fb; &emsp; fa-eyedropper</option>
                                                <option value="fa-facebook">&#xf09a; &emsp; fa-facebook</option>
                                                <option value="fa-facebook-f">&#xf09a; &emsp; fa-facebook-f</option>
                                                <option value="fa-facebook-official">&#xf230; &emsp; fa-facebook-official</option>
                                                <option value="fa-facebook-square">&#xf082; &emsp; fa-facebook-square</option>
                                                <option value="fa-fast-backward">&#xf049; &emsp; fa-fast-backward</option>
                                                <option value="fa-fast-forward">&#xf050; &emsp; fa-fast-forward</option>
                                                <option value="fa-fax">&#xf1ac; &emsp; fa-fax</option>
                                                <option value="fa-feed">&#xf09e; &emsp; fa-feed</option>
                                                <option value="fa-female">&#xf182; &emsp; fa-female</option>
                                                <option value="fa-fighter-jet">&#xf0fb; &emsp; fa-fighter-jet</option>
                                                <option value="fa-file">&#xf15b; &emsp; fa-file</option>
                                                <option value="fa-file-archive-o">&#xf1c6; &emsp; fa-file-archive-o</option>
                                                <option value="fa-file-audio-o">&#xf1c7; &emsp; fa-file-audio-o</option>
                                                <option value="fa-file-code-o">&#xf1c9; &emsp; fa-file-code-o</option>
                                                <option value="fa-file-excel-o">&#xf1c3; &emsp; fa-file-excel-o</option>
                                                <option value="fa-file-image-o">&#xf1c5; &emsp; fa-file-image-o</option>
                                                <option value="fa-file-movie-o">&#xf1c8; &emsp; fa-file-movie-o</option>
                                                <option value="fa-file-o">&#xf016; &emsp; fa-file-o</option>
                                                <option value="fa-file-pdf-o">&#xf1c1; &emsp; fa-file-pdf-o</option>
                                                <option value="fa-file-photo-o">&#xf1c5; &emsp; fa-file-photo-o</option>
                                                <option value="fa-file-picture-o">&#xf1c5; &emsp; fa-file-picture-o</option>
                                                <option value="fa-file-powerpoint-o">&#xf1c4; &emsp; fa-file-powerpoint-o</option>
                                                <option value="fa-file-sound-o">&#xf1c7; &emsp; fa-file-sound-o</option>
                                                <option value="fa-file-text">&#xf15c; &emsp; fa-file-text</option>
                                                <option value="fa-file-text-o">&#xf0f6; &emsp; fa-file-text-o</option>
                                                <option value="fa-file-video-o">&#xf1c8; &emsp; fa-file-video-o</option>
                                                <option value="fa-file-word-o">&#xf1c2; &emsp; fa-file-word-o</option>
                                                <option value="fa-file-zip-o">&#xf1c6; &emsp; fa-file-zip-o</option>
                                                <option value="fa-files-o">&#xf0c5; &emsp; fa-files-o</option>
                                                <option value="fa-film">&#xf008; &emsp; fa-film</option>
                                                <option value="fa-filter">&#xf0b0; &emsp; fa-filter</option>
                                                <option value="fa-fire">&#xf06d; &emsp; fa-fire</option>
                                                <option value="fa-fire-extinguisher">&#xf134; &emsp; fa-fire-extinguisher</option>
                                                <option value="fa-firefox">&#xf269; &emsp; fa-firefox</option>
                                                <option value="fa-flag">&#xf024; &emsp; fa-flag</option>
                                                <option value="fa-flag-checkered">&#xf11e; &emsp; fa-flag-checkered</option>
                                                <option value="fa-flag-o">&#xf11d; &emsp; fa-flag-o</option>
                                                <option value="fa-flash">&#xf0e7; &emsp; fa-flash</option>
                                                <option value="fa-flask">&#xf0c3; &emsp; fa-flask</option>
                                                <option value="fa-flickr">&#xf16e; &emsp; fa-flickr</option>
                                                <option value="fa-floppy-o">&#xf0c7; &emsp; fa-floppy-o</option>
                                                <option value="fa-folder">&#xf07b; &emsp; fa-folder</option>
                                                <option value="fa-folder-o">&#xf114; &emsp; fa-folder-o</option>
                                                <option value="fa-folder-open">&#xf07c; &emsp; fa-folder-open</option>
                                                <option value="fa-folder-open-o">&#xf115; &emsp; fa-folder-open-o</option>
                                                <option value="fa-font">&#xf031; &emsp; fa-font</option>
                                                <option value="fa-fonticons">&#xf280; &emsp; fa-fonticons</option>
                                                <option value="fa-forumbee">&#xf211; &emsp; fa-forumbee</option>
                                                <option value="fa-forward">&#xf04e; &emsp; fa-forward</option>
                                                <option value="fa-foursquare">&#xf180; &emsp; fa-foursquare</option>
                                                <option value="fa-frown-o">&#xf119; &emsp; fa-frown-o</option>
                                                <option value="fa-futbol-o">&#xf1e3; &emsp; fa-futbol-o</option>
                                                <option value="fa-gamepad">&#xf11b; &emsp; fa-gamepad</option>
                                                <option value="fa-gavel">&#xf0e3; &emsp; fa-gavel</option>
                                                <option value="fa-gbp">&#xf154; &emsp; fa-gbp</option>
                                                <option value="fa-ge">&#xf1d1; &emsp; fa-ge</option>
                                                <option value="fa-gear">&#xf013; &emsp; fa-gear</option>
                                                <option value="fa-gears">&#xf085; &emsp; fa-gears</option>
                                                <option value="fa-genderless">&#xf22d; &emsp; fa-genderless</option>
                                                <option value="fa-get-pocket">&#xf265; &emsp; fa-get-pocket</option>
                                                <option value="fa-gg">&#xf260; &emsp; fa-gg</option>
                                                <option value="fa-gg-circle">&#xf261; &emsp; fa-gg-circle</option>
                                                <option value="fa-gift">&#xf06b; &emsp; fa-gift</option>
                                                <option value="fa-git">&#xf1d3; &emsp; fa-git</option>
                                                <option value="fa-git-square">&#xf1d2; &emsp; fa-git-square</option>
                                                <option value="fa-github">&#xf09b; &emsp; fa-github</option>
                                                <option value="fa-github-alt">&#xf113; &emsp; fa-github-alt</option>
                                                <option value="fa-github-square">&#xf092; &emsp; fa-github-square</option>
                                                <option value="fa-gittip">&#xf184; &emsp; fa-gittip</option>
                                                <option value="fa-glass">&#xf000; &emsp; fa-glass</option>
                                                <option value="fa-globe">&#xf0ac; &emsp; fa-globe</option>
                                                <option value="fa-google">&#xf1a0; &emsp; fa-google</option>
                                                <option value="fa-google-plus">&#xf0d5; &emsp; fa-google-plus</option>
                                                <option value="fa-google-plus-square">&#xf0d4; &emsp; fa-google-plus-square</option>
                                                <option value="fa-google-wallet">&#xf1ee; &emsp; fa-google-wallet</option>
                                                <option value="fa-graduation-cap">&#xf19d; &emsp; fa-graduation-cap</option>
                                                <option value="fa-gratipay">&#xf184; &emsp; fa-gratipay</option>
                                                <option value="fa-group">&#xf0c0; &emsp; fa-group</option>
                                                <option value="fa-h-square">&#xf0fd; &emsp; fa-h-square</option>
                                                <option value="fa-hacker-news">&#xf1d4; &emsp; fa-hacker-news</option>
                                                <option value="fa-hand-grab-o">&#xf255; &emsp; fa-hand-grab-o</option>
                                                <option value="fa-hand-lizard-o">&#xf258; &emsp; fa-hand-lizard-o</option>
                                                <option value="fa-hand-o-down">&#xf0a7; &emsp; fa-hand-o-down</option>
                                                <option value="fa-hand-o-left">&#xf0a5; &emsp; fa-hand-o-left</option>
                                                <option value="fa-hand-o-right">&#xf0a4; &emsp; fa-hand-o-right</option>
                                                <option value="fa-hand-o-up">&#xf0a6; &emsp; fa-hand-o-up</option>
                                                <option value="fa-hand-paper-o">&#xf256; &emsp; fa-hand-paper-o</option>
                                                <option value="fa-hand-peace-o">&#xf25b; &emsp; fa-hand-peace-o</option>
                                                <option value="fa-hand-pointer-o">&#xf25a; &emsp; fa-hand-pointer-o</option>
                                                <option value="fa-hand-rock-o">&#xf255; &emsp; fa-hand-rock-o</option>
                                                <option value="fa-hand-scissors-o">&#xf257; &emsp; fa-hand-scissors-o</option>
                                                <option value="fa-hand-spock-o">&#xf259; &emsp; fa-hand-spock-o</option>
                                                <option value="fa-hand-stop-o">&#xf256; &emsp; fa-hand-stop-o</option>
                                                <option value="fa-hdd-o">&#xf0a0; &emsp; fa-hdd-o</option>
                                                <option value="fa-header">&#xf1dc; &emsp; fa-header</option>
                                                <option value="fa-headphones">&#xf025; &emsp; fa-headphones</option>
                                                <option value="fa-heart">&#xf004; &emsp; fa-heart</option>
                                                <option value="fa-heart-o">&#xf08a; &emsp; fa-heart-o</option>
                                                <option value="fa-heartbeat">&#xf21e; &emsp; fa-heartbeat</option>
                                                <option value="fa-history">&#xf1da; &emsp; fa-history</option>
                                                <option value="fa-home">&#xf015; &emsp; fa-home</option>
                                                <option value="fa-hospital-o">&#xf0f8; &emsp; fa-hospital-o</option>
                                                <option value="fa-hotel">&#xf236; &emsp; fa-hotel</option>
                                                <option value="fa-hourglass">&#xf254; &emsp; fa-hourglass</option>
                                                <option value="fa-hourglass-1">&#xf251; &emsp; fa-hourglass-1</option>
                                                <option value="fa-hourglass-2">&#xf252; &emsp; fa-hourglass-2</option>
                                                <option value="fa-hourglass-3">&#xf253; &emsp; fa-hourglass-3</option>
                                                <option value="fa-hourglass-end">&#xf253; &emsp; fa-hourglass-end</option>
                                                <option value="fa-hourglass-half">&#xf252; &emsp; fa-hourglass-half</option>
                                                <option value="fa-hourglass-o">&#xf250; &emsp; fa-hourglass-o</option>
                                                <option value="fa-hourglass-start">&#xf251; &emsp; fa-hourglass-start</option>
                                                <option value="fa-houzz">&#xf27c; &emsp; fa-houzz</option>
                                                <option value="fa-html5">&#xf13b; &emsp; fa-html5</option>
                                                <option value="fa-i-cursor">&#xf246; &emsp; fa-i-cursor</option>
                                                <option value="fa-ils">&#xf20b; &emsp; fa-ils</option>
                                                <option value="fa-image">&#xf03e; &emsp; fa-image</option>
                                                <option value="fa-inbox">&#xf01c; &emsp; fa-inbox</option>
                                                <option value="fa-indent">&#xf03c; &emsp; fa-indent</option>
                                                <option value="fa-industry">&#xf275; &emsp; fa-industry</option>
                                                <option value="fa-info">&#xf129; &emsp; fa-info</option>
                                                <option value="fa-info-circle">&#xf05a; &emsp; fa-info-circle</option>
                                                <option value="fa-inr">&#xf156; &emsp; fa-inr</option>
                                                <option value="fa-instagram">&#xf16d; &emsp; fa-instagram</option>
                                                <option value="fa-institution">&#xf19c; &emsp; fa-institution</option>
                                                <option value="fa-internet-explorer">&#xf26b; &emsp; fa-internet-explorer</option>
                                                <option value="fa-intersex">&#xf224; &emsp; fa-intersex</option>
                                                <option value="fa-ioxhost">&#xf208; &emsp; fa-ioxhost</option>
                                                <option value="fa-italic">&#xf033; &emsp; fa-italic</option>
                                                <option value="fa-joomla">&#xf1aa; &emsp; fa-joomla</option>
                                                <option value="fa-jpy">&#xf157; &emsp; fa-jpy</option>
                                                <option value="fa-jsfiddle">&#xf1cc; &emsp; fa-jsfiddle</option>
                                                <option value="fa-key">&#xf084; &emsp; fa-key</option>
                                                <option value="fa-keyboard-o">&#xf11c; &emsp; fa-keyboard-o</option>
                                                <option value="fa-krw">&#xf159; &emsp; fa-krw</option>
                                                <option value="fa-language">&#xf1ab; &emsp; fa-language</option>
                                                <option value="fa-laptop">&#xf109; &emsp; fa-laptop</option>
                                                <option value="fa-lastfm">&#xf202; &emsp; fa-lastfm</option>
                                                <option value="fa-lastfm-square">&#xf203; &emsp; fa-lastfm-square</option>
                                                <option value="fa-leaf">&#xf06c; &emsp; fa-leaf</option>
                                                <option value="fa-leanpub">&#xf212; &emsp; fa-leanpub</option>
                                                <option value="fa-legal">&#xf0e3; &emsp; fa-legal</option>
                                                <option value="fa-lemon-o">&#xf094; &emsp; fa-lemon-o</option>
                                                <option value="fa-level-down">&#xf149; &emsp; fa-level-down</option>
                                                <option value="fa-level-up">&#xf148; &emsp; fa-level-up</option>
                                                <option value="fa-life-bouy">&#xf1cd; &emsp; fa-life-bouy</option>
                                                <option value="fa-life-buoy">&#xf1cd; &emsp; fa-life-buoy</option>
                                                <option value="fa-life-ring">&#xf1cd; &emsp; fa-life-ring</option>
                                                <option value="fa-life-saver">&#xf1cd; &emsp; fa-life-saver</option>
                                                <option value="fa-lightbulb-o">&#xf0eb; &emsp; fa-lightbulb-o</option>
                                                <option value="fa-line-chart">&#xf201; &emsp; fa-line-chart</option>
                                                <option value="fa-link">&#xf0c1; &emsp; fa-link</option>
                                                <option value="fa-linkedin">&#xf0e1; &emsp; fa-linkedin</option>
                                                <option value="fa-linkedin-square">&#xf08c; &emsp; fa-linkedin-square</option>
                                                <option value="fa-linux">&#xf17c; &emsp; fa-linux</option>
                                                <option value="fa-list">&#xf03a; &emsp; fa-list</option>
                                                <option value="fa-list-alt">&#xf022; &emsp; fa-list-alt</option>
                                                <option value="fa-list-ol">&#xf0cb; &emsp; fa-list-ol</option>
                                                <option value="fa-list-ul">&#xf0ca; &emsp; fa-list-ul</option>
                                                <option value="fa-location-arrow">&#xf124; &emsp; fa-location-arrow</option>
                                                <option value="fa-lock">&#xf023; &emsp; fa-lock</option>
                                                <option value="fa-long-arrow-down">&#xf175; &emsp; fa-long-arrow-down</option>
                                                <option value="fa-long-arrow-left">&#xf177; &emsp; fa-long-arrow-left</option>
                                                <option value="fa-long-arrow-right">&#xf178; &emsp; fa-long-arrow-right</option>
                                                <option value="fa-long-arrow-up">&#xf176; &emsp; fa-long-arrow-up</option>
                                                <option value="fa-magic">&#xf0d0; &emsp; fa-magic</option>
                                                <option value="fa-magnet">&#xf076; &emsp; fa-magnet</option>
                                                <option value="fa-mars-stroke-v">&#xf22a; &emsp; fa-mars-stroke-v</option>
                                                <option value="fa-maxcdn">&#xf136; &emsp; fa-maxcdn</option>
                                                <option value="fa-meanpath">&#xf20c; &emsp; fa-meanpath</option>
                                                <option value="fa-medium">&#xf23a; &emsp; fa-medium</option>
                                                <option value="fa-medkit">&#xf0fa; &emsp; fa-medkit</option>
                                                <option value="fa-meh-o">&#xf11a; &emsp; fa-meh-o</option>
                                                <option value="fa-mercury">&#xf223; &emsp; fa-mercury</option>
                                                <option value="fa-microphone">&#xf130; &emsp; fa-microphone</option>
                                                <option value="fa-mobile">&#xf10b; &emsp; fa-mobile</option>
                                                <option value="fa-motorcycle">&#xf21c; &emsp; fa-motorcycle</option>
                                                <option value="fa-mouse-pointer">&#xf245; &emsp; fa-mouse-pointer</option>
                                                <option value="fa-music">&#xf001; &emsp; fa-music</option>
                                                <option value="fa-navicon">&#xf0c9; &emsp; fa-navicon</option>
                                                <option value="fa-neuter">&#xf22c; &emsp; fa-neuter</option>
                                                <option value="fa-newspaper-o">&#xf1ea; &emsp; fa-newspaper-o</option>
                                                <option value="fa-opencart">&#xf23d; &emsp; fa-opencart</option>
                                                <option value="fa-openid">&#xf19b; &emsp; fa-openid</option>
                                                <option value="fa-opera">&#xf26a; &emsp; fa-opera</option>
                                                <option value="fa-outdent">&#xf03b; &emsp; fa-outdent</option>
                                                <option value="fa-pagelines">&#xf18c; &emsp; fa-pagelines</option>
                                                <option value="fa-paper-plane-o">&#xf1d9; &emsp; fa-paper-plane-o</option>
                                                <option value="fa-paperclip">&#xf0c6; &emsp; fa-paperclip</option>
                                                <option value="fa-paragraph">&#xf1dd; &emsp; fa-paragraph</option>
                                                <option value="fa-paste">&#xf0ea; &emsp; fa-paste</option>
                                                <option value="fa-pause">&#xf04c; &emsp; fa-pause</option>
                                                <option value="fa-paw">&#xf1b0; &emsp; fa-paw</option>
                                                <option value="fa-paypal">&#xf1ed; &emsp; fa-paypal</option>
                                                <option value="fa-pencil">&#xf040; &emsp; fa-pencil</option>
                                                <option value="fa-pencil-square-o">&#xf044; &emsp; fa-pencil-square-o</option>
                                                <option value="fa-phone">&#xf095; &emsp; fa-phone</option>
                                                <option value="fa-photo">&#xf03e; &emsp; fa-photo</option>
                                                <option value="fa-picture-o">&#xf03e; &emsp; fa-picture-o</option>
                                                <option value="fa-pie-chart">&#xf200; &emsp; fa-pie-chart</option>
                                                <option value="fa-pied-piper">&#xf1a7; &emsp; fa-pied-piper</option>
                                                <option value="fa-pied-piper-alt">&#xf1a8; &emsp; fa-pied-piper-alt</option>
                                                <option value="fa-pinterest">&#xf0d2; &emsp; fa-pinterest</option>
                                                <option value="fa-pinterest-p">&#xf231; &emsp; fa-pinterest-p</option>
                                                <option value="fa-pinterest-square">&#xf0d3; &emsp; fa-pinterest-square</option>
                                                <option value="fa-plane">&#xf072; &emsp; fa-plane</option>
                                                <option value="fa-play">&#xf04b; &emsp; fa-play</option>
                                                <option value="fa-play-circle">&#xf144; &emsp; fa-play-circle</option>
                                                <option value="fa-play-circle-o">&#xf01d; &emsp; fa-play-circle-o</option>
                                                <option value="fa-plug">&#xf1e6; &emsp; fa-plug</option>
                                                <option value="fa-plus">&#xf067; &emsp; fa-plus</option>
                                                <option value="fa-plus-circle">&#xf055; &emsp; fa-plus-circle</option>
                                                <option value="fa-plus-square">&#xf0fe; &emsp; fa-plus-square</option>
                                                <option value="fa-plus-square-o">&#xf196; &emsp; fa-plus-square-o</option>
                                                <option value="fa-power-off">&#xf011; &emsp; fa-power-off</option>
                                                <option value="fa-print">&#xf02f; &emsp; fa-print</option>
                                                <option value="fa-puzzle-piece">&#xf12e; &emsp; fa-puzzle-piece</option>
                                                <option value="fa-qq">&#xf1d6; &emsp; fa-qq</option>
                                                <option value="fa-qrcode">&#xf029; &emsp; fa-qrcode</option>
                                                <option value="fa-question">&#xf128; &emsp; fa-question</option>
                                                <option value="fa-question-circle">&#xf059; &emsp; fa-question-circle</option>
                                                <option value="fa-quote-left">&#xf10d; &emsp; fa-quote-left</option>
                                                <option value="fa-quote-right">&#xf10e; &emsp; fa-quote-right</option>
                                                <option value="fa-ra">&#xf1d0; &emsp; fa-ra</option>
                                                <option value="fa-random">&#xf074; &emsp; fa-random</option>
                                                <option value="fa-rebel">&#xf1d0; &emsp; fa-rebel</option>
                                                <option value="fa-recycle">&#xf1b8; &emsp; fa-recycle</option>
                                                <option value="fa-reddit">&#xf1a1; &emsp; fa-reddit</option>
                                                <option value="fa-reddit-square">&#xf1a2; &emsp; fa-reddit-square</option>
                                                <option value="fa-refresh">&#xf021; &emsp; fa-refresh</option>
                                                <option value="fa-registered">&#xf25d; &emsp; fa-registered</option>
                                                <option value="fa-remove">&#xf00d; &emsp; fa-remove</option>
                                                <option value="fa-renren">&#xf18b; &emsp; fa-renren</option>
                                                <option value="fa-reorder">&#xf0c9; &emsp; fa-reorder</option>
                                                <option value="fa-repeat">&#xf01e; &emsp; fa-repeat</option>
                                                <option value="fa-reply">&#xf112; &emsp; fa-reply</option>
                                                <option value="fa-reply-all">&#xf122; &emsp; fa-reply-all</option>
                                                <option value="fa-retweet">&#xf079; &emsp; fa-retweet</option>
                                                <option value="fa-rmb">&#xf157; &emsp; fa-rmb</option>
                                                <option value="fa-road">&#xf018; &emsp; fa-road</option>
                                                <option value="fa-rocket">&#xf135; &emsp; fa-rocket</option>
                                                <option value="fa-rotate-left">&#xf0e2; &emsp; fa-rotate-left</option>
                                                <option value="fa-rotate-right">&#xf01e; &emsp; fa-rotate-right</option>
                                                <option value="fa-rouble">&#xf158; &emsp; fa-rouble</option>
                                                <option value="fa-rss">&#xf09e; &emsp; fa-rss</option>
                                                <option value="fa-rss-square">&#xf143; &emsp; fa-rss-square</option>
                                                <option value="fa-rub">&#xf158; &emsp; fa-rub</option>
                                                <option value="fa-ruble">&#xf158; &emsp; fa-ruble</option>
                                                <option value="fa-rupee">&#xf156; &emsp; fa-rupee</option>
                                                <option value="fa-safari">&#xf267; &emsp; fa-safari</option>
                                                <option value="fa-sliders">&#xf1de; &emsp; fa-sliders</option>
                                                <option value="fa-slideshare">&#xf1e7; &emsp; fa-slideshare</option>
                                                <option value="fa-smile-o">&#xf118; &emsp; fa-smile-o</option>
                                                <option value="fa-sort-asc">&#xf0de; &emsp; fa-sort-asc</option>
                                                <option value="fa-sort-desc">&#xf0dd; &emsp; fa-sort-desc</option>
                                                <option value="fa-sort-down">&#xf0dd; &emsp; fa-sort-down</option>
                                                <option value="fa-spinner">&#xf110; &emsp; fa-spinner</option>
                                                <option value="fa-spoon">&#xf1b1; &emsp; fa-spoon</option>
                                                <option value="fa-spotify">&#xf1bc; &emsp; fa-spotify</option>
                                                <option value="fa-square">&#xf0c8; &emsp; fa-square</option>
                                                <option value="fa-square-o">&#xf096; &emsp; fa-square-o</option>
                                                <option value="fa-star">&#xf005; &emsp; fa-star</option>
                                                <option value="fa-star-half">&#xf089; &emsp; fa-star-half</option>
                                                <option value="fa-stop">&#xf04d; &emsp; fa-stop</option>
                                                <option value="fa-subscript">&#xf12c; &emsp; fa-subscript</option>
                                                <option value="fa-tablet">&#xf10a; &emsp; fa-tablet</option>
                                                <option value="fa-tachometer">&#xf0e4; &emsp; fa-tachometer</option>
                                                <option value="fa-tag">&#xf02b; &emsp; fa-tag</option>
                                                <option value="fa-tags">&#xf02c; &emsp; fa-tags</option>
                                            </select>
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col mdOffset={8} lgOffset={8} md={4} lg={4}>
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

EditMenu = reduxForm({
    form: 'form_edit_menu',
    validate: (values) => {
        let regex_name = /[a-zA-Z]{2,40}/;

        const errors = {};
        if (values.menuNameKh === undefined) {
            errors.menuNameKh = 'Khmer name is required and at least 2 character!';
        }
        if (!regex_name.test(values.menuName) || values.menuName === undefined) {
            errors.menuName = 'English name is required and at least 2 character!';
        }
        return errors
    }
})(EditMenu);

function mapStateToProps(state, own_props) {
    let menu = {
        menuName: "",
        menuNameKh: "",
        icon: ""
    };
    let data = {};
    if (state.menu.getMenusListAndPage.status == 200) {
        data = state.menu.getMenusListAndPage.data.items.list.find(menuItem => menuItem.id == own_props.params.id ) || menu;
        menu = {
            menuName: data.menuName,
            menuNameKh: data.menuNameKh,
            icon: data.icon
        };
    }
    return ({
        updateMenu : state.menu.updateMenu,
        initialValues: menu
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ updateMenuAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditMenu);