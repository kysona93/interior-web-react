import React from 'react';
import {Row, Col, Image } from 'react-bootstrap';
import HeaderNavigation from '../../components/shares/HeaderNavigation';
import MenuPicture from '../../components/shares/MenuPicture';
import FooterWeb from '../../components/shares/FooterWeb';
import MenuWeb from '../../components/shares/MenuWeb';
import './contact.css';

class ContactUs extends React.Component {
    constructor(props){
        super(props);
    }

    render(){

        return(
            <div>
                <HeaderNavigation />
                {/*<MenuWeb />*/}
                <MenuPicture name="images/menu-image-contactus.jpg"/>
                <div className="container">
                    <Row style={{marginTop:'100px'}}>
                        <Col sm={6} md={2}></Col>
                        <Col sm={6} md={4}>
                            <h2>Our Address</h2>
                            <p>No. 20, Street 217</p>
                            <p>Sangkat Psa Dey Huy</p>
                            <p>Khan Sensok</p>
                            <p>Phnom Phenh, Cambodia</p>
                        </Col>
                        <Col sm={6} md={4}>
                            <h2>Contact Us</h2>
                            <p>Phone: 078 999 167</p>
                            <p>Email: trysenghay@yahoo.com</p>
                        </Col>
                        <Col sm={6} md={2}></Col>
                    </Row>
                    <Row style={{marginTop:'100px'}}>
                        <Col sm={12} md={12}>
                            <h2>Our Location</h2>
                            <div id="map"></div>

                            <iframe
                                width="600"
                                height="450"
                                frameBorder="0" style={{border:"0"}}
                                src="https://www.google.com/maps/embed/v1/place?key=AIzaSyCmbX0vvffkEOzv-QAt4j7b2xGlMuaHlj8&q=Space+Needle,Seattle+WA"
                                allowFullScreen>

                            </iframe>

                        </Col>
                    </Row>
                    <br/><br/><br/>
                </div>




                <FooterWeb />
            </div>
        )
    }
}

export default ContactUs;