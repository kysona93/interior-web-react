import React from 'react';
import { Row, Col } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import '../../../components/forms/Styles.css';
import '../../index.css';

class StepWork extends React.Component {
    constructor(props){
        super(props);
    }


    onCloseInstaller(value){
        this.setState({ show: value })
    }

    static handleGenerate(){
        let divToPrint = document.getElementById('step-work-to-xls');
        let htmlToPrint = '' +
            '<style type="text/css">' +
            'table th {' +
            'border-bottom:2px solid #000000;'+
            'margin-right:0;' +
            'color:black'+
            'background-color:#000;'+
            '}' +
            'tr td{' +
            'border-bottom:1px solid #000000;'+
            'background-color:#fff;'+
            '}' +
            '</style>';
        htmlToPrint += divToPrint.outerHTML;
        let newWin = window.open("");
        newWin.document.write("Project No : ");
        newWin.document.write("Step: \n Date: ");
        newWin.document.write(htmlToPrint);
        newWin.print();
        newWin.close();
    }


    render(){
        const installers = [];
        return (
            <div className="container-fluid">
                <Row>
                    <Col lg={12}>
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="step-work-to-xls"
                            filename="step-work"
                            sheet="Step Work"
                            buttonText="Generate Step"/>
                        <div>
                            <table id="step-work-to-xls" className="table table-bordered table-striped">
                                <thead style={{display:'none'}}>
                                    <tr>
                                        <th colSpan={15} style={{textAlign: 'left'}}>
                                            <strong>Project No : 12324</strong>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colSpan={15} style={{textAlign: 'left'}}>
                                            <strong>Step : In-T2-17-0051</strong>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colSpan={15} style={{textAlign: 'left'}}>
                                            <strong>Date : 5-Sep-17</strong>
                                        </th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Phone</th>
                                        <th>Address</th>
                                        <th>Registered Date</th>
                                        <th>Type</th>
                                        <th>Ampere</th>
                                        <th>Pole</th>
                                        <th>Connect Date</th>
                                        <th>Cable CU</th>
                                        <th>Breaker</th>
                                        <th>Meter</th>
                                        <th>Installer</th>
                                        <th>Remark</th>
                                    </tr>
                                </thead>
                                <tbody style={{textAlign: 'center'}}>
                                    <tr>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>DDd</td>
                                        <td>087767644</td>
                                        <td>1</td>
                                        <td>GGG</td>
                                        <td>DDd</td>
                                        <td>087767644</td>
                                        <td>087767644</td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                    </tr>
                                    {[1, 2, 3].map((index) => {
                                        return(
                                            <tr key={index}>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                            </tr>
                                        )
                                    })}
                                    <tr>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td colSpan={3}>
                                            <strong>Request By</strong>
                                            <p>Name : ................................</p>
                                            <p>Date : ........../........../..........</p>
                                        </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td colSpan={3}>
                                            <strong>Check By</strong>
                                            <p>Name : ................................</p>
                                            <p>Date : ........../........../..........</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default StepWork;