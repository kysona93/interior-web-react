import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getStepWorksAction, deleteStepWorkAction } from '../../../actions/installation/stepwork/stepwork';
import '../../../components/forms/Styles.css';
import '../../index.css';
import moment from 'moment';
import EditStepWork from './Edit';

class ListStepWork extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            check: false,
            stepWork: null,
            button: "",
            status: 0,
            label : 'Show New Customer'
        };
        this.handleSelectType = this.handleSelectType.bind(this);
    }

    handleBack(value){
        this.setState({check: value});
    }

    componentWillMount(){
        this.props.getStepWorksAction({
            type: ""
        });
    }

    componentWillReceiveProps(data){
        if(data.workDelete.status === 200){
            this.props.getStepWorksAction({
                type: ""
            });
            alert("Successful delete step work !");
            data.workDelete.status = 0;
        }
    }

    handleCheck(data){
        this.setState({button: "Check", check: true, stepWork: data});
    }

    handleEdit(data){
        this.setState({button: "Edit", check: true, stepWork: data});
    }

    handleRemove(id){
        if(confirm("Are you sure you want to delete this step?") === true){
            this.props.deleteStepWorkAction(id);
        }
    }

    handleView(data){
        this.setState({button: "Print", check: true, stepWork: data});
        console.log(JSON.stringify(data));
    }

    handleSelectType(e){
        this.props.getStepWorksAction({
            type: e.target.value
        });
    }

    handleSubmit(){

    }
    render() {
        const {handleSubmit, invalid, submitting} = this.props;
        return (
            <div className="container-fluid">
                { !this.state.check ?
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title"><span style={{color: '#1c91d0', fontWeight: 'bold', fontSize: '20px'}}>View All Step Works</span></h3>
                        </div>
                        <div className="panel-body">
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                                        <Row>
                                            <Col lgOffset={6} lg={6}>
                                                <table className="table table-inverse search-installer">
                                                    <tbody>
                                                    <tr>
                                                        <td><p className="search-installer">Year</p></td>
                                                        <td colSpan ={2}><Field name="year" type="text" component={TextBox} label="Year of created step"/></td>
                                                    </tr>
                                                    <tr>
                                                        <td><p className="search-installer">Step Type </p></td>
                                                        <td>
                                                            <Field name="stepType" type="select" onChange={this.handleSelectType} component={SelectBox} placeholder="All"
                                                                   values={[{id: 'New Connection', name: 'New Customer'}, {id: 'Other Income', name: 'Other Income'}]} />
                                                        </td>
                                                        <td><ButtonSubmit className="search-installer" invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </Col>
                                        </Row>
                                    </form>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} sm={12} md={12} lg={12}>
                                    <div>
                                        <table className="table table-bordered table-striped installer">
                                            <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Step No</th>
                                                <th>Step Type</th>
                                                <th>Customers</th>
                                                <th>Prepared By</th>
                                                <th>Prepared Date</th>
                                                <th>Checked Date</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody style={{textAlign: 'center'}}>
                                            { this.props.works.items === undefined ?
                                                <tr>
                                                    <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                                </tr>
                                                :
                                                this.props.works.items.map((work, index) => {
                                                    return(
                                                        <tr key={index}>
                                                            <td>{index + 1}</td>
                                                            <td><a onClick={() => this.handleView(work)}>{work.stepNo}</a></td>
                                                            <td>{work.stepType}</td>
                                                            <td>{work.customer}</td>
                                                            <td>{work.preparedBy}</td>
                                                            <td>{moment(work.preparedDate).format("YYYY-MM-DD")}</td>
                                                            <td>
                                                                { work.checkedDate !== null ?
                                                                    moment(work.checkedDate).format("YYYY-MM-DD")
                                                                    :
                                                                    <a onClick={() => this.handleCheck(work)}>
                                                                        <i className="fa fa-check-square-o fa-lg text-success"> </i>
                                                                    </a>
                                                                }
                                                            </td>
                                                            <td className={work.status === 0 ? "new_customer text-align-center" : work.status === 1 ? "installing text-align-center" : "installed text-align-center"}>{work.status === 0 ? "New step" : work.status === 1 ? "Processing" : work.status === 2 ? "Completed" : "Canceled"}</td>
                                                            <td>
                                                                { work.status >= 1 ?
                                                                    null
                                                                    :
                                                                    <div>
                                                                        <a onClick={() => this.handleEdit(work)}>
                                                                            <i className="fa fa-pencil-square-o fa-lg"> </i>
                                                                        </a>&nbsp; | &nbsp;
                                                                        <a onClick={() => this.handleRemove(work.id)}>
                                                                            <i className="fa fa-minus-circle fa-lg text-danger"> </i>
                                                                        </a>
                                                                    </div>
                                                                }
                                                            </td>
                                                        </tr>
                                                    )
                                                })
                                            }
                                            </tbody>
                                        </table>
                                    </div>
                                </Col>
                            </Row>
                            <Button bsStyle="primary" style={{float: 'right'}} onClick={() => location.href='/app/installation/step'}>Add</Button>
                        </div>
                    </div>
                    :
                    <EditStepWork button={this.state.button} handleBack={this.handleBack.bind(this)} stepWork={this.state.stepWork} />
                }
            </div>
        )
    }
}

ListStepWork = reduxForm({
    form: 'form_list_step_work'
})(ListStepWork);

function mapStateToProps(state) {
    //console.log("Work ", state.stepWork.workDelete);
    return {
        works: state.stepWork.works,
        workDelete: state.stepWork.workDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getStepWorksAction, deleteStepWorkAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListStepWork);