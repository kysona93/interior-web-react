import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { authObj } from '../../../../auth';
import { addStepWorkAction, getStepWorkAction } from '../../../actions/installation/stepwork/stepwork';
import moment from 'moment';

class AddStepWork extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            preparedDate: null,
            checkedDate: null,
            stepWorkDetails: this.props.stepWorkDetails,
            workId: 1
        };
        this.handlePreparedDate = this.handlePreparedDate.bind(this);
        this.handleCheckedDate = this.handleCheckedDate.bind(this);
    }

    componentWillMount(){
        this.props.getStepWorkAction();
    }

    componentDidMount(){
        const data = {
            stepType: this.props.stepType,
            preparedBy: authObj().sub,
            customers: this.state.stepWorkDetails.filter((thing, index, self) => self.findIndex((t) => {return t.customerId === thing.customerId }) === index).length
        };
        this.props.dispatch(initialize('form_add_step_work', data));
    }

    componentWillReceiveProps(data){
        let workId = 1;
        if(data.work.items !== undefined){
            workId = data.work.items.id;
        }
        this.props.dispatch(change('form_add_step_work','stepNo', "ST-".concat(new Date().getFullYear().toString(), "-" + workId)));

        if(data.workAdd.status === 200){
            alert('Successful add new Step Work !!');
            this.props.dispatch(initialize('form_register_customer', null));
            data.workAdd.status = 0;
        } else if(data.workAdd.status === (400 || 404) || data.workAdd.status === (500 || 502)){
            alert(data.workAdd.status + " Error !! \nFail add new Step Work !!\n" + data.workAdd.data.message);
            data.workAdd.status = 0;
        } else if(data.workAdd.status === 401){
            alert("Please Login !!");
            data.workAdd.status = 0;
        }
    }

    handlePreparedDate(date) {
        this.setState({preparedDate: date});
    }

    handleCheckedDate(date) {
        this.setState({checkedDate: date});
    }

    handleBack(){
        this.props.handleBack(false);
    }

    handleRemove(id){
        this.setState({stepWorkDetails: this.state.stepWorkDetails.filter(work => work.id !== id)});
    }

    handleSubmit(value){
        if(this.state.stepWorkDetails.length <= 0){
            alert("Nothing to save !!")
        } else {
            const ids = [];
            this.state.stepWorkDetails.map((work) => {
                ids.push(work.id)
            });
            this.props.addStepWorkAction({
                customer: Number(value.customers),
                description: value.description.trim(),
                preparedBy: value.preparedBy.trim(),
                preparedDate: value.preparedDate,
                stepNo: value.stepNo.trim(),
                stepType: value.stepType.trim(),
                workDetailIds: ids
            });
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">New Step of Work</h3>
                </div>
                <div className="panel-body">
                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                        <Row>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Step Work No <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="stepNo" type="text" component={TextBox} label="Step of Work No" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Step Type</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="stepType" type="text" component={TextBox} disabled={true} label="Step Type" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>All Customers</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="customers" type="text" component={TextBox} disabled={true} label="All Customers" />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lgOffset={4} lg={8}>
                                <Row>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Description</strong>
                                    </Col>
                                    <Col md={10} lg={10}>
                                        <Field name="description" type="text" component={TextArea} label="Description" />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lgOffset={4} lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Prepared By</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="preparedBy" type="text" component={TextBox} disabled={true} label="Prepared By" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Prepared Date</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="preparedDate" component={DateTimePicker} placeholder="Prepared Date" defaultDate={this.state.preparedDate} handleChange={this.handlePreparedDate}/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={4}>
                                <Row>
                                    <Col md={6} lg={6}>
                                        <Button bsStyle="success">New Planning</Button>
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Checked By</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="checkedBy" type="text" component={TextBox} disabled={true} label="Checked By" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Checked Date</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="checkedDate" component={DateTimePicker} placeholder="Checked Date" defaultDate={this.state.checkedDate} handleChange={this.handleCheckedDate}/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <div>
                                    <table className="table table-bordered table-striped installer">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Customer ID</th>
                                            <th>Customer Name</th>
                                            <th>Pole</th>
                                            <th>Work Type</th>
                                            <th>Requested Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody style={{textAlign: 'center'}}>
                                        { this.state.stepWorkDetails === undefined ?
                                            <tr>
                                                <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                            </tr>
                                            :
                                            this.state.stepWorkDetails.map((work, index) => {
                                                return(
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>{work.customerId}</td>
                                                        <td>{work.customerName}</td>
                                                        <td>{work.poleSerial}</td>
                                                        <td>{work.workType}</td>
                                                        <td>{moment(work.requestedDate).format("YYYY-MM-DD")}</td>
                                                        <td>
                                                            <a onClick={() => this.handleRemove(work.id)}>
                                                                <i className="fa fa-minus-circle fa-lg text-danger"> </i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col mdOffset={8} lgOffset={8} md={2} lg={2}>
                                <Button bsStyle="primary" style={{width: '115%'}} onClick={this.handleBack.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                            </Col>
                            <Col md={2} lg={2}>
                                <ButtonSubmit error={error} submitting={submitting} label="Save" />
                            </Col>
                        </Row>
                    </form>
                </div>
            </div>
        )
    }
}

AddStepWork  = reduxForm({
    form: 'form_add_step_work',
    validate: (values) => {
        const errors = {};
        if (values.description === undefined) {
            errors.description = 'Please input at least 2 characters !!';
        }
        if (values.preparedDate === undefined) {
            errors.preparedDate = 'Please input prepared date !!';
        }
        return errors;
    },
})(AddStepWork);

function mapStateToProps(state) {
    console.log("Step ", state.stepWork.workAdd);
    return {
        work: state.stepWork.work,
        workAdd: state.stepWork.workAdd
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getStepWorkAction, addStepWorkAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddStepWork);