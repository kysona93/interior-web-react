import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { authObj } from '../../../../auth';
import { getStepWorkDetailsAction, checkStepWorkAction, removeStepWorkDetailAction } from '../../../actions/installation/stepwork/stepwork';
import moment from 'moment';

let ids = [];
class EditStepWork extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            preparedDate: moment(this.props.stepWork.preparedDate),
            checkedDate: this.props.stepWork.checkedDate !== null ? moment(this.props.stepWork.checkedDate) : null,
            stepWorkDetails: [],
            workId: 1,
            status: false,
            detailId: 0
        };
        this.handlePreparedDate = this.handlePreparedDate.bind(this);
        this.handleCheckedDate = this.handleCheckedDate.bind(this);
    }

    componentWillMount(){
        this.props.getStepWorkDetailsAction({
            stepId: this.props.stepWork.id,
            year: 0,
            workType: "",
            installer: "",
            status: "",
            pageSize: 10,
            nowPage: 1,
            orderBy: "id"
        });
    }

    componentDidMount(){
        const data = {
            stepNo: this.props.stepWork.stepNo,
            stepType: this.props.stepWork.stepType,
            preparedBy: this.props.stepWork.preparedBy,
            checkedBy: this.props.button === "Check" ? authObj().sub : this.props.button === "Print" ? this.props.stepWork.checkedBy : '',
            customers: this.props.stepWork.customer,
            description: this.props.stepWork.description
        };
        this.props.dispatch(initialize('form_edit_step_work', data));
    }

    componentWillReceiveProps(data){
        if(data.workDetails.items !== undefined){
            this.setState({stepWorkDetails: data.workDetails.items.list})
        } else this.setState({stepWorkDetails: undefined});
        if(data.workCheck.status === 200){
            alert('Successful checked Step Work !!');
            data.workCheck.status = 0;
        } else if(data.workCheck.status === (400 || 404) || data.workCheck.status === (500 || 502)){
            alert(data.workCheck.status + " Error !! \nFail checked Step Work !!\n" + data.workCheck.message);
            data.workCheck.status = 0;
        } else if(data.workCheck.status === 401){
            alert("Please Login !!");
            data.workCheck.status = 0;
        }
        if(data.workDetailRemove.status === 200){
            this.setState({stepWorkDetails: this.state.stepWorkDetails.filter(work => work.id !== this.state.detailId)});
            data.workDetailRemove.status = 0;
        }
    }

    handlePreparedDate(date) {
        this.setState({preparedDate: date});
    }

    handleCheckedDate(date) {
        this.setState({checkedDate: date});
    }

    handleBack(){
        this.props.handleBack(false);
    }

    handleRemove(id){
        this.setState({detailId: id});
        if(confirm("Are you sure you want to remove this work detail?") === true){
            this.props.removeStepWorkDetailAction(id);
        }
    }

    handleSubmit(value){
        if(value.checkedDate === undefined && this.props.stepWork.checkedDate === null){
            alert("Please select checked date !!")
        } else {
            if(this.props.button === "Check"){
                this.props.checkStepWorkAction({
                    id: this.props.stepWork.id,
                    customer: Number(value.customers),
                    description: value.description.trim(),
                    preparedBy: value.preparedBy.trim(),
                    checkedBy: value.checkedBy,
                    checkedDate: value.checkedDate,
                    stepNo: value.stepNo.trim(),
                    stepType: value.stepType.trim(),
                    workDetailIds: ids,
                    status: 1
                });
            } else if(this.props.button === "Edit"){
                alert("Edit")
            } else {
                if(this.props.stepWork.stepType === "New Connection"){
                    window.open(`/print-step-work-new-connection/${this.props.stepWork.id}/${this.props.stepWork.stepNo}`, '_blank');
                } else window.open(`/print-step-work-other-income/${this.props.stepWork.id}/${this.props.stepWork.stepNo}`, '_blank');
            }
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        ids = [];
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title"><span style={{color: '#1c91d0', fontWeight: 'bold', fontSize: '20px'}}>{this.props.button} Step of Work</span></h3>
                </div>
                <div className="panel-body">
                    <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                        <Row>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Step Work No <span className="label-require">*</span></strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="stepNo" type="text" component={TextBox} label="Step of Work No" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Step Type</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="stepType" type="text" component={TextBox} disabled={true} label="Step Type" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>All Customers</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="customers" type="text" component={TextBox} disabled={true} label="All Customers" />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lgOffset={4} lg={8}>
                                <Row>
                                    <Col md={2} lg={2} className="label-name">
                                        <strong>Description</strong>
                                    </Col>
                                    <Col md={10} lg={10}>
                                        <Field name="description" type="text" component={TextArea} label="Description" />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lgOffset={4} lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Prepared By</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="preparedBy" type="text" component={TextBox} disabled={this.props.button === "Check"} label="Prepared By" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Prepared Date</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="preparedDate" component={DateTimePicker} placeholder="Prepared Date" defaultDate={this.state.preparedDate} disabled={true} handleChange={this.handlePreparedDate} />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col lg={4}>
                                <Row>
                                    <Col md={6} lg={6}>
                                        <Button bsStyle="success" onClick={() => location.href='/app/installation/step'}>Add New Step</Button>
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <strong>Checked By</strong>
                                    </Col>
                                    <Col md={8} lg={8}>
                                        <Field name="checkedBy" type="text" component={TextBox} disabled={true} label="Checked By" />
                                    </Col>
                                </Row>
                            </Col>
                            <Col lg={4}>
                                <Row>
                                    <Col md={5} lg={5} className="label-name">
                                        <strong>Checked Date</strong>
                                    </Col>
                                    <Col md={7} lg={7}>
                                        <Field name="checkedDate" component={DateTimePicker} placeholder="Checked Date" defaultDate={this.state.checkedDate} handleChange={this.handleCheckedDate}/>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <div>
                                    <table className="table table-bordered table-striped installer">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Customer ID</th>
                                            <th>Customer Name</th>
                                            <th>Pole</th>
                                            <th>Work Type</th>
                                            <th>Requested Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody style={{textAlign: 'center'}}>
                                        { this.state.stepWorkDetails !== undefined ?
                                            this.state.stepWorkDetails.map((work, index) => {
                                                ids.push(work.id);
                                                return(
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>{work.customerId}</td>
                                                        <td>{work.customerName}</td>
                                                        <td>{work.poleSerial}</td>
                                                        <td>{work.workType}</td>
                                                        <td>{moment(work.requestedDate).format("YYYY-MM-DD")}</td>
                                                        <td>
                                                            { !(this.props.button === "Edit") ?
                                                                null
                                                                :
                                                                <a onClick={() => this.handleRemove(work.id)}>
                                                                    <i className="fa fa-minus-circle fa-lg text-danger"> </i>
                                                                </a>
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            <tr>
                                                <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                            </tr>
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </Col>
                        </Row>
                        <Row>
                            <Col mdOffset={8} lgOffset={8} md={2} lg={2}>
                                <Button bsStyle="primary" style={{width: '115%'}} onClick={this.handleBack.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                            </Col>
                            <Col md={2} lg={2}>
                                <ButtonSubmit error={error} submitting={submitting} label={this.props.button} />
                            </Col>
                        </Row>
                    </form>
                </div>
            </div>
        )
    }
}

EditStepWork  = reduxForm({
    form: 'form_edit_step_work',
    validate: (values) => {
        const errors = {};
        if (values.description === undefined) {
            errors.description = 'Please input at least 2 characters !!';
        }
        return errors;
    },
})(EditStepWork);

function mapStateToProps(state) {
    //console.log("Step ", state.stepWork.workDetailRemove);
    return {
        workDetails: state.stepWork.workDetails,
        workCheck: state.stepWork.workCheck,
        workDetailRemove: state.stepWork.workDetailRemove
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getStepWorkDetailsAction, checkStepWorkAction, removeStepWorkDetailAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(EditStepWork);