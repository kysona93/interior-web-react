import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, FormGroup, Radio, Button } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import SelectBox from '../../../components/forms/SelectBox';
import { getStepWorkDetailsAction } from '../../../actions/installation/stepwork/stepwork';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import '../../../components/forms/Styles.css';
import '../../index.css';
import AddStepWork from './Add';
import moment from 'moment';

function dateFormat(value, row, index) {
    return moment(value).format('YYYY-MM-DD');
}

let stepWork = {
    stepId: 0,
    year: 0,
    workType: "",
    installer: "",
    status: "",
    pageSize: 10,
    nowPage: 1,
    orderBy: "id"
};

let stepWorkDetails = [];

class StepDetail extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            generate: false,
            status: "",
            stepType: "",
            label : 'Show New Customer'
        };
        this.handleChange = this.handleChange.bind(this);
        this.onRowSelect = this.onRowSelect.bind(this);
    }

    handleChange(e) {
        alert(e.target.value);
        this.setState({ status: e.target.value});
        stepWork.workType = this.state.stepType;
        stepWork.status = e.target.value;
        this.props.getStepWorkDetailsAction(stepWork);
    }

    handleSelectStepType(e){
        this.setState({stepType: e.target.value});
        stepWork.workType = e.target.value;
        stepWork.status = this.state.status;
        this.props.getStepWorkDetailsAction(stepWork);
    }

    componentWillMount(){
        this.props.getStepWorkDetailsAction(stepWork);
    }

    componentWillReceiveProps(data){

    }

    onRowSelect(row, isSelected) {
        if(isSelected){
            stepWorkDetails = stepWorkDetails.concat(row);
        } else stepWorkDetails = stepWorkDetails.filter(work => work.id !== row.id);
        console.log(stepWorkDetails.length)
    }

    static onSelectAll(isSelected, rows) {
        if(isSelected){
            stepWorkDetails = rows;
        } else stepWorkDetails = [];

        console.log(stepWorkDetails.length)
    }

    handleGenerate(){
        if(stepWorkDetails.length <= 0 || this.state.stepType === ""){
            alert("Please select Step Type and Step Detail first !")
        } else {
            this.setState({generate: true});
        }
    }

    handleBack(value){
        this.setState({generate: value});
        stepWorkDetails = [];
    }

    render() {
        const {handleSubmit} = this.props;
        return (
            <div className="container-fluid">
                { !this.state.generate ?
                    <div>
                        <Row>
                            <Col xs={12} sm={12} md={12} lg={12}>
                                <form onSubmit={handleSubmit} className="wrap-full-form search-installer">
                                    <Row>
                                        <Col smOffset={8} mdOffset={8} lgOffset={8} sm={4} md={4} lg={4}>
                                            <table className="table table-inverse search-installer" style={{margin: '0px'}}>
                                                <tbody>
                                                    <tr>
                                                        <td><p className="search-installer">Step Type </p></td>
                                                        <td>
                                                            <Field name="stepType" type="select" onChange={this.handleSelectStepType.bind(this)} component={SelectBox} placeholder="Please select ..."
                                                                   values={[{id: 'New Connection', name: 'New Connection'}, {id: 'Other Income', name: 'Other Income'}]} />
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>
                                </form>
                            </Col>
                        </Row>
                        <BootstrapTable
                            data={this.props.workDetails.items !== undefined ? this.props.workDetails.items.list : []} //list of records
                            trClassName={index => index % 2 === 0 ? 'third-tr' : ''}  //apply style on cell
                            headerStyle={{background: "#d5dee5"}}
                            selectRow={{
                                mode: 'checkbox',
                                clickToSelect: true,
                                selected: [],
                                bgColor: 'rgb(238, 193, 213)',
                                onSelect: this.onRowSelect,
                                onSelectAll: StepDetail.onSelectAll
                            }} //apply function to select record
                            cellEdit={{
                                mode: 'click',
                                blurToSave: true,
                                afterSaveCell: this.onAfterSaveCell
                            }} // apply function to edit cell
                            options={{
                                sortName: 'id',
                                sortOrder: 'asc'
                            }}
                            hover
                        >
                            <TableHeaderColumn dataField='id' width="7%" dataAlign='center' dataSort isKey autoValue>ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='customerName' width="10%" className='good' dataSort >Customer Name</TableHeaderColumn>
                            <TableHeaderColumn dataField='poleSerial' width="10%" dataAlign='center'>Pole</TableHeaderColumn>
                            <TableHeaderColumn dataField='requestedDate' width="10%" dataAlign='center' dataFormat={ dateFormat }>Date Request</TableHeaderColumn>
                            <TableHeaderColumn dataField='workType' width="10%" dataAlign='center'>Step Type</TableHeaderColumn>
                            <TableHeaderColumn dataField='installerName' width="10%" dataAlign='center'>Installer</TableHeaderColumn>
                            <TableHeaderColumn dataField='completedDate' width="10%" dataAlign='center'>Date Complete</TableHeaderColumn>
                            <TableHeaderColumn dataField='status' width="10%" dataAlign='center' dataFormat={value => value == 0 ? "New" : value == 1 ? "Processing" : value == 2 ? "Completed" : "Canceled"}>Status</TableHeaderColumn>
                            <TableHeaderColumn dataField='remark' width="10%" dataAlign='center' editable={{type: 'textarea'}}>Remark</TableHeaderColumn>
                        </BootstrapTable>
                        <Button bsStyle="primary" style={{float: 'right'}} onClick={this.handleGenerate.bind(this)}>Generate</Button>
                    </div>
                    :
                    <AddStepWork handleBack={this.handleBack.bind(this)} stepType={this.state.stepType} stepWorkDetails={stepWorkDetails} />
                }
            </div>
        )
    }

}


StepDetail = reduxForm({
    form: 'form_step_detail'
})(StepDetail);

function mapStateToProps(state) {
    //console.log("Work", state.stepWork.workDetails);
    return {
        workDetails: state.stepWork.workDetails,
        initialValue :{
            name: '',
            installDate: 0,
            status: -1
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getStepWorkDetailsAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(StepDetail);