import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import './index.css';
import { printStepWorkAction } from '../../../actions/installation/stepwork/stepwork';
import moment from 'moment';

class OtherIncome extends React.Component{
    componentWillMount(){
        this.props.printStepWorkAction(this.props.params.id);
    }

    componentWillReceiveProps(data){
        if(data.workPrint.items !== undefined){
            setTimeout(() => {
                window.print();
            }, 100)
        }
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="row">

                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-align-center"><img className="bvc-logo" src="/icons/ibp.png"/> </div>
                    <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-align-center"><h2 className="kingdom-cam">ជាតិ​ សាសនា ព្រះមហាក្សត្រ</h2></div>
                    <div className="col-xs-2 col-sm-2 col-md-2 col-lg-2 text-align-center"> </div>
                    <div className="col-xs-10 col-sm-10 col-md-10 col-lg-10 text-align-center"><u><h3 className="new-install">ឈ្មោះអតិថិជនមានបញ្ហា</h3></u></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <span><p>Project No   : Inter BP</p></span>
                        <span><p>Step NO         : {this.props.params.no}</p></span>
                        <span><p>Date         : ........../......../...............</p></span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-md-12 col-lg-12">
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="other-income-to-xls"
                            filename="installers"
                            sheet="tablexls"
                            buttonText="Export to Excel"/>

                        <table id="other-income-to-xls" className="table report table-bordered">
                            <thead style={{display:'none'}}>
                                <tr>
                                    <th colSpan={14} style={{textAlign: 'left'}}>Project No   : Inter BP</th>
                                </tr>
                                <tr>
                                    <th colSpan={14} style={{textAlign: 'left'}}>Step         : ST-2017-0051</th>
                                </tr>
                                <tr>
                                    <th colSpan={14} style={{textAlign: 'left'}}>Date         : ........../......../...............</th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th>ល.រ</th>
                                    <th>អត្តលេខ</th>
                                    <th>ឈ្មោះ</th>
                                    <th>លេខទូរស័ព្ទ</th>
                                    <th>ភូមិ</th>
                                    <th>ថ្ងៃស្នើសុំ</th>
                                    <th>អំពែរ</th>
                                    <th>បង្គោល</th>
                                    <th>ប្រភេទការងារ</th>
                                    <th>ថ្ងៃជួសជុល</th>
                                    <th>បញ្ហារទី 1</th>
                                    <th>បញ្ហារទី 2</th>
                                    <th>បញ្ហារទី 3</th>
                                    <th>ផ្សេងៗ</th>
                                </tr>
                            </thead>
                            <tbody>
                                { this.props.workPrint.items === undefined ? null :
                                    this.props.workPrint.items.map((work, index) => {
                                        return(
                                            <tr key={index}>
                                                <td>{index + 1}</td>
                                                <td>{work.customerId}</td>
                                                <td>{work.customerName}</td>
                                                <td>{work.phone}</td>
                                                <td>{work.locationKh || work.locationEn}</td>
                                                <td>{moment(work.registeredDate).format("DD-MM-YYYY")}</td>
                                                <td>{work.ampere}</td>
                                                <td>{work.poleSerial}</td>
                                                <td>{JSON.parse(work.stepDetails) !== undefined ? JSON.parse(work.stepDetails).map((s) => s.type.concat(',')) : ""}</td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                                <td> </td>
                                            </tr>
                                        )
                                    })
                                }
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tbody className="body-no-border">
                                <tr>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td colSpan={4}>
                                        <span><p className="request-by">ស្នើរសុំដោយ:</p></span>
                                        <br/>
                                        <br/>
                                        <span><p>ឈ្មោះ : ................................</p></span><br /><br />
                                        <span><p>ថ្ងៃខែឆ្នាំ : ........../........../..........</p></span>
                                    </td>
                                    <td> </td>
                                    <td colSpan={4}>
                                        <span><p className="approve-bye">ត្រួតពិនិត្យដោយ:</p></span>
                                        <br/>
                                        <br/>
                                        <span><p>ឈ្មោះ : ................................</p></span><br /><br />
                                        <span><p>ថ្ងៃខែឆ្នាំ : ........../........../..........</p></span>
                                    </td>
                                    <td> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    //console.log(state.stepWork.workDetails);
    return {
        workPrint: state.stepWork.workPrint
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({printStepWorkAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(OtherIncome);