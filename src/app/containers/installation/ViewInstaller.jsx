import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, FormGroup, Radio } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import './../../components/forms/Styles.css';
import { DateTimePicker } from '../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../components/forms/ButtonSubmit';
import './../index.css';
import ViewInstalledCustomer from './ViewInstalledCustomer';
import { getInstallerReportsAction, getInstallingSchedulesAction } from '../../actions/employee/employee';
import { uniqueArrayObj } from '../../utils/ArrayFunction';

import StepWork from './step/StepWork';

const getNumberCustomerInstallation = (installers, id, status) => {
    const num = installers.find(installer => installer.id === id && installer.installingId === status);
    return num !== undefined ? num.customers : 0
};

let installer = {
    status: 0,
    installDate: 0
};

let schedule = {
    installer : '',
    transformer : '' ,
    installFromDate : '',
    installToDate : '',
    status: 0
};

class ViewInstaller extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startDate: null,
            status: 0,
            show: false,
            schedules: []
        };
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.handleChangeStatus = this.handleChangeStatus.bind(this);
    }

    componentWillMount(){
        this.props.getInstallerReportsAction(installer);
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }
    
    handleChangeStatus(event){
        this.setState({status: Number(event.target.value)});
        installer.status = Number(event.target.value);
        this.props.getInstallerReportsAction(installer);
        if(this.state.show){
            schedule.status = Number(event.target.value);
            this.props.getInstallingSchedulesAction(schedule);
        }
    }
    
    handleSelectInstaller(value){
        schedule.installer = value;
        this.props.getInstallingSchedulesAction(schedule);
        this.setState({show: true});
    }

    onCloseInstaller(value){
        this.setState({ show: value })
    }

    handleSubmit(value){
        installer.status = Number(this.state.status);
        installer.installDate = value.date !== undefined ? new Date(value.date).getTime() : 0;
        this.props.getInstallerReportsAction(installer);
    }

    render(){
        const {handleSubmit, invalid, submitting} = this.props;
        const installers = this.props.installerReports.items;
        return (
            <div className="container-fluid">
                <Row>
                    <Col lg={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                            <Row>
                                <Col mdOffset={7} lgOffset={7} md={5} lg={5}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">Search BY</p></td>
                                            <td><Field name="date" component={DateTimePicker} placeholder="Installing date" defaultDate={this.state.startDate}
                                                       handleChange={this.handleRegisterDate} /></td>
                                            <td><ButtonSubmit className="search-installer" invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        <tr>
                                            <td colSpan ={3}>
                                                <FormGroup className="search-installer" >
                                                    <Radio name="filter" value={0} onChange = {this.handleChangeStatus} checked={this.state.status === 0} inline >All</Radio>{' '}
                                                    <Radio name="filter" value={1} onChange = {this.handleChangeStatus} checked={this.state.status === 1} inline>Installing</Radio>{' '}
                                                    <Radio name="filter" value={2} onChange = {this.handleChangeStatus} checked={this.state.status === 2} inline>Installed</Radio>
                                                </FormGroup>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                { this.state.show ?
                    <ViewInstalledCustomer onCloseInstaller={ this.onCloseInstaller.bind(this) } customers={this.props.installingSchedules.items} />
                    :
                    <Row>
                        <Col lg={12}>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="installers-to-xls"
                                filename="installers"
                                sheet="tablexls"
                                buttonText="Export to Excel"/>
                            <div>
                                <table id="installers-to-xls" className="table table-bordered table-striped installing">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Khmer Name</th>
                                        <th>Latin Name</th>
                                        <th>Phone</th>
                                        <th>Installing Customer</th>
                                        <th>Installed Customer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { installers !== undefined ?
                                        uniqueArrayObj(installers).map((installer, index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td style={{textAlign: 'center'}}>{installer.id}</td>
                                                    <td>{installer.nameKh}</td>
                                                    <td><a onClick = {()=> this.handleSelectInstaller(installer.nameEn)}>{installer.nameEn}</a> </td>
                                                    <td>{installer.phone}</td>
                                                    <td style={{textAlign: 'center'}}>{getNumberCustomerInstallation(installers, installer.id, 1)}</td>
                                                    <td style={{textAlign: 'center'}}>{getNumberCustomerInstallation(installers, installer.id, 2)}</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                }
            </div>
        )
    }
}

ViewInstaller = reduxForm({
    form: 'form_view_installers'
})(ViewInstaller);

function mapStateToProps(state) {
    return {
        installerReports: state.employee.installerReports,
        installingSchedules: state.employee.installingSchedules
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getInstallerReportsAction, getInstallingSchedulesAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ViewInstaller);