import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, FormGroup, Radio } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../components/forms/TextBox';
import { getAllInstallersAction } from './../../actions/employee/employee';
import './../../components/forms/Styles.css';
import { ButtonSubmit } from '../../components/forms/ButtonSubmit';
import FormGenerate from './GenerateForm';
import './../index.css';
import moment from 'moment';
import ReactLoading from 'react-loading';

let installer = {
    name: '',
    installDate: 0,
    status: 0
};

class ViewInstallationCustomer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            startDate: null,
            status: 0,
            values : '',
            show : false,
            hide : true,
            label : 'Show New Customer'
        };

        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount(){
        this.props.getAllInstallersAction(installer);
    }

    componentWillReceiveProps(data){
        if(data.installers.items !== undefined || data.installers.items === undefined){
            installer.name = '';
            installer.status = 0;
            document.getElementById('loading').style.display = 'none';
        }
    }

    handleRegisterDate(date) {this.setState({startDate: date});}

    handleSubmit(value){
        installer.name = value.name.trim();
        installer.status = this.state.status;
        this.props.getAllInstallersAction(installer);
        document.getElementById('loading').style.display = 'block';
    }

    handleChange(event) {
        this.setState({ status: Number(event.target.value)});
        installer.status = Number(event.target.value);
        this.props.getAllInstallersAction(installer);
        document.getElementById('loading').style.display = 'block';
    }

    handleUpdate(update){
        this.setState({  values: update.target.value })
    }

    render(){
        const {handleSubmit, invalid, submitting} = this.props;
        return (
            <div className="container-fluid margin_left_25">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                            <Row>
                                <Col mdOffset={6} lgOffset={6} md={6} lg={6}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">Search Installer Name</p></td>
                                            <td><Field name="name" onChange={this.handleUpdate.bind(this)} type="text" component={TextBox} disabled={this.state.status === -1 ? true : false} label="Installer's name"/></td>
                                            <td><ButtonSubmit className="search-installer" invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                        </tr>
                                        <tr>
                                            <td colSpan ={3}>
                                                <FormGroup className="search-installer"  >
                                                    <Radio name="filter" onChange ={this.handleChange} value={0} checked={this.state.status === 0} inline>All</Radio>{' '}
                                                    <Radio name="filter" onChange ={this.handleChange} value={1} checked={this.state.status === 1} inline>Installing</Radio>{' '}
                                                    <Radio name="filter" onChange ={this.handleChange} value={2} checked={this.state.status === 2} inline>Installed</Radio>{' '}
                                                    <Radio name="filter" onChange ={this.handleChange} value={3} checked={this.state.status === 3} inline>Re-Isntalling</Radio>{' '}
                                                    <Radio name="filter" onChange ={this.handleChange} value={-1} checked={this.state.status === -1} inline>New Customer(s)</Radio>
                                                </FormGroup>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <Row>
                    { this.state.status === -1 ? null :
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <div>
                                <table className="table table-bordered table-striped installer">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Customer Name​(Khmer)</th>
                                        <th>Latin Name</th>
                                        <th>Installed By</th>
                                        <th>Register Date</th>
                                        <th>Install Date</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    { this.props.installers.items !== undefined ?
                                        this.props.installers.items.map((installer,index) => {
                                            return(
                                                <tr key={index}>
                                                    <td className="text-align-center">{installer.customerId}</td>
                                                    <td className="text-align-center">{installer.customerNameKh}</td>
                                                    <td className="text-align-center">{installer.customerNameEn}</td>
                                                    <td className="text-align-center">{installer.installerNameKh != undefined ? installer.installerNameKh : null}</td>
                                                    <td className="text-align-center">{moment(installer.registerDate).format("DD-MM-YYYY")}</td>
                                                    {installer.installingStatus > 0 ? <td>{moment(installer.installDate).format("DD-MM-YYYY")}</td> :<td> </td> }
                                                    {
                                                        installer.installingStatus === 1 ?  <td className="installing text-align-center">Installing</td>:
                                                        installer.installingStatus === 2 ?  <td className="Installed text-align-center">Installed</td>:
                                                        installer.installingStatus === 3 ?  <td className="Re-install text-align-center">Re-install</td> :<td className="newCustomer text-align-center">New customer</td>
                                                    }
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    }
                </Row>
                {this.state.status === -1  ? <FormGenerate installers={this.props.installers.items !== undefined ? this.props.installers.items : null }/> : null}
                {/*  {this.state.status === -1  ?   <button className="form-requesting" onClick = {this.handelGenerate.bind(this)}>Generate Step Installing</button> : null}*/}
            </div>
        )
    }
}

ViewInstallationCustomer = reduxForm({
    form: 'form_installation_customers'
})(ViewInstallationCustomer);

function mapStateToProps(state) {
    return {
        installers: state.employee.installerAll,
        initialValues: {
            name: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllInstallersAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ViewInstallationCustomer);