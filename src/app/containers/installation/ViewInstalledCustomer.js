import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import moment from 'moment';
import './installation.css';
class ViewInstalledCustomer extends React.Component{
    constructor(props){
        super(props);
    }

    onCloseInstaller(){ this.props.onCloseInstaller(false); }

    render(){
        return(
            <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <div>
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button install"
                            className="download-table-xls-button customer-installed"
                            table="schedule-to-xls"
                            filename="installation-schedule"
                            sheet="tablexls"
                            buttonText="Export to Excel"/>
                        <Col md={1} lg={1}>
                            <Button bsStyle="primary" onClick = { this.onCloseInstaller.bind(this) }
                                    style={{width: '100%', background:'#2c699f', border:'0',borderRadius:'0',marginLeft: '-12px',padding: '7px'}}>
                                    <i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                        </Col>
                        <table id="schedule-to-xls" className="table table-bordered table-striped installing">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer Name</th>
                                <th>Phone</th>
                                <th>Ampere</th>
                                <th>Area</th>
                                <th>Transformer</th>
                                <th>Pole</th>
                                <th>Box</th>
                                <th>Room</th>
                                <th>Meter Serial</th>
                                <th>InstallDate</th>
                                <th>RegisterDate</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.customers !== undefined ?
                                this.props.customers.map((emp, index)=>{
                                    return(
                                        <tr key={index}>
                                            <td>{emp.id}</td>
                                            <td>{emp.nameKh}</td>
                                            <td>{emp.phone}</td>
                                            <td>{emp.ampereValue} A</td>
                                            <td>{emp.areaNameEn}</td>
                                            <td>{emp.transSerial}</td>
                                            <td>{emp.poleSerial}</td>
                                            <td>{emp.boxSerial}</td>
                                            <td>{emp.room}</td>
                                            <td>{JSON.parse(emp.meters).map((meter) => {
                                                return meter.meterserial + ' '
                                            })}</td>
                                            <td>{moment(emp.installDate).format("DD-MM-YYYY")}</td>
                                            <td>{moment(emp.registerDate).format("DD-MM-YYYY")}</td>
                                            {
                                                emp.installingStatus === 1 ?  <td className="installing text-align-center">Installing</td>:
                                                emp.installingStatus === 2 ?  <td className="Installed text-align-center">Installed</td>:
                                                emp.installingStatus === 3 ?  <td className="Re-install text-align-center">Re-install</td> :
                                                <td className="newCustomer">New customer</td>
                                            }

                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={13}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>
        )
    }
}

export default ViewInstalledCustomer;