import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { getAllInstallersAction } from './../../actions/installation/installer';
import './../../components/forms/Styles.css';
import './../index.css';
import { DateTimePicker } from '../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../components/forms/ButtonSubmit';
import SelectBox from '../../components/forms/SelectBox';
import {getEmployeesAction, getInstallingSchedulesAction} from '../../actions/employee/employee';
import { getAllTransformersAction } from '../../actions/setup/transformer';
import { getAreaReportsAction } from '../../actions/setup/area';
import moment from 'moment';
import ReactLoading from 'react-loading';

const getNumberCustomerInstallation = (installations, area, status) => {
    const num = installations.find(install => install.id === area && install.installingId == status);
    return num !== undefined ? num.customers : 0
};

const removeDuplicate = (list) => {
    return list.filter((thing, index, self) => self.findIndex((t) => {return t.id === thing.id }) === index);
};

let schedule = {
    installer : '',
    transformer : '' ,
    installFromDate : '',
    installToDate : '',
    status: 0
};

class InstallingSchedule extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            fromDate: null,
            employees: [],
            employeeName : '',
            transformers: [],
            toDate: null
        };
        this.handleFromDate = this.handleFromDate.bind(this);
        this.handleToDate = this.handleToDate.bind(this);
        Array.prototype.sum = function (prop) {
            let total = 0;
            for ( let i = 0, _len = this.length; i < _len; i++ ) {
                total += this[i][prop]
            }
            return total;
        }
    }

    componentWillMount(){
        this.props.getAreaReportsAction();
        this.props.getEmployeesAction({
            position: '',
            name: '',
            pageSize: 10,
            nowPage: 1,
            orderBy: "id"
        });
        this.props.getAllTransformersAction("");
        this.props.getInstallingSchedulesAction(schedule);
    }
    componentWillReceiveProps(data){
        let employees = [];
        let transformers = [];
        if(data.employees.items !== undefined){
            data.employees.items.list.map((emp) => {
                employees.push({
                    id: emp.id,
                    name: emp.nameKh
                })
            });
        }
        if (data.transformerAll.items !== undefined) {
            data.transformerAll.items.map((trans) => {
                transformers.push({
                    id: trans.serial,
                    name: trans.serial
                })
            });
        }
        this.setState({ transformers: transformers, employees: employees});
    }

    handleFromDate(date) {this.setState({fromDate: date});}

    handleToDate(date) {this.setState({toDate: date});}

    handleChange(){
        const name = document.getElementsByName('installerName')[0].options[document.getElementsByName('installerName')[0].selectedIndex].text;
        if(name !== 'All'){
            this.setState({employeeName: name})
        }
    };

    handleSubmit(value){
        schedule = {
            installer : this.state.employeeName || '',
            transformer : value.transformer || '' ,
            installFromDate : value.FROMDATE !== undefined ?  value.FROMDATE : '',
            installToDate : value.TODATE !== undefined ? value.TODATE : '',
            status: 0
        };
        alert(JSON.stringify(schedule));
        this.props.getInstallingSchedulesAction(schedule);
    }

    render(){
        const {handleSubmit, submitting} = this.props;
        const schedules = this.props.installingSchedules.items;
        const areas = this.props.areaReports.items;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form search-installer">
                            <Row>
                                <Col md={6} lg={6} className="pull-right">
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <p className="search-installer">Installer Name</p>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="installerName" type="select"  component={SelectBox} onChange={this.handleChange.bind(this)} placeholder="All" values={this.state.employees} sortBy="name" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <p className="search-installer">InstallDate</p>
                                            </Col>
                                            <Col md={4} lg={4}>
                                                <Field name="FROMDATE" component={DateTimePicker} placeholder="From Date" defaultDate={this.state.fromDate} handleChange={this.handleFromDate}/>
                                            </Col>
                                            <Col md={4} lg={4}>
                                                <Field name="TODATE" component={DateTimePicker} placeholder="To Date" defaultDate={this.state.toDate} handleChange={this.handleToDate}/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4}>
                                                <p className="search-installer">Transformer</p>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field name="transformer" type="select"  component={SelectBox} placeholder="All" values={this.state.transformers} sortBy="name" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={4} lg={4} className=" pull-right">
                                                <ButtonSubmit className="search-installer" submitting={submitting} label="SEARCH" />
                                            </Col>
                                        </Row>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <div id="loading" style={{display: 'none', marginTop: 20}}>
                    <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                </div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div>
                            <table id="schedule-to-xls" className="table table-bordered table-striped installing">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Area</th>
                                        <th>Transformer</th>
                                        <th colSpan={3}>Number of Customers</th>
                                        <th>Connected</th>
                                        <th>Installing</th>
                                        <th>Using</th>
                                        <th colSpan={3}>New Registration</th>
                                    </tr>
                                </thead>
                                <tbody style={{textAlign: 'center'}}>
                                { areas !== undefined ?
                                    removeDuplicate(areas).map((area, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{area.id}</td>
                                                <td>{area.areaNameEn}</td>
                                                <td>{area.transSerial}</td>
                                                <td colSpan={3}>{this.props.areaReports.items.sum("customers")}</td>
                                                <td>{getNumberCustomerInstallation(this.props.areaReports.items, area.id, 2)}</td>
                                                <td>{getNumberCustomerInstallation(this.props.areaReports.items, area.id, 1)}</td>
                                                <td>{getNumberCustomerInstallation(this.props.areaReports.items, area.id, 3)}</td>
                                                <td colSpan={3}>{getNumberCustomerInstallation(this.props.areaReports.items, area.id, null)}</td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={12}><h2 style={{textAlign: 'center'}}>Result Not Found</h2></td>
                                    </tr>
                                }
                                    <tr><td colSpan={12}> </td></tr>
                                </tbody>

                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Customer Name</th>
                                        <th>Phone</th>
                                        <th>Ampere</th>
                                        <th>Area</th>
                                        <th>Transformer</th>
                                        <th>Pole</th>
                                        <th>Box</th>
                                        <th>Room</th>
                                        <th>Meter Serial</th>
                                        <th>Installed Date</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody style={{textAlign: 'center'}}>
                                { schedules !== undefined ?
                                    schedules.map((emp, index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{emp.id}</td>
                                                <td>{emp.nameKh}</td>
                                                <td>{emp.phone}</td>
                                                <td>{emp.ampereValue} A</td>
                                                <td>{emp.areaNameEn}</td>
                                                <td>{emp.transSerial}</td>
                                                <td>{emp.poleSerial}</td>
                                                <td>{emp.boxSerial}</td>
                                                <td>{emp.room}</td>
                                                <td>{JSON.parse(emp.meters).map((meter) => {
                                                    return meter.meterserial + ' '
                                                })}</td>
                                                <td>{moment(emp.installDate).format("DD-MM-YYYY")}</td>
                                                <td className="installing text-align-center">
                                                    { emp.status === 1 ? 'New' :
                                                        emp.status === 2 ? 'Installing' :
                                                            emp.status === 3 ? 'Installed' :
                                                                emp.status === 4 ? 'Using' : 'None'
                                                    }
                                                </td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={12}>
                                            <i className="fa fa-spinner fa-pulse fa-5x fa-fw"> </i>
                                            <span className="sr-only">Loading...</span>
                                            <br />
                                        </td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="schedule-to-xls"
                                filename="installation-schedule"
                                sheet="installation-schedule"
                                buttonText="Export to Excel"/>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

InstallingSchedule = reduxForm({
    form: 'form_list_billing_item',
    validate: (values) => {
        const errors = {};
        if(values.FROMDATE !== undefined && values.TODATE !== undefined){
            if(new Date(values.FROMDATE).getTime() >= new Date(values.TODATE).getTime()){
                errors.TODATE = "TODATE must greater than FROMDATE !!"
            }
        }

        return errors
    }
})(InstallingSchedule);

function mapStateToProps(state) {
    //console.log("DATA : " + JSON.stringify(state.area.areaReports));
    return {
        areaReports: state.area.areaReports,
        employees: state.employee.employees,
        transformerAll: state.transformer.transformerAll,
        installingSchedules: state.employee.installingSchedules,
        initialValues: {
            installer: '',
            transformer: ''
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAreaReportsAction, getEmployeesAction, getAllTransformersAction, getInstallingSchedulesAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InstallingSchedule);