import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { Row, Col, FormGroup, Radio } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getAllInstallersAction } from './../../actions/installation/installer';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { addGenerateFormRequestAction, getAllGenerateFormRequestAction } from './../../actions/customer/network/installation';

function onRowSelect(row, isSelected) {
    //console.log(row);
    // console.log(`selected: ${isSelected}`);
}

function onSelectAll(isSelected) {
    //console.log(`is select all: ${isSelected}`);
}

function onAfterTableComplete() {
    //console.log('Table render complete.');
}
function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
function nameValidator(value) {
    if (!value) {
        return 'Job Name is required!';
    } else if (value.length < 2) {
        return 'Job Name length must great 3 char';
    }
    return true;
}

 class FormGenerate extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
    }

     componentWillReceiveProps(data){

     }

    render() {
        return (
            <div className="container-fluid">
                <BootstrapTable
                    data={this.props.installers} //list of records
                    trClassName={trClassNameFormat}  //apply style on cell
                    headerStyle={{background: "#d5dee5"}}
                    selectRow={{
                        mode: 'checkbox',
                        clickToSelect: true,
                        selected: [],
                        bgColor: 'rgb(238, 193, 213)',
                        onSelect: onRowSelect,
                        onSelectAll: onSelectAll
                    }} //apply function to select record
                    cellEdit={{
                        mode: 'click',
                        blurToSave: true,
                        afterSaveCell: this.onAfterSaveCell
                    }} // apply function to edit cell
                    options={{
                        paginationShowsTotal: true,
                        sortName: 'id',  // default sort column name
                        sortOrder: 'desc',  // default sort order
                        afterTableComplete: onAfterTableComplete, // A hook for after table render complete.
                        onDeleteRow: this.onDeleteRow
                    }} //
                    deleteRow // apply button delete on the top right
                    //search // apply text-box search on the top right
                    hover // apply hover style on row
                    pagination // apply pagination
                >
                    <TableHeaderColumn dataField='id' width="7%" dataAlign='center' dataSort isKey autoValue>ID</TableHeaderColumn>
                    <TableHeaderColumn dataField='typeName' width="10%" className='good' dataSort >TYPE</TableHeaderColumn>
                    <TableHeaderColumn dataField='nameKh' width="14%" className='good' dataSort editable={ { type: 'textarea', validator: nameValidator } }>ឈ្មោះ</TableHeaderColumn>
                    <TableHeaderColumn dataField='name' width="14%" className='good' dataSort editable={ { type: 'textarea', validator: nameValidator } }>NAME</TableHeaderColumn>
                    <TableHeaderColumn dataField='country' width="10%" dataAlign='center'>COUNTRY</TableHeaderColumn>
                    <TableHeaderColumn dataField='province' width="10%" dataAlign='center'>PROVINCE</TableHeaderColumn>
                    <TableHeaderColumn dataField='district' width="10%" dataAlign='center'>DISTRICT</TableHeaderColumn>
                    <TableHeaderColumn dataField='commune' width="10%" dataAlign='center'>COMMUNE</TableHeaderColumn>
                    <TableHeaderColumn dataField='village' width="10%" dataAlign='center'>VILLAGE</TableHeaderColumn>
                </BootstrapTable>
            </div>
        )
    }

}
function mapStateToProps(state) {
    console.log("installer", state.installer.getAllInstallers);
    return {
        getAllInstallers: state.installer.getAllInstallers,
        initialValue :{
            name: '',
            installDate: 0,
            status: -1
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllInstallersAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(FormGenerate);