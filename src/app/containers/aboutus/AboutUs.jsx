import React from 'react';
import {Row, Col, Image} from 'react-bootstrap';
import HeaderNavigation from '../../components/shares/HeaderNavigation';
import MenuPicture from '../../components/shares/MenuPicture';
import FooterWeb from '../../components/shares/FooterWeb';
import MenuWeb from '../../components/shares/MenuWeb';
import './aboutus.css';

class AboutUs extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <HeaderNavigation />
                {/*<MenuWeb />*/}
                <MenuPicture name="images/menu-mage-aboutus.jpg"/>
                <div className="container">
                    <h1 className="cmp-align-center abt-title">
                        The Apsara Home & Furniture Story
                    </h1>
                    <p className="cmp-align-center abt-desc">
                        Apsara Home & Furniture is a local firm of architects, designers, strategists, and specialists.
                        We focus exclusively on environments through the lens of interior architecture—a radical idea in 2013, when APSARA was founded.
                        We are highly connected agents of change, committed to creativity, innovation, growth, & our communities.
                        We’re inspired working alongside our clients to resolve complex issues and design highly energized environments where people thrive.
                        Through our work we strive to share our passion with our clients.
                    </p>
                    <div style={{marginTop:'60px'}}>
                        <h2 className="abt-title">Our Vision</h2>
                        <Row>
                            <Col sm={4} md={4}>
                                <p className="abt-desc" style={{marginTop:'0px'}}>
                                    We are proud to see our clients satisfy with our services and products.
                                    We stay focus on interior design solutions and strategies.
                                    Our team commit to work more harder and put all our effort to give our clients with the best services and products.
                                    We continue to focus more in interior field to find the excellent things for the clients in the future.
                                </p>
                            </Col>
                            <Col sm={8} md={8}>
                                <Image src="images/map.jpg" responsive style={{width:'100%', height:'500px'}}/>
                            </Col>
                        </Row>
                    </div>

                    <div>
                        <h2 className="abt-title">Our Mission</h2>
                        <Row>
                            <Col sm={12} md={12}>
                                <p className="abt-desc" style={{marginTop:'0px'}}>
                                    Our team commit to work more harder and put all our effort to give our clients with the best services and products.
                                    We continue to focus more in interior field to find the excellent things for the clients in the future.
                                </p>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/map.jpg" responsive style={{width:'100%', height:'500px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/map.jpg" responsive style={{width:'100%', height:'500px'}}/>
                            </Col>
                        </Row>
                    </div>
                    <br/><br/>
                </div>
                <FooterWeb />
            </div>
        )
    }
}

export default AboutUs;