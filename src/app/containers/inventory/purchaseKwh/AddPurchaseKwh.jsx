import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox, Tabs, Tab } from 'react-bootstrap';
import { Field, reduxForm, reset, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import '../../../components/forms/Styles.css';
import { getSuppliersAction } from './../../../actions/inventory/supplier';
import { getAllRatePerKwhAction } from './../../../actions/setup/rateKwh';
import { getConnectionBuysAction, addPurchaseKwhAction } from './../../../actions/inventory/purchaseKwh';
import { authObj } from './../../../../auth';

let suppliers = [],
    rates = [],
    filter = {
        limit: 100,
        page: 1,
        keyWords: ''
};

class AddPurchaseKwh extends React.Component {
    constructor(props){
        super(props);
        this.state={
            recordDate: null,
            connectionBuyId: 0,
            suppliers: [],
            rates: [],
            isSearching: false
        };
    }

    componentWillMount(){
        this.props.getSuppliersAction({type: 'ELECTRICITY'});
        this.props.getAllRatePerKwhAction();
    }

    componentWillUnmount() {
        this.setState({ isSearching: false });
        this.props.connectionBuys.status = 0;
    }

    componentWillReceiveProps(newProps){
        //supplier
        if(newProps.getSuppliers.status === 200){
            let items = newProps.getSuppliers.data.items;
            if(items.length === suppliers.length){
                this.setState({suppliers : suppliers});
            }else {
                items.forEach((element) => {
                    suppliers.push({
                        "id": element.id,
                        "name": element.name
                    });
                });
                this.setState({suppliers : suppliers});
            }
        }

        // hide show form add purchase kwh
        if (newProps.connectionBuys.status === 200) {
            this.setState({ isSearching: true });
        }

        // rate
        if(newProps.ratePerKwhAll.items !== undefined){
            let items = newProps.ratePerKwhAll.items;
            if (items.length === rates.length){
                this.setState({rates : rates});
            } else {
                items.forEach((element) => {
                    rates.push({
                        id: element.id,
                        name: element.rate
                    });
                });
                this.setState({rates : rates});
            }
        } 

        if(newProps.purchaseKwhAdd.status === 200){
            if(newProps.purchaseKwhAdd.data.status === 200){
                alert("Successfully added new purchase kwh.");
                newProps.purchaseKwhAdd.data.status = 0;
            } else {
                alert("Fail with add purchase kwh!");
                newProps.purchaseKwhAdd.data.status = 0;
            }
            newProps.purchaseKwhAdd.status = 0;
        }
        if(newProps.purchaseKwhAdd.status === 404 || newProps.purchaseKwhAdd.status === 500){
            alert("Fail with add purchase kwh!");
            newProps.purchaseKwhAdd.status = 0;
        }
    }

    handleRecordDate(date) {
        this.setState({
            recordDate: date
        });
    }

    handleSubmit(values){
        let data = {
            "coefficient": values.coefficient,
            "connectionBuyId": this.state.connectionBuyId,
            "invoiceNo": values.invoiceNo,
            "kwhPurchase": values.purchaseKwh,
            "lastUsageRecord": values.lastUsage,
            "meterSerial": values.meterSerial,
            "newUsageRecord": values.newUsage,
            "ratePerKwhId": values.rateId,
            "recordDate": values.recordDate,
            "status": 1
        };
        this.props.addPurchaseKwhAction(data);
    }

    // handle search
    handleChangeSearch(event) {
        if (event.target.value !== '') {
            if (this.props.getSuppliers.status === 200){
                const item = this.props.getSuppliers.data.items.find(i => i.id === Number(event.target.value));
                filter.keyWords = item.name;
            }
        } else filter.keyWords = '';
    }

    handleSubmitSearch() {
        if (filter.keyWords === '') alert('Please Select Supplier!');
        else this.props.getConnectionBuysAction(filter);
    }

    handleAddPurchaseKwh(object) {
        this.setState({ connectionBuyId: object.id });
        this.props.dispatch(change('form_add_purchase_kwh', 'meterSerial', object.meterSerial));
    }


    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <fieldset>
                            <legend>Search By</legend>
                            <Row>
                                <Col md={8} lg={8} >
                                    <Row>
                                        <Col md={3} lg={3} className="label-name">
                                            <strong>Supplier Name<span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={7} lg={7}>
                                            <Field 
                                                name="supplierId" 
                                                type="select" 
                                                component={SelectBox} 
                                                placeholder="Select Type" 
                                                values={this.state.suppliers}
                                                onChange={this.handleChangeSearch.bind(this)}
                                                icon="" />
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <button className="btn btn-default" onClick={() => this.handleSubmitSearch()} > 
                                                Search
                                            </button>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </fieldset>
                    </Col>
                </Row>
                { this.state.isSearching === true ?
                    <Row>
                        <Col md={2}>
                            <table className="list-smaller-table">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Meter Serial</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {
                                    this.props.connectionBuys.status === 200 ?
                                        this.props.connectionBuys.data.items.list.map((i, index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>
                                                        <a onClick={() => this.handleAddPurchaseKwh(i)}>
                                                            {i.meterSerial}
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    :
                                    <tr>
                                        <td colSpan={14} className="text-align-center">No Record</td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                        </Col>

                        <Col md={10}>
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
                                <fieldset>
                                    <legend>Purchase Kwh</legend>
                                    <Row>
                                        <Col md={6} lg={6} >
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Meter Serial<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="meterSerial" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Meter Serial" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Invoice No<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="invoiceNo" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Invoice No" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Record Date<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field
                                                        name="recordDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.recordDate}
                                                        handleChange={this.handleRecordDate.bind(this)}
                                                        placeholder="Record Date" />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Rate<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="rateId" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Rate" 
                                                        values={this.state.rates}
                                                        icon="" />
                                                </Col>
                                            </Row>
                                        </Col>

                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Purchase (Kwh)<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="purchaseKwh" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Purchase(Kwh)" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Coefficient<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="coefficient" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Coefficient" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Last Usage<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="lastUsage" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Last Usage" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>New Usage<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="newUsage" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="New Usage" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </fieldset>

                                <Row>
                                    <Col mdOffset={8} sm={3} md={2}>
                                        <ButtonSubmit 
                                            error={error} 
                                            invalid={invalid} 
                                            submitting={submitting} 
                                            label="Save" />
                                    </Col>
                                    <Col sm={3} md={2} className="col-padding-left-0">
                                        <Button className="btn btn-default btn-block" 
                                            onClick={() => window.history.back()}>
                                            Back
                                        </Button>
                                    </Col>
                                </Row>
                            </form>

                        </Col>
                    </Row>
                    : 
                    <Row>
                        <Col mdOffset={10} md={2}>
                            <Button className="btn btn-default btn-block" 
                                onClick={() => window.history.back()}>
                                Back
                            </Button>
                        </Col>
                    </Row> 
                }
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_purchase_kwh'));

AddPurchaseKwh = reduxForm({
    form: 'form_add_purchase_kwh',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_code.test(values.meterSerial) || values.meterSerial === undefined){
            errors.meterSerial = 'Invalid Meter Serial!';
        }
        if (!regex_name.test(values.invoiceNo) || values.invoiceNo === undefined) {
            errors.invoiceNo = 'Invalid Invoice No!';
        }

        if (!regex_number.test(values.purchaseKwh) || values.purchaseKwh === undefined) {
            errors.purchaseKwh = 'Invalid Purchase Kwh!';
        }
        if (!regex_number.test(values.coefficient) || values.coefficient === undefined) {
            errors.coefficient = 'Invalid Coefficient!';
        }
        if (!regex_number.test(values.lastUsage) || values.lastUsage === undefined) {
            errors.lastUsage = 'Invalid Last Usage!';
        }
        if (!regex_number.test(values.newUsage) || values.newUsage === undefined) {
            errors.newUsage = 'Invalid New Usage!';
        }
        if (values.rateId === undefined) {
            errors.rateId = 'Rate is required!';
        }
        if (values.recordDate === undefined) {
            errors.recordDate = 'Record Date is required!';
        }
        
        return errors
    }
})(AddPurchaseKwh);

function mapStateToProps(state) {
    return {
        getSuppliers: state.supplier.getSuppliers,
        connectionBuys: state.purchaseKwh.connectionBuys,
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        purchaseKwhAdd: state.purchaseKwh.purchaseKwhAdd,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        getSuppliersAction,
        getConnectionBuysAction,
        getAllRatePerKwhAction,
        addPurchaseKwhAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddPurchaseKwh);