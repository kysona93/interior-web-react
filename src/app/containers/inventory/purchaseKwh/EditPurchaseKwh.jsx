import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox, Tabs, Tab } from 'react-bootstrap';
import { Field, reduxForm, reset, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import '../../../components/forms/Styles.css';
import { getAllRatePerKwhAction } from './../../../actions/setup/rateKwh';
import { getConnectionBuysAction, updatePurchaseKwhAction } from './../../../actions/inventory/purchaseKwh';
import moment from 'moment';

let rates = [],
    filter = {
        limit: 100,
        page: 1,
        keyWords: ''
};

class EditPurchaseKwh extends React.Component {
    constructor(props){
        super(props);
        this.state={
            recordDate: this.props.object.recordDate !== null ? moment(this.props.object.recordDate) : null,
            connectionBuyId: 0,
            rates: []
        };
    }

    componentWillMount(){
        this.props.getAllRatePerKwhAction();

        let object = {
            "coefficient": this.props.object.coefficient,
            "invoiceNo": this.props.object.invoiceNo,
            "purchaseKwh": this.props.object.kwhPurchase,
            "lastUsage": this.props.object.lastUsageRecord,
            "meterSerial": this.props.object.meterSerial,
            "newUsage": this.props.object.newUsageRecord,
            "rateId": this.props.object.ratePerKwh.id,
            "recordDate": moment(this.props.object.recordDate).format("YYYY-MM-DD")
        };
        
        this.props.dispatch(initialize('form_edit_purchase_kwh', object));
    }

    componentWillUnmount() {
        this.props.connectionBuys.status = 0;
    }

    componentWillReceiveProps(newProps){
        // hide show form add purchase kwh
        if (newProps.connectionBuys.status === 200) {
            this.setState({ isSearching: true });
        }

        // rate
        if(newProps.ratePerKwhAll.items !== undefined){
            let items = newProps.ratePerKwhAll.items;
            if (items.length === rates.length){
                this.setState({rates : rates});
            } else {
                items.forEach((element) => {
                    rates.push({
                        id: element.id,
                        name: element.rate
                    });
                });
                this.setState({rates : rates});
            }
        } 

        // update
        if(newProps.purchaseKwhUpdate.status === 200){
            if(newProps.purchaseKwhUpdate.data.status === 200){
                alert("Successfully updated new purchase kwh.");
                this.props.handleHideShowEdit(false)
                newProps.purchaseKwhUpdate.data.status = 0;
            } else {
                alert("Fail with update purchase kwh!");
                newProps.purchaseKwhUpdate.data.status = 0;
            }
            newProps.purchaseKwhUpdate.status = 0;
        }
        if(newProps.purchaseKwhUpdate.status === 404 || newProps.purchaseKwhUpdate.status === 500){
            alert("Fail with update purchase kwh!");
            newProps.purchaseKwhUpdate.status = 0;
        }
    }

    handleRecordDate(date) {
        this.setState({
            recordDate: date
        });
    }

    handleSubmit(values){
        let data = {
            "id": this.props.object.id,
            "coefficient": values.coefficient,
            "invoiceNo": values.invoiceNo,
            "kwhPurchase": values.purchaseKwh,
            "lastUsageRecord": values.lastUsage,
            "meterSerial": values.meterSerial,
            "newUsageRecord": values.newUsage,
            "ratePerKwhId": values.rateId,
            "recordDate": values.recordDate
        };
        this.props.updatePurchaseKwhAction(data);
    }

    // handle search
    handleChangeSearch(event) {
        if (event.target.value !== '') {
            if (this.props.getSuppliers.status === 200){
                const item = this.props.getSuppliers.data.items.find(i => i.id === Number(event.target.value));
                filter.keyWords = item.name;
            }
        } else filter.keyWords = '';
    }

    handleSubmitSearch() {
        if (filter.keyWords === '') alert('Please Select Supplier!');
        else this.props.getConnectionBuysAction(filter);
    }

    handleEditPurchaseKwh(object) {
        this.setState({ connectionBuyId: object.id });
        this.props.dispatch(change('form_edit_purchase_kwh', 'meterSerial', object.meterSerial));
    }


    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
                            <fieldset>
                                <legend>Purchase Kwh</legend>
                                <Row>
                                    <Col md={6} lg={6} >
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Meter Serial<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="meterSerial" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Meter Serial" 
                                                    disabled={true}
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Invoice No<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="invoiceNo" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Invoice No" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Record Date<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field
                                                    name="recordDate" 
                                                    component={DateTimePicker} 
                                                    defaultDate={this.state.recordDate}
                                                    handleChange={this.handleRecordDate.bind(this)}
                                                    placeholder="Record Date" />
                                            </Col>
                                        </Row>
                                    
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Rate<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="rateId" 
                                                    type="select" 
                                                    component={SelectBox} 
                                                    placeholder="Select Rate" 
                                                    values={this.state.rates}
                                                    icon="" />
                                            </Col>
                                        </Row>
                                    </Col>

                                    <Col md={6} lg={6}>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Purchase (Kwh)<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="purchaseKwh" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Purchase(Kwh)" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Coefficient<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="coefficient" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Coefficient" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Last Usage<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="lastUsage" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Last Usage" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>New Usage<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="newUsage" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="New Usage" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </fieldset>

                            <Row>
                                <Col mdOffset={8} sm={3} md={2}>
                                    <ButtonSubmit 
                                        error={error} 
                                        invalid={invalid} 
                                        submitting={submitting} 
                                        label="Save" />
                                </Col>
                                <Col sm={3} md={2} className="col-padding-left-0">
                                    <Button className="btn btn-default btn-block" 
                                        onClick={() => this.props.handleHideShowEdit(false)}>
                                        Back
                                    </Button>
                                </Col>
                            </Row>
                        </form>

                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_edit_purchase_kwh'));

EditPurchaseKwh = reduxForm({
    form: 'form_edit_purchase_kwh',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_code.test(values.meterSerial) || values.meterSerial === undefined){
            errors.meterSerial = 'Invalid Meter Serial!';
        }
        if (!regex_name.test(values.invoiceNo) || values.invoiceNo === undefined) {
            errors.invoiceNo = 'Invalid Invoice No!';
        }
        if (!regex_number.test(values.purchaseKwh) || values.purchaseKwh === undefined) {
            errors.purchaseKwh = 'Invalid Purchase Kwh!';
        }
        if (!regex_number.test(values.coefficient) || values.coefficient === undefined) {
            errors.coefficient = 'Invalid Coefficient!';
        }
        if (!regex_number.test(values.lastUsage) || values.lastUsage === undefined) {
            errors.lastUsage = 'Invalid Last Usage!';
        }
        if (!regex_number.test(values.newUsage) || values.newUsage === undefined) {
            errors.newUsage = 'Invalid New Usage!';
        }
        if (values.rateId === undefined) {
            errors.rateId = 'Rate is required!';
        }
        if (values.recordDate === undefined) {
            errors.recordDate = 'Record Date is required!';
        }
        
        return errors
    }
})(EditPurchaseKwh);

function mapStateToProps(state) {
    return {
        connectionBuys: state.purchaseKwh.connectionBuys,
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        purchaseKwhUpdate: state.purchaseKwh.purchaseKwhUpdate,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getConnectionBuysAction,
        getAllRatePerKwhAction,
        updatePurchaseKwhAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditPurchaseKwh);