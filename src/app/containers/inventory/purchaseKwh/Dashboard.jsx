import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import AddPurchaseKwh from './AddPurchaseKwh';
import AddConnectionBuy from './AddConnectionBuy';

class DashboardPurchaseKwh extends React.Component {
    constructor(props){
        super(props);
        this.state={
            key: 1,
            selectTab1: false
        };
        this.handleSelect = this.handleSelect.bind(this);
    }

    handleSelect(key) {
        this.setState({key});
        if (key == 1) {
            this.setState({ selectTab1: false });
        } else if (key == 2) {
            this.setState({ selectTab1: true });
        }
    }

    render(){
        return(
            <div className="margin_left_25">
                <Tabs activeKey={this.state.key} onSelect={this.handleSelect} animation={false} id="controlled-tab-example">
                    <Tab eventKey={1} title="Add Purchase Kwh" />
                    <Tab eventKey={2} title="Add New Connection Buy" />
                </Tabs>
                <br />
                { this.state.selectTab1 === false ? 
                    <AddPurchaseKwh /> : <AddConnectionBuy />
                }
            </div>
        )
    }
}


export default DashboardPurchaseKwh;