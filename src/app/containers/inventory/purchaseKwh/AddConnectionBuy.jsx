import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox, FormGroup } from 'react-bootstrap';
import { Field, reduxForm, reset, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import {Typeahead} from 'react-bootstrap-typeahead';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import Pagination from './../../../components/forms/Pagination';
import '../../../components/forms/Styles.css';
import { getAllPolesAction } from './../../../actions/setup/pole';
import { getSuppliersAction } from './../../../actions/inventory/supplier';
import { getConnectionTypesAction } from './../../../actions/inventory/connectionType';
import { 
    addConnectionBuyAction, 
    getConnectionBuysAction,
    updateConnectionBuyAction, 
    deleteConnectionBuyAction } from './../../../actions/inventory/purchaseKwh';
import moment from 'moment';
import { canCreate, canUpdate, canDelete } from './../../../../auth';

let poles = [];
let suppliers = [];
let connectionTypes = [];
let filter = {
    limit: 10,
    page: 1,
    keyWords: ''
};

class AddConnectionBuy extends React.Component {
    constructor(props){
        super(props);
        this.state={
            update: false,
            btnSubmit: 'Save',
            btnCancel: 'Back',
            connectedDate: null,
            suppliers: [],
            connectionTypes: [],
            poleId: 0,
            id: 0,
            defaultPole: [],
            meterHistory: ''
        };
        this.handleConnectedDate = this.handleConnectedDate.bind(this);
    }

    componentWillMount(){
        this.props.getAllPolesAction();
        this.props.getSuppliersAction({type: 'ELECTRICITY'});
        this.props.getConnectionTypesAction();
        this.props.getConnectionBuysAction(filter);
    }

    componentWillUnmount() {
        this.props.connectionBuys.status = 0;
    }

    componentWillReceiveProps(newProps){

        //pole
        if(newProps.poleAll.items !== undefined){
            poles = newProps.poleAll.items;
        }

        //supplier
        if(newProps.getSuppliers.status === 200){
            let items = newProps.getSuppliers.data.items;
            if(items.length === suppliers.length){
                this.setState({suppliers : suppliers});
            }else {
                items.forEach((element) => {
                    suppliers.push({
                        "id": element.id,
                        "name": element.name
                    });
                });
                this.setState({suppliers : suppliers});
            }
        }

        //supplier
        if(newProps.connectionTypes.status === 200){
            let items = newProps.connectionTypes.data.items;
            if(items.length === connectionTypes.length){
                this.setState({connectionTypes : connectionTypes});
            }else {
                items.forEach((element) => {
                    connectionTypes.push({
                        "id": element.id,
                        "name": element.type
                    });
                });
                this.setState({connectionTypes : connectionTypes});
            }
        }

        //add
        if(newProps.connectionBuyAdd.status === 200){
            if(newProps.connectionBuyAdd.data.status === 200){
                alert("Successfully added new purchase kwh.");
                this.props.getConnectionBuysAction(filter);
                newProps.connectionBuyAdd.data.status = 0;
            } else {
                alert("Fail with add purchase kwh!");
                newProps.connectionBuyAdd.data.status = 0;
            }
            newProps.connectionBuyAdd.status = 0;
        }
        if(newProps.connectionBuyAdd.status === 404 || newProps.connectionBuyAdd.status === 500){
            alert("Fail with add purchase kwh!");
            newProps.connectionBuyAdd.status = 0;
        }

        //update
        if(newProps.connectionBuyUpdate.status === 200 ){
            if(newProps.connectionBuyUpdate.data.status === 200){
                alert("Successfully updated connection buy.");
                this.handleCancel();
                this.props.getConnectionBuysAction(filter);
                newProps.connectionBuyUpdate.status = 0;
            } else {
                alert("Fail with update connection buy!");
                newProps.connectionBuyUpdate.status = 0;
            }
            newProps.connectionBuyUpdate.status = 0;
        }
        if(newProps.connectionBuyUpdate.status === 400 || newProps.connectionBuyUpdate.status === 500){
            alert("Fail with update connection buy!");
            newProps.connectionBuyUpdate.status = 0;
        }

        //delete
        if(newProps.connectionBuyDelete.status === 200){
            if(newProps.connectionBuyDelete.data.status === 200){
                alert("Successfully deleted this connection buy.");
                this.props.getConnectionBuysAction(filter);
                newProps.connectionBuyDelete.status = 0;
            } else {
                alert("Fail with delete this connection buy!");
                newProps.connectionBuyDelete.status = 0;
            }
            newProps.connectionBuyDelete.status = 0;
        }
        if(newProps.connectionBuyDelete.status === 400 || newProps.connectionBuyDelete.status === 500){
            alert("Fail with delete this connection buy!");
            newProps.connectionBuyDelete.status = 0;
        }
    }

    handleConnectedDate(date) {
        this.setState({
            connectedDate: date
        });
    }
    
    // handle search
    handleChangeSearch(event) {
        filter.keyWords = event.target.value;
    }

    handleSubmitSearch() {
        this.props.getConnectionBuysAction(filter);
    }

    // handle add
    handleSubmit(values){
        if(!this.state.update){
            //add
            let data = {
                "address": values.address,
                "ampere": values.ampere,
                "connectionTypeId": values.connectionTypeId,
                "dateConnected": values.connectedDate,
                "meterSerial": values.meterSerial,
                "phase": values.phase,
                "poleId": this.state.poleId,
                "supplierId": values.supplierId,
                "voltage": values.voltage
            };
            this.props.addConnectionBuyAction(data);
        } else {
            //update
            let data = {
                "id": this.state.id,
                "address": values.address,
                "ampere": values.ampere,
                "connectionTypeId": values.connectionTypeId,
                "dateConnected": values.connectedDate,
                "meterSerial": values.meterSerial,
                "phase": values.phase,
                "poleId": this.state.poleId,
                "supplierId": values.supplierId,
                "voltage": values.voltage
            };
            this.props.updateConnectionBuyAction(data);
        }

    }

    handleUpdate(object){
        this.setState({
            update: true, 
            btnSubmit: 'Edit',
            btnCancel: 'Cancel',
            id: object.id,
            connectedDate: moment(object.dateConnected),
            defaultPole: [object.pole],
            meterHistory: object.meterHistory
        });
        this.props.dispatch(initialize('form_add_connection_buy', object));
        this.props.dispatch(change('form_add_connection_buy', 'supplierId', object.supplier.id));
        this.props.dispatch(change('form_add_connection_buy', 'connectionTypeId', object.connectionType.id));
        this.props.dispatch(change('form_add_connection_buy', 'connectedDate', moment(object.dateConnected).format("YYYY-MM-DD")));
    }

    handleDelete(id){
        if(confirm("Are you sure want to delete this connection?") === true){
            this.props.deleteConnectionBuyAction(id);
        }
    }

    handleCancel(){
        this.setState({
            update: false, 
            btnSubmit: 'Save', 
            btnCancel: 'Back', 
            connectedDate: null, 
            defaultPole: [],
            meterHistory: ''
        });
        this.props.dispatch(initialize('form_add_connection_buy', {}));
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        let total = 0;
        let isSelectPole = true;
        (this.state.poleId !== 0 && this.state.poleId !== undefined) ? isSelectPole = false : isSelectPole = true;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <fieldset className="col-md-12">
                                <legend>Connection Buy</legend>
                                <Row>
                                    <Col md={6} lg={6} >
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Supplier Name<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="supplierId" 
                                                    type="select" 
                                                    component={SelectBox} 
                                                    placeholder="Select Supplier" 
                                                    values={this.state.suppliers}
                                                    icon="" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Connection Type<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="connectionTypeId" 
                                                    type="select" 
                                                    component={SelectBox} 
                                                    placeholder="Select Type" 
                                                    values={this.state.connectionTypes}
                                                    icon="" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Meter<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field
                                                    name="meterSerial" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Meter Serial" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Phase<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="phase" 
                                                    type="text" 
                                                    component={TextBox}
                                                    label="Phase"
                                                    icon="" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Ampere<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="ampere" 
                                                    type="text" 
                                                    component={TextBox}
                                                    label="Ampere"
                                                    icon="" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Voltage<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="voltage" 
                                                    type="text" 
                                                    component={TextBox} 
                                                    label="Voltage"
                                                    icon="" />
                                            </Col>
                                        </Row>
                                    </Col>

                                    <Col md={6} lg={6}>
                                        
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Pole<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Typeahead
                                                    className="form-group"
                                                    clearButton
                                                    onChange={selected => { this.setState({poleId: _.map(selected, "id")[0]}) }}
                                                    labelKey="serial"
                                                    options={poles}
                                                    selected={this.state.defaultPole}
                                                    placeholder="Pole ..."
                                                />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Address<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="address" 
                                                    type="text" 
                                                    component={TextArea} 
                                                    height={90}
                                                    label="Address" 
                                                    icon=""/>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={3} lg={3} className="label-name">
                                                <strong>Connected Date<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={9} lg={9}>
                                                <Field 
                                                    name="connectedDate" 
                                                    component={DateTimePicker} 
                                                    defaultDate={this.state.connectedDate}
                                                    handleChange={this.handleConnectedDate}
                                                    placeholder="Connected Date" />
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col mdOffset={3} lgOffset={3} md={9} lg={9}>
                                                { this.state.meterHistory !== '' ? <b>Meter History</b> : null } <br/>
                                                <div dangerouslySetInnerHTML={{__html: this.state.meterHistory }}></div><br/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </fieldset>

                            <Row>
                                <Col mdOffset={8} sm={3} md={2}>
                                    <ButtonSubmit 
                                        error={error} 
                                        invalid={isSelectPole === true ? true : invalid} 
                                        submitting={submitting} 
                                        label={this.state.btnSubmit} />
                                </Col>
                                <Col sm={3} md={2} className="col-padding-left-0">
                                    <Button className="btn btn-default btn-block" 
                                        onClick={() => this.state.btnCancel === "Back" ? 
                                            window.history.back() : this.handleCancel()}>
                                        {this.state.btnCancel}
                                    </Button>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>

                <Row>
                    <Col md={12}>
                        <Col md={4}>
                            <Field 
                                name="keyWords" 
                                type="text" 
                                component={TextBox} 
                                label="Supplier or Meter Serial" 
                                onChange={this.handleChangeSearch.bind(this)}
                                icon="fa fa-search"/>
                        </Col>
                        <Col md={1}>
                            <button className="btn btn-default" onClick={() => this.handleSubmitSearch()} > 
                                <i className="fa fa-search" />
                            </button>
                        </Col>
                    </Col>
                </Row>

                <Row>
                    <Col md={12}>
                        <div className="">
                            <table className="list-smaller-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Date Connected</th>
                                    <th>Supplier</th>
                                    <th>Connection Type</th>
                                    <th>Meter Serial</th>
                                    <th>Phase</th>
                                    <th>Ampere</th>
                                    <th>Voltage</th>
                                    <th>Pole Serial</th>
                                    <th>Latitude</th>
                                    <th>Longitude</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.props.connectionBuys.status === 200 ?
                                        this.props.connectionBuys.data.items.list.map((i, index)=>{
                                            total = this.props.connectionBuys.data.items.totalCount;
                                            return(
                                                <tr key={index}>
                                                    <td>{i.id}</td>
                                                    <td>{moment(i.dateConnected).format('L')}</td>
                                                    <td>{i.supplier.name}</td>
                                                    <td>{i.connectionType.type}</td>
                                                    <td>{i.meterSerial}</td>
                                                    <td>{i.phase}</td>
                                                    <td>{i.ampere}</td>
                                                    <td>{i.voltage}</td>
                                                    <td>{i.pole.serial}</td>
                                                    <td>{i.pole.x}</td>
                                                    <td>{i.pole.y}</td>
                                                    <td className="text-center">
                                                        {
                                                            canUpdate == false ?
                                                                <a className='btn btn-info btn-xs' onClick={() => this.handleUpdate(i)}>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </a>
                                                            : null
                                                        }
                                                        {
                                                            canDelete == false ?
                                                                <a onClick={()=> this.handleDelete(i.id)} className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            : null
                                                        }
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    :
                                    <tr>
                                        <td colSpan={14} className="text-align-center">RESULT NOT FOUND</td>
                                    </tr>
                                }
                                </tbody>
                            </table>
                            <br/>
                            <Pagination 
                                totalCount = {total}
                                items = {filter}
                                callBackAction = {this.props.getConnectionBuysAction}
                            />
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_connection_buy'));

AddConnectionBuy = reduxForm({
    form: 'form_add_connection_buy',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};

        if (!regex_name.test(values.meterSerial) || values.meterSerial === undefined) {
            errors.meterSerial = 'Invalid Meter Serial!';
        }
        if (values.supplierId === undefined) {
            errors.supplierId = 'Supplier is required!';
        }
        if (values.connectionTypeId === undefined) {
            errors.connectionTypeId = 'Connection Type is required!';
        }
        if (!regex_name.test(values.phase) ||values.phase === undefined) {
            errors.phase = 'Invalid Phase!';
        }
        if (!regex_name.test(values.ampere) ||values.ampere === undefined) {
            errors.ampere = 'Invalid Ampere!';
        }
        if (!regex_name.test(values.voltage) ||values.voltage === undefined) {
            errors.voltage = 'Invalid Voltage!';
        }
        if (values.poleId === undefined) {
            errors.poleId = 'Pole is required!';
        }
        if (values.connectedDate === undefined) {
            errors.connectedDate = 'Connected Date is required!';
        }
        if (values.address === undefined) {
            errors.address = 'Address is required!';
        }
        
        return errors
    }
})(AddConnectionBuy);

function mapStateToProps(state) {
    return {
        poleAll: state.pole.poleAll,
        getSuppliers: state.supplier.getSuppliers,
        connectionTypes: state.connectionType.connectionTypes,
        connectionBuyAdd: state.purchaseKwh.connectionBuyAdd,
        connectionBuys: state.purchaseKwh.connectionBuys,
        connectionBuyUpdate: state.purchaseKwh.connectionBuyUpdate,
        connectionBuyDelete: state.purchaseKwh.connectionBuyDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllPolesAction,
        getSuppliersAction,
        getConnectionTypesAction,
        addConnectionBuyAction,
        getConnectionBuysAction,
        updateConnectionBuyAction,
        deleteConnectionBuyAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddConnectionBuy);