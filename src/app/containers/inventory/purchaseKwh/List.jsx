import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, FormGroup, Label } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import moment from 'moment';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { getPurchaseKwhsAction, deletePurchaseKwhAction } from './../../../actions/inventory/purchaseKwh';
import Pagination from './../../../components/forms/Pagination';
import { canCreate, canUpdate, canDelete } from './../../../../auth';
import EditPurchaseKwh from './EditPurchaseKwh';

let filter = {
    limit: 10,
    page: 1,
    year: "",
    month: 1,
    keyWords: ""
}
class ListPurchaseKwh extends React.Component {
    constructor(props){
        super(props);
        this.state={
            months: [
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ],
            object: {},
            isEdit: false,
            date: null
        };
        this.handleDeleteRow = this.handleDeleteRow.bind(this);
    }

    componentWillMount(){
        this.props.getPurchaseKwhsAction(filter);
    }

    componentWillReceiveProps(data){

        if(data.purchaseKwhDelete.status === 200){
            if(data.purchaseKwhDelete.data.status === 200) {
                alert("Successfully delete purchase kwh!.");
                this.props.getPurchaseKwhsAction(filter);
                data.purchaseKwhDelete.data.status = 0;
            }
            data.purchaseKwhDelete.status = 0;
        }
        if(data.purchaseKwhDelete.status === 400 || data.purchaseKwhDelete.status === 500){
            alert("Fail with delete purchase kwh!");
        }
    }

    handleDeleteRow(id){
        if(confirm("Are you sure want to delete this purchase kwh?") === true){
            this.props.deletePurchaseKwhAction(id);
        }
    }

    handleEditRow(object) {
        this.setState({ 
            object: object,
            isEdit: true
        });
    }

    handleHideShowEdit(value) {
        this.setState({ isEdit: value });
        this.props.getPurchaseKwhsAction(filter);
    }

    handleSubmit(values){

        if(values.year === undefined) filter.year = '';
        else filter.year = values.year;

        if(values.month === undefined) filter.month = 1;
        else filter.month = values.month;

        if(values.keyWords === undefined) filter.keyWords = '';
        else filter.keyWords = values.keyWords;

        this.props.getPurchaseKwhsAction(filter);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let total = 0;
        return(
            this.state.isEdit === false ?
                <div className="margin_left_25 margin_top_minus_30">
                    <Row>
                        <h3 className="text-align-center">List Purchase Kwh</h3>
                    </Row>
                    <Row>
                        <Col md={3}>
                        {
                            canCreate == false ?
                                <Button 
                                    onClick={() => browserHistory.push('/app/inventory/purchase-kwh/add')}
                                    className="btn btn-default">
                                    <i className="fa fa-plus" />
                                    {' '}Add New Purchase
                                </Button>
                            : null
                        }
                        </Col>
                        <Col md={9}>
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="form-inline">
                                <Col md={10}>
                                    <div className="pull-right">
                                        <FormGroup controlId="formInlineName">
                                            <Field
                                                name="year"
                                                type="text"
                                                component={TextBox}
                                                label="year"
                                                />
                                        </FormGroup>
                                        {' '}
                                        <FormGroup controlId="formInlineName">
                                            <Field
                                                name="month"
                                                type="select"
                                                component={SelectBox}
                                                placeholder="Select Month"
                                                values={this.state.months} />
                                        </FormGroup>
                                        {' '}
                                        <FormGroup controlId="formInlineName">
                                            <Field
                                                name="keyWords"
                                                type="text"
                                                component={TextBox}
                                                label="Meter Serial / Invoice No" />
                                        </FormGroup>
                                    </div>
                                </Col>
                                <Col md={2}>
                                    <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH"/>
                                </Col>
                            </form>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={12}>
                            <div className="">
                                <table className="list-smaller-table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Meter Serial</th>
                                        <th>Invoice No</th>
                                        <th>Record Date</th>
                                        <th>Purchase Kwh</th>
                                        <th>Last Usage</th>
                                        <th>New Usage</th>
                                        <th>Coefficient</th>
                                        <th>Rate</th>
                                        <th>Currency</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.purchaseKwhs.status === 200 ?
                                            this.props.purchaseKwhs.data.items.list.map((i ,index) => {
                                                total = this.props.purchaseKwhs.data.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>{i.meterSerial}</td>
                                                        <td>{i.invoiceNo}</td>
                                                        <td>{moment(i.recordDate).format('L')}</td>
                                                        <td>{i.kwhPurchase}</td>
                                                        <td>{i.lastUsageRecord}</td>
                                                        <td>{i.newUsageRecord}</td>
                                                        <td>{i.coefficient}</td>
                                                        <td>{i.ratePerKwh.rate}</td>
                                                        <td>{i.ratePerKwh.currency}</td>
                                                        <td className="text-center">
                                                            {
                                                                canUpdate === false ?
                                                                    <a className='btn btn-info btn-xs' onClick={() => this.handleEditRow(i)}>
                                                                        <span className="glyphicon glyphicon-edit"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                            {
                                                                canDelete === false ?
                                                                    <a onClick={()=> this.handleDeleteRow(i.id)} className="btn btn-danger btn-xs">
                                                                        <span className="glyphicon glyphicon-remove"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        :
                                        <tr>
                                            <td colSpan={11} className="text-align-center">RESULT NOT FOUND</td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                                <br/>
                                <Pagination 
                                    totalCount = {total}
                                    items = {filter}
                                    callBackAction = {this.props.getPurchaseKwhsAction}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            : 
            <EditPurchaseKwh object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} />
        )
    }
}

ListPurchaseKwh = reduxForm({
    form: 'form_list_purchase_kwh',
    validate: (values) => {
        let regex_number = /^[0-9]{4,4}$/;
        
        const errors = {};

        if(values.year !== undefined){
            if (!regex_number.test(values.year)) {
                errors.year = 'Invalid Year!';
            }
        }
        return errors
    }
})(ListPurchaseKwh);

function mapStateToProps(state) {
    return {
        purchaseKwhs: state.purchaseKwh.purchaseKwhs,
        purchaseKwhDelete: state.purchaseKwh.purchaseKwhDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        deletePurchaseKwhAction,
        getPurchaseKwhsAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListPurchaseKwh);