import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, FormGroup, Label } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { getItemsAction, deleteItemAction } from '../../../actions/inventory/item';
import { listAllItemGroupAction } from './../../../actions/inventory/itemGroup';
import Pagination from './../../../components/forms/Pagination';
import EditItem from './Edit';
import { canCreate, canUpdate, canDelete } from './../../../../auth';

let arrItemGroup = [];
let item = {
    limit: 10,
    page: 1,
    group: 0,
    keyWords: "",
    status: -1
};
class ListItem extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activePage: 1,
            status: [{id: 1, name: "Active"},{id: 0, name: "In Active"}],
            itemGroups: [],
            item: {},
            isEdit: false
        };
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentWillMount(){
        this.props.getItemsAction(item);
        this.props.listAllItemGroupAction(1);
    }

    componentWillReceiveProps(data){
        // item group
        if(data.listAllItemGroup.status === 200){
            let items = data.listAllItemGroup.data.items;
            if(items.length === arrItemGroup.length){
                this.setState({itemGroups : arrItemGroup});
            }else {
                items.forEach((element) => {
                    arrItemGroup.push({
                        "id": element.id,
                        "name": element.itemGroup
                    });
                });
                this.setState({itemGroups : arrItemGroup});
            }
        }

        if(data.itemDelete.status === 200){
            alert("Successfully deleted this item.");
            item.group = 0;
            item.status = 1;
            this.props.getItemsAction(item);
            data.itemDelete.status = 0;
        }
        if(data.itemDelete.status === 400 || data.itemDelete.status === 500){
            alert("Fail with delete this item!");
        }
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this item?") === true){
            this.props.deleteItemAction(id);
        }
    }

    handleSubmit(values){
        if(values.group === undefined) item.group = 0;
        else item.group = Number(values.group);

        if(values.keyWords === undefined) item.keyWords = "";
        else item.keyWords = values.keyWords;

        if(values.status === undefined) item.status = -1;
        else item.status = Number(values.status);
        this.props.getItemsAction(item);
    }

    handleEditItem(object) {
        this.setState({ 
            item: object,
            isEdit: true
        });
    }

    handleIsEdit(value) {
        //parent props
        this.setState({ isEdit: value });
        this.props.getItemsAction(item);
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            this.state.isEdit == false ?
                <div className="margin_left_25 margin_top_minus_30">
                    <Row>
                        <h3 className="text-align-center">List Items</h3>
                    </Row>
                    <Row>
                        <Col md={4}>
                        {
                            canCreate == true ?
                                <Button 
                                    onClick={() => browserHistory.push('/app/inventory/items/add')}
                                    className="btn btn-default">
                                    <i className="fa fa-plus" />
                                    {' '}Add Item
                                </Button>
                            : null
                        }
                        </Col>
                        <Col md={8}>
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="form-inline">
                                <Col md={10}>
                                    <div className="pull-right">
                                    <FormGroup controlId="formInlineName">
                                        <Field name="keyWords" type="text" component={TextBox} label="Item Name" icon="fa fa-search"/>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup controlId="formInlineName">
                                        <Field name="group" type="select" component={SelectBox} placeholder="Item group"
                                            values={this.state.itemGroups} sortBy="name" icon=""/>
                                    </FormGroup>
                                    {' '}
                                    <FormGroup controlId="formInlineName">
                                        <Field name="status" type="select" component={SelectBox} placeholder="status"
                                            values={this.state.status} sortBy="name" icon=""/>
                                    </FormGroup>
                                    </div>
                                </Col>
                                <Col md={2}>
                                    <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH"/>
                                </Col>
                            </form>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={12}>
                            <div className="">
                                <table className="list-smaller-table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Item Code</th>
                                        <th>Item Name</th>
                                        <th>Item Name Kh</th>
                                        <th>Short Name</th>
                                        <th>Item Group</th>
                                        <th>Currency</th>
                                        <th>Normal Price</th>
                                        <th>Foreign Price</th>
                                        <th>Business Price</th>
                                        <th>Quantity</th>
                                        <th>Unit Type</th>
                                        <th>Status</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.items.items !== undefined ?
                                            this.props.items.items.list.map((item, index)=>{
                                                total = this.props.items.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td>{item.id}</td>
                                                        <td>{item.itemCode}</td>
                                                        <td>{item.itemNameEn}</td>
                                                        <td>{item.itemNameKh}</td>
                                                        <td>{item.shortName}</td>
                                                        <td>{item.itemGroup.itemGroup}</td>
                                                        <td>{item.currency}</td>
                                                        <td>{item.price1}</td>
                                                        <td>{item.price2}</td>
                                                        <td>{item.price3}</td>
                                                        <td>{item.quantity}</td>
                                                        <td>{item.unitType !== null ? item.unitType.unitType : "Unit"}</td>
                                                        { item.status == 1 ? 
                                                            <td><Label bsStyle="primary">Enable</Label></td> : 
                                                            <td><Label bsStyle="danger">Disable</Label></td>
                                                        }
                                                        <td className="text-center">
                                                            {
                                                                canUpdate == true ?
                                                                    <a className='btn btn-info btn-xs' onClick={() => this.handleEditItem(item)}>
                                                                        <span className="glyphicon glyphicon-edit"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                            {
                                                                canDelete == true ?
                                                                    <a onClick={()=> this.deleteItem(item.id)} className="btn btn-danger btn-xs">
                                                                        <span className="glyphicon glyphicon-remove"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        :
                                        <tr>
                                            <td colSpan={14} className="text-align-center">RESULT NOT FOUND</td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                                <br/>
                                <Pagination 
                                    totalCount = {total}
                                    items = {item}
                                    callBackAction = {this.props.getItemsAction}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            : 
            <EditItem object={this.state.item} handleIsEdit={this.handleIsEdit.bind(this)} />
        )
    }
}

ListItem = reduxForm({
    form: 'form_list_item',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]/;

        const errors = {};
        if (!regex_name.test(values.name)) {
            errors.name = 'Invalid item name!';
        }
        return errors
    }
})(ListItem);

function mapStateToProps(state) {
    return {
        listAllItemGroup: state.itemGroup.listAllItemGroup,
        items: state.item.items,
        itemDelete: state.item.itemDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({listAllItemGroupAction, getItemsAction, deleteItemAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListItem);