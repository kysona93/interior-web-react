import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addItemAction } from '../../../actions/inventory/item';
import { listAllItemGroupAction } from './../../../actions/inventory/itemGroup';
import { getItemCategoryByParent, getItemSubCategoryByParent } from './../../../actions/inventory/itemCategory';
import { listAllUnitTypesAction } from './../../../actions/inventory/unitType';
import { getSuppliersAction } from './../../../actions/inventory/supplier';

let itemGroups = [];
let itemCategories = [];
let unitTypes = [];
let suppliers = [];

class AddItem extends React.Component {
    constructor(props){
        super(props);
        this.state={
            itemType: [{id: "INVENTORY", name: "Inventory"}, {id: "SERVICE", name: "Service"}],
            hideInventory: true,
            status: [{id: 1, name: "Active"}, {id: 0, name: "In Active"}],
            currency : [{id: 'RIEL', name: '(៛) Riel'}, {id: 'USD', name: '($) USD'}],
            itemGroups: [],
            itemCategories: [],
            itemSubCategories: [],
            unitTypes: [],
            suppliers: []
        };

        this.handleItemType = this.handleItemType.bind(this);
        this.handleSelectCategory = this.handleSelectCategory.bind(this);
    }

    componentWillMount(){
        this.props.listAllItemGroupAction(1);
        this.props.getItemCategoryByParent();
        this.props.listAllUnitTypesAction();
        let supplier = {
            type: 'ACCESSORY'
        };
        this.props.getSuppliersAction(supplier);
    }

    componentWillReceiveProps(data){
        //item group
        if(data.listAllItemGroup.status === 200){
            let items = data.listAllItemGroup.data.items;
            if(items.length === itemGroups.length){
                this.setState({itemGroups : itemGroups});
            }else {
                items.forEach((element) => {
                    itemGroups.push({
                        "id": element.id,
                        "name": element.itemGroup
                    });
                });
                this.setState({itemGroups : itemGroups});
            }
        }

        // item category
        if(data.itemCategoryParent.status === 200){
            let items = data.itemCategoryParent.data.items;
            if(items.length === itemCategories.length){
                this.setState({itemCategories : itemCategories});
            }else {
                items.forEach((element) => {
                    itemCategories.push({
                        "id": element.id,
                        "name": element.name
                    });
                });
                this.setState({itemCategories : itemCategories});
            }
        }

        //item sub category
        if(data.itemSubCategoryParent.status === 200){
            let items = data.itemSubCategoryParent.data.items;
            let arrItemSubCategory = [];
            items.forEach((element) => {
                arrItemSubCategory.push({
                    "id": element.id,
                    "name": element.name
                });
            });
            this.setState({itemSubCategories : arrItemSubCategory});
        }

        //unit types
        if(data.listAllUnitType.data !== undefined){
            let items = data.listAllUnitType.data.items;
            if (items !== undefined) {
                if(items.length === unitTypes.length){
                    this.setState({unitTypes : unitTypes});
                }else {
                    items.forEach((element) => {
                        unitTypes.push({
                            "id": element.id,
                            "name": element.unitType
                        });
                    });
                    this.setState({unitTypes : unitTypes});
                }
            }
        }

        //supplier
        if(data.getSuppliers.status === 200){
            let items = data.getSuppliers.data.items;
            if(items.length === suppliers.length){
                this.setState({suppliers : suppliers});
            }else {
                items.forEach((element) => {
                    suppliers.push({
                        "id": element.id,
                        "name": element.name
                    });
                });
                this.setState({suppliers : suppliers});
            }
        }

        if(data.itemAdd.status === 200){
            if(data.itemAdd.data.status === 200){
                alert("Successfully added new item.");
                data.itemAdd.data.status = 0;
            }else {
                alert("Fail with add item!");
                data.itemAdd.data.status = 0;
            }
            data.itemAdd.status = 0;
        }
        if(data.itemAdd.status === 404 || data.itemAdd.status === 500){
            alert("Fail with add item!");
            data.itemAdd.status = 0;
        }
    }

    handleItemType(event) {
        if(event.target.value == 'SERVICE') this.setState({ hideInventory: false }) 
        else this.setState({ hideInventory: true})
    }

    handleSelectCategory(event) {
        this.props.getItemSubCategoryByParent(event.target.value);
    }

    handleSubmit(values){
        if (values.itemType === "SERVICE") {
            let item = {
                "currency": values.currency,
                "description": values.description,
                "itemCode": values.itemCode,
                "itemGroupId": Number(values.itemGroupId),
                "itemNameEn": values.itemNameEn,
                "itemNameKh": values.itemNameKh,
                "itemType": values.itemType,
                "price1": Number(values.price1),
                "price2": Number(values.price2),
                "price3": Number(values.price3),
                "shortName": values.shortName,
                "status": Number(values.status)
            };
            this.props.addItemAction(item);
        } else {
            let item = {
                "categoryId": Number(values.categoryId),
                "cost": values.cost,
                "currency": values.currency,
                "description": values.description,
                "itemCode": values.itemCode,
                "itemGroupId": Number(values.itemGroupId),
                "itemNameEn": values.itemNameEn,
                "itemNameKh": values.itemNameKh,
                "itemType": values.itemType,
                "price1": Number(values.price1),
                "price2": Number(values.price2),
                "price3": Number(values.price3),
                "quantity": values.quantity,
                "shortName": values.shortName,
                "status": Number(values.status),
                "subCategoryId": Number(values.subCategoryId),
                "supplierId": Number(values.supplierId),
                "unitTypeId": Number(values.unitTypeId)
            };
            this.props.addItemAction(item);
        }
    }


    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add New Item</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={6} lg={6} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Item Type <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="itemType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.itemType} 
                                                        onChange={this.handleItemType}
                                                        sortBy="name" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Item Code <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="itemCode" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Item Code" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Item Name En <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="itemNameEn" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Item Name En" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Item Name Kh</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="itemNameKh" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Item Name Kh" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Short Name <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="shortName" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Item Short Name" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            { this.state.hideInventory == true ? 
                                                <div>
                                                    <Row>
                                                        <Col md={4} lg={4} className="label-name">
                                                            <strong>Category <span className="label-require">*</span></strong>
                                                        </Col>
                                                        <Col md={8} lg={8}>
                                                            <Field 
                                                                name="categoryId" 
                                                                type="select" 
                                                                component={SelectBox} 
                                                                placeholder="Select category" 
                                                                values={this.state.itemCategories} 
                                                                onChange={this.handleSelectCategory}
                                                                sortBy="name" icon=""/>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col md={4} lg={4} className="label-name">
                                                            <strong>Sub Category</strong>
                                                        </Col>
                                                        <Col md={8} lg={8}>
                                                            <Field 
                                                                name="subCategoryId" 
                                                                type="select" 
                                                                component={SelectBox} 
                                                                placeholder="Select sub category" 
                                                                values={this.state.itemSubCategories} 
                                                                sortBy="name" icon=""/>
                                                        </Col>
                                                    </Row>
                                                </div>
                                                : null
                                            }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Item Group <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="itemGroupId" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select item group" 
                                                        values={this.state.itemGroups} 
                                                        sortBy="name" icon=""/>
                                                </Col>
                                            </Row>

                                            { this.state.hideInventory == true ? 
                                                <Row>
                                                    <Col md={4} lg={4} className="label-name">
                                                        <strong>Unit Type <span className="label-require">*</span></strong>
                                                    </Col>
                                                    <Col md={8} lg={8}>
                                                        <Field 
                                                            name="unitTypeId" 
                                                            type="select" 
                                                            component={SelectBox} 
                                                            placeholder="Select unit type" 
                                                            values={this.state.unitTypes} 
                                                            sortBy="name" icon=""/>
                                                    </Col>
                                                </Row>
                                            : null
                                            }

                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Currency <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="currency" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select currency" 
                                                        values={this.state.currency} 
                                                        sortBy="name" icon=""/>
                                                </Col>
                                            </Row>

                                            { this.state.hideInventory == true ? 
                                                <Row>
                                                    <Col md={4} lg={4} className="label-name">
                                                        <strong>Cost</strong>
                                                    </Col>
                                                    <Col md={8} lg={8}>
                                                        <Field 
                                                            name="cost" 
                                                            type="text" 
                                                            component={TextBox} 
                                                            label="Cost" 
                                                            icon=""/>
                                                    </Col>
                                                </Row>
                                            : null
                                            }

                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Normal Price <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="price1" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Normal Price" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Business Price <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="price2" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Business Price" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Hotel Price <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="price3" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Hotel Price" 
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                        </Col>
                                        <Col md={6} lg={6}>
                                            { this.state.hideInventory == true ? 
                                                <div>
                                                    <Row>
                                                        <Col md={4} lg={4} className="label-name">
                                                            <strong>Supplier</strong>
                                                        </Col>
                                                        <Col md={8} lg={8}>
                                                            <Field 
                                                                name="supplierId" 
                                                                type="select" 
                                                                component={SelectBox} 
                                                                placeholder="Select supplier" 
                                                                values={this.state.suppliers} 
                                                                sortBy="name" icon=""/>
                                                        </Col>
                                                    </Row>

                                                    <Row>
                                                        <Col md={4} lg={4} className="label-name">
                                                            <strong>Quantity</strong>
                                                        </Col>
                                                        <Col md={8} lg={8}>
                                                            <Field 
                                                                name="quantity" 
                                                                type="text" 
                                                                component={TextBox} 
                                                                label="Quantity" 
                                                                icon=""/>
                                                        </Col>
                                                    </Row>
                                                </div>
                                                : null
                                            }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={120}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="status" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select status" 
                                                        values={this.state.status} 
                                                        sortBy="name" icon=""/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Save" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => window.history.back()}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_billing_item'));

AddItem = reduxForm({
    form: 'form_add_billing_item',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_code.test(values.itemCode) || values.itemCode === undefined){
            errors.itemCode = 'Invalid item code!';
        }

        if (!regex_name.test(values.itemNameEn) || values.itemNameEn === undefined) {
            errors.itemNameEn = 'Invalid Item English Name!';
        }
        if (values.shortName === undefined) {
            errors.shortName = 'Invalid Item Short Name!';
        }
        if (values.itemType === undefined) {
            errors.itemType = 'Item Type is required!';
        }
        if (values.itemGroupId === undefined) {
            errors.itemGroupId = 'Item Group is required!';
        }
        if (values.categoryId === undefined) {
            errors.categoryId = 'Item Category is required!';
        }
        if (values.unitTypeId === undefined) {
            errors.unitTypeId = 'Unit Type is required!';
        }
        if(values.currency === undefined){
            errors.currency = 'Currency is required!';
        }
        if(!regex_price.test(values.price1) || values.price1 === undefined){
            errors.price1 = 'Invalid Normal Price!';
        }
        if(!regex_price.test(values.price2) || values.price2 === undefined){
            errors.price2 = 'Invalid Foreign Price!';
        }
        if(!regex_price.test(values.price3) || values.price3 === undefined){
            errors.price3 = 'Invalid Business Price!';
        }
        if(values.status === undefined){
            errors.status = 'Status is required!';
        }
        return errors
    }
})(AddItem);

function mapStateToProps(state) {
    return {
        listAllItemGroup: state.itemGroup.listAllItemGroup,
        itemAdd: state.item.itemAdd,
        itemCategoryParent: state.itemCategory.itemCategoryParent,
        itemSubCategoryParent: state.itemCategory.itemSubCategoryParent,
        listAllUnitType: state.unitType.listAllUnitType,
        getSuppliers: state.supplier.getSuppliers,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        listAllItemGroupAction, 
        addItemAction, 
        getItemCategoryByParent,
        listAllUnitTypesAction,
        getSuppliersAction,
        getItemSubCategoryByParent
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddItem);