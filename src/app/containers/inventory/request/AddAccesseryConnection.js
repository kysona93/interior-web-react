import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox ,Label} from 'react-bootstrap';
import { Field, reduxForm, reset,change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from './../../../components/forms/DateTimePicker';

class AddAccessoryConnection extends React.Component{
    constructor(props){
        super(props);
        this.state={
            count: 1,
            hideShowForm: 0,
            requestDate:null,
            checkDate : null,
            approveDate: null
        };
        this.handleApproveDate =this.handleApproveDate.bind(this);
        this.handleCheckDate=this.handleCheckDate.bind(this);
        this.handleRequestDate = this.handleRequestDate.bind(this);
        this.handleRemoveItem = this.handleRemoveItem.bind(this);
        this.handleAddItem = this.handleAddItem.bind(this);
    }

    handleRequestDate(date){ this.setState({ requestDate: date }) }
    handleCheckDate(date){ this.setState({ checkDate: date }) }
    handleApproveDate(date){ this.setState({ approveDate: date }) }
    handleConnection(){
        let dynamicFields = [];
        for(let i = 0; i < this.state.count; i++){
            dynamicFields.push(
                <div key={i}>
                    <Row>
                        <Col md={3} lg={3}>
                            <Field name={"item" + i} type="text" component={TextBox} label="Item"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"unit" + i} type="text" component={TextBox} label="Unit"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"quantity" + i} type="text" component={TextBox} label="Quantity"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"description" + i} type="text" component={TextBox} label="Description"/>
                        </Col>
                    </Row>
                </div>
            )
        }
        return dynamicFields || null;
    }
    handlePlanning(){
        let dynamicFields = [];
        for(let i = 0; i < this.state.count; i++){
            dynamicFields.push(
                <div key={i}>
                    <Row>
                        <Col md={3} lg={3}>
                            <Field name={"itemp" + i} type="text" component={TextBox} label="Item"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"unitp" + i} type="text" component={TextBox} label="Unit"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"quantityp" + i} type="text" component={TextBox} label="Quantity"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"descriptionp" + i} type="text" component={TextBox} label="Description"/>
                        </Col>
                    </Row>
                </div>
            )
        }
        return dynamicFields || null;
    }

    handleAddItem(){
        this.setState({count: this.state.count + 1});
    }
    handleRequestType(event){
        if(event.target.value == 1) this.setState({ hideShowForm: 1});
            else if (event.target.value == 2) this.setState({ hideShowForm:2 });
                else this.setState({ hideShowForm:0})
    }

    handleRemoveItem() {
        this.setState({count: this.state.count - 1});
        this.props.dispatch(change('form_add_accessory_connection_item', 'item' + (this.state.count - 1), ''));
    }
    handleSubmit(values){}
    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add PlanningForm</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>RequestNo <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="year"
                                                        type="text"
                                                        component={TextBox}
                                                        label="requestNo"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>RequestFor <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="year"
                                                        type="text"
                                                        component={TextBox}
                                                        label="requestFor"
                                                    />
                                                </Col>
                                            </Row>
                                            <div className="well planning well-sm">New Planning</div>
                                        </Col>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>PrepareBy <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="prepareBy"
                                                        type="text"
                                                        component={TextBox}
                                                        label="prepareBy"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>CheckBy <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="checkBy"
                                                        type="text"
                                                        component={TextBox}
                                                        label="checkBy"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>ApproveBy <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="approveBy"
                                                        type="text"
                                                        component={TextBox}
                                                        label="approveBy"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="description"
                                                        type="text"
                                                        component={TextArea}
                                                        height={120}
                                                        label="Description"
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>RequestDate<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="requestDate" component={DateTimePicker} placeholder="RequestDate" defaultDate={this.state.requestDate} handleChange={this.handleRequestDate}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>CheckDate<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="checkDate" component={DateTimePicker} placeholder="CheckDate" defaultDate={this.state.checkDate} handleChange={this.handleCheckDate}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>ApproveDate<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="approveDate" component={DateTimePicker} placeholder="ApproveDate" defaultDate={this.state.approveDate} handleChange={this.handleApproveDate}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>ApproveType<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="transformer" type="select"
                                                           component={SelectBox}
                                                           placeholder="Planning Type"
                                                           onChange={this.handleRequestType.bind(this)}
                                                           values={[{id:1,name:"New Connection"},{id:2,name:"Planning"}]} sortBy="name" />
                                                </Col>
                                            </Row><Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Verify Code<span className="label-require">*</span></strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field
                                                    name="verifyCode"
                                                    type="text"
                                                    component={TextBox}
                                                    label="VerifyCode"
                                                />
                                            </Col>
                                        </Row>
                                        </Col>
                                    </Row>
                                    {  this.state.hideShowForm == 1 ?
                                        <div>
                                            <br/>
                                            <div className="well planning well-sm">Calculate Accessory</div>
                                            <Row>
                                                <Col md={12}>
                                                    <div className="">
                                                        <table className="list-smaller-table">
                                                            <thead>
                                                            <tr>
                                                                <th>No</th>
                                                                <th>Id</th>
                                                                <th>Customer</th>
                                                                <th>Pole</th>
                                                                <th>Ampere</th>
                                                                <th>Meter Type</th>
                                                                <th>Meter</th>
                                                                <th>Breaker</th>
                                                                <th>CIU</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                                <td className="text-align-center"></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <br/>
                                            <div className="well planning well-sm">Add Other Accessory</div>
                                            <br/>
                                            <Row>
                                                <Col md={12} lg={12}>
                                                    {this.handleConnection()}
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={1} lg={1}>
                                                    <Button bsStyle="primary" onClick={this.handleAddItem}>Add Item</Button>
                                                </Col>
                                                <Col md={1} lg={1}>
                                                    <Button bsStyle="primary" onClick={this.handleRemoveItem}>Remove</Button>
                                                </Col>
                                            </Row>
                                        </div>
                                        :
                                        this.state.hideShowForm == 2 ?
                                      <div>
                                          <br/>
                                          <div className="well planning well-sm">List Accessory</div>
                                          <br/>
                                          <Row>
                                              <Col md={12} lg={12}>
                                                  {this.handlePlanning()}
                                              </Col>
                                          </Row>
                                          <Row>
                                              <Col md={1} lg={1}>
                                                  <Button bsStyle="primary" onClick={this.handleAddItem}>Add Item</Button>
                                              </Col>
                                              <Col md={1} lg={1}>
                                                  <Button bsStyle="primary" onClick={this.handleRemoveItem}>Remove</Button>
                                              </Col>
                                          </Row>
                                      </div>

                                       :
                                            null

                                    }


                                    <br/>
                                    <Row>
                                        <Col mdOffset={8} sm={2} md={2}>
                                            <ButtonSubmit
                                                error={error}
                                                invalid={invalid}
                                                submitting={submitting}
                                                label="Save" />
                                        </Col>
                                        <Col sm={2} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block"
                                                    onClick={() => window.history.back()}>
                                                AddAccessory
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}
AddAccessoryConnection = reduxForm({
    form: 'form_add_accessory_connection_item',
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};

        return errors
    }
})(AddAccessoryConnection);

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddAccessoryConnection);