import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, Checkbox ,Label} from 'react-bootstrap';
import { Field, reduxForm, reset, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import AsyncTypeaheadFilter from './../../../components/forms/AsyncTypeahead';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from './../../../components/forms/DateTimePicker';
import { authObj } from './../../../../auth';
import { getPlanningsApprovedAction } from './../../../actions/inventory/planning';
import { updateRequestAction } from './../../../actions/inventory/request';
import moment from 'moment';

let items = [];
let itemsAll = [];
let verifyNos = [];

class VerifyRequest extends React.Component{
    constructor(props){
        super(props);
        this.state={
            requestDate: this.props.object.requestDate !== null ? moment(this.props.object.requestDate) : null,
            checkedDate: this.props.object.checkedDate !== null ? moment(this.props.object.checkedDate) : null,
            verifiedDate: this.props.object.verifiedDate !== null ? moment(this.props.object.verifiedDate) : null,
            approvedDate: this.props.object.approvedDate !== null ? moment(this.props.object.approvedDate) : null,
            requestType: [
                { id: "NEW_CONNECTION", name: "New Connection" },
                { id: "OTHER_INCOME", name: "Other Income" },
                { id: "PLANNING", name: "New Planning" },
                { id: "ADD_NEW", name: "Add New" }
            ],
            options: [],
            itemsAll: [],
            verifyNos: [],
            verifyArray: [],
            isTypeAddNew: false,
            items: [],
            verifyNo: "",
            requestTypeSelected: "",
            defaultSelected: [],
            requestId: 0,
            accessories: []
        };
        this.handleApprovedDate =this.handleApprovedDate.bind(this);
        this.handleCheckedDate=this.handleCheckedDate.bind(this);
        this.handleVerifiedDate = this.handleVerifiedDate.bind(this);
    }

    componentWillMount() {
        
        this.props.dispatch(initialize('form_verify_request', this.props.object));
        this.props.dispatch(change('form_verify_request', 'verifiedBy', authObj().sub));

        if (this.props.object.requestType === 'PLANNING')
            this.props.getPlanningsApprovedAction();
        else if(this.props.object.requestType === 'ADD_NEW') 
            this.setState({ isTypeAddNew: true });

        this.setState({ 
            requestTypeSelected: this.props.object.requestType,
            verifyNo: this.props.object.verifyNo,
            requestId: this.props.object.id
         });

        if(this.props.object.requestItemDetails.length !== 0) {
            this.setState({ 
                accessories: this.props.object.requestItemDetails
            });
        }
    }

    componentWillReceiveProps(newProps){
        // verifyNos
        if(newProps.planningsApproved !== undefined){
            let items = newProps.planningsApproved.items;
            if (items !== undefined) {
                if (items.length === verifyNos.length){
                    this.setState({verifyNos : verifyNos});
                } else {
                    items.forEach((element) => {
                        verifyNos.push({
                            "id": element.id,
                            "name": element.planningNo
                        });
                    });

                    // find match verify no edit.
                    const item = verifyNos.find(i => i.name === this.props.object.verifyNo);

                    this.setState({
                        verifyNos : verifyNos,
                        verifyArray: items,
                        defaultSelected: [item]
                    });
                }
            }
        }

        // update request
        if(newProps.requestUpdate.status === 200){
            if(newProps.requestUpdate.data.status === 200){
                alert("Successfully updated request.");
                this.props.handleHideShowEdit('');
                newProps.requestUpdate.data.status = 0;
            } else {
                alert("Fail with update request!");
                newProps.requestUpdate.data.status = 0;
            }
            newProps.requestUpdate.status = 0;
        }
        if(newProps.requestUpdate.status === 404 || newProps.requestUpdate.status === 500){
            alert("Fail with update request!");
            newProps.requestUpdate.status = 0;
        }
    }

    componentWillUnmount() {
        items = [];
        itemsAll = [];
        verifyNos = [];
        this.setState({ items: [], itemsAll: [] });
    }

    handleClearAccessory() {
        items = [];
        this.setState({ 
            items: [] ,
            verifyNo: ""
        });
    }

    handleCheckedDate(date){ this.setState({ checkedDate: date }) };
    handleVerifiedDate(date){ this.setState({ verifiedDate: date }) };
    handleApprovedDate(date){ this.setState({ approvedDate: date }) };

    handleSubmit(values){
        

        if(this.state.accessories.length !== 0) {

            let request = {
                id: this.state.requestId,
                verifiedBy: values.verifiedBy,
                verifiedDate: values.verifiedDate
            }

            this.props.updateRequestAction(request);
        } else {
            alert('Error: Accessories no values, please update your accessories before verified requesting!');
            this.props.handleHideShowEdit('');
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Edit Request</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request No <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestNo"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Request No"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request Type<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestType"
                                                        type="select"
                                                        component={SelectBox}
                                                        placeholder="Request Type"
                                                        disabled={true}
                                                        values={this.state.requestType} sortBy="name" />
                                                </Col>
                                            </Row>
                                            { this.state.isTypeAddNew === false ? 
                                                <Row>
                                                    <Col md={4} lg={4} className="label-name">
                                                        <strong>Verify No<span className="label-require">*</span></strong>
                                                    </Col>
                                                    <Col md={8} lg={8}>
                                                        <Field
                                                            name="verifyNo"
                                                            type="select"
                                                            component={AsyncTypeaheadFilter}
                                                            labelKey="name"
                                                            placeholder="Search ..."
                                                            disabled={true}
                                                            defaultSelected={this.state.defaultSelected[0] !== undefined ? this.state.defaultSelected : []}
                                                            values={this.state.verifyNos} />
                                                    </Col>
                                                </Row>
                                            : null }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    { this.props.object.requestStatus === "NEW_REQUEST" || this.props.object.requestStatus === null ?
                                                        <h4><Label bsStyle="primary">New Request</Label></h4> 
                                                        : null 
                                                    }
                                                    { this.props.object.requestStatus === "REQUESTING" ? <h4><Label bsStyle="info">Requesting</Label></h4> : null }
                                                    { this.props.object.requestStatus === "COMPLETE" ? <h4><Label bsStyle="success">Complete</Label></h4> : null }
                                                    { this.props.object.requestStatus === "CANCEL" ? <h4><Label bsStyle="danger">Cancel</Label></h4> : null }
                                                </Col>
                                            </Row>

                                        </Col>

                                        <Col md={8} lg={8} >
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Description<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field
                                                        name="description"
                                                        type="text"
                                                        component={TextArea}
                                                        height={80}
                                                        label="Description"
                                                        disabled={true}
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared By <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="requestBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Prepared By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Request Date<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="requestDate"
                                                        component={DateTimePicker}
                                                        placeholder="Request Date"
                                                        disabled={true}
                                                        defaultDate={this.state.requestDate}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="checkedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Checked By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="checkedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Checked Date"
                                                        disabled={true}
                                                        defaultDate={this.state.checkedDate}
                                                        handleChange={this.handleCheckedDate}/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Verify By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="verifiedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Verify By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Verify Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="verifiedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Verify Date"
                                                        defaultDate={this.state.verifiedDate}
                                                        handleChange={this.handleVerifiedDate}/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="approvedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Approved By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="approvedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Approved Date"
                                                        disabled={true}
                                                        defaultDate={this.state.approveDate}
                                                        handleChange={this.handleApprovedDate}/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row>
                                        <Col md={12}>
                                            <br/>
                                            <div className="well planning well-sm">List Accessory</div>
                                            <br/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={10}>
                                            <table className="list-smaller-table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Item Name</th>
                                                        <th>Unit Type</th>
                                                        <th>Quantity</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.accessories.length !== 0 ?
                                                        this.state.accessories.map((q, index)=>{
                                                            return(
                                                                <tr key={index}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{q.item.itemNameEn}{' '}{q.item.itemNameKh}</td>
                                                                    <td>{q.item.unitType !== null ? q.item.unitType.unitType : "Unit"}</td>
                                                                    <td>{q.quantity}</td>
                                                                </tr>
                                                            )
                                                        })
                                                    :
                                                    <tr>
                                                        <td colSpan={6} className="text-align-center">RESULT NOT FOUND</td>
                                                    </tr>
                                                }
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>

                                    <br/>
                                    <Row>
                                        <Col mdOffset={6} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Save" />

                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.props.handleHideShowEdit('')}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>  
                </Row>
            </div>
        )
    }

}
VerifyRequest = reduxForm({
    form: 'form_verify_request',
    validate: (values) => {

        const errors = {};

        if (values.verifiedDate === "" || values.verifiedDate === null) {
            errors.verifiedDate = 'Verified Date is required!';
        }

        return errors
    }
})(VerifyRequest);

function mapStateToProps(state) {
    return {
        planningsApproved: state.planning.planningsApproved,
        requestUpdate: state.request.requestUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getPlanningsApprovedAction,
        updateRequestAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(VerifyRequest);