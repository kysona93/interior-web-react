import React from 'react';
import './report.css';

class Report extends React.Component{

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-align-center"><h2>Project BVC-EDC Preah Vihea</h2></div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-align-center"><h3>PURECHASE REQUESTED</h3></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <span><p>#56, St 337 Boeung II Quarter,</p></span>
                        <span><p>Dualkok District, Phnom Penh</p></span>
                        <span><p>Kingdom of Cambodia</p></span>
                        <span><p>Tel: 077 54 54 54</p></span>
                        <span>     <p>011 63 61 51</p></span>

                    </div>
                    <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <span><p>Request No: PVH17</p></span>
                        <span><p>Date: ....................................</p></span>
                        <span><p>Project: BVC-EDC Preah Vihea</p></span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-md-12 col-lg-12">
                        <table className="table report table-bordered">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Item/Description</th>
                                <th>Unit</th>
                                <th>Qty</th>
                                <th>Remark</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td className="text-align-center" colSpan ={3}>Total</td>
                                <td>100</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <span><p>Resion for Purchase Request ....................................................................................................................................................................................................................................................
                            ....................................................................................................................................................................................................................................................................................................
                            ....................................................................................................................................................................................................................................................................................................
                            ....................................................................................................................................................................................................................................................................................................
                        </p></span>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <span><p>Requested By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name: Sovan Dara</p></span>
                        <span><p>Date: ...........</p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <span><p>Indorsor By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name: Sovan Dara</p></span>
                        <span><p>Date: ...........</p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <span><p>Verified By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name: </p></span>
                        <span><p>Date: </p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <span><p>Apporval By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name: </p></span>
                        <span><p>Date: </p></span>
                    </div>
                </div>
            </div>
        )
    }

}
export default Report;