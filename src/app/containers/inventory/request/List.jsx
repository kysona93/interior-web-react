import React from 'react';
import {connect} from 'react-redux';
import { browserHistory } from 'react-router';
import { Row, Col, Button, Checkbox ,Label} from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { canCreate, canUpdate, canDelete } from './../../../../auth';
import { getRequestsAction } from './../../../actions/inventory/request';
import moment from 'moment';
import EditRequest from './Edit';
import CheckRequest from './Check';
import VerifyRequest from './Verify';
import ApproveRequest from './Approve';

let filter = {
    year: "",
    month: 1,
    type: "",
    status: ""
}

class ControlRequest extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            months: [
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ],
            requestType: [
                { id: "NEW_CONNECTION", name: "New Connection" },
                { id: "OTHER_INCOME", name: "Other Income" },
                { id: "PLANNING", name: "New Planning" },
                { id: "ADD_NEW", name: "Add New" }
            ],
            requestStatus: [
                { id: "NEW_REQUEST", name: "New Request" },
                { id: "REQUESTING", name: "Requesting" },
                { id: "COMPLETE", name: "Complete" },
                { id: "CANCEL", name: "Cancel" }
            ],
            object: {},
            formName: "",
        }
    }

    componentWillMount() {
        this.props.getRequestsAction(filter);
    }

    handleRenderForm(object, formName) {
        this.setState({ 
            object: object,
            formName: formName
        });
    }
    
    renderForm() {
        if(this.state.formName === 'edit') {
            return(
                <EditRequest object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        } else if(this.state.formName === 'check') {
            return(
                <CheckRequest object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        } else if(this.state.formName === 'verify') {
            return(
                <VerifyRequest object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        } else if(this.state.formName === 'approve') {
            return(
                <ApproveRequest object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        }
    }
    
    handleHideShowEdit(value) {
        this.setState({ formName: value });
        this.props.getRequestsAction(filter);
    }

    handleSubmit(values){
        if(values.year === undefined) filter.year = "";
        else filter.year = values.year;

        if(values.month === undefined) filter.month = 1;
        else filter.month = values.month;

        if(values.type === undefined) filter.type = "";
        else filter.type = values.type;

        if(values.status === undefined) filter.status = "";
        else filter.status = values.status;
        
        this.props.getRequestsAction(filter);
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            this.state.formName == "" ?
                <div className="margin_left_25 margin_top_minus_30">
                    <Row>
                        <h3 className="text-align-center">List Requests</h3>
                    </Row>
                    <Row>
                        <Col md={3}>
                            {
                                canCreate === false ?
                                    <Button
                                        onClick={() => browserHistory.push('/app/inventory/requests/add')}
                                        className="btn btn-default">
                                        <i className="fa fa-plus" />
                                        {' '}Add New Request
                                    </Button>
                                    : null
                            }
                        </Col>
                        <Col md={9}>
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={5} lg={5} >
                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Year</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field
                                                    name="year"
                                                    type="text"
                                                    component={TextBox}
                                                    label="year"
                                                    />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={5} lg={5} >
                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Month</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field
                                                    name="month"
                                                    type="select"
                                                    component={SelectBox}
                                                    placeholder="Select Month"
                                                    values={this.state.months} />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={5} lg={5} >
                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Request Type</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field 
                                                    name="type" 
                                                    type="select" 
                                                    component={SelectBox} 
                                                    placeholder="Request Type"
                                                    values={this.state.requestType} />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={5} lg={5} >
                                        <Row>
                                            <Col md={4} lg={4} className="label-name">
                                                <strong>Request Status</strong>
                                            </Col>
                                            <Col md={8} lg={8}>
                                                <Field 
                                                    name="status" 
                                                    type="select" 
                                                    component={SelectBox} 
                                                    placeholder="Request Type"
                                                    values={this.state.requestStatus} />
                                            </Col>
                                        </Row>
                                    </Col>
                                    <Col md={2}>
                                        <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH"/>
                                    </Col>
                                </Row>
                            </form>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={12}>
                            <div className="">
                                <table className="list-smaller-table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Request No</th>
                                        <th>Request For</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Checked Date</th>
                                        <th>Verify Date</th>
                                        <th>Approve Date</th>
                                        <th>Status</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.requests.status === 200 ?
                                            this.props.requests.data.items.map((request ,index) => {
                                                return(
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" onClick={() => this.handleRenderForm(request, 'edit')}>
                                                                {request.requestNo}
                                                            </a>
                                                        </td>
                                                        <td>
                                                            { request.requestType === "NEW_CONNECTION" ? "New Connection" : null }
                                                            { request.requestType === "OTHER_INCOME" ? "Other Income" : null }
                                                            { request.requestType === "PLANNING" ? "Planning" : null }
                                                            { request.requestType === "ADD_NEW" ? "Add New Request" : null }
                                                        </td>
                                                        <td>{request.description}</td>
                                                        <td>{moment(request.requestDate).format('L')}</td>
                                                        <td>
                                                            {request.checkedDate === null ? 
                                                                <button type="button" className='btn btn-default btn-xs' onClick={() => this.handleRenderForm(request, 'check')}>
                                                                    Checked
                                                                </button> 
                                                                : moment(request.checkedDate).format('L')
                                                            }
                                                        </td>
                                                        <td>
                                                            {request.verifiedDate === null ? 
                                                                <button type="button" disabled={request.checkedDate == null ? true : false} className='btn btn-default btn-xs' onClick={() => this.handleRenderForm(request, 'verify')}>
                                                                    Verified
                                                                </button> 
                                                                : moment(request.verifiedDate).format('L')
                                                            }
                                                        </td>
                                                        <td>
                                                            {request.approvedDate === null ? 
                                                                <button type="button" disabled={request.verifiedDate == null ? true : false} className='btn btn-default btn-xs' onClick={() => this.handleRenderForm(request, 'approve')}>
                                                                    Approved
                                                                </button> 
                                                                : moment(request.approvedDate).format('L')
                                                            }
                                                        </td>
                                                        <td>
                                                            { request.requestStatus === "NEW_REQUEST" || request.requestStatus === null ? 
                                                                <Label bsStyle="primary">New Request</Label> 
                                                                : null 
                                                            }
                                                            { request.requestStatus === "REQUESTING" ? <Label bsStyle="info">Processing</Label> : null }
                                                            { request.requestStatus === "COMPLETE" ? <Label bsStyle="success">Complete</Label> : null }
                                                            { request.requestStatus === "CANCEL" ? <Label bsStyle="danger">Cancel</Label> : null }
                                                        </td>
                                                        <td className="text-center">
                                                            {
                                                                canUpdate === false ?
                                                                    <a className='btn btn-info btn-xs' onClick={() => this.handleRenderForm(request, 'edit')}>
                                                                        <span className="glyphicon glyphicon-edit"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                            {
                                                                canDelete === false ?
                                                                    <a onClick={()=> this.handleDeleteRow(request.id)} className="btn btn-danger btn-xs">
                                                                        <span className="glyphicon glyphicon-remove"></span>
                                                                    </a>
                                                                : null
                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        :
                                        <tr>
                                            <td colSpan={11} className="text-align-center">RESULT NOT FOUND</td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            : 
            this.renderForm()
        )
    }

}
ControlRequest = reduxForm({
    form: 'form_add_billing_item',
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};

        return errors
    }
})(ControlRequest);

function mapStateToProps(state) {
    // console.log(state.request.requests)
    return {
        requests: state.request.requests
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getRequestsAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ControlRequest);