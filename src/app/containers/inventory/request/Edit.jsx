import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, Checkbox ,Label} from 'react-bootstrap';
import { Field, reduxForm, reset, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import AsyncTypeaheadFilter from './../../../components/forms/AsyncTypeahead';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from './../../../components/forms/DateTimePicker';
import { authObj } from './../../../../auth';
import { getAllItemsAction } from './../../../actions/inventory/item';
import { getPlanningsApprovedAction } from './../../../actions/inventory/planning';
import { updateRequestAction } from './../../../actions/inventory/request';
import moment from 'moment';

let items = [];
let accessory = {
    itemId: 0,
    quantity: 0,
    price: 0
};
let accessories = [];
let itemsAll = [];
let verifyNos = [];

class EditRequest extends React.Component{
    constructor(props){
        super(props);
        this.state={
            requestDate: this.props.object.requestDate !== null ? moment(this.props.object.requestDate) : null,
            checkedDate: this.props.object.checkedDate !== null ? moment(this.props.object.checkedDate) : null,
            verifiedDate: this.props.object.verifiedDate !== null ? moment(this.props.object.verifiedDate) : null,
            approvedDate: this.props.object.approvedDate !== null ? moment(this.props.object.approvedDate) : null,
            requestType: [
                { id: "NEW_CONNECTION", name: "New Connection" },
                { id: "OTHER_INCOME", name: "Other Income" },
                { id: "PLANNING", name: "New Planning" },
                { id: "ADD_NEW", name: "Add New" }
            ],
            options: [],
            itemsAll: [],
            verifyNos: [],
            verifyArray: [],
            isTypeAddNew: false,
            items: [],
            verifyNo: "",
            requestTypeSelected: "",
            defaultSelected: [],
            renderDefaultSelected: true,
            requestId: 0
        };
        this.handleApproveDate =this.handleApproveDate.bind(this);
        this.handleCheckDate=this.handleCheckDate.bind(this);
        this.handleRequestDate = this.handleRequestDate.bind(this);
    }

    componentWillMount() {
        this.props.getAllItemsAction('');

        this.props.dispatch(initialize('form_edit_request', this.props.object));

        if (this.props.object.requestType === 'PLANNING')
            this.props.getPlanningsApprovedAction();
        else if(this.props.object.requestType === 'ADD_NEW') 
            this.setState({ isTypeAddNew: true });

        this.setState({ 
            requestTypeSelected: this.props.object.requestType,
            verifyNo: this.props.object.verifyNo,
            requestId: this.props.object.id
         });

        if(this.props.object.requestItemDetails.length !== 0) {
            let q = this.props.object.requestItemDetails;
            let qitems = [];
            for(let i=0; i<q.length; i++) {
                qitems.push(q[i].item);

                accessory = {
                    itemId: q[i].item.id,
                    quantity: q[i].quantity,
                    unitType: q[i].unitType,
                    currency: q[i].currency,
                    price: q[i].price
                };
                accessories.push(accessory);

                this.props.dispatch(change('form_edit_request', 'quantity' + i, q[i].quantity));
            }
            items = qitems;
            this.setState({ items: qitems });
        }

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    }

    componentWillReceiveProps(newProps){
        //itemsAll
        if(newProps.itemsAll !== undefined){
            let items = newProps.itemsAll.items;
            if (items !== undefined) {
                if (items.length === itemsAll.length){
                    this.setState({itemsAll : itemsAll});
                } else {
                    items.forEach((element) => {
                        itemsAll.push({
                            "id": element.id,
                            "name": element.itemNameEn
                        });
                    });
                    this.setState({itemsAll : itemsAll});
                }
            }
        }

        // verifyNos
        if(newProps.planningsApproved !== undefined){
            let items = newProps.planningsApproved.items;
            if (items !== undefined) {
                if (items.length === verifyNos.length){
                    this.setState({verifyNos : verifyNos});
                } else {
                    items.forEach((element) => {
                        verifyNos.push({
                            "id": element.id,
                            "name": element.planningNo
                        });
                    });

                    // find match verify no edit.
                    const item = verifyNos.find(i => i.name === this.props.object.verifyNo);

                    this.setState({
                        verifyNos : verifyNos,
                        verifyArray: items,
                        defaultSelected: [item],
                        renderDefaultSelected: false
                    });
                }
            }
        }

        // update request
        if(newProps.requestUpdate.status === 200){
            if(newProps.requestUpdate.data.status === 200){
                alert("Successfully updated request.");
                this.props.handleHideShowEdit('');
                newProps.requestUpdate.data.status = 0;
            } else {
                alert("Fail with update request!");
                newProps.requestUpdate.data.status = 0;
            }
            newProps.requestUpdate.status = 0;
        }
        if(newProps.requestUpdate.status === 404 || newProps.requestUpdate.status === 500){
            alert("Fail with update request!");
            newProps.requestUpdate.status = 0;
        }
    }

    componentWillUnmount() {
        items = [];
        accessories = [];
        itemsAll = [];
        verifyNos = [];
        this.setState({ items: [], itemsAll: [] });
    }

    handleClearAccessory() {
        items = [];
        accessories = [];
        this.setState({ 
            items: [] ,
            verifyNo: ""
        });
    }

    handleRequestDate(date){ this.setState({ requestDate: date }) }
    handleCheckDate(date){ this.setState({ checkDate: date }) }
    handleApproveDate(date){ this.setState({ approveDate: date }) }

    handleRequestType(event){
        this.setState({
            isTypeAddNew: false,
            requestTypeSelected: event.target.value,
            defaultSelected: []
         });
         this.handleClearAccessory();
        
        if (event.target.value === 'PLANNING')
            this.props.getPlanningsApprovedAction();
        // else if(event.target.value === 'NEW_CONNECTION')
        // else if(event.target.value === 'OTHER_INCOME')
        else if(event.target.value === 'ADD_NEW') 
            this.setState({ isTypeAddNew: true });
    }

    handleSelectVerifyNo(id) {
        if(this.state.renderDefaultSelected === true) {
            if(this.state.verifyArray !== undefined){
                const item = this.state.verifyArray.find(i => i.id === Number(id));
                if(item !== undefined) {
                    if(this.state.requestTypeSelected === "PLANNING") {
                        // clear accessory before select verify no.
                        this.handleClearAccessory();
    
                        this.setState({ verifyNo: item.planningNo });
    
                        if(item.quotations.length !== 0) {
                            let q = item.quotations;
                            let qitems = [];
                            for(let i=0; i<q.length; i++) {
                                qitems.push(q[i].item);
    
                                accessory = {
                                    itemId: q[i].item.id,
                                    quantity: q[i].quantity,
                                    unitType: q[i].unitType,
                                    currency: q[i].currency,
                                    price: q[i].price
                                };
                                accessories.push(accessory);
                                this.props.dispatch(change('form_edit_request', 'quantity' + i, q[i].quantity));
                            }
                            items = qitems;
                            this.setState({ items: qitems });
                        } 
                    }
                } else
                    this.handleClearAccessory();
            } 
        } else 
            this.setState({ renderDefaultSelected: true});
    }

    renderAddNewItem() {
        return(
            <Row>
                <Col md={5} lg={5}>
                    <Field
                        name="item"
                        type="select"
                        component={AsyncTypeaheadFilter}
                        handleOnChange={selected => { this.handleSelectItem(_.map(selected, "id")) }}
                        handleRef={ref => this._typeahead = ref}
                        labelKey="name"
                        placeholder="Please select item ..."
                        values={this.state.itemsAll}/>
                </Col>
                <Col md={1} lg={1}>
                    <Link 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Add New Item" 
                        className="btn btn-default" 
                        to="/app/inventory/items/add">
                        <i className="fa fa-plus-circle" />
                    </Link>
                </Col>
                <Col md={4} lg={4} className="label-name">
                    <span style={{display: 'none'}} id="errorMessage" className="label-require">Item has been selected!</span>
                </Col>
                <Col md={2} lg={2} className="label-name">
                    <Link 
                        className="btn btn-default" 
                        onClick={() => setTimeout(() => {window.open(`/print-planning/${this.props.object.id}`).print()}, 1000)} >
                        <i className="fa fa-print"/>
                        {' '}Printing
                    </Link>
                </Col>
            </Row>
        );
    }

    
    handleSelectItem(id) {
        if(this.props.itemsAll.items !== undefined){
            const item = this.props.itemsAll.items.find(i => i.id === Number(id));

            if(item !== undefined) {
                if(items.find(i => i.id === Number(id)) === undefined) {
                    items.push(item);
                    this.setState({ items: items });
                    accessory = {
                        itemId: item.id,
                        quantity: 1,
                        unitType: item.unitType.unitType,
                        currency: item.currency,
                        price: item.cost
                    };
                    accessories.push(accessory);
                    this.props.dispatch(change('form_edit_request', 'quantity' + items.indexOf(item), 1));
                    setTimeout(() => this._typeahead.getInstance().clear(), 0);
                } else {
                    document.getElementById('errorMessage').style.display = 'block';
                    setTimeout(() => this._typeahead.getInstance().clear(), 0);
                    setTimeout(() => document.getElementById('errorMessage').style.display = 'none', 2000);
                }
            }
        }
    }
    
    handleRemoveRow(item) {
        let index = items.indexOf(item);
        if (index > -1) {
            items.splice(index, 1);

            // dispatch item text fields value again remove.
            accessories.splice(index, 1);

            for(let i=0; i<accessories.length; i++) {
                this.props.dispatch(change('form_edit_request', 'quantity' + i, accessories[i].quantity));
            }
        }
        this.setState({ items: items });
    }

    handleSetQuantity(id, e) {
        accessory = accessories.find(q => q.itemId === Number(id));
        accessory.quantity = Number(e.target.value);
        accessories.map(q => q.itemId === Number(id) ? accessory : q)
    }

    handleSubmit(values){
        let request = {
            id: this.state.requestId,
            description: values.description,
            requestType: values.requestType,
            verifyNo: this.state.verifyNo,
            requestItemDetailForms: accessories
        }

        if (request.requestItemDetailForms.length !== 0) {
            if (this.state.verifyNo === "") {
                if (this.state.requestTypeSelected === "ADD_NEW")
                    this.props.updateRequestAction(request);
                else
                    alert("Failed add request! please check your verify no before submit.");
            } else
                this.props.updateRequestAction(request);
        } else
            alert("Failed add request! please add accessories before submit.");
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Edit Request</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request No <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestNo"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Request No"
                                                    />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request Type<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestType"
                                                        type="select"
                                                        component={SelectBox}
                                                        placeholder="Request Type"
                                                        onChange={this.handleRequestType.bind(this)}
                                                        values={this.state.requestType} sortBy="name" />
                                                </Col>
                                            </Row>
                                            { this.state.isTypeAddNew === false ? 
                                                <Row>
                                                    <Col md={4} lg={4} className="label-name">
                                                        <strong>Verify No<span className="label-require">*</span></strong>
                                                    </Col>
                                                    <Col md={8} lg={8}>
                                                        <Field
                                                            name="verifyNo"
                                                            type="select"
                                                            component={AsyncTypeaheadFilter}
                                                            handleOnChange={ selected => { this.handleSelectVerifyNo(_.map(selected, "id"))} }
                                                            labelKey="name"
                                                            placeholder="Search ..."
                                                            defaultSelected={this.state.defaultSelected[0] !== undefined ? this.state.defaultSelected : []}
                                                            values={this.state.verifyNos} />
                                                    </Col>
                                                </Row>
                                            : null }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    { this.props.object.requestStatus === "NEW_REQUEST" || this.props.object.requestStatus === null ?
                                                        <h4><Label bsStyle="primary">New Request</Label></h4> 
                                                        : null 
                                                    }
                                                    { this.props.object.requestStatus === "REQUESTING" ? <h4><Label bsStyle="info">Requesting</Label></h4> : null }
                                                    { this.props.object.requestStatus === "COMPLETE" ? <h4><Label bsStyle="success">Complete</Label></h4> : null }
                                                    { this.props.object.requestStatus === "CANCEL" ? <h4><Label bsStyle="danger">Cancel</Label></h4> : null }
                                                </Col>
                                            </Row>

                                        </Col>

                                        <Col md={8} lg={8} >
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Description<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field
                                                        name="description"
                                                        type="text"
                                                        component={TextArea}
                                                        height={80}
                                                        label="Description"
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared By <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="requestBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Prepared By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Request Date<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="requestDate"
                                                        component={DateTimePicker}
                                                        placeholder="Request Date"
                                                        disabled={true}
                                                        defaultDate={this.state.requestDate}
                                                        handleChange={this.handleRequestDate}/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="checkedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Checked By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="checkedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Checked Date"
                                                        disabled={true}
                                                        defaultDate={this.state.checkedDate}
                                                        handleChange={this.handleCheckDate}/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Verify By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="verifiedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Verify By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Verify Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="verifiedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Verify Date"
                                                        disabled={true}
                                                        defaultDate={this.state.verifiedDate}
                                                        handleChange={this.handleCheckDate}/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved By</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="approvedBy"
                                                        type="text"
                                                        component={TextBox}
                                                        disabled={true}
                                                        label="Approved By"
                                                    />
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field
                                                        name="approvedDate"
                                                        component={DateTimePicker}
                                                        placeholder="Approved Date"
                                                        disabled={true}
                                                        defaultDate={this.state.approveDate}
                                                        handleChange={this.handleApproveDate}/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    
                                    <Row>
                                        <Col md={12}>
                                            <br/>
                                            <div className="well planning well-sm">Add Accessory</div>
                                            <br/>
                                            { this.renderAddNewItem() }
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={10}>
                                            <table className="list-smaller-table">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Item Name</th>
                                                        <th>Unit Type</th>
                                                        <th>Quantity</th>
                                                        <th className="text-align-center"><i className="fa fa-trash-o" /></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.items.length !== 0 ?
                                                        this.state.items.map((item, index)=>{
                                                            return(
                                                                <tr key={index}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{item.itemNameEn}{' '}{item.itemNameKh}</td>
                                                                    <td>{item.unitType !== null ? item.unitType.unitType : "Unit"}</td>
                                                                    <td className="form-inline">
                                                                        <Field 
                                                                            name={"quantity" + index} 
                                                                            type="text" 
                                                                            component={TextBox} 
                                                                            onChange={(e) => this.handleSetQuantity(item.id, e)}
                                                                            label="Quantity"/>
                                                                    </td>
                                                                    <td>
                                                                        <a onClick={()=> this.handleRemoveRow(item)} className="btn btn-danger btn-xs">
                                                                            <span className="glyphicon glyphicon-minus"></span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                    : null
                                                }
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>

                                    <br/>
                                    <Row>
                                        <Col mdOffset={6} sm={3} md={2}>
                                        { this.props.object.approvedDate === null && 
                                            (this.props.object.requestStatus !== "CANCEL" || this.props.object.requestStatus === null) ?
                                                <ButtonSubmit 
                                                    error={error} 
                                                    invalid={invalid} 
                                                    submitting={submitting} 
                                                    label="Save" />
                                            : null
                                        }
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.props.handleHideShowEdit('')}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>  
                </Row>
            </div>
        )
    }

}
EditRequest = reduxForm({
    form: 'form_edit_request',
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};

        if (values.requestType === undefined) {
            errors.requestType = 'Request Type is required!';
        }
        if (values.requestDate === undefined) {
            errors.requestDate = 'Request Date is required!';
        }
        if (values.description === undefined) {
            errors.description = 'Description is required!';
        }

        return errors
    }
})(EditRequest);

function mapStateToProps(state) {
    return {
        itemsAll: state.item.itemsAll,
        planningsApproved: state.planning.planningsApproved,
        requestUpdate: state.request.requestUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllItemsAction,
        getPlanningsApprovedAction,
        updateRequestAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditRequest);