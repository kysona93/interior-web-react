import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox ,Label} from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';

class ControlRequest extends React.Component{
    constructor(props){
        super(props);
    }

    handleSubmit(values){}

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add Request</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Year <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="year"
                                                        type="text"
                                                        component={TextBox}
                                                        label="year"
                                                        />
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Month <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="month"
                                                        type="text"
                                                        component={TextBox}
                                                        label="month"
                                                        />
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request From<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestFrom"
                                                        type="text"
                                                        component={TextBox}
                                                        label="requestFrom"
                                                        />
                                                </Col>
                                            </Row>
                                        </Col>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Request Type<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field
                                                        name="requestType"
                                                        type="text"
                                                        component={TextBox}
                                                        label="requestType"
                                                        />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12}>
                                            <div className="">
                                                <table className="list-smaller-table">
                                                    <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Id</th>
                                                        <th>Request No</th>
                                                        <th>Request For</th>
                                                        <th>Request Date</th>
                                                        <th>Checked Date</th>
                                                        <th>Verify Date</th>
                                                        <th>Approve Date</th>
                                                        <th>Status</th>
                                                        <th className="text-center">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td className="text-align-center"><Label bsStyle="primary">Check</Label></td>
                                                            <td className="text-align-center"><Label bsStyle="success">Verifi</Label></td>
                                                            <td className="text-align-center"><Label bsStyle="warning">Approve</Label></td>
                                                            <td></td>
                                                            <td className="text-center">
                                                                <a className='btn btn-info btn-xs'>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </a>
                                                                <a  className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col mdOffset={10} sm={2} md={2}>
                                            <ButtonSubmit
                                                error={error}
                                                invalid={invalid}
                                                submitting={submitting}
                                                label="AddNew" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}
ControlRequest = reduxForm({
    form: 'form_add_billing_item',
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};

        return errors
    }
})(ControlRequest);

function mapStateToProps(state) {
    return {
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ControlRequest);