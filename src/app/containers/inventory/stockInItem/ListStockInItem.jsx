import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { Row, Col, Pagination } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { listAllBillingItemsAction, deleteBillingItemAction } from '../../../actions/inventory/item';
import { listAllItemGroupAction } from './../../../actions/inventory/itemGroup';

let arrItemGroup = [];
let billing = {
    limit: 10,
    page: 1,
    groupId: 0,
    name: '',
    status: 1
};
class ListStockInItem extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activePage: 1,
            status: [{id: 1, name: "INSTOCK"},{id: 2, name: "REPAIRED"}, {id: 3, name: "PROBLEM"}],
            itemGroups: []
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
    }

    componentWillMount(){
        this.props.listAllBillingItemsAction(billing);
    }

    componentWillReceiveProps(data){
        if(data.deleteBillingItem.status === 200){
            alert("Successfully deleted this billing item.");
            billing.groupId = 0;
            billing.name = "";
            billing.status = 1;
            this.props.listAllBillingItemsAction(billing);
            data.deleteBillingItem.status = 0;
        }
        if(data.deleteBillingItem.status === 400 || data.deleteBillingItem.status === 500){
            alert("Fail with delete this billing item!");
        }
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }

    handleSelect(eventKey) {
        this.setState({
            activePage: eventKey
        });
        billing.page = eventKey;
        this.props.listAllBillingItemsAction(billing);
    }

    static handleItem(total) {
        if (total <= 10) {
            return 1
        } else if (total % 10 == 0) {
            return total / 10
        } else if (total % 10 > 0) {
            return parseInt(total/10) + 1
        }
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this item?") === true){
            this.props.deleteBillingItemAction(id);
        }
    }

    handleSubmit(values){
        // if(values.groupId === undefined) billing.groupId = 0;
        // else billing.groupId = Number(values.groupId);
        //
        // if(values.name === undefined) billing.name = "";
        // else billing.name = values.name;
        //
        // if(values.status === undefined) billing.status = -1;
        // else billing.status = Number(values.status);
        // console.log(billing);
        // this.props.listAllBillingItemsAction(billing);
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div>
                {/* list all billing items */}
                <h3 style={{textAlign: "center"}}>List Stock In</h3>
                <Row>
                    <Col xs={3} sm={3} md={3} lg={3}></Col>
                    {/* form filter */}
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Item Name </strong>
                                    <Field name="name" type="text" component={TextBox} label="All names" icon=""/>
                                </Col>
                                <Col md={4} lg={4}>
                                    <strong>Date </strong>
                                    <Field name="date" component={DateTimePicker} placeholder="Date"
                                           defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                </Col>
                                <Col md={2} lg={2}>
                                    <strong>Status </strong>
                                    <Field name="status" type="select" component={SelectBox} placeholder="All status"
                                           values={this.state.status} sortBy="name" icon=""/>
                                </Col>
                                <Col md={2} lg={2}>
                                    <br/>
                                    <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH" />
                                </Col>
                            </Row>
                        </form>
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1}></Col>
                </Row>

                <Row>
                    <Col xs={1} sm={1} md={1} lg={1}></Col>
                    <Col xs={10} sm={10} md={10} lg={10}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Item Code</th>
                                    <th>Item Khmer Name</th>
                                    <th>Item English Name</th>
                                    <th>Item Short Name</th>
                                    <th>Item Group</th>
                                    <th>Currency</th>
                                    <th>Normal Price</th>
                                    <th>Foreign Price</th>
                                    <th>Business Price</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllIBillingItems.status == 200 ?
                                        <tbody>
                                        {
                                            this.props.listAllIBillingItems.data.items.list.map((billing,index)=>{
                                                total = this.props.listAllIBillingItems.data.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td>{billing.id}</td>
                                                        <td>{billing.itemCode}</td>
                                                        <td>{billing.itemNameKh}</td>
                                                        <td>{billing.itemNameEn}</td>
                                                        <td>{billing.shortName}</td>
                                                        <td>{billing.itemGroup.itemGroupEn}</td>
                                                        <td>{billing.currency == 1 ? "RIEL" : "USD"}</td>
                                                        <td>{billing.price1}</td>
                                                        <td>{billing.price2}</td>
                                                        <td>{billing.price3}</td>
                                                        <td>{billing.description}</td>
                                                        <td>{billing.status == 1 ? 'Active' : 'In Active'}</td>
                                                        {
                                                            billing.status == 1 ?
                                                                <td className="text-center">
                                                                    <Link to = {"/app/inventory/edit-billing-item/"+billing.id} className='btn btn-info btn-xs'>
                                                                        <span className="glyphicon glyphicon-edit"></span>
                                                                    </Link>
                                                                    &nbsp;
                                                                    <a onClick={()=> this.deleteItem(billing.id)} href="#" className="btn btn-danger btn-xs">
                                                                        <span className="glyphicon glyphicon-remove"></span>
                                                                    </a>
                                                                </td>
                                                                :
                                                                <td className="text-center">
                                                                    <Link to = {"/app/inventory/edit-billing-item/"+billing.id} className='btn btn-info btn-xs'>
                                                                        <span className="glyphicon glyphicon-edit"></span>
                                                                    </Link>
                                                                </td>
                                                        }

                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={13}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                            {total <= 10 ? null
                                :
                                <Pagination style={{ float: 'right'}}
                                            prev
                                            next
                                            first
                                            last
                                            ellipsis
                                            boundaryLinks
                                            items={ListBillingItem.handleItem(total)}
                                            maxButtons={5}
                                            activePage={this.state.activePage}
                                            onSelect={this.handleSelect}
                                />
                            }
                        </div>
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1}></Col>
                </Row>

            </div>
        )
    }
}

ListStockInItem = reduxForm({
    form: 'form_list_billing_item',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]/;

        const errors = {};
        if (!regex_name.test(values.name)) {
            errors.name = 'Invalid item name!';
        }
        return errors
    }
})(ListStockInItem);

function mapStateToProps(state) {
    //console.log("Data: ",state.billingItem.listAllIBillingItems);
    return {
        listAllIBillingItems: state.billingItem.listAllIBillingItems,
        deleteBillingItem: state.billingItem.deleteBillingItem
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({listAllItemGroupAction, listAllBillingItemsAction, deleteBillingItemAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListStockInItem);