import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { Row, Col, Pagination } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import moment from 'moment';
import { listAllBillingItemsAction } from '../../../actions/inventory/item';
import { listAllUnitTypesAction } from './../../../actions/inventory/unitType';
import { getSuppliersAction } from './../../../actions/inventory/supplier';
import { addStockInItemAction, getAllStockInItemsAction, deleteStockInItemAction } from './../../../actions/inventory/stockInItem';

let billingItems = [];
let unitTypes = [];
let suppliers = [];
let stockIn = {
    "page" : 1,
    "limit": 10
};
class AddStockInItem extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activePage: 1,
            status: [{id: "INSTOCK", name: "INSTOCK"},{id: "REPAIRED", name: "REPAIRED"}],
            currency : [{id: 'RIEL', name: 'RIEL'}, {id: 'USD', name: 'USD'}],
            billingItems: [],
            unitType: [],
            supplier: [],
            instock: true
        };
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentWillMount(){
        let billing = {
            limit: 100,
            page: 1,
            groupId: 0,
            name: '',
            status: 1
        };
        this.props.listAllBillingItemsAction(billing);
        this.props.listAllUnitTypesAction();
        this.props.getSuppliersAction();
        this.props.getAllStockInItemsAction(stockIn);
    }

    componentWillReceiveProps(data){
        //item group
        if(data.listAllIBillingItems.data !== undefined){
            let items = data.listAllIBillingItems.data.items;
            if(items.length === billingItems.length){
                this.setState({billingItems : billingItems});
            }else {
                items.forEach((element) => {
                    billingItems.push({
                        "id": element.id,
                        "name": element.itemNameEn
                    });
                });
                this.setState({billingItems : billingItems});
            }
        }
        //unit types
        if(data.listAllUnitType.data !== undefined){
            let items = data.listAllUnitType.data.items;
            if(items.length === unitTypes.length){
                this.setState({unitType : unitTypes});
            }else {
                items.forEach((element) => {
                    unitTypes.push({
                        "id": element.id,
                        "name": element.unitType
                    });
                });
                this.setState({unitType : unitTypes});
            }
        }
        //supplier
        if(data.getSuppliers.data !== undefined){
            let items = data.getSuppliers.data.items;
            if(items.length === suppliers.length){
                this.setState({supplier : suppliers});
            }else {
                items.forEach((element) => {
                    suppliers.push({
                        "id": element.id,
                        "name": element.supplierName
                    });
                });
                this.setState({supplier : suppliers});
            }
        }

        if(data.addStockInItem.status === 200){
            alert("Successfully added stock in item.");
            data.addStockInItem.status = 0;
            this.setState({instock: true});
            stockIn.page = 1;
            stockIn.limit = 10;
            this.props.getAllStockInItemsAction(stockIn);
        }
        if(data.addStockInItem.status === 404 || data.addStockInItem.status === 500){
            alert("Fail with add stock in item!");
            data.addStockInItem.status = 0;
        }

        if(data.deleteStockInItem.status === 200){
            alert("Successfully deleted stock in item.");
            data.deleteStockInItem.status = 0;
            stockIn.page = 1;
            stockIn.limit = 10;
            this.props.getAllStockInItemsAction(stockIn);
        }
        if(data.deleteStockInItem.status === 404 || data.deleteStockInItem.status === 500){
            alert("Fail with delete stock in time!");
            data.deleteStockInItem.status = 0;
        }
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }

    handleSelect(eventKey) {
        this.setState({
            activePage: eventKey
        });
        stockIn.page = eventKey;
        this.props.getAllStockInItemsAction(stockIn);
    }

    handleStatus(event){
        if(event.target.value === "INSTOCK") this.setState({instock: true});
        if(event.target.value === "REPAIRED") this.setState({instock: false});
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this stock in?") === true){
            this.props.deleteStockInItemAction(id);
        }
    }

    handleSubmit(values){
        if(this.state.instock){
            let stockIn = {
                "billingItemId": Number(values.billingItemId),
                "currency": values.currency,
                "cost": Number(values.cost),
                "date": values.date,
                "description": values.description,
                "quantity": Number(values.quantity),
                "stockInStatus": values.stockInStatus,
                "supplierId": Number(values.supplierId),
                "unitTypeId": Number(values.unitTypeId)
            };
            console.log(stockIn);
            this.props.addStockInItemAction(stockIn);
        }else{
            let repaired = {
                "billingItemId": Number(values.billingItemId),
                "date": values.date,
                "description": values.description,
                "quantity": Number(values.quantity),
                "stockInStatus": values.stockInStatus
            };
            console.log(repaired);
            this.props.addStockInItemAction(repaired);
        }
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div>
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add New Stock In</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col xs={6} sm={6} md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="stockInStatus" type="select" component={SelectBox} placeholder="Select status"
                                                           onChange = {this.handleStatus.bind(this)}
                                                           values={this.state.status} sortBy="name" icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Items <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="billingItemId" type="select" component={SelectBox} placeholder="Please select item"
                                                           values={this.state.billingItems} sortBy="name" icon=""/>
                                                </Col>
                                            </Row>
                                            {
                                                this.state.instock ?
                                                    <Row>
                                                    <Col md={4} lg={4} className="label-name">
                                                        <strong>Supplier <span className="label-require">*</span></strong>
                                                    </Col>
                                                    <Col md={8} lg={8}>
                                                        <Field name="supplierId" type="select" component={SelectBox} placeholder="Please select supplier"
                                                               values={this.state.supplier} sortBy="name" icon=""/>
                                                    </Col>
                                                    </Row> : null
                                            }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="date" component={DateTimePicker} placeholder="Date"
                                                           defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                                </Col>
                                            </Row>
                                            {
                                                this.state.instock ?
                                                    <Row>
                                                        <Col md={4} lg={4} className="label-name">
                                                            <strong>Unit Type <span className="label-require">*</span></strong>
                                                        </Col>
                                                        <Col md={8} lg={8}>
                                                            <Field name="unitTypeId" type="select" component={SelectBox} placeholder="Please select unit type"
                                                                   values={this.state.unitType} sortBy="name" icon=""/>
                                                        </Col>
                                                    </Row> : null
                                            }
                                        </Col>
                                        <Col xs={6} sm={6} md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Quantity <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="quantity" type="text" component={TextBox} label="Quantity" icon=""/>
                                                </Col>
                                            </Row>
                                            {
                                                this.state.instock ?
                                                    <div>
                                                        <Row>
                                                            <Col md={4} lg={4} className="label-name">
                                                                <strong>Currency <span className="label-require">*</span></strong>
                                                            </Col>
                                                            <Col md={8} lg={8}>
                                                                <Field name="currency" type="select" component={SelectBox} placeholder="Select currency" values={this.state.currency} sortBy="name" icon=""/>
                                                            </Col>
                                                        </Row>
                                                        <Row>
                                                            <Col md={4} lg={4} className="label-name">
                                                                <strong>Cost <span className="label-require">*</span></strong>
                                                            </Col>
                                                            <Col md={8} lg={8}>
                                                                <Field name="cost" type="text" component={TextBox} label="Cost" icon=""/>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                : null
                                            }
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={4} sm={4} md={4} lg={4}></Col>
                </Row>

                <Row>
                    <Col xs={11} sm={11} md={11} lg={11}>
                        <div className="">
                            <table className="list-smaller-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th>Currency</th>
                                    <th>Unit Type</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.getAllStockInItems.status == 200 ?
                                        <tbody>
                                        {
                                            this.props.getAllStockInItems.data.items.list.map((stockin,index)=>{
                                                total = this.props.getAllStockInItems.data.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td>{stockin.id}</td>
                                                        <td>{stockin.billingItem.itemNameEn}</td>
                                                        <td>{stockin.quantity}</td>
                                                        <td>{stockin.cost}</td>
                                                        <td>{stockin.currency}</td>
                                                        <td>{stockin.unitType == undefined ? null : stockin.unitType.unitType}</td>
                                                        <td>{moment(stockin.date).format("DD-MM-YYYY")}</td>
                                                        <td>{stockin.description}</td>
                                                        <td>{stockin.stockInStatus}</td>
                                                        <td className="text-center">
                                                            <div>
                                                                <Link to = {"/app/inventory/edit-billing-item/"+stockin.id} className='btn btn-info btn-xs'>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </Link>
                                                                &nbsp;
                                                                <a onClick={()=> this.deleteItem(stockin.id)} href="#" className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={13} className="text-align-center"><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                            {total <= 10 ? null
                                :
                                <Pagination style={{ float: 'right'}}
                                            prev
                                            next
                                            first
                                            last
                                            ellipsis
                                            boundaryLinks
                                            items={AddStockInItem.handleItem(total)}
                                            maxButtons={5}
                                            activePage={this.state.activePage}
                                            onSelect={this.handleSelect}
                                />
                            }
                        </div>
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1}></Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_stock_in_item'));

AddStockInItem = reduxForm({
    form: 'form_add_stock_in_item',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(values.billingItemId === undefined){
            errors.billingItemId = 'Item is required!';
        }
        if(values.supplierId === undefined){
            errors.supplierId = 'Supplier is required!';
        }
        if(values.unitTypeId === undefined){
            errors.unitTypeId = 'Unit Type is required!';
        }
        if(values.date === undefined){
            errors.date = 'Date is required!';
        }
        if(values.currency === undefined){
            errors.currency = 'Currency is required!';
        }
        if (!regex_price.test(values.cost) || values.cost === undefined) {
            errors.cost = 'Invalid cost!';
        }
        if (!regex_number.test(values.quantity) || values.quantity === undefined) {
            errors.quantity = 'Invalid quantity!';
        }
        return errors
    }
})(AddStockInItem);

function mapStateToProps(state) {
    //console.log("data : ",state.stockInItem.getAllStockInItems);
    return {
        listAllIBillingItems: state.billingItem.listAllIBillingItems,
        listAllUnitType: state.unitType.listAllUnitType,
        getSuppliers: state.supplier.getSuppliers,
        addStockInItem: state.stockInItem.addStockInItem,
        getAllStockInItems: state.stockInItem.getAllStockInItems,
        deleteStockInItem: state.stockInItem.deleteStockInItem
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ deleteStockInItemAction, getAllStockInItemsAction, getSuppliersAction, listAllUnitTypesAction, listAllBillingItemsAction, addStockInItemAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddStockInItem);