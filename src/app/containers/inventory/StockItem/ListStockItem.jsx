import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router';
import { Field, reduxForm } from 'redux-form';
import { Row, Col, FormGroup } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllStockInItemsAction, deleteStockInItemAction } from './../../../actions/inventory/stockInItem';
import '../../../components/forms/Styles.css';
import moment from 'moment';
import { listAllItemGroupAction } from './../../../actions/inventory/itemGroup';
import Pagination from '../../../components/forms/Pagination';

let arrItemGroup = [];
let stockIn = {
    "page" : 1,
    "limit": 10
};
class ListStockItem extends React.Component {
    constructor(props){
        super(props);
        this.state={
            activePage: 1,
            startDate: null,
            itemGroups: []
        };
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentWillMount(){
        this.props.listAllItemGroupAction(1);
        this.props.getAllStockInItemsAction(stockIn);
    }

    componentWillReceiveProps(data){
        // item group
        if(data.listAllItemGroup.data !== undefined){
            let items = data.listAllItemGroup.data.items;
            if(items.length === arrItemGroup.length){
                this.setState({itemGroups : arrItemGroup});
            }else {
                items.forEach((element) => {
                    arrItemGroup.push({
                        "id": element.id,
                        "name": element.itemGroupEn
                    });
                });
                this.setState({itemGroups : arrItemGroup});
            }
        }

        if(data.deleteStockInItem.status === 200){
            alert("Successfully deleted stock in item.");
            data.deleteStockInItem.status = 0;
            stockIn.page = 1;
            stockIn.limit = 10;
            this.props.getAllStockInItemsAction(stockIn);
        }
        if(data.deleteStockInItem.status === 404 || data.deleteStockInItem.status === 500){
            alert("Fail with delete stock in time!");
            data.deleteStockInItem.status = 0;
        }
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this stock in?") === true){
            this.props.deleteStockInItemAction(id);
        }
    }

    handleSubmit(values){
        // if(values.groupId === undefined) billing.groupId = 0;
        // else billing.groupId = Number(values.groupId);
        //
        // if(values.name === undefined) billing.name = "";
        // else billing.name = values.name;
        //
        // if(values.status === undefined) billing.status = -1;
        // else billing.status = Number(values.status);
        // console.log(billing);
        // this.props.listAllBillingItemsAction(billing);
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="margin_left_25 margin_top_minus_30">
                <Row>
                    <h3 className="text-align-center">List Stock Items</h3>
                </Row>
                <Row>
                    {/* form filter */}
                    <Col md={12}>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="form-inline">
                            <Col md={10}>
                                <div className="pull-right">
                                    <FormGroup>
                                        <Field name="groupId" type="select" component={SelectBox} placeholder="All item group"
                                               values={this.state.itemGroups} sortBy="name" icon=""/>
                                    </FormGroup>
                                    &nbsp;&nbsp;
                                    <FormGroup>
                                        <Field name="date" component={DateTimePicker} placeholder="Stock In Date"
                                               defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                    </FormGroup>
                                    &nbsp;&nbsp;
                                    <FormGroup>
                                        <Field name="name" type="text" component={TextBox} label="All Item Names" icon=""/>
                                    </FormGroup>
                                </div>
                            </Col>
                            <Col md={2}>
                                <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH" />
                            </Col>
                        </form>
                    </Col>
                </Row>

                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div className="">
                            <table className="page-permision-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Item Name</th>
                                    <th>Quantity</th>
                                    <th>Cost</th>
                                    <th>Currency</th>
                                    <th>Unit Type</th>
                                    <th>Item Group</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Stock in Hand</th>
                                    <th>Broken in Hand</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    this.props.getAllStockInItems.status == 200 ?
                                            this.props.getAllStockInItems.data.items.list.map((stockin,index)=>{
                                                total = this.props.getAllStockInItems.data.items.totalCount;
                                                return(
                                                    <tr key={index}>
                                                        <td>{stockin.id}</td>
                                                        <td>{stockin.billingItem.itemNameEn}</td>
                                                        <td>{stockin.quantity}</td>
                                                        <td>{stockin.cost}</td>
                                                        <td>{stockin.currency}</td>
                                                        <td>{stockin.unitType == undefined ? null : stockin.unitType.unitType}</td>
                                                        <td>{stockin.billingItem.itemGroup.itemGroupEn}</td>
                                                        <td>{moment(stockin.date).format("DD-MM-YYYY")}</td>
                                                        <td>{stockin.description}</td>
                                                        <td>{ stockin.stockInStatus == "INSTOCK" ? stockin.stockItem.stockInHand : stockin.quantity }</td>
                                                        <td>0</td>
                                                        <td className="text-center">
                                                            <div>
                                                                <Link to = {"/app/inventory/edit-billing-item/"+stockin.id} className='btn btn-info btn-xs'>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </Link>
                                                                &nbsp;
                                                                <a onClick={()=> this.deleteItem(stockin.id)} href="#" className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                )
                                            })

                                        :
                                        <tr>
                                            <td colSpan={13} className="text-align-center">RESULT NOT FOUND</td>
                                        </tr>
                                }
                                </tbody>
                            </table>
                            <Pagination
                                totalCount = {total}
                                items = {stockIn}
                                callBackAction = {this.props.getAllStockInItemsAction}
                            />
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

ListStockItem = reduxForm({
    form: 'form_list_billing_item',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]/;

        const errors = {};
        if (!regex_name.test(values.name)) {
            errors.name = 'Invalid item name!';
        }
        return errors
    }
})(ListStockItem);

function mapStateToProps(state) {
    //console.log("data : ",state.stockInItem.getAllStockInItems);
    return {
        listAllItemGroup: state.itemGroup.listAllItemGroup,
        getAllStockInItems: state.stockInItem.getAllStockInItems,
        deleteStockInItem: state.stockInItem.deleteStockInItem
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ listAllItemGroupAction, deleteStockInItemAction, getAllStockInItemsAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListStockItem);