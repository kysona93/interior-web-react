import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, FormGroup, Label } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import moment from 'moment';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { getPlanningsAction, deletePlanningAction } from './../../../actions/inventory/planning';
import Pagination from './../../../components/forms/Pagination';
import { canCreate, canUpdate, canDelete } from './../../../../auth';
import EditPlan from './Edit';
//import CheckPlan from './Check';
import ApprovePlan from './Approve';
import './style.css';

let filter = {
    year: "",
    month: 1,
    type: ""
}
class ListPlan extends React.Component {
    constructor(props){
        super(props);
        this.state={
            months: [
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ],
            planningType: [
                {id: "NEW_EXPAND", name: "New Expand for New Area"},
                {id: "MAINTENANCE", name: "Maintenance"}, 
                {id: "UPGRADE_LINE", name: "Upgrade Line"}, 
                {id: "OTHER", name: "Other"}
            ],
            object: {},
            formName: "",
            date: null
        };
        this.handleDeleteRow = this.handleDeleteRow.bind(this);
        this.handleFilterDate = this.handleFilterDate.bind(this);
    }

    componentWillMount(){
        this.props.getPlanningsAction(filter);
    }

    componentWillReceiveProps(data){

        if(data.planningDelete.status === 200){
            if(data.planningDelete.data.status === 200) {
                alert("Successfully cancel planning!.");
                this.props.getPlanningsAction(filter);
                data.planningDelete.data.status = 0;
            }
            data.planningDelete.status = 0;
        }
        if(data.planningDelete.status === 400 || data.planningDelete.status === 500){
            alert("Fail with cancel planning!");
        }
    }

    handleDeleteRow(id){
        if(confirm("Are you sure want to cancel this planning?") === true){
            this.props.deletePlanningAction(id);
        }
    }

    handleSubmit(values){

        if(values.year === undefined) filter.year = "";
        else filter.year = values.year;

        if(values.month === undefined) filter.month = 1;
        else filter.month = values.month;

        if(values.type === undefined) filter.type = "";
        else filter.type = values.type;

        this.props.getPlanningsAction(filter);
    }

    handleRenderForm(object, formName) {
        this.setState({ 
            object: object,
            formName: formName
        });
    }

    handleHideShowEdit(value) {
        this.setState({ formName: value });
        this.props.getPlanningsAction(filter);
    }

    handleFilterDate(date) {
        this.setState({
            date: date
        });
    }

    renderForm() {
        if(this.state.formName === 'edit') {
            return(
                <EditPlan object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        }
        // else if(this.state.formName === 'check') {
        //     return(
        //         <CheckPlan object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} />
        //     );
        // }
        else if(this.state.formName === 'approve') {
            return(
                <ApprovePlan object={this.state.object} handleHideShowEdit={this.handleHideShowEdit.bind(this)} /> 
            );
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            this.state.formName == "" ?
                <div className="margin_left_25 margin_top_minus_30">
                    <Row>
                        <h3 className="text-align-center">List Planning</h3>
                    </Row>
                    <Row>
                        <Col md={3}>
                        {
                            canCreate == false ?
                                <Button 
                                    onClick={() => browserHistory.push('/app/inventory/plans/add')}
                                    className="btn btn-default">
                                    <i className="fa fa-plus" />
                                    {' '}Add New Plan
                                </Button>
                            : null
                        }
                        </Col>
                        <Col md={9}>
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="form-inline">
                                <Col md={10}>
                                    <div className="pull-right">
                                        <FormGroup controlId="formInlineName">
                                            <Field
                                                name="year"
                                                type="text"
                                                component={TextBox}
                                                label="year"
                                                />
                                        </FormGroup>
                                        {' '}
                                        <FormGroup controlId="formInlineName">
                                            <Field
                                                name="month"
                                                type="select"
                                                component={SelectBox}
                                                placeholder="Select Month"
                                                values={this.state.months} />
                                        </FormGroup>
                                        {' '}
                                        <FormGroup controlId="formInlineName">
                                            <Field name="type" type="select" component={SelectBox} placeholder="Plan Type"
                                                values={this.state.planningType} />
                                        </FormGroup>
                                    </div>
                                </Col>
                                <Col md={2}>
                                    <ButtonSubmit invalid={invalid} submitting={submitting} label="SEARCH"/>
                                </Col>
                            </form>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={12}>
                            <div className="">
                                <table className="list-smaller-table">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Planning No</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Number of Village</th>
                                        <th>Number of Family</th>
                                        <th>Length(km)</th>
                                        <th>Checked Date</th>
                                        <th>Approved Date</th>
                                        <th>Status</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.props.plannings.status === 200 ?
                                            this.props.plannings.data.items.map((planning ,index) => {
                                                return(
                                                    <tr key={index}>
                                                        <td>{index + 1}</td>
                                                        <td>
                                                            <a href="javascript:void(0)" onClick={() => this.handleRenderForm(planning, 'edit')}>
                                                                {planning.planningNo}
                                                            </a>
                                                        </td>
                                                        <td>{planning.description}</td>
                                                        <td>{moment(planning.preparedDate).format('L')}</td>
                                                        <td>{planning.numberOfVillage}</td>
                                                        <td>{planning.numberOfFamily}</td>
                                                        <td>{planning.lengthKm}</td>
                                                        <td>
                                                            {planning.checkedDate === null ? 
                                                                <button type="button" disabled={planning.planningStatus === "CANCEL" ? true : false} className='btn btn-default btn-xs' onClick={() => this.handleRenderForm(planning, 'check')}>
                                                                    Checked
                                                                </button> 
                                                                : moment(planning.checkedDate).format('L')
                                                            }
                                                        </td>
                                                        <td>
                                                            {planning.approvedDate === null ? 
                                                                <button type="button" disabled={planning.checkedDate === null ? true : false} className='btn btn-default btn-xs' onClick={() => this.handleRenderForm(planning, 'approve')}>
                                                                    Approved
                                                                </button> 
                                                                : moment(planning.approvedDate).format('L')
                                                            }
                                                        </td>
                                                        <td>
                                                            { planning.planningStatus === "NEW_PLANNING" || planning.planningStatus === null ? 
                                                                <Label bsStyle="primary">New Request</Label> 
                                                                : null 
                                                            }
                                                            { planning.planningStatus === "PROCESSING" ? <Label bsStyle="info">Processing</Label> : null }
                                                            { planning.planningStatus === "COMPLETE" ? <Label bsStyle="success">Complete</Label> : null }
                                                            { planning.planningStatus === "CANCEL" ? <Label bsStyle="danger">Cancel</Label> : null }
                                                        </td>
                                                        { planning.planningStatus !== "CANCEL" ?
                                                            <td className="text-center">
                                                                {
                                                                    canUpdate === false ?
                                                                        <a className='btn btn-info btn-xs' onClick={() => this.handleRenderForm(planning, 'edit')}>
                                                                            <span className="glyphicon glyphicon-edit"></span>
                                                                        </a>
                                                                    : null
                                                                }
                                                                {
                                                                    canDelete === false ?
                                                                        <a onClick={()=> this.handleDeleteRow(planning.id)} className="btn btn-danger btn-xs">
                                                                            <span className="glyphicon glyphicon-remove"></span>
                                                                        </a>
                                                                    : null
                                                                }
                                                            </td>
                                                        : <td></td> }
                                                    </tr>
                                                )
                                            })
                                        :
                                        <tr>
                                            <td colSpan={11} className="text-align-center">RESULT NOT FOUND</td>
                                        </tr>
                                    }
                                    </tbody>
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            : 
            this.renderForm()
        )
    }
}

ListPlan = reduxForm({
    form: 'form_list_plannings',
    validate: (values) => {
        let regex_number = /^[0-9]{4,4}$/;
        
        const errors = {};

        if(values.year !== undefined){
            if (!regex_number.test(values.year)) {
                errors.year = 'Invalid Year!';
            }
        }
        return errors
    }
})(ListPlan);

function mapStateToProps(state) {
    return {
        plannings: state.planning.plannings,
        planningDelete: state.planning.planningDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getPlanningsAction,
        deletePlanningAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListPlan);