import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, Checkbox, Label } from 'react-bootstrap';
import { Field, reduxForm, reset, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { Typeahead } from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import '../../../components/forms/Styles.css';
import { updatePlanningAction } from './../../../actions/inventory/planning';
import { getAllItemsAction } from './../../../actions/inventory/item';
import { authObj } from './../../../../auth';
import moment from 'moment';
import accounting from 'accounting';


class ApprovePlan extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            planningType: [
                {id: "NEW_EXPAND", name: "New Expand for New Area"}, 
                {id: "MAINTENANCE", name: "Maintenance"}, 
                {id: "UPGRADE_LINE", name: "Upgrade Line"}, 
                {id: "OTHER", name: "Other"}
            ],
            preparedDate: this.props.object.preparedDate !== null ? moment(this.props.object.preparedDate) : null,
            checkedDate: this.props.object.checkedDate !== null ? moment(this.props.object.checkedDate) : null,
            approvedDate: this.props.object.approvedDate !== null ? moment(this.props.object.approvedDate) : null,
            quotations: []
        };
        this.handleApprovedDate = this.handleApprovedDate.bind(this);
    }

    componentWillMount(){
        let planning = {
            "description": this.props.object.description,
            "lengthKm": this.props.object.lengthKm,
            "numOfFamily": this.props.object.numberOfFamily,
            "numOfVillage": this.props.object.numberOfVillage,
            "planningNo": this.props.object.planningNo,
            "planningTitle": this.props.object.planningTitle,
            "planningType": this.props.object.planningType,
            "preparedBy": this.props.object.preparedBy,
            "approvedBy": authObj().sub,
            "approvedDate": this.props.object.approvedDate,
            "checkedBy": this.props.object.checkedBy,
            "checkedDate": this.props.object.checkedDate,
        }
        this.props.dispatch(initialize('form_approve_planning', planning));

        if(this.props.object.quotations.length !== 0) {
            this.setState({ quotations: this.props.object.quotations });
        }
    }

    componentWillUnmount() {
        this.setState({ quotations: [] });
    }

    componentWillReceiveProps(newProps){
        if(newProps.planningUpdate.status === 200){
            if(newProps.planningUpdate.data.status === 200){
                alert("Successfully approved planning.");
                this.props.handleHideShowEdit('');
                newProps.planningUpdate.data.status = 0;
            } else {
                alert("Fail with approve planning!");
                newProps.planningUpdate.data.status = 0;
            }
            newProps.planningUpdate.status = 0;
        }
        if(newProps.planningUpdate.status === 404 || newProps.planningUpdate.status === 500){
            alert("Fail with approve planning!");
            newProps.planningUpdate.status = 0;
        }
    }

    handleApprovedDate(date) {
        this.setState({
            approvedDate: date
        });
    }

    handleSubmit(values){
        let planning = {
            "id": this.props.object.id,
            "lengthKm": this.props.object.lengthKm,
            "numberOfFamily": this.props.object.numberOfFamily,
            "numberOfVillage": this.props.object.numberOfVillage,
            "approvedBy": values.approvedBy,
            "approvedDate": values.approvedDate,
            "planningStatus": "NEW_PLANNING"
        }

        this.props.updatePlanningAction(planning);
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        let totalPriceRiel = 0;
        let totalPriceUsd = 0;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Checked Planning</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={5} lg={5} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning No <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningNo" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Planning No" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning Type <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.planningType}
                                                        disabled={true}
                                                        icon="" />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Village <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfVillage" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Village" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Length of Planning(km) <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="lengthKm" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Length" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Family <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfFamily" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Family" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    { this.props.object.planningStatus == "NEW_PLANNING" ? <h4><Label bsStyle="primary">New Request</Label></h4> : null }
                                                    { this.props.object.planningStatus == "PROCESSING" ? <h4><Label bsStyle="info">Processing</Label></h4> : null }
                                                    { this.props.object.planningStatus == "COMPLETE" ? <h4><Label bsStyle="success">Complete</Label></h4> : null }
                                                    { this.props.object.planningStatus == "CANCEL" ? <h4><Label bsStyle="danger">Cancel</Label></h4> : null }
                                                </Col>
                                            </Row>
                                            
                                        </Col>

                                        <Col md={7} lg={7}>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Planning Title <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="planningTitle" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Title" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Description <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={120}
                                                        disabled={true}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared by <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Prepared by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.preparedDate}
                                                        handleChange={this.handleApprovedDate}
                                                        disabled={true}
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked by <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Checked by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.checkedDate}
                                                        disabled={true} 
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved by <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Approve by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approve Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedDate" 
                                                        component={DateTimePicker}
                                                        defaultDate={this.state.approvedDate} 
                                                        handleChange={this.handleApprovedDate}
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12}>
                                            <br/>
                                            <div className="well planning well-sm">List Quotation</div>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12}>
                                            <table className="list-smaller-table">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Item Name</th>
                                                    <th>Unit Type</th>
                                                    <th>Quantity</th>
                                                    <th>Unit Price</th>
                                                    <th>Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.quotations.map((q, index)=>{
                                                        if (q.item.currency === 'USD') {
                                                            totalPriceUsd = totalPriceUsd + (q.quantity * q.price);
                                                        } else {
                                                            totalPriceRiel = totalPriceRiel + (q.quantity * q.price);
                                                        }
                                                        return(
                                                            <tr key={index}>
                                                                <td>{index + 1}</td>
                                                                <td>{q.item.itemNameEn}{' '}{q.item.itemNameKh}</td>
                                                                <td>{q.item.unitType !== null ? q.item.unitType.unitType : "Unit"}</td>
                                                                <td>{q.quantity}</td>
                                                                <td>{accounting.formatMoney(q.price, { symbol: "",  format: "%v %s" })}{' '}{q.item.currency}</td>
                                                                <td>{accounting.formatMoney(q.quantity * q.price, { symbol: "",  format: "%v %s" })}{' '}{q.item.currency}</td>
                                                            </tr>
                                                        )
                                                    })
                                                }
                                                    <tr>
                                                        <td colSpan={5} rowSpan={2} className="text-align-center"><h4>Total Amount</h4></td>
                                                        <td><b>{accounting.formatMoney(totalPriceRiel, { symbol: "Riel",  format: "%v %s" })}</b></td>
                                                    </tr>
                                                    <tr>
                                                        <td><b>{accounting.formatMoney(totalPriceUsd, { symbol: "USD",  format: "%v %s" })}</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Approved" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.props.handleHideShowEdit('')}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_approve_planning'));

ApprovePlan = reduxForm({
    form: 'form_approve_planning',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {

        const errors = {};
        if (values.approvedDate === "" || values.approvedDate === null) {
            errors.approvedDate = 'Approved Date is required!';
        }
        
        return errors
    }
})(ApprovePlan);

function mapStateToProps(state) {
    return {
        planningUpdate: state.planning.planningUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        updatePlanningAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ApprovePlan);