import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPlanningsAction } from './../../../actions/inventory/planning';
import { listSettingAction } from './../../../actions/setup/setting';

class PrintPlaning extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            planning: {},
            officeTitle: "",
            officeAddress: "",
            officePhone: ""
        }
    }

    componentWillMount(){
        let filter = {
            year: "",
            month: 1,
            type: ""
        }
        this.props.getPlanningsAction(filter);
        this.props.listSettingAction();

        setTimeout(() => {window.print()}, 1500)
    }

    componentWillReceiveProps(newProps){
        let id = this.props.params.planId;
        if(newProps.plannings.status === 200) {
            const item = newProps.plannings.data.items.find(i => i.id === Number(id));
            this.setState({ planning: item });
        }
        if(newProps.listSetting.status === 200) {
            const item1 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_TITLE");
            const item2 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_ADDRESS");
            const item3 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_PHONE");
            this.setState({ 
                officeTitle: item1.value,
                officeAddress: item2.value,
                officePhone: item3.value
            });
        }
    }

    render(){
        let total = 0;
        return(
            <div className="container" style={{width: 700, marginTop: 30}}>
                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-align-center"><h2>{this.state.officeTitle}</h2></div>
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-align-center"><h3>QUOTATIONS</h3></div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-xs-7 col-sm-7 col-md-7 col-lg-7">
                        <span><p>{this.state.officeAddress}</p></span>
                        <span><p>Tel: {this.state.officePhone}</p></span>
                    
                    </div>
                    <div className="col-xs-5 col-sm-5 col-md-5 col-lg-5">
                        <span><p>Planning No: {this.state.planning !== undefined ? this.state.planning.planningNo : ""}</p></span>
                        <span><p>Date: ....................................</p></span>
                        <span><p>Project: {this.state.officeTitle}</p></span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12 col-md-12 col-lg-12">
                        <table className="table report table-bordered">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Item/Description</th>
                                    <th>Unit</th>
                                    <th>Qty</th>
                                    <th width="250">Remark</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                this.state.planning !== undefined ?
                                    this.state.planning.quotations !== undefined ? 
                                        this.state.planning.quotations.map((q, index) => {
                                            total = total + q.quantity;
                                            return(
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{q.item.itemNameEn}</td>
                                                    <td>{q.item.unitType.unitType}</td>
                                                    <td>{q.quantity}</td>
                                                    <td></td>
                                                </tr>
                                            );
                                        })
                                    : null
                                : null
                            }
                                <tr>
                                    <td className="text-align-center" colSpan ={3}>Total</td>
                                    <td>{total}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12">
                        <p>Reasons for planning: ........................................................................................................................................</p>
                        <p>............................................................................................................................................................................</p>
                        <p>............................................................................................................................................................................</p>
                        <p>............................................................................................................................................................................</p>
                        <p>............................................................................................................................................................................</p>
                        <p>............................................................................................................................................................................</p>
                    </div>
                </div>
                <br/>
                <div className="row">
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" style={{borderTop: '1px solid', borderLeft: '1px solid', borderBottom: '1px solid', padding: 10}}>
                        <span><p>Requested By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name:</p></span>
                        <span><p>Date:</p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" style={{borderTop: '1px solid', borderLeft: '1px solid', borderBottom: '1px solid', padding: 10}}>
                        <span><p>Endorsor By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name:</p></span>
                        <span><p>Date:</p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" style={{borderTop: '1px solid', borderLeft: '1px solid', borderBottom: '1px solid', padding: 10}}>
                        <span><p>Verified By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name:</p></span>
                        <span><p>Date:</p></span>
                    </div>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3" style={{border: '1px solid', padding: 10}}>
                        <span><p>Apporval By:</p></span>
                        <br/>
                        <br/>
                        <br/>
                        <span><p>Name:</p></span>
                        <span><p>Date:</p></span>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        plannings: state.planning.plannings,
        listSetting: state.setting.listSetting
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getPlanningsAction,
        listSettingAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(PrintPlaning);