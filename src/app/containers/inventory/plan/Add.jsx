import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Checkbox } from 'react-bootstrap';
import { Field, reduxForm, reset, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import '../../../components/forms/Styles.css';
import { getPlanningNoAction, addPlanningAction } from './../../../actions/inventory/planning';
import { authObj } from './../../../../auth';

class AddPlan extends React.Component {
    constructor(props){
        super(props);
        this.state={
            planningType: [
                {id: "NEW_EXPAND", name: "New Expand for New Area"},
                {id: "MAINTENANCE", name: "Maintenance"}, 
                {id: "UPGRADE_LINE", name: "Upgrade Line"}, 
                {id: "OTHER", name: "Other"}
            ],
            preparedDate: null
        };
        this.handlePreparedDate = this.handlePreparedDate.bind(this);
    }

    componentWillMount(){
        this.props.getPlanningNoAction();
    }

    componentWillReceiveProps(newProps){

        if(newProps.planningNo.status === 200) {
            let planningNoExist = newProps.planningNo.data.items.planningNo;
            let newPlanningNo = this.handleConvertPlanningNo(planningNoExist);
            this.props.dispatch(change('form_add_planing', 'planningNo', newPlanningNo));
        } else {
            let newPlanningNo = "PN-"+new Date().getFullYear()+"-0001";
            this.props.dispatch(change('form_add_planing', 'planningNo', newPlanningNo));
        }

        if(newProps.planningAdd.status === 200){
            if(newProps.planningAdd.data.status === 200){
                alert("Successfully added new planning.");
                window.history.back()
                newProps.planningAdd.data.status = 0;
            } else {
                alert("Fail with add planning!");
                newProps.planningAdd.data.status = 0;
            }
            newProps.planningAdd.status = 0;
        }
        if(newProps.planningAdd.status === 404 || newProps.planningAdd.status === 500){
            alert("Fail with add planning!");
            newProps.planningAdd.status = 0;
        }
    }

    handleConvertPlanningNo(data) {
        var str1 = data; // ex: PN-2017-0001
        var str2 = str1.substr(str1.indexOf("-")+1, str1.length); // 2017-0001 output
        var str3 = str2.substr(0, str2.indexOf("-")); // 2017 output
        var str4 = str2.substr(str2.indexOf("-")+1, str2.length); // 0001 output
        
        var cYear = new Date().getFullYear();
        let planningNo;

        if (cYear === Number(str3)) {
            var orderNo = (Number(str4) + 1);
            var strOrderNo;
            switch (orderNo.toString().length) {
                case 1:
                    strOrderNo = "000" + orderNo;
                    break;
                case 2:
                    strOrderNo = "00" + orderNo;
                    break;
                case 3:
                    strOrderNo = "0" + orderNo;
                    break;
                case 4:
                    strOrderNo = orderNo;
                    break;
            }
            planningNo = "PN-" + str3 + "-" + strOrderNo;
        } else {
            planningNo = "PN-" + cYear + "-0001";
        }
        return planningNo;
    }

    handlePreparedDate(date) {
        this.setState({
            preparedDate: date
        });
    }

    handleSubmit(values){
        
        let planning = {
            "description": values.description,
            "lengthKm": Number(values.lengthKm),
            "numberOfFamily": Number(values.numOfFamily),
            "numberOfVillage": Number(values.numOfVillage),
            "planningNo": values.planningNo,
            "planningTitle": values.planningTitle,
            "planningType": values.planningType,
            "preparedBy": values.preparedBy,
            "preparedDate": values.preparedDate
        };

        this.props.addPlanningAction(planning);
    }


    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add Planning</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning No<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningNo" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Planning No" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning Type<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.planningType}
                                                        icon="" />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Village<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfVillage" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Village" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Length of Planning(km) <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="lengthKm" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Length" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Family<span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfFamily" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Family" 
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningStatus" 
                                                        type="text" 
                                                        disabled={true}
                                                        component={TextBox} 
                                                        label="Status" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            
                                        </Col>

                                        <Col md={8} lg={8}>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Planning Title <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="planningTitle" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Title" 
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Description <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={120}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared by <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Prepared by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.preparedDate}
                                                        handleChange={this.handlePreparedDate}
                                                        placeholder="Prepared Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked by</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Checked by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedDate" 
                                                        component={DateTimePicker} 
                                                        disabled={true}
                                                        placeholder="Checked Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved by</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Approve by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approve Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedDate" 
                                                        component={DateTimePicker} 
                                                        disabled={true} 
                                                        placeholder="Approved Date" />
                                                </Col>
                                            </Row>

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Save" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => window.history.back()}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_planing'));

AddPlan = reduxForm({
    form: 'form_add_planing',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_code.test(values.planningNo) || values.planningNo === undefined){
            errors.planningNo = 'Invalid Planning No!';
        }
        if (!regex_name.test(values.planningTitle) || values.planningTitle === undefined) {
            errors.planningTitle = 'Invalid Planning Title!';
        }
        if (values.planningType === undefined) {
            errors.planningType = 'Planning Type is required!';
        }
        if (!regex_number.test(values.numOfVillage) || values.numOfVillage === undefined) {
            errors.numOfVillage = 'Invalid Number of Village!';
        }
        if (!regex_number.test(values.numOfFamily) || values.numOfFamily === undefined) {
            errors.numOfFamily = 'Invalid Number of Family!';
        }
        if (!regex_number.test(values.lengthKm) || values.lengthKm === undefined) {
            errors.lengthKm = 'Invalid Length of Planning!';
        }
        if (values.preparedDate === undefined) {
            errors.preparedDate = 'Prepared Date is required!';
        }
        if (values.description === undefined) {
            errors.description = 'Description is required!';
        }
        
        return errors
    }
})(AddPlan);

function mapStateToProps(state) {
    return {
        planningNo: state.planning.planningNo,
        planningAdd: state.planning.planningAdd,
        initialValues: {
            preparedBy: authObj().sub,
            planningStatus: "New Planning"
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        getPlanningNoAction, 
        addPlanningAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddPlan);