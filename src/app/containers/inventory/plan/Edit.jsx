import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, Checkbox, Label } from 'react-bootstrap';
import { Field, reduxForm, reset, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import AsyncTypeaheadFilter from './../../../components/forms/AsyncTypeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import '../../../components/forms/Styles.css';
import { updatePlanningAction } from './../../../actions/inventory/planning';
import { getAllItemsAction } from './../../../actions/inventory/item';
import moment from 'moment';
import { Typeahead } from 'react-bootstrap-typeahead';

let items = [];
let qoutation = {
    itemId: 0,
    quantity: 0,
    unitType: "",
    currency: "",
    price: 0
};
let qoutations = [];
let itemsAll = [];
class EditPlan extends React.Component {
    constructor(props){
        super(props);
        this.state={
            planningType: [
                {id: "NEW_EXPAND", name: "New Expand for New Area"},
                {id: "MAINTENANCE", name: "Maintenance"}, 
                {id: "UPGRADE_LINE", name: "Upgrade Line"}, 
                {id: "OTHER", name: "Other"}
            ],
            preparedDate: this.props.object.preparedDate !== null ? moment(this.props.object.preparedDate) : null,
            checkedDate: this.props.object.checkedDate !== null ? moment(this.props.object.checkedDate) : null,
            approvedDate: this.props.object.approvedDate !== null ? moment(this.props.object.approvedDate) : null,
            items: [],
            itemsAll: []
        };
        this.handleSelectItem = this.handleSelectItem.bind(this);
        this.handleSetQuantity = this.handleSetQuantity.bind(this);
    }

    componentWillMount(){
        this.props.getAllItemsAction('');

        let planning = {
            "description": this.props.object.description,
            "lengthKm": this.props.object.lengthKm,
            "numOfFamily": this.props.object.numberOfFamily,
            "numOfVillage": this.props.object.numberOfVillage,
            "planningNo": this.props.object.planningNo,
            "planningTitle": this.props.object.planningTitle,
            "planningType": this.props.object.planningType,
            "preparedBy": this.props.object.preparedBy,
            "approvedBy": this.props.object.approvedBy,
            "approvedDate": this.props.object.approvedDate,
            "checkedBy": this.props.object.checkedBy,
            "checkedDate": this.props.object.checkedDate,
        };
        this.props.dispatch(initialize('form_edit_planning', planning));

        if(this.props.object.quotations.length !== 0) {
            let q = this.props.object.quotations;

            let qitems = [];
            for(let i=0; i<q.length; i++) {
                qitems.push(q[i].item);

                qoutation = {
                    itemId: q[i].item.id,
                    quantity: q[i].quantity,
                    unitType: q[i].unitType,
                    currency: q[i].currency,
                    price: q[i].price
                };
                qoutations.push(qoutation);

                this.props.dispatch(change('form_edit_planning', 'quantity' + i, q[i].quantity));
                this.props.dispatch(change('form_edit_planning', 'price' + i, q[i].price));

            }
            items = qitems;

            this.setState({ items: qitems });
        }

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });
    }

    componentWillUnmount() {
        items = [];
        qoutations = [];
        this.setState({ items: [], itemsAll: [] });
    }

    componentWillReceiveProps(newProps){
        //itemsAll
        if(newProps.itemsAll !== undefined){
            let items = newProps.itemsAll.items;
            if (items !== undefined) {
                if(items.length === itemsAll.length){
                    this.setState({itemsAll : itemsAll});
                }else {
                    items.forEach((element) => {
                        itemsAll.push({
                            "id": element.id,
                            "name": element.itemNameEn
                        });
                    });
                    this.setState({itemsAll : itemsAll});
                }
            }
        }

        if(newProps.planningUpdate.status === 200){
            if(newProps.planningUpdate.data.status === 200){
                alert("Successfully updated planning.");
                this.props.handleHideShowEdit('')
                newProps.planningUpdate.data.status = 0;
            } else {
                alert("Fail with update planning!");
                newProps.planningUpdate.data.status = 0;
            }
            newProps.planningUpdate.status = 0;
        }
        if(newProps.planningUpdate.status === 404 || newProps.planningUpdate.status === 500){
            alert("Fail with update planning!");
            newProps.planningUpdate.status = 0;
        }
    }

    handleSubmit(values){
        let planning = {
            "id": this.props.object.id,
            "description": values.description,
            "lengthKm": Number(values.lengthKm),
            "numberOfFamily": Number(values.numOfFamily),
            "numberOfVillage": Number(values.numOfVillage),
            "planningTitle": values.planningTitle,
            "planningType": values.planningType,
            "quotationForms": qoutations
        };

        this.props.updatePlanningAction(planning);
    }

    handleSelectItem(id) {
        if(this.props.itemsAll.items !== undefined){
            const item = this.props.itemsAll.items.find(i => i.id === Number(id));

            if(item !== undefined) {
                if(items.find(i => i.id === Number(id)) === undefined) {
                    items.push(item);
                    this.setState({ items: items });
                    qoutation = {
                        itemId: item.id,
                        quantity: 1,
                        unitType: item.unitType.unitType,
                        currency: item.currency,
                        price: item.cost
                    };
                    qoutations.push(qoutation);
                    this.props.dispatch(change('form_edit_planning', 'quantity' + items.indexOf(item), 1));
                    this.props.dispatch(change('form_edit_planning', 'price' + items.indexOf(item), item.cost));
                    setTimeout(() => this._typeahead.getInstance().clear(), 0);
                } else {
                    document.getElementById('errorMessage').style.display = 'block';
                    setTimeout(() => this._typeahead.getInstance().clear(), 0);
                    setTimeout(() => document.getElementById('errorMessage').style.display = 'none', 2000);
                }
            }
        }
    }

    handleRemoveRow(item) {
        let index = items.indexOf(item);
        if (index > -1) {
            items.splice(index, 1);

            // dispatch item text fields value again remove.
            qoutations.splice(index, 1);

            for(let i=0; i<qoutations.length; i++) {
                this.props.dispatch(change('form_edit_planning', 'quantity' + i, qoutations[i].quantity));
                this.props.dispatch(change('form_edit_planning', 'price' + i, qoutations[i].price));
            }
        }
        this.setState({ items: items });
    }

    handleSetQuantity(id, e) {
        qoutation = qoutations.find(q => q.itemId === Number(id));
        qoutation.quantity = Number(e.target.value);
        qoutations.map(q => q.itemId === Number(id) ? qoutation : q)
    }

    handleSetPrice(id, e) {
        qoutation = qoutations.find(q => q.itemId === Number(id));
        qoutation.price = Number(e.target.value);
        qoutations.map(q => q.itemId === Number(id) ? qoutation : q)
    }

    renderAddNewItem() {
        return(
            <Row>
                <Col md={5} lg={5}>
                    <Field
                        name="requestType"
                        type="select"
                        component={AsyncTypeaheadFilter}
                        handleOnChange={selected => { this.handleSelectItem(_.map(selected, "id")) }}
                        handleRef={ref => this._typeahead = ref}
                        labelKey="name"
                        placeholder="Please select item ..."
                        values={this.state.itemsAll}/>
                </Col>
                <Col md={1} lg={1}>
                    <Link 
                        data-toggle="tooltip" 
                        data-placement="top" 
                        title="Add New Item" 
                        className="btn btn-default" 
                        to="/app/inventory/items/add">
                        <i className="fa fa-plus-circle" />
                    </Link>
                </Col>
                <Col md={4} lg={4} className="label-name">
                    <span style={{display: 'none'}} id="errorMessage" className="label-require">Item has been selected!</span>
                </Col>
                <Col md={2} lg={2} className="label-name">
                    <Link 
                        className="btn btn-default" 
                        onClick={() => window.open(`/print-planning/${this.props.object.id}`)} >
                        <i className="fa fa-print"/>
                        {' '}Printing
                    </Link>
                </Col>
            </Row>
        );
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={12}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Edit Planning</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={5} lg={5} >
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning No <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningNo" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Planning No" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Planning Type <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="planningType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.planningType}
                                                        icon="" />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Village <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfVillage" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Village" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Length of Planning(km) <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="lengthKm" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Length" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Number of Family <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="numOfFamily" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Number of Family" 
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    { this.props.object.planningStatus === "NEW_PLANNING" || this.props.object.planningStatus === null ?
                                                        <h4><Label bsStyle="primary">New Planning</Label></h4> 
                                                        : null 
                                                    }
                                                    { this.props.object.planningStatus === "PROCESSING" ? <h4><Label bsStyle="info">Processing</Label></h4> : null }
                                                    { this.props.object.planningStatus === "COMPLETE" ? <h4><Label bsStyle="success">Complete</Label></h4> : null }
                                                    { this.props.object.planningStatus === "CANCEL" ? <h4><Label bsStyle="danger">Cancel</Label></h4> : null }
                                                </Col>
                                            </Row>
                                            
                                        </Col>

                                        <Col md={7} lg={7}>
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Planning Title <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="planningTitle" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Title" 
                                                        icon=""/>
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Description <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={9} lg={9}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={120}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            
                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared by <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Prepared by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>

                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Prepared Date <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="preparedDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.preparedDate}
                                                        handleChange={this.handlePreparedDate}
                                                        disabled={true}
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked by</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Checked by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Checked Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="checkedDate" 
                                                        component={DateTimePicker} 
                                                        defaultDate={this.state.checkedDate}
                                                        disabled={true}
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                            <Row>
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approved by</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedBy" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Approve by" 
                                                        disabled={true}
                                                        icon=""/>
                                                </Col>
                                                
                                                <Col md={3} lg={3} className="label-name">
                                                    <strong>Approve Date</strong>
                                                </Col>
                                                <Col md={3} lg={3}>
                                                    <Field 
                                                        name="approvedDate" 
                                                        component={DateTimePicker}
                                                        defaultDate={this.state.approvedDate} 
                                                        disabled={true} 
                                                        placeholder="Date" />
                                                </Col>
                                            </Row>

                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12}>
                                            <br/>
                                            <div className="well planning well-sm">Add Quotation</div>
                                            <br/>
                                            { this.renderAddNewItem() }
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={12}>
                                            <table className="list-smaller-table">
                                                <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Item Name</th>
                                                    <th>Unit Type</th>
                                                    <th>Quantity</th>
                                                    <th>Price</th>
                                                    <th className="text-align-center"><i className="fa fa-trash-o" /></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    this.state.items.length !== 0 ?
                                                        this.state.items.map((item, index)=>{
                                                            return(
                                                                <tr key={index}>
                                                                    <td>{index + 1}</td>
                                                                    <td>{item.itemNameEn}{' '}{item.itemNameKh}</td>
                                                                    <td>{item.unitType !== null ? item.unitType.unitType : "Unit"}</td>
                                                                    <td className="form-inline">
                                                                        <Field 
                                                                            name={"quantity" + index} 
                                                                            type="text" 
                                                                            component={TextBox} 
                                                                            onChange={(e) => this.handleSetQuantity(item.id, e)}
                                                                            label="Quantity"/>
                                                                    </td>
                                                                    <td className="form-inline">
                                                                        <Field 
                                                                            name={"price" + index} 
                                                                            type="text" 
                                                                            component={TextBox} 
                                                                            onChange={(e) => this.handleSetPrice(item.id, e)}
                                                                            label="price" />

                                                                            {' '}{item.currency}
                                                                    </td>
                                                                    <td>
                                                                        <a onClick={()=> this.handleRemoveRow(item)} className="btn btn-danger btn-xs">
                                                                            <span className="glyphicon glyphicon-minus"></span>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                    : null
                                                }
                                                </tbody>
                                            </table>
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            { this.props.object.approvedDate === null && 
                                                (this.props.object.planningStatus !== "CANCEL" || this.props.object.planningStatus === null) ?
                                                    <ButtonSubmit 
                                                        error={error} 
                                                        invalid={invalid} 
                                                        submitting={submitting} 
                                                        label="Save" />
                                                : null
                                            }
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.props.handleHideShowEdit('')}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_edit_planning'));

EditPlan = reduxForm({
    form: 'form_edit_planning',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_code = /[0-9a-zA-Z]{2,20}/;
        let regex_name = /[0-9a-zA-Z]{2,255}/;
        let regex_number = /^[0-9]{1,8}$/;
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_code.test(values.planningNo) || values.planningNo === undefined){
            errors.planningNo = 'Invalid Planning No!';
        }

        if (!regex_name.test(values.planningTitle) || values.planningTitle === undefined) {
            errors.planningTitle = 'Invalid Planning Title!';
        }
        if (values.planningType === "" || values.planningType === null) {
            errors.planningType = 'Planning Type is required!';
        }
        if (!regex_number.test(values.numOfVillage) || values.numOfVillage === undefined) {
            errors.numOfVillage = 'Invalid Number of Village!';
        }
        if (!regex_number.test(values.numOfFamily) || values.numOfFamily === undefined) {
            errors.numOfFamily = 'Invalid Number of Family!';
        }
        if (!regex_number.test(values.lengthKm) || values.lengthKm === undefined) {
            errors.lengthKm = 'Invalid Length of Planning!';
        }
        if (values.description === "" || values.description === null) {
            errors.description = 'Description is required!';
        }

        // quantity of add quotation
        if (!regex_number.test(values.quantity0) || values.quantity0 === undefined) {
            errors.quantity0 = 'Invalid Quantity!';
        }
        if (!regex_number.test(values.quantity1) || values.quantity1 === undefined) {
            errors.quantity1 = 'Invalid Quantity!';
        }
        if (!regex_number.test(values.quantity2) || values.quantity2 === undefined) {
            errors.quantity2 = 'Invalid Quantity!';
        }
        if (!regex_number.test(values.quantity3) || values.quantity3 === undefined) {
            errors.quantity3 = 'Invalid Quantity!';
        }
        if (!regex_number.test(values.quantity4) || values.quantity4 === undefined) {
            errors.quantity4 = 'Invalid Quantity!';
        }
        if (!regex_number.test(values.quantity5) || values.quantity5 === undefined) {
            errors.quantity5 = 'Invalid Quantity!';
        }
        
        return errors
    }
})(EditPlan);

function mapStateToProps(state) {
    return {
        planningUpdate: state.planning.planningUpdate,
        itemsAll: state.item.itemsAll,
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        updatePlanningAction,
        getAllItemsAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditPlan);