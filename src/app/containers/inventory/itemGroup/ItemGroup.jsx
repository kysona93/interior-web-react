import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button, Label } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addItemGroupAction, getItemGroupsListAction, updateItemGroupAction, deleteItemGroupAction } from './../../../actions/inventory/itemGroup';

let groupId = 0;

class ItemGroup extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Item Group',
            status: [{id : 1, name : "Active"}, {id : 0, name : "In Active"}],
        };
        this.deleteItem = this.deleteItem.bind(this);
    }

    componentWillMount() {
        this.props.getItemGroupsListAction();
    }

    componentWillReceiveProps(data){
        if(data.addItemGroup.status === 200){
            alert("Successfully added new item group.");
            this.props.getItemGroupsListAction();
            data.addItemGroup.status = 0;
        }
        if(data.addItemGroup.status === 404 || data.addItemGroup.status === 500){
            alert("Fail with add item group!");
        }

        if(data.updateItemGroup.status === 200 ){
            alert("Successfully updated item group.");
            this.setState({
                label: 'Save',
                labelForm: 'Add New Item Group',
                update: false
            });
            this.handleCancel();
            this.props.getItemGroupsListAction();
            data.updateItemGroup.status = 0;
        }
        if(data.updateItemGroup.status === 400 || data.updateItemGroup.status === 500){
            alert("Fail with update item group!");
        }

        if(data.deleteItemGroup.status === 200){
            alert("Successfully deleted this item group.");
            this.props.getItemGroupsListAction();
            data.deleteItemGroup.status = 0;
        }
        if(data.deleteItemGroup.status === 400 || data.deleteItemGroup.status === 500){
            alert("Fail with delete this item group!");
        }
    }

    handleCancel(){
        this.setState({update: false, label: 'Save', labelForm: 'New Ampere'});
        this.props.dispatch(initialize('form_add_item_group', null));
    }

    updateItem(data){
        groupId = data.id;
        this.setState({
            update: true, 
            label: 'Edit', 
            labelForm: 'Edit Item Group'
        });
        this.props.dispatch(initialize('form_add_item_group', data));
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this item group?") === true){
            this.props.deleteItemGroupAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add
            let itemGroup = {
                "itemGroupEn": values.itemGroupEn,
                "itemGroupKh": values.itemGroupKh,
                "description": values.description,
                "status": Number(values.status)
            };
            this.props.addItemGroupAction(itemGroup);
        }else{
            // edit
            let itemGroup = {
                "id": groupId,
                "itemGroupEn": values.itemGroupEn,
                "itemGroupKh": values.itemGroupKh,
                "description": values.description,
                "status": Number(values.status)
            };
            this.props.updateItemGroupAction(itemGroup);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col md={6} >
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Item Group <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field 
                                                name="itemGroupEn" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Item Group" 
                                                icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Item Group Kh</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field 
                                                name="itemGroupKh" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Item Group Kh" 
                                                icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field 
                                                name="description" 
                                                type="text" 
                                                component={TextArea} 
                                                label="Description" 
                                                icon="fa fa-shopping-bag"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field 
                                                name="status" 
                                                type="select" 
                                                component={SelectBox} 
                                                placeholder="Select status" 
                                                values={this.state.status} 
                                                sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} className="pull-right">
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleCancel()}>Cancel</Button>
                                                </Col>
                                            : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Item Group</th>
                                    <th>Item Group Kh</th>
                                    <th>Description</th>
                                    <th>status</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                { this.props.ItemGroupsList.status == 200 ?
                                        this.props.ItemGroupsList.data.items.map((group,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{group.id}</td>
                                                    <td>{group.itemGroupEn}</td>
                                                    <td>{group.itemGroupKh}</td>
                                                    <td>{group.description}</td>
                                                    { group.status == 1 ? 
                                                        <td><Label bsStyle="primary">Enable</Label></td> : 
                                                        <td><Label bsStyle="danger">Disable</Label></td>
                                                    }
                                                    <td className="text-center">
                                                        <a className='btn btn-info btn-xs' onClick={() => this.updateItem(group)}>
                                                            <span className="glyphicon glyphicon-edit"></span>
                                                        </a>
                                                        &nbsp;
                                                        <a onClick={()=> this.deleteItem(group.id)} href="#" className="btn btn-danger btn-xs">
                                                            <span className="glyphicon glyphicon-remove"></span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr>
                                            <td colSpan={7}>RESULT NOT FOUND</td>
                                        </tr>
                                    }
                                </tbody>
                            </table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

ItemGroup = reduxForm({
    form: 'form_add_item_group',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.itemGroup) || values.itemGroup === undefined) {
            errors.itemGroup = 'Invalid Item Group!';
        }
        if (values.groupReportId == undefined) {
            errors.groupReportId = 'Group Report is required!';
        }
        if (values.isConnection === undefined) {
            errors.isConnection = 'Connection is required!';
        }
        return errors;
    }
})(ItemGroup);

function mapStateToProps(state) {
    return {
        addItemGroup: state.itemGroup.addItemGroup,
        ItemGroupsList: state.itemGroup.ItemGroupsList,
        updateItemGroup: state.itemGroup.updateItemGroup,
        deleteItemGroup: state.itemGroup.deleteItemGroup
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ 
        addItemGroupAction, 
        getItemGroupsListAction, 
        updateItemGroupAction, 
        deleteItemGroupAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ItemGroup);