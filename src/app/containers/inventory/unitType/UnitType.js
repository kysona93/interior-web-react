import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addUnitTypeAction, listAllUnitTypesAction, updateUnitTypeAction, deleteUnitTypeAction } from './../../../actions/inventory/unitType';

let unitId = 0;
class UnitType extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Unit Type'
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.listAllUnitTypesAction();
    }

    componentWillReceiveProps(data){
        if(data.addUnitType.status === 200){
            alert("Successfully added new unit type.");
            this.props.listAllUnitTypesAction();
            data.addUnitType.status = 0;
        }
        if(data.addUnitType.status === 404 || data.addUnitType.status === 500){
            alert("Fail with add unit type!");
        }

        if(data.updateUnitType.status === 200 ){
            alert("Successfully updated unit type.");
            this.setState({label: 'Save'});
            this.setState({labelForm: 'Add New Unit Type'});
            this.setState({update: false});
            this.props.listAllUnitTypesAction();
            data.updateUnitType.status = 0;
        }
        if(data.updateUnitType.status === 400 || data.updateUnitType.status === 500){
            alert("Fail with update unit type!");
        }

        if(data.deleteUnitType.status === 200){
            alert("Successfully deleted this unit type.");
            this.props.listAllUnitTypesAction();
            data.deleteUnitType.status = 0;
        }
        if(data.deleteUnitType.status === 400 || data.deleteUnitType.status === 500){
            alert("Fail with delete this unit type!");
        }
    }

    handleBack(){
        location.href = "/app/inventory/unit-types";
    }

    updateItem(data){
        unitId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Unit Type'});
        this.props.dispatch(initialize(
            'form_add_unit_type',
            data,
            ['name', 'description', 'status'])
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this unit type?") === true){
            this.props.deleteUnitTypeAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add
            let unitType = {
                "description": values.description,
                "name": values.name,
                "status": 1
            };
            this.props.addUnitTypeAction(unitType);
        }else{
            // edit
            let unitType = {
                "id": unitId,
                "description": values.description,
                "name": values.name,
                "status": 1
            };
            this.props.updateUnitTypeAction(unitType);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Unit Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="name" type="text" component={TextBox} label="Unit type" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-shopping-bag"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Unit Type</th>
                                    <th>Description</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllUnitType.data != undefined ?
                                        <tbody>
                                        { this.props.listAllUnitType.data.items.map((unit,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{unit.id}</td>
                                                    <td>{unit.name}</td>
                                                    <td>{unit.description}</td>
                                                    <td className="text-center">
                                                         <a className='btn btn-info btn-xs' onClick={() => this.updateItem(unit)}>
                                                               <span className="glyphicon glyphicon-edit"></span>
                                                               </a>
                                                               &nbsp;
                                                               <a onClick={()=> this.deleteItem(unit.id)} href="#" className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                    </a>
                                                         </td>
                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>
            </div>
        )
    }
}

UnitType = reduxForm({
    form: 'form_add_unit_type',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;

        const errors = {};
        if (!regex_name.test(values.name) || values.name === undefined) {
            errors.name = 'Unit Type is required and at least 2 character!';
        }
        return errors;
    }
})(UnitType);

function mapStateToProps(state) {
    //console.log(state.unitType.listAllUnitType);
    return {
        addUnitType: state.unitType.addUnitType,
        listAllUnitType: state.unitType.listAllUnitType,
        updateUnitType: state.unitType.updateUnitType,
        deleteUnitType: state.unitType.deleteUnitType
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addUnitTypeAction, listAllUnitTypesAction, updateUnitTypeAction, deleteUnitTypeAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(UnitType);