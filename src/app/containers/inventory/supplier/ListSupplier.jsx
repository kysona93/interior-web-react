import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Form, FormGroup, Button, Label } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getMenusListAndPageAction } from './../../../actions/menu';
import { getAllSupplierAction, deleteSupplierAction } from './../../../actions/inventory/supplier';
import Pagination from '../../../components/forms/Pagination';
import { canCreate, canUpdate, canDelete } from './../../../../auth';
import EditSupplier from './EditSupplier';

let supplier = {
    limit: 10,
    page: 1,
    keyword: ''
};

class ListSupplier extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            supplier: {},
            isEdit: false
        }
        this.handleDeleteSupplier = this.handleDeleteSupplier.bind(this);
    }

    componentWillMount(){
        this.props.getAllSupplierAction(supplier);
    }

    handleSubmit(values){
        if(values.keyword !== undefined){
            supplier.keyword = values.keyword;
        } else {
            supplier.keyword = "";
        }
        this.props.getAllSupplierAction(supplier);
    }

    handleEditSupplier(object) {
        this.setState({ 
            supplier: object,
            isEdit: true
        });
    }

    handleIsEdit(value) {
        this.setState({ isEdit: value });
    }

    componentWillReceiveProps(newProps){
        if(newProps.supplierDelete.status === 200){
            alert("Successfully delete supplier.");
            this.props.getAllSupplierAction(supplier);
            return newProps.supplierDelete.status = 0;
        }
        if(newProps.supplierDelete.status === 500 || newProps.supplierDelete.status === 400){
            alert("Fail with delete supplier!");
            return newProps.supplierDelete.status = 0;
        }
    }

    handleDeleteSupplier(id){
        if(confirm("Are you sure to delete this supplier?") == true){
            this.props.deleteSupplierAction(id);
        }
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;

        return (
            this.state.isEdit == false ?
                <div className="margin_left_25 margin_top_minus_30">
                    <Row>
                        <Col md={12}>
                            <h3 className="text-align-center">Supplier Management</h3>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} sm={12} md={11}>
                            <div className="">
                                <Row>
                                    <Col md={8}>
                                        {
                                            canCreate == true ?
                                                <Button 
                                                    className="btn btn-default"
                                                    onClick={() => browserHistory.push('/app/inventory/supplier/add')}>
                                                    <i className="fa fa-plus" />
                                                    &nbsp;&nbsp;Add Supplier
                                                </Button>
                                            : null
                                        }
                                    </Col>
                                    <Col md={4}>
                                        <Form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
                                            <Field 
                                                name="keyword" 
                                                type="text" 
                                                component={TextBox} 
                                                label="Search keyword"
                                                className="keysearch pull-right"
                                                icon="fa fa-search"/>
                                        </Form>
                                    </Col>
                                </Row>

                                <table className="list-smaller-table">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Supplier Name En</th>
                                            <th>Supplier Name Kh</th>
                                            <th>Supplier Type</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>Status</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    { this.props.supplierAll.status == 200 ?
                                        this.props.supplierAll.data.items.list.map((supplier, index) => {
                                            total = this.props.supplierAll.data.items.totalCount;
                                            return (
                                                <tr key={index}>
                                                    <td>{supplier.id}</td>
                                                    <td>{supplier.name}</td>
                                                    <td>{supplier.nameKh}</td>
                                                    <td>{supplier.supplierType}</td>
                                                    <td>{supplier.phone}</td>
                                                    <td>{supplier.email}</td>
                                                    <td>{supplier.address}</td>
                                                    { supplier.status == 1 ? 
                                                        <td><Label bsStyle="primary">Enable</Label></td> : 
                                                        <td><Label bsStyle="danger">Disable</Label></td>
                                                    }
                                                    <td className="text-center">
                                                        {
                                                            canUpdate == true ? 
                                                                <a onClick={() => this.handleEditSupplier(supplier)} title="Edit" className='btn btn-info btn-xs'>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </a>
                                                            : null
                                                        }
                                                        {
                                                            canDelete == true ? 
                                                                <a onClick={() => this.handleDeleteSupplier(supplier.id)} title="Edit" className='btn btn-danger btn-xs'>
                                                                    <span className="glyphicon glyphicon-trash"></span>
                                                                </a>
                                                            : null
                                                        }
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    : <tr>
                                        <td colSpan={9} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                    </tr> }
                                    </tbody>
                                </table>

                                <Pagination 
                                    totalCount = {total}
                                    items = {supplier}
                                    callBackAction = {this.props.getAllSupplierAction}
                                />

                            </div>
                        </Col>
                    </Row>
                </div>
                : 
                <EditSupplier object={this.state.supplier} handleIsEdit={this.handleIsEdit.bind(this)} />
        )
    }
}

ListSupplier = reduxForm({
    form: 'form_supplier_list',
    validate: (values) => {
        const errors = {};
        return errors
    }
})(ListSupplier);

function mapStateToProps(state) {
    return ({
        supplierAll: state.supplier.supplierAll,
        supplierDelete: state.supplier.supplierDelete
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllSupplierAction, 
        deleteSupplierAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListSupplier);