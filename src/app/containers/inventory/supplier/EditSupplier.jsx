import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, reset, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { updateSupplierAction } from './../../../actions/inventory/supplier';
import { getAllSupplierAction } from './../../../actions/inventory/supplier';

class EditSupplier extends React.Component {
    constructor(props){
        super(props);
        this.state={
            status: [{id: 1, name: "Active"},{id: 2, name: "InActive"}],
            supplierType : [{id: 'ACCESSORY', name: 'Accessory'}, {id: 'ELECTRICITY', name: 'Electricity'}],
            supplierId: 0
        };
    }

    componentWillMount(){
        let supplier  =  {
            "supplierName": this.props.object.name,
            "supplierNameKh": this.props.object.nameKh,
            "address": this.props.object.address,
            "description": this.props.object.description,
            "phone": this.props.object.phone,
            "email": this.props.object.email,
            "status": this.props.object.status,
            "supplierType": this.props.object.supplierType
        }

        this.props.dispatch(initialize('form_edit_supplier', supplier));

        this.setState({ supplierId: this.props.object.id });

        if (this.props.object.supplierType == "ELECTRICITY") {
            this.setState({ isTypeElectricity: true });
        }
    }

    componentWillReceiveProps(newProps){
        if(newProps.supplierUpdate.status === 200){
            if(newProps.supplierUpdate.data.status === 200) {
                let pagination = {
                    limit: 10,
                    page: 1,
                    keyword: ''
                }
                this.props.getAllSupplierAction(pagination);
                alert("Successfully update supplier.");
                this.props.handleIsEdit(false);
                newProps.supplierUpdate.data.status = 0;
            } else {
                alert("Fail with update supplier!");
                newProps.supplierUpdate.data.status = 0;
            }
            newProps.supplierUpdate.status = 0;
        }
        if(newProps.supplierUpdate.status === 404 || newProps.supplierUpdate.status === 500){
            alert("Fail with update supplier!");
            newProps.supplierUpdate.status = 0;
        }
    }

    handleSubmit(values){
        let supplier = {
            "address": values.address,
            "description": values.description,
            "email": values.email,
            "phone": values.phone,
            "status": Number(values.status),
            "name": values.supplierName,
            "nameKh": values.supplierNameKh,
            "supplierType": values.supplierType,
            "id": this.state.supplierId
        };
        this.props.updateSupplierAction(supplier);
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;

        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Edit Supplier</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Supplier Type <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.supplierType} 
                                                        sortBy="name"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Suppler Name En <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierName" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="supplier Name" 
                                                        icon="fa fa-address-book"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Supplier Name Kh</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierNameKh" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Supplier Name Kh" 
                                                        icon="fa fa-address-book"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Phone </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="phone" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Phone" 
                                                        icon="fa fa-phone"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Email</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="email" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Email" 
                                                        icon="fa fa-envelope"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Address</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="address" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={80}
                                                        label="Address" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            
                                        </Col>
                                            
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={80}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="status" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Status" 
                                                        values={this.state.status} 
                                                        sortBy="name" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Save" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.props.handleIsEdit(false)}>
                                                Cancel
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_edit_supplier'));

EditSupplier = reduxForm({
    form: 'form_edit_supplier',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_phone = /^0\d{8,9}$/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex_number = /[0-9]{1,10}/;

        const errors = {};
        if(values.supplierName === ""){
            errors.supplierName = 'Supplier Name is required!';
        }
        if(values.phone !== ""){
            if (!regex_phone.test(values.phone)) {
                errors.phone = 'Invalid Phone!';
            }
        }
        if(values.email !== ""){
            if (!regex_email.test(values.email)) {
                errors.email = 'Invalid Email!';
            }
        }
        if(values.supplierType === ""){
            errors.supplierType = 'Supplier Type is required!';
        }
        if(values.status === ""){
            errors.status = 'Status is required!';
        }

        return errors
    }
})(EditSupplier);

function mapStateToProps(state) {
    return {
        supplierUpdate: state.supplier.supplierUpdate,
        initialValues: {
            "supplierName": "",
            "supplierNameKh": "",
            "address": "",
            "description": "",
            "phone": "",
            "email": "",
            "status": 0,
            "supplierType": ""
        }
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        updateSupplierAction,
        getAllSupplierAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(EditSupplier);