import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import { TextArea } from '../../../components/forms/TextArea';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addSupplierAction } from './../../../actions/inventory/supplier';

class AddSupplier extends React.Component {
    constructor(props){
        super(props);
        this.state={
            status: [{id: 1, name: "Active"},{id: 2, name: "InActive"}],
            supplierType : [{id: 'ACCESSORY', name: 'Accessory'}, {id: 'ELECTRICITY', name: 'Electricity'}],
        };
    }

    componentWillMount(){
    }

    componentWillReceiveProps(newProps){
        if(newProps.supplierAdd.status === 200){
            if(newProps.supplierAdd.data.status === 200){
                alert("Successfully added new supplier.");
                newProps.supplierAdd.data.status = 0;
            } else {
                alert("Fail with add new supplier!");
                newProps.supplierAdd.data.status = 0;
            }
            newProps.supplierAdd.status = 0;
        }
        if(newProps.supplierAdd.status === 404 || newProps.supplierAdd.status === 500){
            alert("Fail with add new supplier!");
            newProps.supplierAdd.status = 0;
        }
    }

    handleSubmit(values){
        let supplier = {
            "address": values.address,
            "description": values.description,
            "email": values.email,
            "phone": values.phone,
            "status": Number(values.status),
            "name": values.supplierName,
            "nameKh": values.supplierNameKh,
            "supplierType": values.supplierType
        };
        this.props.addSupplierAction(supplier);
    }

    render(){
        const {handleSubmit, error, invalid, submitting, reset} = this.props;
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Add Supplier</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Supplier Type <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierType" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Type" 
                                                        values={this.state.supplierType} 
                                                        sortBy="name" />
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Suppler Name En <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierName" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="supplier Name" 
                                                        icon="fa fa-address-book"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Supplier Name Kh</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="supplierNameKh" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Supplier Name Kh" 
                                                        icon="fa fa-address-book"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Phone </strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="phone" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Phone" 
                                                        icon="fa fa-phone"/>
                                                </Col>
                                            </Row>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Email</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="email" 
                                                        type="text" 
                                                        component={TextBox} 
                                                        label="Email" 
                                                        icon="fa fa-envelope"/>
                                                </Col>
                                            </Row>
                                            
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Address</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="address" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={80}
                                                        label="Address" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                        </Col>

                                        <Col md={6} lg={6}>
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Description</strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="description" 
                                                        type="text" 
                                                        component={TextArea} 
                                                        height={80}
                                                        label="Description" 
                                                        icon="fa fa-user-circle"/>
                                                </Col>
                                            </Row>
                                            
                                            <Row>
                                                <Col md={4} lg={4} className="label-name">
                                                    <strong>Status <span className="label-require">*</span></strong>
                                                </Col>
                                                <Col md={8} lg={8}>
                                                    <Field 
                                                        name="status" 
                                                        type="select" 
                                                        component={SelectBox} 
                                                        placeholder="Select Status" 
                                                        values={this.state.status} 
                                                        sortBy="name" 
                                                        icon=""/>
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Save" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => window.history.back()}>
                                                Back
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
    dispatch(reset('form_add_supplier'));

AddSupplier = reduxForm({
    form: 'form_add_supplier',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_phone = /^0\d{8,9}$/;
        let regex_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        let regex_number = /[0-9]{1,10}/;

        const errors = {};
        if(values.supplierName === undefined){
            errors.supplierName = 'Supplier Name is required!';
        }
        if(values.phone !== undefined){
            if (!regex_phone.test(values.phone)) {
                errors.phone = 'Invalid Phone!';
            }
        }
        if(values.email !== undefined){
            if (!regex_email.test(values.email)) {
                errors.email = 'Invalid Email!';
            }
        }
        if(values.supplierType === undefined){
            errors.supplierType = 'Supplier Type is required!';
        }
        if(values.status === undefined){
            errors.status = 'Status is required!';
        }

        return errors
    }
})(AddSupplier);

function mapStateToProps(state) {
    return {
        supplierAdd: state.supplier.supplierAdd
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        addSupplierAction 
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddSupplier);