import React from 'react';
import {Image, Row, Col} from 'react-bootstrap';
import './bodyHome.css';

class BodyHome extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="container">
                <div style={{marginTop:"80px"}}>
                    <h1 className="cmp-align-center title-text">
                        WHO WE ARE ?
                    </h1>
                    <h3 className="desc-text">
                        Apsara Home & Furniture is a local firm of architects, designers, strategists, and specialists.
                    </h3>
                    <h3 className="desc-text">
                        We focus exclusively on environments through the lens of interior architecture—a radical idea in 2013, when APSARA was founded. We are highly connected agents of change, committed to creativity, innovation, growth, & our communities.
                    </h3>
                    <h3 className="desc-text">
                        We’re inspired working alongside our clients to resolve complex issues and design highly energized environments where people thrive. Through our work we strive to share our passion with our clients.
                    </h3>
                    <div style={{marginTop:'50px'}}>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/we1.jpg" responsive style={{width:'555px', height:'416px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/we2.jpg" responsive style={{width:'555px', height:'416px'}}/>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div style={{marginTop:"80px"}}>
                    <h1 className="cmp-align-center title-text">OUR SERVICES</h1>
                    <h3 className="desc-text ">
                        Apsara Home & Furniture provides clients many choices to design their passion and build what their love.
                    </h3>
                    <h3 className="desc-text ">
                        Our teams use intelligent-design methods to solve some of the most complex projects.
                        We also have awesome people with highly experiences to construct the highest quality structures across the sectors.
                    </h3>
                    <h3 className="desc-text ">
                        We place ourselves in designing, construction and leading to better, more beautiful, cost-efficient projects.
                    </h3>
                    <div style={{marginTop:'50px'}}>
                        <Row>
                            <Col sm={12} md={12}>
                                <Image style={{width:'100%',height:'500px'}} src="images/service1.jpg" responsive />
                            </Col>
                        </Row>
                    </div>
                    <div style={{marginTop:'30px'}}>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/service2.jpg" responsive style={{width:'555px', height:'416px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/service3.jpg" responsive style={{width:'555px', height:'416px'}}/>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div style={{marginTop:"80px"}}>
                    <h1 className="cmp-align-center title-text">WHAT DO WE OFFER ?</h1>
                    <h3 className="desc-text">
                        It is our pleasure to provide high-class interior design services to our clients.
                    </h3>
                    <h3 className="desc-text">
                        We are confident to make your place look beautiful and comfortable with our creative design methodologies.
                    </h3>
                    <div style={{marginTop:'50px'}}>
                        <Row>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer1.jpg" responsive />
                            </Col>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer2.jpg" responsive />
                            </Col>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer3.jpg" responsive />
                            </Col>
                        </Row>
                        <Row style={{marginTop:'30px'}}>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer4.jpg" responsive />
                            </Col>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer5.jpg" responsive />
                            </Col>
                            <Col sm={6} md={4}>
                                <Image style={{width:'100%',height:'350px'}} src="images/offer6.jpg" responsive />
                            </Col>
                        </Row>
                    </div>
                </div>
                <div style={{marginTop:"80px"}}>
                    <h1 className="cmp-align-center title-text" style={{marginTop:'50px'}}>OUR VISION</h1>
                    <div style={{marginTop:'50px', marginBottom:'20px'}}>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image style={{width:'100%',height:'400px'}} src="images/vision.jpg" responsive />
                            </Col>
                            <Col sm={6} md={6}>
                                <h3 className="vision-text" style={{marginTop:'0px'}}>
                                    We are proud to see our clients satisfy with our services and products.
                                </h3>
                                <h3 className="vision-text" style={{marginTop:'0px'}}>
                                    We stay focus on interior design solutions and strategies.
                                </h3>
                                <h3 className="vision-text" style={{marginTop:'0px'}}>
                                    Our team commit to work more harder and put all our effort to give our clients with the best services and products.
                                </h3>
                                <h3 className="vision-text" style={{marginTop:'0px'}}>
                                    We continue to focus more in interior field to find the excellent things for the clients in the future.
                                </h3>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
        );
    }
}
export default BodyHome;