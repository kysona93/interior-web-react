import React from 'react';
import {connect} from 'react-redux';
import { Link } from 'react-router'
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllMeterHistoryAction } from './../../../actions/customer/network/meter';
import { loadCustomer } from '../../../localstorages/localstorage';
import moment from 'moment';
class MeterHistory extends React.Component{

    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.getAllMeterHistoryAction(1);
    }
    componentWillReceiveProps(){

    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                    <div className="custyle">
                        <table className="table table-bordered table-striped custab">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Customer Name</th>
                                <th>Changed Date</th>
                                <th>Meter Serial</th>
                                <th>Ampere</th>
                                <th>Meter Type</th>
                                <th>From Customer</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.meterHistoryAll.response !== undefined ?
                                this.props.meterHistoryAll.response.items.map((history, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{history.customerId}</td>
                                            <td>{"Customer name"}</td>
                                            { history.isTransfer == 1 ? <td> </td>: <td>{moment(history.changeDate).format("YYYY-MM-DD")}</td> }
                                            { history.isTransfer == 1 ? <td> </td>: <td>{history.oldMeterSerial} <img src="/icons/right_arrow_small.png"/>{history.newMeterSerial}</td> }
                                            { history.isTransfer == 1 ? <td> </td>: <td>{history.fromAmpere} <img src="/icons/right_arrow_small.png"/> {history.toAmpere}</td> }
                                            { history.isTransfer == 1 ? <td> </td>: <td>{history.fromMeterType} <img src="/icons/right_arrow_small.png"/> {history.toMeterType}</td> }
                                            { history.isTransfer == 1 ? <td> {history.customerName} <img src="/icons/right_arrow_small.png"/> {history.transferTo} </td>: <td> </td> }
                                            <td>{history.description}</td>
                                        </tr>
                                    )
                                })
                                :
                                <tr>
                                    <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                </tr>
                            }
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>
        )
    }
}

function mapStateToProps(state) {
    return {
        meterHistoryAll: state.changeMeter.meterHistoryAll
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllMeterHistoryAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MeterHistory);