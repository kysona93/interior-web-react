import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import './../../../index.css';
import './../../../administration/index.css';
import {escapeRegexCharacters} from '../../../../utils/EscapeRegexCharacters';
import {clean} from '../../../../utils/ArrayFunction';
import {getEmployeesAction} from '../../../../actions/employee/employee';
import { getBoxesAction } from './../../../../actions/setup/box';
import { getPolesAction } from './../../../../actions/setup/pole';
import { getAllBreakersAction } from './../../../../actions/setup/breaker';
import { listAllBillingItemsAction } from '../../../../actions/inventory/item';
import { getFreeMetersAction } from './../../../../actions/customer/network/meter';
import { updateInstallingAction } from  '../../../../actions/customer/network/installation';
import { loadCustomer } from '../../../../localstorages/localstorage';
import { removeDuplicate, compare } from '../../../../utils/ArrayFunction';
import moment from 'moment';
let oldMeters = [];
let oldBreakers = [];
let oldMeter = {};
let oldBreaker = {};

let meterIds = [];
let meterSerials = [];
let breakerIds = [];
let breakerSerials = [];

class EditInstalling extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            installingDate: moment(loadCustomer().installDate) || null,
            room: "",
            count: 1,
            employees: [],
            boxes: [],
            poles: [],
            transformers: [],
            meters: [],
            breakers: [],
            meter: [],
            breaker: [],
            items: [],
            meterId: [],
            meterSerial: [],
            breakerId: [],
            breakerSerial: [],
            oldItems: []
        };
        this.handleInstallingDate = this.handleInstallingDate.bind(this);
        this.handleSelectArea = this.handleSelectArea.bind(this);
        this.handleSelectPole = this.handleSelectPole.bind(this);
        this.handleSelectBox = this.handleSelectBox.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRemoveItem = this.handleRemoveItem.bind(this);
        this.handleAddItem = this.handleAddItem.bind(this);
    }

    removeDuplicate (list) {
        return list.filter((thing, index, self) => self.findIndex((t) => {return t.id === thing.id && t.name === thing.name; }) === index)
    }

    componentWillMount(){
        this.props.getEmployeesAction({
            position: '',
            name: '',
            pageSize: 10,
            nowPage: 1,
            orderBy: "id"
        });
        this.props.listAllBillingItemsAction({
            itemType: "",
            isSaleItem: 1,
            check: ""
        });
        this.props.getAllBreakersAction();
        this.props.getFreeMetersAction();
    }

    componentDidMount(){
        if(loadCustomer() !== undefined){
            loadCustomer().meters.map((meter) => {
                oldMeters.push({
                    id: meter.id,
                    name: meter.meterSerial
                })
            });
            loadCustomer().breakers.map((breaker) => {
                oldBreakers.push({
                    id: breaker.id,
                    name: breaker.breakerSerial
                })
            });
            const oldItems = loadCustomer().connectionDetails.filter(item => !escapeRegexCharacters("meter").test(item.itemNameEn))
                .filter(item => !escapeRegexCharacters("breaker").test(item.itemNameEn));
            this.setState({oldItems: oldItems});
            for(let i=0; i< oldItems.length; i++){
                this.props.dispatch(change('form_edit_installing', 'item' + i, oldItems[i].billingItemId));
                this.props.dispatch(change('form_edit_installing', 'serial' + i, oldItems[i].serial));
                this.props.dispatch(change('form_edit_installing', 'quantity' + i, oldItems[i].quantity));
                this.props.dispatch(change('form_edit_installing', 'status' + i, oldItems[i].status));
            }
        }
    }

    componentWillReceiveProps(data){
        if(data.installingUpdate.status === 200){
            alert("Successful add installing !!");
        } else if(data.installingUpdate.status === (400 || 404) || data.installingUpdate.status === (500 || 502)){
            alert(data.installingUpdate.status + " Error !! \nFail add installing !!\n"+data.installingUpdate.message);
        } else if(data.installingUpdate.status === 401){
            alert("Please Login !!");
        }
        data.installingUpdate.status = 0;

        let billingItems = [];
        let items = [];
        let meter = [];
        let breaker = [];
        let poles = [];
        let boxes = [];
        let meters = [];
        let breakers = [];
        let employees = [];
        if (data.billingItems.items !== undefined) {
            data.billingItems.items.map((billing) => {
                billingItems.push({
                    id: billing.id,
                    name: billing.itemNameEn
                })
            });
        }
        if (billingItems.length > 0) {
            items = billingItems.filter(item => !escapeRegexCharacters("meter").test(item.name))
                .filter(item => !escapeRegexCharacters("breaker").test(item.name));
            meter = billingItems.filter(item => escapeRegexCharacters("meter").test(item.name));
            breaker = billingItems.filter(item => escapeRegexCharacters("breaker").test(item.name));
            this.setState({items: items, meter: meter, breaker: breaker})
        }
        if(data.boxes.items !== undefined){
            data.boxes.items.list.map((box) => {
                boxes.push({
                    id: box.id + '-' + box.room + '-' + box.available,
                    name: box.serial
                })
            })
        }
        if(data.poles.items !== undefined){
            data.poles.items.list.map((pole) => {
                poles.push({
                    id: pole.id,
                    name: pole.serial
                })
            });
        }
        if(data.meters.items !== undefined){
            data.meters.items.map((meter) => {
                meters.push({
                    id: meter.id,
                    name: meter.serial
                })
            })
        }
        if(data.breakers.items !== undefined){
            data.breakers.items.map((breaker) => {
                breakers.push({
                    id: breaker.id,
                    name: breaker.breakerSerial
                })
            });
        }
        if(data.employees.items !== undefined){
            data.employees.items.list.map((emp) => {
                employees.push({
                    id: emp.id,
                    name: emp.nameKh
                })
            });
        }
        this.setState({
            poles: poles,
            boxes: boxes,
            meters: this.removeDuplicate(oldMeters.concat(meters)),
            breakers: this.removeDuplicate(oldBreakers.concat(breakers)),
            employees: employees
        });
    }

    handleInstallingDate(date) {
        this.setState({installingDate: date});
    }

    handleSelectArea(e){
        this.setState({room: ""});
        this.props.getPolesAction({
            area: Number(e.target.value),
            transId: 0,
            serial: "",
            pageSize: 1000,
            nowPage: 1,
            orderBy: "id"
        })
    }

    handleSelectPole(e){
        this.setState({room: ""});
        this.props.getBoxesAction({
            serial: '',
            id: e.target.value,
            pageSize: 1000,
            nowPage: 1,
            orderBy: "id"
        });
    }

    handleSelectBox(e){
        this.setState({room: e.target.value});
    }

    handleExistedItems(){
        let fields = [];
        for(let i = 0; i < this.state.oldItems.length; i++){
            fields.push(
                <div key={i}>
                    <Row>
                        <Col md={3} lg={3}>
                            <Field name={"item" + i} type="select" component={SelectBox}  values={this.state.items} placeholder="Please select ..." sortBy="name" />
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"serial" + i} type="text" component={TextBox} label="Serial"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"quantity" + i} type="text" component={TextBox} label="Quantity"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={ "status" + i } type="select" component={SelectBox}
                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                        </Col>
                    </Row>
                </div>
            )
        }
        return fields || null;
    }

    handleItems(){
        let dynamicFields = [];
        for(let i = 1; i < this.state.count; i++){
            dynamicFields.push(
                <div key={i}>
                    <Row>
                        <Col md={3} lg={3}>
                            <Field name={"addItem" + i} type="select" component={SelectBox}  values={this.state.items} placeholder="Please select ..." sortBy="name" />
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"addSerial" + i} type="text" component={TextBox} label="Serial"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"addQuantity" + i} type="text" component={TextBox} label="Quantity"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={ "addStatus" + i } type="select" component={SelectBox}
                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                        </Col>
                    </Row>
                </div>
            )
        }
        return dynamicFields || null;
    }

    handleAddItem(){
        this.setState({count: this.state.count + 1});
    }

    handleRemoveItem() {
        this.setState({count: this.state.count - 1});
        this.props.dispatch(change('form_edit_installing', 'addItem' + (this.state.count - 1), ''));
    }

    handleSubmit(value){
        let newItems = [];
        for(let i=0; i<this.state.oldItems.length; i++){
            newItems.push({
                id: this.state.oldItems[i].id,
                billingItemId: Number(value[`item${i}`]),
                quantity: value[`quantity${i}`] !== undefined ? Number(value[`quantity${i}`]) : this.state.oldItems[i].quantity,
                serial: value[`serial${i}`] !== undefined ? value[`serial${i}`] : this.state.oldItems[i].serial,
                status: value[`status${i}`] !== undefined ? value[`status${i}`] : this.state.oldItems[i].status
            })
        }
        const newMeter = {
            id: oldMeter.id,
            billingItemId: Number(value.meterId),
            quantity: Number(value.meterQuantity),
            serial: meterSerials.map((id) => {return id + ' '}).toString().trim(),
            status: value.meterStatus
        };
        const newBreaker = {
            id: oldBreaker.id,
            billingItemId: Number(value.breakerId),
            quantity: Number(value.breakerQuantity),
            serial: breakerSerials.map((id) => {return id + ' '}).toString().trim(),
            status: value.breakerStatus
        };
        let addItems = [];
        if(this.state.count > 1){
            for(let i=1; i<=this.state.count-1; i++){
                addItems.push({
                    billingItemId: Number(value[`addItem${i}`]),
                    quantity: value[`addQuantity${i}`] !== undefined ? Number(value[`addQuantity${i}`]) : 1,
                    serial: value[`addSerial${i}`] !== undefined ? value[`addSerial${i}`] : '',
                    status: value[`addStatus${i}`] !== undefined ? value[`addStatus${i}`] : 'New'
                })
            }
        }
        let oldMeterIds = [];
        let oldBreakerIds = [];
        if(loadCustomer().meters !== undefined){
            loadCustomer().meters.map((meter) => {
                oldMeterIds.push(meter.id)
            })
        }
        if(loadCustomer().breakers !== undefined){
            loadCustomer().breakers.map((breaker) => {
                oldBreakerIds.push(breaker.id)
            })
        }
        this.props.updateInstallingAction({
            id: loadCustomer().connectionId,
            breakerIdMore: breakerIds,
            breakerIds: oldBreakerIds,
            connectionDetailMore: addItems,
            connectionDetails: newItems.concat([newMeter, newBreaker]),
            customerId: loadCustomer().id,
            description: value.description,
            employeeId: Number(value.employeeId),
            installDate: value.installingDate !== undefined ? value.installingDate : moment(loadCustomer().installingDate).format("YYYY-MM-DD"),
            installingStatusId: Number(value.installingStatus),
            meterIdMore: meterIds,
            meterIds: oldMeterIds,
            oldBoxId: loadCustomer().boxId,
            boxId: Number(value.boxId.split("-")[0]),
            room: Number(value.room)
        });
    }

    closeUpdate(){
        oldMeters = [];
        oldBreakers = [];
        this.props.closeUpdate(false);
        this.props.dispatch(initialize('form_edit_installing', null));
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        let areas = [];
        if(this.props.areaAll.items !== undefined){
            this.props.areaAll.items.map((area) => {
                areas.push({
                    id: area.id,
                    name: area.areaNameEn
                })
            });
        }else {areas = []}

        return(
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Installing</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Install By <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="employeeId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={this.state.employees} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Start Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="installingDate" component={DateTimePicker} defaultDate={this.state.installingDate}  handleChange={this.handleInstallingDate} placeholder="Installing date" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="installingStatus" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[{id: 1, name: "Installing"},{id: 2, name: "Installed"},{id: 3, name: "Re-install"}]} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Area (Transformer)</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="areaId" type="select" onChange={this.handleSelectArea} component={SelectBox} placeholder="Please select ..." values={areas} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Pole</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="poleId" type="select" onChange={this.handleSelectPole} component={SelectBox} placeholder="Please select ..." values={this.state.poles} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Box No</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="boxId" type="select" onChange={this.handleSelectBox} component={SelectBox} placeholder="Please select ..." values={this.state.boxes} />
                                        </Col>
                                    </Row>
                                    { this.state.room === "" ? null :
                                        <Row>
                                            <Col lgOffset={8} lg={4}>
                                                <p style={{color: 'blue'}}>Total room(s) : {this.state.room.split("-")[1]}</p>
                                                <p style={{color: 'blue'}}>Available room(s) : {this.state.room.split("-")[2]}</p>
                                            </Col>
                                        </Row>
                                    }
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Room No</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="room" type="text" component={TextBox} label="Room no" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <div className="wrap-form-items-add">
                                <div className="header">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Items </strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Serial</strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Quantity</strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Status</strong>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={3} lg={3}>
                                            <Field name="meterId" type="select" component={SelectBox}  values={this.state.meter} disabled={true} placeholder="Please select ..." />
                                        </Col>
                                        <Col md={4} lg={4}>
                                            <Typeahead
                                                clearButton
                                                selected={this.removeDuplicate(oldMeters)}
                                                onChange={selected => { meterIds = _.map(selected, "id"); meterSerials = _.map(selected, "name"); }}
                                                labelKey="name"
                                                multiple
                                                options={this.state.meters}
                                                placeholder="Meter serial ..."
                                            />
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <Field name="meterQuantity" type="text" component={TextBox} label="Quantity"/>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="meterStatus" type="select" component={SelectBox}
                                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3}>
                                            <Field name="breakerId" type="select" component={SelectBox}  values={this.state.breaker} disabled={true} placeholder="Please select" />
                                        </Col>
                                        <Col md={4} lg={4}>
                                            <Typeahead
                                                clearButton
                                                selected={this.removeDuplicate(oldBreakers)}
                                                onChange={selected => {breakerIds = _.map(selected, "id"); breakerSerials = _.map(selected, "name")}}
                                                labelKey="name"
                                                multiple
                                                options={this.state.breakers}
                                                placeholder="Breaker serial ..."
                                            />
                                        </Col>
                                        <Col md={2} lg={2}>
                                            <Field name="breakerQuantity" type="text" component={TextBox} label="Quantity"/>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="breakerStatus" type="select" component={SelectBox}
                                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                                        </Col>
                                    </Row>
                                    <div className="items-detail">
                                        <Row>
                                            <Col md={12} lg={12}>
                                                {this.handleExistedItems()}
                                                <br />
                                                {this.handleItems()}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={1} lg={1}>
                                                <Button bsStyle="primary" onClick={this.handleAddItem}>Add Item</Button>
                                            </Col>
                                            <Col md={1} lg={1}>
                                                <Button bsStyle="primary" onClick={this.handleRemoveItem}>Remove</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={2} lg={2}>
                                    <Button bsStyle="primary" style={{width: '100%'}} onClick={this.closeUpdate.bind(this)}><i className="fa fa-angle-double-left">&nbsp;Back</i></Button>
                                </Col>
                                <Col md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

EditInstalling = reduxForm({
    form: 'form_edit_installing',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.employeeId === ""){
            errors.employeeId = 'Please select Installer !!';
        }
        if(values.installingStatus === ""){
            errors.installingStatus = "Please select Installing status !!";
        }
        if(values.meterId === ""){
            errors.meterId = "Please select item meter !!";
        }
        if(!number.test(values.meterQuantity)){
            errors.meterQuantity = 'Please input number only !!';
        }
        if(values.meterStatus === ""){
            errors.meterStatus = "Please select item status !!";
        }
        if(values.breakerId === ""){
            errors.breakerId = "Please select item breaker !!";
        }
        if(!number.test(values.breakerQuantity)){
            errors.breakerQuantity = 'Please input number only !!';
        }
        if(values.breakerStatus === ""){
            errors.breakerStatus = "Please select item status !!";
        }
        if(values.boxId === ""){
            errors.boxId = 'Please select Box !!';
        }
        if(!number.test(values.room)){
            errors.room = 'Please input number only !!';
        }
        return errors
    },
})(EditInstalling);

function mapStateToProps(state) {
    oldMeter = loadCustomer().connectionDetails.find(item => escapeRegexCharacters("meter").test(item.itemNameEn));
    oldBreaker = loadCustomer().connectionDetails.find(item => escapeRegexCharacters("breaker").test(item.itemNameEn));
    return {
        employees: state.employee.employees,
        transformerAll: state.transformer.transformerAll,
        areaAll: state.area.areaAll,
        boxes: state.box.boxes,
        poles: state.pole.poles,
        breakers: state.breaker.breakerAll,
        meters: state.changeMeter.meterFree,
        billingItems: state.billingItem.listAllIBillingItems,
        installingUpdate: state.installing.installingUpdate,
        initialValues: {
            employeeId: loadCustomer().employeeId || '',
            installingStatus: loadCustomer().installingStatus || '',
            description: loadCustomer().connectionDescription || '',
            areaId: loadCustomer().areaId || '',
            poleId: loadCustomer().poleId || '',
            boxId: loadCustomer().boxId + '-' + loadCustomer().boxRoom + '-' + loadCustomer().boxAvailable || '',
            room: loadCustomer().room || '',
            meterId: oldMeter !== undefined ? oldMeter.billingItemId : '',
            meterQuantity: oldMeter !== undefined ? oldMeter.quantity : '',
            meterStatus: oldMeter !== undefined ? oldMeter.status : '',
            breakerId: oldBreaker !== undefined ? oldBreaker.billingItemId : '',
            breakerQuantity: oldBreaker !== undefined ? oldBreaker.quantity : '',
            breakerStatus: oldBreaker !== undefined ? oldBreaker.status : '',
        }
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getEmployeesAction,
        getBoxesAction,
        getPolesAction,
        getAllBreakersAction,
        getFreeMetersAction,
        listAllBillingItemsAction,
        updateInstallingAction}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(EditInstalling)