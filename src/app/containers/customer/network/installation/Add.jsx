import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, change, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import './../../../index.css';
import './../../../administration/index.css';
import {escapeRegexCharacters} from '../../../../utils/EscapeRegexCharacters';
import {getEmployeesAction} from '../../../../actions/employee/employee';
import { getBoxesAction } from './../../../../actions/setup/box';
import { getPolesAction } from './../../../../actions/setup/pole';
import { getAllBreakersAction } from './../../../../actions/setup/breaker';
import { getAllItemsAction } from '../../../../actions/inventory/item';
import { getFreeMetersAction } from './../../../../actions/customer/network/meter';
import { addInstallingAction } from  '../../../../actions/customer/network/installation';

class AddInstalling extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            installingDate: null,
            room: "",
            count: 1,
            employees: [],
            boxes: [],
            poles: [],
            transformers: [],
            meters: [],
            breakers: [],
            meter: [],
            breaker: [],
            items: [],
            meterId: [],
            meterSerial: [],
            breakerId: [],
            breakerSerial: []
        };
        this.handleInstallingDate = this.handleInstallingDate.bind(this);
        this.handleSelectArea = this.handleSelectArea.bind(this);
        this.handleSelectPole = this.handleSelectPole.bind(this);
        this.handleSelectBox = this.handleSelectBox.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRemoveItem = this.handleRemoveItem.bind(this);
        this.handleAddItem = this.handleAddItem.bind(this);
    }

    componentWillMount(){
        this.props.getEmployeesAction({
            position: '',
            name: '',
            pageSize: 10,
            nowPage: 1,
            orderBy: "id"
        });
        this.props.getAllItemsAction('');
        this.props.getAllBreakersAction();
        this.props.getFreeMetersAction();
    }

    componentDidMount(){
        if(this.props.customer !== undefined){
            const installing = {
                employeeId: '',
                installingDate: '',
                installingStatus: 2,
                description: '',
                areaId: this.props.customer.areaId || '',
                poleId: this.props.customer.poleId || '',
                boxId: this.props.customer.boxId + '-' + this.props.customer.boxRoom + '-' + this.props.customer.boxAvailable || '',
                room: this.props.customer.room || '',
                meterId: '',
                meterQuantity: '',
                meterStatus: '',
                breakerId: '',
                breakerQuantity: '',
                breakerStatus: ''
            };
            this.props.dispatch(initialize('form_add_installing', installing));
        }
    }

    componentWillReceiveProps(data){
        let billingItems = [];
        let items = [];
        let meter = [];
        let breaker = [];
        let poles = [];
        let boxes = [];
        let meters = [];
        let breakers = [];
        let employees = [];
        if (data.billingItems.items !== undefined) {
            data.billingItems.items.map((billing) => {
                billingItems.push({
                    id: billing.id,
                    name: billing.itemNameEn
                })
            });
        }
        if (billingItems.length > 0) {
            items = billingItems.filter(item => !escapeRegexCharacters("meter").test(item.name))
                .filter(item => !escapeRegexCharacters("breaker").test(item.name));
            meter = billingItems.filter(item => escapeRegexCharacters("meter").test(item.name));
            breaker = billingItems.filter(item => escapeRegexCharacters("breaker").test(item.name));
            this.setState({items: items, meter: meter, breaker: breaker})
        }
        if(data.boxes.items !== undefined){
            data.boxes.items.list.map((box) => {
                boxes.push({
                    id: box.id + '-' + box.room + '-' + box.available,
                    name: box.serial
                })
            })
        }
        if(data.poles.items !== undefined){
            data.poles.items.list.map((pole) => {
                poles.push({
                    id: pole.id,
                    name: pole.serial
                })
            });
        }
        if(data.meters.items !== undefined){
            data.meters.items.map((meter) => {
                meters.push({
                    id: meter.id,
                    name: meter.serial
                })
            })
        }
        if(data.breakers.items !== undefined){
            data.breakers.items.map((breaker) => {
                breakers.push({
                    id: breaker.id,
                    name: breaker.breakerSerial
                })
            });
        }
        if(data.employees.items !== undefined){
            data.employees.items.list.map((emp) => {
                employees.push({
                    id: emp.id,
                    name: emp.nameKh
                })
            });
        }
        this.setState({poles: poles, boxes: boxes, meters: meters, breakers: breakers, employees: employees});
        if(data.installingAdd.status === 200){
            alert("Successful add installing !!");
            this.props.dispatch(initialize('form_add_installing', null));
            data.installingAdd.status = 0;
            location.reload();
        } else if(data.installingAdd.status === (400 || 404) || data.installingAdd.status === (500 || 502)){
            alert(data.installingAdd.status + " Error !! \nFail add installing !!\n"+data.installingAdd.data.message);
            data.installingAdd.status = 0;
        } else if(data.installingAdd.status === 401){
            alert("Please Login !!");
        }
    }

    handleInstallingDate(date) {
        this.setState({installingDate: date});
    }

    handleSelectArea(e){
        this.setState({room: ""});
        this.props.getPolesAction({
            area: Number(e.target.value),
            serial: "",
            limit: 1000,
            page: 1,
            orderBy: "id",
            orderType: "asc"
        })
    }

    handleSelectPole(e){
        this.setState({room: ""});
        this.props.getBoxesAction({
            id: e.target.value,
            serial: '',
            limit: 1000,
            page: 1,
            orderBy: "id"
        });
    }

    handleSelectBox(e){
        this.setState({room: e.target.value});
    }

    handleItems(){
        let dynamicFields = [];
        for(let i = 0; i < this.state.count; i++){
            dynamicFields.push(
                <div key={i}>
                    <Row>
                        <Col md={3} lg={3}>
                            <Field name={"item" + i} type="select" component={SelectBox}  values={this.state.items} placeholder="Please select ..." sortBy="name" />
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"serial" + i} type="text" component={TextBox} label="Serial"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={"quantity" + i} type="text" component={TextBox} label="Quantity"/>
                        </Col>
                        <Col md={3} lg={3}>
                            <Field name={ "status" + i } type="select" component={SelectBox}
                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                        </Col>
                    </Row>
                </div>
            )
        }
        return dynamicFields || null;
    }

    handleAddItem(){
        this.setState({count: this.state.count + 1});
    }

    handleRemoveItem() {
        this.setState({count: this.state.count - 1});
        this.props.dispatch(change('form_add_installing', 'item' + (this.state.count - 1), ''));
    }

    handleSubmit(value){
        let addItems = [];
        if(this.state.count >= 1){
            for(let i=0; i<this.state.count; i++){
                addItems.push({
                    billingItemId: Number(value[`item${i}`]),
                    quantity: value[`quantity${i}`] !== undefined ? Number(value[`quantity${i}`]) : 1,
                    serial: value[`serial${i}`] !== undefined ? value[`serial${i}`] : '',
                    status: value[`status${i}`] !== undefined ? value[`status${i}`] : 'New'
                })
            }
        }
        const meterItem = {
            billingItemId: Number(value.meterId),
            quantity: value.meterQuantity !== undefined ? Number(value.meterQuantity) : 0,
            serial: this.state.meterSerial.map((id) => {return id + ' '}).toString().trim(),
            status: value.meterStatus !== undefined ? value.meterStatus : 'NEW'
        };
        const breakerItem = {
            billingItemId: Number(value.breakerId),
            quantity: value.breakerQuantity !== undefined ? Number(value.breakerQuantity) : 0,
            serial: this.state.breakerSerial.map((id) => {return id + ' '}).toString().trim(),
            status: value.breakerStatus !== undefined ? value.breakerStatus : 'NEW'
        };

        if(Number(value.room) <= 0){
            alert("There is no available box on the pole !! \n Please do add more box first !!");
        } else {
            /*if(loadCustomer().status <= 0){
                alert("Please do add charge first !!");
            }else {
                this.props.addInstallingAction({
                    boxId: Number(value.boxId.split("-")[0]),
                    oldBoxId: loadCustomer().boxId,
                    room: Number(value.room),
                    connectionDetails: addItems.concat([meterItem, breakerItem]),
                    customerId:loadCustomer().id,
                    description: value.description,
                    employeeId: Number(value.employeeId),
                    installDate: value.installingDate,
                    installingStatusId: Number(value.installingStatus),
                    meterIds: this.state.meterId || [],
                    breakerIds: this.state.breakerId || []
                });
            }*/
            this.props.addInstallingAction({
                boxId: Number(value.boxId.split("-")[0]),
                oldBoxId: this.props.customer.boxId,
                room: Number(value.room),
                connectionDetails: addItems.concat([meterItem, breakerItem]),
                customerId:this.props.customer.id,
                description: value.description,
                employeeId: Number(value.employeeId),
                installDate: value.installingDate,
                installingStatusId: Number(value.installingStatus),
                meterIds: this.state.meterId || [],
                breakerIds: this.state.breakerId || []
            });
        }
    }

    render(){
        const {handleSubmit, error, submitting} = this.props;
        let areas = [];
        if(this.props.areaAll.items !== undefined){
            this.props.areaAll.items.map((area) => {
                areas.push({
                    id: area.id,
                    name: area.areaNameEn
                })
            });
        }else {areas = []}
        return(
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Installing</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Install By <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="employeeId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={this.state.employees} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Start Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="installingDate" component={DateTimePicker} defaultDate={this.state.installingDate}  handleChange={this.handleInstallingDate} placeholder="Installing date" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Status <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="installingStatus" type="select" component={SelectBox} disabled={true} placeholder="Please select ..."
                                                   values={[{id: 1, name: "Installing"},{id: 2, name: "Installed"},{id: 3, name: "Re-install"}]} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                </Col>
                                <Col lg={6}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Area (Transformer)</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="areaId" type="select" onChange={this.handleSelectArea} component={SelectBox} placeholder="Please select ..." values={areas} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Pole</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="poleId" type="select" onChange={this.handleSelectPole} component={SelectBox} placeholder="Please select ..." values={this.state.poles} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Box No</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="boxId" type="select" onChange={this.handleSelectBox} component={SelectBox} placeholder="Please select ..." values={this.state.boxes} />
                                        </Col>
                                    </Row>
                                    { this.state.room === "" ? null :
                                        <Row>
                                            <Col lgOffset={8} lg={4}>
                                                <p style={{color: 'blue'}}>Total room(s) : {this.state.room.split("-")[1]}</p>
                                                <p style={{color: 'blue'}}>Available room(s) : {this.state.room.split("-")[2]}</p>
                                            </Col>
                                        </Row>
                                    }
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <p className="label-name">Room No</p>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="room" type="text" component={TextBox} label="Room no" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <div className="wrap-form-items-add">
                                <div className="header">
                                    <Row>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Items </strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Serial</strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Quantity</strong>
                                        </Col>
                                        <Col md={3} lg={3} className="label-name text-align-center">
                                            <strong>Status</strong>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3}>
                                            <Field name="meterId" type="select" component={SelectBox}  values={this.state.meter} placeholder="Please select ..." />
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { this.setState({ meterId: _.map(selected, "id"), meterSerial: _.map(selected, "name")}) }}
                                                labelKey="name"
                                                multiple
                                                options={this.state.meters}
                                                placeholder="Meter serial ..."
                                            />
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="meterQuantity" type="text" component={TextBox} label="Quantity"/>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="meterStatus" type="select" component={SelectBox}
                                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3} lg={3}>
                                            <Field name="breakerId" type="select" component={SelectBox}  values={this.state.breaker} placeholder="Please select" />
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { this.setState({ breakerId: _.map(selected, "id"), breakerSerial: _.map(selected, "name")}) }}
                                                labelKey="name"
                                                multiple
                                                options={this.state.breakers}
                                                placeholder="Breaker serial ..."
                                            />
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="breakerQuantity" type="text" component={TextBox} label="Quantity"/>
                                        </Col>
                                        <Col md={3} lg={3}>
                                            <Field name="breakerStatus" type="select" component={SelectBox}
                                                   values={[{id: 'New', name: "New"},{id: 'CM', name: "Change Meter"},{id: 'CB', name: "Change Breaker"},{id: 'Other', name: "Other"}]} placeholder="Please select ..." />
                                        </Col>
                                    </Row>
                                    <div className="items-detail">
                                        <Row>
                                            <Col md={12} lg={12}>
                                                {this.handleItems()}
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md={1} lg={1}>
                                                <Button bsStyle="primary" onClick={this.handleAddItem}>Add Item</Button>
                                            </Col>
                                            <Col md={1} lg={1}>
                                                <Button bsStyle="primary" onClick={this.handleRemoveItem}>Remove</Button>
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

AddInstalling = reduxForm({
    form: 'form_add_installing',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.employeeId === ""){
            errors.employeeId = 'Please select Installer !!';
        }

        if(values.installingDate == ("" || null)){
            errors.installingDate = 'Please select Installing date !!';
        }

        if(values.installingStatus === ""){
            errors.installingStatus = "Please select Installing status !!";
        }

        if(values.meterId === ""){
            errors.meterId = "Please select item meter !!";
        }
        if(!number.test(values.meterQuantity)){
            errors.meterQuantity = 'Please input number only !!';
        }
        if(values.meterStatus === ""){
            errors.meterStatus = "Please select item status !!";
        }
        if(values.breakerId === ""){
            errors.breakerId = "Please select item breaker !!";
        }
        if(!number.test(values.breakerQuantity)){
            errors.breakerQuantity = 'Please input number only !!';
        }
        if(values.breakerStatus === ""){
            errors.breakerStatus = "Please select item status !!";
        }
        if(values.boxId === ""){
            errors.boxId = 'Please select Box !!';
        }
        if(!number.test(values.room)){
            errors.room = 'Please input number only !!';
        }
        return errors
    },
})(AddInstalling);

function mapStateToProps(state) {
    return {
        employees: state.employee.employees,
        transformerAll: state.transformer.transformerAll,
        areaAll: state.area.areaAll,
        boxes: state.box.boxes,
        poles: state.pole.poles,
        breakers: state.breaker.breakerAll,
        meters: state.changeMeter.meterFree,
        billingItems: state.item.itemsAll,
        installingAdd: state.installing.installingAdd,
        initialValues: {
            employeeId: '',
            installingDate: '',
            installingStatus: 2,
            description: '',
            areaId: '',
            poleId: '',
            boxId: '',
            room: '',
            meterId: '',
            meterQuantity: '',
            meterStatus: '',
            breakerId: '',
            breakerQuantity: '',
            breakerStatus: ''
        }
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({getEmployeesAction,
        getBoxesAction,
        getPolesAction,
        getAllBreakersAction,
        getFreeMetersAction,
        getAllItemsAction,
        addInstallingAction}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddInstalling)