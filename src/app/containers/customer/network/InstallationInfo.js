import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import { getInstallingAction, deleteInstallingAction } from  '../../../actions/customer/network/installation';
import EditInstalling from './installation/Edit.jsx';
import moment from 'moment';
import ViewItem from './../../../containers/customer/network/VeiwItems';
import './index.css';

class InstallationInfo extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            update : false,
            viewItem : false
        };
        this.onUpdate = this.onUpdate.bind(this);
        this.closeUpdate = this.closeUpdate.bind(this);
        this.onViewItem = this.onViewItem.bind(this);

    }

    onUpdate(){
        this.setState({update: true, viewItem:false})
    }
    onViewItem(){
        this.setState({ viewItem:true  })
    }
    closeUpdate(value){
        this.setState({update: value})
    }
    handleDelete() {
        if (confirm("Are you sure want to delete this box type?") === true) {
            this.props.deleteInstallingAction(this.props.customer !== undefined ? this.props.customer.connectionId : 0);
        }
    }

    render(){
        const customer = this.props.customer;
        return(
            <div>
                { !this.state.update ?
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <div className="custyle">
                                <table className="table table-bordered table-striped custab">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Installer Name</th>
                                        <th>Pole</th>
                                        <th>Box</th>
                                        <th>Room No.</th>
                                        <th>Install Date</th>
                                        <th>Status</th>
                                        <th>Description</th>
                                        <th className="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    { customer !== undefined ?
                                        <tbody>
                                        <tr>
                                            <td>{customer.employeeId}</td>
                                            <td>{customer.employeeName}</td>
                                            <td>{customer.poleSerial}</td>
                                            <td>{customer.boxSerial}</td>
                                            <td>{customer.room}</td>
                                            <td>{moment(customer.installDate).format("DD/MM/YYYY")}</td>
                                            {
                                                customer.installingStatus === 1 ?  <td className="installing text-align-center">Installing</td>:
                                                    customer.installingStatus === 2 ?  <td className="Installed text-align-center">Installed</td>:
                                                        customer.installingStatus === 3 ?  <td className="Re-install text-align-center">Re-install</td> :
                                                            <td className="newCustomer">New customer</td>
                                            }
                                            <td>{customer.connectionDescription}</td>
                                            <td className="text-center">
                                                <a onClick = { ()=> this.onUpdate()} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-edit"> </span>
                                                </a>&nbsp;
                                                <a onClick={ ()=> this.onViewItem()} className='btn btn-info btn-xs'>
                                                    <span className="glyphicon glyphicon-eye-open"> </span>
                                                </a>&nbsp;
                                                <a onClick = {this.handleDelete.bind(this)} className="btn btn-danger btn-xs">
                                                    <span className="glyphicon glyphicon-remove"> </span>
                                                </a>
                                            </td>
                                        </tr>
                                        </tbody>
                                        : null
                                    }
                                </table>
                            </div>
                        </Col>
                    </Row>
                    :
                    <EditInstalling closeUpdate={this.closeUpdate} />
                }
                { this.state.viewItem ?
                    <ViewItem customer={customer !== undefined ? customer : undefined}/>
                    :null
                }
         </div>
        )
    }
}
function mapStateToProps(state) {
    //console.log("load customer",loadCustomer());
    return {
        customer: state.customer.customer,
        installingGet: state.installing.installingGet,
        installingDelete: state.installing.installingDelete
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getInstallingAction,deleteInstallingAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(InstallationInfo);