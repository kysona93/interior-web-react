import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { loadCustomer } from '../../../localstorages/localstorage';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { getFreeMetersAction, getUsedMetersAction, updateChangeMeterAction } from './../../../actions/customer/network/meter';
import './../../index.css';

class ChangeMeter extends React.Component{
    constructor(props){
        super(props);
        this.state={
            oldMeters: [],
            newMeters: [],
            changeDate:null
        };
        this.handleChangeDate = this.handleChangeDate.bind(this);
    }

    componentWillReceiveProps(data){
        if(data.meterUpdate.status === 200){
            alert("Meter Has been change!! ");
            data.meterUpdate.status = 0;
        }

        if(data.meterUpdate.status === 404 || data.meterUpdate.status === 500 ){
            alert(" Meter can't be Update !! ")
        }

    }
    handleSubmit(value){
        if(this.state.oldMeters.length <= 0){
            alert("Please Select Current Meter !!")
        }
        if(this.state.newMeters.length <= 0){
            alert("Please Select New Meter !!")
        }
        if(this.state.newMeters.length > 0 && this.state.oldMeters.length > 0){
            let olds = [];
            let news = [];
            this.state.oldMeters.map((meter) => {
                olds.push({
                    id: meter.id,
                    serial: meter.meterSerial,
                    ampere: meter.minAmpere.toString().concat('-', meter.maxAmpere.toString()),
                    originalUsage: meter.originalUsageRecord,
                    type: meter.typeName
                });
            });
            this.state.newMeters.map((meter) => {
                news.push({
                    id: meter.id,
                    serial: meter.serial,
                    ampere: meter.minAmpere.toString().concat('-', meter.maxAmpere.toString()),
                    originalUsage: meter.originalUsageRecord,
                    type: meter.meterType

                });
            });
            this.props.updateChangeMeterAction({
                customerId: this.props.customer.id,
                customerName: this.props.customer.nameKh,
                oldMeterForms: olds,
                newMeterForms: news,
                changeDate : value.changeDate,
                description: value.description
            });
        }

    }
    handleChangeDate(date){
        this.setState({ changeDate: date })
    }
    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Change Meter</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={5}>
                                    <div className="change-to">
                                        <center><p>From Meter</p></center>
                                    </div>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>SelectMeter to Transfer : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { this.setState({oldMeters: selected}) }}
                                                labelKey="serial"
                                                multiple
                                                options={this.props.customer.meters !== undefined ? this.props.customer.meters : []}
                                                placeholder="Meter..."
                                            />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col lg={2}><img className="change-to" src="/icons/arrow.png"/> </Col>

                                <Col lg={5}>
                                    <div className="to-meter">
                                        <center><p>To Meter</p></center>
                                    </div>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Free Meters : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { this.setState({newMeters: selected}) }}
                                                labelKey="serial"
                                                multiple
                                                options={this.props.meters.items !== undefined ? this.props.meters.items : []}
                                                placeholder="Please select available ..."
                                            />
                                        </Col>
                                    </Row>
                                    <br/>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Change Date : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="changeDate" component={DateTimePicker} placeholder="Install Date"
                                                   defaultDate={this.state.changeDate} handleChange={this.handleChangeDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea}  label="Description"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
ChangeMeter = reduxForm({
    form: 'form_change_meter',
    validate: (values) => {
        const errors = {};
        if(values.description === undefined || values.description =="" ){
            errors.description = "Field Require";
        }
        if(values.changeDate === undefined || values.changeDate =="" ){
            errors.changeDate = "Field Require";
        }
        return errors
    }
})(ChangeMeter);
function mapStateToProps(state) {
    return {
        meters: state.changeMeter.meterFree,
        meterUpdate:state.changeMeter.meterUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ updateChangeMeterAction },dispatch)
}


export default connect(mapStateToProps,mapDispatchToProps)(ChangeMeter);