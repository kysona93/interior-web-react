import React from '../../../../../node_modules/react';
import './../../index.css';

export default class LocationCustomer extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const customer = this.props.customer;
        return(
            <div className="container">
                { customer !== undefined ?
                    <div>
                        <div>
                            <br/>
                            <div className="row">
                                <div className="customer-transformer">
                                    <h3 className="transformer">Transformer</h3>
                                    <div className="col-md-12">
                                        <div className="col-xs-2 col-md-2 transformer-img"><img src="/icons/transformers.png"/></div>
                                        <div className="col-xs-10 col-md-10 transformer-content">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>Serial</td>
                                                    <td>:   {customer.transSerial}</td>
                                                </tr>
                                                <tr>
                                                    <td>Latitude</td>
                                                    <td>:   {customer.transLatitude}</td>
                                                </tr>
                                                <tr>
                                                    <td>Longitude</td>
                                                    <td>:   {customer.transLongitude}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td>:   {customer.transVillageKh + ", " + customer.transCommuneKh + ", " + customer.transDistrictKh + ", " + customer.transProvinceKh}</td>
                                                </tr>
                                                <tr>
                                                    <td>description</td>
                                                    <td>:   {customer.transDescription}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="customer-transformer">
                                    <h3 className="pole">Pole</h3>
                                    <div className="col-md-12">
                                        <div className="col-xs-2 col-md-2 transformer-img"><img src="/icons/pole.png"/></div>
                                        <div className="col-xs-10 col-md-10 transformer-content">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>Serial</td>
                                                    <td>:   {customer.poleSerial}</td>
                                                </tr>
                                                <tr>
                                                    <td>Height</td>
                                                    <td>:   {customer.poleHeight}</td>
                                                </tr>
                                                <tr>
                                                    <td>Latitude</td>
                                                    <td>:   {customer.poleLatitude}</td>
                                                </tr>
                                                <tr>
                                                    <td>Longitude</td>
                                                    <td>:   {customer.poleLongitude}</td>
                                                </tr>
                                                <tr>
                                                    <td>Location</td>
                                                    <td>:   {customer.poleVillageKh + ", " + customer.poleCommuneKh + ", " + customer.poleDistrictKh + ", " + customer.poleProvinceKh}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>:   {customer.poleDescription}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="customer-transformer">
                                    <h3 className="box">Box</h3>
                                    <div className="col-md-12">
                                        <div className="col-xs-2 col-md-2 transformer-img"><img src="/icons/box.jpg"/></div>
                                        <div className="col-xs-10 col-md-10 transformer-content">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td>Serial</td>
                                                    <td>:   {customer.boxSerial}</td>
                                                </tr>
                                                <tr>
                                                    <td>Room No.</td>
                                                    <td>:   {customer.room}</td>
                                                </tr>
                                                <tr>
                                                    <td>Amount Room</td>
                                                    <td>:   {customer.boxRoom}</td>
                                                </tr>
                                                <tr>
                                                    <td>Description</td>
                                                    <td>:   {customer.boxDescription}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : <h3>Data Not Found</h3>
                }
            </div>
        )
    }
}
