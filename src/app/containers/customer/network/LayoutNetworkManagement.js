import React from '../../../../../node_modules/react';
import Location from './LocationCustomer';
import InstallingInfo from './InstallationInfo';
import ChangeMeter from './ChangeMeter';
import MeterTransfer from './MeterTransfer';
import MeterHistory from './MeterHistory';
import Installing from './installation/Add.jsx';
import './index.css';

export default class LayoutNetworkManagement extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            location: true,
            installing: false,
            installed: false,
            cmeter: false,
            tmeter: false,
            hmeter: false
        };
        this.handleLocation = this.handleLocation.bind(this);
        this.handleInstalling = this.handleInstalling.bind(this);
        this.handleInstalled = this.handleInstalled.bind(this);
        this.handleCmeter = this.handleCmeter.bind(this);
        this.handleTmeter = this.handleTmeter.bind(this);
        this.handleHmeter = this.handleHmeter.bind(this);
    }

    handleLocation(){
        this.setState({
            location: true,
            installing: false,
            installed: false,
            cmeter: false,
            tmeter: false,
            hmeter: false
        });
    }
    handleInstalling(){
        this.setState({
            location: false,
            installing: true,
            installed: false,
            cmeter: false,
            tmeter: false,
            hmeter: false
        });
    }

    handleInstalled(){
        this.setState({
            location: false,
            installing: false,
            installed: true,
            cmeter: false,
            tmeter: false,
            hmeter: false
        });
    }
    handleCmeter(){
        this.setState({
            location: false,
            installing: false,
            installed: false,
            cmeter: true,
            tmeter: false,
            hmeter: false
        });
    }
    handleTmeter(){
        this.setState({
            location: false,
            installing: false,
            installed: false,
            cmeter: false,
            tmeter: true,
            hmeter: false
        });
    }
    handleHmeter(){
        this.setState({
            location: false,
            installing: false,
            installed: false,
            cmeter: false,
            tmeter: false,
            hmeter: true
        });
    }

    render(){
        const customer = this.props.customer;
        return(
            <div className="container-fluid">
                <div className="col-md-12">
                    <div className="panel with-nav-tabs panel-default installing">
                        <div className="panel-heading installing">
                            <ul className="nav nav-tabs installing">
                                <li className="active">
                                    <a href="#tab1default" onClick={this.handleLocation} data-toggle="tab">Location</a>
                                </li>
                                { customer !== undefined ?
                                    customer.connectionId !== null ?
                                        <li className="dropdown">
                                            <a href="#" data-toggle="dropdown">Meter <span className="caret"> </span></a>
                                            <ul className="dropdown-menu" role="menu">
                                                <li><a href="#tab4default" onClick={this.handleCmeter} data-toggle="tab">Change Meter</a></li>
                                                <li><a href="#tab5default" onClick={this.handleTmeter} data-toggle="tab">Transfer Meter</a></li>
                                                <li><a href="#tab7default" onClick={this.handleHmeter} data-toggle="tab">Meter History</a></li>
                                            </ul>
                                        </li>
                                        : null
                                    : null
                                }
                                { customer !== undefined ?
                                    customer.connectionId !== null ?
                                        <li><a href="#tab2default" onClick={this.handleInstalled} data-toggle="tab">Installing Info</a></li>
                                        :
                                        <li><a href="#installing" onClick={this.handleInstalling} data-toggle="tab">Installing</a></li>
                                    : null
                                }
                            </ul>
                        </div>
                        <div className="panel-body">
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="tab1default">
                                    { this.state.location ?
                                        <Location customer={customer !== undefined ? customer : undefined} />
                                        : null
                                    }
                                </div>
                                <div className="tab-pane fade" id="tab2default">
                                    {   this.state.installed ?
                                        <InstallingInfo customer={customer !== undefined ? customer : undefined}/>
                                        : null
                                    }
                                </div>
                                <div className="tab-pane fade" id="installing">
                                    { this.state.installing ?
                                        <Installing customer={customer !== undefined ? customer : undefined}/>
                                        : null
                                    }
                                </div>

                                <div className="tab-pane fade" id="tab4default">
                                    { this.state.cmeter ?
                                        <ChangeMeter customer={customer !== undefined ? customer : undefined}/>
                                        : null
                                    }
                                </div>
                                <div className="tab-pane fade" id="tab5default">
                                    { this.state.tmeter ?
                                        <MeterTransfer customer={customer !== undefined ? customer : undefined}/>
                                        : null
                                    }
                                </div>
                                <div className="tab-pane fade" id="tab7default">
                                    { this.state.hmeter ?
                                        <MeterHistory customer={customer !== undefined ? customer : undefined}/>
                                        : null
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
