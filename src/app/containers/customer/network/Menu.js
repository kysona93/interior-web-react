import React from 'react';
import { Link } from 'react-router';
class Menu extends React.Component{

    render(){
        return(
            <div className="row">
                <div className="menu-wrap">
                    <nav className="menu">
                        <ul className="clearfix">
                            <li><Link to="/app/customers/network">Location</Link></li>
                            <li>
                                <a href="#">Meter <span className="arrow">&#9660;</span></a>
                                <ul className="sub-menu">
                                    <li><Link to="/app/customers/network/change-meter">Change Meter</Link></li>
                                    <li><Link to="/app/customers/network/add-item-mv">Add Item MV</Link></li>
                                    <li><Link to="/app/customers/network/meter-transfer">Meter Transfer</Link></li>
                                    <li><Link to="/app/customers/network/meter-history">Meter History</Link></li>
                                </ul>
                            </li>
                            <li><Link to="/app/customers/network/installation-info">InstallationInfo</Link></li>
                            <li><a href="#">Estimate Equipment</a></li>
                        </ul>
                    </nav>
                </div>
            </div>

        )
    }
}
export default Menu;