import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllInstallingAction } from './../../../actions/customer/network/installation';
import { loadCustomer } from '../../../localstorages/localstorage';

class ViewItem extends React.Component{
    constructor(props){
        super(props);
    }
    componentWillMount(){
        this.props.getAllInstallingAction(this.props.customer !== undefined ? this.props.customer.id : 0);
    }

    render(){
        return(
         <div className="container-fluid">
             <Row>
                 <Col xs={6} sm={6} md={6} lg={6}>
                     <div className="custyle">
                         <table className="table table-bordered table-striped custab">
                             <thead>
                             <tr>
                                 <th>ID</th>
                                 <th>Item Name</th>
                                 <th>Item Serial</th>
                                 <th>Qty</th>
                                 <th>Status</th>
                             </tr>
                             </thead>
                             <tbody>
                             { this.props.installingAll.items !== undefined ?
                                 this.props.installingAll.items.map((item, index) => {
                                     return (
                                         <tr key={index}>
                                             <td>{index + 1}</td>
                                             <td>{item.billingItemNameKh}</td>
                                             <td>{item.connectionDetailSerial}</td>
                                             <td>{item.connectionDetailQuantity}</td>
                                             <td>{item.connectionDetailStatus}</td>
                                         </tr>
                                     )
                                 })
                                 :
                                 <tr>
                                     <td colSpan={4}><h2>RESULT NOT FOUND</h2></td>
                                 </tr>
                             }
                             </tbody>
                         </table>
                     </div>
                 </Col>
             </Row>
         </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        installingAll:state.installing.installingAll
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllInstallingAction },dispatch)
}
export default connect(mapStateToProps,mapDispatchToProps)(ViewItem);