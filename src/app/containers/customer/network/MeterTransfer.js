import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm ,initialize} from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getUsedMetersAction,updateTransformerMeterAction } from './../../../actions/customer/network/meter';
import { getCustomersAction } from './../../../actions/customer/customer';
import { loadCustomer } from '../../../localstorages/localstorage';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';

class MeterTransfer extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            meters: [],
            meterId: [],
            customerId: [],
            customers: [],
            changeDate: null
        };
        this.handleChangeDate = this.handleChangeDate.bind(this);
    }
    handleChangeDate(date){
        this.setState({ changeDate: date })
    }
    componentWillMount(){
        this.props.getUsedMetersAction(this.props.customer.id);
        this.props.getCustomersAction({
            type: '',
            option: '',
            search: '',
            limit: 1000,
            page: 1,
            orderBy: "id"
        });
    }
    componentWillReceiveProps(data){
        let meters = [];
        let customers = [];
        if(data.meterUsage.items !== undefined){
            data.meterUsage.items.map((meter) => {
                meters.push({
                    id: meter.id,
                    name: meter.serial
                })
            })
        }
        if(data.customers.items !== undefined){
            data.customers.items.list.map((cus) => {
                customers.push({
                    id:cus.id,
                    name: cus.nameEn
                })
            })
        }
        if(data.transferUpdate.status === 200){
            alert("Successful Transfer Meter !!");
            this.props.dispatch(initialize('form_meter_transfer', null));
            location.href = `/app/customers/info?name=${this.props.customer.nameKh}`;
            data.transferUpdate.status = 0;
        }
        if(data.transferUpdate.status === (400 || 404 || 500 || 502)){
            alert(data.transferUpdate.status + "Error ! \nFail Transfer Meter !!\n"+data.transferUpdate.error);
            data.transferUpdate.status = 0;
        }
        this.setState({ meters: meters , customers:customers });
    }
    handleSubmit(values){
           this.props.updateTransformerMeterAction({
               id: this.props.customer.id,
               nameKh: values.nameKh,
               nameEn: values.nameEn,
               phone : values.phone,
               transferMeterLogForm : {
                   changeDate: values.changeDate,
                   customerId: this.props.customer.id,
                   customerName: this.props.customer.nameKh,
                   description:values.description,
                   isTransfer: 1,
                   transferTo: values.nameKh
               }
           })
    }
    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Meter Transfer</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                {
                                    this.props.meterUsage.items == undefined ? null
                                        :
                                        <Col lg={5}>
                                            {
                                               this.props.meterUsage.items.map((meter,index) => {
                                                   return(
                                                       <div key={index}>
                                                           <Row>
                                                               <Col md={4} lg={4} className="label-name">
                                                                   <strong>Name Kh : <span
                                                                       className="label-require">*</span></strong>
                                                               </Col>
                                                               <Col md={8} lg={8}>
                                                                   <Field name="activePart" type="text" component={TextBox} disabled={true}
                                                                          label={meter.nameKh} icon="fa fa-keyboard-o"/>
                                                               </Col>
                                                           </Row>
                                                           <Row>
                                                               <Col md={4} lg={4} className="label-name">
                                                                   <strong>Name En : <span
                                                                       className="label-require">*</span></strong>
                                                               </Col>
                                                               <Col md={8} lg={8}>
                                                                   <Field name="activePart" type="text" component={TextBox} disabled={true}
                                                                      label={meter.nameEn} icon="fa fa-keyboard-o"/>
                                                               </Col>
                                                           </Row>
                                                           <Row>
                                                               <Col md={4} lg={4} className="label-name">
                                                                   <strong> Meter Serial : <span
                                                                       className="label-require">*</span></strong>
                                                               </Col>
                                                               <Col md={8} lg={8}>
                                                                   <Field name="serial" type="text" component={TextBox}
                                                                          disabled={true} label={meter.serial} icon="fa fa-keyboard-o"/>
                                                               </Col>
                                                           </Row>
                                                           <Row>
                                                               <Col md={4} lg={4} className="label-name">
                                                                   <strong>Type : </strong>
                                                               </Col>
                                                               <Col md={8} lg={8}>
                                                                   <Field name="activePart" type="text" component={TextBox}
                                                                          disabled={true} label={meter.typeKh} icon="fa fa-keyboard-o"/>
                                                               </Col>
                                                           </Row>
                                                       </div>
                                                   );
                                               })
                                            }

                                        </Col>
                                }
                                <Col lg={2}><img className="change-to" src="/icons/arrow.png"/> </Col>
                                <Col lg={5}>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name Kh : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameKh" type="text" component={TextBox} label="Latin Name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name En : <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="nameEn" type="text" component={TextBox} label="English Name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Phone Number : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="phone" type="text" component={TextBox} label="Phone" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Install Date : </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="changeDate" component={DateTimePicker} placeholder="Install Date"
                                                   defaultDate={this.state.changeDate} handleChange={this.handleChangeDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea}  label="Description"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="រក្សាទុក/Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
MeterTransfer = reduxForm({
    form: 'form_meter_transfer',
    validate: (values) => {
        const errors = {};
        if(values.nameKh === undefined || values.nameKh =="" ){
            errors.nameKh = "Field Require";
        }
        if(values.nameEn === undefined || values.nameEn =="" ){
            errors.nameEn = "Field Require";
        }
        if(values.phone === undefined || values.phone =="" ){
            errors.phone = "Field Require";
        }
        if(values.changeDate === undefined || values.changeDate =="" ){
            errors.changeDate = "Field Require";
        }
        if(values.description === undefined || values.description =="" ){
            errors.description = "Field Require";
        }

        return errors
    }
})(MeterTransfer);
function mapStateToProps(state) {
    return {
        customers: state.customer.customers,
        meterUsage: state.changeMeter.meterUsage,
        transferUpdate:state.changeMeter.transferUpdate
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getUsedMetersAction, getCustomersAction, updateTransformerMeterAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MeterTransfer);