import React from '../../../../../node_modules/react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { store } from "../../../../store.js";
import { Row, Table } from 'react-bootstrap';
import Pagination from '../../../components/forms/Pagination';
import '../../../components/forms/Styles.css';
import { getCustomersAction, getCustomerAction } from './../../../actions/customer/customer';
import { getAllRatePerKwhAction } from './../../../actions/setup/rateKwh';
import { getAllConsumptionTypesAction} from '../../../actions/setup/type/consumptionType';
import { getAllGroupInvoiceAction } from './../../../actions/invoice/groupInvoices';
import { getAllCustomerGroupsAction } from '../../../actions/setup/type/customerGroup';
import { getAllCustomerTypesAction } from './../../../actions/setup/type/customerType';
import { getAllVoltageTypesAction } from './../../../actions/setup/type/voltageType';
import { getAllAmperesAction } from './../../../actions/setup/ampere';
import { getAllOccupationsAction } from './../../../actions/setup/occupation';
import { getAllTransformersAction } from './../../../actions/setup/transformer';
import { getAllBanksAction } from './../../../actions/setup/bank';
import { getAllAreasAction } from './../../../actions/setup/area';
import { getAllItemsMvAction } from './../../../actions/customer/network/meter';
import { getProvincesAction } from '../../../actions/location';
import './style.css';

let customer = {
    type: '',
    option: '',
    search: '',
    limit: 10,
    page: 1,
    orderBy: "id"
};

class ListCustomer extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            id: 0,
            name: ""
        };
    }

    handleNewTab(link, name){
        if(link !== null && link !== undefined && link !== ""){
            this.props.tab(
                link,
                name
            );
            store.dispatch(push(link));
        }
    }

    componentWillMount(){
        const query = this.props.location.query;
        customer.type = query.type || 'fuzzy';
        customer.option = query.opt || 'id';
        customer.search = query.search || '';
        this.props.getCustomersAction(customer);
        this.props.getProvincesAction();
        this.props.getAllRatePerKwhAction();
        this.props.getAllConsumptionTypesAction();
        this.props.getAllGroupInvoiceAction();
        this.props.getAllCustomerGroupsAction(0);
        this.props.getAllCustomerTypesAction();
        this.props.getAllVoltageTypesAction();
        this.props.getAllOccupationsAction();
        this.props.getAllAmperesAction(0);
        this.props.getAllItemsMvAction({name:''});
        this.props.getAllAreasAction({
            area: '',
            serial: ''
        });
        this.props.getAllBanksAction();
    }

    componentWillReceiveProps(data){
        setTimeout(() => {
            customer.limit = 10;
            customer.page = 1;
            if(customer.search !== data.location.query.search) {
                customer.search = data.location.query.search;
                data.getCustomersAction(customer);
            }
            if(customer.type !== data.location.query.type){
                customer.type = data.location.query.type;
                data.getCustomersAction(customer);
            }
            if(customer.option !== data.location.query.opt){
                customer.option = data.location.query.opt;
                data.getCustomersAction(customer);
            }
        }, 500);

        if(data.customers.items !== undefined && data.provinces.items !== undefined){
            this.setState({loading: 'none'});
        }

        if(data.customer.items !== undefined && this.props.customer.items === undefined){
            this.handleNewTab(`/app/customers/info?id=${this.state.id}&name=${this.state.name}`, this.state.name);
        } else if(data.customer.items !== undefined && this.props.customer.items !== undefined){
            if (data.customer.items[0].id !== this.props.customer.items[0].id){
                this.handleNewTab(`/app/customers/info?id=${this.state.id}&name=${this.state.name}`, this.state.name);
            }
        }
    }

    handleSelect(e){
        this.props.getCustomerAction(e.id);
        this.setState({id: e.id, name: e.nameKh});
        //this.handleNewTab(`/app/customers/info?id=${e.id}&name=${e.nameKh}`, e.nameKh);
    }

    render(){
        const customers = this.props.customers.items;
        let total = 0;
        return (
            <div className="container-fluid">
                <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                    <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                    <span className="sr-only">Loading...</span>
                </div>
                <Row style={{margin: '0', padding: '0'}}>
                    <Table responsive condensed bordered hover className="search-result">
                        <thead >
                        <tr>
                            <th>ID</th>
                            <th>Name (Khmer)</th>
                            <th>Name (Latin)</th>
                            <th>Occupation</th>
                            <th>Phase</th>
                            <th>Ampere</th>
                            <th>Transformer</th>
                            <th>Area</th>
                            <th>Pole</th>
                            <th>Box</th>
                        </tr>
                        </thead>
                        <tbody>
                        { this.props.provinces.items !== undefined && customers !== undefined ?
                            customers.list.map((customer, index)=>{
                                total = customers.totalCount;
                                return(
                                    <tr key={index}>
                                        <td className="text-center">
                                            <a onClick={() => this.handleSelect({id: customer.id, nameKh: customer.nameKh, nameEn: customer.nameEn})} style={{fontWeight: 'bold'}} className='btn btn-xs'>
                                                {customer.id}
                                            </a>
                                        </td>
                                        <td className="text-center">{customer.nameKh}</td>
                                        <td className="text-center">{customer.nameEn}</td>
                                        <td className="text-center">{customer.occupationKh}</td>
                                        <td className="text-center">{customer.customerPhase}</td>
                                        <td className="text-center">{customer.ampereValue} A</td>
                                        <td className="text-center">{customer.transSerial}</td>
                                        <td className="text-center">{customer.areaNameEn}</td>
                                        <td className="text-center">{customer.poleSerial}</td>
                                        <td className="text-center">{customer.boxSerial}</td>
                                    </tr>
                                )
                            })
                            :
                            <tr>
                                <td colSpan="10">
                                   <h3 style={{textAlign: 'center'}}>RESULT NOT FOUND!</h3>
                                </td>
                            </tr>
                        }
                        </tbody>
                    </Table>
                    <Pagination
                        totalCount={total}
                        items={customer}
                        callBackAction={this.props.getCustomersAction}
                    />
                </Row>
            </div>
        )
    }
}
function mapStateToProps(state) {
    //console.log("customer", state.location.locationAll);
    return {
        customers: state.customer.customers,
        customer: state.customer.customer,
        provinces: state.location.provinces,
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        consumptionTypeAll: state.consumptionType.consumptionTypeAll,
        groupInvoiceAll: state.groupInvoice.groupInvoiceAll,
        customerGroupAll: state.customerGroup.customerGroupAll,
        customerTypeAll: state.customerType.customerTypeAll,
        voltageTypeAll: state.voltageType.voltageTypeAll,
        ampereAll: state.ampere.ampereAll,
        occupationAll: state.occupation.occupationAll,
        areaAll: state.area.areaAll,
        bankAll: state.bank.bankAll,
        transformerAll: state.transformer.transformerAll,
        customerUpdate: state.customer.customerUpdate,
        itemMvGetAll: state.changeMeter.itemMvGetAll,
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getCustomersAction,
        getCustomerAction,
        getAllRatePerKwhAction,
        getAllConsumptionTypesAction,
        getAllGroupInvoiceAction,
        getAllCustomerGroupsAction,
        getAllCustomerTypesAction,
        getAllVoltageTypesAction,
        getAllAmperesAction,
        getAllOccupationsAction,
        getAllTransformersAction,
        getAllAreasAction,
        getAllBanksAction,
        getAllItemsMvAction,
        getProvincesAction},dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps)(ListCustomer);