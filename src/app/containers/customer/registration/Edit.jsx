import React from '../../../../../node_modules/react';
import _ from '../../../../../node_modules/lodash';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import SelectBox from '../../../components/forms/SelectBox';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getCustomerAction, updateCustomerAction } from './../../../actions/customer/customer';
import { getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../actions/location';
import { getPolesAction } from './../../../actions/setup/pole';
import { getBoxesAction } from './../../../actions/setup/box';
import { getAllAmperesAction } from './../../../actions/setup/ampere';
import './../finance/tab.css';
import LayoutNetworkManagement from '../network/LayoutNetworkManagement';
import LayoutFinance from './../finance/LayoutFinance';
import moment from 'moment';
import './style.css';

class EditCustomer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            info: true,
            network: false,
            finance: false,
            history: false,
            graph: false,
            audit: false,
            register: null,
            provinces: [],
            districts: [],
            communes: [],
            villages: [],
            amperes: [],
            boxes: [],
            poles: [],
            rates: [],
            licenses: [],
            consumptions: [],
            groupInvoices: [],
            customerSaleTypes: [],
            customerTypes: [],
            voltageTypes: [],
            occupations: [],
            areas: [],
            banks: [],
            room: "",
            check: false,
            address: "",
            invoiceAddress: "",
            areaId: 0
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
        this.handleSelectVillage = this.handleSelectVillage.bind(this);
        this.handlePhase = this.handlePhase.bind(this);
        this.handleSelectPole = this.handleSelectPole.bind(this);
        this.handleSelectBox = this.handleSelectBox.bind(this);
        this.handleInvoiceAddress = this.handleInvoiceAddress.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
        this.handleSelectCustomerGroup = this.handleSelectCustomerGroup.bind(this);
        this.infoTab = this.infoTab.bind(this);
        this.networkTab = this.networkTab.bind(this);
        this.financeTab = this.financeTab.bind(this);
        this.historyTab = this.historyTab.bind(this);
        this.graphTab = this.graphTab.bind(this);
        this.auditTab = this.auditTab.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({register: date});
    }

    static handleAddress(value){
        return value.options[value.selectedIndex].text;
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSelectVillage(){
        this.setState({address: EditCustomer.handleAddress(document.getElementsByName('village')[0]).concat(", " + this.state.address)});
    }

    handleSubmit(value){
        if(confirm("Are you sure want to edit this customer?") === true){
            /*alert(JSON.stringify(
                {
                    id: Number(this.props.location.query.id),
                    address: value.address,
                    ampereId: Number(value.ampereId),
                    bankId: value.bankId !== "" ? Number(value.bankId) : 0,
                    boxId: Number(value.boxId.split("-")[0]),
                    oldBoxId: this.props.customer.boxId,
                    consumptionTypeId: Number(value.consumptionId),
                    customerSaleTypeId: Number(value.saleId),
                    gender: value.gender,
                    groupInvoiceId: Number(value.groupInvoiceId),
                    identifier: value.identifier,
                    identifierTypeId: Number(value.identifierId),
                    usageTypeId: Number(value.usageId),
                    invoiceAddress: this.state.check ? this.state.invoiceAddress : value.invoiceAddress,
                    locationId: Number(value.village),
                    nameEn: value.nameEn.trim(),
                    nameKh: value.nameKh.trim(),
                    occupationId: Number(value.occupationId),
                    phone: value.phone,
                    ratePerKwhId: Number(value.rateId),
                    licenseId: Number(value.licenseId),
                    room: Number(value.room),
                    voltageTypeId: Number(value.voltageId)
                }
            ));*/
            this.props.updateCustomerAction({
                id: Number(this.props.location.query.id),
                address: value.address,
                ampereId: Number(value.ampereId),
                bankId: value.bankId !== "" ? Number(value.bankId) : 0,
                boxId: Number(value.boxId.split("-")[0]),
                oldBoxId: this.props.customer.items[0].boxId,
                consumptionTypeId: Number(value.consumptionId),
                customerSaleTypeId: Number(value.saleId),
                gender: value.gender,
                groupInvoiceId: Number(value.groupInvoiceId),
                identifier: value.identifier,
                identifierTypeId: Number(value.identifierId),
                usageTypeId: Number(value.usageId),
                invoiceAddress: this.state.check ? this.state.invoiceAddress : value.invoiceAddress,
                locationId: Number(value.village),
                nameEn: value.nameEn.trim(),
                nameKh: value.nameKh.trim(),
                occupationId: Number(value.occupationId),
                phone: value.phone,
                ratePerKwhId: Number(value.rateId),
                licenseId: Number(value.licenseId),
                room: Number(value.room),
                voltageTypeId: Number(value.voltageId)
            });
        }
    }

    componentDidMount(){
        const cus = this.props.customer.items !== undefined ? this.props.customer.items[0] : undefined;
        if(cus !== undefined){
            this.props.getDistrictsAction(cus.province);
            this.props.getCommunesAction(cus.district);
            this.props.getVillagesAction(cus.commune);
            this.props.getPolesAction({
                area: cus.areaId ,
                transId: 0,
                serial: "",
                limit: 1000,
                page: 1,
                orderBy: "id",
                orderType: "asc"
            });
            this.props.getBoxesAction({
                serial: '',
                id: cus.poleId,
                limit: 1000,
                page: 1,
                orderBy: "id"
            });
            const customer = {
                id: cus.id,
                nameKh: cus.nameKh,
                nameEn: cus.nameEn,
                gender: cus.gender,
                phone: cus.phone,
                identifierId: cus.identifierId,
                usageId: cus.usageId,
                identifier: cus.identifier,
                occupationId: cus.occupationId,
                country: "Cambodia",
                province: cus.province,
                district: cus.district,
                commune: cus.commune,
                village: cus.village,
                address: cus.address,
                invoiceAddress: cus.invoiceAddress,
                room: cus.room,
                rateId: cus.rateId,
                licenseId: cus.licenseId,
                consumptionId: cus.consumptionId,
                saleId: cus.saleId,
                typeId: cus.typeId,
                groupInvoiceId: cus.groupInvoiceId,
                bankId: cus.bankId,
                phaseId: cus.phaseId,
                ampereId: cus.ampereId,
                transSerial: cus.transSerial,
                voltageId: cus.voltageId,
                boxId: cus.boxId + '-' + cus.boxRoom + '-' + cus.boxAvailable,
                poleId: cus.poleId
            };
            this.props.dispatch(initialize('form_edit_customer', customer));
        } else {
            location.href = "/app";
        }
    }

    componentWillReceiveProps(data){
        let amperes = [],
            boxes = [],
            poles = [],
            rates = [],
            licenses = [],
            consumptions = [],
            groupInvoices = [],
            customerSaleTypes = [],
            customerTypes = [],
            voltageTypes = [],
            occupations = [],
            areas = [],
            banks = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [],
            areaId = 0;

        if(data.customer.items !== undefined && this.props.customer.items !== undefined){
            areaId = data.customer.items[0].areaId;
            this.setState({loading: 'none'});
            if (data.customer.items[0].id !== this.props.customer.items[0].id){
                const cus = data.customer.items[0];
                if(cus !== undefined){
                    this.props.getDistrictsAction(cus.province);
                    this.props.getCommunesAction(cus.district);
                    this.props.getVillagesAction(cus.commune);
                    data.getPolesAction({
                        area: cus.areaId ,
                        transId: 0,
                        serial: "",
                        limit: 1000,
                        page: 1,
                        orderBy: "id",
                        orderType: "asc"
                    });
                    data.getBoxesAction({
                        serial: '',
                        id: cus.poleId,
                        limit: 1000,
                        page: 1,
                        orderBy: "id"
                    });
                    const customer = {
                        id: cus.id,
                        nameKh: cus.nameKh,
                        nameEn: cus.nameEn,
                        gender: cus.gender,
                        phone: cus.phone,
                        identifierId: cus.identifierId,
                        usageId: cus.usageId,
                        identifier: cus.identifier,
                        occupationId: cus.occupationId,
                        country: "Cambodia",
                        province: cus.province,
                        district: cus.district,
                        commune: cus.commune,
                        village: cus.village,
                        address: cus.address,
                        invoiceAddress: cus.invoiceAddress,
                        room: cus.room,
                        rateId: cus.rateId,
                        licenseId: cus.licenseId,
                        consumptionId: cus.consumptionId,
                        saleId: cus.saleId,
                        typeId: cus.typeId,
                        groupInvoiceId: cus.groupInvoiceId,
                        bankId: cus.bankId,
                        phaseId: cus.phaseId,
                        ampereId: cus.ampereId,
                        transSerial: cus.transSerial,
                        voltageId: cus.voltageId,
                        boxId: cus.boxId + '-' + cus.boxRoom + '-' + cus.boxAvailable,
                        poleId: cus.poleId
                    };
                    this.props.dispatch(initialize('form_edit_customer', customer));
                } else {
                    location.href = "/app";
                }
                this.setState({loading: 'block'});
            }
        }

        if(data.ratePerKwhAll.items !== undefined){
            data.ratePerKwhAll.items.map((rate) => {
                rates.push({
                    id: rate.id,
                    name: rate.rate
                })
            })
        } else {rates = []}
        if(data.itemMvGetAll.items !== undefined){
            data.itemMvGetAll.items.map((license) => {
                licenses.push({
                    id: license.id,
                    name: license.licenseNameKh
                })
            })
        } else {licenses = []}
        if(data.consumptionTypeAll.items !== undefined){
            data.consumptionTypeAll.items.map((consumption) => {
                consumptions.push({
                    id: consumption.id,
                    name: consumption.consumptionEn
                })
            })
        } else {consumptions = []}
        if(data.groupInvoiceAll.items !== undefined){
            data.groupInvoiceAll.items.map((group) => {
                groupInvoices.push({
                    id: group.id,
                    name: group.groupName
                })
            })
        } else {groupInvoices = []}
        if(data.customerGroupAll.items !== undefined){
            data.customerGroupAll.items.map((saleType) => {
                customerSaleTypes.push({
                    id: saleType.id,
                    name: saleType.customerSaleTypeEn
                })
            })
        } else {customerSaleTypes = []}

        if(data.customerTypeAll.items !== undefined){
            data.customerTypeAll.items.map((type) => {
                customerTypes.push({
                    id: type.id,
                    name: type.customerTypeEn
                })
            })
        } else {customerTypes = []}

        if(data.bankAll.items !== undefined){
            data.bankAll.items.map((bank) => {
                banks.push({
                    id: bank.id,
                    name: bank.bankNameKh
                })
            })
        } else {banks = []}
        if(data.voltageTypeAll.items !== undefined){
            data.voltageTypeAll.items.map((type) => {
                voltageTypes.push({
                    id: type.id,
                    name: type.voltageType
                })
            })
        } else {voltageTypes = []}
        if(data.occupationAll.items !== undefined){
            data.occupationAll.items.map((occ) => {
                occupations.push({
                    id: occ.id,
                    name: occ.occupationKh || occ.occupationEn
                })
            })
        } else {occupations = []}

        if(data.areaAll.items !== undefined){
            areas = data.areaAll.items;
        }

        if(data.ampereAll.items !== undefined){
            data.ampereAll.items.map((ampere) => {
                amperes.push({
                    id: ampere.id,
                    name: ampere.ampere + "-" + ampere.value
                })
            })
        } else {amperes = []}
        if(data.boxes.items !== undefined){
            data.boxes.items.list.map((box) => {
                boxes.push({
                    id: box.id + '-' + box.room + '-' + box.available,
                    name: box.serial
                })
            })
        } else {boxes = []}
        if(data.customerGroupAll.items !== undefined){
            data.customerGroupAll.items.map((group) => {
                customerSaleTypes.push({
                    id: group.id,
                    name: group.customerSaleTypeEn
                })
            })
        } else {customerSaleTypes = []}

        if(data.poles.items !== undefined){
            data.poles.items.list.map((pole) => {
                poles.push({
                    id: pole.id,
                    name: pole.serial
                })
            });
        } else poles = [];

        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];

        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];

        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];

        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];

        this.setState({
            amperes: amperes,
            boxes: boxes,
            poles: poles,
            rates: rates,
            licenses: licenses,
            consumptions: consumptions,
            groupInvoices: groupInvoices,
            customerSaleTypes: customerSaleTypes,
            customerTypes: customerTypes,
            voltageTypes: voltageTypes,
            occupations: occupations,
            areas: areas,
            banks: banks,
            provinces: provinces,
            districts: districts,
            communes: communes,
            villages: villages,
            areaId: areaId
        });

        if(data.customerUpdate.status === 200){
            alert("Successful update customer !!");
            this.props.getCustomerAction(data.customer.items !== undefined ? data.customer.items[0].id : 0);
        } else if(data.customerUpdate.status === 500 || data.customerUpdate.status === 502){
            alert(data.customerUpdate.status + " Error !! \nFail edit Customer !!\n" + data.customerUpdate.message);
        } else if(data.customerUpdate.status === 401){
            alert("Please Login !!");
        }
        data.customerUpdate.status = 0;
    }

    handlePhase(e){
        this.props.getAllAmperesAction(e.target.value);
    }

    handleSelectPole(e){
        this.setState({room: ""});
        this.props.getBoxesAction({
            serial: '',
            id: e.target.value,
            limit: 1000,
            page: 1,
            orderBy: "id"
        });
    }

    handleSelectBox(e){
        alert(e.target.value);
        this.setState({room: e.target.value});
    }

    handleInvoiceAddress(e){
        if(e.target.checked){
            const invoiceAddress = document.getElementsByName('address')[0].value.concat(", " + this.state.address);
            document.getElementsByName('invoiceAddress')[0].value = invoiceAddress;
            this.setState({check: true, invoiceAddress: invoiceAddress});
        }else {
            document.getElementsByName('invoiceAddress')[0].value = "";
            this.setState({check: false, invoiceAddress: ""});
        }
    }

    handleRefresh(){
        this.props.getCustomerAction(this.props.customer.items !== undefined ? this.props.customer.items[0].id : 0);
    }

    handleSelectCustomerGroup(e){
        this.props.getAllCustomerTypesAction(Number(e.target.value));
    }

    infoTab(){
        this.setState({
            info: true,
            network: false,
            finance: false,
            history: false,
            graph: false,
            audit: false
        })
    }
    networkTab(){
        this.setState({
            info: false,
            network: true,
            finance: false,
            history: false,
            graph: false,
            audit: false
        });
    }
    financeTab(){
        this.setState({
            info: false,
            network: false,
            finance: true,
            history: false,
            graph: false,
            audit: false
        })
    }
    historyTab(){
        this.setState({
            info: false,
            network: false,
            finance: false,
            history: true,
            graph: false,
            audit: false
        })
    }
    graphTab(){
        this.setState({
            info: false,
            network: false,
            finance: false,
            history: false,
            graph: true,
            audit: false
        })
    }
    auditTab(){
        this.setState({
            info: false,
            network: false,
            finance: false,
            history: false,
            graph: false,
            audit: true
        })
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;
        const customer = this.props.customer.items !== undefined ? this.props.customer.items[0] : this.props.customer.items;
        let meters = [];
        let breakers = [];
        if(customer !== undefined){
            meters = customer.meters !== null ? JSON.parse(customer.meters) : [];
            breakers = customer.breakers !== null ? JSON.parse(customer.breakers) : [];
        }
        const renderCustomerInfo = () => {
            if(customer !== undefined){
                return(
                <div className="row customer-information">
                    <br/>
                    <div className="col-xs-12 col-sm-12 col-md-12">
                        <table className="table table-inverse customer-information">
                            <tbody>
                            <tr>
                                <td><strong>Customer ID: </strong></td>
                                <td>{customer.id}</td>
                                <td><strong>Name: </strong></td>
                                <td>{customer.nameKh}</td>
                                <td><strong>Register Date: </strong></td>
                                <td><p className="cus-info-label-status">{moment(customer.register).format('YYYY-MM-DD')}</p></td>
                                <td><strong>Status: </strong></td>
                                <td><p className="cus-info-label-status">{customer.status}</p></td>
                                <td><strong>Type: </strong></td>
                                <td><p className="cus-info-label-status">{customer.usageId === 1 ? 'Pre-Paid(IC)' : customer.usageId === 2 ? 'Pre-Paid(RF)' : 'Post-Paid'}</p></td>
                            </tr>
                            { meters !== undefined ?
                                meters.length === 1 ?
                                    <tr>
                                        <td><strong>Meter serial: </strong></td>
                                        <td>{meters[0].serial + '/' + meters[0].phase}</td>
                                        <td><strong>Last record: </strong></td>
                                        <td>{meters[0].lastRecord}</td>
                                        <td><strong>Remaining record: </strong></td>
                                        <td>{meters[0].originalRecord}</td>
                                        <td><strong>Breaker Info: </strong></td>
                                        <td>{ breakers !== undefined ?
                                            breakers.map(breaker => {return breaker.serial.concat("/")})
                                            : null
                                        }</td>
                                    </tr>
                                    :
                                    meters.map((meter, index) => {
                                        return (
                                            <tr key={index}>
                                                <td><strong>Meter serial {index+1}: </strong></td>
                                                <td>{meter.serial.concat("/") + meters[0].phase}</td>
                                                <td><strong>Last record: </strong></td>
                                                <td>{meter.lastRecord}</td>
                                                <td><strong>Remaining record: </strong></td>
                                                <td>{meter.originalRecord}</td>
                                                <td><strong>Breaker Info: </strong></td>
                                                <td>{ breakers !== undefined ?
                                                    breakers.map(breaker => {return breaker.serial.concat("/")})
                                                    : null
                                                }</td>
                                            </tr>
                                        )
                                    })
                                : null
                            }
                            </tbody>
                        </table>
                        <Row>
                            <Col lgOffset={6} lg={2}>
                                <Button onClick={this.handleRefresh} className="cus-info-btn">Refresh</Button>
                            </Col>
                            <Col lg={2}>
                                <Button className="cus-info-btn">Customer Change Log</Button>
                            </Col>
                            <Col lg={2}>
                                <Button className="cus-info-btn">Change Prepaid ID</Button>
                            </Col>
                        </Row>
                    </div>
                </div>
                )
            }
        };

        return (
            <div className="container-fluid">
                <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                    <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                    <span className="sr-only">Loading...</span>
                </div>
                { customer !== undefined ?
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card">
                                <ul className="nav nav-tabs search-tab" role="tablist">
                                    <li role="presentation" className="active"><a href="#customer-info" onClick={this.infoTab} aria-controls="customer-info" role="tab" data-toggle="tab">Customer Info</a></li>
                                    <li role="presentation"><a href="#network-management" onClick={this.networkTab} aria-controls="network" role="tab" data-toggle="tab">Network Management</a></li>
                                    <li role="presentation"><a href="#finance" onClick={this.financeTab} aria-controls="finance" role="tab" data-toggle="tab">Finance</a></li>
                                    {/*<li role="presentation"><a href="#user-history" onClick={this.historyTab} aria-controls="history" role="tab" data-toggle="tab">User History</a></li>
                                    <li role="presentation"><a href="#user-graph" onClick={this.graphTab} aria-controls="usergraph" role="tab" data-toggle="tab">User Graph</a></li>*/}
                                    <li role="presentation"><a href="#audit-trial" onClick={this.auditTab} aria-controls="audittrial" role="tab" data-toggle="tab">Audit Trial</a></li>
                                </ul>
                                <div className="search-tab tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="customer-info">
                                        { this.state.info ?
                                            <div>
                                                {renderCustomerInfo()}
                                                <div>
                                                    <div className="panel panel-default">
                                                        <div className="panel-heading">
                                                            <h3 className="panel-title">Customer Information</h3>
                                                        </div>
                                                        <div className="panel-body">
                                                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                                                <Row>
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Name (Khmer) <span className="label-require">*</span></strong>
                                                                                <Field name="nameKh" type="text" component={TextBox} label="Name (Khmer)" />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Name (Latin) <span className="label-require">*</span></strong>
                                                                                <Field name="nameEn" type="text" component={TextBox} label="Name (Latin)" />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Gender <span className="label-require">*</span></strong>
                                                                                <Field name="gender" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 'MALE', name: 'Male'},{id: 'FEMALE', name: 'Female'}]} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Phone Number <span className="label-require">*</span></strong>
                                                                                <Field name="phone" type="text" component={TextBox} label="Phone number" />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Identity Type <span className="label-require">*</span></strong>
                                                                                <Field name="identifierId" type="select" component={SelectBox} placeholder="Please select ..."
                                                                                       values={[
                                                                                           {id: 1, name: 'អត្តសញ្ញាណប័ណ្ណ'},
                                                                                           {id: 2, name: 'សៀវភៅគ្រួសារ'},
                                                                                           {id: 3, name: 'សំបុត្រកំណើត'},
                                                                                           {id: 4, name: 'កាតទាហ៊ាន'},
                                                                                           {id: 5, name: 'កាតប៉ូលីស'},
                                                                                           {id: 6, name: 'កាតគ្រូបង្រៀន'},
                                                                                           {id: 7, name: 'លិខិតឆ្លងដែន'},
                                                                                           {id: 8, name: 'លិខិតសំគាល់ខ្លួនផ្សេងៗ'}
                                                                                       ]}
                                                                                />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Identify/Family Number <span className="label-require">*</span></strong>
                                                                                <Field name="identifier" type="text" component={TextBox} label="Identify/Family number" />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Occupation <span className="label-require">*</span></strong>
                                                                                <Field name="occupationId" type="select" component={SelectBox} placeholder="Occupation ..." values={this.state.occupations} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Country <span className="label-require">*</span></strong>
                                                                                <Field name="country" type="text" component={TextBox} label="Country" disabled={true} icon="fa fa-map-marker" />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Province <span className="label-require">*</span></strong>
                                                                                <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" icon="fa fa-map-marker"/>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">District <span className="label-require">*</span></strong>
                                                                                <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" icon="fa fa-map-marker"/>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Commune <span className="label-require">*</span></strong>
                                                                                <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" icon="fa fa-map-marker"/>
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Village <span className="label-require">*</span></strong>
                                                                                <Field name="village" type="select" onChange={this.handleSelectVillage} component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" icon="fa fa-map-marker"/>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                </Row>

                                                                <Row>
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">License Name <span className="label-require">*</span></strong>
                                                                                <Field name="licenseId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.licenses} />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    <Col lg={8}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Invoice Address <span className="label-require">*</span></strong>
                                                                                <Field name="invoiceAddress" type="text" component={TextBox} label="Invoice Address" />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                </Row>

                                                                <hr />
                                                                <h4 style={{color: 'blue'}}>Meter and Breaker Info</h4>
                                                                <table className="table table-inverse customer-information" style={{borderStyle: 'groove', borderColor: '#1c91d0'}}>
                                                                    <tbody>
                                                                    { customer !== undefined ?
                                                                        meters !== undefined ?
                                                                            meters.length === 1 ?
                                                                                <tr>
                                                                                    <td><strong>Serial: </strong></td>
                                                                                    <td>{meters[0].serial}</td>
                                                                                    <td><strong>Phase: </strong></td>
                                                                                    <td>{meters[0].phase}</td>
                                                                                    <td><strong>Type: </strong></td>
                                                                                    <td>{meters[0].type}</td>
                                                                                    <td><strong>Ampere: </strong></td>
                                                                                    <td>{meters[0].minAmpere + '-' + meters[0].maxAmpere} A</td>
                                                                                    <td><strong>Last record: </strong></td>
                                                                                    <td>{meters[0].lastRecord}</td>
                                                                                    <td><strong>Original record: </strong></td>
                                                                                    <td>{meters[0].originalRecord}</td>
                                                                                    <td><strong>Breaker Info: </strong></td>
                                                                                    <td>{ breakers !== undefined ?
                                                                                        breakers.map(breaker => {return breaker.serial.concat("/")})
                                                                                        : null
                                                                                    }</td>
                                                                                </tr>
                                                                                :
                                                                                meters.map((meter, index) => {
                                                                                    return (
                                                                                        <tr key={index}>
                                                                                            <td><strong>Serial {index + 1}: </strong></td>
                                                                                            <td>{meter.serial}</td>
                                                                                            <td><strong>Phase: </strong></td>
                                                                                            <td>{meter.phase}</td>
                                                                                            <td><strong>Type: </strong></td>
                                                                                            <td>{meter.type}</td>
                                                                                            <td><strong>Ampere: </strong></td>
                                                                                            <td>{meter.minAmpere + '-' + meter.maxAmpere} A</td>
                                                                                            <td><strong>Last record: </strong></td>
                                                                                            <td>{meter.lastRecord}</td>
                                                                                            <td><strong>Remaining record: </strong></td>
                                                                                            <td>{meter.originalRecord}</td>
                                                                                            <td><strong>Breaker Info: </strong></td>
                                                                                            <td>{ breakers !== undefined ?
                                                                                                breakers.map(breaker => {return breaker.serial.concat("/")})
                                                                                                : null
                                                                                            }</td>
                                                                                        </tr>
                                                                                    )
                                                                                })
                                                                            : null
                                                                        : null
                                                                    }
                                                                    </tbody>
                                                                </table>
                                                                <hr />
                                                                <Row>
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Rate <span className="label-require">*</span></strong>
                                                                                <Field name="rateId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.rates} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Customer Type <span className="label-require">*</span></strong>
                                                                                <Field name="typeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.customerTypes} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Customer Sale Type <span className="label-require">*</span></strong>
                                                                                <Field name="saleId" type="select" onChange={this.handleSelectCustomerGroup} component={SelectBox} placeholder="Please select ..." values={this.state.customerSaleTypes} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Consumption Type <span className="label-require">*</span></strong>
                                                                                <Field name="consumptionId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.consumptions} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Group Invoice <span className="label-require">*</span></strong>
                                                                                <Field name="groupInvoiceId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.groupInvoices} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-require">Note: '*' Fields are require !</strong>
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    {/*Row 2*/}
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Customer Usage Type <span className="label-require">*</span></strong>
                                                                                <Field name="usageId" type="select" component={SelectBox} placeholder="Please select ..."
                                                                                       values={[
                                                                                           {id: 1, name: 'Pre-Paid(IC)'},
                                                                                           {id: 2, name: 'Pre-Paid(RF)'},
                                                                                           {id: 3, name: 'Post-Paid'}
                                                                                       ]}
                                                                                />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Pay us by Bank</strong>
                                                                                <Field name="bankId" type="select" component={SelectBox} placeholder="Please select ..."
                                                                                       values={this.state.banks} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Phase Type <span className="label-require">*</span></strong>
                                                                                <Field name="phaseId" type="select" onChange={this.handlePhase} component={SelectBox} placeholder="Please select ..."
                                                                                       values={[{id: 1, name: '1PH'},{id: 2, name: '2PH'}, {id:3, name: '3PH'}]} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Ampere <span className="label-require">*</span></strong>
                                                                                <Field name="ampereId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.amperes} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Voltage Type <span className="label-require">*</span></strong>
                                                                                <Field name="voltageId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.voltageTypes} />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                    {/* Row 3 */}
                                                                    <Col lg={4}>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Area <span className="label-require">*</span></strong>
                                                                                <Typeahead
                                                                                    clearButton
                                                                                    selected={this.state.areas.filter(area => area.id === this.state.areaId)}
                                                                                    onChange={selected => { selected === undefined ? console.log() :
                                                                                        this.props.dispatch(change('form_edit_customer', 'transSerial' , _.map(selected, "serial")[0] || 'None' ));
                                                                                        this.props.getPolesAction({
                                                                                            area: _.map(selected, "id")[0],
                                                                                            transId: 0,
                                                                                            serial: "",
                                                                                            limit: 1000,
                                                                                            page: 1,
                                                                                            orderBy: "id",
                                                                                            orderType: "asc"
                                                                                        })
                                                                                    }}
                                                                                    labelKey="areaNameEn"
                                                                                    options={this.state.areas !== undefined ? this.state.areas : []}
                                                                                    placeholder="Area Name ..."
                                                                                />
                                                                            </Col>
                                                                        </Row>

                                                                        <Row style={{marginTop: '13px'}}>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Transformer </strong>
                                                                                <Field name="transSerial" type="text" component={TextBox} disabled={true} label="Transformer serial" />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Pole <span className="label-require">*</span></strong>
                                                                                <Field name="poleId" type="select" onChange={this.handleSelectPole} component={SelectBox} placeholder="Please select ..." values={this.state.poles} />
                                                                            </Col>
                                                                        </Row>
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Box No <span className="label-require">*</span></strong>
                                                                                <Field name="boxId" type="select" onChange={this.handleSelectBox} component={SelectBox} placeholder="Please select ..." values={this.state.boxes} />
                                                                            </Col>
                                                                        </Row>
                                                                        { this.state.room === "" ? null :
                                                                            <div>
                                                                                <p style={{color: 'blue'}}>Total room(s) : {this.state.room.split("-")[1]}</p> &nbsp;&nbsp;
                                                                                <p style={{color: 'blue'}}>Available room(s) : {this.state.room.split("-")[2]}</p>
                                                                            </div>
                                                                        }
                                                                        <Row>
                                                                            <Col md={12} lg={12}>
                                                                                <strong className="label-name">Room No <span className="label-require">*</span></strong>
                                                                                <Field name="room" type="text" component={TextBox} label="Room no" />
                                                                            </Col>
                                                                        </Row>
                                                                    </Col>
                                                                </Row>
                                                                <Row>
                                                                    <Col mdOffset={10} lgOffset={10} md={2} lg={2}>
                                                                        <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                                                    </Col>
                                                                </Row>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="network-management">
                                        { this.state.network ?
                                            <div>
                                                {renderCustomerInfo()}
                                                <div className="row">
                                                    <LayoutNetworkManagement customer={customer !== undefined ? customer : undefined} />
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="finance">
                                        { this.state.finance ?
                                            <div>
                                                {renderCustomerInfo()}
                                                <div className="row">
                                                    <LayoutFinance customer={customer !== undefined ? customer : undefined} />
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                    {/*<div role="tabpanel" className="tab-pane" id="user-history">
                                        { this.state.history ?
                                            <div>
                                                <div className="row customer-information">
                                                    <br/>
                                                    <div className="col-xs-12 col-sm-12 col-md-12">
                                                        <table className="table table-inverse customer-information">
                                                            {renderCustomerInfo()}
                                                        </table>
                                                        <Row>
                                                            <Col lgOffset={8} lg={2}>
                                                                <Button className="cus-info-btn">Customer Change Log</Button>
                                                            </Col>
                                                            <Col lg={2}>
                                                                <Button className="cus-info-btn">Change Prepaid ID</Button>
                                                            </Col>
                                                        </Row>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <h2>User History</h2>
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="user-graph">
                                        { this.state.graph ?
                                            <div>
                                                <div className="row customer-information">
                                                    <br/>
                                                    <div className="col-xs-12 col-sm-12 col-md-12">
                                                        <table className="table table-inverse customer-information">
                                                            <tbody>
                                                            <tr>
                                                                <td>CustomerId:</td>
                                                                <td>0001</td>
                                                                <td>Last Record:</td>
                                                                <td>0</td>
                                                                <td>Meter Info:</td>
                                                                <td>1007150906 , 1Ph</td>
                                                                <td>Status:</td>
                                                                <td>Using</td>
                                                                <td>Type</td>
                                                                <td>Pre-paid (IC)</td>
                                                            </tr>
                                                            <tr>
                                                                <td>CustomerName:</td>
                                                                <td>Sok Dara</td>
                                                                <td>Remaining Usage:</td>
                                                                <td>0</td>
                                                                <td>Breaker:</td>
                                                                <td>Bk 0001</td>
                                                                <td>Register Date:</td>
                                                                <td>01/01/2017</td>
                                                                <td>Deposit:</td>
                                                                <td>0</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <h2>Graph</h2>
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>*/}
                                    <div role="tabpanel" className="tab-pane" id="audit-trial">
                                        { this.state.audit ?
                                            <div>
                                                {renderCustomerInfo()}
                                                <div className="row">
                                                    <h2 style={{textAlign: 'center'}}>Under construction</h2>
                                                </div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    : null
                }
            </div>
        )
    }
}
EditCustomer = reduxForm({
    form: 'form_edit_customer',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.nameKh.length < 2){
            errors.nameKh = 'Please input at least 2 characters !!';
        }
        if(values.nameEn.length < 2){
            errors.nameEn = 'Please input at least 2 characters !!';
        }
        if(values.gender === ""){
            errors.gender = 'Please select gender !!';
        }

        if(values.identifierId === ""){
            errors.identifierId = 'Please select Identity Type !!';
        }

        if(values.rateId === ""){
            errors.rateId = 'Please select Rate per Kwh !!';
        }

        if(values.groupInvoiceId === ""){
            errors.groupInvoiceId = 'Please select Group Invoice !!';
        }

        if(values.consumptionId === ""){
            errors.consumptionId = 'Please select Consumption Type !!';
        }
        if(values.saleId === ""){
            errors.saleId = 'Please select Customer Sale Type !!';
        }

        if(values.typeId === ""){
            errors.typeId = 'Please select Customer Type !!';
        }

        if(values.ampereId === ""){
            errors.ampereId = 'Please select Ampere !!';
        }

        if(values.voltageId === ""){
            errors.voltageId = 'Please select Voltage Type !!';
        }

        if(values.boxId === ""){
            errors.boxId = 'Please select Box !!';
        }

        if(!number.test(values.room)){
            errors.room = 'Please input number only !!';
        }

        if (values.province === ("0" || "")) {
            errors.province = "Please select province !!";
        }
        if (values.district === ("0" || "")) {
            errors.district = "Please select district !!";
        }
        if (values.commune === ("0" || "")) {
            errors.commune = "Please select district !!";
        }
        if (values.village === ("0" || "")) {
            errors.village = "Please select district !!";
        }
        return errors
    },
})(EditCustomer);

function mapStateToProps(state) {
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        consumptionTypeAll: state.consumptionType.consumptionTypeAll,
        groupInvoiceAll: state.groupInvoice.groupInvoiceAll,
        customerGroupAll: state.customerGroup.customerGroupAll,
        customerTypeAll: state.customerType.customerTypeAll,
        voltageTypeAll: state.voltageType.voltageTypeAll,
        ampereAll: state.ampere.ampereAll,
        boxes: state.box.boxes,
        occupationAll: state.occupation.occupationAll,
        poles: state.pole.poles,
        areaAll: state.area.areaAll,
        bankAll: state.bank.bankAll,
        transformerAll: state.transformer.transformerAll,
        customerUpdate: state.customer.customerUpdate,
        customer: state.customer.customer,
        itemMvGetAll: state.changeMeter.itemMvGetAll,
        initialValues: {
            nameKh: "",
            nameEn: "",
            gender: "",
            phone: "",
            identifierId: "",
            identifier: "",
            occupationId: "",
            country: "",
            province: "0",
            district: "0",
            commune: "0",
            village: "0",
            address: "",
            invoiceAddress: "",
            rateId: "",
            groupInvoiceId: "",
            consumptionId: "",
            saleId: "",
            typeId: "",
            bankId: "",
            phaseId: "",
            ampereId: "",
            transSerial: "",
            voltageId: "",
            boxId: "",
            room: "",
            licenseId: ""
        }
    })
}

function mapDispatchToProps(dispatch) {

    return bindActionCreators({
        updateCustomerAction,
        getCustomerAction,
        getDistrictsAction,
        getCommunesAction,
        getVillagesAction,
        getPolesAction,
        getBoxesAction,
        getAllAmperesAction}, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(EditCustomer);