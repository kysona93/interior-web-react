import React from '../../../../../node_modules/react';
import _ from '../../../../../node_modules/lodash';
import {connect} from 'react-redux';
import { push } from 'react-router-redux';
import { store } from "../../../../store.js";
import { Row, Col, Checkbox } from 'react-bootstrap';
import { Field, reduxForm, initialize, change } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import SelectBox from '../../../components/forms/SelectBox';
import {Typeahead} from 'react-bootstrap-typeahead';
import 'react-bootstrap-typeahead/css/Typeahead.css';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction } from '../../../actions/location';
import { getAllRatePerKwhAction } from './../../../actions/setup/rateKwh';
import { getAllConsumptionTypesAction} from '../../../actions/setup/type/consumptionType';
import { getAllGroupInvoiceAction } from './../../../actions/invoice/groupInvoices';
import { getAllCustomerGroupsAction } from '../../../actions/setup/type/customerGroup';
import { getAllCustomerTypesAction } from './../../../actions/setup/type/customerType';
import { getAllVoltageTypesAction } from './../../../actions/setup/type/voltageType';
import { getAllAmperesAction } from './../../../actions/setup/ampere';
import { getBoxesAction } from './../../../actions/setup/box';
import { getAllOccupationsAction } from './../../../actions/setup/occupation';
import { getAllAreasAction } from './../../../actions/setup/area';
import { getPolesAction } from './../../../actions/setup/pole';
import { getAllBanksAction } from './../../../actions/setup/bank';
import { addCustomerAction, getCustomerAction } from './../../../actions/customer/customer';
import { getAllItemsMvAction } from './../../../actions/customer/network/meter';
import './style.css';

class AddCustomer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            createdDate: null,
            districts: [],
            communes: [],
            villages: [],
            amperes: [],
            boxes: [],
            poles: [],
            provinces: [],
            rates: [],
            licenses: [],
            consumptions: [],
            groupInvoices: [],
            customerSaleTypes: [],
            voltageTypes: [],
            occupations: [],
            areas: [],
            banks: [],
            room: "",
            check: false,
            address: "",
            invoiceAddress: "",
            occId: 0,
            occupation: "",
            name: ""
        };
        this.handleCreatedDate = this.handleCreatedDate.bind(this);
        this.handleSelectProvince = this.handleSelectProvince.bind(this);
        this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
        this.handleSelectCommune = this.handleSelectCommune.bind(this);
        this.handleSelectVillage = this.handleSelectVillage.bind(this);
        this.handlePhase = this.handlePhase.bind(this);
        this.handleSelectBox = this.handleSelectBox.bind(this);
        this.handleInvoiceAddress = this.handleInvoiceAddress.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCreatedDate(date) {
        this.setState({createdDate: date});
    }

    static handleAddress(value){
        return value.options[value.selectedIndex].text;
    }

    handleNewTab(link, name){
        if(link !== null && link !== undefined && link !== ""){
            this.props.tab(
                link,
                name
            );
            store.dispatch(push(link));
        }
    }

    handleSelectProvince(event){
        this.props.getDistrictsAction(event.target.value);
    }

    handleSelectDistrict(event){
        this.props.getCommunesAction(event.target.value);
    }

    handleSelectCommune(event){
        this.props.getVillagesAction(event.target.value);
    }

    handleSelectVillage(){
        this.setState({address: AddCustomer.handleAddress(document.getElementsByName('village')[0]).concat(", " + this.state.address)});
    }

    handleSubmit(value){
        this.setState({name: value.nameKh.trim()});
        let room = -1;
        if(Number(this.state.room.split("-")[2]) <= 0){
            room = -1;
        }else {
            room = Number(value.room);
        }
        this.props.addCustomerAction({
            address: value.address,
            ampereId: Number(value.ampereId),
            bankId: value.bankId !== "" ? Number(value.bankId) : 0,
            boxId: Number(value.boxId.split("-")[0]),
            consumptionTypeId: Number(value.consumptionId),
            customerSaleTypeId: Number(value.customerSaleTypeId),
            gender: value.gender,
            groupInvoiceId: Number(value.groupInvoiceId),
            identifier: value.identifier,
            identifierTypeId: Number(value.identifierTypeId),
            invoiceAddress: this.state.check ? this.state.invoiceAddress : value.invoiceAddress,
            usageTypeId: Number(value.usageTypeId),
            locationId: Number(value.village),
            nameEn: value.nameEn.trim(),
            nameKh: value.nameKh.trim(),
            occupation: this.state.occupation.trim(),
            occupationId: this.state.occId !== undefined ? this.state.occId : 0,
            phone:  value.phone,
            ratePerKwhId: Number(value.rateId),
            licenseId: value.licenseId !=="" ? Number(value.licenseId) : '',
            registerDate: value.registerDate,
            room: room,
            voltageTypeId: Number(value.voltageId)
        });
    }

    componentWillMount(){
        this.props.getProvincesAction();
        this.props.getAllRatePerKwhAction();
        this.props.getAllConsumptionTypesAction();
        this.props.getAllGroupInvoiceAction();
        this.props.getAllItemsMvAction({name:''});
        this.props.getAllCustomerTypesAction();
        this.props.getAllVoltageTypesAction();
        this.props.getAllOccupationsAction();
        this.props.getAllAreasAction();
        this.props.getAllBanksAction();

        this.props.getAllCustomerGroupsAction(0);
        this.props.getAllAmperesAction(0);
    }

    componentWillReceiveProps(data){
        let amperes = [],
            boxes = [],
            poles = [],
            provinces = [],
            districts = [],
            communes = [],
            villages = [],
            rates = [],
            licenses = [],
            consumptions = [],
            groupInvoices = [],
            customerSaleTypes = [],
            voltageTypes = [],
            occupations = [],
            areas = [],
            banks = [];

        if(data.provinces.items !== undefined){
            data.provinces.items.map((province) => {
                provinces.push({
                    id: province.id,
                    name: province.name
                })
            })
        } else provinces = [];
        if(data.districts.items !== undefined){
            data.districts.items.map((district) => {
                districts.push({
                    id: district.id,
                    name: district.name
                })
            })
        } else districts = [];
        if(data.communes.items !== undefined){
            data.communes.items.map((commune) => {
                communes.push({
                    id: commune.id,
                    name: commune.name
                })
            })
        } else communes = [];
        if(data.villages.items !== undefined){
            data.villages.items.map((village) => {
                villages.push({
                    id: village.id,
                    name: village.name
                })
            })
        } else villages = [];

        if(data.ratePerKwhAll.items !== undefined){
            data.ratePerKwhAll.items.map((rate) => {
                rates.push({
                    id: rate.id,
                    name: rate.rate
                })
            })
        } else {rates = []}
        if(data.itemMvGetAll.items !== undefined){
            data.itemMvGetAll.items.map((license) => {
                licenses.push({
                    id: license.id,
                    name: license.licenseNameKh
                })
            })
        } else {licenses = []}
        if(data.consumptionTypeAll.items !== undefined){
            data.consumptionTypeAll.items.map((consumption) => {
                consumptions.push({
                    id: consumption.id,
                    name: consumption.consumptionKh || consumption.consumptionEn
                })
            })
        } else {consumptions = []}
        if(data.groupInvoiceAll.items !== undefined){
            data.groupInvoiceAll.items.map((group) => {
                groupInvoices.push({
                    id: group.id,
                    name: group.groupName
                })
            })
        } else {groupInvoices = []}
        if(data.customerGroupAll.items !== undefined){
            data.customerGroupAll.items.map((saleType) => {
                customerSaleTypes.push({
                    id: saleType.id,
                    name: saleType.customerSaleTypeEn
                })
            })
        } else {customerSaleTypes = []}
        if(data.bankAll.items !== undefined){
            data.bankAll.items.map((bank) => {
                banks.push({
                    id: bank.id,
                    name: bank.bankNameKh
                })
            })
        } else {banks = []}
        if(data.voltageTypeAll.items !== undefined){
            data.voltageTypeAll.items.map((type) => {
                voltageTypes.push({
                    id: type.id,
                    name: type.voltageType
                })
            })
        } else {voltageTypes = []}
        if(data.occupationAll.items !== undefined){
            occupations = data.occupationAll.items;
        }
        if(data.areaAll.items !== undefined){
            areas = data.areaAll.items;
        }

        if(data.ampereAll.items !== undefined){
            data.ampereAll.items.map((ampere) => {
                amperes.push({
                    id: ampere.id,
                    name: ampere.ampere + "-" + ampere.value
                })
            })
        } else {amperes = []}
        if(data.boxes.items !== undefined){
            data.boxes.items.list.map((box) => {
                boxes.push({
                    id: box.id + '-' + box.room + '-' + box.available,
                    name: box.serial
                })
            })
        } else {boxes = []}
        if(data.customerGroupAll.items !== undefined){
            data.customerGroupAll.items.map((group) => {
                customerSaleTypes.push({
                    id: group.id,
                    name: group.customerSaleTypeEn
                })
            })
        } else {customerSaleTypes = []}

        if(data.poles.items !== undefined){
            poles = data.poles.items.list;
        } else poles = [];

        this.setState({
            amperes: amperes,
            boxes: boxes,
            poles: poles,
            provinces: provinces,
            districts: districts,
            communes: communes,
            villages: villages,
            rates: rates,
            licenses: licenses,
            consumptions: consumptions,
            groupInvoices: groupInvoices,
            customerSaleTypes: customerSaleTypes,
            voltageTypes: voltageTypes,
            occupations: occupations,
            areas: areas,
            banks: banks
        });

        if(data.customerAdd.status === 200){
            alert(`Successful add new Customer ${data.customerAdd.id} !!`);
            this.props.getCustomerAction(data.customerAdd.id);
            this.props.dispatch(initialize('form_register_customer', null));
            setTimeout(() => {
                this.handleNewTab(`/app/customers/info?id=${data.customerAdd.id}&name=${this.state.name}`, this.state.name);
            }, 1500)
        } else if(data.customerAdd.status === (400 || 404) || data.customerAdd.status === (500 || 502)){
            alert(data.customerAdd.status + " Error !! \nFail add new Customer !!\n" + data.customerAdd.message);
        } else if(data.customerAdd.status === 401){
            alert("Please Login !!");
        }
        data.customerAdd.status = 0;
    }

    handlePhase(e){
        this.props.getAllAmperesAction(e.target.value);
    }

    handleSelectBox(e){
        this.setState({room: e.target.value});
    }

    handleInvoiceAddress(e){
        if(e.target.checked){
            const invoiceAddress = document.getElementsByName('address')[0].value.concat(", " + this.state.address);
            document.getElementsByName('invoiceAddress')[0].value = invoiceAddress;
            this.setState({check: true, invoiceAddress: invoiceAddress});
        }else {
            document.getElementsByName('invoiceAddress')[0].value = "";
            this.setState({check: false, invoiceAddress: ""});
        }
    }

    render() {
        const {handleSubmit, error, submitting} = this.props;


        return (
            <div className="container-fluid">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">New Customer</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit)} className="wrap-full-form">
                            <Row>
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Name (Khmer) <span className="label-require">*</span></strong>
                                            <Field name="nameKh" type="text" component={TextBox} label="Name (Khmer)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Name (Latin) <span className="label-require">*</span></strong>
                                            <Field name="nameEn" type="text" component={TextBox} label="Name (Latin)" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Gender <span className="label-require">*</span></strong>
                                            <Field name="gender" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 'MALE', name: 'Male'},{id: 'FEMALE', name: 'Female'}]} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Phone Number <span className="label-require">*</span></strong>
                                            <Field name="phone" type="text" component={TextBox} label="Phone number" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Identity Type <span className="label-require">*</span></strong>
                                            <Field name="identifierTypeId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[
                                                       {id: 1, name: 'អត្តសញ្ញាណប័ណ្ណ'},
                                                       {id: 2, name: 'សៀវភៅគ្រួសារ'},
                                                       {id: 3, name: 'សំបុត្រកំណើត'},
                                                       {id: 4, name: 'កាតទាហ៊ាន'},
                                                       {id: 5, name: 'កាតប៉ូលីស'},
                                                       {id: 6, name: 'កាតគ្រូបង្រៀន'},
                                                       {id: 7, name: 'លិខិតឆ្លងដែន'},
                                                       {id: 8, name: 'លិខិតសំគាល់ខ្លួនផ្សេងៗ'}
                                                   ]}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Identify/Family Number <span className="label-require">*</span></strong>
                                            <Field name="identifier" type="text" component={TextBox} label="Identify/Family number" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Occupation <span className="label-require">*</span></strong>
                                            <Typeahead
                                                clearButton
                                                onInputChange={e => this.setState({occupation: e})}
                                                onChange={selected => {this.setState({occId: _.map(selected, "id")[0]})}}
                                                labelKey="occupationKh"
                                                options={this.state.occupations}
                                                placeholder="Occupation ..."
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Country <span className="label-require">*</span></strong>
                                            <Field name="country" type="select" component={SelectBox} placeholder="Please select ..." values={[{id: 1, name: "Cambodia"}]} disabled={true} sortBy="name" />
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Province <span className="label-require">*</span></strong>
                                            <Field name="province" type="select" onChange={this.handleSelectProvince} component={SelectBox} placeholder="Please select ..." values={this.state.provinces} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">District <span className="label-require">*</span></strong>
                                            <Field name="district" type="select" onChange={this.handleSelectDistrict} component={SelectBox} placeholder="Please select ..." values={this.state.districts} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Commune <span className="label-require">*</span></strong>
                                            <Field name="commune" type="select" onChange={this.handleSelectCommune} component={SelectBox} placeholder="Please select ..." values={this.state.communes} sortBy="name" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Village <span className="label-require">*</span></strong>
                                            <Field name="village" type="select" onChange={this.handleSelectVillage} component={SelectBox} placeholder="Please select ..." values={this.state.villages} sortBy="name" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4}>
                                    <strong className="label-name">Register Date <span className="label-require">*</span></strong>
                                    <Field name="registerDate" component={DateTimePicker} placeholder="Created date" defaultDate={this.state.createdDate} handleChange={this.handleCreatedDate}/>
                                </Col>
                                <Col md={4} lg={4}>
                                    <strong className="label-name">Customer Address <span className="label-require">*</span></strong>
                                    <Field name="address" type="text" component={TextBox} label="Home no, Road no .." />
                                </Col>
                                <Col md={4} lg={4}>
                                    <strong className="label-name">License Name</strong>
                                    <Field name="licenseId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.licenses} />
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} lg={12}>
                                    <Checkbox onClick={this.handleInvoiceAddress} inline ><strong style={{color: 'blue'}}>Use above address?</strong></Checkbox>
                                    <br />
                                    <strong className="label-name">Invoice Address <span className="label-require">*</span></strong>
                                    <Field name="invoiceAddress" type="text" component={TextArea} height="59px" label="Address"/>
                                </Col>
                            </Row>
                            <hr />
                            <Row>
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Rate <span className="label-require">*</span></strong>
                                            <Field name="rateId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.rates} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Consumption Type <span className="label-require">*</span></strong>
                                            <Field name="consumptionId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.consumptions} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Customer Type <span className="label-require">*</span></strong>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { selected === undefined ? console.log() :
                                                    this.props.getAllCustomerGroupsAction(_.map(selected, "id")[0]);
                                                }}
                                                labelKey="customerTypeEn"
                                                options={this.props.customerTypeAll.items || []}
                                                placeholder="Customer Type ..."
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Customer Sale Type <span className="label-require">*</span></strong>
                                            <Field name="customerSaleTypeId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.customerSaleTypes} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Group Invoice <span className="label-require">*</span></strong>
                                            <Field name="groupInvoiceId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.groupInvoices} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-require">Note: '*' Fields are require !</strong>
                                        </Col>
                                    </Row>
                                </Col>
                                {/** Row 2 **/}
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Customer Usage Type <span className="label-require">*</span></strong>
                                            <Field name="usageTypeId" type="select" component={SelectBox} placeholder="Please select ..."
                                                   values={[
                                                       {id: 1, name: 'Pre-Paid(IC)'},
                                                       {id: 2, name: 'Pre-Paid(RF)'},
                                                       {id: 3, name: 'Post-Paid'}
                                                   ]}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Pay us by Bank</strong>
                                            <Field name="bankId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.banks} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Phase Type <span className="label-require">*</span></strong>
                                            <Field name="customerPhase" type="select" onChange={this.handlePhase} component={SelectBox} placeholder="Please select ..."
                                                   values={[{id: 1, name: '1PH'},{id: 2, name: '2PH'}, {id:3, name: '3PH'}]} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Ampere <span className="label-require">*</span></strong>
                                            <Field name="ampereId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.amperes} />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Voltage Type <span className="label-require">*</span></strong>
                                            <Field name="voltageId" type="select" component={SelectBox} placeholder="Please select ..." values={this.state.voltageTypes} />
                                        </Col>
                                    </Row>
                                </Col>
                                {/** Row 3 **/}
                                <Col lg={4}>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Area <span className="label-require">*</span></strong>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { selected === undefined ? console.log() :
                                                    this.props.dispatch(change('form_register_customer', 'transformer' , _.map(selected, "serial")[0] || 'None' ));
                                                    this.props.getPolesAction({
                                                        area: _.map(selected, "id")[0],
                                                        serial: "",
                                                        limit: 1000,
                                                        page: 1,
                                                        orderBy: "id",
                                                        orderType: "asc"
                                                    })
                                                }}
                                                labelKey="areaNameEn"
                                                options={this.state.areas}
                                                placeholder="Area Name ..."
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Transformer </strong>
                                            <Field name="transformer" type="text" component={TextBox} disabled={true} label="Transformer serial" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Pole <span className="label-require">*</span></strong>
                                            <Typeahead
                                                clearButton
                                                onChange={selected => { selected === undefined ? console.log() :
                                                    this.props.getBoxesAction({
                                                        serial: '',
                                                        id: _.map(selected, "id")[0],
                                                        limit: 1000,
                                                        page: 1,
                                                        orderBy: "id"
                                                    });
                                                }}
                                                labelKey="serial"
                                                options={this.state.poles}
                                                placeholder="Pole ..."
                                            />
                                        </Col>
                                    </Row>
                                    <Row style={{marginTop: '13px'}}>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Box No <span className="label-require">*</span></strong>
                                            <Field name="boxId" type="select" onChange={this.handleSelectBox} component={SelectBox} placeholder="Please select ..." values={this.state.boxes} />
                                        </Col>
                                    </Row>
                                    { this.state.room === "" ? null :
                                        <div>
                                            <strong style={{color: 'blue'}}>Total room(s) : {this.state.room.split("-")[1]}</strong> &nbsp;&nbsp;
                                            <strong style={{color: 'blue'}}>Available room(s) : {this.state.room.split("-")[2]}</strong>
                                        </div>
                                    }
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <strong className="label-name">Room No <span className="label-require">*</span></strong>
                                            <Field name="room" type="text" component={TextBox} label="Room no" />
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={10} lgOffset={10} md={2} lg={2}>
                                    <ButtonSubmit error={error} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
AddCustomer = reduxForm({
    form: 'form_register_customer',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.nameKh.length < 2){
            errors.nameKh = 'Please input at least 2 characters !!';
        }
        if(values.nameEn.length < 2){
            errors.nameEn = 'Please input at least 2 characters !!';
        }
        if(values.gender === ""){
            errors.gender = 'Please select gender !!';
        }
        if(values.phone.length < 9){
            errors.phone = 'Please input at least 9 characters !!';
        }
        if(values.identifierTypeId === ""){
            errors.identifierTypeId = 'Please select Identity Type !!';
        }
        if(values.identifier.length < 2){
            errors.identifier = 'Please input at least 2 characters !!';
        }
        if(values.address.length < 2){
            errors.address = 'Please input at least 2 characters !!';
        }

        if(values.usageTypeId === ""){
            errors.usageTypeId = 'Please select Usage Type !!';
        }

        if(values.registerDate == ("" || null)){
            errors.registerDate = 'Please select Register date !!'
        }

        if(values.rateId === ""){
            errors.rateId = 'Please select Rate per Kwh !!';
        }

        if(values.groupInvoiceId === ""){
            errors.groupInvoiceId = 'Please select Group Invoice !!';
        }

        if(values.consumptionId === ""){
            errors.consumptionId = 'Please select Consumption Type !!';
        }
        if(values.customerGroupId === ""){
            errors.customerGroupId = 'Please select Customer Group !!';
        }

        if(values.customerSaleTypeId === ""){
            errors.customerSaleTypeId = 'Please select Customer Type !!';
        }

        if(values.ampereId === ""){
            errors.ampereId = 'Please select Ampere !!';
        }

        if(values.voltageId === ""){
            errors.voltageId = 'Please select Voltage Type !!';
        }

        if(values.boxId === ""){
            errors.boxId = 'Please select Box !!';
        }

        if(!number.test(values.room)){
            errors.room = 'Please input number only !!';
        }

        if (values.province === ("0" || "")) {
            errors.province = "Please select province !!";
        }
        if (values.district === ("0" || "")) {
            errors.district = "Please select district !!";
        }
        if (values.commune === ("0" || "")) {
            errors.commune = "Please select district !!";
        }
        if (values.village === ("0" || "")) {
            errors.village = "Please select district !!";
        }
        return errors
    },
})(AddCustomer);

function mapStateToProps(state) {
    //console.log("RatePerKwh ====> ", state.ratePerKwh.ratePerKwhAll);
    return({
        provinces: state.location.provinces,
        districts: state.location.districts,
        communes: state.location.communes,
        villages: state.location.villages,
        ratePerKwhAll: state.ratePerKwh.ratePerKwhAll,
        consumptionTypeAll: state.consumptionType.consumptionTypeAll,
        groupInvoiceAll: state.groupInvoice.groupInvoiceAll,
        customerGroupAll: state.customerGroup.customerGroupAll,
        customerTypeAll: state.customerType.customerTypeAll,
        voltageTypeAll: state.voltageType.voltageTypeAll,
        ampereAll: state.ampere.ampereAll,
        boxes: state.box.boxes,
        occupationAll: state.occupation.occupationAll,
        poles: state.pole.poles,
        areaAll: state.area.areaAll,
        itemMvGetAll: state.changeMeter.itemMvGetAll,
        bankAll: state.bank.bankAll,
        customerAdd: state.customer.customerAdd,
        customer: state.customer.customer,
        initialValues: {
            nameKh: '',
            nameEn: '',
            gender: '',
            phone: '',
            identifierTypeId: '',
            identifier: '',
            occupation: '',
            country: '1',
            province: '0',
            district: '0',
            commune: '0',
            village: '0',
            address: '',
            licenseId:'',
            invoiceAddress: '',
            usageTypeId: 3,
            rateId: '',
            groupInvoiceId: '',
            consumptionId: '',
            customerGroupId: '',
            customerSaleTypeId: '',
            bankId: '',
            customerPhaseId: '',
            ampereId: '',
            voltageId: '',
            boxId: '',
            room: ''
        }
    })
}

function mapDispatchToProps(dispatch) {

    return bindActionCreators({
        getProvincesAction, getDistrictsAction, getCommunesAction, getVillagesAction,
        getAllRatePerKwhAction,
        getAllConsumptionTypesAction,
        getAllGroupInvoiceAction,
        getAllCustomerGroupsAction,
        getAllCustomerTypesAction,
        getAllVoltageTypesAction,
        getAllAmperesAction,
        getBoxesAction,
        getAllOccupationsAction,
        getAllAreasAction,
        getPolesAction,
        getAllBanksAction,
        addCustomerAction,
        getAllItemsMvAction,
        getCustomerAction}, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(AddCustomer);