import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import Barcode from 'react-barcode';
import { getAllInvoiceDetailsByInvoiceIdAction, getInvoiceInformationByInvoiceIdAction, getHistoryInvoice12monthByCustomerIdAction } from './../../../actions/invoice/customerInvoice';
import { loadCustomer } from '../../../localstorages/localstorage';
import './invoice.css';
import moment from 'moment';

let invoice = [];

let reactiveAmount = 0;
let reactiveList = [];

let history = {
    'customerId' : 0,
    'issueDate' : ''
};
let history12months = [];
class PrintInvoiceEDC extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            settings: JSON.parse(localStorage.getItem('_setting'))
        }
    }

    componentWillMount(){
        // this.props.getInvoiceInformationByInvoiceIdAction(2);
        // this.props.getAllInvoiceDetailsByInvoiceIdAction(2);
        // history.issueDate = '2017-12-01';
        // history.customerId = 1;
        // this.props.getHistoryInvoice12monthByCustomerIdAction(history);

        this.props.getInvoiceInformationByInvoiceIdAction(this.props.params.invoiceId);
        this.props.getAllInvoiceDetailsByInvoiceIdAction(this.props.params.invoiceId);
        history.issueDate = this.props.params.issueDate;
        history.customerId = loadCustomer() !== undefined ? loadCustomer().id : '';
        this.props.getHistoryInvoice12monthByCustomerIdAction(history);
    }

    componentWillReceiveProps(data){
        /* invoice */
        if(data.getInvoiceInformationByInvoiceId.status === 200){
            invoice = data.getInvoiceInformationByInvoiceId.data.items[0];
        }
        if(data.getInvoiceInformationByInvoiceId.status === 401 || data.getInvoiceInformationByInvoiceId.status === 404 || data.getInvoiceInformationByInvoiceId.status === 500){
            invoice = [];
        }

        /* invoice detail by invoice id*/
        if(data.getAllInvoiceDetailsByInvoiceId.status === 200){
            reactiveList = [];
            reactiveAmount = 0;
            let items = data.getAllInvoiceDetailsByInvoiceId.data.items;
            let reactiveCurrency = items[0].currency === "RIEL" ? "R" : "USD";
            let reactiveRate = reactiveCurrency ===  "R"? 100 : 0.025;
            items.forEach((element) => {
                if(element.reactivePower > 0) {
                    reactiveList.push({
                        "meterSerial" : element.meterSerial,
                        "oldUsage": element.lastReactiveRecord,
                        "newUsage": element.newReactiveRecord,
                        "coefficient": element.coefficient,
                        "reactivePower": element.reactivePower,
                        "currency": reactiveCurrency,
                        "rate": reactiveRate,
                        "amount": (element.reactivePower*reactiveRate),
                    });
                    reactiveAmount += (element.reactivePower*reactiveRate);
                }
            });
            setInterval(function(){
                window.print();
            }, 500);
        }
        if(data.getAllInvoiceDetailsByInvoiceId.status === 401 || data.getAllInvoiceDetailsByInvoiceId.status === 404 || data.getAllInvoiceDetailsByInvoiceId.status === 500){
            reactiveList = [];
            reactiveAmount = 0;
        }

        /* history 12 months of customer invoices */
        if(data.getHistoryInvoice12monthByCustomerId.status === 200){
            history12months = [];
            let items = data.getHistoryInvoice12monthByCustomerId.data.items;
            // sum all kw together for each months
            for(let i=0; i<items.length; i++){
                let index = history12months.findIndex(element => element.invoiceId === Number(items[i].invoiceId));
                if(index === -1){
                    // not exist
                    history12months.push({
                        "invoiceId": items[i].invoiceId,
                        "issueDate": items[i].issueDate,
                        "usage" : items[i].activePower
                    });
                }else{
                    // exist
                    history12months[index].usage += items[i].activePower;
                }
            }
            // complete 12 months date more
            history12months.sort((a, b) => {
                return parseFloat(b.issueDate) - parseFloat(a.issueDate); // sort issueDate by desc
            });
            let addLength = 12 - history12months.length;
            let startDate = history12months[0].issueDate;
            for(let j=1;j<= addLength; j++){
                history12months.push({
                    "invoiceId": 0,
                    "issueDate": moment(startDate).subtract(j, 'month'),
                    "usage" : 0
                });
            }
            // format history date
            history12months.forEach((element)=>{
                element.issueDate = moment(element.issueDate).format("MM-YYYY");
            });
        }
        /* history 12 months of customer invoices */
        if(data.getHistoryInvoice12monthByCustomerId.status === 401 || data.getHistoryInvoice12monthByCustomerId.status === 404 || data.getHistoryInvoice12monthByCustomerId.status === 502){
            history12months = [];
            let historyDate = new Date(invoice.issueDate);
            let year = historyDate.getFullYear();
            let month = historyDate.getMonth();
            let newMonth = 12;
            let lastYear = year -1;
            for(let j= 0; j<12; j++){
                if(month < 1) {
                    history12months.push({
                        "invoiceId": 0,
                        "issueDate": newMonth+"-"+lastYear,
                        "usage" : 0
                    });
                    newMonth = newMonth - 1;
                }else{
                    history12months.push({
                        "invoiceId": 0,
                        "issueDate": month+"-"+year,
                        "usage" : 0
                    });
                    month = month -1;
                }
            }
        }
    }
    /* helper function */
    static formatMoney(money){
        if(money === undefined){
            return 0;
        }else{
            let p = money.toFixed(2).split(".");
            return p[0].split("").reverse().reduce(function(acc, money, i, orig) {
                    return  money === "-" ? acc : money + (i && !(i % 3) ? "," : "") + acc;
                }, "") + "." + p[1];
        }
    }

    render(){
        return (
            <div className="container">
                <h1></h1>
                <div className="wrapp-conten">
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 header-invoice">
                            <div className="header-logo">
                                <div className="row">
                                    <div className="col-xs-3 col-sm-3">
                                        <span>
                                            {
                                                this.state.settings.map((set, index)=>{
                                                    if(set.keyword === "LOGO_COMPANY"){
                                                        return (
                                                            <img key={index} className="company-logo" src={`${window.location.origin}/icons/${set.value}`}/>
                                                        )
                                                    }}
                                                )
                                            }
                                        </span>
                                    </div>
                                    <div className="col-xs-7 col-sm-7">
                                        <div>
                                            {
                                                this.state.settings.map((set, index)=>{
                                                    if(set.keyword === "TITLE_COMPANY_KH"){
                                                        return (
                                                            <p key={index} className="company-kh">{set.value}</p>
                                                        )
                                                    }
                                                })
                                            }
                                            {
                                                this.state.settings.map((set, index) => {
                                                    if(set.keyword === "TITLE_COMPANY_EN"){
                                                        return (
                                                            <p key={index} className="company-en">{set.value}</p>
                                                        )
                                                    }
                                                })
                                            }
                                            {
                                                this.state.settings.map((set,index) => {
                                                    if(set.keyword === "COMPANY_ADDRESS") {
                                                        return (
                                                            <p key={index} className="company-address">{set.value}</p>
                                                        )
                                                    }
                                                })
                                            }
                                        </div>
                                    </div>
                                    <div className="col-xs-2 col-sm-2"></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 barcode">
                            <div className="col-xs-4 col-sm-4 barcode">
                                <Barcode value={invoice.barcode} width={5} />
                            </div>
                            <div className="col-xs-8 col-sm-8 invoice-title"><img src="/icons/invoice-bvc.png"/></div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-6 col-sm-6">
                            <div className="table-cus-info">
                                <div className="cutomer-info">
                                    <div className="col-xs-6 col-sm-6"><p>វិក្កយបត្រលេខ:</p></div><div className="col-xs-6 col-sm-6"><p>{invoice.invoiceNo}</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>អត្តលេខ :</p></div><div className="col-xs-6 col-sm-6"><p>{invoice.customerId}</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះអតិថិជន: </p></div><div className="col-xs-6 col-sm-6"><p>{invoice.customerName}</p></div>
                                    <div className="col-xs-6 col-sm-6"><p>អាស័យដ្ធានទទួលវិក្កយបត្រ:</p></div><div className="col-xs-6 col-sm-6 location"><p>{invoice.invoiceAddress}</p></div>
                                </div>
                                <img src="/icons/customer-info-bvc.png"/>
                                <br/>
                            </div>
                        </div>

                        <div className="col-xs-6 col-sm-6 content-text">
                            <div className="col-xs-6 col-sm-6"><p>ទីតាំងប្រើប្រាស់ចរន្ត:</p></div><div className="col-xs-6 col-sm-6"><p>{invoice.transformerSerial}</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញវិក្កយបត្រ:</p></div><div className="col-xs-6 col-sm-6"><p>{moment(invoice.issueDate).format("DD-MM-YYYY")}</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ចាប់ពីថ្ងៃ :</p></div><div className="col-xs-6 col-sm-6"><p>{moment(invoice.startBillingDate).format("DD-MM-YYYY")}</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ដល់ថ្ងៃទី:</p></div><div className="col-xs-6 col-sm-6"><p>{moment(invoice.endBillingDate).format("DD-MM-YYYY")}</p></div>
                        </div>
                    </div>
                    <div className="master-content">
                        <img src="/icons/b-account.jpg"/>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12">
                                <div className="col-xs-7 col-sm-7">
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះគណនីធនាគារ(ACCOUNT NAME)  :</p></div><div className="col-xs-6 col-sm-6"><p><b> {invoice.accountName} </b></p></div>
                                    <div className="col-xs-6 col-sm-6"><p>ឈ្មោះធនាគារ(BANK NAME) :</p></div><div className="col-xs-6 col-sm-6"><p><b>{invoice.bankNameKh}</b></p></div>
                                </div>
                                <div className="col-xs-5 col-sm-5">
                                    <div className="col-xs-6 col-sm-6"><p>លេខគណនីធនាគារ: </p></div><div className="col-xs-6 col-sm-6"><p><b>{invoice.bankNo}</b></p></div>
                                    <div className="col-xs-6 col-sm-6"><p>រូបិយប័ណ្ណ(CURRENCY) :</p></div><div className="col-xs-6 col-sm-6"><p><b>{invoice.bankCurrency === "USD" ? "ដុល្លារ" : "រៀល"}</b></p></div>
                                </div>
                            </div>
                        </div>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12 table-invoice">
                                <table className="table invoice">
                                    <thead className="thead-inverse">
                                    <tr>
                                        <th>
                                            <p>នាឡិកាស្ទង់</p>
                                            <p>METER</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានចាស់</p>
                                            <p>PREVIOUS</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានថ្មី</p>
                                            <p>CURRENT</p>
                                        </th>
                                        <th>
                                            <p>មេគុណ</p>
                                            <p>MULTIPLIER</p>
                                        </th>
                                        <th>
                                            <p>ការប្រើប្រាស់ថាមពល</p>
                                            <p>CONSUMPTION</p>
                                        </th>
                                        <th>
                                            <p>តម្លៃឯកតា</p>
                                            <p>RATE</p>
                                        </th>
                                        <th>
                                            <p>តម្លៃសរុប</p>
                                            <p>VALUE</p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><p>Enalty for active energy</p></td>
                                        <td></td>
                                    </tr>
                                    {
                                        this.props.getAllInvoiceDetailsByInvoiceId.status === 200 ?
                                            this.props.getAllInvoiceDetailsByInvoiceId.data.items.map((invoice,index) => {
                                            return (
                                                <tr key={index}>
                                                    <td><p>{invoice.meterSerial}</p></td>
                                                    <td><p>{invoice.lastUsageRecord}</p></td>
                                                    <td><p>{invoice.newUsageRecord}</p></td>
                                                    <td><p>{invoice.coefficient}</p></td>
                                                    <td><p>{invoice.activePower }</p></td>
                                                    <td><p>{invoice.currency === "RIEL" ? "R "+ invoice.rateRiel : "USD "+ invoice.rateUSD}</p></td>
                                                    <td><p>{invoice.currency === "RIEL" ?
                                                            "R "+ PrintInvoiceEDC.formatMoney(invoice.amountRiel)
                                                            : "USD "+ PrintInvoiceEDC.formatMoney(invoice.amountUSD)}</p>
                                                    </td>
                                                </tr>
                                                )
                                            })
                                            :
                                            null
                                    }
                                    {
                                        reactiveAmount > 0 ?
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><p>Enalty for reactive energy</p></td>
                                                <td></td>
                                            </tr>
                                            :
                                            null
                                    }
                                    {
                                        reactiveAmount > 0 ?
                                            reactiveList.map((invoice, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td><p>{"Re."+invoice.meterSerial}</p></td>
                                                        <td><p>{invoice.oldUsage}</p></td>
                                                        <td><p>{invoice.newUsage}</p></td>
                                                        <td><p>{invoice.coefficient}</p></td>
                                                        <td><p>{invoice.reactivePower <= 0 ? 0 : Math.round(invoice.reactivePower*100)/100}</p></td>
                                                        <td><p>{invoice.currency + " " + invoice.rate}</p></td>
                                                        <td>{ invoice.amount <= 0 ? (invoice.currency + " 0")
                                                            : (invoice.currency +" " +PrintInvoiceEDC.formatMoney(Math.round(invoice.amount))) }
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                            :
                                            null
                                    }
                                    {
                                        invoice.broughtForwardBalanceRiel === 0 || invoice.broughtForwardBalanceUSD === 0 ? null
                                            :
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>From Invoice No: {invoice.broughtBalanceTo}</td>
                                                <td>Balance brought forward</td>
                                                <td>{invoice.currency === "RIEL" ? ("R "+ PrintInvoiceEDC.formatMoney(invoice.broughtForwardBalanceRiel)): ("USD "+PrintInvoiceEDC.formatMoney(invoice.broughtForwardBalanceUSD)) }</td>
                                            </tr>
                                    }
                                    </tbody>
                                </table>
                                <div className="background">
                                    <h1 className="background">វិក្កយបត្រ INVOICE</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 lase-usage">
                                <div className="panel panel-default usage">
                                    <div className="col-xs-12 col-sm-12 history-usage"><p>ប្រវត្តិនៃការប្រើប្រាស់អគ្គិសនីរយះពេល១២ខែកន្លងមក</p></div>
                                    <div className="panel-body invoice">
                                        {
                                            history12months.length > 0 ?
                                                <div>
                                                    <ul>
                                                        <li>{history12months[0].issueDate}</li>
                                                        <li>{history12months[3].issueDate}</li>
                                                        <li>{history12months[6].issueDate}</li>
                                                        <li>{history12months[9].issueDate}</li>
                                                    </ul>
                                                    <ul>
                                                        <li>{history12months[0].usage} គ</li>
                                                        <li>{history12months[3].usage} គ</li>
                                                        <li>{history12months[6].usage} គ</li>
                                                        <li>{history12months[9].usage} គ</li>
                                                    </ul>

                                                    <ul>
                                                        <li>{history12months[1].issueDate}</li>
                                                        <li>{history12months[4].issueDate}</li>
                                                        <li>{history12months[7].issueDate}</li>
                                                        <li>{history12months[10].issueDate}</li>
                                                    </ul>
                                                    <ul>
                                                        <li>{history12months[1].usage} គ</li>
                                                        <li>{history12months[4].usage} គ</li>
                                                        <li>{history12months[7].usage} គ</li>
                                                        <li>{history12months[10].usage} គ</li>
                                                    </ul>

                                                    <ul>
                                                        <li>{history12months[2].issueDate}</li>
                                                        <li>{history12months[5].issueDate}</li>
                                                        <li>{history12months[8].issueDate}</li>
                                                        <li>{history12months[11].issueDate}</li>
                                                    </ul>
                                                    <ul>
                                                        <li>{history12months[2].usage} គ</li>
                                                        <li>{history12months[5].usage} គ</li>
                                                        <li>{history12months[8].usage} គ</li>
                                                        <li>{history12months[11].usage} គ</li>
                                                    </ul>
                                                </div>
                                                :
                                                null
                                        }
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default usage">
                                    <div className="panel-body invoice">
                                        <div className="col-xs-12 col-sm-12 note1"><p>ប្រសិនជាអ្នកប្រើប្រាស់មានចម្ងល់ឬចង់តវ៉ាសូមទំនាក់ទំនងដោយផ្ទាល់ជាមួយភ្នាក់ងារទទួលខុសត្រូវរបស់សាខា។</p></div>
                                        <div className="col-xs-12 col-sm-12 note1"><p>អគ្គិសនី
                                            <span>
                                                {
                                                    this.state.settings.map((set, index)=>{
                                                    if(set.keyword === "TITLE_COMPANY_KH"){
                                                        return (
                                                            <span key={index}>{set.value}</span>
                                                        )
                                                    }
                                                    })
                                                }
                                            </span>
                                            រក្សាសិទ្ធិមិនដោះស្រាយជូនតាមរយៈការបង្ហោះបញ្ហាលើប្រព័ន្ធសារពត៏មានសង្គមជាលក្ខណៈបុគ្គលនោះទេ។</p></div>
                                        <div className="col-xs-12 col-sm-12 note1"><p>អ្នកចុះផ្សាយមិនមានការទទួលខុសត្រូវដែលប៉ះពាល់ដល់កិត្តិយសរបស់អគ្គិសនី
                                           <span>{
                                                this.state.settings.map((set, index)=>{
                                                        if(set.keyword === "TITLE_COMPANY_KH"){
                                                            return (
                                                                <span key={index}>{set.value}</span>
                                                            )
                                                        }
                                                    })}
                                           </span>
                                            អាចប្រឈមមុខជាមួយច្បាប់ដែលពាក់ព័ន្ធជាធរមាន។
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default price">
                                    <p>លុយដែលត្រូវផាកខែមុន(Punished Money)</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default price1">
                                    <p>0</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default money">
                                    <p>ថ្ងៃផុតកំណត់(Expiry Date)</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default money1">
                                    <p>{moment(invoice.dueDate).format("DD-MM-YYYY")}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="panel-body">
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull;​ ហួសពីថ្ងៃផុតកំណត់បង់ប្រាក់ដែលបានកំណត់នេះក្នុងករណីអតិថិជនមិនបានបង់ប្រាក់អគ្គិសនី
                                            <span>
                                                { this.state.settings.map((set, index)=>{
                                                    if(set.keyword === "TITLE_COMPANY_KH"){
                                                        return (
                                                            <span key={index}>{set.value}</span>
                                                        )
                                                    }})
                                                }
                                            </span>
                                            និងផ្អាកការផ្គត់ផ្គង់ថាមពលជូន។</p>
                                        </div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; ការផ្ជាប់ចរន្តជូនវិញលុះត្រាតែអតិថិជនបានបង់ប្រាក់បំណុលសរុបក្នុងវិក្កយបត្រនេះនិងថ្លៃផាកពិន័យ ក្នុងមួយថ្ងៃ ០.០៧ ភាគរយ រួចរាល់ហើយ។</p></div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; សូមអញ្ជើញមកបង់ប្រាក់នៅបេឡាអគ្គិសនីឬគណនីធនាគារបស់ក្រុមហ៊ុន។</p></div>
                                        <div className="col-xs-12 col-sm-12 note"><p>&bull; រាល់ការបង់ប្រាក់តាមរយៈធនាគារអតិធិជនគឺជាអ្នកទទួលខុសត្រូវលើថ្លៃសេវ៉ារបស់ធនាគារ។</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>បំណុលសរុប(Amount)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay">
                                            <p>{invoice.currency === "RIEL" ? ("R "+ PrintInvoiceEDC.formatMoney(invoice.amountRiel)) : ("USD "+PrintInvoiceEDC.formatMoney(invoice.amountUSD))}</p>
                                        </div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>ថ្ងៃបង់ប្រាក់ (Paid Date)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <hr/>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    {/*<Barcode value={invoice.barcode} width={5} />*/}
                                    <div className="panel-body invoice">
                                        <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញវិក្កយបត្រ</p></div><div className="col-xs-6 col-sm-6"><p>: {moment(invoice.issueDate).format("DD-MM-YYYY")}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>វិក្កយបត្រលេខ </p></div><div className="col-xs-6 col-sm-6"><p>: {invoice.invoiceNo}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>ឈ្មោះអតិថិជន</p></div><div className="col-xs-6 col-sm-6"><p>: {invoice.customerName}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>អត្តលេខអតិថិជន</p></div><div className="col-xs-6 col-sm-6 location"><p>: {invoice.customerId}</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed">
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>បំណុលសរុប(Amount)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p>{invoice.currency === "RIEL" ?("R "+ PrintInvoiceEDC.formatMoney(invoice.amountRiel)) : ("USD "+PrintInvoiceEDC.formatMoney(invoice.amountUSD))}</p></div>
                                    </div>
                                    <div className="col-xs-12 col-sm-12 payed">
                                        <div className="col-xs-6 col-sm-6 amount"><p>ថ្ងៃបង់ប្រាក់ (Paid Date)</p></div>
                                        <div className="col-xs-6 col-sm-6 pay"><p></p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    //console.log("DATA : ",state.customerInvoices.getInvoiceInformationByInvoiceId);
    return {
        getInvoiceInformationByInvoiceId: state.customerInvoices.getInvoiceInformationByInvoiceId,
        getAllInvoiceDetailsByInvoiceId: state.customerInvoices.getAllInvoiceDetailsByInvoiceId,
        getHistoryInvoice12monthByCustomerId: state.customerInvoices.getHistoryInvoice12monthByCustomerId
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllInvoiceDetailsByInvoiceIdAction,
        getInvoiceInformationByInvoiceIdAction,
        getHistoryInvoice12monthByCustomerIdAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PrintInvoiceEDC);