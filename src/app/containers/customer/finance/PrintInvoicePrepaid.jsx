import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import Barcode from 'react-barcode';
import { loadCustomer } from '../../../localstorages/localstorage';
import './invoice.css';
import moment from 'moment';
import { updateReturnInvoiceByPrepaidIdAction } from './../../../actions/transaction/prepaid/returnInvoice';

class PrintInvoicePrepaid extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            settings: JSON.parse(localStorage.getItem('_setting')),
            invoices: JSON.parse(localStorage.getItem('_returnInvoice')),
            monthKh: '',
            prepaidHistories: [],
            usageHistories: [],
            isShowMessage: false
        }
    }
    
    componentDidMount() {
        // TODO: convert and return month in khmer language.
        this.handleConvertMonthToKhmer(this.state.invoices.numOfMonth);
        // check condition usage histories
        if(this.state.invoices.usageHistories !== null) {
            let usageHistories = JSON.parse(this.state.invoices.usageHistories);
            if (usageHistories.length === 12) {
                this.setState({ usageHistories: usageHistories });
            } else {
                let remainLength = 12 - usageHistories.length;
                let remainUsage = this._handleReturnNewDateRemaining(usageHistories[0].date, remainLength);
                let newUsageHistories = remainUsage.concat(usageHistories)
                this.setState({ usageHistories: newUsageHistories });
            }
        }
        // all prepaid histries in one month.
        if(this.state.invoices.prepaidHistories !== null) {
            this.setState({ prepaidHistories: JSON.parse(this.state.invoices.prepaidHistories) });
        }
        // ============ event for leave browser
        window.addEventListener('beforeunload', this.keepOnPage);
    }
      
    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.keepOnPage);
    }

    componentWillReceiveProps(newProps){
        if(newProps.updateReturnInvoiceByPrepaidId.status === 200){
            if(newProps.updateReturnInvoiceByPrepaidId.data.status === 200) {
                if (this.state.isShowMessage === true) {
                    alert("Successfully returned invoice prepaid.");
                }
                newProps.updateReturnInvoiceByPrepaidId.data.status = 0;
            } else {
                alert("Fail with return invoice prepaid!");
                newProps.updateReturnInvoiceByPrepaidId.data.status = 0;
            }
            return newProps.updateReturnInvoiceByPrepaidId.status = 0;
        }
        if(newProps.updateReturnInvoiceByPrepaidId.status === 404 || newProps.updateReturnInvoiceByPrepaidId.status === 500){
            alert("Fail with return invoice prepaid!");
            return newProps.updateReturnInvoiceByPrepaidId.status = 0;
        }
    }
      
    keepOnPage(e) {
        return localStorage.removeItem('_returnInvoice');
    }

    _handleReturnNewDateRemaining(monthYear, count) {
        // monthYear => MM-YYYY
        let mm = Number(monthYear.substring(0,2)), 
            yyyy = Number(monthYear.substring(3,monthYear.length)),
            newArray = [];
        for (var i=0; i<count; i++) {
            let newMM = 0, newYYYY = 0;
            if (mm === 1)
                newMM = 12, newYYYY = yyyy - 1;
            else 
                newMM = mm - 1, newYYYY = yyyy;
            
            mm = newMM;
            yyyy = newYYYY;
    
            if (newMM.toString().length < 2) 
                newMM = "0" + newMM;

            let newUsage = {
                date: (newMM + '-' + newYYYY).toString(),
                uasge: 0
            };
            newArray.push(newUsage);
        };
        return newArray.reverse();
    }

    handlePrint() {
        if (window.confirm("If you click Ok, it will show return invoice printing. Please make sure, you finish your Return, it will not show it again after you close it!")) { 
            window.print();
            this.setState({ isShowMessage: false });
            this.props.updateReturnInvoiceByPrepaidIdAction(this.state.invoices.prepaidId);
        }
    }

    handleReturn() {
        if (window.confirm("If you click Ok, it will not show it again after you close it! Please make sure, you finish your Return.")) { 
            this.setState({ isShowMessage: true });
            this.props.updateReturnInvoiceByPrepaidIdAction(this.state.invoices.prepaidId);
        }
    }

    handleConvertMonthToKhmer(monthNumber) {
        var monthKh = '';
        switch (Number(monthNumber)) {
            case 1:
                monthKh = "មករា";
                break;
            case 2:
                monthKh = "កុម្ភៈ";
                break;
            case 3:
                monthKh = "មីនា";
                break;
            case 4:
                monthKh = "មេសា";
                break;
            case 5:
                monthKh = "ឧសភា";
                break;
            case 6:
                monthKh = "មិថុនា";
                break;
            case 7:
                monthKh = "កក្កដា";
                break;
            case 8:
                monthKh = "សីហា";
                break;
            case 9:
                monthKh = "កញ្ញា";
                break;
            case 10:
                monthKh = "តុលា";
                break;
            case 11:
                monthKh = "វិច្ឆិកា";
                break;
            case 12:
                monthKh = "ធ្នូ";
        }
        this.setState({ monthKh: monthKh });
    }

    render(){
        return (
            <div className="container">
                <div className="no-print" style={{marginTop: 10}}>
                    <button className="btn btn-primary" onClick={()=>this.handlePrint()}>Print and Return</button>&nbsp;&nbsp;
                    <button className="btn btn-success" onClick={()=>this.handleReturn()}>Return</button>
                </div>
                <div className="wrapp-conten">
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 header-invoice">
                            <div className="header-logo">
                                <div className="row">
                                    <div className="col-xs-3 col-sm-3">
                                        { this.state.settings !== undefined ? <span>{ this.state.settings.map((set, index)=>{
                                            if(set.keyword === "OFFICE_LOGO"){
                                                return (
                                                    <img key={index} className="company-logo" src={"/icons/"+set.value}/>
                                                )
                                            }
                                        })}</span> : null}
                                    </div>
                                    <div className="col-xs-7 col-sm-7">
                                        {
                                            this.state.settings !== undefined ?
                                                <div>
                                                    {
                                                        this.state.settings.map((set, index)=>{
                                                            if(set.keyword === "OFFICE_TITLE_KH"){
                                                                return (
                                                                    <p key={index} className="company-kh">{set.value}</p>
                                                                )
                                                            }
                                                            if(set.keyword === "OFFICE_TITLE"){
                                                                return (
                                                                    <p key={index} className="company-en">{set.value}</p>
                                                                )
                                                            }
                                                            if(set.keyword === "OFFICE_ADDRESS") {
                                                                return (
                                                                    <p key={index} className="company-address">{set.value}</p>
                                                                )
                                                            }
                                                        })
                                                    }
                                                </div>
                                                :
                                                <div>
                                                    <p className="company-kh">UNKNOWN</p>dsadadas
                                                    <p className="company-en">UNKNOWN</p>
                                                    <p className="company-address">UNKNOWN</p>
                                                </div>
                                        }

                                    </div>
                                    <div className="col-xs-2 col-sm-2"></div>
                                </div>
                                {/* <img src="/icons/logo-bvc-header.png"/>*/}
                            </div>
                        </div>
                        <div className="col-xs-12 col-sm-12 barcode">
                            <div className="col-xs-4 col-sm-4 barcode-pre">
                                &emsp;<Barcode value={this.state.invoices.barcode} width={5} />
                            </div>
                            <div className="col-xs-8 col-sm-8 invoice-title"><h3>វិក្កយបត្រ អគ្គិសនី(PRE-PAID)</h3></div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-6 col-sm-6">
                            <div className="table-cus-info">
                                <div className="cutomer-info">
                                    <div className="col-xs-5 col-sm-5"><p>វិក្កយបត្រលេខ:</p></div><div className="col-xs-7 col-sm-7"><p>{this.state.invoices.invoiceNo}</p></div>
                                    <div className="col-xs-5 col-sm-5"><p>អត្ថលេខ :</p></div><div className="col-xs-7 col-sm-7"><p>{this.state.invoices.customerId}</p></div>
                                    <div className="col-xs-5 col-sm-5"><p>ឈ្មោះអតិថិជន: </p></div><div className="col-xs-7 col-sm-7"><p>{this.state.invoices.customerNameKh}</p></div>
                                    <div className="col-xs-5 col-sm-5"><p>អាស័យដ្ធានទទួលវិក្កយបត្រ:</p></div><div className="col-xs-7 col-sm-7 location"><p>{this.state.invoices.invoiceAddress}</p></div>
                                </div>
                                <img src="/icons/customer-info-bvc.png"/>
                                <br/>
                            </div>
                        </div>
                        <div className="col-xs-6 col-sm-6 content-text" style={{lineHeight: '1.7em'}}>
                            <div className="col-xs-6 col-sm-6"><p>ទីតាំងប្រើប្រាស់ចរន្ត:</p></div><div className="col-xs-6 col-sm-6">
                                <p>{this.state.invoices.transformerSerial !== null ? this.state.invoices.transformerSerial:this.state.invoices.areaNameKh}</p>
                            </div>
                            <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញវិក្កយបត្រ:</p></div><div className="col-xs-6 col-sm-6"><p>{moment(this.state.invoices.issueDate).format('DD-MM-YYYY')}</p></div>
                            <div className="col-xs-3 col-sm-3 col-padding-left-0"><p>ចាប់ពីថ្ងៃ :</p></div><div className="col-xs-3 col-sm-3"><p>{moment(this.state.invoices.startBillingDate).format('DD-MM-YYYY')}</p></div>
                            <div className="col-xs-3 col-sm-3"><p>ដល់ថ្ងៃទី:</p></div><div className="col-xs-3 col-sm-3"><p>{moment(this.state.invoices.endBillingDate).format('DD-MM-YYYY')}</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ថាមពលនៅសល់ដើមខែ({this.state.monthKh}):</p></div><div className="col-xs-6 col-sm-6"><p>{this.state.invoices.oldBalance}</p></div>
                            <div className="col-xs-6 col-sm-6"><p>ថាមពលនៅសល់ចុងខែ({this.state.monthKh}):</p></div><div className="col-xs-6 col-sm-6"><p>{this.state.invoices.newBalance}</p></div>
                        </div>
                    </div>
                    <div className="master-content" style={{height: 380, maxHeight: 380}}>
                        <p className="title-invoice">ការទិញចាមពលសំរាប់ខែ ៖ {this.state.monthKh}</p>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12 table-invoice-pre">
                                <table className="table invoice">
                                    <thead className="thead-inverse">
                                        <tr>
                                            <th>
                                                <p>NO</p>
                                            </th>
                                            <th>
                                                <p>PRE-PAID ID</p>
                                            </th>
                                            <th>
                                                <p>NAME (ENG)</p>
                                            </th>
                                            <th>
                                                <p>PURCHASE DATE</p>
                                            </th>
                                            <th>
                                                <p>PURCHASE ENERGY</p>
                                            </th>
                                            <th>
                                                <p>UNIT</p>
                                            </th>
                                            <th>
                                                <p>AMOUNT</p>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        { this.state.prepaidHistories !== undefined ?
                                            this.state.prepaidHistories.map((i, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <td className="text-align-center"><p>{index + 1}</p></td>
                                                        <td className="text-align-center"><p>{this.state.invoices.prepaidId}</p></td>
                                                        <td className="text-align-center"><p>{this.state.invoices.customerNameEn}</p></td>
                                                        <td className="text-align-center"><p>{moment(i.purchaseDate).format('DD-MM-YYYY')}</p></td>
                                                        <td className="text-align-center"><p>{i.purchaseEnergy}</p></td>
                                                        <td className="text-align-center"><p>{i.price}</p></td>
                                                        <td className="text-align-center"><p>{i.totalAmount}</p></td>
                                                    </tr>
                                                )
                                            })
                                            : null
                                        }
                                        <tr>
                                            <td className="text-align-center bg-yellow" colSpan={7}><p>ចំនួនគីឡូវ៉ាត់សរុបក្នុងខែ({this.state.monthKh}) : {this.state.invoices.totalPurchasePower} KW</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div className="master-content">
                        <p className="title-invoice">ការប្រើប្រាស់លំអិតសំរាប់ខែ ៖​ {this.state.monthKh}</p>
                        <div className="row invioce">
                            <div className="col-xs-12 col-sm-12 table-invoice-sub">
                                <table className="table invoice">
                                    <thead className="thead-inverse">
                                    <tr>
                                        <th>
                                            <p>នាឡិការស្ទង់</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានចាស់</p>
                                        </th>
                                        <th>
                                            <p>លេខអំណានថ្មី</p>
                                        </th>
                                        <th>
                                            <p>មេគុណ</p>
                                        </th>
                                        <th>
                                            <p>ថាមពលប្រើប្រាស់</p>
                                        </th>
                                        <th>
                                            <p>តំលៃឯកតា</p>
                                        </th>
                                        <th>
                                            <p>តំលៃសរុប</p>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td className="text-align-center"><p>{this.state.invoices.meterSerial}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.oldUsage}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.newUsage}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.coefficient}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.unit}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.price}</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.totalUse}</p></td>
                                        </tr>
                                        <tr>
                                            <td className="text-right" colSpan={6}><p>ចំនួនគីឡូវ៉ាត់ប្រើប្រាស់ក្នុងខែ({this.state.monthKh}) :</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.unit}</p></td>
                                        </tr>
                                        <tr>
                                            <td className="text-right" colSpan={6}><p>ទឹកប្រាក់ប្រើប្រាស់ក្នុងខែ({this.state.monthKh}) :</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.totalUse}</p></td>
                                        </tr>
                                        <tr>
                                            <td className="text-right" colSpan={6}><p>ទឹកប្រាក់អនុគ្រោះ :</p></td>
                                            <td className="text-align-center"><p>{this.state.invoices.returnAmount}</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <br/>

                            </div>
                        </div>
                    </div>

                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 lase-usage">
                                <div className="panel panel-default usage-pre">
                                    <div className="col-xs-12 col-sm-12 history-usage">
                                        <p>ប្រវត្តិនៃការប្រើប្រាស់អគ្គិសនីរយៈពេល១២ខែកន្លងមក</p>
                                    </div>
                                    <div className="panel-body invoice-pre">
                                        <div>
                                            <ul>
                                                <li>{this.state.usageHistories[11]!==undefined?this.state.usageHistories[11].date:null}</li>
                                                <li>{this.state.usageHistories[8]!==undefined?this.state.usageHistories[8].date:null}</li>
                                                <li>{this.state.usageHistories[5]!==undefined?this.state.usageHistories[5].date:null}</li>
                                                <li>{this.state.usageHistories[2]!==undefined?this.state.usageHistories[2].date:null}</li>
                                            </ul>
                                            <ul>
                                                <li>{this.state.usageHistories[11]!==undefined?this.state.usageHistories[11].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[8]!==undefined?this.state.usageHistories[8].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[5]!==undefined?this.state.usageHistories[5].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[2]!==undefined?this.state.usageHistories[2].uasge:null} គ</li>
                                            </ul>
                                            <ul>
                                                <li>{this.state.usageHistories[10]!==undefined?this.state.usageHistories[10].date:null}</li>
                                                <li>{this.state.usageHistories[7]!==undefined?this.state.usageHistories[7].date:null}</li>
                                                <li>{this.state.usageHistories[4]!==undefined?this.state.usageHistories[4].date:null}</li>
                                                <li>{this.state.usageHistories[1]!==undefined?this.state.usageHistories[1].date:null}</li>
                                            </ul>
                                            <ul>
                                                <li>{this.state.usageHistories[10]!==undefined?this.state.usageHistories[10].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[7]!==undefined?this.state.usageHistories[7].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[4]!==undefined?this.state.usageHistories[4].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[1]!==undefined?this.state.usageHistories[1].uasge:null} គ</li>
                                            </ul>
                                            <ul>
                                                <li>{this.state.usageHistories[9]!==undefined?this.state.usageHistories[9].date:null}</li>
                                                <li>{this.state.usageHistories[6]!==undefined?this.state.usageHistories[6].date:null}</li>
                                                <li>{this.state.usageHistories[3]!==undefined?this.state.usageHistories[3].date:null}</li>
                                                <li>{this.state.usageHistories[0]!==undefined?this.state.usageHistories[0].date:null}</li>
                                            </ul>
                                            <ul>
                                                <li>{this.state.usageHistories[9]!==undefined?this.state.usageHistories[9].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[6]!==undefined?this.state.usageHistories[6].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[3]!==undefined?this.state.usageHistories[3].uasge:null} គ</li>
                                                <li>{this.state.usageHistories[0]!==undefined?this.state.usageHistories[0].uasge:null} គ</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default usage-pre">
                                    <div className="panel-body invoice">
                                        <div className="col-xs-12 col-sm-12 note1">
                                            <p>ប្រសិនជាអ្នកប្រើប្រាស់មានចម្ងល់ឬចង់តវ៉ាសូមទំនាក់ទំនងដោយផ្ទាល់ជាមួយភ្នាក់ងារទទួលខុសត្រូវរបស់សាខា។</p>
                                        </div>
                                        <div className="col-xs-12 col-sm-12 note1"><p>អគ្គិសនី
                                            { this.state.settings !== undefined ? <span>{ this.state.settings.map((set, index)=>{
                                                    if(set.keyword === "OFFICE_TITLE_KH"){
                                                        return (
                                                            <span key={index}>{set.value}</span>
                                                        )
                                                    }
                                                    })}</span>
                                                : null}រក្សាសិទ្ធិមិនដោះស្រាយជូនតាមរយៈការបង្ហោះបញ្ហាលើប្រព័ន្ធសារពត៏មានសង្គមជាលក្ខណៈបុគ្គលនោះទេ។</p></div>
                                        <div className="col-xs-12 col-sm-12 note1">
                                            <p>អ្នកចុះផ្សាយមិនមានការទទួលខុសត្រូវដែលប៉ះពាល់ដល់កិត្តិយសរបស់អគ្គិសនី
                                            { this.state.settings !== undefined ? <span>{ this.state.settings.map((set, index)=>{
                                                if(set.keyword === "OFFICE_TITLE_KH"){
                                                    return (
                                                        <span key={index}>{set.value}</span>
                                                    )
                                                }
                                            })}</span>
                                                : null}អាចប្រឈមមុខជាមួយច្បាប់ដែលពាក់ព័ន្ធជាធរមាន។
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default price">
                                    <p>ទឹកប្រាក់អនុគ្រោះខែមុន</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default price1">
                                    <p>{this.state.invoices.oldReturnAmount}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="co-xs-12 col-sm-12 money">
                            <div className="col-xs-9 col-sm-9 price">
                                <div className="panel panel-default money">
                                    <p>ថ្ងៃផុតកំណត់(Expiry Date)</p>
                                </div>
                            </div>
                            <div className="col-xs-3 col-sm-3 price">
                                <div className="panel panel-default money1">
                                    <p>{moment(this.state.invoices.expiredDate).format('DD-MM-YYYY')}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed-top">
                                    <div className="panel-body">
                                        <div className="col-xs-12 col-sm-12 note-pre"><p>&bull; ​សូមមេត្តាយកកាតទៅបញ្ចូល ក្នុងឧបករណ៍បញ្ចូលភ្លើង(Panel) នៅរៀងរាល់ចុងខែ (ពីថ្អៃទី ២៤ ដល់ថ្ងៃទី ២៨)</p></div>
                                        <div className="col-xs-12 col-sm-12 note-pre"><p>&bull; សូមមេត្តាយកកាតមក ការិយាល័យលក់អគ្គិសនីយើងខ្ញុំ ដើម្បីទទួលបានប្រាក់អនុគ្រោះ ក្នុងលក្ខណ្ឌណាមួយនៅរៀងរាល់ដើមខែ(ពីថ្ងៃទី ១ ដល់ ថ្ងៃទី ៧)</p></div>
                                        <div className="col-xs-12 col-sm-12 note-pre"><p>&bull; បើមិនដូច្នោះទេ ក្រុមហុនយើងនឹងគណនាទៅតាមរូបមន្តរបស់អាជ្ញាធរអគ្គិសនី</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <table className="table detail-invoice-top">
                                    <tbody>
                                        <tr>
                                            <td className="total-used"><p>ទឹកប្រាក់ប្រើប្រាស់ខែ({this.state.monthKh})</p></td>
                                            <td><p>{this.state.invoices.totalUse} R</p></td>
                                        </tr>
                                        <tr>
                                            <td className="total-used"><p>ទឹកប្រាក់អនុគ្រោះខែ({this.state.monthKh})</p></td>
                                            <td><p>{this.state.invoices.returnAmount} R</p></td>
                                        </tr>
                                        <tr>
                                            <td className="total"><p>ថ្ងៃទូទាត់ប្រាក់</p></td>
                                            <td><p></p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div className="row invioce">
                        <hr/>
                    </div>
                    <div className="row invioce">
                        <div className="col-xs-12 col-sm-12 last-usage">
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <div className="panel panel-default payed-pre">
                                    &emsp;<Barcode value={this.state.invoices.barcode} width={5} />
                                    <div className="panel-body invoice">
                                        <div className="col-xs-6 col-sm-6"><p>ថ្ងៃចេញវិក្កយបត្រ</p></div><div className="col-xs-6 col-sm-6"><p>: {moment(this.state.invoices.issueDate).format('DD-MM-YYYY')}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>វិក្កយបត្រលេខ </p></div><div className="col-xs-6 col-sm-6"><p>: {this.state.invoices.invoiceNo}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>ឈ្មោះអតិថិជន</p></div><div className="col-xs-6 col-sm-6"><p>: {this.state.invoices.customerNameKh}</p></div>
                                        <div className="col-xs-6 col-sm-6"><p>អត្ថលេខអតិថិជន</p></div><div className="col-xs-6 col-sm-6 location"><p>: {this.state.invoices.customerId}</p></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xs-6 col-sm-6 last-usage">
                                <table className="table detail-invoice">
                                    <tbody>
                                        <tr>
                                            <td className="total-used"><p>ទឹកប្រាក់ប្រើប្រាស់ខែ({this.state.monthKh})</p></td>
                                            <td><p>{this.state.invoices.totalUse} R</p></td>
                                        </tr>
                                        <tr>
                                            <td className="total-used"><p>ទឹកប្រាក់អនុគ្រោះខែ({this.state.monthKh})</p></td>
                                            <td><p>{this.state.invoices.returnAmount} R</p></td>
                                        </tr>
                                        <tr>
                                            <td className="total"><p>ថ្ងៃទូទាត់ប្រាក់</p></td>
                                            <td><p></p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        updateReturnInvoiceByPrepaidId: state.returnInvoices.updateReturnInvoiceByPrepaidId
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        updateReturnInvoiceByPrepaidIdAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PrintInvoicePrepaid);