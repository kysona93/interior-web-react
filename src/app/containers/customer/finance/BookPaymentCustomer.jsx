import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, FormGroup, FormControl } from 'react-bootstrap';
import { Field, reduxForm, initialize, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import './../../index.css';
import { getAllCashBoxesByUserIdAction } from '../../../actions/accounting/cashBox';
import { addCustomerBookPaymentAction } from '../../../actions/invoice/bookPayment';
//import { getAllLocationPayAction } from './../../../actions/customer/finance/locationPay';
import { getAllPaymentTypesAction } from './../../../actions/customer/finance/paymentType';
import { getAllCustomerInvoiceNoAction } from './../../../actions/invoice/customerInvoice';
import { loadCustomer } from '../../../localstorages/localstorage';

let cashBoxId = 0;
let customerId = 0;
let userAccounts = [];
let allPayments = [];
class BookPaymentCustomer extends React.Component {
    constructor(props){
        super(props);
        this.state={
            invoiceNo: [],
            //locationPay: [],
            paymentType: [],
            startDate: null,
            paidRiel: true,
            paidUSD: true
        };
        this.handleRegisterDate = this.handleRegisterDate.bind(this);
        this.handleSelected = this.handleSelected.bind(this);
    }

    handleRegisterDate(date) {
        this.setState({
            startDate: date
        });
    }

    componentWillMount(){
        this.props.getAllCashBoxesByUserIdAction();
        // this.props.getAllLocationPayAction();
        this.props.getAllPaymentTypesAction();
        this.props.getAllCustomerInvoiceNoAction(loadCustomer().id);
    }

    componentWillReceiveProps(data){
        /* user accounts */
        if(data.getAllCashBoxesByUserId.status === 200){
            userAccounts = data.getAllCashBoxesByUserId.data.items;
        }
        // // location pay
        // if(data.getAllLocationPays.status === 200){
        //     let locations = data.getAllLocationPays.data.items;
        //     let arrLocations = [];
        //     locations.forEach((element) => {
        //         arrLocations.push({
        //             "id": element.id,
        //             "name": element.location
        //         });
        //     });
        //     this.setState({locationPay : arrLocations});
        // }

        // payment type
        if(data.getAllPaymentTypes.status === 200){
            let paymentTypes = data.getAllPaymentTypes.data.items;
            let arrPaymentTypes = [];
            paymentTypes.forEach((element) => {
                arrPaymentTypes.push({
                    "id": element.id,
                    "name": element.paymentType
                });
            });
            this.setState({paymentType : arrPaymentTypes});
        }
        // invoice no
        if(data.getAllCustomerInvoiceNo.status === 200){
            let payments = data.getAllCustomerInvoiceNo.data.items;
            let arrInvoiceNo = [];
            allPayments = data.getAllCustomerInvoiceNo.data.items;
            payments.forEach((element) => {
                arrInvoiceNo.push({
                    "id": element.id,
                    "name": element.invoiceNo
                });
            });
            this.setState({invoiceNo : arrInvoiceNo});
        }

        if(data.addCustomerBookPayment.status === 200){
            if(confirm("Successfully book payment.") === true){
                data.addCustomerBookPayment.status = 0;
                this.props.getAllCustomerInvoiceNoAction(1/*loadCustomer().id*/);
                this.setState({startDate: null});
                this.props.dispatch(initialize('form_book_payment', null));
            }else{
                data.addCustomerBookPayment.status = 0;
            }
        }
        if(data.addCustomerBookPayment.status === 404 || data.addCustomerBookPayment.status === 500){
            alert("Fail with book payment!");
            this.props.getAllCustomerInvoiceNoAction(loadCustomer().id);
        }
    }

    /* select cash balance */
    selectGLAccount(event){
        if(event.target.value === ""){
            cashBoxId = 0;
            this.setState({accountName: 'Unknown'});
        }else{
            for(let i=0; i< userAccounts.length; i++){
                if(userAccounts[i].id === Number(event.target.value)){
                    cashBoxId = userAccounts[i].id;
                    this.setState({accountName: userAccounts[i].name}); break;
                }
            }
        }
    }

    handleSelected(event){
        let payment = {
            paidAmountRiel: '',
            paidAmountUSD: ''
        };
        if(allPayments.length > 0){
            allPayments.forEach((element) => {
                if(element.id === Number(event.target.value)) {
                    payment.paidAmountRiel = element.unpaidAmountRiel;
                    payment.paidAmountUSD = element.unpaidAmountUSD;
                    customerId = element.customerId;
                }
            });
        }
        if(payment.paidAmountRiel <= 0) this.setState({paidRiel: false});
        else this.setState({paidRiel: true});
        if(payment.paidAmountUSD <= 0) this.setState({paidUSD: false});
        else this.setState({paidUSD: true});
        this.props.dispatch(initialize('form_book_payment', payment));
    }

    handleSubmit(values){
        if(cashBoxId !== 0){
            let bookPayment = {
                "cashBoxId" : cashBoxId,
                "customerId" : customerId,
                "invoiceId": Number(values.invoiceId),
                "paidAmountRiel": Number(values.paidAmountRiel),
                "paidAmountUSD": Number(values.paidAmountUSD),
                "paidDate": values.paidDate,
                //"paymentLocationId": 0,
                "paymentTypeId": Number(values.paymentTypeId),
                "description": values.description,
                "status": 1
            };
            // if(values.paymentLocationId === undefined || values.paymentLocationId === null){
            //     bookPayment.paymentLocationId = 0;
            // }else{
            //     bookPayment.paymentLocationId = Number(values.paymentLocationId);
            // }
            this.props.addCustomerBookPaymentAction(bookPayment);
        }else {
            alert("Please select Account Number!");
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div>
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <Row>
                            <Col md={3} lg={3} className="label-name">
                                <strong>Account Number <span className="label-require">*</span></strong>
                            </Col>
                            <Col md={5} lg={5}>
                                <FormGroup controlId="formControlsSelect">
                                    <FormControl componentClass="select" onChange={this.selectGLAccount.bind(this)}>
                                        <option value="">Please select account number</option>
                                        {
                                            this.props.getAllCashBoxesByUserId.status === 200 ?
                                                this.props.getAllCashBoxesByUserId.data.items.map((account,index) => {
                                                    return (
                                                        <option key={index} value={account.id}
                                                        >{account.name}</option>
                                                    )
                                                })
                                                :null
                                        }
                                    </FormControl>
                                </FormGroup>
                            </Col>
                            <Col md={4} lg={4} className="label-name"><b>is {this.state.accountName}</b></Col>
                        </Row>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Book Payment Customer ID: {loadCustomer().id}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Select Invoice No <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="invoiceId" type="select" component={SelectBox} placeholder="Please Select Invoice No"
                                                   onChange={this.handleSelected}
                                                   values={this.state.invoiceNo}
                                                   sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Payment Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidDate" component={DateTimePicker} placeholder="Register Date"
                                                   defaultDate={this.state.startDate} handleChange={this.handleRegisterDate}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Amount Riel</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidAmountRiel" type="text" component={TextBox} label="Amount RIEL" icon=""
                                                   disabled = {this.state.paidRiel ? false : true}
                                            />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Amount USD</strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paidAmountUSD" type="text" component={TextBox} label="Amount USD" icon=""
                                                   disabled = {this.state.paidUSD ? false : true}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Payment Type <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paymentTypeId" type="select" component={SelectBox} placeholder="Please Select Payment Type"
                                                   values={this.state.paymentType} sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    {/*<Row>*/}
                                        {/*<Col md={4} lg={4} className="label-name">*/}
                                            {/*<strong>Location</strong>*/}
                                        {/*</Col>*/}
                                        {/*<Col md={8} lg={8}>*/}
                                            {/*<Field name="paymentLocationId" type="select" component={SelectBox} placeholder="Please Select Location"*/}
                                                   {/*values={this.state.locationPay} sortBy="name" icon=""/>*/}
                                        {/*</Col>*/}
                                    {/*</Row>*/}
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={4} sm={4} md={4} lg={4}></Col>
                </Row>
            </div>
        )
    }
}

BookPaymentCustomer = reduxForm({
    form: 'form_book_payment',
    validate: (values) => {
        let regex_price = /^(([1-9]\d*)|0)(\.\d{1,2})?$/;

        const errors = {};
        if(!regex_price.test(values.paidAmountRiel) || values.paidAmountRiel === undefined){
            errors.paidAmountRiel = "Invalid amount as RIEL.";
        }
        if(!regex_price.test(values.paidAmountUSD) || values.paidAmountUSD === undefined){
            errors.paidAmountUSD = "Invalid amount as USD.";
        }
        if(values.invoiceId === undefined){
            errors.invoiceId = "Please select invoice no.";
        }
        if(values.paidDate === undefined){
            errors.paidDate = "Please select payment date.";
        }
        if(values.paymentTypeId === undefined){
            errors.paymentTypeId = "Please select payment type.";
        }
        // if(values.paymentLocationId === undefined){
        //     errors.paymentLocationId = "Please select location.";
        // }
        return errors
    }
})(BookPaymentCustomer);

function mapStateToProps(state) {
    //console.log("DATA : ", state.customerInvoices.getAllCustomerInvoiceNo);
    return {
        getAllCashBoxesByUserId: state.cashBoxes.getAllCashBoxesByUserId,
        //getAllLocationPays: state.locationPays.getAllLocationPays,
        getAllPaymentTypes: state.paymentTypes.getAllPaymentTypes,
        getAllCustomerInvoiceNo: state.customerInvoices.getAllCustomerInvoiceNo,
        addCustomerBookPayment: state.customerBookPayment.addCustomerBookPayment
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getAllCashBoxesByUserIdAction,
        getAllPaymentTypesAction,
        //getAllLocationPayAction,
        addCustomerBookPaymentAction,
        getAllCustomerInvoiceNoAction},
        dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(BookPaymentCustomer);