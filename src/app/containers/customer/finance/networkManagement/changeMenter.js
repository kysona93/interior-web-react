import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../../components/forms/TextBox';
import { TextArea } from '../../../../components/forms/TextArea';
import SelectBox from '../../../../components/forms/SelectBox';
import { DateTimePicker } from '../../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../../components/forms/ButtonSubmit';
import './../../../index.css';
class ChangeMeter extends React.Component{
    constructor(props){
        super(props);
        this.state={
            meter: [{id: 1, name: "0001"},{id: 2, name: "0002"},{id: 3, name: "0003"},{id: 4, name: "0004"}]
        };
    }
    componentWillMount(){

    }
    componentWillReceiveProps(){

    }
    handleSubmit(values){

    }
    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container">
                <div className="panel panel-default">
                    <div className="panel-heading">
                        <h3 className="panel-title">Change Meter</h3>
                    </div>
                    <div className="panel-body">
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col lg={5}>
                                    <div className="change-to">
                                        <center><p>From Meter</p></center>
                                    </div>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Serial (Old) <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="gender" type="select" component={SelectBox} placeholder="Please Select Invoice No"
                                                   values={this.state.meter} sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Ampere: <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Meter Type: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="activePart" type="text" component={TextBox} label="Weight active part" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Last Usage: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="transformerWeight" type="text" component={TextBox} label="Transformer weight" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Branch: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="dimension" type="text" component={TextBox} label="Transformer dimension" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                </Col>
                                <Col lg={2}><img className="change-to" src="/icons/arrow.png"/> </Col>
                                <Col lg={5}>
                                    <div className="to-meter">
                                        <center><p>To Meter</p></center>
                                    </div>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Meter Serial (New): </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="manufacturer" type="text" component={TextBox} label="Manufacturer" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Ampere: <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="serial" type="text" component={TextBox} label="Serial number" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong> Meter Type: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="activePart" type="text" component={TextBox} label="Weight active part" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Last Usage: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="transformerWeight" type="text" component={TextBox} label="Transformer weight" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Branch: </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="dimension" type="text" component={TextBox} label="Transformer dimension" icon="fa fa-keyboard-o"/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={9} lgOffset={9} md={3} lg={3}>
                                    <ButtonSubmit error={error} submitting={submitting} label="រក្សាទុក/Save" />
                                </Col>
                            </Row>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
ChangeMeter = reduxForm({
    form: 'form_change_meter',
    validate: (values) => {
        const errors = {};


        return errors
    }
})(ChangeMeter);
function mapStateToProps(state) {
    return {}
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ChangeMeter);