import React from '../../../../../node_modules/react';
import ListCustomerInvoice from  './ListCustomerInvoice';
import BookPaymentCustomer from './BookPaymentCustomer';
import AddCharge from './charge/AddCharge';
import './../network/index.css';

class LayoutFinance extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            invoice: true,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        };
        this.handleInvoice = this.handleInvoice.bind(this);
        this.handleInvoiceEDC = this.handleInvoiceEDC.bind(this);
        this.handleList = this.handleList.bind(this);
        this.handleBPayment = this.handleBPayment.bind(this);
        this.handleBDescription = this.handleBDescription.bind(this);
        this.handleTransaction = this.handleTransaction.bind(this);
        this.handleCharge = this.handleCharge.bind(this);
        this.handleLock = this.handleLock.bind(this);
        this.handlePrepaid = this.handlePrepaid.bind(this);
    }

    handleInvoice(){
        this.setState({
            invoice: true,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleInvoiceEDC(){
        this.setState({
            invoice: false,
            invoiceEDC: true,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleList(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: true,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleBPayment(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: true,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleBDescription(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: true,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleTransaction(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: true,
            charge: false,
            lock: false,
            prepaid: false
        });
    }
    handleCharge(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: true,
            lock: false,
            prepaid: false
        });
    }
    handleLock(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: true,
            prepaid: false
        });
    }
    handlePrepaid(){
        this.setState({
            invoice: false,
            invoiceEDC: false,
            list: false,
            bPayment: false,
            bDescription: false,
            transaction: false,
            charge: false,
            lock: false,
            prepaid: true
        });
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="col-md-12">
                    <div className="panel with-nav-tabs panel-default installing">
                        <div className="panel-heading installing">
                            <ul className="nav nav-tabs installing">
                                <li className="active"><a href="#tabinvoice" onClick={this.handleInvoice} data-toggle="tab">Invoices</a></li>
                                {/*
                                 <li ><a href="#tabinvoiceedc" onClick={this.handleInvoiceEDC} data-toggle="tab">Invoices EDC</a></li>
                                 <li ><a href="#tabdepreciation" onClick={this.handleList} data-toggle="tab">Depreciation List</a></li>
                                */}
                                <li className="dropdown">
                                    <a href="#" data-toggle="dropdown">Payment<span className="caret"> </span></a>
                                    <ul className="dropdown-menu" role="menu">
                                        <li><a href="#tabbookpayment" onClick={this.handleBPayment} data-toggle="tab">Book Payment</a></li>
                                        {/*<li><a href="#tabbookdepreciation" onClick={this.handleBDescription} data-toggle="tab">Book Depreciation</a></li>*/}
                                    </ul>
                                </li>
                                {/*<li><a href="#tabfinancial" onClick={this.handleTransaction} data-toggle="tab">Financial Transaction</a></li>*/}
                                <li><a href="#tabaddcharge" onClick={this.handleCharge} data-toggle="tab">Add Charge</a></li>
                                {/*<li className="dropdown">
                                    <a href="#" data-toggle="dropdown">Action Menu<span className="caret"> </span></a>
                                    <ul className="dropdown-menu" role="menu">
                                        <li><a href="#tablock" onClick={this.handleLock} data-toggle="tab">Lock</a></li>
                                        <li><a href="#tabchangetoprepaid" onClick={this.handlePrepaid} data-toggle="tab">Change to Pre-paid</a></li>
                                    </ul>
                                </li>
                                <li><a href="#tabrefresh" data-toggle="tab">Refresh</a></li>*/}
                            </ul>
                        </div>
                        <div className="panel-body">
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="tabinvoice">
                                    { this.state.invoice ?
                                        <ListCustomerInvoice customer={this.props.customer !== undefined ? this.props.customer : undefined} />
                                        : null
                                    }
                                </div>
                                {/*
                                 <div className="tab-pane fade" id="tabinvoiceedc">
                                 { this.state.invoiceEDC ?
                                 <ListEDCInvoice customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                 : null
                                 }
                                 </div>
                                 <div className="tab-pane fade" id="tabdepreciation">
                                 { this.state.list ?
                                 <ListDepreciation customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                 : null
                                 }
                                 </div>
                                */}
                                <div className="tab-pane fade" id="tabbookpayment">
                                    { this.state.bPayment ?
                                        <BookPaymentCustomer customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                        : null
                                    }
                                </div>
                                {/*
                                 <div className="tab-pane fade" id="tabbookdepreciation">
                                 { this.state.bDescription ?
                                 <BookDepreciation customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                 : null
                                 }
                                 </div>
                                 <div className="tab-pane fade" id="tabfinancial">
                                 { this.state.transaction ?
                                 <ListFinancialTransaction customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                 : null
                                 }
                                 </div>
                                */}
                                <div className="tab-pane fade" id="tabaddcharge">
                                    { this.state.charge ?
                                        <AddCharge customer={this.props.customer !== undefined ? this.props.customer : undefined}/>
                                        : null
                                    }
                                </div>
                                <div className="tab-pane fade" id="tablock">

                                </div>
                                <div className="tab-pane fade" id="tabchangetoprepaid">

                                </div>
                                <div className="tab-pane fade" id="tabrefresh">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default LayoutFinance;