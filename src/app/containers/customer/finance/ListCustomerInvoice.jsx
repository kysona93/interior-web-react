import React from '../../../../../node_modules/react';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import { Row, Col, Table } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import { getAllCustomerInvoicesAction, voidCustomerInvoiceAction } from './../../../actions/invoice/customerInvoice';
import './invoice.css';

let customerId = 0;
class ListCustomerInvoice extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            activePage: 1
        };
        this.voidInvoice = this.voidInvoice.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentWillMount(){
        customerId = this.props.customer !== undefined ? this.props.customer.id : 0;
        this.props.getAllCustomerInvoicesAction(customerId);
    }

    componentWillReceiveProps(data){
        if(data.voidCustomerInvoice.status === 200){
            alert("Successfully void this invoice.");
            customerId = this.props.customer !== undefined ? this.props.customer.id : 0;
            this.props.getAllCustomerInvoicesAction(customerId);
            data.voidCustomerInvoice.status = 0;
        }
        if(data.voidCustomerInvoice.status === 400 || data.voidCustomerInvoice.status === 500){
            alert("Fail with void this invoice!");
        }
    }

    static handlePrint(invoiceId, issueDate, typeInvoice){
        if(typeInvoice === "ITEM"){
            window.open("/print-add-charge-invoice/"+invoiceId);
        }else{
            window.open("/print-edc-invoice/"+invoiceId+"/"+issueDate);
        }
    }

    handleSelect(eventKey) {
        this.setState({
            activePage: eventKey
        });
        criteria.page = eventKey;
        //this.props.listAllBillingItemsAction(billing);
        this.props.getAllCustomerInvoicesAction(criteria);
    }

    static handleItem(total) {
        if (total <= 10) {
            return 1
        } else if (total % 10 === 0) {
            return total / 10
        } else if (total % 10 > 0) {
            return parseInt(total/10) + 1
        }
    }

    voidInvoice(id){
        if(confirm("Are you sure want to void this invoice?") === true){
            this.props.voidCustomerInvoiceAction(id);
        }
    }

    render(){
        return(
            <div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <div>
                            <Table responsive condensed bordered hover className="search-result">
                                <thead>
                                <tr style={{backgroundColor: '#d9e5f7'}}>
                                    <th>Invoice No</th>
                                    <th>Issue Date</th>
                                    <th>Start Billing Date</th>
                                    <th>End Billing Date</th>
                                    <th>Amount(RIEL)</th>
                                    <th>Amount(USD)</th>
                                    <th>Invoice Amount(RIEL)</th>
                                    <th>Invoice Amount(USD)</th>
                                    <th>Brought Forward Balance(RIEL)</th>
                                    <th>Brought Forward Balance(USD)</th>
                                    <th>Unpaid Amount(RIEL)</th>
                                    <th>Unpaid Amount(USD)</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.getAllCustomerInvoices.status === 200 ?
                                        <tbody>
                                        { this.props.getAllCustomerInvoices.data.items.map((invoice,index)=>{
                                                return(
                                                    <tr key={index} >
                                                        <td><Link onClick={() => ListCustomerInvoice.handlePrint(invoice.invoiceId, moment(invoice.issueDate).format("YYYY-MM-DD") ,invoice.typeInvoice)}>{invoice.invoiceNo}</Link></td>
                                                        <td>{moment(invoice.issueDate).format("YYYY-MM-DD")}</td>
                                                        <td>{moment(invoice.startBillingDate).format("YYYY-MM-DD")}</td>
                                                        <td>{moment(invoice.endBillingDate).format("YYYY-MM-DD")}</td>
                                                        <td>{invoice.amountRiel}</td>
                                                        <td>{invoice.amountUSD}</td>
                                                        <td>{invoice.invoiceAmountRiel}</td>
                                                        <td>{invoice.invoiceAmountUSD}</td>
                                                        <td>{invoice.broughtForwardBalanceRiel}</td>
                                                        <td>{invoice.broughtForwardBalanceUSD}</td>
                                                        <td>{invoice.unpaidAmountRiel}</td>
                                                        <td>{invoice.unpaidAmountUSD}</td>
                                                        <td className="text-center">
                                                            {
                                                                invoice.isVoid === 0 ?
                                                                    <a onClick={()=> this.voidInvoice(invoice.invoiceId)} href="#" className="btn btn-warning btn-xs">
                                                                        <span>Void</span>
                                                                    </a>
                                                                    :
                                                                    <h5 style={{color: 'red', fontWeight: 'bold'}}>Broken</h5>

                                                            }
                                                        </td>
                                                    </tr>
                                                )
                                        }
                                        )}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan = {13}><h5 style={{textAlign: 'center'}}>RESULT NOT FOUND </h5></td>
                                        </tr>
                                        </tbody>
                                }
                            </Table>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}

function mapStateToProps(state) {
    //console.log("DATA : ",state.customerInvoices.getAllCustomerInvoices);
    return {
        voidCustomerInvoice: state.customerInvoices.voidCustomerInvoice,
        getAllCustomerInvoices: state.customerInvoices.getAllCustomerInvoices
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllCustomerInvoicesAction,voidCustomerInvoiceAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListCustomerInvoice);