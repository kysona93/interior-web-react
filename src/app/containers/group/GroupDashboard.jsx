import React from 'react';
import { Link, browserHistory } from 'react-router';
import { Tabs, Tab } from 'react-bootstrap';

export default class GroupDashboard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            key: 1
        }
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentWillMount() {
        let currentUrl = window.location.pathname;
        if(currentUrl == "/app/security/groups/inactive") {
            this.setState({ key: 2 })
        } else if(currentUrl == "/app/security/groups/add") {
            this.setState({ key: 3 })
        }
    }

    handleSelect(key) {
        this.setState({key});
        if (key == 1) {
            browserHistory.push('/app/security/groups');
        } else if (key == 2) {
            browserHistory.push('/app/security/groups/inactive');
        } else if (key == 3) {
            browserHistory.push('/app/security/groups/add');
        }
    }
    componentWillReceiveProps(newProps) {
        let currentUrl = window.location.pathname;
        if(currentUrl == "/app/security/groups") {
            this.setState({ key: 1 })
        }
    }

    render(){
        return(
            <Tabs activeKey={this.state.key} onSelect={this.handleSelect} animation={false} id="controlled-tab-example" className="margin_left_25">
                <Tab eventKey={1} title="Active Groups" />
                <Tab eventKey={2} title="Inactive Groups" />
                <Tab eventKey={3} title="Add New Group" />
                { this.props.children }
            </Tabs>
        )
    }

}