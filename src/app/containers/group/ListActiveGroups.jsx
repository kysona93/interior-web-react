import React from 'react';
import { Link } from 'react-router';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { bindActionCreators } from 'redux';
import './../../components/forms/Styles.css';
import { getGroupsStatusAction } from '../../actions/authentication';
import { canUpdate } from './../../../auth';

class ListActiveGroups extends React.Component {
   constructor(props){
       super(props);
   }

    componentWillMount(){
        this.props.getGroupsStatusAction(true);
    }

   render(){
       return(
            <Row>
                <Col xs={12} sm={12} md={6} lg={6}>
                    <div className="">
                        <table className="list-smaller-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Group Name</th>
                                <th>Description</th>
                                <th className="text-center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            { this.props.getGroupsStatus.status == 200 ?
                                this.props.getGroupsStatus.data.items.map((group, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{group.id}</td>
                                            <td>{group.groupName}</td>
                                            <td>{group.description}</td>
                                            <td className="text-center">
                                            {
                                                canUpdate == true ? 
                                                    <Link to={`/app/security/groups/edit/${group.id}`} className='btn btn-info btn-xs'>
                                                        <span className="glyphicon glyphicon-edit"></span>
                                                    </Link>
                                                : null
                                            }
                                            </td>
                                        </tr>
                                    )
                                })
                            : <tr>
                                <td colSpan={4} className="text-align-center">RESULT NOT FOUND</td>
                            </tr> }
                            </tbody>
                        </table>
                    </div>
                </Col>
            </Row>
       )
   }

}

function mapStateToProps(state) {
    return ({
        getGroupsStatus: state.userAuthentication.getGroupsStatus
    })
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getGroupsStatusAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ListActiveGroups);