import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, reset } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from './../../components/forms/TextBox';
import { TextArea } from './../../components/forms/TextArea';
import SelectBox from './../../components/forms/SelectBox';
import { ButtonSubmit } from './../../components/forms/ButtonSubmit';
import './../../components/forms/Styles.css';
import { addGroupAction } from '../../actions/authentication';

class AddGroup extends React.Component {
    constructor(props){
        super(props)
        this.state={
            status: [{id: true, name: "Active"},{id: false, name: "In Active"}]
        };
    }

    componentWillReceiveProps(data){
        if(data.addGroup.status == 200){
            alert("Successfully added new group");
            return data.addGroup.status = 0;
        }
        if(data.addGroup.status == 400 || data.addGroup.status == 500){
            alert("Fail with add new group!");
            return data.addGroup.status = 0;
        }
    }

    handleSubmit(values){
        let group = {
            "description": values.description,
            "enabled": values.enabled,
            "groupName": values.groupName
        };
        this.props.addGroupAction(group);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div>
                <Row>
                    <Col xs={12} sm={12} md={5} lg={5}>
                        <h3>Add New Group</h3>
                        <hr/>
                        <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Group Name <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="groupName" type="text" component={TextBox} label="Group name" icon="fa fa-users"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Description </strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-user-circle"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={4} lg={4} className="label-name">
                                    <strong>Status <span className="label-require">*</span></strong>
                                </Col>
                                <Col md={8} lg={8}>
                                    <Field name="enabled" type="select" component={SelectBox} placeholder="Please select status" values={this.state.status} sortBy="name" icon="fa fa-hand-paper-o"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col mdOffset={8} lgOffset={8} md={4} lg={4}>
                                    <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
            </div>
        )
    }
}

const afterSubmit = (result, dispatch) =>
  dispatch(reset('form_add_group'));

AddGroup = reduxForm({
    form: 'form_add_group',
    onSubmitSuccess: afterSubmit,
    validate: (values) => {
        let regex_name = /[a-zA-Z]{4,20}/;

        const errors = {};
        if (!regex_name.test(values.groupName) || values.groupName === undefined) {
            errors.groupName = 'Group name is required and at least 4 character!';
        }
        if(values.enabled === undefined){
            errors.enabled = 'Status is required!';
        }
        return errors
    }
})(AddGroup);

function mapStateToProps(state) {
    return {
        addGroup : state.userAuthentication.addGroup
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({addGroupAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AddGroup);