import React from 'react';
import {connect} from 'react-redux';
import { Row, Col, Button } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { TextArea } from '../../../components/forms/TextArea';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../../../components/forms/Styles.css';
import { addGroupReportAction, listAllGroupReportsAction, updateGroupReportAction, deleteGroupReportAction } from './../../../actions/report/groupReport';

let groupId = 0;
class GroupReport extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            update: false,
            label: 'Save',
            labelForm: 'Add New Group Report',
            status: [{id: 1, name: "Active"},{id: 0, name: "In Active"}]
        };
        this.deleteItem = this.deleteItem.bind(this);
        this.handleBack = this.handleBack.bind(this);
    }

    componentWillMount(){
        this.props.listAllGroupReportsAction(-1);
    }

    componentWillReceiveProps(data){
        if(data.addGroupReport.status === 200){
            alert("Successfully added new group report.");
            this.props.listAllGroupReportsAction(-1);
            data.addGroupReport.status = 0;
        }
        if(data.addGroupReport.status === 404 || data.addGroupReport.status === 500){
            alert("Fail with add group report!");
        }

        if(data.updateGroupReport.status === 200 ){
            alert("Successfully updated group report.");
            this.setState({label: 'Save'});
            this.setState({labelForm: 'Add New Group Report'});
            this.setState({update: false});
            this.props.listAllGroupReportsAction(-1);
            data.updateGroupReport.status = 0;
        }
        if(data.updateGroupReport.status === 400 || data.updateGroupReport.status === 500){
            alert("Fail with update group report!");
        }

        if(data.deleteGroupReport.status === 200){
            alert("Successfully deleted this group report.");
            this.props.listAllGroupReportsAction(-1);
            data.deleteGroupReport.status = 0;
        }
        if(data.deleteGroupReport.status === 400 || data.deleteGroupReport.status === 500){
            alert("Fail with delete this group report!");
        }
    }

    handleBack(){
        location.href = "/app/reports/group-reports";
    }

    updateItem(data){
        groupId = data.id;
        this.setState({update: true});
        this.setState({label: 'Edit'});
        this.setState({labelForm: 'Edit Group Report'});
        this.props.dispatch(initialize(
            'form_add_group_report',
            data,
            ['name', 'ordering','description', 'status'])
        );
    }

    deleteItem(id){
        if(confirm("Are you sure want to delete this group report?") === true){
            this.props.deleteGroupReportAction(id);
        }
    }

    handleSubmit(values){
        if(!this.state.update){
            // add
            let group = {
                "description": values.description,
                "name": values.name,
                "ordering": Number(values.ordering),
                "status": 1
            };
            this.props.addGroupReportAction(group);
        }else{
            // edit
            let group = {
                "id": groupId,
                "description": values.description,
                "name": values.name,
                "ordering": Number(values.ordering),
                "status": 1
            };
            this.props.updateGroupReportAction(group);
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="container-fluid">
                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="name" type="text" component={TextBox} label="Name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Ordering <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="ordering" type="text" component={TextBox} label="Ordering" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" icon="fa fa-shopping-bag"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label={this.state.label} />
                                        </Col>
                                        {
                                            this.state.label == "Edit" ?
                                                <Col md={3} lg={3} className="pull-right">
                                                    <Button className="bt-cancel" onClick={() => this.handleBack()}>Cancel</Button>
                                                </Col>
                                                : null
                                        }
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>

                <Row>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="custyle">
                            <table className="table table-bordered table-striped custab">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Ordering</th>
                                    <th>Description</th>
                                    <th>status</th>
                                    <th className="text-center">Action</th>
                                </tr>
                                </thead>
                                {
                                    this.props.listAllGroupReport.status == 200 ?
                                        <tbody>
                                        { this.props.listAllGroupReport.data.items.map((group,index)=>{
                                            return(
                                                <tr key={index}>
                                                    <td>{group.id}</td>
                                                    <td>{group.name}</td>
                                                    <td>{group.ordering}</td>
                                                    <td>{group.description}</td>
                                                    <td>{group.status == 1 ? 'Active' : 'In Active'}</td>
                                                    {
                                                        group.status == 1?
                                                            <td className="text-center">
                                                                <a className='btn btn-info btn-xs' onClick={() => this.updateItem(group)}>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </a>
                                                                &nbsp;
                                                                <a onClick={()=> this.deleteItem(group.id)} href="#" className="btn btn-danger btn-xs">
                                                                    <span className="glyphicon glyphicon-remove"></span>
                                                                </a>
                                                            </td>
                                                            :
                                                            <td className="text-center">
                                                                <a className='btn btn-warning btn-xs' onClick={() => this.updateItem(group)}>
                                                                    <span className="glyphicon glyphicon-edit"></span>
                                                                </a>
                                                            </td>
                                                    }

                                                </tr>
                                            )
                                        })}
                                        </tbody>
                                        :
                                        <tbody>
                                        <tr>
                                            <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                        </tr>
                                        </tbody>
                                }
                            </table>
                        </div>
                    </Col>
                    <Col xs={2} sm={2} md={2} lg={2}></Col>
                </Row>
            </div>
        )
    }
}

GroupReport = reduxForm({
    form: 'form_add_group_report',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]{2,100}/;
        let regex_number = /[0-9]{1,4}/;

        const errors = {};
        if (!regex_name.test(values.name) || values.name === undefined) {
            errors.name = 'Invalid name!';
        }
        if (!regex_number.test(values.ordering) || values.ordering === undefined) {
            errors.ordering = 'Ordering accepts only number!';
        }
        return errors;
    }
})(GroupReport);

function mapStateToProps(state) {
    //console.log(state.groupReport.listAllGroupReport);
    return {
        addGroupReport: state.groupReport.addGroupReport,
        listAllGroupReport: state.groupReport.listAllGroupReport,
        updateGroupReport: state.groupReport.updateGroupReport,
        deleteGroupReport: state.groupReport.deleteGroupReport
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ addGroupReportAction, listAllGroupReportsAction, updateGroupReportAction, deleteGroupReportAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(GroupReport);