import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import SelectBox from '../../../components/forms/SelectBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../report-res-prepaid.css';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getTransformerReportsAction, getPoleReportsAction } from '../../../actions/report/transformer';
import { authObj } from '../../../../auth';
import { getSetting } from './../../../utils/Setting';
import moment from 'moment';

class TransformerReport extends React.Component{
    constructor(props){
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount(){

    }
    componentWillReceiveProps(data){
        if(data.history12Months.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.history12Months.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        let licenseName = "";
        let address = "";
        if(data.settings.items !== undefined){
            licenseName = getSetting(data.settings.items, "LICENSE_NAME").value;
            address = getSetting(data.settings.items, "LOCATION").value;
        }
        this.setState({licenseName: licenseName, address: address});
    }

    handleSearch(value){

    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;

        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <form onSubmit={handleSubmit(this.handleSearch)} className="wrap-full-form search-installer">
                            <Row>
                                <Col lg={10}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">ID</p></td>
                                            <td><Field name="area" type="select" component={SelectBox} placeholder="Please select ..." values={[]} /></td>
                                            <td><Field name="trans" type="select" component={SelectBox} placeholder="Please select ..." values={[]} /></td>
                                            <td><p className="search-installer">DATE</p></td>
                                            <td><ButtonSubmit className="search-installer" error={error} invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                            <td><button style={{color: '#fff', height: '32px', border: '0', borderRadius: 'inherit', background: '#1c91d0'}}>Refresh</button></td>
                                            <td>
                                                <ReactHTMLTableToExcel
                                                    id="button_report_ref_12_months"
                                                    className="download-table-xls-button"
                                                    table="report_ref_12_months"
                                                    filename="report_raf_12_months"
                                                    sheet="report_ref_12_months"
                                                    buttonText="Export to Excel"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                    <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                    <span className="sr-only">Loading...</span>
                </div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="report_ref_12_months" striped bordered condensed className="prepaid-res-report">
                            <thead>
                            <tr style={{border:'1.5pt solid white', fontFamily: "Khmer OS", fontSize: "11pt", background: "#afafaf"}}>
                                <th>ល.រ</th>
                                <th>កូដ</th>
                                <th>ឈ្មោះអតិថិជន</th>
                                <th>បង្គោល</th>
                                <th>ប្រអប់​</th>
                                <th>កុងទ័រ</th>
                                <th>អំពែរ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colSpan ={19}><strong>លក់អោយអតិថិជន MV (ឧស្សាហកម្ម)</strong></td>
                            </tr>
                            </tbody>
                            <tbody>
                            <tr>
                                <td colSpan ={19}><strong>លក់អោយអតិថិជន តង់ស្យុងទាប (លំនៅដ្ឋាន )</strong></td>
                            </tr>
                            </tbody>
                            <tbody>
                            <tr>
                                <td colSpan={3}>
                                    <strong style={{textAlign: 'center'}}>អតិថិជនសរុបចំនួន  {history12Months !== undefined ? history12Months.length : 0}  នាក់   </strong>
                                </td>
                            </tr>
                            <tr><td colSpan={19}> </td></tr>
                            <tr style={{textAlign: 'center'}}>
                                <td colSpan={9}>
                                    <strong>បោះពុម្ភដោយ ៖ {authObj().sub.toUpperCase()}</strong>
                                </td>
                                <td colSpan={10}>
                                    <strong>កាលបរិច្ឆេទបោះពុម្ភ ៖ {moment().format("DD-MM-YY")}</strong>
                                </td>
                            </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

TransformerReport = reduxForm({
    form: 'form_transformer_reports',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.customerId !== undefined){
            if(!number.test(values.customerId)){
                errors.customerId = 'Please input number only !!';
            }
        }
        return errors;
    },
})(TransformerReport);

function mapStateToProps(state){
    //console.log(JSON.stringify(state.reportRef.history12Months));
    return{
        transformers: state.transformerReport.transformerReports,
        poles: state.transformerReport.poleReports
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({getTransformerReportsAction, getPoleReportsAction},dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps) (TransformerReport);