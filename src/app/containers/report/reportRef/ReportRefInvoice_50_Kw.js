import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Field , initialize, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { DateTimePicker }  from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../report-res-prepaid.css';
import { getAllReportRefPrepaidGreaterThan50KwNotSponsorAction, getAllReportRefPrepaidGreaterThan50KwResidentialAction } from './../../../actions/report/reportRef';
import moment from 'moment';
import ReactLoading from 'react-loading';

class ReportRefInvoice_50_Kw extends React.Component{
    constructor(props){
        super(props);
        this.state={
            startDate: moment()
        };
        this.handleStartDate=this.handleStartDate.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }
    // componentWillMount(){
    //         this.props.getAllReportRefPrepaidGreaterThan50KwNotSponsorAction({
    //             id:1,
    //             date:""
    //         });
    //         this.props.getAllReportRefPrepaidGreaterThan50KwResidentialAction({
    //             id:3,
    //             date:""
    //         });
    // }
    componentWillReceiveProps(data){
        if(data._51KwSponsor.status === 200 || data._51KwSponsor.status === 404){
            document.getElementById('loading').style.display = 'none';
            //this.setState({loading: 'none'});
        }

        if(data._51KwNoSponsor.status === 200 || data._51KwNoSponsor.status === 404){
            document.getElementById('loading').style.display = 'none';
            //this.setState({loading: 'none'});
        }
    }
    handleSearch(value){
        document.getElementById('loading').style.display = 'block';
        this.props.getAllReportRefPrepaidGreaterThan50KwNotSponsorAction({
            id:1,
            date:value.startDate
        });
        this.props.getAllReportRefPrepaidGreaterThan50KwResidentialAction({
            id:3,
            date:value.startDate
        });
    }
    handleStartDate(date){
        this.setState({
            startDate:date
        })
    }
    /* helper functions */
    formatStrings(str){
        let items = JSON.parse(str);
        if(items !== null && items !== undefined){
            if(items.length === 1){
                return items[0];
            }else if(items.length === 2){
                return items[0]+"/"+items[1];
            }else {
                return items[0]+"/"+items[1]+"/"+items[2];
            }
        }
    }
    render(){
        const {handleSubmit, invalid, error, submitting} = this.props;
        let powerBig = 0;
        let paidAmountRielBig = 0;
        let paidAmountUsdBig = 0;
        let power2001 = 0;
        let paidAmountRiel2001 = 0;
        let paidAmountUsd2001 = 0;
        let power2000 = 0;
        let paidAmountRiel2000 = 0;
        let paidAmountUsd2000 = 0;
        let power51 = 0;
        let paidAmountRiel51 = 0;
        let paidAmountUsd51 = 0;
        return(
            <div className="container-fluid">
                <br />
                <h3 style={{marginTop: '-2%', textAlign: 'center', textDecoration: 'underline'}}>
                    REPORT RAF 51KWH FOR: <strong style={{color: '#1c91d0'}}>{moment(this.state.startDate).format("MM-YYYY")}</strong>
                </h3>
                <br />
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <form onSubmit={handleSubmit(this.handleSearch)} className="wrap-full-form search-installer">
                            <Row>
                                <Col lg={10}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">DATE</p></td>
                                            <td><Field name="startDate" component={DateTimePicker} defaultDate={this.state.startDate}  handleChange={this.handleStartDate} placeholder="Select.." /></td>
                                            <td><ButtonSubmit className="search-installer" error={error} invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                            <td>
                                                <ReactHTMLTableToExcel
                                                    id="button_report_ref_12_months"
                                                    className="download-table-xls-button"
                                                    table="table-to-xls"
                                                    filename="raf_report_51kwh"
                                                    sheet="raf_report_51kwh"
                                                    buttonText="Export to Excel"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12} >
                        <Table striped bordered condensed className="prepaid-res-report" id = "table-to-xls">
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Khmer Moul',border:'1px solid transparent'}} colSpan={12}><h2 className="top-header" style={{textAlign: 'center'}}>សាខាក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឹលូសិន ខបភើរេសិន</h2></th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Kh Siemreap', borderBottom:'1px solid gray'}} colSpan={12}>   <p className="prepaid-header" style={{textAlign: 'center'}}>ខេត្តព្រៃវែង</p></th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Khmer OS Muol'}} colSpan={12}> <h4 className="header-report" style={{textAlign: 'center'}}>របាយការណ៍ លំអិតការចេញវិក្កយបត្រប្រចាំខែ {moment(this.state.startDate).format("MM-YYYY")}</h4></th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Kh Siemreap'}} colSpan={12}>  <p className="prepaid-header" style={{textAlign: 'center'}}>អតិថិជន៖គ្រប់អតិថិជន  ការភ្ជាប់ចរន្ត៖ គ្រប់ប្រភេទភ្ជាប់ចរន្ត</p></th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Kh Siemreap',borderTop:'0'}} colSpan={12}> <p className="prepaid-header" style={{textAlign: 'center'}}>ថាមពល៖>50 kWh  រូបបិយប័ណ្ណ៖គ្រប់រូបិយប័ណ្ណ </p></th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr className="custom-header-table">
                                <th style={{fontFamily:'Kh Siemreap',borderBottom:'0'}} colSpan={12}><p className="prepaid-header" style={{textAlign: 'center'}}>ជុំទូទាត់៖ គ្រប់ជុំការទូទាត់     តំបន់៖ គ្រប់តំបន់</p></th>
                            </tr>
                            </thead>
                            <thead style={{background:'gray'}}>
                            <tr style={{fontFamily:'Kh Siemreap', fontSize:'14px'}}>
                                <th style={{background:'rgb(175, 175, 175)',border:'1px'}}>ល.រ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>កូដ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ឈ្មោះអតិថិជន</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>អំពែ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ប្រអប់​</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>កុងទ័រ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>តំលៃ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ចាស់</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ថ្មី</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>មេគុណ</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ថាមពល</th>
                                <th style={{background:'rgb(175, 175, 175)'}}>ទឹកប្រាក់</th>
                            </tr>
                            </thead>
                            {/* Not Sponsor  */}
                            <tbody >
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td colSpan ={12}>ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)</td>
                            </tr>
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td> </td>
                                <td colSpan ={11}>ថាមពលលក់លើតងលក់លើតង់ស្យុងមធ្យម</td>
                            </tr>
                            <tr>
                                <td colSpan ={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    {/*<i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>*/}
                                    {/*<span className="sr-only">Loading...</span>*/}
                                </td>
                            </tr>
                            { this.props._51KwNoSponsor.items !== undefined ?
                                this.props._51KwNoSponsor.items.map((ref,index)=>{
                                    if(ref.voltage >= 2200){
                                        powerBig += Number(ref.power);
                                        if(ref.rate !== null && ref.rate !== undefined){
                                            if(ref.rate.indexOf('RIEL') !== -1){
                                                paidAmountRielBig += Number(ref.paidAmount);
                                            } else{
                                                paidAmountUsdBig += Number(ref.paidAmount);
                                            }
                                        }
                                        return(
                                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'12px', textAlign:'center'}} key={index}>
                                                <td>{index+1}</td>
                                                <td>{ref.id}</td>
                                                <td>{ref.nameKh}</td>
                                                <td>{ref.ampere}</td>
                                                <td>{ref.boxSerial}</td>
                                                <td>{this.formatStrings(ref.meterSerial)}</td>
                                                <td>{ref.rate}</td>
                                                <td>{this.formatStrings(ref.oldUsage)}</td>
                                                <td>{this.formatStrings(ref.newUsage)}</td>
                                                <td>{this.formatStrings(ref.coefficient)}</td>
                                                <td>{ref.power}</td>
                                                <td>{ref.paidAmount}</td>
                                            </tr>
                                        )
                                    }else {
                                    }

                                })
                                :
                                null
                            }
                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'14px', fontWeight:'bold', textAlign:'center'}} className="total">
                                <td colSpan ={10} style={{background:'#afafaf', textAlign: 'left'}}>សរុប៖</td>
                                <td style={{background:'#afafaf'}}>{powerBig}</td>
                                <td style={{background:'#afafaf'}}>{paidAmountRielBig}៛ និង {paidAmountUsdBig}$</td>
                            </tr>

                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td colSpan ={12}>ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)</td>
                            </tr>
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td> </td>
                                <td colSpan ={11}>ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើចាប់ពី 2001 kWh/ខែ ឡើង</td>
                            </tr>
                            <tr>
                                <td colSpan ={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    {/*<i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>*/}
                                    {/*<span className="sr-only">Loading...</span>*/}
                                </td>
                            </tr>
                            { this.props._51KwNoSponsor.items !== undefined ?
                                this.props._51KwNoSponsor.items.map((ref,index)=>{
                                    if(ref.power >= 2001){
                                        power2001 += Number(ref.power);
                                        if(ref.rate !== null && ref.rate !== undefined){
                                            if(ref.rate.indexOf('RIEL') !== -1){
                                                paidAmountRiel2001 += Number(ref.paidAmount);
                                            } else{
                                                paidAmountUsd2001 += Number(ref.paidAmount);
                                            }
                                        }
                                        return(
                                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'12px', textAlign:'center'}} key={index}>
                                                <td>{index+1}</td>
                                                <td>{ref.id}</td>
                                                <td>{ref.nameKh}</td>
                                                <td>{ref.ampere}</td>
                                                <td>{ref.boxSerial}</td>
                                                <td>{this.formatStrings(ref.meterSerial)}</td>
                                                <td>{ref.rate}</td>
                                                <td>{this.formatStrings(ref.oldUsage)}</td>
                                                <td>{this.formatStrings(ref.newUsage)}</td>
                                                <td>{this.formatStrings(ref.coefficient)}</td>
                                                <td>{ref.power}</td>
                                                <td>{ref.paidAmount}</td>
                                            </tr>
                                        )
                                    }else {
                                    }

                                })
                                :
                                null
                            }
                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'14px', fontWeight:'bold', textAlign:'center'}} className="total">
                                <td colSpan ={10} style={{background:'#afafaf', textAlign: 'left'}}>សរុប៖</td>
                                <td style={{background:'#afafaf'}}>{power2001}</td>
                                <td style={{background:'#afafaf'}}>{paidAmountRiel2001}៛ និង {paidAmountUsd2001}$</td>
                            </tr>

                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td colSpan ={12}>ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)</td>
                            </tr>
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td> </td>
                                <td colSpan ={11}>ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ</td>
                            </tr>
                            <tr>
                                <td colSpan ={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    {/*<i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>*/}
                                    {/*<span className="sr-only">Loading...</span>*/}
                                </td>
                            </tr>
                            { this.props._51KwNoSponsor.items !== undefined ?
                                this.props._51KwNoSponsor.items.map((ref,index)=>{
                                    if(ref.power >= 2001){
                                        power2001 += Number(ref.power);
                                        if(ref.rate !== null && ref.rate !== undefined){
                                            if(ref.rate.indexOf('RIEL') !== -1){
                                                paidAmountRiel2001 += Number(ref.paidAmount);
                                            } else{
                                                paidAmountUsd2001 += Number(ref.paidAmount);
                                            }
                                        }
                                        return(
                                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'12px', textAlign:'center'}} key={index}>
                                                <td>{index+1}</td>
                                                <td>{ref.id}</td>
                                                <td>{ref.nameKh}</td>
                                                <td>{ref.ampere}</td>
                                                <td>{ref.boxSerial}</td>
                                                <td>{this.formatStrings(ref.meterSerial)}</td>
                                                <td>{ref.rate}</td>
                                                <td>{this.formatStrings(ref.oldUsage)}</td>
                                                <td>{this.formatStrings(ref.newUsage)}</td>
                                                <td>{this.formatStrings(ref.coefficient)}</td>
                                                <td>{ref.power}</td>
                                                <td>{ref.paidAmount}</td>
                                            </tr>
                                        )
                                    }else {
                                    }

                                })
                                :
                                null
                            }
                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'14px', fontWeight:'bold', textAlign:'center'}} className="total">
                                <td colSpan ={10} style={{background:'#afafaf', textAlign: 'left'}}>សរុប៖</td>
                                <td style={{background:'#afafaf'}}>{power2001}</td>
                                <td style={{background:'#afafaf'}}>{paidAmountRiel2001}៛ និង {paidAmountUsd2001}$</td>
                            </tr>
                            </tbody>
                            {/* Sponsor */}
                            <tbody >
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td colSpan ={12}>ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណៈ)</td>
                            </tr>
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td> </td>
                                <td colSpan ={11}>ថាមពលលក់លើ LV នាឡិកាស្ទង់ LV ភ្ជាប់ពីខ្សែសាធារណៈប្រើក្រោម 2001 kWh/ខែ</td>
                            </tr>
                            <tr>
                                <td colSpan ={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    {/*<i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>*/}
                                    {/*<span className="sr-only">Loading...</span>*/}
                                </td>
                            </tr>
                            { this.props._51KwSponsor.items !== undefined ?
                                this.props._51KwSponsor.items.map((ref,index)=>{
                                    if(ref.power < 2001){
                                        power2000 += Number(ref.power);
                                        if(ref.rate !== null && ref.rate !== undefined){
                                            if(ref.rate.indexOf('RIEL') !== -1){
                                                paidAmountRiel2000 += Number(ref.paidAmount);
                                            } else{
                                                paidAmountUsd2000 += Number(ref.paidAmount);
                                            }
                                        }

                                        return(
                                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'12px', textAlign:'center'}} key={index}>
                                                <td>{index+1}</td>
                                                <td>{ref.id}</td>
                                                <td>{ref.nameKh}</td>
                                                <td>{ref.ampere}</td>
                                                <td>{ref.boxSerial}</td>
                                                <td>{this.formatStrings(ref.meterSerial)}</td>
                                                <td>{ref.rate}</td>
                                                <td>{this.formatStrings(ref.oldUsage)}</td>
                                                <td>{this.formatStrings(ref.newUsage)}</td>
                                                <td>{this.formatStrings(ref.coefficient)}</td>
                                                <td>{ref.power}</td>
                                                <td>{ref.paidAmount}</td>
                                            </tr>
                                        )
                                    }else {}
                                })
                                :null
                            }
                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'14px', fontWeight:'bold', textAlign:'center'}} className="total">
                                <td style={{background:'#afafaf'}} colSpan ={10}>សរុប៖</td>
                                <td style={{background:'#afafaf'}}>{power2000}</td>
                                <td style={{background:'#afafaf'}}>{paidAmountRiel2000}៛ និង {paidAmountUsd2000}$</td>
                            </tr>
                            </tbody>
                            {/* Normal */}
                            <tbody >
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td colSpan ={12}>ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន</td>
                            </tr>
                            <tr style={{fontFamily:'Kh Siemreap'}}>
                                <td> </td>
                                <td colSpan ={11}>ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើធំជាង 51 kWh/ខែ</td>
                            </tr>
                            <tr>
                                <td colSpan ={12}>
                                    <div id="loading" style={{display: 'none', marginTop: 20}}>
                                        <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                                    </div>
                                    {/*<i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>*/}
                                    {/*<span className="sr-only">Loading...</span>*/}
                                </td>
                            </tr>
                            { this.props._51KwSponsor.items !== undefined ?
                                this.props._51KwSponsor.items.map((ref,index)=>{
                                    if(51 <= ref.power < 2001){
                                        power51 +=Number(ref.power);
                                        if(ref.rate !== undefined && ref.rate !== null){
                                            if(ref.rate.indexOf('RIEL')!== -1){
                                                paidAmountRiel51 += Number(ref.paidAmount);
                                            }else{
                                                paidAmountRiel51 += Number(ref.paidAmount);
                                            }
                                        }
                                        return(
                                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'12px', textAlign:'center'}} key={index}>
                                                <td>{index+1}</td>
                                                <td>{ref.id}</td>
                                                <td>{ref.nameKh}</td>
                                                <td>{ref.ampere}</td>
                                                <td>{this.formatStrings(ref.boxSerial)}</td>
                                                <td>{this.formatStrings(ref.meterSerial)}</td>
                                                <td>{ref.rate}</td>
                                                <td>{this.formatStrings(ref.oldUsage)}</td>
                                                <td>{this.formatStrings(ref.newUsage)}</td>
                                                <td>{this.formatStrings(ref.coefficient)}</td>
                                                <td>{ref.power}</td>
                                                <td>{ref.paidAmount}</td>
                                            </tr>
                                        )
                                    }else{}
                                })
                                : null
                            }
                            <tr style={{fontFamily:'Kh Siemreap',fontSize:'14px', fontWeight:'bold', textAlign:'center'}} className="total">
                                <td style={{background:'#afafaf', textAlign: 'left'}} colSpan ={10}>សរុប៖</td>
                                <td style={{background:'#afafaf'}}>{power51}</td>
                                <td style={{background:'#afafaf'}}>{paidAmountRiel51}៛ និង {paidAmountUsd51}$</td>
                            </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}
ReportRefInvoice_50_Kw = reduxForm({
    form: 'form-report-ref-greater-than-50'
})(ReportRefInvoice_50_Kw);

function mapStateToProps(state){
    //console.log("not sponsor:",state.reportRef.getAllReportRefPrepaidGreaterThan50KwNotSponsor);
    return{
        _51KwNoSponsor: state.reportRef.getAllReportRefPrepaidGreaterThan50KwNotSponsor,
        _51KwSponsor: state.reportRef.getAllReportRefPrepaidGreaterThan50KwResidential
    }
}
function mapDispatchToProps(dispatch){
   return bindActionCreators({getAllReportRefPrepaidGreaterThan50KwNotSponsorAction, getAllReportRefPrepaidGreaterThan50KwResidentialAction},dispatch);
}
export default connect(mapStateToProps,mapDispatchToProps) (ReportRefInvoice_50_Kw);