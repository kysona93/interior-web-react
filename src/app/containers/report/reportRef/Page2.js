import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Row, Col, Table, Panel, Button } from 'react-bootstrap';
import SelectBox from '../../../components/forms/SelectBox';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import {
    // sale LV
    getReportSubsidySaleLV0to2000kwAction,
    getReportSubsidySaleLV0to10kwAction,
    getReportSubsidySaleLV11to50kwAction,
    getReportSubsidySaleLV51kwUpAction
} from '../../../actions/report/subsidy/reportSaleLV';
import  {
    getReportSubsidySaleMVLicenseAction,
    getReportSubsidySaleMVAction,
    getReportSubsidySaleLVKw2001UPAction
} from '../../../actions/report/subsidy/reportSaleMV';
// sale LV
let bus2001kwUp = {
    customers : 0,
    powers: 0,
    rate: ''
};
let bus0to2000kw = {
    customers : 0,
    powers: 0,
    rate: ''
};
let normal0to10kw = {
    customers : 0,
    powers: 0,
    rate: ''
};
let normal11to50kw = {
    customers : 0,
    powers: 0,
    rate: ''
};
let normal51kwUp = {
    customers : 0,
    powers: 0,
    rate: ''
};
class Page2 extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            date : "",
            months :[
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ]
        };
    }

    componentWillReceiveProps(data){
        // sale MV
        if(data.getAllReportSubsidySaleLVKW2001UP.status === 200){
            bus2001kwUp.customers = 0; bus2001kwUp.powers = 0; bus2001kwUp.rate = "";
            let items = data.getAllReportSubsidySaleLVKW2001UP.data.items;
            items.forEach((element) =>{
                bus2001kwUp.customers += element.customers;
                bus2001kwUp.powers += element.power;
                bus2001kwUp.rate = (element.rate + "/" + bus2001kwUp.rate);
                bus2001kwUp.rate = bus2001kwUp.rate.replace('USD','').replace('RIEL','');
            });
            data.getAllReportSubsidySaleLVKW2001UP.status = 0;
        }
        if(data.getReportSubsidySaleLV0to2000kw.status === 200){
            bus0to2000kw.customers = 0; bus0to2000kw.powers = 0; bus0to2000kw.rate = "";
            let items = data.getReportSubsidySaleLV0to2000kw.data.items;
            items.forEach((element) =>{
                bus0to2000kw.customers += element.customers;
                bus0to2000kw.powers += element.power;
                bus0to2000kw.rate = (element.rate + "/" + bus0to2000kw.rate);
                bus0to2000kw.rate = bus0to2000kw.rate.replace('USD','').replace('RIEL','');
            });
            data.getReportSubsidySaleLV0to2000kw.status = 0;
        }

        // sale LV
        if(data.getReportSubsidySaleLV0to10kw.status === 200 ){
            normal0to10kw.customers = 0; normal0to10kw.powers = 0; normal0to10kw.rate = "";
            let items = data.getReportSubsidySaleLV0to10kw.data.items;
            items.forEach((element) =>{
                normal0to10kw.customers += element.customers;
                normal0to10kw.powers += element.power;
                normal0to10kw.rate = (element.rate + "/" + normal0to10kw.rate);
                normal0to10kw.rate = normal0to10kw.rate.replace('USD','').replace('RIEL','');
            });
            data.getReportSubsidySaleLV0to10kw.status = 0;
        }
        if(data.getReportSubsidySaleLV11to50kw.status === 200 ){
            normal11to50kw.customers = 0; normal11to50kw.powers = 0; normal11to50kw.rate = "";
            let items = data.getReportSubsidySaleLV11to50kw.data.items;
            items.forEach((element) =>{
                normal11to50kw.customers += element.customers;
                normal11to50kw.powers += element.power;
                normal11to50kw.rate = (element.rate + "/" + normal11to50kw.rate);
                normal11to50kw.rate = normal11to50kw.rate.replace('USD','').replace('RIEL','');
            });
            data.getReportSubsidySaleLV11to50kw.status = 0;
        }
        if(data.getReportSubsidySaleLV51kwUp.status === 200 ){
            normal51kwUp.customers = 0; normal51kwUp.powers = 0; normal51kwUp.rate = "";
            let items = data.getReportSubsidySaleLV51kwUp.data.items;
            items.forEach((element) =>{
                normal51kwUp.customers += element.customers;
                normal51kwUp.powers += element.power;
                normal51kwUp.rate = (element.rate + "/" + normal51kwUp.rate);
                normal51kwUp.rate = normal51kwUp.rate.replace('USD','').replace('RIEL','');
            });
            data.getReportSubsidySaleLV51kwUp.status = 0;
        }
    }

    handleSubmit(values){
        this.setState({date : values.month+"-"+values.year});
        this.handleClear();
        // sale MV
        this.props.getReportSubsidySaleMVLicenseAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28"
        });
        this.props.getReportSubsidySaleMVAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28"
        });
        this.props.getReportSubsidySaleLVKw2001UPAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'consumptionId' : 2,
            'startUnit' : 2001,
            'endUnit' : 0,
            'isUpper' : 1
        });
        // sale LV
        this.props.getReportSubsidySaleLV0to2000kwAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'consumptionId' : 2,
            'startUnit' : 0,
            'endUnit' : 2000,
            'isUpper' : 0
        });
        this.props.getReportSubsidySaleLV0to10kwAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'consumptionId' : 1,
            'startUnit' : 0,
            'endUnit' : 10,
            'isUpper' : 0
        });
        this.props.getReportSubsidySaleLV11to50kwAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'consumptionId' : 1,
            'startUnit' : 11,
            'endUnit' : 50,
            'isUpper' : 0
        });
        this.props.getReportSubsidySaleLV51kwUpAction({
            'startDate' : values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'consumptionId' : 1,
            'startUnit' : 51,
            'endUnit' : 0,
            'isUpper' : 1
        });
    }

    // helper functions
    formatStrings(str){
        let rate = "";
        let items = JSON.parse(str);
        if(items.length > 0){
            items.forEach((element) => {
                rate = element + "/" + rate;
                rate = rate.replace('RIEL','').replace('USD','');
            });
        }
        return rate;
    }

    handleRefresh(){
        location.href = "/app/reports/ref/subsidy";
    }

    handleClear(){
        bus2001kwUp.customers = 0;
        bus2001kwUp.powers= 0;
        bus2001kwUp.rate= '';

        bus0to2000kw.customers = 0;
        bus0to2000kw.powers= 0;
        bus0to2000kw.rate= '';

        normal0to10kw.customers = 0;
        normal0to10kw.powers= 0;
        normal0to10kw.rate= '';

        normal11to50kw.customers = 0;
        normal11to50kw.powers= 0;
        normal11to50kw.rate= '';

        normal51kwUp.customers = 0;
        normal51kwUp.powers= 0;
        normal51kwUp.rate= '';
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div>
                <Row>
                    <Col md={8} lg={8} className="pull-right">
                        <Panel header={"Report Ref for " + this.state.date} bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field
                                            name="month"
                                            type="select"
                                            component={SelectBox}
                                            placeholder="Select Month"
                                            values={this.state.months} />
                                    </Col>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field name="year" type="text" component={TextBox} label="Year"/>
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="table-to-xls"
                            filename="report_subsidy_kw_using"
                            sheet="tablexls"
                            buttonText="Export to XLS"/>
                        <Table striped bordered condensed className="prepaid-res-report" id="table-to-xls">
                            <thead hidden>
                            <tr>
                                <th className="bg-none" colSpan={6}>
                                    <h2 style={{textAlign: 'left', fontFamily: "Kh Muol", fontSize: "13pt"}}>អាជ្ញាប័ណ្ណលេខ ៖ 259</h2>
                                </th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr>
                                <th className="bg-none" colSpan={6}>
                                    <p style={{textAlign: 'left', fontFamily: "Kh Siemreap", fontSize: "9pt"}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​ ៖ ក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឹលូសិន ខបភើរេសិន </p>
                                </th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr>
                                <th className="bg-none" colSpan={6}>
                                    <h4 style={{textAlign: 'left', fontFamily: "Khmer OS Muol", fontSize: "10pt"}}>ទិន្នន័យសម្រាប់ខែ ៖  {this.state.date}</h4>
                                </th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr>
                                <th className="bg-none" colSpan={6}>
                                    <h4 style={{textAlign: 'center', fontFamily: "Khmer OS Muol", fontSize: "10pt"}}>តារាងលក់ថាមពល</h4>
                                </th>
                            </tr>
                            </thead>
                            <thead hidden>
                            <tr>
                                <th className="bg-none" colSpan={6}>
                                    <h4 style={{textAlign: 'center', fontFamily: "Khmer OS Muol", fontSize: "10pt"}}> Table for Total Sale </h4>
                                </th>
                            </tr>
                            </thead>
                            <thead>
                            <tr>
                                <th colSpan={2} style={{fontFamily: "Khmer OS Battambang", fontSize: "12pt", backgroundColor: "#BDBDBD"}}>ប្រភេទនៃការលក់<br/>(Type of Sale)</th>
                                <th style={{fontFamily: "Khmer OS Battambang", fontSize: "12pt", backgroundColor: "#BDBDBD"}}>ចំនួនអតិថិជន<br/>​(No. of Consumer)</th>
                                <th style={{fontFamily: "Khmer OS Battambang", fontSize: "12pt", backgroundColor: "#BDBDBD"}}>ចំនួនថាមពលលក់<br/>(KWh sold)</th>
                                <th style={{fontFamily: "Khmer OS Battambang", fontSize: "12pt", backgroundColor: "#BDBDBD"}}>អត្រាថ្លៃ<br/>(Tariff of Sale)</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colSpan ={5} style={{fontFamily: "Khmer OS Battambang", fontSize: "11pt"}}>I.លក់អោយអ្នកកាន់អាជ្ញាប័ណ្ណ</td>
                                </tr>
                            </tbody>
                            {
                                this.props.getAllReportSubsidySaleMVLicense.status === 200 ?
                                    <tbody>
                                    {
                                        this.props.getAllReportSubsidySaleMVLicense.data.items.map((element, index) => {
                                            return(
                                                <tr key={index} style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                                    <td colSpan={2}>{index+1 + ". "}{element.saleTypeKh}</td>
                                                    <td>{element.customers}</td>
                                                    <td>{element.power.toFixed(2)}</td>
                                                    <td>{this.formatStrings(element.rate)}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                :
                                    <tbody>
                                        <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                            <td colSpan={2}>1.លក់លើ​ MV នាឡិកាស្ទង់ MV</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                            <td colSpan={2}>2.លក់លើ​ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ(វិនិយោគដោយអ្នកទិញ)</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                        <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                            <td colSpan={2}>3.លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ(វិនិយោគដោយអ្នកលក់)</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tbody>
                            }
                            <tbody>
                                <tr >
                                    <td colSpan ={5} style={{fontFamily: "Khmer OS Battambang", fontSize: "11pt"}}>II.ថាមពលលក់ឲ្យអតិថិជនដែលពុំទទួលថវិកាឧបត្ថម្ភធន (ឧស្សាហកម្ម ពាណិជ្ជកម្ម អាជីវកម្ម)</td>
                                </tr>
                            </tbody>
                            {
                                this.props.getAllReportSubsidySaleMV.status === 200 ?
                                    <tbody>
                                    {
                                        this.props.getAllReportSubsidySaleMV.data.items.map((element, index) => {
                                            return(
                                                <tr key={index} style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                                    <td colSpan={2}>{index+1 + ". "}{element.saleTypeKh}</td>
                                                    <td>{element.customers}</td>
                                                    <td>{element.power.toFixed(2)}</td>
                                                    <td>{this.formatStrings(element.rate)}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                    </tbody>
                                    :
                                    <tbody>
                                    <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                        <td colSpan={2}>1.លក់លើ​ MV នាឡិកាស្ទង់ MV</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                        <td colSpan={2}>2.លក់លើ​ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ(វិនិយោគដោយអ្នកទិញ)</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                        <td colSpan={2}>3.លក់លើ MV នាឡិកាស្ទង់ LV ក្រោមនៃត្រង់ស្វូ(វិនិយោគដោយអ្នកលក់)</td>
                                        <td>0</td>
                                        <td>0</td>
                                        <td>0</td>
                                    </tr>
                                    </tbody>
                            }
                            <tbody>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={2}>4.អតិថិជនអាជីវកម្មប្រើធំជាង 2000 kwh/ខែ</td>
                                    <td>{bus2001kwUp.customers}</td>
                                    <td>{bus2001kwUp.powers.toFixed(2)}</td>
                                    <td>{bus2001kwUp.rate}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <td colSpan ={5} style={{fontFamily: "Khmer OS Battambang", fontSize: "11pt"}}>III.ថាមពលលក់ឲ្យអតិថិជនដែលត្រូវទទួលថវិកាឧបត្ថម្ភធនប៉ះប៉ូវ (លក់លើតង់ស្យុងទាប ភ្ជាប់ពីខ្សែសាធារណះ)</td>
                                </tr>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={2}>1-ថាមពលលក់ឲ្យអតិថិជន អាជីវកម្ម ប្រើក្រោម 2001 kWh/ខែ</td>
                                    <td>{bus0to2000kw.customers}</td>
                                    <td>{bus0to2000kw.powers.toFixed(2)}</td>
                                    <td>{bus0to2000kw.rate}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={5}>2.អតិថិជនធម្មតា</td>
                                </tr>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={2}>3.លក់ឱ្យលំនៅដ្ឋានប្រើតិចជាង ១០ kwh/ខែ</td>
                                    <td>{normal0to10kw.customers}</td>
                                    <td>{normal0to10kw.powers.toFixed(2)}</td>
                                    <td>{normal0to10kw.rate}</td>
                                </tr>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={2}>4.លក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី ១១​ ដល់ ៥0 kwh/ខែ</td>
                                    <td>{normal11to50kw.customers}</td>
                                    <td>{normal11to50kw.powers.toFixed(2)}</td>
                                    <td>{normal11to50kw.rate}</td>
                                </tr>
                                <tr style={{fontFamily: "Khmer OS Battambang", fontSize: "10pt"}}>
                                    <td colSpan={2}>5.លក់ឱ្យលំនៅដ្ឋានប្រើធំជាង ៥០ kwh/ខែ</td>
                                    <td>{normal51kwUp.customers}</td>
                                    <td>{normal51kwUp.powers.toFixed(2)}</td>
                                    <td>{normal51kwUp.rate}</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

Page2 = reduxForm({
    form: 'form_report_subsidy_kw_using_page2',
    validate: (values) => {
        const errors = {};
        let regex_year = /^\d{4}$/;
        if (!regex_year.test(values.year) || values.year === undefined) {
            errors.year = 'Invalid year!';
        }
        if(values.month === undefined){
            errors.month = "Invalid month!";
        }
        return errors;
    }
})(Page2);

function mapStateToProps(state) {
    //console.log("DATA : ", state.reportSubsidySaleLV.getReportSubsidySaleLV0to2000kw);
    return {
        // sale MV
        getAllReportSubsidySaleMVLicense: state.reportSubsidySaleMV.getAllReportSubsidySaleMVLicense,
        getAllReportSubsidySaleMV: state.reportSubsidySaleMV.getAllReportSubsidySaleMV,
        getAllReportSubsidySaleLVKW2001UP: state.reportSubsidySaleMV.getAllReportSubsidySaleLVKW2001UP,
        // sale LV
        getReportSubsidySaleLV0to2000kw: state.reportSubsidySaleLV.getReportSubsidySaleLV0to2000kw,
        getReportSubsidySaleLV0to10kw: state.reportSubsidySaleLV.getReportSubsidySaleLV0to10kw,
        getReportSubsidySaleLV11to50kw: state.reportSubsidySaleLV.getReportSubsidySaleLV11to50kw,
        getReportSubsidySaleLV51kwUp: state.reportSubsidySaleLV.getReportSubsidySaleLV51kwUp
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        // sale MV
        getReportSubsidySaleMVLicenseAction,
        getReportSubsidySaleMVAction,
        getReportSubsidySaleLVKw2001UPAction,
        // sale LV
        getReportSubsidySaleLV0to2000kwAction,
        getReportSubsidySaleLV0to10kwAction,
        getReportSubsidySaleLV11to50kwAction,
        getReportSubsidySaleLV51kwUpAction
    },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Page2);