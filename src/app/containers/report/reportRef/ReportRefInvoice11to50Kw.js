import React from 'react';
import {connect} from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Row, Col, Table, Panel, Button } from 'react-bootstrap';
import SelectBox from '../../../components/forms/SelectBox';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import accounting from 'accounting';
import { getAllReportRefPrepaid11to50KwAction, getAllReportRefPostpaid11to50KwAction  } from '../../../actions/report/reportRef';
import ReactLoading from 'react-loading';

let totalRiel = 0;
let totalUSD = 0;
let prepaidRiel = 0;
let prepaidUSD = 0;
let postKw = 0;
let preKw = 0;
class ReportRefInvoice11to50Kw extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            date : "",
            months :[
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ]
        };
    }

    componentWillReceiveProps(data){
        if(data.getAllReportRefPostpaid11to50Kw.status === 200 || data.getAllReportRefPostpaid11to50Kw.status === 404){
            totalRiel = 0; totalUSD = 0;
            prepaidRiel = 0; prepaidUSD =0;
            preKw = 0; postKw = 0;
            document.getElementById('loading').style.display = 'none';
        }
        if(data.getAllReportRefPrepaid11to50Kw.status === 200 || data.getAllReportRefPrepaid11to50Kw.status === 404){
            totalRiel = 0; totalUSD = 0;
            prepaidRiel = 0; prepaidUSD =0;
            preKw = 0; postKw = 0;
            document.getElementById('loading').style.display = 'none';
        }
    }

    handleSubmit(values){
        document.getElementById('loading').style.display = 'block';
        totalRiel = 0; totalUSD = 0;
        prepaidRiel = 0; prepaidUSD =0;
        preKw = 0; postKw = 0;
        this.setState({date : values.month+"-"+values.year});
        this.props.getAllReportRefPrepaid11to50KwAction({
            'startDate': values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'startUnit' : 11,
            'endUnit': 50
        });
        this.props.getAllReportRefPostpaid11to50KwAction({
            'startDate': values.year + "-" + values.month + "-01",
            'endDate' : values.year + "-" + values.month + "-28",
            'startUnit' : 11,
            'endUnit': 50
        });
    }

    /* helper functions */
    getMeterSerial(str){
        let items = JSON.parse(str);
        if(items.length === 1){
            return items[0];
        }else if(items.length === 2){
            return items[0]+","+items[1];
        }else {
            return items[0]+","+items[1]+","+items[2];
        }
    }
    getCoefficient(str){
        return str.replace("[","").replace("]","");
    }
    getUsageRecord(str){
        return str.replace("[[","").replace("]]","");
    }
    getCurrency(riel){
        let amountRiel = Number(riel);
        if(amountRiel > 0){
            return "RIEL";
        }else{
            return "USD";
        }
    }
    getPrice(riel,usd){
        let strRiel = riel.replace("[[","").replace("]]","");
        let strUSD = usd.replace("[[","").replace("]]","");
        let rateRiel = strRiel.split(",");
        let rateUSD = strUSD.split(",");
        if(rateRiel[0] > 0){
            return rateRiel[0];
        }else if(rateUSD[0] > 0){
            return rateUSD[0];
        }
        // let rateRiel = JSON.parse(riel);
        // let rateUSD = JSON.parse(usd);
        // if(rateRiel.length > 0){
        //     if(rateRiel[0] > 0){ // riel
        //         return rateRiel[0];
        //     }else { // usd
        //         return rateUSD[0];
        //     }
        // }
    }

    formatAmount(riel,usd){
        let amountRiel = Number(riel);
        let amountUsd = Number(usd);
        if( amountRiel > 0){
            totalRiel += amountRiel;
            return Number(riel);
        }else if(amountUsd > 0){
            totalUSD += amountUsd;
            return Number(usd);
        }
        // let amountRiel = JSON.parse(riel);
        // let amountUSD = JSON.parse(usd);
        // if(amountRiel.length > 0){
        //     if(amountRiel[0] > 0){ // riel
        //         if(amountRiel.length === 1){
        //             totalRiel = totalRiel + (amountRiel[0]);
        //         }else if(amountRiel.length === 2){
        //             totalRiel = totalRiel + (amountRiel[0 ]+ amountRiel[1]);
        //         }else {
        //             totalRiel = totalRiel + (amountRiel[0]+ amountRiel[1] + amountRiel[2]);
        //         }
        //         return amountRiel[0];
        //     }else { // usd
        //         if(amountUSD.length === 1){
        //             totalUSD = totalUSD + (amountUSD[0]);
        //         }else if(amountUSD.length === 2){
        //             totalUSD = totalUSD + (amountUSD[0 ]+ amountUSD[1]);
        //         }else {
        //             totalUSD = totalUSD + (amountUSD[0]+ amountUSD[1] + amountUSD[2]);
        //         }
        //         return amountUSD[0];
        //     }
        // }
    }

    handleRefresh(){
        location.href = "/app/reports/ref/report-11-50-kw";
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div style={{margin: '25px'}}>
                <Row>
                    <Col md={8} lg={8} className="pull-right">
                        <Panel header={"Report ref invoices 11 to 50 kwh for " + this.state.date} bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field
                                            name="month"
                                            type="select"
                                            component={SelectBox}
                                            placeholder="Select Month"
                                            values={this.state.months} />
                                    </Col>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field name="year" type="text" component={TextBox} label="Year"/>
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <div>
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="table-to-xls"
                                filename="reportrefinvoice_11to50kwh"
                                sheet="tablexls"
                                buttonText="Export to XLS"/>
                            <div id="loading" style={{display: 'none', marginTop: 20}}>
                                <ReactLoading type="spokes" color="#888888" className="margin-auto" />
                            </div>
                            <Table id="table-to-xls" striped bordered condensed className="prepaid-res-report">
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <h2 style={{textAlign: 'center', fontFamily: "Kh Muol", fontSize: "13pt"}}>សាខាក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឹលូសិន ខបភើរេសិន</h2>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <p style={{textAlign: 'center', fontFamily: "Kh Siemreap", fontSize: "9pt"}}>ខេត្តព្រៃវែង</p>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <h4 style={{textAlign: 'center', fontFamily: "Khmer OS Muol", fontSize: "10pt"}}>របាយការណ៍ លំអិតការចេញវិក្កយបត្រប្រចាំខែ {this.state.date}</h4>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <p style={{textAlign: 'center', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt"}}>អតិថិជន៖គ្រប់អតិថិជន &nbsp;&nbsp;&nbsp;&nbsp; ការភ្ជាប់ចរន្ត៖ គ្រប់ប្រភេទភ្ជាប់ចរន្ត</p>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <p style={{textAlign: 'center', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt" }}>ថាមពល៖ 11 - 50 kWh &nbsp;&nbsp;&nbsp;&nbsp; រូបបិយប័ណ្ណ៖គ្រប់រូបិយប័ណ្ណ </p>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={13}>
                                        <p style={{textAlign: 'center', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt"}}>ជុំទូទាត់៖ គ្រប់ជុំការទូទាត់  &nbsp;&nbsp;&nbsp;&nbsp; តំបន់៖ គ្រប់តំបន់</p>
                                    </th>
                                </tr>
                                </thead>
                                <thead hidden><tr><th className="bg-none" colSpan={13}><br/></th></tr></thead>
                                <thead>
                                <tr>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ល.រ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>កូដ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ឈ្មោះអតិថិជន</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>អំពែ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ប្រអប់​</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>កុងទ័រ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>តំលៃ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ចាស់</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ថ្មី</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>មេគុណ</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ថាមពល</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ទឹកប្រាក់</th>
                                    <th style={{fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>រូបិយប័ណ្ណ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colSpan ={13} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt"}}>ថាមពលលក់ឲ្យអតិថិជនលំនៅដ្ឋាន</td>
                                </tr>
                                <tr>
                                    <td colSpan ={13} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt"}}>ថាមពលលក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 kWh/ខែ</td>
                                </tr>
                                {
                                    this.props.getAllReportRefPrepaid11to50Kw.status === 200 ?
                                        this.props.getAllReportRefPrepaid11to50Kw.data.items.map((customer, index) => {
                                            preKw = preKw + customer.unit;
                                            if(customer.currency === "RIEL"){
                                                prepaidRiel = prepaidRiel + customer.usingAmount;
                                            }else{
                                                prepaidUSD = prepaidUSD + customer.usingAmount;
                                            }
                                            return (
                                                <tr key={index}>
                                                    <td style={{width:'50px'}}>{index+1}</td>
                                                    <td style={{width:'100px'}}>{customer.customerId}</td>
                                                    <td style={{width:'100px'}}>{customer.customerName}</td>
                                                    <td style={{width:'80px'}}>{customer.ampere}</td>
                                                    <td style={{width:'80px'}}>{customer.box}</td>
                                                    <td style={{width:'80px'}}>{customer.meterSerial}</td>
                                                    <td style={{width:'80px'}}>{customer.price}</td>
                                                    <td style={{width:'80px'}}>{customer.oldUsage}</td>
                                                    <td style={{width:'80px'}}>{customer.newUsage}</td>
                                                    <td style={{width:'80px'}}>{customer.coefficient}</td>
                                                    <td style={{width:'80px'}}>{customer.unit}</td>
                                                    <td style={{width:'80px'}}>{customer.usingAmount}</td>
                                                    <td style={{width:'80px'}}>{customer.currency}</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        null
                                }
                                {
                                    this.props.getAllReportRefPostpaid11to50Kw.status === 200 ?
                                        this.props.getAllReportRefPostpaid11to50Kw.data.items.map((customer, index) => {
                                            postKw = postKw + Number(customer.unit);
                                        return (
                                                <tr key={index}>
                                                    <td style={{width:'50px'}}>{index+1}</td>
                                                    <td style={{width:'100px'}}>{customer.customerId}</td>
                                                    <td style={{width:'100px'}}>{customer.nameKh}</td>
                                                    <td style={{width:'80px'}}>{customer.ampere}</td>
                                                    <td style={{width:'80px'}}>{customer.box}</td>
                                                    <td style={{width:'80px'}}>{this.getMeterSerial(customer.meterSerial)}</td>
                                                    <td style={{width:'80px'}}>{this.getPrice(customer.rateRiel,customer.rateUSD)}</td>
                                                    <td style={{width:'80px'}}>{this.getUsageRecord(customer.lastUsageRecord)}</td>
                                                    <td style={{width:'80px'}}>{this.getUsageRecord(customer.newUsageRecord)}</td>
                                                    <td style={{width:'80px'}}>{this.getCoefficient(customer.coefficient)}</td>
                                                    <td style={{width:'80px'}}>{customer.unit}</td>
                                                    <td style={{width:'80px'}}>{this.formatAmount(customer.amountRiel,customer.amountUSD)}</td>
                                                    <td style={{width:'80px'}}>{this.getCurrency(customer.amountRiel)}</td>
                                                </tr>
                                            )
                                        })
                                        :
                                        null
                                }
                                <tr className="total">
                                    <td colSpan ={10} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD"}}>សរុប៖</td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}>{preKw+postKw} </td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}>
                                        {accounting.formatNumber(totalRiel+prepaidRiel)}
                                    </td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}>RIEL</td>
                                </tr>
                                <tr className="total">
                                    <td colSpan ={10} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD"}}></td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}></td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}>
                                        {accounting.formatNumber(totalUSD+prepaidUSD)}
                                    </td>
                                    <td style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt", backgroundColor: "#BDBDBD", textAlign: "center"}}>USD</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

ReportRefInvoice11to50Kw = reduxForm({
    form: 'form_report_ref_11_to_50_kw',
    validate: (values) => {
        const errors = {};
        let regex_year = /^\d{4}$/;
        if (!regex_year.test(values.year) || values.year === undefined) {
            errors.year = 'Invalid year!';
        }
        if(values.month === undefined){
            errors.month = "Invalid month!";
        }
        return errors;
    }
})(ReportRefInvoice11to50Kw);

function mapStateToProps(state) {
    // console.log("post paid :", state.reportRef.getAllReportRefPostpaid11to50Kw);
    // console.log("pre paid :", state.reportRef.getAllReportRefPrepaid11to50Kw);
    return {
        getAllReportRefPostpaid11to50Kw: state.reportRef.getAllReportRefPostpaid11to50Kw,
        getAllReportRefPrepaid11to50Kw: state.reportRef.getAllReportRefPrepaid11to50Kw
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllReportRefPrepaid11to50KwAction, getAllReportRefPostpaid11to50KwAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ReportRefInvoice11to50Kw);