import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../../auth';
import ReportRefInvoice0Kw from '../../../containers/report/reportRef/ReportRefInvoice0Kw';
import ReportRefInvoice1to10Kw from '../../../containers/report/reportRef/ReportRefInvoice1to10Kw';
import ReportRefInvoice11to50Kw from '../../../containers/report/reportRef/ReportRefInvoice11to50Kw';
import ReportRefInvoice_50_Kw from '../../../containers/report/reportRef/ReportRefInvoice_50_Kw';
import ReportRefHistory12Month from '../../../containers/report/reportRef/ReportRefHistory12Month';
import SubsidyIndex from '../../../containers/report/reportRef/subsidy';

export const ref = (
    <Route path="ref">
        <Route path="report-0-kw" component={ReportRefInvoice0Kw}/>
        <Route path="report-1-10-kw" component={ReportRefInvoice1to10Kw}/>
        <Route path="report-11-50-kw" component={ReportRefInvoice11to50Kw}/>
        <Route path="report-50-kw" component={ReportRefInvoice_50_Kw}/>
        <Route path="report-history-12-month" component={ReportRefHistory12Month}/>
        <Route path="subsidy" component={SubsidyIndex}/>
    </Route>
);