import React from 'react';
import Page1 from '../reportRef/page-1';
import Page2 from '../reportRef/Page2';
import Page3 from '../../../containers/report/prepaid/AdditionSubsidy';
//import './index.css';
import './../index.css';

class SubsidyIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
            <div>
                <div className="col-md-12 col-lg-12">
                    <div className="panel with-nav-tabs panel-default subsidy">
                        <ul className="nav nav-tabs search-tab" role="tablist">
                            <li role="presentation" className="active"><a href="#tab1default" aria-controls="customer-info" role="tab" data-toggle="tab">Table for Purchase and Generation of Electricity</a></li>
                            <li role="presentation"><a href="#tab2default" aria-controls="network" role="tab" data-toggle="tab">Table for Total Sale</a></li>
                            <li role="presentation"><a href="#tab3default" aria-controls="finance" role="tab" data-toggle="tab">Table for Calculation of Subsidy Amount</a></li>
                        </ul>
                        <div className="panel-body subsidy">
                            <div className="tab-content">
                                <div className="tab-pane fade in active" id="tab1default"><Page1/></div>
                                <div className="tab-pane fade" id="tab2default"><Page2/></div>
                                <div className="tab-pane fade" id="tab3default"><Page3/></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SubsidyIndex;
