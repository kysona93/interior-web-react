import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { Field, reduxForm } from 'redux-form';
import { TextBox } from '../../../components/forms/TextBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import '../report-res-prepaid.css';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getHistory12MonthsAction, getHistory12MonthsBusinessAction } from '../../../actions/report/reportRef';
import { get12Months, combineArrayObj } from '../../../utils/12Months';
import { loadLocation } from '../../../localstorages/localstorage';
import { authObj } from '../../../../auth';
import { listSettingAction } from './../../../actions/setup/setting';
import { getSetting } from './../../../utils/Setting';
import moment from 'moment';

let report = {
    consumption: 1,
    id: 0,
    date: ""
};
let business = {
    consumption: 2,
    id: 0,
    date: ""
};
class ReportRefHistory12Month extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            filterDate: moment()
        };
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleFilterDate(date) {
        this.setState({filterDate: date});
    }

    componentWillMount(){
        this.props.listSettingAction();
        this.props.getHistory12MonthsAction(report);
        this.props.getHistory12MonthsBusinessAction(business);
    }
    componentWillReceiveProps(data){
        if(data.history12Months.items !== undefined){
            this.setState({loading: 'none'});
        } else if(data.history12Months.status === (200 && 404)){
            this.setState({loading: 'none'});
        }

        let licenseName = "";
        let address = "";
        if(data.settings.items !== undefined){
            licenseName = getSetting(data.settings.items, "LICENSE_NAME").value;
            address = getSetting(data.settings.items, "LOCATION").value;
        }
        this.setState({licenseName: licenseName, address: address});
    }

    handleSearch(value){
        report.id = Number(value.customerId) || 0;
        report.date = value.filterDate || "";
        business.id = Number(value.customerId) || 0;
        business.date = value.filterDate || "";
        this.props.getHistory12MonthsAction(report);
        this.props.getHistory12MonthsBusinessAction(business);
    }

    static formatString(str){
        const items = JSON.parse(str);
        if(items !== null && items !== undefined){
            if(items.length === 1){
                return items[0];
            } else if(items.length === 2){
                return items[0]+"/"+items[1];
            } else {
                return items[0]+"/"+items[1]+"/"+items[2];
            }
        }
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let maxPre = 0;
        let maxPost = 0;
        let minPre = 0;
        let minPost = 0;
        let dates = [];
        get12Months(this.state.filterDate.toString()).map((d) => {
            dates.push(new Date('01-'.concat(d.date)).getTime())
        });
        const history12Months = this.props.history12Months.items;
        const history12MonthsBusiness = this.props.history12MonthsBusiness.items;
        const listHistories = (list, id) => {
            return list.filter(h => h.locationId === id);
        };
        const preyveng = loadLocation().find(location => location.name === "Prey Veng" || location.nameKh === "ព្រៃវែង" || location.id === 15);
        const locations = loadLocation().filter(l => l.province === (preyveng.province || 14) && l.typeId === 5);
        return(
            <div className="container-fluid">
                <br />
                <h3 style={{marginTop: '-2%', textAlign: 'center', textDecoration: 'underline'}}>
                    REPORT RAF HISTORY 12 MONTHS FROM <strong style={{color: '#1c91d0'}}>{moment(dates.sort()[0]).format("MM-YYYY")}</strong> to <strong style={{color: '#1c91d0'}}>{moment(dates.sort()[11]).format("MM-YYYY")}</strong>
                </h3>
                <br />
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <form onSubmit={handleSubmit(this.handleSearch)} className="wrap-full-form search-installer">
                            <Row>
                                <Col lg={10}>
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                            <tr>
                                                <td><p className="search-installer">ID</p></td>
                                                <td><Field name="customerId" type="text" component={TextBox} label="Customer ID"/></td>
                                                <td><p className="search-installer">DATE</p></td>
                                                <td>
                                                    <Field name="filterDate" component={DateTimePicker} placeholder="From Date"
                                                           defaultDate={this.state.filterDate} handleChange={this.handleFilterDate.bind(this)} />
                                                </td>
                                                <td><ButtonSubmit className="search-installer" error={error} invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                                <td><button style={{color: '#fff', height: '32px', border: '0', borderRadius: 'inherit', background: '#1c91d0'}}>Refresh</button></td>
                                                <td>
                                                    <ReactHTMLTableToExcel
                                                        id="button_report_ref_12_months"
                                                        className="download-table-xls-button"
                                                        table="report_ref_12_months"
                                                        filename="report_raf_12_months"
                                                        sheet="report_ref_12_months"
                                                        buttonText="Export to Excel"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                    <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                    <span className="sr-only">Loading...</span>
                </div>
                {history12Months !== undefined || history12MonthsBusiness !== undefined ?
                    <Row>
                        <Col xs={12} sm={12} md={12} lg={12}>
                            <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="report_ref_12_months" striped bordered condensed className="prepaid-res-report">
                                <thead style={{display: 'none', fontFamily: "Khmer OS", fontSize: "12pt"}}>
                                    <tr>
                                        <th colSpan={19}><h2 style={{textAlign: 'center'}}>{this.state.licenseName}</h2></th>
                                    </tr>
                                    <tr>
                                        <th colSpan={19}><p style={{textAlign: 'center'}}>{this.state.address}</p></th>
                                    </tr>
                                    <tr>
                                        <th colSpan={19}><h4 style={{textAlign: 'center'}}>របាយការណ៍ការប្រវត្តិការប្រើប្រាស់ ថាមពលអគ្គិសនីរបស់អតិថិជន ចាប់ពីខែ {moment(dates.sort()[0]).format("MM-YYYY")} ដល់ខែ {moment(dates.sort()[11]).format("MM-YYYY")}</h4></th>
                                    </tr>
                                    <tr>
                                        <th colSpan={19}><p style={{textAlign: 'center'}}>តំបន់ ​​   ៖     គ្រប់តំបន់         តំលៃ​     ៖​​  គ្រប់តំលៃ</p></th>
                                    </tr>
                                    <tr>
                                        <th colSpan={19}><p style={{textAlign: 'center'}}>ប្រភេទ ៖ ​    គ្រប់ប្រភេទ      ជុំទូទាត់​​​ ៖  គ្រប់ជុំការទូទាត់</p></th>
                                    </tr>
                                    <tr>
                                        <th colSpan={19}> </th>
                                    </tr>
                                </thead>
                                <thead>
                                <tr style={{border:'1.5pt solid white', fontFamily: "Khmer OS", fontSize: "11pt", background: "#afafaf"}}>
                                    <th>ល.រ</th>
                                    <th>កូដ</th>
                                    <th>ឈ្មោះអតិថិជន</th>
                                    <th>បង្គោល</th>
                                    <th>ប្រអប់​</th>
                                    <th>កុងទ័រ</th>
                                    <th>អំពែរ</th>
                                    {dates.sort().map((date, index) => {
                                        return(
                                            <th key={index}>{moment(date).format("MM-YYYY")}</th>
                                        )
                                    })}
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colSpan ={19}><strong>លក់អោយអតិថិជន MV (ឧស្សាហកម្ម)</strong></td>
                                    </tr>
                                    { history12MonthsBusiness !== undefined ?
                                        listHistories(history12Months, location.id).map((history, ind) => {
                                            return(
                                                <tr style={{textAlign: 'center'}} key={ind}>
                                                    <td>{ind + 1}</td>
                                                    <td>{history.id}</td>
                                                    <td>{history.nameKh}</td>
                                                    <td>{history.poleSerial}</td>
                                                    <td>{history.boxSerial}</td>
                                                    <td>{ReportRefHistory12Month.formatString(history.meterSerial)}</td>
                                                    <td>{history.ampere}</td>
                                                    {JSON.parse(history.prepaid) !== null ?
                                                        combineArrayObj(get12Months(), JSON.parse(history.prepaid)).sort(function(a, b) {
                                                            return new Date('01-'.concat(a.date)).getTime() - new Date('01-'.concat(b.date)).getTime();
                                                        }).map((d, i) => {
                                                            maxPre = JSON.parse(history.prepaid)[0].power;
                                                            if((maxPre - d.power) < -20 || (maxPre - d.power) > 20){
                                                                return(
                                                                    <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                )
                                                            } else {
                                                                return(
                                                                    <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                )
                                                            }
                                                        })
                                                        :
                                                        JSON.parse(history.postpaid) !== null ?
                                                            combineArrayObj(get12Months(), JSON.parse(history.postpaid)).sort(function(a, b) {
                                                                return new Date('01-'.concat(a.date)).getTime() - new Date('01-'.concat(b.date)).getTime();
                                                            }).map((d, i) => {
                                                                maxPost = JSON.parse(history.postpaid)[0].power;
                                                                if((maxPost - d.power) < -20 || (maxPost - d.power) > 20){
                                                                    return(
                                                                        <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                    )
                                                                } else {
                                                                    return(
                                                                        <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                    )
                                                                }
                                                            })
                                                            : <td colSpan={19}><h4 style={{textAlign: 'center'}}>NO RECORD</h4></td>
                                                    }
                                                </tr>
                                            )
                                        })
                                        :
                                        <tr><td colSpan={19}> </td></tr>
                                    }
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td colSpan ={19}><strong>លក់អោយអតិថិជន តង់ស្យុងទាប (លំនៅដ្ឋាន )</strong></td>
                                    </tr>
                                </tbody>
                                { locations !== undefined ?
                                    locations.map((location, index) => {
                                        return(
                                                listHistories(history12Months, location.id).length > 0 ?
                                                    <tbody key={index}>
                                                        <tr>
                                                            <td colSpan ={2}><strong>តំបន់ ៖ {location.nameKh || location.name}</strong></td>
                                                        </tr>
                                                        { listHistories(history12Months, location.id).map((history, ind) => {
                                                            return(
                                                                <tr style={{textAlign: 'center'}} key={ind}>
                                                                    <td>{ind + 1}</td>
                                                                    <td>{history.id}</td>
                                                                    <td>{history.nameKh}</td>
                                                                    <td>{history.poleSerial}</td>
                                                                    <td>{history.boxSerial}</td>
                                                                    <td>{ReportRefHistory12Month.formatString(history.meterSerial)}</td>
                                                                    <td>{history.ampere}</td>
                                                                    {JSON.parse(history.prepaid) !== null ?
                                                                        combineArrayObj(get12Months(), JSON.parse(history.prepaid)).sort(function(a, b) {
                                                                            return new Date('01-'.concat(a.date)).getTime() - new Date('01-'.concat(b.date)).getTime();
                                                                        }).map((d, i) => {
                                                                            minPre = JSON.parse(history.prepaid)[0].power;
                                                                            if((minPre - d.power) < -20 || (minPre - d.power) > 20){
                                                                                return(
                                                                                    <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                                )
                                                                            } else {
                                                                                return(
                                                                                    <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                                )
                                                                            }
                                                                        })
                                                                        :
                                                                        JSON.parse(history.postpaid) !== null ?
                                                                            combineArrayObj(get12Months(), JSON.parse(history.postpaid)).sort(function(a, b) {
                                                                                return new Date('01-'.concat(a.date)).getTime() - new Date('01-'.concat(b.date)).getTime();
                                                                            }).map((d, i) => {
                                                                                minPost = JSON.parse(history.postpaid)[0].power;
                                                                                if((minPost - d.power) < -20 || (minPost - d.power) > 20){
                                                                                    return(
                                                                                        <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                                    )
                                                                                } else {
                                                                                    return(
                                                                                        <td key={i} style={{background: 'yellow'}}>{d.power}</td>
                                                                                    )
                                                                                }
                                                                            })
                                                                            : <td colSpan={19}><h4 style={{textAlign: 'center'}}>NO RECORD</h4></td>
                                                                    }
                                                                </tr>
                                                            )
                                                        })}
                                                    </tbody>
                                                : null
                                        )
                                    })
                                    : <tbody><tr><td colSpan={19}><h4 style={{textAlign: 'center'}}>NO RECORD</h4></td></tr></tbody>
                                }
                                <tbody>
                                    <tr>
                                        <td colSpan={3}>
                                            <strong style={{textAlign: 'center'}}>អតិថិជនសរុបចំនួន  {history12Months !== undefined ? history12Months.length : 0}  នាក់   </strong>
                                        </td>
                                    </tr>
                                    <tr><td colSpan={19}> </td></tr>
                                    <tr style={{textAlign: 'center'}}>
                                        <td colSpan={9}>
                                            <strong>បោះពុម្ភដោយ ៖ {authObj().sub.toUpperCase()}</strong>
                                        </td>
                                        <td colSpan={10}>
                                            <strong>កាលបរិច្ឆេទបោះពុម្ភ ៖ {moment().format("DD-MM-YY")}</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Col>
                    </Row>
                    :
                    null
                }
            </div>
        )
    }
}

ReportRefHistory12Month = reduxForm({
    form: 'form_report_ref_history_12_months',
    validate: (values) => {
        const number = /^\d+$/;
        const errors = {};
        if(values.customerId !== undefined){
            if(!number.test(values.customerId)){
                errors.customerId = 'Please input number only !!';
            }
        }
        return errors;
    },
})(ReportRefHistory12Month);

function mapStateToProps(state){
    //console.log(JSON.stringify(state.reportRef.history12Months));
    return{
        settings: state.setting.listSetting,
        history12Months: state.reportRef.history12Months,
        history12MonthsBusiness: state.reportRef.history12MonthsBusiness
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({listSettingAction, getHistory12MonthsAction, getHistory12MonthsBusinessAction},dispatch)
}
export default connect(mapStateToProps, mapDispatchToProps) (ReportRefHistory12Month);