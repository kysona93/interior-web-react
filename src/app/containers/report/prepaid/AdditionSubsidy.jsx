import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
import { Field ,reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { DateTimePicker }  from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAdditionSubsidiesAction } from '../../../actions/report/reportRef';
import { listSettingAction } from './../../../actions/setup/setting';
import { getSetting } from './../../../utils/Setting';
import moment from 'moment';
import '../report-res-prepaid.css';

class AdditionSubsidy extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            filterDate: moment()
        };
        this.handleSearch = this.handleSearch.bind(this);
    }
    handleFilterDate(date) {
        this.setState({filterDate: date});
    }
    componentWillMount(){
        this.props.listSettingAction();
        this.props.getAdditionSubsidiesAction({
            consumption: 1,
            date: ""
        });
    }

    componentWillReceiveProps(data){
        if(data.additionSubsidies.status === (200 && 404)){
            this.setState({loading: 'none'});
        }
        if(data.additionSubsidies.items !== undefined){
            const subsidies = data.additionSubsidies.items;
            let power10 = 0;
            let power50 = 0;
            let power2000 = 0;
            let count10 = 0;
            let count50 = 0;
            let count2000 = 0;

            let licenseName  = "";
            let licenseNumber = "";
            let tariff = 0;
            let max = 0;
            let normal = 0;
            let min = 0;
            if(data.settings.items !== undefined){
                licenseName = getSetting(data.settings.items, "LICENSE_NAME").value;
                licenseNumber = getSetting(data.settings.items, "LICENSE_NUMBER").value;
                tariff = Number(getSetting(data.settings.items, "BASE_TARIFF").value);
                max = Number(getSetting(data.settings.items, "MAX_KWH_PRICE").value);
                normal = Number(getSetting(data.settings.items, "NORMAL_KWH_PRICE").value);
                min = Number(getSetting(data.settings.items, "MIN_KWH_PRICE").value);
            }

            for(let sub of subsidies){
                if(sub.power <= 10){
                    power10 += sub.power;
                    count10 += 1;
                } else if(sub.power <= 50){
                    power50 += sub.power;
                    count50 += 1;
                } else if(sub.power <= 2000){
                    power2000 += sub.power;
                    count2000 += 1;
                }
            }
            this.setState({
                subsidies10: {
                    max: max,
                    normal: normal,
                    min: min,
                    tariff: tariff,
                    count: count10,
                    power: power10,
                    rate: max - min,
                    amount: (max - min) * power10
                },
                subsidies50: {
                    max: max,
                    normal: normal,
                    min: min,
                    tariff: tariff,
                    count: count50,
                    power: power50,
                    rate: max - normal,
                    amount: (max - normal) * power50
                },
                subsidies2000: {
                    max: max,
                    normal: normal,
                    min: min,
                    tariff: tariff,
                    count: count2000,
                    power: power2000,
                    rate: tariff - max,
                    amount: (tariff - max) * power2000
                },
                licenseName: licenseName,
                licenseNumber: licenseNumber
            })
        }
    }

    handleSearch(value){
        this.props.getAdditionSubsidiesAction({
            consumption: 1,
            date: value.filterDate || ""
        });
    }

    render(){
        const {handleSubmit, invalid, error, submitting} = this.props;
        return(
            <div className="container-fluid">
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <form onSubmit={handleSubmit(this.handleSearch)} className="wrap-full-form search-installer">
                            <Row>
                                <Col md={7} lg={7} className="pull-right">
                                    <table className="table table-inverse search-installer">
                                        <tbody>
                                        <tr>
                                            <td><p className="search-installer">FILTER DATE</p></td>
                                            <td>
                                                <Field name="filterDate" component={DateTimePicker} placeholder="Filter Date"
                                                       defaultDate={this.state.filterDate} handleChange={this.handleFilterDate.bind(this)} />
                                            </td>
                                            <td><ButtonSubmit className="search-installer" error={error} invalid={invalid} submitting={submitting} label="SEARCH" /></td>
                                            <td>
                                                <ReactHTMLTableToExcel
                                                    id="button_report_ref_12_months"
                                                    className="download-table-xls-button"
                                                    table="report_ref_calculation_subsidy"
                                                    filename="calculation_subsidy"
                                                    sheet="calculation_subsidy"
                                                    buttonText="Export to Excel"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </Col>
                            </Row>
                        </form>
                    </Col>
                </Row>
                <div id="loading" style={{textAlign: 'center', display: this.state.loading, marginTop: 20}}>
                    <i className="fa fa-circle-o-notch fa-spin fa-5x fa-fw"> </i>
                    <span className="sr-only">Loading...</span>
                </div>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Table style={{fontFamily: "Kh Siemreap", fontSize: "10pt"}} id="report_ref_calculation_subsidy" striped bordered condensed className="prepaid-res-report">
                            <thead style={{display: 'none', fontFamily: "Kh Siemreap", fontSize: "12pt"}}>
                                <tr>
                                    <th><p style={{textAlign: 'left'}}>អាជ្ញាប័ណ្ណលេខ ៖</p></th>
                                    <th colSpan={6}><p style={{textAlign: 'left'}}>{this.state.licenseNumber}</p></th>
                                </tr>
                                <tr>
                                    <th><p style={{textAlign: 'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​ ៖</p></th>
                                    <th colSpan={6}><p style={{textAlign: 'left'}}>{this.state.licenseName}</p></th>
                                </tr>
                                <tr>
                                    <th><p style={{textAlign: 'left'}}>ទិន្នន័យសម្រាប់ខែ ៖</p></th>
                                    <th colSpan={6}><p style={{textAlign: 'left'}}>{moment(this.state.filterDate).format("MM-YYYY")}</p></th>
                                </tr>
                                <tr>
                                    <th colSpan={7}><h3>តារាងគណនាចំនួនប្រាក់ឧបត្ថម្ភ</h3></th>
                                </tr>
                                <tr>
                                    <th colSpan={7}><h3>Table for Calculation of subsidy amount</h3></th>
                                </tr>
                                <tr>
                                    <th colSpan={7}> </th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ល.រ</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ប្រភេទនៃការលក់<br/>(Type of Sale)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>សរុបចំនួនថាមពលលក់​<br/>(Total Sold kWh)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>អត្រាថ្លៃគោលកំណត់ដោយ អ.អ.ក<br/>(Cost based Tariff approved by EAC)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>អត្រាថ្លៃឧបត្ថម្ភកំណតដោយអ.អ.ក<br/>(Subsidizes Tariff of Sale approved by EAC)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>អត្រាថ្លៃឧបត្ថម្ភ<br/>(Tariff of Sale)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ចំនួនទឹកប្រាក់ឧបត្ថម្ភធន<br/>(Amount of Subsidy)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr style={{textAlign:'center',fontFamily:'Kh Siemreap'}}>
                                    <td >A</td>
                                    <td >B</td>
                                    <td >C</td>
                                    <td >D</td>
                                    <td >E</td>
                                    <td >F=D-E</td>
                                    <td >G=C*F</td>
                                </tr>
                                {this.state.subsidies2000 !== undefined ?
                                    <tr style={{textAlign: 'center', fontFamily: 'Kh Siemreap'}}>
                                        <td>1</td>
                                        <td>ថាមពលលក់លើតង់ស្យុងទាប</td>
                                        <td>{this.state.subsidies2000.power}</td>
                                        <td>{this.state.subsidies2000.tariff}</td>
                                        <td>{this.state.subsidies2000.max}</td>
                                        <td>{this.state.subsidies2000.rate}</td>
                                        <td>{this.state.subsidies2000.amount}</td>
                                    </tr>
                                    :
                                    <tr style={{textAlign: 'center', fontFamily: 'Kh Siemreap'}}>
                                        <td>1</td>
                                        <td>ថាមពលលក់លើតង់ស្យុងទាប</td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                        <td> </td>
                                    </tr>
                                }
                                <tr rowSpan={3}><td colSpan={7}> </td></tr>
                            </tbody>
                            {/*Table 2*/}
                            <thead>
                                <tr>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ល.រ</th>
                                    <th colSpan={2} style={{border:'1.5pt solid white', background: "#afafaf"}}>ប្រភេទនៃការលក់<br/>(Type of Sale)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ចំនួនអតិថិជន<br/>(No. of Consumers)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ចំនួនថាមពលលក់​<br/>(Total kWh Sold)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>អត្រាថ្លៃឧបត្ថម្ភបន្ថែម<br/>(Additional Subsidy)</th>
                                    <th style={{border:'1.5pt solid white', background: "#afafaf"}}>ចំនួនទឹកប្រាក់ឧបត្ថម្ភធន<br/>(Amount of Subsidy)</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr style={{textAlign:'center'}}>
                                <td >A</td>
                                <td colSpan={2}>B</td>
                                <td >C</td>
                                <td >D</td>
                                <td >E</td>
                                <td >F=D*E</td>
                            </tr>
                            { this.state.subsidies10 !== undefined ?
                                <tr style={{textAlign:'center'}}>
                                    <td >1</td>
                                    <td colSpan={2} style={{textAlign:'left'}}>លក់ឲ្យលំនៅដ្ឋានប្រើតិច​ជាង11Kwh/ខែ</td>
                                    <td >{this.state.subsidies10.count}</td>
                                    <td >{this.state.subsidies10.power}</td>
                                    <td >{this.state.subsidies10.rate}</td>
                                    <td >{this.state.subsidies10.amount}</td>
                                </tr>
                                :
                                <tr style={{textAlign:'center'}}>
                                    <td >1</td>
                                    <td colSpan={2} style={{textAlign:'left'}}>លក់ឲ្យលំនៅដ្ឋានប្រើតិច​ជាង11Kwh/ខែ</td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                </tr>
                            }
                            { this.state.subsidies50 !== undefined ?
                                <tr style={{textAlign:'center'}}>
                                    <td >2</td>
                                    <td colSpan={2} style={{textAlign:'left'}}>លក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 Kwh/ខែ</td>
                                    <td >{this.state.subsidies50.count}</td>
                                    <td >{this.state.subsidies50.power}</td>
                                    <td >{this.state.subsidies50.rate}</td>
                                    <td >{this.state.subsidies50.amount}</td>
                                </tr>
                                :
                                <tr style={{textAlign:'center'}}>
                                    <td >2</td>
                                    <td colSpan={2} style={{textAlign:'left'}}>លក់ឱ្យលំនៅដ្ឋានប្រើចាប់ពី 11 ដល់ 50 Kwh/ខែ</td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                </tr>
                            }
                                <tr style={{textAlign:'center'}}>
                                    <td >3</td>
                                    <td colSpan={2} style={{textAlign:'left'}}>លក់ឲ្យការបូមទឹកធ្វើកសិកម្មម៉ោង09:00យប់ ម៉ោង 07:00ព្រឹក</td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                    <td > </td>
                                </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}
AdditionSubsidy = reduxForm({
    form: 'form_addition_subsidy'
})(AdditionSubsidy);
function mapStateToProps(state){
    return{
        settings: state.setting.listSetting,
        additionSubsidies: state.reportRef.additionSubsidies
    }
}
function mapDispatchToProps(dispatch){
    return bindActionCreators({listSettingAction, getAdditionSubsidiesAction},dispatch);
}
export default connect(mapStateToProps, mapDispatchToProps)(AdditionSubsidy);