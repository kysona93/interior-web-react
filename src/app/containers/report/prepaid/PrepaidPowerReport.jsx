import React from 'react';
import {connect} from 'react-redux';
import { Link, browserHistory } from 'react-router';
import { Row, Col, Button, FormGroup, Label } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../../components/forms/TextBox';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getPrepaidPowerReportsAction } from './../../../actions/report/icCardDailySale';
import { getReturnInvoiceByPrepaidIdAction } from './../../../actions/transaction/prepaid/returnInvoice';
import ReactLoading from 'react-loading';
import accounting from 'accounting';
import moment from 'moment';
import './style.css';

function onAfterTableComplete() {
    //console.log('Table render complete.');
}
function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
let arrayList = [];
let filter = {
    sDate: '',
    eDate: '',
    cusId: '',
    prepaidId: '',
    cusName: '',
    meterNo: ''
};

class PrepaidPowerReport extends React.Component {
    constructor(props){
        super(props);
        this.state={
            disabled1: true,
            disabled2: true,
            disabled3: true,
            disabled4: true,
            startDate: null,
            endDate: null,
            isLoadingData: false
        };
        this.handleSearchBy = this.handleSearchBy.bind(this);
    }

    componentWillMount(){
        this.setState({
            startDate: moment(new Date()),
            endDate: moment(new Date())
        });
    }

    componentWillReceiveProps(newProps){
        /* get ic card filter */
        if(newProps.prepaidPowerReports.status === 200){
            this.setState({ isLoadingData: false });
            arrayList = [];
            let items = newProps.prepaidPowerReports.data.items;
            items.forEach((element) => {
                arrayList.push({
                    "userId": element.prepaidId,
                    "userName": element.cusNameEn,
                    "makeId": element.meterSerial,
                    "buyTimes": element.buyTimes,
                    "buyDate": moment(element.buyDate).format('YYYY-MM-DD'),
                    "buyPowers": element.buyPower,
                    "powerCost": accounting.formatNumber(element.powerCost),
                    "areaName": element.office,
                    "note": element.note,
                    "sellID": element.sellID,
                    "company": element.company,
                    "lastBalance": element.lastBalance,
                    "lastDateBalance": element.lastDateBalance,
                    isReturn: element.isReturn
                });
            });
            newProps.prepaidPowerReports.status = 0;
        }

        if(newProps.prepaidPowerReports.status === 404){
            this.setState({ isLoadingData: false });
            arrayList = [];

            if(this.state.startDate !== null && this.state.endDate !== null) {
                alert(`Sold date from ${moment(this.state.startDate).format('YYYY-MM-DD')} `
                        +`to ${moment(this.state.endDate).format('YYYY-MM-DD')} no data!`);
            }

            newProps.prepaidPowerReports.status = 0;
        }
    }

    handleRefresh() {
        arrayList = [];
        this.setState({
            startDate: moment(new Date()),
            endDate: moment(new Date()),
            disabled1: true,
            disabled2: true,
            disabled3: true,
        });
        if(this.props.prepaidPowerReports.status === 200)
            this.props.prepaidPowerReports.data.items = [];
        
        this.props.dispatch(initialize("form_prepaid_power_report", {}));
    }

    handleSubmit(values) {
        this.setState({ isLoadingData: true });

        if (values.cusId === undefined) filter.cusId = '';
        else filter.cusId = values.cusId;

        if (values.cusName === undefined) filter.cusName = '';
        else filter.cusName = values.cusName;

        if (values.prepaidId === undefined) filter.prepaidId = '';
        else filter.prepaidId = values.prepaidId;

        if (values.meterNo === undefined) filter.meterNo = '';
        else filter.meterNo = values.meterNo;

        if(this.state.startDate === null) filter.sDate = '';
        else filter.sDate = moment(this.state.startDate).format('YYYY-MM-DD');

        if(this.state.endDate === null) filter.eDate = '';
        else filter.eDate = moment(this.state.endDate).format('YYYY-MM-DD');

        this.props.getPrepaidPowerReportsAction(filter);
    }

    handleSearchBy(e) {
        this.setState({ 
            disabled1: true,
            disabled2: true,
            disabled3: true,
            disabled4: true
        });

        this.props.dispatch(initialize('form_prepaid_power_report', {}));
        
        if (e.target.value === '1')
            this.setState({ disabled1: false });
        else if (e.target.value === '2')
            this.setState({ disabled2: false });
        else if (e.target.value === '3')
            this.setState({ disabled3: false });
        else if (e.target.value === '4')
            this.setState({ disabled4: false });
    }

    handleStartDate(date) {
        this.setState({
            startDate: date 
        });
    }

    handleEndDate(date) {
        this.setState({
            endDate: date
        });
    }

    render(){
        let total = 0;
        const {handleSubmit, error, invalid, submitting} = this.props;

        const returnInvoice = (prepaidId) => {
            // prepaidId = '000674';
            this.props.getReturnInvoiceByPrepaidIdAction(prepaidId);
            window.open(`/print-prepaid-invoice`);
        }
        
        function renderReturn(cell, row){
            // row.userId = prepaidId in billing system.
            return (
                row.isReturn === 1 ?
                    <button 
                        onClick={() => returnInvoice(row.userId)}
                        className="btn btn-primary" 
                        style={{fontSize: 11, width: 55, height: 16, padding: 0}}
                        type="button">
                        <i className="fa fa-share"></i> Return
                    </button>
                : null
            );
        }
        return(
            <div className="margin_left_25">
                <Row>
                    <Col md={10}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">Daily Report For Sale</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={12} lg={12}>
                                            <fieldset className="col-md-12">
                                                <legend>Search By</legend>
                                                <Row>
                                                    <Col md={2} lg={2} className="label-name">
                                                        <label>
                                                            <input type="radio" name="searchByRadio" value="1" onChange={this.handleSearchBy} />
                                                            {' '}&nbsp;<strong>Cus Id </strong>
                                                        </label>
                                                    </Col>
                                                    <Col md={4} lg={4}>
                                                        <Field 
                                                            name="cusId" 
                                                            type="text" 
                                                            component={TextBox} 
                                                            disabled={this.state.disabled1}
                                                            label="Customer Id" />
                                                    </Col>
                                                    <Col md={2} lg={2} className="label-name">
                                                        <label>
                                                            <input type="radio" name="searchByRadio" value="2" onChange={this.handleSearchBy} />
                                                            {' '}&nbsp;<strong>Cus Name </strong>
                                                        </label>
                                                    </Col>
                                                    <Col md={4} lg={4}>
                                                        <Field 
                                                            name="cusName" 
                                                            type="text" 
                                                            component={TextBox} 
                                                            disabled={this.state.disabled2}
                                                            label="Customer Name" />
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={2} lg={2} className="label-name">
                                                        <label>
                                                            <input type="radio" name="searchByRadio" value="3" onChange={this.handleSearchBy} />
                                                            {' '}&nbsp;<strong>Prepaid ID</strong>
                                                        </label>
                                                    </Col>
                                                    <Col md={4} lg={4}>
                                                        <Field 
                                                            name="customerId" 
                                                            type="text" 
                                                            component={TextBox} 
                                                            disabled={this.state.disabled3}
                                                            label="Prepaid ID" />
                                                    </Col>
                                                    <Col md={2} lg={2} className="label-name">
                                                        <label>
                                                            <input type="radio" name="searchByRadio" value="4" onChange={this.handleSearchBy} />
                                                            {' '}&nbsp;<strong>Meter No </strong>
                                                        </label>
                                                    </Col>
                                                    <Col md={4} lg={4}>
                                                        <Field 
                                                            name="meterNo" 
                                                            type="text" 
                                                            component={TextBox} 
                                                            disabled={this.state.disabled4}
                                                            label="Meter No" />
                                                    </Col>
                                                </Row>
                                            </fieldset>

                                            <fieldset className="col-md-12">
                                                <legend>Sold Date</legend>
                                                <div className="form-inline">
                                                    <label>Form : </label>{' '}
                                                    <Field 
                                                        name="startDate"
                                                        component={DateTimePicker}
                                                        defaultDate={this.state.startDate}
                                                        handleChange={this.handleStartDate.bind(this)}
                                                        placeholder="Start Date"/>

                                                    {' '}To{' '}
                                                    <Field 
                                                        name="endDate" 
                                                        component={DateTimePicker}
                                                        defaultDate={this.state.endDate}
                                                        handleChange={this.handleEndDate.bind(this)}
                                                        placeholder="End Date"/>
                                                </div>
                                            </fieldset>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col mdOffset={8} sm={3} md={2}>
                                            <ButtonSubmit 
                                                error={error} 
                                                invalid={invalid} 
                                                submitting={submitting} 
                                                label="Search" />
                                        </Col>
                                        <Col sm={3} md={2} className="col-padding-left-0">
                                            <Button className="btn btn-default btn-block" 
                                                onClick={() => this.handleRefresh()}>
                                                <i className="fa fa-refresh"/> Refresh
                                            </Button>
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                    
                <Row>
                    <Col md={12}>
                        { this.state.isLoadingData === true ? 
                            <center>
                                <i className="fa fa-refresh fa-spin fa-3x fa-fw" style={{color: '#777'}}></i>
                                <span className="sr-only">Loading...</span>
                            </center>
                            : null
                        }

                        <BootstrapTable
                            data={arrayList} //list of records
                            trClassName={trClassNameFormat}  //apply style on cell
                            bodyStyle={{ fontSize:'10pt'}}
                            options={{
                                sizePerPage: 50,
                                sizePerPageList: [ 10, 20, 30, 40 , 50, arrayList.length ],
                                paginationShowsTotal: true,
                                sortName: 'buyDate',  // default sort column name
                                sortOrder: 'desc',  // default sort order
                                afterTableComplete: onAfterTableComplete, // A hook for after table render complete.
                                onRowDoubleClick: function(row) {
                                    window.open(`/print-invoice-daily-sale/${row.userId}`, '','width=1000,height=800');
                                    localStorage.setItem("dailySaleObject", JSON.stringify(row));
                                }
                            }}
                            exportCSV
                            hover // apply hover style on row
                            pagination
                            
                        >
                            <TableHeaderColumn dataField='userId' dataSort isKey autoValue>PrepaidID</TableHeaderColumn>
                            <TableHeaderColumn dataField='userName' width="150px" dataSort >CusName</TableHeaderColumn>
                            <TableHeaderColumn dataField='makeId' dataSort>Meter No</TableHeaderColumn>
                            <TableHeaderColumn dataField='buyTimes' dataSort>Purchase Count</TableHeaderColumn>
                            <TableHeaderColumn dataField='buyDate' dataSort>Purchase Date</TableHeaderColumn>
                            <TableHeaderColumn dataField='buyPowers' dataSort>Purchase Energy</TableHeaderColumn>
                            <TableHeaderColumn dataField='lastBalance' dataSort>Last Balance</TableHeaderColumn>
                            {/*<TableHeaderColumn dataField='lastDateBalance' dataSort>Last Date Balance</TableHeaderColumn>*/}
                            <TableHeaderColumn dataField='powerCost' dataSort>Payment Money</TableHeaderColumn>
                            <TableHeaderColumn dataField='areaName' dataSort>Client Area</TableHeaderColumn>
                            {/*<TableHeaderColumn dataField='note' dataSort>Comment</TableHeaderColumn>*/}
                            {/*<TableHeaderColumn dataField='isReturn' width="70px" dataSort dataFormat={renderReturn}>Return</TableHeaderColumn>
                            <TableHeaderColumn dataField='sellID' dataSort>Sell ID</TableHeaderColumn>
                            <TableHeaderColumn dataField='company' dataSort>Company</TableHeaderColumn>*/}
                        </BootstrapTable>
                    </Col>
                </Row>
            </div>
        )
    }
}

PrepaidPowerReport = reduxForm({
    form: 'form_prepaid_power_report',
    validate: (values) => {
        let regex_name = /[0-9a-zA-Z]/;

        const errors = {};

        return errors
    }
})(PrepaidPowerReport);

function mapStateToProps(state) {
    if (state.returnInvoices.returnInvoiceByPrepaidId.data !== undefined)
        localStorage.setItem("_returnInvoice", JSON.stringify(state.returnInvoices.returnInvoiceByPrepaidId.data.items));
    return {
        prepaidPowerReports: state.icCardDailySale.prepaidPowerReports,
        returnInvoiceByPrepaidId: state.returnInvoices.returnInvoiceByPrepaidId
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getPrepaidPowerReportsAction,
        getReturnInvoiceByPrepaidIdAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(PrepaidPowerReport);