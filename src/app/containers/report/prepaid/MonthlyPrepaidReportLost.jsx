import React from 'react';
import { Row, Col, Panel } from 'react-bootstrap';
import {connect} from 'react-redux';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { Field, reduxForm } from 'redux-form';
import { bindActionCreators } from 'redux';
import { DateTimePicker } from '../../../components/forms/DateTimePicker';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import { getAllLostKwhAction } from './../../../actions/transaction/prepaid/lostKwh';
import moment from 'moment';

class MonthlyPrepaidReportLost extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            startDate: null,
            endDate: null
        }
    }

    handleStartDate(date){
        this.setState({startDate: date});
    }
    handleEndDate(date){
        this.setState({endDate: date});
    }

    handleSubmit(values){
        this.props.getAllLostKwhAction({
            'startDate': moment(values.startDate).format("YYYY-MM-DD"),
            'endDate': moment(values.endDate).format("YYYY-MM-DD")
        });
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return (
            <div className="margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8} className="pull-right">
                        <Panel header="Monthly Prepaid Report Lost" bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={4} lg={4}>
                                        <strong>Start Date <span className="label-require">*</span></strong>
                                        <Field name="startDate" component={DateTimePicker} placeholder="Start Date"
                                               defaultDate={this.state.startDate}
                                               handleChange={this.handleStartDate.bind(this)}/>
                                    </Col>
                                    <Col md={4} lg={4}>
                                        <strong>End Date <span className="label-require">*</span></strong>
                                        <Field name="endDate" component={DateTimePicker} placeholder="End Date"
                                               defaultDate={this.state.endDate}
                                               handleChange={this.handleEndDate.bind(this)}/>
                                    </Col>
                                    <Col md={4} lg={4} className="pull-right">
                                        <br/>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting}
                                                      label="Search" />
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="download-table-xls-button"
                            table="table-to-xls"
                            filename="monthly-prepaid-report-lost-kwh"
                            sheet="tablexls"
                            buttonText="Export to XLS"/>
                        <table className="page-permision-table" id="table-to-xls">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Customer ID</th>
                                <th>Customer Name</th>
                                <th>Meter Serial</th>
                                <th>Prepaid ID</th>
                                <th>Date Lost</th>
                                <th>KW</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                this.props.getAllLostKwh.status === 200 ?
                                    this.props.getAllLostKwh.data.items.map((kwh, index) => {
                                        return (
                                            <tr key={index}>
                                                <td>{index+1}</td>
                                                <td>{kwh.customerId}</td>
                                                <td>{kwh.nameKh}</td>
                                                <td>{kwh.meterSerial}</td>
                                                <td>{kwh.prepaidId}</td>
                                                <td>{moment(kwh.dateLost).format("DD-MM-YYYY")}</td>
                                                <td>{kwh.kw}</td>
                                            </tr>
                                        )
                                    })
                                    :
                                    <tr>
                                        <td colSpan={7} style={{textAlign: 'center'}}>RESULT NOT FOUND</td>
                                    </tr>
                            }
                            </tbody>
                        </table>
                    </Col>
                </Row>
            </div>
        )
    }

}

MonthlyPrepaidReportLost = reduxForm({
    form: 'form_monthly_prepaid_lost_kwh',
    validate: (values) => {
        const errors = {};
        if (values.startDate === undefined) {
            errors.startDate = 'Start Date is required!';
        }
        if (values.endDate === undefined) {
            errors.endDate = 'End Date is required!';
        }
        return errors
    }
})(MonthlyPrepaidReportLost);

function mapStateToProps(state) {
    return {
        getAllLostKwh: state.lostKwh.getAllLostKwh
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getAllLostKwhAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MonthlyPrepaidReportLost);