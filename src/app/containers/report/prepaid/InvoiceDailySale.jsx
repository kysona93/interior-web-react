import React from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import { listSettingAction } from './../../../actions/setup/setting';
import { getICCardDailySalesByCusIdAction } from './../../../actions/report/icCardDailySale';
import accounting from 'accounting';
import './../report-res-prepaid.css';

class InvoiceDailySale extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            prepaidObj: {},
            officeTitle: "",
            officeAddress: "",
            officePhone: ""
        }
    }

    componentWillMount(){
        this.setState({ prepaidObj: JSON.parse(localStorage.getItem("dailySaleObject")) });
        
        let date = JSON.parse(localStorage.getItem("dailySaleObject")) !== null 
                        ? JSON.parse(localStorage.getItem("dailySaleObject")).buyDate : '';

        let arr = date.split("-");

        let filter = {
            month: Number(arr[1]),
            year: Number(arr[0]),
            cusId: this.props.params.cusId
        }
        this.props.listSettingAction();
        this.props.getICCardDailySalesByCusIdAction(filter);

        setTimeout(() => {window.print()}, 1500);
    }

    componentDidMount() {
        window.addEventListener('beforeunload', this.keepOnPage);
    }
      
    componentWillUnmount() {
        window.removeEventListener('beforeunload', this.keepOnPage);
    }
      
    keepOnPage(e) {
        // var message = 'Warning!\n\nNavigating away from this page will delete your text if you haven\'t already saved it.';
        // e.returnValue = message;
        // return message;
        return localStorage.removeItem("dailySaleObject");
    }

    componentWillReceiveProps(newProps){
        if(newProps.listSetting.status === 200) {
            const item1 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_TITLE_KH");
            const item2 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_ADDRESS");
            const item3 = newProps.listSetting.data.items.find(i => i.keyword === "OFFICE_PHONE");
            this.setState({ 
                officeTitle: item1.value,
                officeAddress: item2.value,
                officePhone: item3.value
            });
        }
    }

    render(){
        let total = 0;
        return(
            <div className="mini-report-wrapper">
                {/*cellSpacing={1} cellSpacing={0}*/}
                <table  width="100%">
                    <tbody>
                        <tr>
                            <td>
                                <center>
                                    <img src="/icons/Logo-InterBP.png" width="250"/>
                                    <h5 className="mini-report-title">{this.state.officeTitle}</h5>
                                    <p>{this.state.officeAddress}</p>
                                </center>
                            </td>
                        </tr>
                    </tbody>
                </table>
                &nbsp;
                <table cellSpacing={1} width="100%">
                    <tbody>
                        <tr>
                            <td className="text-align-center" colSpan={2}><u style={{fontSize: 16}}>បង្កាន់ដៃទទួលប្រាក់</u></td>
                        </tr>
                        <tr>
                            <td><span>អត្ថលេខ: {this.state.prepaidObj !== null ? this.state.prepaidObj.userId : ''}</span></td>
                        </tr>
                        <tr>
                            <td>ឈ្មោះអតិថិជន: {this.state.prepaidObj !== null ?this.state.prepaidObj.userName : ''}</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>កាលបរិចេ្ឆទ: {this.state.prepaidObj !== null ?this.state.prepaidObj.buyDate : ''}</td>
                        </tr>
                    </tbody>
                </table>
                <table className="mini-report">
                    <thead>
                        <tr>
                            <td>ល.រ</td>
                            <td>បរិយាយ</td>
                            <td>ចំនួន(KW)</td>
                            <td>តំលៃ</td>
                            <td>សរុប</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="text-align-center">1</td>
                            <td className="text-align-center">
                                {this.state.prepaidObj !== null ?this.state.prepaidObj.makeId : ''}
                            </td>
                            <td className="text-align-center">
                                {this.state.prepaidObj !== null ?this.state.prepaidObj.buyPowers : ''}
                            </td>
                            <td className="text-align-center">
                                {this.state.prepaidObj !== null ?accounting.formatNumber(this.state.prepaidObj.powerCost) : ''}
                            </td>
                            <td className="text-align-center">
                                {this.state.prepaidObj !== null ?accounting.formatNumber(this.state.prepaidObj.buyPowers * accounting.unformat(this.state.prepaidObj.powerCost)) : ''}
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p className="padding-5px">ប្រវត្តទិញក្នុងខែ:</p>
                <table className="mini-report">
                    <thead>
                        <tr>
                            <td>ល.រ</td>
                            <td>កាលបរិច្ឆេទ</td>
                            <td>លើក</td>
                            <td>ចំនួន(KW)</td>
                            <td>ផ្សេងៗ</td>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        this.props.icCardDailySalesByCusId.status === 200 ?
                            this.props.icCardDailySalesByCusId.data.items.map((i, index) => {
                                total = total + i.buyPowers;
                                return(
                                    <tr key={index}>
                                        <td className="text-align-center">{index + 1}</td>
                                        <td className="text-align-center">{i.buyDate}</td>
                                        <td className="text-align-center">{i.buyTimes}</td>
                                        <td className="text-align-center">{i.buyPowers}</td>
                                        <td className="text-align-center"></td>
                                    </tr>
                                );
                            })
                        : null
                    }
                    <tr>
                        <td className="text-align-center" colSpan={3}>សរុប</td>
                        <td className="text-align-center">{total}</td>
                        <td className="text-align-center"></td>
                    </tr>
                    </tbody>
                </table>
                {/*<p  className="padding-5px">ប្រវត្តិទិញក្នុងខែ</p>
                <table>
                    <tbody>
                        <tr>
                            <td className="text-align-center">09-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">08-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">07-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">06-2017: 54គ &nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td className="text-align-center">05-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">04-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">03-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">02-2017: 54គ &nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td className="text-align-center">01-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">12-2016: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">11-2016: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">10-2016: 54គ &nbsp;&nbsp;</td>
                        </tr>
                    </tbody>
                </table>*/}
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        listSetting: state.setting.listSetting,
        icCardDailySalesByCusId: state.icCardDailySale.icCardDailySalesByCusId
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        listSettingAction,
        getICCardDailySalesByCusIdAction
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceDailySale);