import React from 'react';
import { Row, Col, Table } from 'react-bootstrap';
class Return extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="container">
                <br/>
                <br/>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}><p> Customer Use less than 10 KW Need To Return </p></Col>
                    <Col xs={12} sm={12} md={12} lg={12}><p>Project No</p></Col>
                    <Col xs={12} sm={12} md={12} lg={12}><p>Date   ៖ 07-2017</p></Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <Table striped bordered condensed className="prepaid-res-report">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Ampere</th>
                                <th>Box</th>
                                <th>Meter</th>
                                <th>Price</th>
                                <th>Old Usage</th>
                                <th>New Usage</th>
                                <th>Coefficients</th>
                                <th>KW</th>
                                <th>Amount</th>
                                <th>Office</th>
                                <th>KW Return</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className="text-align-center">1</td>
                                <td className="text-align-left">150</td>
                                <td className="text-align-left">ស្រី ផុន</td>
                                <td className="text-align-center">10A</td>
                                <td className="text-align-center">T4-L59</td>
                                <td className="text-align-center">1202100609</td>
                                <td className="text-align-center">480</td>
                                <td className="text-align-center"> </td>
                                <td className="text-align-center"> </td>
                                <td className="text-align-center">1</td>
                                <td className="text-align-center">10</td>
                                <td className="text-align-center">4800</td>
                                <td className="text-align-center">Office 1</td>
                                <td className="text-align-center"> </td>

                            </tr>
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}
export default Return;
