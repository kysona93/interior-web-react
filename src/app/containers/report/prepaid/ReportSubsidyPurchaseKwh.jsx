import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { Row, Col, Table, Panel, Button } from 'react-bootstrap';
import SelectBox from '../../../components/forms/SelectBox';
import { TextBox } from '../../../components/forms/TextBox';
import { ButtonSubmit } from '../../../components/forms/ButtonSubmit';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { getPurchaseKwhMonthlyReportsAction } from './../../../actions/inventory/purchaseKwh';

let filter = {
    year: 0,
    month: 0
}
let blankColumns = [];

class ReportSubsidyPurchaseKwh extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            date : "",
            months :[
                {id: 1, name: "January"},
                {id: 2, name: "February"},
                {id: 3, name: "March"},
                {id: 4, name: "April"},
                {id: 5, name: "May"},
                {id: 6, name: "June"},
                {id: 7, name: "July"},
                {id: 8, name: "August"},
                {id: 9, name: "September"},
                {id: 10, name: "October"},
                {id: 11, name: "November"},
                {id: 12, name: "December"}
            ],
            blankColumns: [
                {id: 1},
                {id: 2},
                {id: 3},
                {id: 4},
                {id: 5},
                {id: 6},
                {id: 7}
            ]
        };
    }

    handleRefresh() {
        filter = {
            year: 0,
            month: 0
        }
        this.props.getPurchaseKwhMonthlyReportsAction(filter);
        this.props.dispatch(initialize('form_report_subsidy_purchase_kwh', {}));
    }

    handleSubmit(values){
        this.setState({date : values.month+"-"+values.year});

        if(values.year === undefined) filter.year = 0;
        else filter.year = values.year;

        if(values.month === undefined) filter.month = 0;
        else filter.month = values.month;

        this.props.getPurchaseKwhMonthlyReportsAction(filter);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        let dataLenght = 0;
        let remainBlankColunms = 0;
        return(
            <div className="container">
                <br/>
                <Row>
                    <Col md={8} lg={8} className="pull-right">
                        <Panel header={"Report ref subsidy purchase kwh for " + this.state.date} bsStyle="info">
                            <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                <Row>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field
                                            name="month"
                                            type="select"
                                            component={SelectBox}
                                            placeholder="Select Month"
                                            values={this.state.months} />
                                    </Col>
                                    <Col md={4} lg={4} className="label-name">
                                        <Field name="year" type="text" component={TextBox} label="Year"/>
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Search" />
                                    </Col>
                                    <Col md={2} lg={2}>
                                        <Button onClick={() => this.handleRefresh()}>Refresh</Button>
                                    </Col>
                                </Row>
                            </form>
                        </Panel>
                    </Col>
                </Row>
                <Row>
                    <Col xs={12} sm={12} md={12} lg={12}>
                        <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="download-table-xls-button"
                                table="table-to-xls"
                                filename="report_subsidy_purchase_kwh"
                                sheet="tablexls"
                                buttonText="Export to XLS"/>
                        <Table id="table-to-xls" striped bordered condensed className="prepaid-res-report">
                            <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={4}>
                                        <p style={{textAlign: 'left', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt"}}>
                                            អាជ្ញាបណ្ណលេខ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;៖ 259
                                        </p>
                                    </th>
                                </tr>
                                <tr>
                                    <th className="bg-none" colSpan={4}>
                                        <p style={{textAlign: 'left', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt"}}>
                                            ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ &nbsp;៖ ក្រុមហ៊ុន អ៊ិនធើ ប៊ីភី សឹលូសិន ខបភើរេសិន​
                                        </p>
                                    </th>
                                </tr>
                                <tr>
                                    <th className="bg-none" colSpan={4}>
                                        <p style={{textAlign: 'left', fontWeight: "lighter", fontFamily: "Kh Siemreap",fontSize: "9pt"}}>
                                            ទិន្នន័យសម្រាប់ខែ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;៖ { this.state.date }
                                        </p>
                                    </th>
                                </tr>
                            </thead>
                            <thead hidden>
                                <tr>
                                    <th className="bg-none" colSpan={4}>
                                        <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "12pt"}}>តារាងទិញ និងផលិតថាមពលអគ្គិសនី</p>
                                    </th>
                                </tr>
                                <tr>
                                    <th className="bg-none" colSpan={4}>
                                        <p style={{textAlign: 'center', fontFamily: "Kh Siemreap",fontSize: "12pt"}}>Table for Purchase and Generation of Electricity</p>
                                    </th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <th style={{width:'60px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>លេខរៀង <br/>(No)</th>
                                    <th style={{width:'300px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>បរិយាយថាមពល <br/>​(Description)</th>
                                    <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>ចំនួនថាមពល <br/>(kWh)</th>
                                    <th style={{width:'150px', fontFamily: "Kh Siemreap", fontSize: "9pt", backgroundColor: "#BDBDBD"}}>អត្រាថ្លៃ<br/>(Rate)</th>
                                </tr>
                            </thead>
                            { this.props.purchaseKwhMonthlyReports.status === 200 ?
                                <tbody>
                                    <tr>
                                        <td colSpan={4} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt"}}>ការទិញថាមពល</td>
                                    </tr>
                                    { this.props.purchaseKwhMonthlyReports.data.items.map((i,index) => {
                                        dataLenght = this.props.purchaseKwhMonthlyReports.data.items.length;
                                        if (dataLenght < 7) {
                                            remainBlankColunms = 7 - dataLenght;
                                        }

                                        return(
                                            <tr key={index}>
                                                <td style={{width:'60px'}} className="text-align-center">{index + 1}</td>
                                                <td style={{width:'300px'}} className="text-align-center">{i.nameKh} ({i.name})</td>
                                                <td style={{width:'150px'}} className="text-align-left">{i.kwh}</td>
                                                <td style={{width:'150px'}} className="text-align-center">{i.rate} {i.currency === 'USD' ? '$' : '៛'}</td>
                                            </tr>
                                        )
                                    })}
                                    { Array(remainBlankColunms).fill().map((_, i) => {
                                        dataLenght = dataLenght + 1;
                                        return(
                                            <tr key={i}>
                                                <td style={{width:'60px'}} className="text-align-center">{dataLenght}</td>
                                                <td style={{width:'300px'}} className="text-align-center"></td>
                                                <td style={{width:'150px'}} className="text-align-left"></td>
                                                <td style={{width:'150px'}} className="text-align-center"></td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                                :
                                <tbody>
                                    <tr>
                                        <td colSpan={4} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt"}}>ការទិញថាមពល</td>
                                    </tr>
                                    { this.state.blankColumns.map((i,index) => {
                                        return(
                                            <tr key={index}>
                                                <td style={{width:'60px'}} className="text-align-center">{i.id}</td>
                                                <td style={{width:'300px'}} className="text-align-center"></td>
                                                <td style={{width:'150px'}} className="text-align-left"></td>
                                                <td style={{width:'150px'}} className="text-align-center"></td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            }

                            <tbody>
                                <tr>
                                    <td colSpan={4} style={{fontFamily: "Kh Siemreap", fontWeight: "bold", fontSize: "10pt"}}>ការផលិតថាមពល</td>
                                </tr>
                                { this.state.blankColumns.map((i,index) => {
                                    return(
                                        <tr key={index}>
                                            <td style={{width:'60px'}} className="text-align-center">{i.id}</td>
                                            <td style={{width:'300px'}} className="text-align-center"></td>
                                            <td style={{width:'150px'}} className="text-align-left"></td>
                                            <td style={{width:'150px'}} className="text-align-center"></td>
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

ReportSubsidyPurchaseKwh = reduxForm({
    form: 'form_report_subsidy_purchase_kwh',
    validate: (values) => {
        const errors = {};
        let regex_year = /^\d{4}$/;
        if (!regex_year.test(values.year) || values.year === undefined) {
            errors.year = 'Invalid year!';
        }
        if(values.month === undefined){
            errors.month = "Invalid month!";
        }
        return errors;
    }
})(ReportSubsidyPurchaseKwh);

function mapStateToProps(state) {
    return {
        purchaseKwhMonthlyReports: state.purchaseKwh.purchaseKwhMonthlyReports
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({
        getPurchaseKwhMonthlyReportsAction
    }, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ReportSubsidyPurchaseKwh);