import React from 'react';
import {Row,Col, Table} from 'react-bootstrap';
import '../report-res-prepaid.css';

class ReportMini extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="container-fuid" style={{marginLeft:'20px'}}>
                <table width="300"  cellSpacing={1} cellSpacing={0}>
                    <tbody>
                        <tr>
                            <td>
                                <img src="./icons/Logo-Inter BP.png" width="290"/>
                               <h5 className="mini-report-title"> ក្រុមហ៊ុន អិនធើប៊ីភី សឹលូសិន ខបបើរេសិន</h5>
                                &nbsp;
                                អាស័យដ្ឋាន: ភូមិ ពោធិ៍2, ឃុំ ដូនកឹង, ស្រុក កំចាយមារ, ខេត្ត ព្រៃវែង
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
                &nbsp;
                    <table width="300" cellSpacing={1}>
                        <tbody>
                            <tr>
                                <td className="text-align-center" colSpan={2}><u>បង្កាន់ដៃទទួលប្រាក់</u></td>
                            </tr>
                            <tr>
                                <td><span>លេខ: 001</span></td>
                            </tr>
                            <tr>
                                <td>អតិថិជនឈ្មោះ: Piset Pisenyary3</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>កាលបរិចេ្ឆទ: 27/Oct/16</td>
                            </tr>
                    </tbody>
                    </table>
                <table className="mini-report">
                    <thead>
                        <tr>
                            <td >ល.រ</td>
                            <td >បរិយាយ</td>
                            <td>ចំនួន(Kwh)</td>
                            <td>តំលៃ</td>
                            <td>សរុប</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="text-align-center">1</td>
                            <td className="text-align-center">1234</td>
                            <td className="text-align-center">10</td>
                            <td className="text-align-center">790</td>
                            <td className="text-align-center">7900</td>
                        </tr>
                    </tbody>
                </table>
                <p className="padding-5px">ប្រវត្រិទិញ ក្នុងខែ</p>
                <table className="mini-report">
                    <thead>
                        <tr>
                            <td >ល.រ</td>
                            <td >កាលបរិច្ឆេត</td>
                            <td>លើក</td>
                            <td>ចំនួន</td>
                            <td>ផ្សេងៗ</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="text-align-center">1</td>
                            <td className="text-align-center">09-09-2017</td>
                            <td className="text-align-center">1</td>
                            <td className="text-align-center">12</td>
                            <td className="text-align-center"></td>
                        </tr>
                    <tr>
                        <td className="text-align-center" colSpan={4}>សរុប</td>
                        <td className="text-align-center">25</td>
                    </tr>
                    </tbody>
                </table>
                <p  className="padding-5px">ប្រវត្តិទិញក្នុងខែ</p>
                <table>
                    <tbody>
                        <tr>
                            <td className="text-align-center">09-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">08-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">07-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">06-2017: 54គ &nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td className="text-align-center">05-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">04-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">03-2017: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">02-2017: 54គ &nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                            <td className="text-align-center">01-2017: 54គ​ &nbsp;&nbsp;</td>
                            <td className="text-align-center">12-2016: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">11-2016: 54គ &nbsp;&nbsp;</td>
                            <td className="text-align-center">10-2016: 54គ &nbsp;&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ReportMini;