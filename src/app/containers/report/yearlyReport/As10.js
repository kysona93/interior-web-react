import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As10 extends React.Component {
    render() {
        return (
            <div className="container">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as10" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS10</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ការគណនាចំណាយត្រឹមត្រូវ		</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>បរិយាយ</th>
                            <th>គំរូតារាង</th>
                            <th>តួលេខជាក់ស្ដែង សម្រាប់ឆ្នាំ២០១៥</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center', whiteSpace:'nowrap'}}>
                        <tr style={{ textAlign:'center'}}>
                            <td>ក</td>
                            <td></td>
                            <td style={{textAlign:'​left', textDecoration:'underline', fontWeight:'bold', fontSize:'18px'}}>ចំណាយផ្នែកផលិតកម្ម</td>
                            <td></td>
                            <td>(រៀល)</td>
                        </tr>
                        <tr style={{ textAlign:'center'}}>
                            <td></td>
                            <td>1</td>
                            <td style={{ textAlign:'left'}}>ចំណាយប្រេងម៉ាស៊ូត</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td style={{ textAlign:'left'}}>ចំណាយប្ប្រេង ខ្មៅ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td style={{ textAlign:'left'}}>ចំណាយប្រេងរំអិល</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td style={{ textAlign:'left'}}>ចំណាយលើបុគ្គលិកផ្នែកផលិតកម្ម</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td style={{ textAlign:'left'}}>ចំណាយដំណេីរការនិងការថែទាំ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>6</td>
                            <td style={{ textAlign:'left'}}>ចំណាយរដ្ឋបាលនិងការគ្រប់គ្រងទូទៅ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>7</td>
                            <td style={{ textAlign:'left'}}>ចំណាយសម្រាប់រំលោះ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>8</td>
                            <td style={{ textAlign:'left'}}>ការប្រាក់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>9</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់ចំណេញសមស្រប</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{textAlign:'​left', textDecoration:'underline', fontWeight:'bold', fontSize:'18px'}}>ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកផលិតកម្ម</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>10</td>
                            <td style={{ textAlign:'left'}}>ថាមពលបពា្ជូនគិតជាគីឡូវ៉ាត់ម៉េាង</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>11</td>
                            <td style={{ textAlign:'left'}}>ថ្លៃដេីមផលិតក្នុង១គីឡូវ៉ាត់ម៉េាង</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ខ</td>
                            <td></td>
                            <td style={{textAlign:'​left', textDecoration:'underline', fontWeight:'bold', fontSize:'18px'}}>ចំណាយផ្នែកចែកចាយ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td style={{ textAlign:'left'}}>ចំណាយទិញថាមពល</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td style={{ textAlign:'left'}}>ចំណាយលើបុគ្គលិកផ្នែកចែកចាយ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td style={{ textAlign:'left'}}>ចំណាយដំណេីរការ និងការថែទាំ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td style={{ textAlign:'left'}}>ចំណាយរដ្ឋបាល និងការគ្រប់គ្រងទូទៅ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td style={{ textAlign:'left'}}>ចំណាយសម្រាប់រំលោះ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>6</td>
                            <td style={{ textAlign:'left'}}>ការប្រាក់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>7</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់ចំណេញសមស្រប</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>8</td>
                            <td style={{ textAlign:'left'}}>ចំណាយត្រឹមត្រូវសរុបសំរាប់ផ្នែកចែកចាយ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ណាយសរុប (ផលិតកម្ម ចែកចាយ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>គ</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ដកចំណូលផ្សេងក្រៅពីប្រភពថ្លៃលក់អគ្គិសនី</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ឃ</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ចំណូលសរុបប្រចាំឆ្នាំដែលត្រូវមាន (ក-ខ-គ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ង</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ចំណូលសរុបដែលបានមកពីថ្លៃលក់បច្ចុប្បន្ន</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ច</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ចំណេញ/(ខាត) មុនកែសម្រួលថ្លៃលក់អគ្គិសនី (ង-ឃ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ឈ</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ផលចំណេញបានមកពីការកែសម្រួលថ្លៃលក់អគ្គិសនី</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ជ</td>
                            <td></td>
                            <td style={{ textAlign:'left'}}>ចំណេញ/(ខាត) ក្រេាយកែសម្រួលថ្លៃលក់អគ្គិសនី (ង-ឆ-ឃ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan={7}><p><b style={{fontWeight:'bold', fontSize:'18px'}}>សំគាល់ៈ</b> នៅពេលចុះបញ្ជីអចលនទ្រព្យតាមតារាងខាងលើ អ្នកកាន់អាជ្ងា ប័ណ្ណត្រូវដកចេញនូវវិភាគទានរបស់អ្នកប្រើប្រាស់ និងជំនួយផ្សេងៗទាំងអស់។</p></td>
                        </tr>
                    </tbody>
                </Table> 
                
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as10"
                        filename="yearly-report-as10"
                        sheet="yearly-report-as10"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As10);
