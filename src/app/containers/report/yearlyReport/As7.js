import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As7 extends React.Component {
    render() {
        return (
            <div className="container">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as7" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS7</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ពត៏មានលម្អិតអំពី អចលនទ្រព្យសរុប</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th>បរិយាយ អចលនទ្រព្យ</th>
                            <th>អចលនទ្រព្យនៅ<br/> ដើមឆ្នាំ​ ២០១៤</th>
                            <th>ការបន្ថែមអចលនទ្រព្យ<br/> ក្នុងឆ្នាំ ២០១៤</th>
                            <th>ការដកចេញអចលនទ្រព្យ <br/>ក្នុងឆ្នាំ ២០១៤</th>
                            <th>សមតុល្យ អចលនទ្រព្យ<br/> នៅចុងឆ្នាំ ២០១៤</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'left'}}>
                        <tr style={{ textAlign:'center'}}>
                            <td></td>
                            <td></td>
                            <td>(រៀល)</td>
                            <td>(រៀល)</td>
                            <td>(រៀល)</td>
                            <td>(រៀល)</td>
                        </tr>
                        <tr style={{ textAlign:'center'}}>
                            <td></td>
                            <td></td>
                            <td>(a)</td>
                            <td>(b)</td>
                            <td>(c)</td>
                            <td>d=(a)+(b)-(c)</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>អចលនទ្រព្យផ្នែកផលិតកម្ម</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{fontWeight:'bold',fontSize:'20px'}}>សរុប </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>អចលនទ្រព្យផ្នែកចែកចាយ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>ប៉ុស្ដិ៏ត្រង់ស្វូ និងបរិក្ខារួមផ្សំ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>ខ្សែបណ្ដាញតង់ស្យុងមធ្យម</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>ខ្សែបណ្ដាញតង់ស្យុងទាប</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>បង្គោលតង់ស្យុងមធ្យម</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>បង្គោលតង់ស្យុងទាប</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>ប្រអប់នាឡិកាស្ទង់</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>បរិក្ខារផ្គត់ផ្គង់ឲអ្នកប្រើប្រាស់</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>បរិក្ខាររួមផ្សំផ្នែកតង់ស្យុងមធ្យម</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>បរិក្ខាររួមផ្សំផ្នែកតង់ស្យុងទាប</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{fontWeight:'bold',fontSize:'20px'}}>សរុប </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>មធ្យោបាយធ្វើការងារ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>អគារធ្ធ្វើការ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>យានជំនិះ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>ឧបករណ៌រ.ជនិងការពារសុវត្ថិភាព</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{fontWeight:'bold',fontSize:'18px'}}>សរុប</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold',fontSize:'20px'}}>
                            <td> សរុបរួម</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan={6}><p><b style={{fontWeight:'bold', fontSize:'18px'}}>សំគាល់ៈ</b> នៅពេលចុះបញ្ជីអចលនទ្រព្យតាមតារាងខាងលើ អ្នកកាន់អាជ្ងា ប័ណ្ណត្រូវដកចេញនូវវិភាគទានរបស់អ្នកប្រើប្រាស់ និងជំនួយផ្សេងៗទាំងអស់។</p></td>
                        </tr>
                    </tbody>
                </Table> 
                
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as7"
                        filename="yearly-report-as7"
                        sheet="yearly-report-as7"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As7);
