import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class Fa12 extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="table-responsive">
                    <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-fa12" striped bordered condensed className="yearly-report-as3">
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ព្រះរាជាណាចក្រកម្ពុជា</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ជាតិ សាសនា ព្រះមហាក្សត្រ</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'center'}}>បញ្ជីអចលនទ្រព្យ		</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'center'}}>សំរាប់ឆ្នាំការិយបរិច្ឆេត		</p></th>
                            </tr>
                        </thead>
                        <thead hidden>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                            </tr>
                        </thead>
                        <thead hidden>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'left'}}>ប្រភេទសេវាកម្មអគ្គិសនីៈ ចែកចាយ</p></th>
                            </tr>
                        </thead>
                        <thead hidden>
                            <tr style={{background:'none'}}>
                                <th colSpan={16}><p style={{textAlign:'left'}}>ទីតាំងធ្វើសេវាកម្មអគ្គិសនី : ពីព្រំប្រទល់ប្រទេសកម្ពុជា-វៀតណាម មកដល់ ស្រុកកំចាយមា ស្រុកស្វាយអន្ទរ និង ស្រុកពារាំង ខេត្ត ព្រៃវែង </p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th>ល.រ</th>
                                <th>បរិយាយអំពី<br/> អចលនទ្រព្យនីមួយៗ</th>
                                <th>ឯកតា</th>
                                <th>សញ្ញាសំគាល់</th>
                                <th>បរិមាណ</th>
                                <th>ប្រភពនិងឆ្នាំផលិត</th>
                                <th>ឆ្នាំពេលទិញ <br/> ឆ្នាំ</th>
                                <th>ថ្លៃ១ឯកតា ពេលទិញ<br/> រៀល</th>
                                <th>ស្ថានភាពពេលទិញ</th>
                                <th>ទឹកប្រាក់សរុប<br/>​ រៀល</th>
                                <th>រយៈពេល<br/>រំលោះឆ្នាំ</th>
                                <th>អត្ររំលោះ<br/>ក្នុងមួយឆ្នាំ %</th>
                                <th>សំគាល់</th>
                            </tr>
                        </thead>
                        <tbody style={{textAlign:'center', whiteSpace:'nowrap'}}>
                            <tr style={{ textAlign:'center'}}>
                                <td>I</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>មធ្យោបាយចែកចាយ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td style={{ textAlign:'center',fontWeight:'bold',fontSize:'18px'}}>ក</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>ផ្នែកតង់ស្យង់មធ្យម</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center',fontWeight:'bold',fontSize:'18px'}}>
                                <td style={{ textAlign:'center',fontWeight:'bold',fontSize:'18px'}}>ក១</td>
                                <td style={{textAlign:'left'}}>ខ្សែបណ្ដាញ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                               
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                
                            </tr>
                            <tr style={{ textAlign:'center',fontWeight:'bold',fontSize:'18px'}}>
                                <td style={{ textAlign:'center',fontWeight:'bold',fontSize:'18px'}}>ក២</td>
                                <td style={{textAlign:'left'}}>ខ្សែបណ្ដាញ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </Table> 
                </div>
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-fa12"
                        filename="yearly-report-fa12"
                        sheet="yearly-report-fa12"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Fa12);
