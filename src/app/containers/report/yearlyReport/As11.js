import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As11 extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="table-responsive">
                    <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as11" striped bordered condensed className="yearly-report-as3">
                        <thead hidden>
                            <tr style={{background:'none'}}>
                                <th colSpan={40}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={40}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS11</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={40}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ចំណូលពីថ្លៃលក់អគ្គិសនីកំពុងអនុវត្ដ		</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={40}><p style={{textAlign:'center'}}>តួរលេខជាក់ស្ដែងសំរាប់ខែ		</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th colSpan={7}>បញ្ជីថ្លៃលក់	</th>
                                <th colSpan={7}>ទិន្នន័យពាក់ព័ន្ធចំណូលលក់ថាមពល	</th>
                                <th colSpan={8}>ចំណូលតាមវិក្កយបត្រពេញ១ឆ្នាំ	</th>
                                <th colSpan={8}>ចំណូលប្រមូលបាន	</th>
                            </tr>
                            <tr>
                                <th>ខែ</th>
                                <th>ប្រភេទអ្នកប្រើប្រាស់</th>
                                <th>ចំនួនអ្នកប្រើប្រាស់នៅចុងឆ្នាំ</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(លំនៅដ្ធាន)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អង់តែន)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អាជីវកម្ម១)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អាជីវកម្ម២)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អាជីវកម្ម៣)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អ្នកកាន់កាប់)</th>
                                <th>អត្រាថ្លៃលក់អគ្គិសនី<br/>(អគ្គិសនីកម្ពុជា)</th>
                                <th>ថាមពលលក់<br/>(លំនៅដ្ធាន)</th>
                                <th>ថាមពលលក់<br/>(អង់តែន)</th>
                                <th>ថាមពលលក់<br/>(អាជីវកម្ម១)</th>
                                <th>ថាមពលលក់<br/>(អាជីវកម្ម២)</th>
                                <th>ថាមពលលក់<br/>(អាជីវកម្ម៣)</th>
                                <th>ថាមពលលក់<br/>(អ្នកកាន់កាប់)</th>
                                <th>ថាមពលលក់<br/>(អគ្គិសនីកម្ពុជា)</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ១</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ២</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៣</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៤</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៥</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៦</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៧</th>
                                <th>សរុប</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ១</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ២</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៣</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៤</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៥</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៦</th>
                                <th>ចំណូលពីការលក់តាមរយៈ <br/>ថ្លៃប្រភេទ៧</th>
                                <th>សរុប</th>
                            </tr>
                            
                        </thead>
                        <tbody style={{textAlign:'center', whiteSpace:'nowrap'}}>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>រៀល/គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>គ.វ៉.ម៉</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                                <td>រៀល</td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>មករា</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                    </Table> 
                </div>
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as11"
                        filename="yearly-report-as11"
                        sheet="yearly-report-as11"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As11);
