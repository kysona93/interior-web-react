import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As3 extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as3" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={10}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={10}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS3</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={10}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ពត៏មានលំអិតអំពីចំណាយលើបុគ្គលិក</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={10}><p style={{textAlign:'center'}}>តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ ២០១៤</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th>ប្រភេទបុគ្គលិក</th>
                            <th>ចំនួនបុគ្គលិកនៅចុងឆ្នាំ</th>
                            <th>ប្រាក់ខែមូលដ្ធាន</th>
                            <th>ប្រាក់ឧបត្ថម១<br/>(ម្ហូបអាហា)</th>
                            <th>ប្រាក់ឧបត្ថម២<br/>(បំណាច់ឆ្នាំ)</th>
                            <th>បុព្វលាភ និងសគុណផ្សេងៗ<br/>(មាតុភាព)</th>
                            <th>ផលប្រយោជន៏និងរង្វាន់លើកទឹកចិត្ត<br/>(បុណ្យទាន)</th>
                            <th>ផ្សេងៗ(OT)</th>
                            <th>សរុប</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            <td>រៀល</td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td>ផ្នែករដ្ធបាល</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>ប្រធានផ្នែកហិរិញ្ញវត្ថុ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>ប្រធានរដ្ធបាល</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>អនុប្រធាន</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>បេឡា</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>បញ្ជារទិញ(Stock)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>អ្នករត់សំបុត្រ(Messanger)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold', fontSize:'20px'}}>
                            <td></td>
                            <td>សរុប</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        {/**
                         * --------------
                         */}
                         <tr>
                            <td></td>
                            <td>ផ្នែកបច្ចេកទេស</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>បញ្ជូន</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>ប្រធានផ្នែកបណ្ដាញ (MV)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>អនុប្រធានផ្នែកបណ្ដាញ(MV)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>បុគ្គលិកបណ្ដាញ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold', fontSize:'20px'}}>
                            <td></td>
                            <td>សរុប</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        {/**
                         * -----------------
                         */}
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>ចែកចាយ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>អនុប្រធាន ផ្នែកបណ្ដាញ LV</td>
                            <td>1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>បុគ្គលិកបណ្ដាញ</td>
                            <td>6</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold', fontSize:'20px'}}>
                            <td></td>
                            <td>សរុប</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        {/**
                         * 
                         */}
                        <tr>
                            <td>1</td>
                            <td>ផ្នែកពិនិត្យបញ្ជី (Audit)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>អនុប្រធានផ្នែក(Audit)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>អនុប្រធានផ្នែក(Audit)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>បុគ្គលិក(Audit)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold', fontSize:'20px'}}>
                            <td></td>
                            <td>សរុប</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        <tr style={{fontWeight:'bold', fontSize:'20px'}}>
                            <td></td>
                            <td>សរុបរួម</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                        
                    </tbody>
                </Table>  
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as3"
                        filename="yearly-report-as3"
                        sheet="yearly-report-as3"
                        buttonText="Export to Excel"/>        
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As3);
