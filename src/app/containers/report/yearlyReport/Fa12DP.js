import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class Fa12DP extends React.Component {
    render() {
        return (
            <div className="container">
                <div className="table-responsive">
                    <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-fa12" striped bordered condensed className="yearly-report-as3">
                        <thead hidden>
                            <tr style={{background:'none'}}>
                                <th colSpan={18}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={18}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS12</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={18}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ពត៏មានអំពីថាមពល</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr style={{background:'none'}}>
                                <th colSpan={18}><p style={{textAlign:'center'}}>តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ		</p></th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th></th>
                                <th>បរិយាយ</th>
                                <th>ឯកតា</th>
                                <th>មករា</th>
                                <th>កម្ភៈ</th>
                                <th>មិនា</th>
                                <th>មេសា</th>
                                <th>ឧសភា</th>
                                <th>មិថុនា</th>
                                <th>កក្កដា</th>
                                <th>សីហា</th>
                                <th>កញ្ញា</th>
                                <th>តុលា</th>
                                <th>វិច្ឆិការ</th>
                                <th>ឆ្នូរ</th>
                                <th>សរុប</th>
                            </tr>
                            
                        </thead>
                        <tbody style={{textAlign:'center', whiteSpace:'nowrap'}}>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>1</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>ថាមពលលក់</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}>ថាមពលលក់លើតង់ស្យង់ទាប</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}>ថាមពលបញ្ជូន លក់ឲ្យសេវាករ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}>ថាមពលបញ្ជូនលក់ឲ្យអគ្គិសនីកម្ពុជា</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>2</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>ការបាត់បង់លើបណ្ដាញចែកចាយ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>3</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>តម្រូវការថាមពលសុទ្ធ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}>ថាមពលផលិតសុទ្ធ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td></td>
                                <td style={{textAlign:'left'}}>ថាមពលទិញ</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>4</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>ថាមពលសរុបសំរាប់លក់</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr style={{ textAlign:'center'}}>
                                <td>5</td>
                                <td style={{textAlign:'left',fontWeight:'bold',fontSize:'18px'}}>លើស/(ខ្វះ)</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            
                        </tbody>
                    </Table> 
                </div>
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-fa12"
                        filename="yearly-report-fa12"
                        sheet="yearly-report-fa12"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Fa12DP);
