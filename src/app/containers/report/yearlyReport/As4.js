import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As4 extends React.Component {
    render() {
        return (
            <div className="container">
            <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as4" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS4</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ចំណាយដំណើរការនិងការថែទាំ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th rowSpan={2} style={{lineHeight:'3.5'}}>បរិយាយ</th>
                            <th colSpan={3}>តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ ២០១៦</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>ផ្នែកផលិតកម្ម</th>
                            <th>ផ្នែកចែកចាយ</th>
                            <th>សរុប</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style={{textAlign:'center'}}>
                            <td></td>
                            <td></td>
                            <td>(រៀល)</td>
                            <td>(រៀល)</td>
                            <td>(រៀល)</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>គ្រឿងបន្លាស់</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>សំភារៈ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>ជួសជុលនិងថែទាំ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>ចំណាយលើការជួល</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>ទឹក ភ្លើង</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>បំណុលពិបាកទារ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>ផ្សេងៗ (បញ្ជាក់)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold',fontSize:'20px'}}>
                            <td></td>
                            <td>Total</td>
                            <td>-</td>
                            <td>--</td>
                            <td>--</td>
                        </tr>
                    </tbody>
                </Table> 
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as4"
                        filename="yearly-report-as4"
                        sheet="yearly-report-as4"
                        buttonText="Export to Excel"/>        
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As4);
