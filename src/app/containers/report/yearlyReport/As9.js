import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As9 extends React.Component {
    render() {
        return (
            <div className="container">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as9" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS9</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ការគណនាប្រាក់ចំណេញសមស្រប	</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th>បរិយាយ</th>
                            <th>គំរូតារាង</th>
                            <th>តួលេខជាក់ស្ដែង សម្រាប់ឆ្នាំ២០១៥</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center', whiteSpace:'nowrap'}}>
                        <tr style={{ textAlign:'center'}}>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>(រៀល)</td>
                        </tr>
                        <tr style={{ textAlign:'center'}}>
                            <td></td>
                            <td style={{textAlign:'center', textDecoration:'underline', fontWeight:'bold', fontSize:'18px'}}>អាជីវកម្មផ្នែកផលិតកម្ម</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td style={{ textAlign:'left'}}>តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នកប្រើប្រាស់និងជំនួយ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td style={{ textAlign:'left'}}>សំវិធានធនសម្រាប់ទុនចល័ត</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td style={{ textAlign:'left'}}>តម្លៃអចលនទ្រព្យសរុប</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td style={{ textAlign:'left'}}>ដក រំលោះបូកយោង</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td style={{ textAlign:'left'}}>តម្លៃអចលនទ្រព្យសុទ្ធ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់កម្ចីសម្រាប់អចលនទ្រព្យកំពុងប្រើប្រាស់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td style={{ textAlign:'left'}}>មូលធនជាក់ស្ដែងផ្ទាល់ (5-6)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td style={{ textAlign:'left'}}>អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្ដែងផ្ទាល់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td style={{ textAlign:'left'}}>​ប្រាក់ចំណេញលើមូលធនជាក់ស្ដែងផ្ទាល់ តាមអត្រាខាងលើ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់ចំណេញសមស្រប</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{ textAlign:'left'}}>អាជីវកម្មផ្នែកចែកចាយ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td style={{ textAlign:'left'}}>តម្លៃដើមសរុបនៃអចលនទ្រព្យ (ដកវិភាគទានអ្នក ប្រើប្រាស់និងជំនួយ)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td style={{ textAlign:'left'}}>សំវិធានធនសម្រាប់ទុនចល័ត</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td style={{ textAlign:'left'}}>តម្លៃអចលនទ្រព្យសរុប</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td style={{ textAlign:'left'}}>ដក រំលេាះបូកយេាង</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td style={{ textAlign:'left'}}>តម្លៃអចលនទ្រព្យសុទ្ធ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់កម្ចីសំរាប់អចលនទ្រព្យកំពុងប្រើប្រាស់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td style={{ textAlign:'left'}}>មូលធនជាក់ស្ដែងផ្ទាល់ (5-6)</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style={{ textAlign:'left'}}></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td style={{ textAlign:'left'}}>អត្រាប្រាក់ចំណេញលើមូលធនជាក់ស្ដែងផ្ទាល់</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់ចំណេញលើមូលធនជាក់ស្ដែងផ្ទាល់ តាមអត្រាខាងលើ</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td style={{ textAlign:'left'}}>ប្រាក់ចំណេញសមស្រប</td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan={7}><p><b style={{fontWeight:'bold', fontSize:'18px'}}>សំគាល់ៈ</b> នៅពេលចុះបញ្ជីអចលនទ្រព្យតាមតារាងខាងលើ អ្នកកាន់អាជ្ងា ប័ណ្ណត្រូវដកចេញនូវវិភាគទានរបស់អ្នកប្រើប្រាស់ និងជំនួយផ្សេងៗទាំងអស់។</p></td>
                        </tr>
                    </tbody>
                </Table> 
                
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as9"
                        filename="yearly-report-as9"
                        sheet="yearly-report-as9"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As9);
