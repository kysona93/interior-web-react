import React from 'react';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class As1 extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                 <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as1" striped bordered condensed className="yearly-report-as1">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS1</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ទិន្នន័យផលិតកម្ម</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center'}}>តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ ២០១៤</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>បរិយាយ</th>
                            <th>ឯកតា</th>
                            <th>មករា</th>
                            <th>កុម្ភៈ</th>
                            <th>មិនា</th>
                            <th>មេសា</th>
                            <th>ឧសភា</th>
                            <th>មិថុនា</th>
                            <th>កក្កដា</th>
                            <th>សីហា</th>
                            <th>កញ្ញា</th>
                            <th>តុលា</th>
                            <th>វិឆ្ឆិកា</th>
                            <th>ធ្នូ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ក</td>
                            <td></td>
                            <td>ទិន្នន័យបច្ចេកទេស</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td>ថាមពលផលិត</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td>ថាមពល ប្រើប្រាស់ក្នុងផលិតកម្ម</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td>ថាមពល ប្រើប្រាស់ក្នុងផលិតកម្ម</td>
                            <td>%</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td>ថាមពល បញ្ជូន</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td>មេគុន អនុភាព</td>
                            <td>%</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>6</td>
                            <td>ចំនួនម៉ោងដំណើការ</td>
                            <td>ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>ខ</td>
                            <td></td>
                            <td>ទិន្នន័យប្រេង</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>ប្រភព (សូមបញ្ជាក់)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>7</td>
                            <td>បរិមាណ ប្រងម៉ាស៊ូតទិញចូល</td>
                            <td>លីត្រ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>8</td>
                            <td>បរិមាណ ប្រងម៉ាស៊ូត ប្រើប្រាស់</td>
                            <td>លីត្រ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>9</td>
                            <td>ថ្លៃប្រេងម៉ាស៊ូត មធ្យម</td>
                            <td>រៀល /លីត្រ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>10</td>
                            <td>ចំណាយលើ ប្រេងម៉ាស៊ូត</td>
                            <td>រៀល</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>អត្រាប្រើប្រាស់ប្រេងឥន្ទនៈ</td>
                            <td>លីត្រ​ / គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>ប្រភព (សូមបញ្ជក់)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                        <td></td>
                        <td>11</td>
                        <td>បរិមាណ ប្រងម៉ាស៊ូតទិញចូល</td>
                        <td>លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>12</td>
                        <td>បរិមាណ ប្រងម៉ាស៊ូត ប្រើប្រាស់</td>
                        <td>លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>13</td>
                        <td>ថ្លៃប្រេងម៉ាស៊ូត មធ្យម</td>
                        <td>រៀល /លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>14</td>
                        <td>ចំណាយលើ ប្រេងម៉ាស៊ូត</td>
                        <td>រៀល</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>15</td>
                        <td>អត្រាប្រើប្រាស់ប្រេងឥន្ទនៈ</td>
                        <td>លីត្រ​ / គ.វ៉.ម៉</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td>ប្រភព (សូមបញ្ជក់)</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>16</td>
                        <td>បរិមាណ ប្រេងរំអិលទិញចូល</td>
                        <td>លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>17</td>
                        <td>បរិមាណ ប្រេងរំអិល ប្រើប្រាស់</td>
                        <td>លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>18</td>
                        <td>ថ្លៃប្រេងរំអិលមធ្យម</td>
                        <td>រៀល/លីត្រ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>19</td>
                        <td>ចំណាយលើ ប្រេងរំអិល</td>
                        <td>រៀល</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>10</td>
                        <td>ចំណាយសរុប លើប្រេងឥន្ទនៈ</td>
                        <td>រៀល</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>20</td>
                        <td>អត្រាប្រើប្រាស់ប្រេងរំអិល</td>
                        <td>លីត្រ/គ.វ៉.ម៉</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan={16}></td>
                        </tr>
                        <tr>
                            <td colSpan={16}>ក.ពត៍មានផ្នែកម៉ាសីុន</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td colSpan={2}>បរិយាយ</td>
                            <td >ឯកតា</td>
                            <td colSpan={2}>ម៉ាសីុនទីមួយ</td>
                            <td colSpan={2}>ម៉ាសីុនទីពីរ</td>
                            <td colSpan={2}>ម៉ាសីុនទីបី</td>
                            <td colSpan={2}>ម៉ាសីុនទី បួន</td>
                            <td colSpan={2}>ម៉ាសុីនទី ប្រាំ</td>
                            <td colSpan={1}>សរុប</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1-</td>
                            <td colSpan={2}>ម៉ាក</td>
                            <td ></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2-</td>
                            <td colSpan={2}>អនុភាពម៉ាសីុនអូស</td>
                            <td >HP</td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3-</td>
                            <td colSpan={2}>អនុភាពអាល់ទែណាទ័រ</td>
                            <td >KVA</td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4-</td>
                            <td colSpan={2}>ម៉ាសុីន</td>
                            <td ></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5-</td>
                            <td colSpan={2}>ចំនួនម៉ោងធ្វើការ/ថ្ងៃ</td>
                            <td >h</td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td colSpan={2}>ពីម៉ោង........ទៅម៉ោង</td>
                            <td ></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td colSpan={2}>ពីម៉ោង........ទៅម៉ោង</td>
                            <td ></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={2}></td>
                            <td colSpan={1}></td>
                        </tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colSpan={9}></td>
                        </tr>
                        <tr>
                            <td colSpan={9}>ខ.ពត៏មានអំពីបុស្តិ័ ត្រង់ស្វូ</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >បរិយាយ</td>
                            <td >តង់ស្យុង</td>
                            <td >ឯកតា</td>
                            <td >ប្រវែងខ្សែរស្រោប</td>
                            <td >ប្រវែងខ្សែរស្រាត</td>
                            <td >តង់ស្យុងចុងខ្សែរ</td>
                            <td >ចំនួួនបង្គោល</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1-</td>
                            <td >ខ្សែមណ្ដាញតង់ស្យុងទាប</td>
                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >មួយហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >ពីរហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >បីហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td>2-</td>
                            <td >ខ្សែមណ្ដាញតង់ស្យុងមធ្យម</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >មួយហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >ពីរហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >បីហ្វា</td>
                            <td >m</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        
                        </tr>
                    </tbody>

                    <tbody>
                        <tr>
                            <td colSpan={7}></td>
                        </tr>
                        <tr>
                            <td colSpan={7}>គ.ពត៍មានអំពីខ្សែរបណ្ដាញ</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>លរ</td>
                            <td >បរិយាយ</td>
                            <td >តង់ស្យុងខ្ពស់</td>
                            <td >តត់ស្យុងទាប</td>
                            <td >អានុភាព</td>
                            <td >ចំនួួន</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>១</td>
                            <td >បន្ទប់ត្រង់ស្វូតម្លើង</td>
                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td ></td>
                            
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td>២</td>
                            <td >បន្ទប់ត្រង់ស្វូ</td>
                            <td ></td>
                            <td></td>
                            <td ></td>
                            <td ></td>
                            
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td>៣</td>
                            <td >ត្រង់ស្វូលើបង្គោលតម្លើង</td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            
                            
                        </tr>
                        <tr>
                            <td></td>
                            <td>៤</td>
                            <td >ត្រង់ស្វូលើបង្គោលតម្លាក់</td>
                            <td ></td>
                            <td ></td>
                            <td></td>
                            <td></td>
                            
                        
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td >22Kv</td>
                            <td >0.4kV</td>
                            <td >1000KVA</td>
                            <td >1</td>
                            
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td >22Kv</td>
                            <td >0.4Kv</td>
                            <td >160kVA</td>
                            <td >4</td>
                           
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td >22Kv</td>
                            <td >0.4Kv</td>
                            <td >100KVA</td>
                            <td >1</td>
                           
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td >22Kv</td>
                            <td >0.4Kv</td>
                            <td >30KVA</td>
                            <td >1</td>
                            
                           
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td >បីហ្វា</td>
                            <td >22Kv</td>
                            <td >0.4Kv</td>
                            <td >50KVA</td>
                            <td >1</td>
                           
                           
                        </tr>
                    </tbody>
                 </Table>
                 <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as1"
                        filename="yearly-report-as1"
                        sheet="report_ref_12_months"
                        buttonText="Export to Excel"/>
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As1);
