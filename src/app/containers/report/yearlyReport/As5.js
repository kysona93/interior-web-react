import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';

class As5 extends React.Component {
    render() {
        return (
            <div className="container">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as5" striped bordered condensed className="yearly-report-as3">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS5</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={7}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ពត៏មានលម្អិតអំពី ចំណាយរដ្ឌបាល និង ចំណាយទូទៅ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th rowSpan={2} style={{lineHeight:'3.5'}}>បរិយាយ</th>
                            <th >តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ ២០១៦</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th>(រៀល)</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign:'center'}}>
                        <tr >
                            <td>1</td>
                            <td>សេវាធនាគា</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>កម្រៃ អាជ្ងាប័ណ្ណ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>ចំណាយលើសេវាផ្នែកច្បាប់</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>ចំណាយលើការដឹកជញ្ជូន</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>ចំណាយលើបេសកកម្ម និង​ ការទទួលភ្ញៀវ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>សេវា ប្រៃសណីយ៏ និង ទូរគមនាគមន៏</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>ចំណាយលើសំភារៈការិយាល័យ</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>15</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>16</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>17</td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr style={{fontWeight:'bold',fontSize:'20px'}}>
                            <td></td>
                            <td>Total</td>
                            <td>-</td>
                        </tr>
                    </tbody>
                </Table> 
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as5"
                        filename="yearly-report-as5"
                        sheet="yearly-report-as5"
                        buttonText="Export to Excel"/>      
            </div>
        );
    }
}



const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(As5);
