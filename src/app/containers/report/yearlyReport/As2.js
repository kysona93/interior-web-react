import React from 'react';
import { Table } from 'react-bootstrap';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

class ComponentName extends React.Component {
    render() {
        return (
            <div className="container-fluid">
                <Table style={{fontFamily: "Khmer OS", fontSize: "10pt"}} id="yearly-report-as2" striped bordered condensed className="yearly-report-as1">
                    <thead hidden>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'left'}}>ឈ្មោះអ្នកកាន់អាជ្ញាប័ណ្ណ​​​ : សហគ្រាសអ៊ីនធើប៊ីភីសឹលូសិន , លេខអាជ្ញាប័ណ្ណ : ២៥៩.ឯអ</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center',textDecoration:'underline'}}>តារាង​ AS2</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center',fontFamily:'Khmer OS Muol'}}>ពត៏មានលំអិតអំពីការទិញថាមពល</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr style={{background:'none'}}>
                            <th colSpan={16}><p style={{textAlign:'center'}}>តួរលេខជាក់ស្ដែងសំរាប់ឆ្នាំ ២០១៤</p></th>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>បរិយាយ</th>
                            <th>ឯកតា</th>
                            <th>មករា</th>
                            <th>កុម្ភៈ</th>
                            <th>មិនា</th>
                            <th>មេសា</th>
                            <th>ឧសភា</th>
                            <th>មិថុនា</th>
                            <th>កក្កដា</th>
                            <th>សីហា</th>
                            <th>កញ្ញា</th>
                            <th>តុលា</th>
                            <th>វិឆ្ឆិកា</th>
                            <th>ធ្នូ</th>
                            <th>សរុប</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ក</td>
                            <td></td>
                            <td>ប្រភពមួយ១ (EDCon.)</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td>EDCon</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td>ចំណាយលើអនហុភាព​ (រៀល)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td>ចំណាយលើការទិញថាមពល (រៀល)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td>ចំណាយសរុប​ (រៀល)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td>បរិមាណថាមពលទិញសរុប</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td>ចំណាយជាមធ្យម (រៀល/គីឡូវ៉ាត់ម៉ោង)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    
                        <tr>
                            <td>ខ</td>
                            <td></td>
                            <td>ប្រភពពីរ (ឈ្នោះ)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td>ចំណាយលើអនហុភាព​​ (រៀល)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td>ចំណាយលើការទិញថាមពល (រៀល)</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td>ចំណាយសរុប​</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td>បរិមាណថាមពលទិញសរុប</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td>ចំណាយជាមធ្យម (រៀល/គីឡូវ៉ាត់ម៉ោង)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>

                        <tr>
                            <td>គ</td>
                            <td></td>
                            <td>ប្រភពបី (ឈ្នោះ)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1</td>
                            <td>ចំណាយលើអានុភាព​​​ (រៀល)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>2</td>
                            <td>ចំណាយលើការទិញថាមពល (រៀល)</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>3</td>
                            <td>ចំណាយសរុប​</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>4</td>
                            <td>បរិមាណថាមពលទិញសរុប</td>
                            <td>គ.វ៉.ម៉</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>5</td>
                            <td>ចំណាយជាមធ្យម​ (រៀល/គីឡូវ៉ាត់ម៉ោង)</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>


                    </tbody>
                </Table>  
                <ReactHTMLTableToExcel
                        id="button_report_ref_12_months"
                        className="download-table-xls-button"
                        table="yearly-report-as2"
                        filename="yearly-report-as2"
                        sheet="yearly-report-as2"
                        buttonText="Export to Excel"/>  
            </div>
        );
    }
}


const mapStateToProps = (state) => ({
  
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ComponentName);
