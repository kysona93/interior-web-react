import React from 'react';
import HeaderNavigation from '../../components/shares/HeaderNavigation';
import MenuPicture from '../../components/shares/MenuPicture';
import FooterWeb from '../../components/shares/FooterWeb';
import MenuWeb from '../../components/shares/MenuWeb';
import BodyPortfolio from './BodyPortfolio';

class Portfolio extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <HeaderNavigation />
                {/*<MenuWeb />*/}
                <MenuPicture name="images/menu-image-portfolio.jpg"/>
                <BodyPortfolio />
                <FooterWeb />
            </div>
        )
    }
}

export default Portfolio;