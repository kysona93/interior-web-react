import React from 'react';
import {Image, Row, Col} from 'react-bootstrap';
import './portfolio.css';

class BodyPortfolio extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div className="container">
                <div>
                    <h1 className="pfl-title">
                        Apsara Home & Furniture
                    </h1>
                    <h1 className="pfl-desc">
                        we give our clients with awesome designing and well executed.
                    </h1>
                    <div style={{marginTop:'25px'}}>
                        <Row>
                            <Col sm={6} md={3}>
                                <Image src="images/port2.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port1.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port3.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port7.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop:'25px'}}>
                            <Col sm={6} md={3}>
                                <Image src="images/port5.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port6.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port4.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port8.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                        </Row>
                        <Row style={{marginTop:'25px'}}>
                            <Col sm={6} md={3}>
                                <Image src="images/port9.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port12.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port11.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                            <Col sm={6} md={3}>
                                <Image src="images/port10.jpg" responsive style={{width:'555px', height:'200px'}}/>
                            </Col>
                        </Row>
                        <br/><br/>
                        <h1 className="pfl-desc cmp-align-center" style={{marginTop:'80px'}}>
                            We are glad that we have been success with many clients in Cambodia. <br/>
                            We would love to share more of our work with you.
                        </h1>
                        <Row style={{marginTop:'60px'}}>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port13.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port14.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port15.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                        </Row>
                        <Row style={{marginTop:'50px'}}>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port16.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port17.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                            <Col sm={12} md={4}>
                                <h3 className="pfl-company">SEVEN BRIGHT</h3>
                                <Image src="images/port18.jpg" responsive style={{width:'555px', height:'316px'}}/>
                                <p className="pfl-company-desc">
                                    The villa in Cam Ko City. Our company response all interior things for that house.
                                </p>
                            </Col>
                        </Row>
                        <br/><br/><br/>

                </div>
                </div>
            </div>
        );
    }
}
export default BodyPortfolio;