import React from 'react';
import { } from 'react-bootstrap';
import HeaderNavigation from '../components/shares/HeaderNavigation';
import MenuPicture from '../components/shares/MenuPicture';
import CarouselSlider from '../components/shares/CarouselSlider';
import BodyHome from './home/BodyHome';
import FooterWeb from '../components/shares/FooterWeb';
import MenuWeb from '../components/shares/MenuWeb';

class Home extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div>
                <HeaderNavigation />
                {/*<MenuWeb />*/}
                <MenuPicture name="images/menu-image-home.jpg"/>
                {/*<CarouselSlider />*/}
                <BodyHome />
                <FooterWeb />
            </div>
            )
    }
}

export default  Home;
