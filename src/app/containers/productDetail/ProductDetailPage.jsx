import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './detail.css';
import HeaderNavigation from '../../components/shares/HeaderNavigation';
import CarouselSlider from '../../components/shares/CarouselSlider';
import MenuWeb from '../../components/shares/MenuWeb';
import BodyProductDetail from './BodyProductDetail';
import FooterWeb from '../../components/shares/FooterWeb';

class ProductDetailPage extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div>
                <HeaderNavigation />
                <MenuWeb />
                <CarouselSlider />
                <BodyProductDetail />
                <FooterWeb />
            </div>
        );
    }
}

export default ProductDetailPage;