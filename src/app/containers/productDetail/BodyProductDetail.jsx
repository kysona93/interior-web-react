import React from 'react';
import { Row, Col, Thumbnail, Button, Form, FormGroup, FormControl, Pagination  } from 'react-bootstrap';
import {Link} from 'react-router';
import './detail.css';

class BodyProductDetail extends React.Component{
    constructor(props){
        super(props);

    }

    render() {

        return (
            <div className="container">
                <div className="filter-product">
                    <Form inline>
                        <FormGroup controlId="formControlsSelect">
                            <FormControl componentClass="select" placeholder="Category">
                                <option value="select">Choose Category</option>
                                <option value="other">...</option>
                            </FormControl>
                        </FormGroup>&nbsp;&nbsp;&nbsp;&nbsp;
                        <FormGroup controlId="formInlineEmail">
                            <FormControl type="text" placeholder="From Price" />
                            <span>To</span>
                            <FormControl type="text" placeholder="To Price" />
                        </FormGroup>{' '}
                        <Button type="submit">Search</Button>
                    </Form>
                </div>

                <div>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                        <Col sm={6} md={4}>
                            <Thumbnail src="images/item2.jpg" alt="242x200">
                                <h3>I'm a Product</h3>
                                <p>Price: 200$</p>
                                <p>
                                    <Button bsSize="small" bsStyle="success">View</Button>
                                </p>
                            </Thumbnail>
                        </Col>
                    </Row>


                </div>

            </div>
        );
    }
}
export default BodyProductDetail;