import React from 'react';
import HeaderNavigation from '../../components/shares/HeaderNavigation';
import MenuPicture from '../../components/shares/MenuPicture';
import FooterWeb from '../../components/shares/FooterWeb';
import MenuWeb from '../../components/shares/MenuWeb';
import ServiceBody from './ServiceBody';

class Service extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <di>
                <HeaderNavigation />
                {/*<MenuWeb />*/}
                <MenuPicture name="images/menu-image-service.jpg"/>
                <ServiceBody />
                <FooterWeb />
            </di>
        )
    }
}

export default Service;