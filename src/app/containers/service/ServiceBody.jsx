import React from 'react';
import {Row ,Col, Image } from 'react-bootstrap';
import './service.css';

class ServiceBody extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="container">
                <Row>
                    <Col sm={4} md={4}>
                        <h1 className="ser-title">SERVICE 1</h1>
                        <p className="ser-desc">
                            Whatever your company is most known for should go right here,
                            whether that's bratwurst or baseball caps or vampire bat removal.
                        </p>
                    </Col>
                    <Col sm={4} md={4}>
                        <h1 className="ser-title">SERVICE 2</h1>
                        <p className="ser-desc">
                            Whatever your company is most known for should go right here,
                            whether that's bratwurst or baseball caps or vampire bat removal.
                        </p>
                    </Col>
                    <Col sm={4} md={4}>
                        <h1 className="ser-title">SERVICE 3</h1>
                        <p className="ser-desc">
                            Whatever your company is most known for should go right here,
                            whether that's bratwurst or baseball caps or vampire bat removal.
                        </p>
                    </Col>
                </Row>

                <div style={{marginTop:'70px'}}>
                    <h2 className="cmp-align-center ser-title-room">OUR WORKS FOR CLIENTS</h2>
                    <div>
                        <h3 className="ser-title-room">Living Room</h3>
                        <p className="ser-desc-room">
                            Some people call it a living room, others, a lounge, or more formally, a sitting room.
                            But however you refer to it, there’s no doubting this room’s main purpose in your home:
                            to be a comfortable space where the household can relax.
                        </p>
                        <Image src="images/livingroom.jpg" responsive style={{width:'100%', height:'700px'}}/>
                    </div>
                </div>
                <div style={{marginTop:'50px'}}>
                    <div>
                        <h3 className="ser-title-room">Bedroom</h3>
                        <p className="ser-desc-room">
                            Some people call it a living room, others, a lounge, or more formally, a sitting room.
                            But however you refer to it, there’s no doubting this room’s main purpose in your home:
                            to be a comfortable space where the household can relax.
                        </p>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/bedroom1.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/bedroom2.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                        </Row>

                    </div>
                </div>
                <div style={{marginTop:'50px'}}>
                    <div>
                        <h3 className="ser-title-room">Kitchen</h3>
                        <p className="ser-desc-room">
                            Some people call it a living room, others, a lounge, or more formally, a sitting room.
                            But however you refer to it, there’s no doubting this room’s main purpose in your home:
                            to be a comfortable space where the household can relax.
                        </p>
                        <Image src="images/kitchen.jpg" responsive style={{width:'100%', height:'650px'}}/>
                    </div>
                </div>

                <div style={{marginTop:'50px'}}>
                    <div>
                        <h3 className="ser-title-room">Kid Bedroom</h3>
                        <p className="ser-desc-room">
                            Some people call it a living room, others, a lounge, or more formally, a sitting room.
                            But however you refer to it, there’s no doubting this room’s main purpose in your home:
                            to be a comfortable space where the household can relax.
                        </p>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/kidroom1.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/kidroom2.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                        </Row>
                    </div>
                </div>
                <div style={{marginTop:'50px'}}>
                    <div>
                        <h3 className="ser-title-room">Shop</h3>
                        <p className="ser-desc-room">
                            Some people call it a living room, others, a lounge, or more formally, a sitting room.
                            But however you refer to it, there’s no doubting this room’s main purpose in your home:
                            to be a comfortable space where the household can relax.
                        </p>
                        <Row>
                            <Col sm={6} md={6}>
                                <Image src="images/shop1.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                            <Col sm={6} md={6}>
                                <Image src="images/shop2.jpg" responsive style={{width:'100%', height:'350px'}}/>
                            </Col>
                        </Row>

                    </div>
                </div>
                <br/><br/>
            </div>
        )
    }
}

export default ServiceBody;

