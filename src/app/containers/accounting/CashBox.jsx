import React from 'react';
import {connect} from 'react-redux';
import { Row, Col } from 'react-bootstrap';
import { Field, reduxForm, initialize } from 'redux-form';
import { bindActionCreators } from 'redux';
import { TextBox } from '../../components/forms/TextBox';
import SelectBox from '../../components/forms/SelectBox';
import { DateTimePicker } from '../../components/forms/DateTimePicker';
import { TextArea } from '../../components/forms/TextArea';
import { ButtonSubmit } from '../../components/forms/ButtonSubmit';
import '../../components/forms/Styles.css';
import { getAllUsersCashBoxAction, addCashBoxAction, getAllCashBoxesAction } from './../../actions/accounting/cashBox';
import { getAllLocationPayAction } from './../../actions/customer/finance/locationPay';
import moment from 'moment';

class CashBox extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            createDate: null,
            accountId: 0,
            users: [],
            locations: [],
            labelForm: "Add New Cash Box"
        };
    }

    componentWillMount(){
        this.props.getAllUsersCashBoxAction();
        this.props.getAllLocationPayAction();
        this.props.getAllCashBoxesAction();
    }

    componentWillReceiveProps(data){
        /* get all users */
        if(data.getAllUsersCashBoxes.status === 200){
            this.setState({users: []});
            let users = [];
            let items = data.getAllUsersCashBoxes.data.items;
            items.forEach((element) => {
                users.push({
                    id: element.userId,
                    name: element.nameEn
                })
            });
            this.setState({users: users});
        }
        /* location */
        if(data.getAllLocationPays.status === 200){
            this.setState({locations: []});
            let locations = [];
            let items = data.getAllLocationPays.data.items;
            items.forEach((element) => {
                locations.push({
                    id: element.id,
                    name: element.location
                })
            });
            this.setState({locations: locations});
        }
        if(data.addCashBox.status === 200){
            alert("Successfully added new cash box.");
            this.props.dispatch(initialize('form_cash_box', null));
            this.props.getAllCashBoxesAction();
            data.addCashBox.status = 0;
        }
        if(data.addCashBox.status === 401 || data.addCashBox.status === 404 || data.addCashBox.status === 500){
            alert("Fail with add new cash box!");
        }
    }

    handleCreateDate(date){
        this.setState({createDate: date});
    }

    handleSubmit(values){
        let data = {
            "createDate": moment(values.createDate).format("YYYY-MM-DD"),
            "description": values.description,
            "name": values.name,
            "paymentLocationId": Number(values.paymentLocationId),
            "userId": Number(values.userId)
        };
        this.props.addCashBoxAction(data);
    }

    render(){
        const {handleSubmit, error, invalid, submitting} = this.props;
        return(
            <div className="container-fluid margin_left_25">
                <Row>
                    <Col xs={8} sm={8} md={8} lg={8}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h3 className="panel-title">{this.state.labelForm}</h3>
                            </div>
                            <div className="panel-body">
                                <form onSubmit={handleSubmit(this.handleSubmit.bind(this))} className="wrap-full-form">
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Cash Name <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="name" type="text" component={TextBox} label="Name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>User <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="userId" type="select" component={SelectBox}
                                                   placeholder="Select user"
                                                   values={this.state.users}
                                                   sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Location <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="paymentLocationId" type="select" component={SelectBox}
                                                   placeholder="Select location"
                                                   values={this.state.locations}
                                                   sortBy="name" icon=""/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Date <span className="label-require">*</span></strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="createDate" component={DateTimePicker} placeholder="Create Date"
                                                   defaultDate={this.state.createDate}
                                                   handleChange={this.handleCreateDate.bind(this)}/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={4} lg={4} className="label-name">
                                            <strong>Description </strong>
                                        </Col>
                                        <Col md={8} lg={8}>
                                            <Field name="description" type="text" component={TextArea} label="Description" />
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={2} lg={2} className="pull-right">
                                            <ButtonSubmit error={error} invalid={invalid} submitting={submitting} label="Save" />
                                        </Col>
                                    </Row>
                                </form>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col lg={12}>
                        <table className="page-permision-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>User</th>
                                <th>Location</th>
                                <th>Create Date</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            { this.props.getAllCashBoxes.status === 200 ?
                                <tbody>
                                {
                                    this.props.getAllCashBoxes.data.items.map((account,index)=>{
                                        return(
                                            <tr key={index}>
                                                <td>{account.cashId}</td>
                                                <td>{account.name}</td>
                                                <td>{account.nameEn}</td>
                                                <td>{account.location}</td>
                                                <td>{moment(account.createDate).format("DD-MM-YYYY")}</td>
                                                <td>{account.description}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                                :
                                <tbody>
                                <tr>
                                    <td colSpan={6}><h2>RESULT NOT FOUND</h2></td>
                                </tr>
                                </tbody>
                            }
                        </table>
                    </Col>
                </Row>
            </div>
        )
    }

}

CashBox = reduxForm({
    form: 'form_cash_box',
    validate: (values) => {
        const errors = {};
        if (values.name === undefined) {
            errors.name = 'Name is required!';
        }
        if (values.userId === undefined) {
            errors.userId = 'User is required!';
        }
        if (values.paymentLocationId === undefined) {
            errors.paymentLocationId = 'Location is required!';
        }
        if (values.createDate === undefined) {
            errors.createDate = 'Create Date is required!';
        }
        return errors;
    }
})(CashBox);

function mapStateToProps(state) {
    //console.log("DATA : ",state.cashBoxes.getAllCashBoxes);
    return {
        addCashBox: state.cashBoxes.addCashBox,
        getAllCashBoxes: state.cashBoxes.getAllCashBoxes,
        getAllUsersCashBoxes: state.cashBoxes.getAllUsersCashBoxes,
        getAllLocationPays: state.locationPays.getAllLocationPays
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({ getAllUsersCashBoxAction, getAllLocationPayAction, addCashBoxAction, getAllCashBoxesAction },dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(CashBox);