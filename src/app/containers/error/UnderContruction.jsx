import React from 'react';
import { Link, browserHistory } from 'react-router';

export default class UnderContruction extends React.Component {
    render(){
        return(
            <div style={{paddingLeft: 50}}>
                <h3><font color="#FF9908"><i className="fa fa-exclamation-circle" /></font> Under Construction</h3>
                <p>
                    Sorry, this page is currently under construction.
                </p>
                <p>
                    <Link style={{textDecoration: 'underline'}} to="#" onClick={() => window.history.back()}><i>Back</i></Link>&nbsp;&nbsp;
                    <Link style={{textDecoration: 'underline'}} to="/app"><i>Go to dashboard</i></Link></p>
            </div>
        )
    }
}
