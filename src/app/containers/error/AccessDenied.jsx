import React from 'react';
import { Link, browserHistory } from 'react-router';

export default class AccessDenied extends React.Component {
    render(){
        return(
            <div style={{paddingLeft: 50}}>
                <h3><font color="#FF9908"><i className="fa fa-exclamation-triangle" /></font> Access Denied</h3>
                <p>
                    You do not have permissions to open this page in the browser. <br/>
                    Please contact the Network Administrator if you think there has been an error.
                </p>
                <p>
                    <Link style={{textDecoration: 'underline'}} to="#" onClick={() => window.history.back()}><i>Back</i></Link>&nbsp;&nbsp;
                    <Link style={{textDecoration: 'underline'}} to="/app"><i>Go to dashboard</i></Link></p>
            </div>
        )
    }
}
