import React from '../../../node_modules/react';
import {Tab} from 'react-draggable-tab';
import { authObj, clearAuth, clearPages } from './../../auth';
import MenuLeft from './../components/shares/menu-left/MenuLeft';
import DynamicTabs from './dynamic-tabs/DynamicTabs';
import { defaultTabs } from './dynamic-tabs/Contents';
import './../../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css';
import './index.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { listSettingAction } from '../actions/setup/setting';

class Layout extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            open: true,
            tabs: defaultTabs(window.location.pathname, this.props.location.query),
            customer: {}
        };
        this.handleNewTab = this.handleNewTab.bind(this);
        this.handleCloseTab = this.handleCloseTab.bind(this);
        this.handleSelectTab = this.handleSelectTab.bind(this);
        this.handleCustomer = this.handleCustomer.bind(this);
    }

    componentWillMount() {
        this.props.listSettingAction();
        
        // function when browser no action.
        let events = ['click', 'mousemove', 'keydown'],
            i = events.length,
            timer,
            delay = 900000, // 15min
            logout = function () {
                // do whatever it is you want to do
                // after a period of inactivity

                // action clear token.
                Layout.handleClearAuth();
            },
            reset = function () {
                clearTimeout(timer);
                timer = setTimeout(logout, delay);
            };

        while (i) {
            i -= 1;
            document.addEventListener(events[i], reset, false);
        }
        reset();
    }

    componentDidMount() {
        Layout.openNav();

        //check expired token
        const et = authObj().exp;
        const ct = new Date().getTime() / 1000;

        if (et < ct) {
            // action clear token.
            Layout.handleClearAuth();
        }
    }

    static openNav() {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "285px";
    }

    static closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }

    handleNewTab(key, title) {
        if (this.state.tabs.find(tab => tab.key === key)) {
            this.handleSelectTab(key, this.state.tabs)
        } else {
            let newTab = (<Tab key={key} title={title}>
                <div> </div>
            </Tab>);
            let newTabs = this.state.tabs.concat([newTab]);
            this.setState({
                tabs: newTabs,
                selectedTab: key
            });
        }
    }

    handleSelectTab(key, tabs){
        this.setState({selectedTab: key, tabs: tabs});
    }
    handleCloseTab(tabs){
        this.setState({tabs: tabs});
    }

    static handleClearAuth() {
        clearAuth();
        clearPages();
    }

    handleCustomer(customer){
        this.setState({customer: Object.assign({}, customer)});
    }

    renderChildren() {
        return React.Children.map(this.props.children, child => {
            /*if (child.type === React.Children.Child) {
             return React.cloneElement(child, {
             test: Layout.handleTest
             })
             } else {
             return child
             }*/
            return React.cloneElement(child, {tab: this.handleNewTab, handleCustomer: this.handleCustomer, info: this.state.customer})
        });
    }

    render(){
        return(
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12 paddingLR">
                        <nav className="navbar navbar-inverse">
                            <div className="container-fluid">
                                <div className="navbar-header">
                                    <a className="navbar-brand blog-menu" href="/app"><img src="/icons/bnd_copyright.png"/> </a>
                                    <span className="open-menu" style={{fontSize:'18px',cursor:'pointer'}} onClick={Layout.openNav}>&#9776;</span>
                                </div>
                                <ul className="nav navbar-nav navbar-right">
                                    <li><a href="#"><span className="glyphicon glyphicon-user"> </span> Sign Up</a></li>
                                    <li><a href="" onClick={() => Layout.handleClearAuth()}><span className="glyphicon glyphicon-log-in"> </span> Log Out</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12 paddingLR">
                        <div id="main">
                            <DynamicTabs activeTab={this.state.selectedTab} tabs={this.state.tabs} closeTab={this.handleCloseTab} selectTab={this.handleSelectTab} defaultTab={window.location.pathname} />
                            <br />
                            <br />
                            {this.renderChildren()}
                        </div>
                    </div>
                </div>
                <div>
                    <div id="mySidenav" className="sidenav">
                        <a href="/app"> <img src="/icons/bnd_copyright.png"/></a>
                        <a href="javascript:void(0)" className="closebtn" onClick={Layout.closeNav}>&times;</a>
                        <MenuLeft tab={this.handleNewTab} />
                    </div>
                </div>
            </div>

        )
    }
}

function mapStateToProps(state) {
    if(state.setting.listSetting.items !== undefined) 
        localStorage.setItem('_setting', JSON.stringify(state.setting.listSetting.items));
    return({
        listSetting: state.setting.listSetting
    })
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({listSettingAction}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);