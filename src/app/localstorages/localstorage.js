import { browserHistory } from 'react-router';

export const loadCustomer = () => {
    try {
        const serializedState = localStorage.getItem('customer');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    }
    catch (error) {
        console.log(error);
    }
};

export const saveLicense = (state) => {
    try {
        localStorage.removeItem('license');
        const serializedState = JSON.stringify(state);
        localStorage.setItem('license', serializedState);
    } catch (error) {
        console.log(error);
    }
};
export const loadLicense = () => {
    try {
        const serializedState = localStorage.getItem('license');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    }
    catch (error) {
        console.log(error);
    }
};


export const loadLocation = () => {
    try {
        const serializedState = localStorage.getItem('location');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    }
    catch (error) {
        console.log(error);
    }
};

