import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';
import ICCardDailySaleReport from './../../../containers/report/prepaid/ICCardDailySaleReport';
import PrepaidPowerReport from './../../../containers/report/prepaid/PrepaidPowerReport';

export const prepaidReport =(
    <Route path="prepaid-reports">
        <Route path="ic_card_daily_sale_report" component={ICCardDailySaleReport}/>
        <Route path="prepaid-power-reports" component={PrepaidPowerReport}/>
    </Route>
);