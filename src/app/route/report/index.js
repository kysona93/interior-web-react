import React from "react";
import { Route, IndexRoute } from "react-router";
import MonthlyPrepaidReportLost from './../../containers/report/prepaid/MonthlyPrepaidReportLost';
// other reports
import Return from './../../../app/containers/report/prepaid/Return';
import ReportSubsidyPurchaseKwh from './../../../app/containers/report/prepaid/ReportSubsidyPurchaseKwh';

import As1 from './../../../app/containers/report/yearlyReport/As1';
import As2 from './../../../app/containers/report/yearlyReport/As2';
import As3 from './../../../app/containers/report/yearlyReport/As3';
import As4 from './../../../app/containers/report/yearlyReport/As4';
import As5 from './../../../app/containers/report/yearlyReport/As5';
// report ref
import { ref } from './../../containers/report/reportRef/index';
import As7 from './../../../app/containers/report/yearlyReport/As7';
import As9 from './../../../app/containers/report/yearlyReport/As9';
import As10 from './../../../app/containers/report/yearlyReport/As10';
import As11 from './../../../app/containers/report/yearlyReport/As11';
import As12 from './../../../app/containers/report/yearlyReport/As12';
import Fa12 from './../../../app/containers/report/yearlyReport/Fa12';
import Fa12DP from './../../../app/containers/report/yearlyReport/Fa12DP';

import { prepaidReport } from './prepaid-report/index';

export const report = (
    <Route path="reports">
        {ref}
        {prepaidReport}
        <Route path="monthly-report-lost-kwh" components={MonthlyPrepaidReportLost} />

        <Route path="as1" component={As1}/>
        <Route path="as2" component={As2}/>
        <Route path="as3" component={As3}/>
        <Route path="as4" component={As4}/>
        <Route path="as5" component={As5}/>

        <Route path="as7" component={As7}/>
        <Route path="as9" component={As9}/>
        <Route path="as10" component={As10}/>
        <Route path="as11" component={As11}/>
        <Route path="as12" component={As12}/>
        <Route path="fa12" component={Fa12}/>
        <Route path="fa12dp" component={Fa12DP}/>

        <Route path="/return" component={Return}/>
        {/*<Route path="/min-report" component={MiniReport}/>
        <Route path="report-subsidy-purchase-kwh" component={ReportSubsidyPurchaseKwh} />*/}
        
    </Route>
);