import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../auth';
import Add from './../../containers/inventory/item/Add';
import List from './../../containers/inventory/item/List';
import ItemGroup from './../../containers/inventory/itemGroup/ItemGroup';
import AddStockInItem from './../../containers/inventory/stockInItem/AddStockInItem';
import ListStockInItem from './../../containers/inventory/stockInItem/ListStockInItem';
import UnitType from './../../containers/inventory/unitType/UnitType';
import AddSupplier from '../../containers/inventory/supplier/AddSupplier';
import ListSupplier from '../../containers/inventory/supplier/ListSupplier';
import ListStockItem from '../../containers/inventory/stockItem/ListStockItem';
import ListPlan from './../../containers/inventory/plan/List';
import AddPlan from './../../containers/inventory/plan/Add';
import ListRequest from './../../containers/inventory/request/List';
import AddRequest from './../../containers/inventory/request/Add';
// import ControlRequest from './../../containers/inventory/request/ControllRequest';
// import AddPlanning from './../../containers/inventory/request/AddPlanning';
// import AddAccessoryConnection from './../../containers/inventory/request/AddAccesseryConnection';
// import AddAccessoryPlanning from './../../containers/inventory/request/AddAccessoryPlanning';
// import Report from './../../containers/inventory/request/Report';
import ListPurchaseKwh from './../../containers/inventory/purchaseKwh/List';
import DashboardPurchaseKwh from './../../containers/inventory/purchaseKwh/Dashboard';

export const inventory =(
    <Route path="inventory">
        <Route path="items">
            <IndexRoute components={List} /> {/* onEnter={permission.checkPermission} */}
            <Route path="add" components={Add} />{/* onEnter={permission.checkCanCreate} */}
        </Route>

        <Route path="plans">
            <IndexRoute components={ListPlan} />
            <Route path="add" components={AddPlan}  />
        </Route>

        <Route path="purchase-kwh">
            <IndexRoute components={ListPurchaseKwh} />
            <Route path="add" components={DashboardPurchaseKwh}  />
        </Route>

        <Route path="requests">
            <IndexRoute components={ListRequest} />
            <Route path="add" components={AddRequest}  />
            {/*<Route path="control-request" components={ControlRequest}  />*/}
            {/*<Route path="add-plan" components={AddPlanning}  />*/}
            {/*<Route path="add-conn" components={AddAccessoryConnection}  />*/}
            {/*<Route path="add-plans" components={AddAccessoryPlanning}  />*/}
            {/*<Route path="report" components={Report}  />*/}
        </Route>

        <Route path="unit-types" components={UnitType} />
        <Route path="item-groups" components={ItemGroup} />
        <Route path="stockitems" components={ListStockItem} />

        <Route path="stockin/add" components={AddStockInItem} />
        <Route path="stockins" components={ListStockInItem} />

        <Route path="supplier" > 
            <IndexRoute components={ListSupplier} onEnter={permission.checkPermission} />
            <Route path="add" components={AddSupplier} />
        </Route>
    </Route>
);