import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../../auth';
import PrepaidPower from '../../../containers/transaction/prepaid/PrepaidPower';
import GenerateBalanceKwh from '../../../containers/transaction/prepaid/GenerateBalanceKwh';
import IssueBillPrepaid from '../../../containers/transaction/prepaid/IssueBillPrepaid';
import InvoicePrepaidPrinting from '../../../containers/transaction/prepaid/InvoicePrepaidPrinting';
import LostKwh from '../../../containers/transaction/prepaid/LostKwh';
import ListEndPurchase from '../../../containers/transaction/prepaid/ListEndPurchase';
import GenerateKwPerDay from '../../../containers/transaction/prepaid/GenerateKwPerDay';
import ReportRefInvoice_50_Kw from '../../../containers/report/reportRef/ReportRefInvoice_50_Kw';
import ReportRefHistory12Month from '../../../containers/report/reportRef/ReportRefHistory12Month';
import AddBalancePrepaidKw from '../../../containers/transaction/prepaid/AddBalancePrepaidKw';

export const prepaid = (
    <Route path="prepaid">
            <Route path="import-prepaid-power" components={PrepaidPower} />
            <Route path="verify-end-purchase" components={ListEndPurchase} />
            <Route path="generate-kw-per-day" components={GenerateKwPerDay} />
            <Route path="generate-balance" components={GenerateBalanceKwh} />
            <Route path="issue-bill-prepaid" components={IssueBillPrepaid} />
            <Route path="invoice-prepaid-printing" components={InvoicePrepaidPrinting} />
            <Route path="lost-kwh" components={LostKwh} />
            <Route path="report-ref-greater-50-kw" components={ReportRefInvoice_50_Kw} />
            <Route path="report-ref-history-12-month" components={ReportRefHistory12Month} />
            <Route path="save-balance" components={AddBalancePrepaidKw} />
    </Route>
);