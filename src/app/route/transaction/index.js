import React from "react";
import { Route, IndexRoute } from "react-router";

import { postpaid } from './postpaid/index';
import { prepaid } from './prepaid/index';

export const transaction = (
    <Route path="transactions">
        { postpaid }
        { prepaid }
    </Route>
);