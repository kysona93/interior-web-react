import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../../auth';

import RecordKhwMonthly from '../../../containers/transaction/postpaid/RecordKhwMonthly';
import UsageEntry from '../../../containers/transaction/postpaid/UsageEntry';
import IssueBill from '../../../containers/transaction/postpaid/IssueBill';
import InvoicePrinting from '../../../containers/transaction/postpaid/InvoicePrinting';
import QuickPayment from '../../../containers/transaction/postpaid/QuickPayment';
import ReuseMeter from '../../../containers/transaction/postpaid/ReuseMeter';
import ListInvoicesToPunish from '../../../containers/transaction/punish/ListInvoicesToPunish';
import ListPunishCustomer from '../../../containers/transaction/punish/ListPunishCustomer';

export const postpaid = (
    <Route path="postpaid">
        <Route path="record-khw-monthly" components={RecordKhwMonthly} />
        <Route path="usage-entry" components={UsageEntry} />
        <Route path="issue-bill" components={IssueBill} />
        <Route path="invoice-printing" components={InvoicePrinting} />
        <Route path="quick-payment" components={QuickPayment} />
        <Route path="reuse-meter" components={ReuseMeter} />
        <Route path="list-invoices-to-punish" components={ListInvoicesToPunish} />
        <Route path="list-punish-customers" components={ListPunishCustomer} />
    </Route>
);