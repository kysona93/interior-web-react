import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../auth';
import { setup } from './setup/index';
import { employee } from './employee/index';
import { setupInvoice } from './invoice/index';

export const administration =(
    <Route path="administration">
        { setup }
        { employee }
        { setupInvoice }
    </Route>
);