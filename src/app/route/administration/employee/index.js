import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';
import AddEmployee from './../../../containers/administration/setup/employee/Add';
import ListEmployee from './../../../containers/administration/setup/employee/List';

export const employee =(
    <Route path="employees">
        <IndexRoute component={ListEmployee}/>
        <Route path="add" component={AddEmployee}/>
    </Route>
);