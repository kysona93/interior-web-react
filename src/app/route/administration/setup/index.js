import React from "../../../../../node_modules/react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';

import { type } from './type';

import SaleType from '../../../containers/administration/setup/type/CustomerSaleType';
import AddLocation from './../../../containers/administration/setup/location/Add';
import Occupation from './../../../containers/administration/setup/occupation/Occupation';
import AddRatePerKwh from './../../../containers/administration/setup/ratePerKwh/AddRatePerKwh';

import AddArea from './../../../containers/administration/setup/area/Add';

import AddTransformer from './../../../containers/administration/setup/transformer/Add';
import ListTransformer from './../../../containers/administration/setup/transformer/List';

import AddMeter from './../../../containers/administration/setup/meter/Add';
import ListMeter from './../../../containers/administration/setup/meter/List';

import AddPole from './../../../containers/administration/setup/pole/Add';
import ListPole from './../../../containers/administration/setup/pole/List';

import AddBox from './../../../containers/administration/setup/box/Add';
import ListBoxes from './../../../containers/administration/setup/box/List';
import EditBox from './../../../containers/administration/setup/box/Edit';

import ListBreaker from './../../../containers/administration/setup/breaker/List';
import AddBreaker from './../../../containers/administration/setup/breaker/Add';

import Ampere from '../../../containers/administration/setup/ampere/Ampere';

import AddExchangeRate from './../../../containers/administration/setup/exchangeRate/AddExchangeRate';

import Setting from './../../../containers/administration/setup/setting/Setting';
import Bank from './../../../containers/administration/setup/bank/Bank';

import License from './../../../containers/administration/setup/license/Add';
import ListLicense from './../../../containers/administration/setup/license/List';
import RentPart from './../../../containers/administration/setup/license/RentPart';
import RentPole from './../../../containers/administration/setup/license/RentPole';
import ViewRentPart from './../../../containers/administration/setup/license/ViewPoleDetail';
import Installer from  './../../../containers/administration/setup/installer/Installer.jsx';
import ListInstaller from  './../../../containers/administration/setup/installer/ListInstaller.jsx';

import PunishRate from  './../../../containers/administration/setup/punishRate/PunishRate.jsx';


export const setup =(
    <Route path="setup">
        {/** types */}
        { type }
        {/** breaker **/}
        <Route path="breakers">
            <IndexRoute component={ListBreaker}/>
            <Route path="add" component={AddBreaker}/>
        </Route>
        {/** Customer sale type **/}
        <Route path="sales" component={SaleType}/>
        {/** Location **/}
        <Route path="locations" component={AddLocation} />
        {/** Occupation */}
        <Route path="occupations" component={Occupation} />
        {/** Rate per kwh **/}
        <Route path="rate-per-kwh" component={AddRatePerKwh} />

        /** Area **/
        <Route path="areas" component={AddArea} />
        /* Installer */
        <Route path="installers">
            <IndexRoute component={ListInstaller}/>
            <Route path="add" component={Installer}/>
        </Route>
        {/** transformer **/}
        <Route path="transformers">
            <IndexRoute component={ListTransformer}/>
            <Route path="add" component={AddTransformer}/>
        </Route>
        {/** Meter **/}
        <Route path="meters">
            <IndexRoute component={ListMeter}/>
            <Route path="add" component={AddMeter}/>
        </Route>
        {/** Pole **/}
        <Route path="poles">
            <IndexRoute component={ListPole}/>
            <Route path="add" component={AddPole}/>
        </Route>
        {/* box */}
        <Route path="boxes">
            <IndexRoute component={ListBoxes}/>
            <Route path="edit" component={EditBox}/>
            <Route path="add" component={AddBox}/>
        </Route>
        {/* set up ampere */}
        <Route path="amperes" component={Ampere} >
            <IndexRoute component={Ampere}/>
            <Route path="add" component={Ampere}/>
        </Route>
        {/* exchange rate */}
        <Route path="exchangerates">
            <IndexRoute component={AddExchangeRate}/>
        </Route>
        {/* setting */}
        <Route path="setting" component={Setting}/>
        <Route path="banks" component={Bank}/>
        {/* License */}
        <Route path="licenses">
            <IndexRoute component={ListLicense}/>
            <Route path="add" component={License}/>
            <Route path="rent-part" component={RentPart}/>
            <Route path="rent-pole" component={RentPole}/>
            <Route path="view/:id" component={ViewRentPart}/>\
        </Route>
        {/* punish rate for invoices and customers */}
        <Route path="punish-rates" component={PunishRate} />
    </Route>
);