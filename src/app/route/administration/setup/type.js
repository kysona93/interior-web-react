import React from "../../../../../node_modules/react";
import { Route } from "react-router";
import { checkPermisstion } from './../../../../auth';
import CustomerType from './../../../containers/administration/setup/type/CustomerType';
import PoleType from './../../../containers/administration/setup/type/PoleType';
import BoxType from './../../../containers/administration/setup/type/BoxType';
import TransformerType from './../../../containers/administration/setup/type/TransformerType';
import BreakerType from './../../../containers/administration/setup/type/BreakerType';
import MeterType from './../../../containers/administration/setup/type/MeterType';
import VoltageType from './../../../containers/administration/setup/type/Voltage';
import ConsumptionType from './../../../containers/administration/setup/type/ConsumptionType';
import CustomerSaleType from './../../../containers/administration/setup/type/CustomerSaleType';

export const type =(
    <Route path="types">
        <Route path="customer" component={CustomerType}/>
        <Route path="pole" component={PoleType}/>
        <Route path="box" component={BoxType}/>
        <Route path="transformer" component={TransformerType}/>
        <Route path="breaker" component={BreakerType}/>
        <Route path="meter" component={MeterType}/>
        <Route path="consumption" component={ConsumptionType}/>
        <Route path="voltage" component={VoltageType}/>
        <Route path="sale" component={CustomerSaleType}/>
    </Route>
);