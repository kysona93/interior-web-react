import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';

import AddCustomerType from './../../../containers/administration/setup/customer/type/Add';
import InvoiceType from './../../../containers/administration/invoice/InvoiceType';
import GroupInvoice from './../../../containers/administration/invoice/GroupInvoice';
import RateRange from './../../../containers/administration/invoice/rateRange/RateRange';

export const setupInvoice =(
    <Route path="setup-invoices">
        <Route path="type" component={InvoiceType}/>
        <Route path="group" components={GroupInvoice} />
        <Route path="rate-range" components={RateRange} />
    </Route>
);