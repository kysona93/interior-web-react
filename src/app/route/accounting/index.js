import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../auth';
import CashBox from '../../containers/accounting/CashBox';
//import AddCharge from "./../../containers/customer/finance/charge/AddCharge";

export const accounting = (
    <Route path="accounting">
        <Route path="cash-boxes" components={CashBox} />
        {/*<Route path="invoice" components={AddCharge} />*/}
    </Route>
);