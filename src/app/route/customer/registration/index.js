import React from "react";
//import { Route, IndexRoute } from "react-router";
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';
import { checkPermisstion } from './../../../../auth';
import AddCustomer from '../../../containers/customer/registration/Add';
import EditCustomer from '../../../containers/customer/registration/Edit';

export const registration =(
    <Route path="registration">
        <Route path="add" component={AddCustomer}/>
        <Route path="edit(/:name)" component={EditCustomer}/>
    </Route>
);