import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';
import ChangeMeter from '../../../containers/customer/network/ChangeMeter';
import LocationCustomer from '../../../containers/customer/network/LocationCustomer';
import InstallationInfo from '../../../containers/customer/network/InstallationInfo';
import MeterTransfer from '../../../containers/customer/network/MeterTransfer';
import MeterHistory from '../../../containers/customer/network/MeterHistory';
import LayoutNetworkManagement from '../../../containers/customer/network/LayoutNetworkManagement';
import AddInstalling from '../../../containers/customer/network/installation/Add';

export const network =(
    <Route path="network" component={LayoutNetworkManagement}>
        <IndexRoute component={LocationCustomer}/>
        <Route path="installation-info" components={InstallationInfo}/>
        <Route path="change-meter" components={ChangeMeter}/>
        <Route path="meter-transfer" component={MeterTransfer}/>
        <Route path="meter-history" component={MeterHistory}/>
        <Route path="installing" component={AddInstalling}/>
    </Route>
);