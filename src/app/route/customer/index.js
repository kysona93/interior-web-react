import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../auth';
import { registration } from './registration/index';
import { finance } from './finance/index';
import { network } from './network/index';
import ListCustomer from '../../containers/customer/registration/List';
import EditCustomer from '../../containers/customer/registration/Edit';

export const customer =(
    <Route path="customers(:type)(:opt)(:search)">
        <IndexRoute component={ListCustomer} />
        <Route path="info(:id)(:name)" component={EditCustomer} />
        { registration }
        { finance }
        { network }
    </Route>
);