import React from "react";
import { Route, IndexRoute } from "react-router";
import { checkPermisstion } from './../../../../auth';
import ListCustomerInvoice from '../../../containers/customer/finance/ListCustomerInvoice';
import BookPaymentCustomer from './../../../containers/customer/finance/BookPaymentCustomer';
import AddCharge from './../../../containers/customer/finance/charge/AddCharge';

export const finance =(
    <Route path="finance">
        <Route path="invoices" components={ListCustomerInvoice} />
        <Route path="bookpayment" components={BookPaymentCustomer} />
        <Route path="addcharge" components={AddCharge} />
    </Route>
);