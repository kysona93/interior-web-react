import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../auth';
import GroupDashboard from './../../containers/group/GroupDashboard';
import AddGroup from './../../containers/group/AddGroup';
import ListActiveGroups from './../../containers/group/ListActiveGroups';
import ListInactiveGroups from './../../containers/group/ListInactiveGroups';
import EditGroup from './../../containers/group/EditGroup';
import UserDashboard from './../../containers/user/UserDashboard';
import ListActiveUsers from './../../containers/user/ListActiveUsers';
import ListInactiveUsers from './../../containers/user/ListInactiveUsers';
import AddUser from './../../containers/user/AddUser';
import EditUser from './../../containers/user/EditUser';
import AssignMenu from './../../containers/menu/AssignMenu';
import ListMenu from './../../containers/menu/ListMenu';
import EditMenu from './../../containers/menu/EditMenu';
import ListMailSettings from './../../containers/security/mail-settings/ListMailSettings';

export const security =(
    <Route path="security">
        {/* groups */}
        <Route path="groups" component={GroupDashboard} onEnter={permission.checkPermission} >
            <IndexRoute component={ListActiveGroups}/>
            <Route path="inactive" component={ListInactiveGroups}/>
            <Route path="add" component={AddGroup} onEnter={permission.checkCanCreate} />
            <Route path="edit/:id" component={EditGroup} onEnter={permission.checkCanUpdate} />
        </Route>

        {/* users */}
        <Route path="users" component={UserDashboard} onEnter={permission.checkPermission}>
            <IndexRoute component={ListActiveUsers}/>
            <Route path="inactive" component={ListInactiveUsers}/>
            <Route path="add" component={AddUser} onEnter={permission.checkCanCreate} />
            <Route path="edit/:id" component={EditUser} onEnter={permission.checkCanUpdate} />
        </Route>
        
        {/* menu */}
        <Route path="menus" onEnter={permission.checkPermission}>
            <IndexRoute component={ListMenu} />
            <Route path="edit/:id" component={EditMenu} onEnter={permission.checkCanUpdate}/>
            <Route path="assign" component={AssignMenu} onEnter={permission.checkPermission}/>
        </Route>

        {/* menu */}
        <Route path="mail-settings" onEnter={permission.checkPermission}>
            <IndexRoute component={ListMailSettings} />
        </Route>
    </Route>
);