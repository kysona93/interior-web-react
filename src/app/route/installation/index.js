import React from "react";
import { Route, IndexRoute } from "react-router";
import * as permission from './../../../auth';
import ViewInstaller from '../../containers/installation/ViewInstaller';
import ViewInstallationCustomer from './../../containers/installation/ViewInstallationCustomer.jsx';
import TrackInstallingCustomer from '../../containers/installation/TrackInstallingCustomer.jsx';
import InstallingSchedule from './../../containers/installation/InstallingSchedule.jsx';
import StepDetail from '../../containers/installation/step/StepDetail';
import ListStepWork from '../../containers/installation/step/List';

export const installation = (
    <Route path="installation">
        <Route path="step">
            <IndexRoute components={StepDetail} />
            <Route path="works" components={ListStepWork} />
        </Route>
        <Route path="installers">
            <IndexRoute component={ViewInstaller}/>
            <Route path="install-customer" component={ViewInstallationCustomer}/>
            <Route path="install-schedule" component={InstallingSchedule}/>
            <Route path="track-installing" component={TrackInstallingCustomer}/>
        </Route>
        {/*<Route path="supplier" onEnter={permission.checkPermission}>*/}
        {/*<IndexRoute components={ViewInstaller} />*/}
                {/*<Route path="add" components={ViewInstaller} onEnter={permission.checkCanCreate} />*/}
        {/*</Route>*/}
    </Route>
);