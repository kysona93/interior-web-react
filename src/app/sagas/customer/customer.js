import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as CUSTOMER_TYPE from '../../actions/customer/customer';
import * as CUSTOMER_API from '../../services/customer/customer';

function* getCustomerSaga(action){
    const {response, error} = yield call(CUSTOMER_API.getCustomerApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE.GET_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.GET_CUSTOMER_FAIL, error});
    }
}

function* getCustomersSaga(action){
    const {response, error} = yield call(CUSTOMER_API.getCustomersApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE.GET_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.GET_CUSTOMERS_FAIL, error});
    }
}

function* getAllCustomersSaga(){
    const {response, error} = yield call(CUSTOMER_API.getAllCustomersApi);
    if(response){
        yield put({type: CUSTOMER_TYPE.GET_ALL_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.GET_ALL_CUSTOMERS_FAIL, error});
    }
}

function* addCustomerSaga(action){
    const {response, error} = yield call(CUSTOMER_API.addCustomerApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE.ADD_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.ADD_CUSTOMER_FAIL, error});
    }
}

function* updateCustomerSaga(action){
    const {response, error} = yield call(CUSTOMER_API.updateCustomerApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE.UPDATE_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.UPDATE_CUSTOMER_FAIL, error});
    }
}

function* deleteCustomerSaga(action){
    const {response, error} = yield call(CUSTOMER_API.deleteCustomerApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE.DELETE_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE.DELETE_CUSTOMER_FAIL, error});
    }
}

export default function* customerSaga() {
    yield fork(takeEvery, CUSTOMER_TYPE.GET_CUSTOMER, getCustomerSaga);
    yield fork(takeEvery, CUSTOMER_TYPE.GET_CUSTOMERS, getCustomersSaga);
    yield fork(takeEvery, CUSTOMER_TYPE.GET_ALL_CUSTOMERS, getAllCustomersSaga);
    yield fork(takeEvery, CUSTOMER_TYPE.ADD_CUSTOMER, addCustomerSaga);
    yield fork(takeEvery, CUSTOMER_TYPE.UPDATE_CUSTOMER, updateCustomerSaga);
    yield fork(takeEvery, CUSTOMER_TYPE.DELETE_CUSTOMER, deleteCustomerSaga);
}


