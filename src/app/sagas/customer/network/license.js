import { takeEvery } from 'redux-saga';
import { call, put, fork } from 'redux-saga/effects';

import * as RENT from './../../../actions/customer/network/license';
import * as RENT_API from './../../../services/customer/network/license';

function* addRentPoleSaga(action){
    const { response, error } = yield call(RENT_API.addRentPoleApi, action);
    if(response){ yield put({ type:RENT.ADD_RENT_POLE_SUCCESS, response });
    }else{
        yield put({ type:RENT.ADD_RENT_POLE_FAIL, error });
    }
}
function* updateRentPoleSaga(action){
    const { response, error } = yield call(RENT_API.updateRentPoleApi, action);
    if(response){ yield put({ type:RENT.UPDATE_RENT_POLE_SUCCESS, response });
    }else{
        yield put({ type:RENT.UPDATE_RENT_POLE_FAIL, error });
    }
}
function* deleteRentPoleSaga(action){
    const { response, error } = yield call(RENT_API.deleteRentPoleApi, action);
    if(response){ yield put({ type:RENT.DELETE_RENT_POLE_SUCCESS, response });
    }else{
        yield put({ type:RENT.DELETE_RENT_POLE_FAIL, error });
    }
}
function* getRentPoleSaga(action){
    const { response, error } = yield call(RENT_API.getRentPoleApi, action);
    if(response){ yield put({ type:RENT.GET_RENT_POLE_SUCCESS, response });
    }else{
        yield put({ type:RENT.GET_RENT_POLE_FAIL, error });
    }
}
function* getAllRentPoleSaga(action){
    const { response, error } = yield call(RENT_API.getAllRentPoleApi, action);
    if(response){ yield put({ type:RENT.GET_ALL_RENT_POLE_SUCCESS, response });
    }else{
        yield put({ type:RENT.GET_ALL_RENT_POLE_FAIL, error });
    }
}
/*********** RENT PART ************/
function* addRentPartSaga(action){
    const { response, error } = yield call(RENT_API.addRentPartApi, action);
    if(response){ yield put({ type:RENT.ADD_RENT_PART_SUCCESS, response });
    }else{
        yield put({ type:RENT.ADD_RENT_PART_FAIL, error });
    }
}
function* updateRentPartSaga(action){
    const { response, error } = yield call(RENT_API.updateRentPartApi, action);
    if(response){ yield put({ type:RENT.UPDATE_RENT_PART_SUCCESS, response });
    }else{
        yield put({ type:RENT.UPDATE_RENT_PART_FAIL, error });
    }
}
function* deleteRentPartSaga(action){
    const { response, error } = yield call(RENT_API.deleteRentPartApi, action);
    if(response){ yield put({ type:RENT.DELETE_RENT_PART_SUCCESS, response });
    }else{
        yield put({ type:RENT.DELETE_RENT_PART_FAIL, error });
    }
}
function* getRentPartSaga(action){
    const { response, error } = yield call(RENT_API.getRentPartApi, action);
    if(response){ yield put({ type:RENT.GET_RENT_PART_SUCCESS, response });
    }else{
        yield put({ type:RENT.GET_RENT_PART_FAIL, error });
    }
}
function* getAllRentPartSaga(action){
    const { response, error } = yield call(RENT_API.getAllRentPartApi, action);
    if(response){ yield put({ type:RENT.GET_ALL_RENT_PART_SUCCESS, response });
    }else{
        yield put({ type:RENT.GET_ALL_RENT_PART_FAIL, error });
    }
}
export default function* rentPoleSaga() {
    yield fork(takeEvery, RENT.ADD_RENT_PART, addRentPartSaga);
    yield fork(takeEvery, RENT.UPDATE_RENT_PART, updateRentPartSaga);
    yield fork(takeEvery, RENT.DELETE_RENT_PART, deleteRentPartSaga);
    yield fork(takeEvery, RENT.GET_RENT_PART, getRentPartSaga);
    yield fork(takeEvery, RENT.GET_ALL_RENT_PART, getAllRentPartSaga);

    yield fork(takeEvery, RENT.ADD_RENT_POLE, addRentPoleSaga);
    yield fork(takeEvery, RENT.UPDATE_RENT_POLE, updateRentPoleSaga);
    yield fork(takeEvery, RENT.DELETE_RENT_POLE, deleteRentPoleSaga);
    yield fork(takeEvery, RENT.GET_RENT_POLE, getRentPoleSaga);
    yield fork(takeEvery, RENT.GET_ALL_RENT_POLE, getAllRentPoleSaga);
}