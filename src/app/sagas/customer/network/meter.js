import { takeEvery } from 'redux-saga';
import { call, put, fork } from 'redux-saga/effects';

import * as METER from './../../../actions/customer/network/meter';
import * as METER_API from '../../../services/customer/network/meter';

function* updateChangeMeterSaga(action){

    const { response, error } = yield call(METER_API.updateChangeMeterApi, action);
    if(response){
        yield put({ type:METER.UPDATE_CHANGE_METER_SUCCESS, response });
    }else{
        yield put({ type:METER.UPDATE_CHANGE_METER_FAIL, error });
    }
}

function* addItemMvSaga(action){

    const { response , error } = yield call(METER_API.addItemMvApi, action);
    if(response){
        yield put({ type:METER.ADD_ITEM_MV_SUCCESS, response});
    }else{
        yield put({ type:METER.ADD_ITEM_MV_FAIL, error });
    }
}
function* updateItemMvSaga(action){

    const { response , error } = yield call(METER_API.updateItemMvApi, action);
    if(response){
        yield put({ type:METER.UPDATE_ITEM_MV_SUCCESS, response});
    }else{
        yield put({ type:METER.UPDATE_ITEM_MV_FAIL, error });
    }
}
function* deleteItemMvSaga(action){

    const { response , error } = yield call(METER_API.deleteItemsMvApi, action);
    if(response){
        yield put({ type:METER.DELETE_ITEMS_MV_SUCCESS, response});
    }else{
        yield put({ type:METER.DELETE_ITEMS_MV_FAIL, error });
    }
}
function* getItemsMvSaga(action){

    const { response , error } = yield call(METER_API.getItemsMvApi, action);
    if(response){
        yield put({ type:METER.GET_ITEMS_MV_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_ITEMS_MV_FAIL, error });
    }
}
function* getItemsMvSummarySaga(action){

    const { response , error } = yield call(METER_API.getItemsMvSummaryApi, action);
    if(response){
        yield put({ type:METER.GET_ITEMS_MV_SUMMARY_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_ITEMS_MV_SUMMARY_FAIL, error });
    }
}

function* getConnectionPointSaga(action){

    const { response , error } = yield call(METER_API.getConnectionPointApi, action);
    if(response){
        yield put({ type:METER.GET_CONNECTION_POINT_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_CONNECTION_POINT_FAIL, error });
    }
}

function* getAllItemsMvSaga(action){

    const { response , error } = yield call(METER_API.getAllItemsMvApi, action);
    if(response){
        yield put({ type:METER.GET_ALL_ITEMS_MV_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_ALL_ITEMS_MV_FAIL, error });
    }
}

function* updateTransforMeterSaga(action){

    const { response, error } = yield call(METER_API.updateTransformerApi, action);
    if(response){
        yield put({ type:METER.UPDATE_TRANSFORMER_METER_SUCCESS, response });
    }else{
        yield put({ type:METER.UPDATE_TRANSFORMER_METER_FAIL, error });
    }
}
function* getAllMeterHistorySaga(action){
    const response = yield call(METER_API.getMeterHistoryApi, action);
    if(!(response == undefined)){
        yield put({ type:METER.GET_METER_HISTORY_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_METER_HISTORY_FAIL, response })
    }
}
function* getAllMetersSaga(action){
    const {response, error } = yield call(METER_API.getFreeMetersApi, action);
    if(response){
        yield put({type:METER.GET_FREE_METERS_SUCCESS, response});
    }else{
        yield put({type:METER.GET_FREE_METERS_FAIL, error});
    }
}
function* getUsedMetersSaga(action){

    const {response, error }  = yield call(METER_API.getUsedMetersApi, action);
    if(response){
        yield put({ type:METER.GET_USED_METERS_SUCCESS, response});
    }else{
        yield put({ type:METER.GET_USED_METERS_FAIL, error })
    }
}

export default function* meterChangeSaga(){
    yield fork(takeEvery, METER.UPDATE_CHANGE_METER, updateChangeMeterSaga);
    yield fork(takeEvery, METER.UPDATE_TRANSFORMER_METER, updateTransforMeterSaga);
    yield fork(takeEvery, METER.GET_METER_HISTORY,getAllMeterHistorySaga);
    yield fork(takeEvery, METER.GET_FREE_METERS, getAllMetersSaga);
    yield fork(takeEvery, METER.GET_USED_METERS, getUsedMetersSaga);
    yield fork(takeEvery, METER.ADD_ITEM_MV, addItemMvSaga);
    yield fork(takeEvery, METER.UPDATE_ITEM_MV, updateItemMvSaga);
    yield fork(takeEvery, METER.DELETE_ITEMS_MV, deleteItemMvSaga);
    yield fork(takeEvery, METER.GET_ITEMS_MV, getItemsMvSaga);
    yield fork(takeEvery, METER.GET_ALL_ITEMS_MV, getAllItemsMvSaga);
    yield fork(takeEvery, METER.GET_ITEMS_MV_SUMMARY, getItemsMvSummarySaga);
    yield fork(takeEvery, METER.GET_CONNECTION_POINT, getConnectionPointSaga);
}