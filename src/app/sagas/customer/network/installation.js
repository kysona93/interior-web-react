import { takeEvery } from 'redux-saga';
import { call, put, fork } from 'redux-saga/effects';

import * as INSTALLING from '../../../actions/customer/network/installation';
import * as INSTALLING_API from '../../../services/customer/network/installation';

function* getInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.getInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.GET_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.GET_INSTALLING_FAIL, error });
    }
}
function* getAllInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.getAllInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.GET_ALL_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.GET_ALL_INSTALLING_FAIL, error });
    }
}
function* getScheduleInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.getScheduleInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.GET_SCHEDULE_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.GET_SCHEDULE_INSTALLING_FAIL, error });
    }
}
function* updateInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.updateInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.UPDATE_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.UPDATE_INSTALLING_FAIL, error });
    }
}
function* deleteInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.deleteInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.DELETE_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.DELETE_INSTALLING_FAIL, error });
    }
}
function* addInstallingSaga(action){
    const { response , error } = yield call(INSTALLING_API.addInstallingApi, action);
    if(response){
        yield put({ type:INSTALLING.ADD_INSTALLING_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.ADD_INSTALLING_FAIL, error });
    }
}
function* addGenerateFormRequestSaga(action){
    const { response , error } = yield call(INSTALLING_API.addGenerateFormRequestApi, action);
    if(response){
        yield put({ type:INSTALLING.ADD_GENERATE_FORM_REQUEST_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.ADD_GENERATE_FORM_REQUEST_FAIL, error });
    }
}
function* getAllGenerateFormRequestSaga(action){
    const { response , error } = yield call(INSTALLING_API.getAllGenerateFormRequestApi, action);
    if(response){
        yield put({ type:INSTALLING.GET_ALL_GENERATE_FORM_REQUEST_SUCCESS, response });
    }else{
        yield put({ type:INSTALLING.GET_ALL_GENERATE_FORM_REQUEST_FAIL, error });
    }
}

export default function* installingSaga(){
    yield fork(takeEvery, INSTALLING.GET_INSTALLING, getInstallingSaga);
    yield fork(takeEvery, INSTALLING.GET_ALL_INSTALLING, getAllInstallingSaga);
    yield fork(takeEvery, INSTALLING.GET_SCHEDULE_INSTALLING, getScheduleInstallingSaga);
    yield fork(takeEvery, INSTALLING.ADD_INSTALLING, addInstallingSaga);
    yield fork(takeEvery, INSTALLING.UPDATE_INSTALLING, updateInstallingSaga);
    yield fork(takeEvery, INSTALLING.DELETE_INSTALLING, deleteInstallingSaga);
    yield fork(takeEvery, INSTALLING.ADD_GENERATE_FORM_REQUEST, addGenerateFormRequestSaga);
    yield fork(takeEvery, INSTALLING.GET_ALL_GENERATE_FORM_REQUEST, getAllGenerateFormRequestSaga);
}