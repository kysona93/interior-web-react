import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_PAYMENT_TYPE,
    ADD_PAYMENT_TYPE_FAIL,
    ADD_PAYMENT_TYPE_SUCCESS,
    GET_ALL_PAYMENT_TYPES,
    GET_ALL_PAYMENT_TYPES_FAIL,
    GET_ALL_PAYMENT_TYPES_SUCCESS,
    UPDATE_PAYMENT_TYPE,
    UPDATE_PAYMENT_TYPE_FAIL,
    UPDATE_PAYMENT_TYPE_SUCCESS,
    DELETE_PAYMENT_TYPE,
    DELETE_PAYMENT_TYPE_FAIL,
    DELETE_PAYMENT_TYPE_SUCCESS
} from '../../../actions/customer/finance/paymentType';
import {
    addPaymentTypeApi,
    getAllPaymentTypeApi,
    updatePaymentTypeApi,
    deletePaymentTypeApi
} from './../../../services/customer/finance/paymentType';

function* addPaymentTypeSaga(action){
    const { response, error } = yield call(addPaymentTypeApi, action);
    if(response){
        yield put({type: ADD_PAYMENT_TYPE_SUCCESS, response});
    }else{
        yield put({type: ADD_PAYMENT_TYPE_FAIL, error});
    }
}

function* getAllPaymentTypesSaga(action){
    const { response, error } = yield call(getAllPaymentTypeApi, action);
    if(response){
        yield put({type: GET_ALL_PAYMENT_TYPES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_PAYMENT_TYPES_FAIL, error});
    }
}

function* updatePaymentTypeSaga(action){
    const { response, error } = yield call(updatePaymentTypeApi, action);
    if(response){
        yield put({type: UPDATE_PAYMENT_TYPE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_PAYMENT_TYPE_FAIL, error});
    }
}

function* deletePaymentTypeSaga(action){
    const { response, error } = yield call(deletePaymentTypeApi, action);
    if(response){
        yield put({type: DELETE_PAYMENT_TYPE_SUCCESS, response});
    }else{
        yield put({type: DELETE_PAYMENT_TYPE_FAIL, error});
    }
}

export default function* paymentTypesSaga() {
    yield fork(takeEvery, ADD_PAYMENT_TYPE, addPaymentTypeSaga);
    yield fork(takeEvery, GET_ALL_PAYMENT_TYPES, getAllPaymentTypesSaga);
    yield fork(takeEvery, UPDATE_PAYMENT_TYPE, updatePaymentTypeSaga);
    yield fork(takeEvery, DELETE_PAYMENT_TYPE, deletePaymentTypeSaga);
}