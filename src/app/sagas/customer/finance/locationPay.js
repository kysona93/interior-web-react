import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_LOCATION_PAY,
    ADD_LOCATION_PAY_FAIL,
    ADD_LOCATION_PAY_SUCCESS,
    GET_ALL_LOCATION_PAY,
    GET_ALL_LOCATION_PAY_FAIL,
    GET_ALL_LOCATION_PAY_SUCCESS,
    UPDATE_LOCATION_PAY,
    UPDATE_LOCATION_PAY_FAIL,
    UPDATE_LOCATION_PAY_SUCCESS,
    DELETE_LOCATION_PAY,
    DELETE_LOCATION_PAY_FAIL,
    DELETE_LOCATION_PAY_SUCCESS
} from '../../../actions/customer/finance/locationPay';
import {
    addLocationPayApi,
    getAllLocationPayApi,
    updatePLocationPayApi,
    deleteLocationPayApi
} from './../../../services/customer/finance/locationPay';

function* addLocationPaySaga(action){
    const { response, error } = yield call(addLocationPayApi, action);
    if(response){
        yield put({type: ADD_LOCATION_PAY_SUCCESS, response});
    }else{
        yield put({type: ADD_LOCATION_PAY_FAIL, error});
    }
}

function* getAllLocationPaysSaga(action){
    const { response, error } = yield call(getAllLocationPayApi, action);
    if(response){
        yield put({type: GET_ALL_LOCATION_PAY_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_LOCATION_PAY_FAIL, error});
    }
}

function* updateLocationPaySaga(action){
    const { response, error } = yield call(updatePLocationPayApi, action);
    if(response){
        yield put({type: UPDATE_LOCATION_PAY_SUCCESS, response});
    }else{
        yield put({type: UPDATE_LOCATION_PAY_FAIL, error});
    }
}

function* deleteLocationPaySaga(action){
    const { response, error } = yield call(deleteLocationPayApi, action);
    if(response){
        yield put({type: DELETE_LOCATION_PAY_SUCCESS, response});
    }else{
        yield put({type: DELETE_LOCATION_PAY_FAIL, error});
    }
}

export default function* locationPaysSaga() {
    yield fork(takeEvery, ADD_LOCATION_PAY, addLocationPaySaga);
    yield fork(takeEvery, GET_ALL_LOCATION_PAY, getAllLocationPaysSaga);
    yield fork(takeEvery, UPDATE_LOCATION_PAY, updateLocationPaySaga);
    yield fork(takeEvery, DELETE_LOCATION_PAY, deleteLocationPaySaga);
}