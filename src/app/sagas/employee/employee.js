import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as EMPLOYEE_TYPE from '../../actions/employee/employee';
import * as EMPLOYEE_API from '../../services/employee/employee';

function* getEmployeesSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.getEmployeesApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.GET_EMPLOYEES_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.GET_EMPLOYEES_FAIL, error});
    }
}

function* getAllEmployeesSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.getAllEmployeesApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.GET_ALL_EMPLOYEES_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.GET_ALL_EMPLOYEES_FAIL, error});
    }
}
function* getAllInstallersSaga(installer){
    const { response, error } = yield call(EMPLOYEE_API.getAllInstallersApi, installer);
    if(response){
        yield put({type: EMPLOYEE_TYPE.GET_ALL_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.GET_ALL_INSTALLER_FAIL, error});
    }
}

function* getInstallerReportsSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.getInstallerReportsApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.GET_INSTALLER_REPORTS_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.GET_INSTALLER_REPORTS_FAIL, error});
    }
}

function* getInstallingSchedulesSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.getInstallingSchedulesApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.GET_INSTALLING_SCHEDULES_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.GET_INSTALLING_SCHEDULES_FAIL, error});
    }
}

function* addEmployeeSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.addEmployeeApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.ADD_EMPLOYEE_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.ADD_EMPLOYEE_FAIL, error});
    }
}

function* updateEmployeeSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.updateEmployeeApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.UPDATE_EMPLOYEE_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.UPDATE_EMPLOYEE_FAIL, error});
    }
}

function* deleteEmployeeSaga(action){
    const { response, error } = yield call(EMPLOYEE_API.deleteEmployeeApi, action);
    if(response){
        yield put({type: EMPLOYEE_TYPE.DELETE_EMPLOYEE_SUCCESS, response});
    }else{
        yield put({type: EMPLOYEE_TYPE.DELETE_EMPLOYEE_FAIL, error});
    }
}

export default function* employeeSaga() {
    yield fork(takeEvery, EMPLOYEE_TYPE.GET_EMPLOYEES, getEmployeesSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.GET_ALL_EMPLOYEES, getAllEmployeesSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.GET_ALL_INSTALLER, getAllInstallersSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.GET_INSTALLER_REPORTS, getInstallerReportsSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.GET_INSTALLING_SCHEDULES, getInstallingSchedulesSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.ADD_EMPLOYEE, addEmployeeSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.UPDATE_EMPLOYEE, updateEmployeeSaga);
    yield fork(takeEvery, EMPLOYEE_TYPE.DELETE_EMPLOYEE, deleteEmployeeSaga);
}


