import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as LOCATION_TYPE from '../actions/location';
import * as LOCATION_API from '../services/location';

function* getAllLocationsSaga(){
    const {response, error} = yield call(LOCATION_API.getAllLocationsApi);
    if(response){
        yield put({type: LOCATION_TYPE.GET_ALL_LOCATIONS_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.GET_ALL_LOCATIONS_FAIL, error});
    }
}

function* addLocationSaga(action){
    const {response, error} = yield call(LOCATION_API.addLocationApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.ADD_LOCATION_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.ADD_LOCATION_FAIL, error});
    }
}

function* updateLocationSaga(action){
    const {response, error} = yield call(LOCATION_API.updateLocationApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.UPDATE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.UPDATE_LOCATION_FAIL, error});
    }
}

function* deleteLocationSaga(action){
    const {response, error} = yield call(LOCATION_API.deleteLocationApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.DELETE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.DELETE_LOCATION_FAIL, error});
    }
}

function* getProvincesSaga(){
    const {response, error} = yield call(LOCATION_API.getProvincesApi);
    if(response){
        yield put({type: LOCATION_TYPE.GET_PROVINCES_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.GET_PROVINCES_FAIL, error});
    }
}

function* getDistrictsSaga(action){
    const {response, error} = yield call(LOCATION_API.getDistrictsApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.GET_DISTRICTS_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.GET_DISTRICTS_FAIL, error});
    }
}

function* getCommunesSaga(action){
    const {response, error} = yield call(LOCATION_API.getCommunesApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.GET_COMMUNES_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.GET_COMMUNES_FAIL, error});
    }
}

function* getVillagesSaga(action){
    const {response, error} = yield call(LOCATION_API.getVillagesApi, action);
    if(response){
        yield put({type: LOCATION_TYPE.GET_VILLAGES_SUCCESS, response});
    }else{
        yield put({type: LOCATION_TYPE.GET_VILLAGES_FAIL, error});
    }
}
export default function* locationSaga() {
    yield fork(takeEvery, LOCATION_TYPE.GET_ALL_LOCATIONS, getAllLocationsSaga);
    yield fork(takeEvery, LOCATION_TYPE.ADD_LOCATION, addLocationSaga);
    yield fork(takeEvery, LOCATION_TYPE.UPDATE_LOCATION, updateLocationSaga);
    yield fork(takeEvery, LOCATION_TYPE.DELETE_LOCATION, deleteLocationSaga);
    yield fork(takeEvery, LOCATION_TYPE.GET_PROVINCES, getProvincesSaga);
    yield fork(takeEvery, LOCATION_TYPE.GET_DISTRICTS, getDistrictsSaga);
    yield fork(takeEvery, LOCATION_TYPE.GET_COMMUNES, getCommunesSaga);
    yield fork(takeEvery, LOCATION_TYPE.GET_VILLAGES, getVillagesSaga);
}


