import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as INSTALLER_TYPE from '../../actions/installation/installer';
import * as INSTALLER_API from '../../services/installation/installer';

/* ADD INSTALLER */
export function* addInstallerSaga(action){
    const { response, error } = yield call(INSTALLER_API.addInstallerApi, action);
    if(response){
        yield put({type: INSTALLER_TYPE.ADD_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: INSTALLER_TYPE.ADD_INSTALLER_FAIL, error});
    }
}

/* LIST ALL INSTALLERS */
export function* getAllInstallersSaga(action){
    const { response, error } = yield call(INSTALLER_API.getAllInstallersApi, action);
    if(response){
        yield put({type: INSTALLER_TYPE.GET_ALL_INSTALLERS_SUCCESS, response});
    }else{
        yield put({type: INSTALLER_TYPE.GET_ALL_INSTALLERS_FAIL, error});
    }
}

/* UPDATE INSTALLER */
export function* updateInstallerSaga(action){
    const { response, error } = yield call(INSTALLER_API.updateInstallersApi, action);
    if(response){
        yield put({type: INSTALLER_TYPE.UPDATE_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: INSTALLER_TYPE.UPDATE_INSTALLER_FAIL, error});
    }
}

/* DELETE INSTALLER */
export function* deleteInstallerSaga(action){
    const { response, error } = yield call(INSTALLER_API.deleteInstallerApi, action);
    if(response){
        yield put({type: INSTALLER_TYPE.DELETE_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: INSTALLER_TYPE.DELETE_INSTALLER_FAIL, error});
    }
}

export default function* installerSaga() {
    yield fork(takeEvery, INSTALLER_TYPE.ADD_INSTALLER, addInstallerSaga);
    yield fork(takeEvery, INSTALLER_TYPE.GET_ALL_INSTALLERS, getAllInstallersSaga);
    yield fork(takeEvery, INSTALLER_TYPE.UPDATE_INSTALLER, updateInstallerSaga);
    yield fork(takeEvery, INSTALLER_TYPE.DELETE_INSTALLER, deleteInstallerSaga);
}