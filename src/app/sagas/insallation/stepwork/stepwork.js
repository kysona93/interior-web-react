import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as STEP_WORK_TYPE from '../../../actions/installation/stepwork/stepwork';
import * as STEP_WORK_API from '../../../services/installation/stepwork/stepwork';

function* addStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.addStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.ADD_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.ADD_STEP_WORK_FAIL, error});
    }
}

function* updateStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.updateStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.UPDATE_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.UPDATE_STEP_WORK_FAIL, error});
    }
}

function* checkStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.checkStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.CHECK_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.CHECK_STEP_WORK_FAIL, error});
    }
}

function* deleteStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.deleteStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.DELETE_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.DELETE_STEP_WORK_FAIL, error});
    }
}

function* getStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.getStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORK_FAIL, error});
    }
}

function* getStepWorksSaga(action){
    const { response, error } = yield call(STEP_WORK_API.getStepWorksApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORKS_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORKS_FAIL, error});
    }
}

function* getStepWorkDetailsSaga(action){
    const { response, error } = yield call(STEP_WORK_API.getStepWorkDetailsApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORK_DETAILS_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.GET_STEP_WORK_DETAILS_FAIL, error});
    }
}

function* printStepWorkSaga(action){
    const { response, error } = yield call(STEP_WORK_API.printStepWorkApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.PRINT_STEP_WORK_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.PRINT_STEP_WORK_FAIL, error});
    }
}

function* removeStepWorkDetailSaga(action){
    const { response, error } = yield call(STEP_WORK_API.removeStepWorkDetailApi, action);
    if(response){
        yield put({type: STEP_WORK_TYPE.REMOVE_STEP_WORK_DETAIL_SUCCESS, response});
    }else{
        yield put({type: STEP_WORK_TYPE.REMOVE_STEP_WORK_DETAIL_FAIL, error});
    }
}

export default function* stepWorkSaga() {
    yield fork(takeEvery, STEP_WORK_TYPE.ADD_STEP_WORK, addStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.UPDATE_STEP_WORK, updateStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.CHECK_STEP_WORK, checkStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.DELETE_STEP_WORK, deleteStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.GET_STEP_WORK, getStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.GET_STEP_WORKS, getStepWorksSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.GET_STEP_WORK_DETAILS, getStepWorkDetailsSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.PRINT_STEP_WORK, printStepWorkSaga);
    yield fork(takeEvery, STEP_WORK_TYPE.REMOVE_STEP_WORK_DETAIL, removeStepWorkDetailSaga);
}