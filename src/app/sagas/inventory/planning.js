import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from '../../actions/inventory/planning';
import * as API from '../../services/inventory/planning';

function* addPlanningSaga(action){
    const { response, error } = yield call(API.addPlanningApi, action);
    if(response){
        yield put({type: ACTION.ADD_PLANNING_SUCCESS, response});
    }else{
        yield put({type: ACTION.ADD_PLANNING_FAIL, error});
    }
}

function* getPlanningsSaga(action){
    const { response, error } = yield call(API.getPlanningsApi, action);
    if(response){
        yield put({type: ACTION.GET_PLANNINGS_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PLANNINGS_FAIL, error});
    }
}

function* getPlanningNoSaga(action){
    const { response, error } = yield call(API.getPlanningNoApi, action);
    if(response){
        yield put({type: ACTION.GET_PLANNING_NO_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PLANNING_NO_FAIL, error});
    }
}

function* updatePlanningSaga(action){
    const { response, error } = yield call(API.updatePlanningApi, action);
    if(response){
        yield put({type: ACTION.UPDATE_PLANNING_SUCCESS, response});
    }else{
        yield put({type: ACTION.UPDATE_PLANNING_FAIL, error});
    }
}

function* deletePlanningSaga(action){
    const { response, error } = yield call(API.deletePlanningApi, action);
    if(response){
        yield put({type: ACTION.DELETE_PLANNING_SUCCESS, response});
    }else{
        yield put({type: ACTION.DELETE_PLANNING_FAIL, error});
    }
}

function* getPlanningsApprovedSaga(action){
    const { response, error } = yield call(API.getPlanningsApprovedApi, action);
    if(response){
        yield put({type: ACTION.GET_PLANNINGS_APPROVED_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PLANNINGS_APPROVED_FAIL, error});
    }
}

export default function* planningSaga() {
    yield fork(takeEvery, ACTION.ADD_PLANNING, addPlanningSaga);
    yield fork(takeEvery, ACTION.GET_PLANNINGS, getPlanningsSaga);
    yield fork(takeEvery, ACTION.GET_PLANNING_NO, getPlanningNoSaga);
    yield fork(takeEvery, ACTION.UPDATE_PLANNING, updatePlanningSaga);
    yield fork(takeEvery, ACTION.DELETE_PLANNING, deletePlanningSaga);
    yield fork(takeEvery, ACTION.GET_PLANNINGS_APPROVED, getPlanningsApprovedSaga);
}