import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as SUPPLIER from './../../actions/inventory/supplier';
import * as SUPPLIER_API from './../../services/inventory/supplier';

function* getAllSupplierSaga(action){
    const { response, error } = yield call(SUPPLIER_API.getAllSupplierApi, action);
    if(response){
        yield put({type: SUPPLIER.GET_ALL_SUPPLIER_SUCCESS, response});
    }else{
        yield put({type: SUPPLIER.GET_ALL_SUPPLIER_FAIL, error});
    }
}

function* getSuppliersSaga(action){
    const { response, error } = yield call(SUPPLIER_API.getSuppliersApi, action);
    if(response){
        yield put({type: SUPPLIER.GET_SUPPLIERS_SUCCESS, response});
    }else{
        yield put({type: SUPPLIER.GET_SUPPLIERS_FAIL, error});
    }
}

function* addSupplierSaga(action){
    const { response, error } = yield call(SUPPLIER_API.addSupplierApi, action);
    if(response){
        yield put({type: SUPPLIER.ADD_SUPPLIER_SUCCESS, response});
    }else{
        yield put({type: SUPPLIER.ADD_SUPPLIER_FAIL, error});
    }
}

function* updateSupplierSaga(action){
    const { response, error } = yield call(SUPPLIER_API.updateSupplierApi, action);
    if(response){
        yield put({type: SUPPLIER.UPDATE_SUPPLIER_SUCCESS, response});
    }else{
        yield put({type: SUPPLIER.UPDATE_SUPPLIER_FAIL, error});
    }
}

function* deleteSupplierSaga(action){
    const { response, error } = yield call(SUPPLIER_API.deleteSupplierApi, action);
    if(response){
        yield put({type: SUPPLIER.DELETE_SUPPLIER_SUCCESS, response});
    }else{
        yield put({type: SUPPLIER.DELETE_SUPPLIER_SUCCESS, error});
    }
}

export default function* supplierSaga() {
    yield fork(takeEvery, SUPPLIER.GET_ALL_SUPPLIER, getAllSupplierSaga);
    yield fork(takeEvery, SUPPLIER.GET_SUPPLIERS, getSuppliersSaga);
    yield fork(takeEvery, SUPPLIER.ADD_SUPPLIER, addSupplierSaga);
    yield fork(takeEvery, SUPPLIER.UPDATE_SUPPLIER, updateSupplierSaga);
    yield fork(takeEvery, SUPPLIER.DELETE_SUPPLIER, deleteSupplierSaga);
}


