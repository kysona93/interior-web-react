import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from '../../actions/inventory/purchaseKwh';
import * as API from '../../services/inventory/purchaseKwh';

function* addConnectionBuySaga(action){
    const { response, error } = yield call(API.addConnectionBuyApi, action);
    if(response){
        yield put({type: ACTION.ADD_CONNECTION_BUY_SUCCESS, response});
    }else{
        yield put({type: ACTION.ADD_CONNECTION_BUY_FAIL, error});
    }
}

function* getConnectionBuysSaga(action){
    const { response, error } = yield call(API.getConnectionBuysApi, action);
    if(response){
        yield put({type: ACTION.GET_CONNECTION_BUY_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_CONNECTION_BUY_FAIL, error});
    }
}

function* updateConnectionBuySaga(action){
    const { response, error } = yield call(API.updateConnectionBuyApi, action);
    if(response){
        yield put({type: ACTION.UPDATE_CONNECTION_BUY_SUCCESS, response});
    }else{
        yield put({type: ACTION.UPDATE_CONNECTION_BUY_FAIL, error});
    }
}

function* deleteConnectionBuySaga(action){
    const { response, error } = yield call(API.deleteConnectionBuyApi, action);
    if(response){
        yield put({type: ACTION.DELETE_CONNECTION_BUY_SUCCESS, response});
    }else{
        yield put({type: ACTION.DELETE_CONNECTION_BUY_FAIL, error});
    }
}


function* addPurchaseKwhSaga(action){
    const { response, error } = yield call(API.addPurchaseKwhApi, action);
    if(response){
        yield put({type: ACTION.ADD_PURCHASE_KWH_SUCCESS, response});
    }else{
        yield put({type: ACTION.ADD_PURCHASE_KWH_FAIL, error});
    }
}

function* getPurchaseKwhsSaga(action){
    const { response, error } = yield call(API.getPurchaseKwhsApi, action);
    if(response){
        yield put({type: ACTION.GET_PURCHASE_KWH_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PURCHASE_KWH_FAIL, error});
    }
}

function* updatePurchaseKwhSaga(action){
    const { response, error } = yield call(API.updatePurchaseKwhApi, action);
    if(response){
        yield put({type: ACTION.UPDATE_PURCHASE_KWH_SUCCESS, response});
    }else{
        yield put({type: ACTION.UPDATE_PURCHASE_KWH_FAIL, error});
    }
}

function* deletePurchaseKwhSaga(action){
    const { response, error } = yield call(API.deletePurchaseKwhApi, action);
    if(response){
        yield put({type: ACTION.DELETE_PURCHASE_KWH_SUCCESS, response});
    }else{
        yield put({type: ACTION.DELETE_PURCHASE_KWH_FAIL, error});
    }
}

function* getPurchaseKwhMonthlyReportsSaga(action){
    const { response, error } = yield call(API.getPurchaseKwhMonthlyReportsApi, action);
    if(response){
        yield put({type: ACTION.GET_PURCHASE_KWH_MONTHLY_REPORT_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PURCHASE_KWH_MONTHLY_REPORT_FAIL, error});
    }
}


export default function* purchaseKwhSaga() {
    yield fork(takeEvery, ACTION.ADD_CONNECTION_BUY, addConnectionBuySaga);
    yield fork(takeEvery, ACTION.GET_CONNECTION_BUY, getConnectionBuysSaga);
    yield fork(takeEvery, ACTION.UPDATE_CONNECTION_BUY, updateConnectionBuySaga);
    yield fork(takeEvery, ACTION.DELETE_CONNECTION_BUY, deleteConnectionBuySaga);
    yield fork(takeEvery, ACTION.ADD_PURCHASE_KWH, addPurchaseKwhSaga);
    yield fork(takeEvery, ACTION.GET_PURCHASE_KWH, getPurchaseKwhsSaga);
    yield fork(takeEvery, ACTION.UPDATE_PURCHASE_KWH, updatePurchaseKwhSaga);
    yield fork(takeEvery, ACTION.DELETE_PURCHASE_KWH, deletePurchaseKwhSaga);
    yield fork(takeEvery, ACTION.GET_PURCHASE_KWH_MONTHLY_REPORT, getPurchaseKwhMonthlyReportsSaga);
}