import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ITEM_TYPE from '../../actions/inventory/item';
import * as ITEM_API from '../../services/inventory/item';

function* addItemSaga(action){
    const { response, error } = yield call(ITEM_API.addItemApi, action);
    if(response){
        yield put({type: ITEM_TYPE.ADD_ITEM_SUCCESS, response});
    }else{
        yield put({type: ITEM_TYPE.ADD_ITEM_FAIL, error});
    }
}

function* getAllItemsSaga(action){
    const { response, error } = yield call(ITEM_API.getAllItemsApi, action);
    if(response){
        yield put({type: ITEM_TYPE.GET_ALL_ITEMS_SUCCESS, response});
    }else{
        yield put({type: ITEM_TYPE.GET_ALL_ITEMS_FAIL, error});
    }
}

function* getItemsSaga(action){
    const { response, error } = yield call(ITEM_API.getItemsApi, action);
    if(response){
        yield put({type: ITEM_TYPE.GET_ITEMS_SUCCESS, response});
    }else{
        yield put({type: ITEM_TYPE.GET_ITEMS_FAIL, error});
    }
}

function* updateItemSaga(action){
    const { response, error } = yield call(ITEM_API.updateItemApi, action);
    if(response){
        yield put({type: ITEM_TYPE.UPDATE_ITEM_SUCCESS, response});
    }else{
        yield put({type: ITEM_TYPE.UPDATE_ITEM_FAIL, error});
    }
}

function* deleteItemSaga(action){
    const { response, error } = yield call(ITEM_API.deleteItemApi, action);
    if(response){
        yield put({type: ITEM_TYPE.DELETE_ITEM_SUCCESS, response});
    }else{
        yield put({type: ITEM_TYPE.DELETE_ITEM_FAIL, error});
    }
}

export default function* itemSaga() {
    yield fork(takeEvery, ITEM_TYPE.ADD_ITEM, addItemSaga);
    yield fork(takeEvery, ITEM_TYPE.GET_ALL_ITEMS, getAllItemsSaga);
    yield fork(takeEvery, ITEM_TYPE.GET_ITEMS, getItemsSaga);
    yield fork(takeEvery, ITEM_TYPE.UPDATE_ITEM, updateItemSaga);
    yield fork(takeEvery, ITEM_TYPE.DELETE_ITEM, deleteItemSaga);
}