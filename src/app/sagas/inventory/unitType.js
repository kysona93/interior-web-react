import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_UNIT_TYPE,
    ADD_UNIT_TYPE_FAIL,
    ADD_UNIT_TYPE_SUCCESS,
    GET_ALL_UNIT_TYPES,
    GET_ALL_UNIT_TYPES_FAIL,
    GET_ALL_UNIT_TYPES_SUCCESS,
    UPDATE_UNIT_TYPE,
    UPDATE_UNIT_TYPE_FAIL,
    UPDATE_UNIT_TYPE_SUCCESS,
    DELETE_UNIT_TYPE,
    DELETE_UNIT_TYPE_FAIL,
    DELETE_UNIT_TYPE_SUCCESS
} from './../../actions/inventory/unitType';
import {
    addUnitTypeApi,
    listAllUnitTypesApi,
    updateUnitTypeApi,
    deleteUnitTypeApi
} from './../../services/inventory/unitType';

function* addUnitTypeSaga(action){
    const { response, error } = yield call(addUnitTypeApi, action);
    if(response){
        yield put({type: ADD_UNIT_TYPE_SUCCESS, response});
    }else{
        yield put({type: ADD_UNIT_TYPE_FAIL, error});
    }
}

function* listAllUnitTypeSaga(action){
    const { response, error } = yield call(listAllUnitTypesApi, action);
    if(response){
        yield put({type: GET_ALL_UNIT_TYPES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_UNIT_TYPES_FAIL, error});
    }
}

function* updateUnitTypeSaga(action){
    const { response, error } = yield call(updateUnitTypeApi, action);
    if(response){
        yield put({type: UPDATE_UNIT_TYPE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_UNIT_TYPE_FAIL, error});
    }
}

function* deleteUnitTypeSaga(action){
    const { response, error } = yield call(deleteUnitTypeApi, action);
    if(response){
        yield put({type: DELETE_UNIT_TYPE_SUCCESS, response});
    }else{
        yield put({type: DELETE_UNIT_TYPE_FAIL, error});
    }
}

export default function* unitTypeSaga() {
    yield fork(takeEvery, ADD_UNIT_TYPE, addUnitTypeSaga);
    yield fork(takeEvery, GET_ALL_UNIT_TYPES, listAllUnitTypeSaga);
    yield fork(takeEvery, UPDATE_UNIT_TYPE, updateUnitTypeSaga);
    yield fork(takeEvery, DELETE_UNIT_TYPE, deleteUnitTypeSaga);
}


