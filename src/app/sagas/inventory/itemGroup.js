import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_ITEM_GROUP,
    ADD_ITEM_GROUP_FAIL,
    ADD_ITEM_GROUP_SUCCESS,
    GET_ALL_ITEM_GROUPS,
    GET_ALL_ITEM_GROUPS_FAIL,
    GET_ALL_ITEM_GROUPS_SUCCESS,
    UPDATE_ITEM_GROUP,
    UPDATE_ITEM_GROUP_FAIL,
    UPDATE_ITEM_GROUP_SUCCESS,
    DELETE_ITEM_GROUP,
    DELETE_ITEM_GROUP_FAIL,
    DELETE_ITEM_GROUP_SUCCESS,
    GET_ALL_ITEM_GROUPS_LIST,
    GET_ALL_ITEM_GROUPS_LIST_SUCCESS,
    GET_ALL_ITEM_GROUPS_LIST_FAIL
} from './../../actions/inventory/itemGroup';
import {
    addItemGroupApi,
    listAllItemGroupsApi,
    updateItemGroupApi,
    deleteItemGroupApi,
    getItemGroupsListApi
} from './../../services/inventory/itemGroup';

function* addItemGroupSaga(action){
    const { response, error  } = yield call(addItemGroupApi, action);
    if(response){
        yield put({type: ADD_ITEM_GROUP_SUCCESS, response});
    }else{
        yield put({type: ADD_ITEM_GROUP_FAIL, error});
    }
}

function* listAllItemGroupSaga(action){
    const { response, error  } = yield call(listAllItemGroupsApi, action);
    if(response){
        yield put({type: GET_ALL_ITEM_GROUPS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_ITEM_GROUPS_FAIL, error});
    }
}

function* updateItemGroupSaga(action){
    const { response, error  } = yield call(updateItemGroupApi, action);
    if(response){
        yield put({type: UPDATE_ITEM_GROUP_SUCCESS, response});
    }else{
        yield put({type: UPDATE_ITEM_GROUP_SUCCESS, error});
    }
}

function* deleteItemGroupSaga(action){
    const { response, error  } = yield call(deleteItemGroupApi, action);
    if(response){
        yield put({type: DELETE_ITEM_GROUP_SUCCESS, response});
    }else{
        yield put({type: DELETE_ITEM_GROUP_FAIL, error});
    }
}

function* getItemGroupsListSaga(action){
    const { response, error  } = yield call(getItemGroupsListApi, action);
    if(response){
        yield put({type: GET_ALL_ITEM_GROUPS_LIST_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_ITEM_GROUPS_LIST_FAIL, error});
    }
}

export default function* itemGroupSaga() {
    yield fork(takeEvery, ADD_ITEM_GROUP, addItemGroupSaga);
    yield fork(takeEvery, GET_ALL_ITEM_GROUPS, listAllItemGroupSaga);
    yield fork(takeEvery, UPDATE_ITEM_GROUP, updateItemGroupSaga);
    yield fork(takeEvery, DELETE_ITEM_GROUP, deleteItemGroupSaga);
    yield fork(takeEvery, GET_ALL_ITEM_GROUPS_LIST, getItemGroupsListSaga);
}


