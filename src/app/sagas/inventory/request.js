import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from '../../actions/inventory/request';
import * as API from '../../services/inventory/request';

function* addRequestSaga(action){
    const { response, error } = yield call(API.addRequestApi, action);
    if(response){
        yield put({type: ACTION.ADD_REQUEST_SUCCESS, response});
    }else{
        yield put({type: ACTION.ADD_REQUEST_FAIL, error});
    }
}

function* getRequestsSaga(action){
    const { response, error } = yield call(API.getRequestsApi, action);
    if(response){
        yield put({type: ACTION.GET_REQUESTS_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_REQUESTS_FAIL, error});
    }
}

function* getRequestNoSaga(action){
    const { response, error } = yield call(API.getRequestNoApi, action);
    if(response){
        yield put({type: ACTION.GET_REQUEST_NO_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_REQUEST_NO_FAIL, error});
    }
}

function* updateRequestSaga(action){
    const { response, error } = yield call(API.updateRequestApi, action);
    if(response){
        yield put({type: ACTION.UPDATE_REQUEST_SUCCESS, response});
    }else{
        yield put({type: ACTION.UPDATE_REQUEST_FAIL, error});
    }
}

function* deleteRequestSaga(action){
    const { response, error } = yield call(API.deleteRequestApi, action);
    if(response){
        yield put({type: ACTION.DELETE_REQUEST_SUCCESS, response});
    }else{
        yield put({type: ACTION.DELETE_REQUEST_FAIL, error});
    }
}

function* getRequestsApprovedSaga(action){
    const { response, error } = yield call(API.getRequestsApprovedApi, action);
    if(response){
        yield put({type: ACTION.GET_REQUESTS_APPROVED_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_REQUESTS_APPROVED_FAIL, error});
    }
}

export default function* requestSaga() {
    yield fork(takeEvery, ACTION.ADD_REQUEST, addRequestSaga);
    yield fork(takeEvery, ACTION.GET_REQUESTS, getRequestsSaga);
    yield fork(takeEvery, ACTION.GET_REQUEST_NO, getRequestNoSaga);
    yield fork(takeEvery, ACTION.UPDATE_REQUEST, updateRequestSaga);
    yield fork(takeEvery, ACTION.DELETE_REQUEST, deleteRequestSaga);
    yield fork(takeEvery, ACTION.GET_REQUESTS_APPROVED, getRequestsApprovedSaga);
}