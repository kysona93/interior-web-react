import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_STOCK_IN_ITEM,
    ADD_STOCK_IN_ITEM_FAIL,
    ADD_STOCK_IN_ITEM_SUCCESS,
    GET_ALL_STOCK_IN_ITEMS,
    GET_ALL_STOCK_IN_ITEMS_FAIL,
    GET_ALL_STOCK_IN_ITEMS_SUCCESS,
    UPDATE_STOCK_IN_ITEM,
    UPDATE_STOCK_IN_ITEM_FAIL,
    UPDATE_STOCK_IN_ITEM_SUCCESS,
    DELETE_STOCK_IN_ITEM,
    DELETE_STOCK_IN_ITEM_FAIL,
    DELETE_STOCK_IN_ITEM_SUCCESS
} from '../../actions/inventory/stockInItem';
import {
    addStockInItemApi,
    getAllStockInItemsApi,
    updateStockInItemApi,
    deleteStockInItemApi
} from '../../services/inventory/stockInItem';

function* addStockInItemSaga(action){
    const { response, error } = yield call(addStockInItemApi, action);
    if(response){
        yield put({type: ADD_STOCK_IN_ITEM_SUCCESS, response});
    }else{
        yield put({type: ADD_STOCK_IN_ITEM_FAIL, error});
    }
}

function* getAllStockInItemsSaga(action){
    const { response, error } = yield call(getAllStockInItemsApi, action);
    if(response){
        yield put({type: GET_ALL_STOCK_IN_ITEMS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_STOCK_IN_ITEMS_FAIL, error});
    }
}

function* updateStockInItemSaga(action){
    const { response, error } = yield call(updateStockInItemApi, action);
    if(response){
        yield put({type: UPDATE_STOCK_IN_ITEM_SUCCESS, response});
    }else{
        yield put({type: UPDATE_STOCK_IN_ITEM_FAIL, error});
    }
}

function* deleteStockInItemSaga(action){
    const { response, error } = yield call(deleteStockInItemApi, action);
    if(response){
        yield put({type: DELETE_STOCK_IN_ITEM_SUCCESS, response});
    }else{
        yield put({type: DELETE_STOCK_IN_ITEM_FAIL, error});
    }
}

export default function* stockInItemSaga() {
    yield fork(takeEvery, ADD_STOCK_IN_ITEM, addStockInItemSaga);
    yield fork(takeEvery, GET_ALL_STOCK_IN_ITEMS, getAllStockInItemsSaga);
    yield fork(takeEvery, UPDATE_STOCK_IN_ITEM, updateStockInItemSaga);
    yield fork(takeEvery, DELETE_STOCK_IN_ITEM, deleteStockInItemSaga);
}