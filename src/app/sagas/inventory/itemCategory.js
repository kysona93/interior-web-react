import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as CATEGORY_ACTION from './../../actions/inventory/itemCategory';
import * as CATEGORY_API from './../../services/inventory/itemCategory';

function* getItemCategoryByParentSaga(action){
    const { response, error } = yield call(CATEGORY_API.getItemCategoryByParentApi, action);
    if(response){
        yield put({type: CATEGORY_ACTION.GET_ITEM_CATEGORY_PARENT_SUCCESS, response});
    }else{
        yield put({type: CATEGORY_ACTION.GET_ITEM_CATEGORY_PARENT_FAIL, error});
    }
}

function* getItemSubCategoryByParentSaga(action){
    const { response, error } = yield call(CATEGORY_API.getItemSubCategoryByParentApi, action);
    if(response){
        yield put({type: CATEGORY_ACTION.GET_ITEM_SUB_CATEGORY_PARENT_SUCCESS, response});
    }else{
        yield put({type: CATEGORY_ACTION.GET_ITEM_SUB_CATEGORY_PARENT_FAIL, error});
    }
}

export default function* itemCategorySaga() {
    yield fork(takeEvery, CATEGORY_ACTION.GET_ITEM_CATEGORY_PARENT, getItemCategoryByParentSaga);
    yield fork(takeEvery, CATEGORY_ACTION.GET_ITEM_SUB_CATEGORY_PARENT, getItemSubCategoryByParentSaga);
}


