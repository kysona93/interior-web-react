import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from './../../actions/inventory/connectionType';
import * as API from './../../services/inventory/connectionType';

function* getConnectionTypesSaga(action){
    const { response, error } = yield call(API.getConnectionTypesApi, action);
    if(response){
        yield put({type: ACTION.GET_CONNECTION_TYPES_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_CONNECTION_TYPES_FAIL, error});
    }
}

export default function* connectionTypeSaga() {
    yield fork(takeEvery, ACTION.GET_CONNECTION_TYPES, getConnectionTypesSaga);
}


