import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as GROUP_INVOICE_TYPE from './../../actions/invoice/groupInvoices';
import * as GROUP_INVOICE_API from './../../services/invoice/groupInvoices';

function* addGroupInvoiceSaga(action){
    const { response, error } = yield call(GROUP_INVOICE_API.addGroupInvoiceApi, action);
    if(response){
        yield put({type: GROUP_INVOICE_TYPE.ADD_GROUP_INVOICE_SUCCESS, response});
    }else{
        yield put({type: GROUP_INVOICE_TYPE.ADD_GROUP_INVOICE_FAIL, error});
    }
}

function* getAllGroupInvoicesSaga(action){
    const { response, error } = yield call(GROUP_INVOICE_API.getAllGroupInvoiceApi, action);
    if(response){
        yield put({type: GROUP_INVOICE_TYPE.GET_ALL_GROUP_INVOICE_SUCCESS, response});
    }else{
        yield put({type: GROUP_INVOICE_TYPE.GET_ALL_GROUP_INVOICE_FAIL, error});
    }
}

function* updateGroupInvoiceSaga(action){
    const { response, error } = yield call(GROUP_INVOICE_API.updateGroupInvoiceApi, action);
    if(response){
        yield put({type: GROUP_INVOICE_TYPE.UPDATE_GROUP_INVOICE_SUCCESS, response});
    }else{
        yield put({type: GROUP_INVOICE_TYPE.UPDATE_GROUP_INVOICE_FAIL, error});
    }
}

function* deleteGroupInvoiceSaga(action){
    const { response, error } = yield call(GROUP_INVOICE_API.deleteGroupInvoiceApi, action);
    if(response){
        yield put({type: GROUP_INVOICE_TYPE.DELETE_GROUP_INVOICE_SUCCESS, response});
    }else{
        yield put({type: GROUP_INVOICE_TYPE.DELETE_GROUP_INVOICE_FAIL, error});
    }
}

export default function* groupInvoiceSaga() {
    yield fork(takeEvery, GROUP_INVOICE_TYPE.ADD_GROUP_INVOICE, addGroupInvoiceSaga);
    yield fork(takeEvery, GROUP_INVOICE_TYPE.GET_ALL_GROUP_INVOICE, getAllGroupInvoicesSaga);
    yield fork(takeEvery, GROUP_INVOICE_TYPE.UPDATE_GROUP_INVOICE, updateGroupInvoiceSaga);
    yield fork(takeEvery, GROUP_INVOICE_TYPE.DELETE_GROUP_INVOICE, deleteGroupInvoiceSaga);
}