import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_INVOICE,
    ADD_INVOICE_FAIL,
    ADD_INVOICE_SUCCESS,
    GET_ALL_INVOICES,
    GET_ALL_INVOICES_FAIL,
    GET_ALL_INVOICES_SUCCESS,
    UPDATE_INVOICE,
    UPDATE_INVOICE_FAIL,
    UPDATE_INVOICE_SUCCESS,
    DELETE_INVOICE,
    DELETE_INVOICE_FAIL,
    DELETE_INVOICE_SUCCESS,
    VOID_INVOICE,
    VOID_INVOICE_FAIL,
    VOID_INVOICE_SUCCESS
} from '../../actions/invoice/invoice';
import {
    addInvoiceApi,
    getAllInvoicesApi,
    updateInvoiceApi,
    deleteInvoiceApi,
    voidInvoiceApi
} from './../../services/invoice/invoice';

function* addInvoiceSaga(action){
    const { response, error } = yield call(addInvoiceApi, action);
    if(response){
        yield put({type: ADD_INVOICE_SUCCESS, response});
    }else{
        yield put({type: ADD_INVOICE_FAIL, error});
    }
}

function* getAllInvoicesSaga(action){
    const { response, error } = yield call(getAllInvoicesApi, action);
    if(response){
        yield put({type: GET_ALL_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_INVOICES_FAIL, error});
    }
}

function* updateInvoiceSaga(action){
    const { response, error } = yield call(updateInvoiceApi, action);
    if(response){
        yield put({type: UPDATE_INVOICE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_INVOICE_FAIL, error});
    }
}

function* deleteInvoiceSaga(action){
    const { response, error } = yield call(deleteInvoiceApi, action);
    if(response){
        yield put({type: DELETE_INVOICE_SUCCESS, response});
    }else{
        yield put({type: DELETE_INVOICE_FAIL, error});
    }
}

function* voidInvoiceSaga(action){
    const { response, error } = yield call(voidInvoiceApi, action);
    if(response){
        yield put({type: VOID_INVOICE_SUCCESS, response});
    }else{
        yield put({type: VOID_INVOICE_FAIL, error});
    }
}

export default function* invoicesSaga() {
    yield fork(takeEvery, ADD_INVOICE, addInvoiceSaga);
    yield fork(takeEvery, GET_ALL_INVOICES, getAllInvoicesSaga);
    yield fork(takeEvery, UPDATE_INVOICE, updateInvoiceSaga);
    yield fork(takeEvery, DELETE_INVOICE, deleteInvoiceSaga);
    yield fork(takeEvery, VOID_INVOICE, voidInvoiceSaga);
}