import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_CUSTOMER_INVOICE,
    ADD_CUSTOMER_INVOICE_FAIL,
    ADD_CUSTOMER_INVOICE_SUCCESS,
    GET_ALL_CUSTOMER_INVOICES,
    GET_ALL_CUSTOMER_INVOICES_FAIL,
    GET_ALL_CUSTOMER_INVOICES_SUCCESS,
    VOID_CUSTOMER_INVOICE,
    VOID_CUSTOMER_INVOICE_FAIL,
    VOID_CUSTOMER_INVOICE_SUCCESS,
    DELETE_CUSTOMER_INVOICE,
    DELETE_CUSTOMER_INVOICE_FAIL,
    DELETE_CUSTOMER_INVOICE_SUCCESS,
    GET_ALL_CUSTOMER_INVOICES_NO,
    GET_ALL_CUSTOMER_INVOICE_NO_FAIL,
    GET_ALL_CUSTOMER_INVOICES_NO_SUCCESS,
    GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID,
    GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_FAIL,
    GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_SUCCESS,
    GET_INVOICE_INFORMATION_BY_ID,
    GET_INVOICE_INFORMATION_BY_ID_FAIL,
    GET_INVOICE_INFORMATION_BY_ID_SUCCESS,
    GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID,
    GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_FAIL,
    GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_SUCCESS,
    GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID,
    GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_FAIL,
    GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_SUCCESS,
    GET_ALL_LICENSE_INVOICES_NO_SUCCESS,
    GET_ALL_LICENSE_INVOICE_NO_FAIL,
    GET_ALL_LICENSE_INVOICES_NO,
    GET_INVOICE_DETAILS_RENT_POLE,
    GET_INVOICE_DETAILS_RENT_POLE_SUCCESS,
    GET_INVOICE_DETAILS_RENT_POLE_FAIL

} from '../../actions/invoice/customerInvoice';
import {
    addCustomerInvoiceApi,
    getAllCustomerInvoicesApi,
    voidCustomerInvoiceApi,
    deleteCustomerInvoiceApi,
    getAllCustomerInvoiceNoApi,
    getAllInvoiceDetailsByInvoiceIdApi,
    getInvoiceInformationByInvoiceIdApi,
    getHistoryInvoice12monthByCustomerIdApi,
    getAllInvoiceDetailItemsByInvoiceIdApi,
    getAllLicenseInvoicesApi,
    getInvoiceDetailRentPoleApi
} from './../../services/invoice/customerInvoice';

function* addCustomerInvoiceSaga(action){
    const { response, error } = yield call(addCustomerInvoiceApi, action);
    if(response){
        yield put({type: ADD_CUSTOMER_INVOICE_SUCCESS, response});
    }else{
        yield put({type: ADD_CUSTOMER_INVOICE_FAIL, error});
    }
}

function* getAllCustomerInvoicesSaga(action){
    const { response, error } = yield call(getAllCustomerInvoicesApi, action);
    if(response){
        yield put({type: GET_ALL_CUSTOMER_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CUSTOMER_INVOICES_FAIL, error});
    }
}

function* deleteCustomerInvoiceSaga(action){
    const { response, error } = yield call(deleteCustomerInvoiceApi, action);
    if(response){
        yield put({type: DELETE_CUSTOMER_INVOICE_SUCCESS, response});
    }else{
        yield put({type: DELETE_CUSTOMER_INVOICE_FAIL, error});
    }
}

function* voidCustomerInvoiceSaga(action){
    const { response, error } = yield call(voidCustomerInvoiceApi, action);
    if(response){
        yield put({type: VOID_CUSTOMER_INVOICE_SUCCESS, response});
    }else{
        yield put({type: VOID_CUSTOMER_INVOICE_FAIL, error});
    }
}

function* getAllCustomerInvoiceNoSaga(action){
    const { response, error } = yield call(getAllCustomerInvoiceNoApi, action);
    if(response){
        yield put({type: GET_ALL_CUSTOMER_INVOICES_NO_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CUSTOMER_INVOICE_NO_FAIL, error});
    }
}
function* getAllLicenseInvoiceNoSaga(action){
    const { response, error } = yield call(getAllLicenseInvoicesApi, action);
    if(response){
        yield put({type: GET_ALL_LICENSE_INVOICES_NO_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_LICENSE_INVOICE_NO_FAIL, error});
    }
}

function* getAllInvoiceDetailsByInvoiceIdSaga(action){
    const { response, error } = yield call(getAllInvoiceDetailsByInvoiceIdApi, action);
    if(response){
        yield put({type: GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID_FAIL, error});
    }
}

function* getInvoiceInformationByInvoiceIdSaga(action){
    const { response, error } = yield call(getInvoiceInformationByInvoiceIdApi, action);
    if(response){
        yield put({type: GET_INVOICE_INFORMATION_BY_ID_SUCCESS, response});
    }else{
        yield put({type: GET_INVOICE_INFORMATION_BY_ID_FAIL, error});
    }
}

function* getHistoryInvoice12monthByCustomerIdSaga(action){
    const { response, error } = yield call(getHistoryInvoice12monthByCustomerIdApi, action);
    if(response){
        yield put({type: GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_SUCCESS, response});
    }else{
        yield put({type: GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID_FAIL, error});
    }
}

function* getAllInvoiceDetailItemsByInvoiceIdSaga(action){
    const { response, error } = yield call(getAllInvoiceDetailItemsByInvoiceIdApi, action);
    if(response){
        yield put({type: GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID_FAIL, error});
    }
}
function* getInvoiceDetailRentPoleSaga(action){
    const { response, error } = yield call(getInvoiceDetailRentPoleApi, action);
    if(response){
        yield put({type: GET_INVOICE_DETAILS_RENT_POLE_SUCCESS, response});
    }else{
        yield put({type: GET_INVOICE_DETAILS_RENT_POLE_FAIL, error});
    }
}

export default function* customerInvoicesSaga() {
    yield fork(takeEvery, ADD_CUSTOMER_INVOICE, addCustomerInvoiceSaga);
    yield fork(takeEvery, GET_ALL_CUSTOMER_INVOICES, getAllCustomerInvoicesSaga);
    yield fork(takeEvery, DELETE_CUSTOMER_INVOICE, deleteCustomerInvoiceSaga);
    yield fork(takeEvery, VOID_CUSTOMER_INVOICE, voidCustomerInvoiceSaga);
    yield fork(takeEvery, GET_ALL_CUSTOMER_INVOICES_NO, getAllCustomerInvoiceNoSaga);
    yield fork(takeEvery, GET_ALL_INVOICE_DETAILS_BY_INVOICE_ID, getAllInvoiceDetailsByInvoiceIdSaga);
    yield fork(takeEvery, GET_INVOICE_INFORMATION_BY_ID, getInvoiceInformationByInvoiceIdSaga);
    yield fork(takeEvery, GET_HISTORY_INVOICE_12MONTH_BY_CUSTOMER_ID, getHistoryInvoice12monthByCustomerIdSaga);
    yield fork(takeEvery, GET_ALL_INVOICE_DETAIL_ITEMS_BY_INVOICE_ID, getAllInvoiceDetailItemsByInvoiceIdSaga);
    yield fork(takeEvery, GET_ALL_LICENSE_INVOICES_NO, getAllLicenseInvoiceNoSaga);
    yield fork(takeEvery, GET_INVOICE_DETAILS_RENT_POLE, getInvoiceDetailRentPoleSaga);
}