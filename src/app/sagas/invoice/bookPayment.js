import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_CUSTOMER_BOOK_PAYMENT,
    ADD_CUSTOMER_BOOK_PAYMENT_FAIL,
    ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS,
    ADD_CUSTOMER_BOOK_DEPRECIATION,
    ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL,
    ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS

} from '../../actions/invoice/bookPayment';
import {
    addCustomerBookPaymentApi
} from '../../services/invoice/bookPayment';


function* addCustomerBookPaymentSaga(action){
    const { response, error } = yield call(addCustomerBookPaymentApi, action);
    if(response){
        yield put({type: ADD_CUSTOMER_BOOK_PAYMENT_SUCCESS, response});
    }else{
        yield put({type: ADD_CUSTOMER_BOOK_PAYMENT_FAIL, error});
    }
}

function* addCustomerBookDepreciationSaga(action){
    const { response, error } = yield call(addCustomerBookPaymentApi, action);
    if(response){
        yield put({type: ADD_CUSTOMER_BOOK_DEPRECIATION_SUCCESS, response});
    }else{
        yield put({type: ADD_CUSTOMER_BOOK_DEPRECIATION_FAIL, error});
    }
}

export default function* customerBookPaymentSaga() {
    yield fork(takeEvery, ADD_CUSTOMER_BOOK_PAYMENT, addCustomerBookPaymentSaga);
    yield fork(takeEvery, ADD_CUSTOMER_BOOK_DEPRECIATION, addCustomerBookDepreciationSaga);
}


