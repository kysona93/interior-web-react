import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as INVOICE_TYPE from './../../actions/invoice/invoiceType';
import * as INVOICE_API from './../../services/invoice/invioceType';

function* getAllInvoiceTypeSaga(action){
    const { response, error } = yield call(INVOICE_API.getAllInvoiceTypeApi, action);
    if(response){
        yield put({type: INVOICE_TYPE.GET_ALL_INVOICE_TYPE_SUCCESS, response});
    }else{
        yield put({type: INVOICE_TYPE.GET_ALL_INVOICE_TYPE_FAIL, error});
    }
}

function* addInvoiceTypeSaga(action){
    const { response, error } = yield call(INVOICE_API.addInvoiceTypeApi, action);
    if(response){
        yield put({type: INVOICE_TYPE.ADD_INVOICE_TYPE_SUCCESS, response});
    }else{
        yield put({type: INVOICE_TYPE.ADD_INVOICE_TYPE_FAIL, error});
    }
}

function* updateInvoiceTypeSaga(action){
    const { response, error } = yield call(INVOICE_API.updateInvoiceTypeApi, action);
    if(response){
        yield put({type: INVOICE_TYPE.UPDATE_INVOICE_TYPE_SUCCESS, response});
    }else{
        yield put({type: INVOICE_TYPE.UPDATE_INVOICE_TYPE_FAIL, error});
    }
}

function* deleteInvoiceTypeSaga(action){
    const { response, error } = yield call(INVOICE_API.deleteInvoiceTypeApi, action);
    if(response){
        yield put({type: INVOICE_TYPE.DELETE_INVOICE_TYPE_SUCCESS, response});
    }else{
        yield put({type: INVOICE_TYPE.DELETE_INVOICE_TYPE_FAIL, error});
    }
}

export default function* invoiceTypeSaga() {
    yield fork(takeEvery, INVOICE_TYPE.GET_ALL_INVOICE_TYPE, getAllInvoiceTypeSaga);
    yield fork(takeEvery, INVOICE_TYPE.ADD_INVOICE_TYPE, addInvoiceTypeSaga);
    yield fork(takeEvery, INVOICE_TYPE.UPDATE_INVOICE_TYPE, updateInvoiceTypeSaga);
    yield fork(takeEvery, INVOICE_TYPE.DELETE_INVOICE_TYPE, deleteInvoiceTypeSaga);
}


