import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';

/* authentication */
import {
    GET_ALL_MENUS,
    GET_ALL_MENUS_SUCCESS,
    GET_ALL_MENUS_FAIL,
    GET_MENUS,
    GET_MENUS_SUCCESS,
    GET_MENUS_FAIL,
    GET_MENU,
    GET_MENU_SUCCESS,
    GET_MENU_FAIL,
    UPDATE_MENU,
    UPDATE_MENU_SUCCESS,
    UPDATE_MENU_FAIL,
    DELETE_MENU,
    DELETE_MENU_SUCCESS,
    DELETE_MENU_FAIL,
    LIST_ALL_PAGES,
    LIST_ALL_PAGES_SUCCESS,
    LIST_ALL_PAGES_FAIL,
    LIST_PAGES_BY_MENUS,
    LIST_PAGES_BY_MENUS_SUCCESS,
    LIST_PAGES_BY_MENUS_FAIL,
    ASSIGN_MENUS,
    ASSIGN_MENUS_SUCCESS,
    ASSIGN_MENUS_FAIL,
    GET_MENUS_BY_GROUP,
    GET_MENUS_BY_GROUP_SUCCESS,
    GET_MENUS_BY_GROUP_FAIL,
    GET_MENUS_LIST_AND_PAGE,
    GET_MENUS_LIST_AND_PAGE_SUCCESS,
    GET_MENUS_LIST_AND_PAGE_FAIL
} from '../actions/menu';
import {
    getAllMenusApi,
    getMenusApi,
    getMenuDetailApi,
    updateMenuApi,
    apiDeleteMenu,
    apiListAllPages,
    apiListPagesByMenus,
    assignMenusApi,
    getMenusByGroupApi,
    getMenusListAndPageApi
} from '../services/menu';

/* GET ALL MENUS */
function* getAllMenusSaga(action){
    const { response, error } = yield call(getAllMenusApi, action);
    if(response){
        yield put({type: GET_ALL_MENUS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_MENUS_FAIL, error});
    }
}

/* GET MENUS */
function* getMenusSaga(action){
    const response = yield call(getMenusApi, action);
    if(!(response == undefined)){
        yield put({type: GET_MENUS_SUCCESS, response});
    }else{
        yield put({type: GET_MENUS_FAIL, response});
    }
}

/* GET MENU DETAIL */
function* getMenuDetailSaga(action){
    const response = yield call(getMenuDetailApi, action);
    if(!(response == undefined)){
        yield put({type: GET_MENU_SUCCESS, response});
    }else{
        yield put({type: GET_MENU_FAIL, response});
    }
}

/* UPDATE MENU */
function* updateMenuSaga(action){
    const response = yield call(updateMenuApi, action); 
    if(!(response == undefined)){
        yield put({type: UPDATE_MENU_SUCCESS, response});
    }else{
        yield put({type: UPDATE_MENU_FAIL, response});
    }
}

/* DELETE MENU */
function* deleteMenuSaga(action){
    const response = yield call(apiDeleteMenu, action);
    if(!(response == undefined)){
        yield put({type: DELETE_MENU_SUCCESS, response});
    }else{
        yield put({type: DELETE_MENU_FAIL, response});
    }
}

/* LIST ALL PAGES */
function* listAllPagesSaga(action){
    const response = yield call(apiListAllPages, action);
    if(!(response == undefined)){
        yield put({type: LIST_ALL_PAGES_SUCCESS, response});
    }else{
        yield put({type: LIST_ALL_PAGES_FAIL, response});
    }
}

/* LIST PAGES BY MENUS */
function* listPagesByMenusSaga(action){
    const response = yield call(apiListPagesByMenus, action);
    if(!(response == undefined)){
        yield put({type: LIST_PAGES_BY_MENUS_SUCCESS, response});
    }else{
        yield put({type: LIST_PAGES_BY_MENUS_FAIL, response});
    }
}

/* ASSIGN MENUS */
function* assignMenusSaga(action){
    const response = yield call(assignMenusApi, action);
    if(!(response == undefined)){
        yield put({type: ASSIGN_MENUS_SUCCESS, response});
    }else{
        yield put({type: ASSIGN_MENUS_FAIL, response});
    }
}

/* GET MENUS BY GROUP */
function* getMenusByGroupSaga(action){
    const { response, error } = yield call(getMenusByGroupApi, action);
    if(response){
        yield put({type: GET_MENUS_BY_GROUP_SUCCESS, response});
    }else{
        yield put({type: GET_MENUS_BY_GROUP_FAIL, error});
    }
}

/* GET MENUS LIST AND PAGE */
function* getMenusListAndPageSaga(action){
    const { response, error } = yield call(getMenusListAndPageApi, action);
    if(response){
        yield put({type: GET_MENUS_LIST_AND_PAGE_SUCCESS, response});
    }else{
        yield put({type: GET_MENUS_LIST_AND_PAGE_FAIL, error});
    }
}

export default function* menuSaga(){
    yield fork(takeEvery, GET_ALL_MENUS, getAllMenusSaga);
    yield fork(takeEvery, GET_MENUS, getMenusSaga);
    yield fork(takeEvery, GET_MENU, getMenuDetailSaga);
    yield fork(takeEvery, UPDATE_MENU, updateMenuSaga);
    yield fork(takeEvery, DELETE_MENU, deleteMenuSaga);
    yield fork(takeEvery, LIST_ALL_PAGES, listAllPagesSaga);
    yield fork(takeEvery, LIST_PAGES_BY_MENUS, listPagesByMenusSaga );
    yield fork(takeEvery, ASSIGN_MENUS, assignMenusSaga);
    yield fork(takeEvery, GET_MENUS_BY_GROUP, getMenusByGroupSaga);
    yield fork(takeEvery, GET_MENUS_LIST_AND_PAGE, getMenusListAndPageSaga);
}