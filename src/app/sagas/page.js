import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';

/* page */
import * as PAGE_ACTION from '../actions/page';

import * as pageService from '../services/page';

/* get pages by group */
function* getPagesByGroupSaga(action){
    const { response, error } = yield call(pageService.getPagesByGroupApi, action);
    if(response){
        yield put({type: PAGE_ACTION.GET_PAGES_BY_GROUP_SUCCESS, response});
    }else{
        yield put({type: PAGE_ACTION.GET_PAGES_BY_GROUP_FAIL, error});
    }
}

/* get pages by user */
function* getPagesByUserSaga(action){
    const { response, error } = yield call(pageService.getPagesByUserApi, action);
    if(response){
        yield put({type: PAGE_ACTION.GET_PAGES_BY_USER_SUCCESS, response});
    }else{
        yield put({type: PAGE_ACTION.GET_PAGES_BY_USER_FAIL, error});
    }
}

/* get pages by username */
function* getPagesByUsernameSaga(action){
    const { response, error } = yield call(pageService.getPagesByUsernameApi, action);
    if(response){
        yield put({type: PAGE_ACTION.GET_PAGES_BY_USERNAME_SUCCESS, response});
    }else{
        yield put({type: PAGE_ACTION.GET_PAGES_BY_USERNAME_FAIL, error});
    }
}

export default function* pageSaga(){
    yield fork(takeEvery, PAGE_ACTION.GET_PAGES_BY_GROUP, getPagesByGroupSaga);
    yield fork(takeEvery, PAGE_ACTION.GET_PAGES_BY_USER, getPagesByUserSaga);
    yield fork(takeEvery, PAGE_ACTION.GET_PAGES_BY_USERNAME, getPagesByUsernameSaga);
}