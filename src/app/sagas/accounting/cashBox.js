import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_CASH_BOX,
    ADD_CASH_BOX_FAIL,
    ADD_CASH_BOX_SUCCESS,
    GET_ALL_CASH_BOXES,
    GET_ALL_CASH_BOXES_FAIL,
    GET_ALL_CASH_BOXES_SUCCESS,
    GET_ALL_USERS_CASH_BOXES,
    GET_ALL_USERS_CASH_BOXES_FAIL,
    GET_ALL_USERS_CASH_BOXES_SUCCESS,
    GET_CASH_BOXES_BY_USER_ID,
    GET_CASH_BOXES_BY_USER_ID_FAIL,
    GET_CASH_BOXES_BY_USER_ID_SUCCESS
} from '../../actions/accounting/cashBox';
import {
    getAllCashBoxesApi,
    addCashBoxApi,
    getAllUsersCashBoxesApi,
    getAllCashBoxesByUserIdApi
} from '../../services/accounting/cashBox';


function* addCashBoxSaga(action){
    const { response, error } = yield call(addCashBoxApi, action);
    if(response){
        yield put({type: ADD_CASH_BOX_SUCCESS, response});
    }else{
        yield put({type: ADD_CASH_BOX_FAIL, error});
    }
}

function* getAllCashBoxesSaga(action){
    const { response, error } = yield call(getAllCashBoxesApi, action);
    if(response){
        yield put({type: GET_ALL_CASH_BOXES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CASH_BOXES_FAIL, error});
    }
}

function* getAllUsersCashBoxesSaga(action){
    const { response, error } = yield call(getAllUsersCashBoxesApi, action);
    if(response){
        yield put({type: GET_ALL_USERS_CASH_BOXES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_USERS_CASH_BOXES_FAIL, error});
    }
}

function* getAllCashBoxesByUserIdSaga(action){
    const { response, error } = yield call(getAllCashBoxesByUserIdApi, action);
    if(response){
        yield put({type: GET_CASH_BOXES_BY_USER_ID_SUCCESS, response});
    }else{
        yield put({type: GET_CASH_BOXES_BY_USER_ID_FAIL, error});
    }
}

export default function* cashBoxesSaga() {
    yield fork(takeEvery, ADD_CASH_BOX, addCashBoxSaga);
    yield fork(takeEvery, GET_ALL_CASH_BOXES, getAllCashBoxesSaga);
    yield fork(takeEvery, GET_ALL_USERS_CASH_BOXES, getAllUsersCashBoxesSaga);
    yield fork(takeEvery, GET_CASH_BOXES_BY_USER_ID, getAllCashBoxesByUserIdSaga);
}


