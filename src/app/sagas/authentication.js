import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';

/* authentication */
import * as AUTH_ACTION from '../actions/authentication';
import * as authApi from '../services/authentication';

/* USER LOGIN */
function* userLoginSaga(action){
    const { response, error } = yield call(authApi.userLoginApi, action);
    if(response){
        yield put({type: AUTH_ACTION.USER_LOGIN_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.USER_LOGIN_FAIL, error});
    }
}

/* ADD USER */
function* addUserSaga(action){
    const { response, error } = yield call(authApi.AddUserApi, action);
    if(response){
        yield put({type: AUTH_ACTION.ADD_USER_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.ADD_USER_FAIL, error});
    }
}

/* LIST USERS */
function* getUsersStatusSaga(action){
    const { response, error } = yield call(authApi.getUsersStatusApi, action);
    if(response){
        yield put({type: AUTH_ACTION.GET_USERS_STATUS_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.GET_USERS_STATUS_FAIL, error});
    }
}

/* GET USER */
function* getUserSaga(action){
    const { response, error } = yield call(authApi.getUserApi, action);
    if(response){
        yield put({type: AUTH_ACTION.GET_USER_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.GET_USER_FAIL, error});
    }
}

/* UPDATE USER */
function* updateUserSaga(action){
    const { response, error } = yield call(authApi.apiUpdateUser, action);
    if(response){
        yield put({type: AUTH_ACTION.UPDATE_USER_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.UPDATE_USER_FAIL, error});
    }
}

/* DELETE USER */
function* deleteUserSaga(action){
    const { response, error } = yield call(authApi.deleteUserApi, action);
    if(response){
        yield put({type: AUTH_ACTION.DELETE_USER_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.DELETE_USER_FAIL, error});
    }
}

/* ADD GROUP */
function* addGroupSaga(action){
    const { response, error } = yield call(authApi.addGroupApi, action);
    if(response){
        yield put({type: AUTH_ACTION.ADD_GROUP_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.ADD_GROUP_FAIL, error});
    }
}

/* LIST GROUPS */
function* getGroupsStatusSaga(action){
    const { response, error } = yield call(authApi.getGroupsStatusApi, action);
    if(response){
        yield put({type: AUTH_ACTION.GET_GROUPS_STATUS_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.GET_GROUPS_STATUS_FAIL, error});
    }
}

/* GET GROUP */
function* getGroupSaga(action){
    const { response, error } = yield call(authApi.getGroupApi, action);
    if(response){
        yield put({type: AUTH_ACTION.GET_GROUP_SUCCESS, response});
    }else{
        yield put({type: AUTH_ACTION.GET_GROUP_FAIL, error});
    }
}

/* UPDATE GROUP */
function* updateGroupSaga(action){
    const { response, error } = yield call(authApi.updateGroupApi, action);
    if(response){
        yield put({type: AUTH_ACTION.UPDATE_GROUP_SUCCESS, response});
    } else {
        yield put({type: AUTH_ACTION.UPDATE_GROUP_FAIL, error});
    }
}

/* ENABLED GROUP */
function* enabledGroupSaga(action){
    const { response, error } = yield call(authApi.enabledGroupApi, action);
    if(response){
        yield put({type: AUTH_ACTION.ENABLED_GROUP_SUCCESS, response});
    } else {
        yield put({type: AUTH_ACTION.ENABLED_GROUP_FAIL, error});
    }
}

export default function* userAuthenticationSaga() {
    yield fork(takeEvery, AUTH_ACTION.USER_LOGIN, userLoginSaga);
    yield fork(takeEvery, AUTH_ACTION.ADD_USER, addUserSaga);
    yield fork(takeEvery, AUTH_ACTION.GET_USERS_STATUS, getUsersStatusSaga);
    yield fork(takeEvery, AUTH_ACTION.GET_USER, getUserSaga);
    yield fork(takeEvery, AUTH_ACTION.UPDATE_USER, updateUserSaga);
    yield fork(takeEvery, AUTH_ACTION.DELETE_USER, deleteUserSaga);
    yield fork(takeEvery, AUTH_ACTION.ADD_GROUP, addGroupSaga);
    yield fork(takeEvery, AUTH_ACTION.GET_GROUPS_STATUS, getGroupsStatusSaga);
    yield fork(takeEvery, AUTH_ACTION.GET_GROUP, getGroupSaga);
    yield fork(takeEvery, AUTH_ACTION.UPDATE_GROUP, updateGroupSaga);
    yield fork(takeEvery, AUTH_ACTION.ENABLED_GROUP, enabledGroupSaga);
}