import { takeEvery, takeLatest } from "redux-saga";
import { fork } from "redux-saga/effects";

/* authentication */
import userAuthenticationSaga from './authentication';
/* menu */
import menuSaga from './menu';
/** page */
import pageSaga from './page';
/* installer */
import installerSaga from './insallation/installer';
/* job */
import * as constJob from './../actions/setup/job';
import * as job from './setup/job';
/* exchange rate */
import exchangeRateSaga from './setup/exchangeRate';
/* set up ampere */
import { ampereSaga } from './setup/ampere';
/* setting */
import settingSaga from './setup/setting';
/* meter phase */
import meterPhaseSaga from './setup/meterPhase';
/* billing item */
import itemSaga from './inventory/item';
/* meter usage */
import meterUsageSaga from './setup/meterUsage';
/* report */
import groupReportSaga from './report/groupReport';
import icCardDailySaleSaga from './report/icCardDailySale';
import reportRefSaga from './report/reportRef';
import reportSubsidySaleLVSaga from './report/subsidy/reportSaleLV';
import reportSubsidySaleMVSaga from './report/subsidy/reportSaleMV';
/* inventory */
import unitTypeSaga from './inventory/unitType';
import itemGroupSaga from './inventory/itemGroup';
import stockInItemSaga from './inventory/stockInItem';
import itemCategorySaga from './inventory/itemCategory';
import planningSaga from './inventory/planning';
import requestSaga from './inventory/request';
import connectionTypeSaga from './inventory/connectionType';
import purchaseKwhSaga from './inventory/purchaseKwh';
/* invoice */
import groupInvoiceSaga from './invoice/groupInvoices';
import invoicesSaga from './invoice/invoice';
import customerInvoicesSaga from './invoice/customerInvoice';
import customerBookPaymentSaga from './invoice/bookPayment';
/* finance */
import paymentTypesSaga from './customer/finance/paymentType';
import locationPaysSaga from './customer/finance/locationPay';
/** sale_type **/
import saleTypeSaga from './setup/type/saleType';
/** network management **/
import meterChangeSaga from './customer/network/meter';
import installingSaga from './customer/network/installation';
import customerGroupSaga from './setup/type/customerGroup';
import supplierSaga from './inventory/supplier';
import invoiceTypeSaga from './invoice/invoiceType';
import voltageSaga from './setup/type/voltageType';
/** EMPLOYEE */
import employeeSaga from './employee/employee';
/** CUSTOMER */
import customerSaga from './customer/customer';
import punishRatesSaga from './transaction/punish/punishRate';
import punishCustomersSaga from './transaction/punish/punishCustomer';

/** ============================== SET UP ============================== **/
/* OCCUPATION */
import occupationSaga from './setup/occupation';
/* RATE PER KWH */
import ratePerKhwSaga from './setup/rateKwh';
/* POLE TYPE */
import poleTypeSaga from './setup/type/poleType';
/* BOX TYPE */
import boxTypeSaga from './setup/type/boxType';
/* TRANSFORMER TYPE */
import transformerTypeSaga from './setup/type/transformerType';
/* BREAKER TYPE */
import breakerTypeSaga from './setup/type/breakerType';
/* METER TYPE */
import meterTypeSaga from './setup/type/meterType';
/* METER */
import meterSaga from './setup/meter';
/* LOCATION */
import locationSaga from './location';
/* AREA */
import areaSaga from './setup/area';
/* TRANSFORMER */
import transformerSaga from './setup/transformer';
/* POLE */
import poleSaga from './setup/pole';
/* BOX */
import boxSaga from './setup/box';
/* BREAKER */
import breakerSaga from './setup/breaker';
/* CUSTOMER TYPE */
import customerTypeSaga from './setup/type/customerType';
/* CONSUMPTION TYPE */
import consumptionTypeSaga from './setup/type/consumptionType';
import banksSaga from './setup/bank';
/** ============================== SET UP ============================== **/
/* transaction */
import invoicesPrintingSaga from './transaction/invoicePrinting';
import issueBillsSaga from './transaction/issueBill';
import quickPaymentsSaga from './transaction/quickPayment';
import recordKwhMonthlySaga from './transaction/recordKwhMonthly';
import usageEntrySaga from './transaction/usageEntry';
import rateRangesSaga from './setup/rateRange';
import prepaidPowerSaga from './transaction/prepaid/prepaidPower';
import balancePrepaidKwhSaga from './transaction/prepaid/balancePrepaidKwh';
import returnInvoicesSaga from './transaction/prepaid/returnInvoice';
import lostKwhSaga from './transaction/prepaid/lostKwh';
import kwPerDaySaga from './transaction/prepaid/kwPerDay';
/* accounting */
import cashBoxesSaga from './accounting/cashBox';

import mailSettingsSaga from './security/mailSettings';

/** STEP WORK **/
import stepWorkSaga from './insallation/stepwork/stepwork';
import rentPoleSaga from './customer/network/license';


/**
 * REPORT
 */
import transformerReportSaga from './report/transformer';

export function* sagas() {
    yield [
        /* user authentication */
        fork(userAuthenticationSaga),
        /** ============================== SET UP ============================== **/
        /* RATE PER KWH */
        fork(ratePerKhwSaga),
        /* OCCUPATION */
        fork(occupationSaga),
        /* LOCATION */
        fork(locationSaga),
        /* POLE TYPE */
        fork(poleTypeSaga),
        /* BOX TYPE */
        fork(boxTypeSaga),
        /* BOX */
        fork(boxSaga),
        /* TRANSFORMER TYPE */
        fork(transformerTypeSaga),
        /* BREAKER TYPE */
        fork(breakerTypeSaga),
        /* METER TYPE */
        fork(meterTypeSaga),
        /* AREA */
        fork(areaSaga),
        /* TRANSFORMER */
        fork(transformerSaga),
        /* POLE */
        fork(poleSaga),
        /* BREAKER */
        fork(breakerSaga),
        /* METER */
        fork(meterSaga),
        /* CUSTOMER TYPE */
        fork(customerTypeSaga),
        /* CONSUMPTION TYPE */
        fork(consumptionTypeSaga),
        fork(banksSaga),
        /** ============================== SET UP ============================== **/


        /** EMPLOYEE **/
        fork(employeeSaga),
        /** CUSTOMER **/
        fork(customerSaga),
        fork(punishRatesSaga),
        fork(punishCustomersSaga),
        /* menu */
        fork(menuSaga),

        /* page */
        fork(pageSaga),

        /* installer*/
        fork(installerSaga),

        /* job */
        fork(takeEvery, constJob.GET_ALL_JOBS, job.listAllJobsSaga),

        /* exchange rate */
        fork(exchangeRateSaga),

        /* set up ampere */
        fork(ampereSaga),

        /* setting */
        fork(settingSaga),

        /* meter phase */
        fork(meterPhaseSaga),

        /* meter usage */
        fork(meterUsageSaga),

        /* billing item */
        fork(itemSaga),

        /* report */
        fork(groupReportSaga),
        fork(icCardDailySaleSaga),
        fork(reportRefSaga),
        fork(reportSubsidySaleLVSaga),
        fork(reportSubsidySaleMVSaga),

        /* inventory */
        fork(unitTypeSaga),
        fork(itemGroupSaga),
        fork(stockInItemSaga),
        fork(itemCategorySaga),
        fork(planningSaga),
        fork(requestSaga),
        fork(connectionTypeSaga),
        fork(purchaseKwhSaga),

        /* finance */
        fork(customerGroupSaga),
        fork(paymentTypesSaga),
        fork(locationPaysSaga),

        /* invoice */
        fork(groupInvoiceSaga),
        fork(invoicesSaga),
        fork(customerInvoicesSaga),
        fork(customerBookPaymentSaga),
        
        /* sale type */
        fork(saleTypeSaga),

        /** network management **/
        fork(meterChangeSaga),
        fork(installingSaga),
        fork(supplierSaga),
        fork(invoiceTypeSaga),
        fork(voltageSaga),

        /* transaction */
        fork(invoicesPrintingSaga),
        fork(issueBillsSaga),
        fork(quickPaymentsSaga),
        fork(recordKwhMonthlySaga),
        fork(usageEntrySaga),
        fork(prepaidPowerSaga),
        fork(balancePrepaidKwhSaga),
        fork(returnInvoicesSaga),
        fork(lostKwhSaga),
        fork(kwPerDaySaga),

        /** mail settings */
        fork(mailSettingsSaga),

        fork(rateRangesSaga),
        /* accounting */
        fork(cashBoxesSaga),

        /** STEP WORK **/
        fork(stepWorkSaga),
        /** rentPole **/
        fork(rentPoleSaga),

        /** REPORT */
        fork(transformerReportSaga)
    ];
}