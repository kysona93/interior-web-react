import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_CUSTOMER_BILLS,
    GET_ALL_CUSTOMER_BILLS_FAIL,
    GET_ALL_CUSTOMER_BILLS_SUCCESS,
    ISSUE_BILL_CUSTOMERS,
    ISSUE_BILL_CUSTOMERS_FAIL,
    ISSUE_BILL_CUSTOMERS_SUCCESS,
    GET_ALL_CUSTOMERS_PROBLEM,
    GET_ALL_CUSTOMERS_PROBLEM_FAIL,
    GET_ALL_CUSTOMERS_PROBLEM_SUCCESS
} from './../../actions/transaction/issueBill';
import {
    getAllCustomerBillsApi,
    issueBillCustomersApi
} from './../../services/transaction/issueBill';

function* getAllCustomerBillsSaga(action){
    const { response, error } = yield call(getAllCustomerBillsApi, action);
    if(response){
        yield put({type: GET_ALL_CUSTOMER_BILLS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CUSTOMER_BILLS_FAIL, error});
    }
}

function* issueCustomerBillSaga(action){
    const { response, error } = yield call(issueBillCustomersApi, action);
    if(response){
        yield put({type: ISSUE_BILL_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: ISSUE_BILL_CUSTOMERS_FAIL, error});
    }
}

function* getAllCustomersProblemSaga(action){
    const { response, error } = yield call(getAllCustomerBillsApi, action);
    if(response){
        yield put({type: GET_ALL_CUSTOMERS_PROBLEM_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CUSTOMERS_PROBLEM_FAIL, error});
    }
}

export default function* issueBillsSaga() {
    yield fork(takeEvery, GET_ALL_CUSTOMER_BILLS, getAllCustomerBillsSaga);
    yield fork(takeEvery, ISSUE_BILL_CUSTOMERS, issueCustomerBillSaga);
    yield fork(takeEvery, GET_ALL_CUSTOMERS_PROBLEM, getAllCustomersProblemSaga);
}


