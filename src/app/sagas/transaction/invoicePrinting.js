import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_ISSUE_INVOICES,
    GET_ALL_ISSUE_INVOICES_FAIL,
    GET_ALL_ISSUE_INVOICES_SUCCESS,
    GENERATE_ALL_INVOICE,
    GENERATE_ALL_INVOICE_FAIL,
    GENERATE_ALL_INVOICE_SUCCESS
} from './../../actions/transaction/invoicePrinting';
import {
    getAllIssueInvoicesApi,
    generateAllInvoicesApi
} from './../../services/transaction/invoicePrinting';

function* getAllIssueInvoicesSaga(action){
    const { response, error } = yield call(getAllIssueInvoicesApi, action);
    if(response){
        yield put({type: GET_ALL_ISSUE_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_ISSUE_INVOICES_FAIL, error});
    }
}

function* generateAllInvoicesSaga(action){
    const { response, error } = yield call(generateAllInvoicesApi, action);
    if(response){
        yield put({type: GENERATE_ALL_INVOICE_SUCCESS, response});
    }else{
        yield put({type: GENERATE_ALL_INVOICE_FAIL, error});
    }
}

export default function* invoicesPrintingSaga() {
    yield fork(takeEvery, GET_ALL_ISSUE_INVOICES, getAllIssueInvoicesSaga);
    yield fork(takeEvery, GENERATE_ALL_INVOICE, generateAllInvoicesSaga);
}


