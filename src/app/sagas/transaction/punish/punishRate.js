import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_PUNISH_RATE,
    ADD_PUNISH_RATE_FAIL,
    ADD_PUNISH_RATE_SUCCESS,
    GET_ALL_PUNISH_RATE,
    GET_ALL_PUNISH_RATE_FAIL,
    GET_ALL_PUNISH_RATE_SUCCESS,
    UPDATE_PUNISH_RATE,
    UPDATE_PUNISH_RATE_FAIL,
    UPDATE_PUNISH_RATE_SUCCESS,
    DELETE_PUNISH_RATE,
    DELETE_PUNISH_RATE_FAIL,
    DELETE_PUNISH_RATE_SUCCESS
} from '../../../actions/transaction/punish/punishRate';
import {
    addPunishRateApi,
    getAllPunishRatesApi,
    updatePunishRateApi,
    deletePunishRateApi
} from '../../../services/transaction/punish/punishRate';

function* getAllPunishRatesSaga(action){
    const { response, error } = yield call(getAllPunishRatesApi, action);
    if(response){
        yield put({type: GET_ALL_PUNISH_RATE_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_PUNISH_RATE_FAIL, error});
    }
}

function* addPunishRateSaga(action){
    const { response, error } = yield call(addPunishRateApi, action);
    if(response){
        yield put({type: ADD_PUNISH_RATE_SUCCESS, response});
    }else{
        yield put({type: ADD_PUNISH_RATE_FAIL, error});
    }
}

function* updatePunishRateSaga(action){
    const { response, error } = yield call(updatePunishRateApi, action);
    if(response){
        yield put({type: UPDATE_PUNISH_RATE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_PUNISH_RATE_FAIL, error});
    }
}

function* deletePunishRateSaga(action){
    const { response, error } = yield call(deletePunishRateApi, action);
    if(response){
        yield put({type: DELETE_PUNISH_RATE_SUCCESS, response});
    }else{
        yield put({type: DELETE_PUNISH_RATE_FAIL, error});
    }
}

export default function* punishRatesSaga() {
    yield fork(takeEvery, GET_ALL_PUNISH_RATE, getAllPunishRatesSaga);
    yield fork(takeEvery, ADD_PUNISH_RATE, addPunishRateSaga);
    yield fork(takeEvery, UPDATE_PUNISH_RATE, updatePunishRateSaga);
    yield fork(takeEvery, DELETE_PUNISH_RATE, deletePunishRateSaga);
}


