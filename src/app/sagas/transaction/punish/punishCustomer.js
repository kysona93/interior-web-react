import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_PUNISH_INVOICES_SUCCESS,
    GET_ALL_PUNISH_INVOICES_FAIL,
    GET_ALL_PUNISH_INVOICES,
    ADD_PUNISH_CUSTOMERS,
    ADD_PUNISH_CUSTOMERS_FAIL,
    ADD_PUNISH_CUSTOMERS_SUCCESS,
    GET_ALL_PUNISH_CUSTOMERS,
    GET_ALL_PUNISH_CUSTOMERS_FAIL,
    GET_ALL_PUNISH_CUSTOMERS_SUCCESS,
    UPDATE_PUNISH_CUSTOMER,
    UPDATE_PUNISH_CUSTOMER_FAIL,
    UPDATE_PUNISH_CUSTOMER_SUCCESS,
    DELETE_PUNISH_CUSTOMER,
    DELETE_PUNISH_CUSTOMER_FAIL,
    DELETE_PUNISH_CUSTOMER_SUCCESS
} from '../../../actions/transaction/punish/punishCustomer';
import {
     getAllInvoicesPunishApi,
     addCustomerPunishApi,
     getAllCustomersPunishRatesApi,
     updateCustomerPunishApi,
     deleteCustomerPunishApi
} from '../../../services/transaction/punish/punishCustomer';

function* getAllInvoicesPunishSaga(action){
    const { response, error } = yield call(getAllInvoicesPunishApi, action);
    if(response){
        yield put({type: GET_ALL_PUNISH_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_PUNISH_INVOICES_FAIL, error});
    }
}

function* addCustomerPunishSaga(action){
    const { response, error } = yield call(addCustomerPunishApi, action);
    if(response){
        yield put({type: ADD_PUNISH_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: ADD_PUNISH_CUSTOMERS_FAIL, error});
    }
}

function* getAllCustomersPunishSaga(action){
    const { response, error } = yield call(getAllCustomersPunishRatesApi, action);
    if(response){
        yield put({type: GET_ALL_PUNISH_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_PUNISH_CUSTOMERS_FAIL, error});
    }
}

function* updateCustomerPunishSaga(action){
    const { response, error } = yield call(updateCustomerPunishApi, action);
    if(response){
        yield put({type: UPDATE_PUNISH_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: UPDATE_PUNISH_CUSTOMER_FAIL, error});
    }
}

function* deleteCustomerPunishSaga(action){
    const { response, error } = yield call(deleteCustomerPunishApi, action);
    if(response){
        yield put({type: DELETE_PUNISH_CUSTOMER_SUCCESS, response});
    }else{
        yield put({type: DELETE_PUNISH_CUSTOMER_FAIL, error});
    }
}

export default function* punishCustomersSaga() {
     yield fork(takeEvery, GET_ALL_PUNISH_INVOICES, getAllInvoicesPunishSaga);
     yield fork(takeEvery, ADD_PUNISH_CUSTOMERS, addCustomerPunishSaga);
     yield fork(takeEvery, GET_ALL_PUNISH_CUSTOMERS, getAllCustomersPunishSaga);
     yield fork(takeEvery, UPDATE_PUNISH_CUSTOMER, updateCustomerPunishSaga);
     yield fork(takeEvery, DELETE_PUNISH_CUSTOMER, deleteCustomerPunishSaga);
}


