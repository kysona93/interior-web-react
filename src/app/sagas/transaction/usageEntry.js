import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_METER_USAGE_RECORD,
    ADD_METER_USAGE_RECORD_FAIL,
    ADD_METER_USAGE_RECORD_SUCCESS,
    IMPORT_EXCEL_METER_USAGE_RECORD,
    IMPORT_EXCEL_METER_USAGE_RECORD_FAIL,
    IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS,
    GET_CUSTOMER_INFO_BY_METER_SERIAL,
    GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL,
    GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS,
    GET_METERS_BY_CUSTOMER_ID,
    GET_METERS_BY_CUSTOMER_ID_FAIL,
    GET_METERS_BY_CUSTOMER_ID_SUCCESS,
    GET_METER_USAGE_BY_METER_SERIAL,
    GET_METER_USAGE_BY_METER_SERIAL_FAIL,
    GET_METER_USAGE_BY_METER_SERIAL_SUCCESS,
    UPDATE_METER_USAGE_BY_ID,
    UPDATE_METER_USAGE_BY_ID_FAIL,
    UPDATE_METER_USAGE_BY_ID_SUCCESS,
    VERIFY_METER_USAGE_INDIVIDUAL,
    VERIFY_METER_USAGE_INDIVIDUAL_FAIL,
    VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS,
    GET_ALL_CUSTOMER_IDS_AND_SERIALS,
    GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL,
    GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS,
    GET_METER_USAGE_PROBLEMS,
    GET_METER_USAGE_PROBLEMS_FAIL,
    GET_METER_USAGE_PROBLEMS_SUCCESS
} from './../../actions/transaction/usageEntry';
import {
    getCustomerInfoByMeterSerialApi,
    addMeterUsageRecordApi,
    importExcelMeterUsageRecordApi,
    getMetersByCustomerIdApi,
    getMeterUsageByMeterSerialApi,
    updateMeterUsageByIdApi,
    verifyMeterUsageIndividualApi,
    getAllCustomerIdsAndMeterSerialsApi,
    getMeterUsageProblemsApi
} from './../../services/transaction/usageEntry';

function* getCustomerInfoByMeterSerialSaga(action){
    const { response, error } = yield call(getCustomerInfoByMeterSerialApi, action);
    if(response){
        yield put({type: GET_CUSTOMER_INFO_BY_METER_SERIAL_SUCCESS, response});
    }else{
        yield put({type: GET_CUSTOMER_INFO_BY_METER_SERIAL_FAIL, error});
    }
}

function* addMeterUsageRecordSaga(action){
    const { response, error } = yield call(addMeterUsageRecordApi, action);
    if(response){
        yield put({type: ADD_METER_USAGE_RECORD_SUCCESS, response});
    }else{
        yield put({type: ADD_METER_USAGE_RECORD_FAIL, error});
    }
}

function* importExcelMeterUsageRecordSaga(action){
    const { response, error } = yield call(importExcelMeterUsageRecordApi, action);
    if(response){
        yield put({type: IMPORT_EXCEL_METER_USAGE_RECORD_SUCCESS, response});
    }else{
        yield put({type: IMPORT_EXCEL_METER_USAGE_RECORD_FAIL, error});
    }
}

function* getMetersByCustomerIdSaga(action){
    const { response, error } = yield call(getMetersByCustomerIdApi, action);
    if(response){
        yield put({type: GET_METERS_BY_CUSTOMER_ID_SUCCESS, response});
    }else{
        yield put({type: GET_METERS_BY_CUSTOMER_ID_FAIL, error});
    }
}

function* getMeterUsageByMeterSerialSaga(action){
    const { response, error } = yield call(getMeterUsageByMeterSerialApi, action);
    if(response){
        yield put({type: GET_METER_USAGE_BY_METER_SERIAL_SUCCESS, response});
    }else{
        yield put({type: GET_METER_USAGE_BY_METER_SERIAL_FAIL, error});
    }
}

function* updateMeterUsageByIdSaga(action){
    const { response, error } = yield call(updateMeterUsageByIdApi, action);
    if(response){
        yield put({type: UPDATE_METER_USAGE_BY_ID_SUCCESS, response});
    }else{
        yield put({type: UPDATE_METER_USAGE_BY_ID_FAIL, error});
    }
}

function* verifyMeterUsageIndividualSaga(action){
    const { response, error } = yield call(verifyMeterUsageIndividualApi, action);
    if(response){
        yield put({type: VERIFY_METER_USAGE_INDIVIDUAL_SUCCESS, response});
    }else{
        yield put({type: VERIFY_METER_USAGE_INDIVIDUAL_FAIL, error});
    }
}

function* getAllCustomerIdsAndMeterSerialsSaga(action){
    const { response, error } = yield call(getAllCustomerIdsAndMeterSerialsApi, action);
    if(response){
        yield put({type: GET_ALL_CUSTOMER_IDS_AND_SERIALS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_CUSTOMER_IDS_AND_SERIALS_FAIL, error});
    }
}

function* getMeterUsageProblemsSaga(action){
    const { response, error } = yield call(getMeterUsageProblemsApi, action);
    if(response){
        yield put({type: GET_METER_USAGE_PROBLEMS_FAIL, response});
    }else{
        yield put({type: GET_METER_USAGE_PROBLEMS_SUCCESS, error});
    }
}

export default function* usageEntrySaga() {
    yield fork(takeEvery, GET_CUSTOMER_INFO_BY_METER_SERIAL, getCustomerInfoByMeterSerialSaga);
    yield fork(takeEvery, ADD_METER_USAGE_RECORD, addMeterUsageRecordSaga);
    yield fork(takeEvery, IMPORT_EXCEL_METER_USAGE_RECORD, importExcelMeterUsageRecordSaga);
    yield fork(takeEvery, GET_METERS_BY_CUSTOMER_ID, getMetersByCustomerIdSaga);
    yield fork(takeEvery, GET_METER_USAGE_BY_METER_SERIAL, getMeterUsageByMeterSerialSaga);
    yield fork(takeEvery, UPDATE_METER_USAGE_BY_ID, updateMeterUsageByIdSaga);
    yield fork(takeEvery, VERIFY_METER_USAGE_INDIVIDUAL, verifyMeterUsageIndividualSaga);
    yield fork(takeEvery, GET_ALL_CUSTOMER_IDS_AND_SERIALS, getAllCustomerIdsAndMeterSerialsSaga);
    yield fork(takeEvery, GET_METER_USAGE_PROBLEMS, getMeterUsageProblemsSaga);
}


