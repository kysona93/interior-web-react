import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_INVOICES_CUSTOMER_PAYMENT,
    GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL,
    GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS,
    ADD_QUICK_PAID_PAYMENT,
    ADD_QUICK_PAID_PAYMENT_FAIL,
    ADD_QUICK_PAID_PAYMENT_SUCCESS,
    GET_INVOICE_BY_BARCODE,
    GET_INVOICE_BY_BARCODE_FAIL,
    GET_INVOICE_BY_BARCODE_SUCCESS
} from './../../actions/transaction/quickPayment';
import {
    getInvoiceByBarcodeApi,
    addQuickPaidPaymentApi,
    getAllInvoicesCustomerPaymentApi
} from './../../services/transaction/quickPayment';

function* getAllInvoicesCustomerPaymentSaga(action){
    const { response, error } = yield call(getAllInvoicesCustomerPaymentApi, action);
    if(response){
        yield put({type: GET_ALL_INVOICES_CUSTOMER_PAYMENT_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_INVOICES_CUSTOMER_PAYMENT_FAIL, error});
    }
}

function* addQuickPaidPaymentSaga(action){
    const { response, error } = yield call(addQuickPaidPaymentApi, action);
    if(response){
        yield put({type: ADD_QUICK_PAID_PAYMENT_SUCCESS, response});
    }else{
        yield put({type: ADD_QUICK_PAID_PAYMENT_FAIL, error});
    }
}

function* getInvoiceByBarcodeSaga(action){
    const { response, error } = yield call(getInvoiceByBarcodeApi, action);
    if(response){
        yield put({type: GET_INVOICE_BY_BARCODE_SUCCESS, response});
    }else{
        yield put({type: GET_INVOICE_BY_BARCODE_FAIL, error});
    }
}

export default function* quickPaymentsSaga() {
    yield fork(takeEvery, GET_ALL_INVOICES_CUSTOMER_PAYMENT, getAllInvoicesCustomerPaymentSaga);
    yield fork(takeEvery, ADD_QUICK_PAID_PAYMENT, addQuickPaidPaymentSaga);
    yield fork(takeEvery, GET_INVOICE_BY_BARCODE, getInvoiceByBarcodeSaga);
}


