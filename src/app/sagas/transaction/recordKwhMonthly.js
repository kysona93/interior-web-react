import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_RECORD_KWH_MONTHLY,
    GET_ALL_RECORD_KWH_MONTHLY_FAIL,
    GET_ALL_RECORD_KWH_MONTHLY_SUCCESS
} from './../../actions/transaction/recordKhwMonthly';
import {
    getRecordKwhMonthlyApi
} from './../../services/transaction/recordKwhMonthly';

function* getAllRecordKwhMonthlySaga(action){
    const { response, error } = yield call(getRecordKwhMonthlyApi, action);
    if(response){
        yield put({type: GET_ALL_RECORD_KWH_MONTHLY_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_RECORD_KWH_MONTHLY_FAIL, error});
    }
}

export default function* recordKwhMonthlySaga() {
    yield fork(takeEvery, GET_ALL_RECORD_KWH_MONTHLY, getAllRecordKwhMonthlySaga);
}


