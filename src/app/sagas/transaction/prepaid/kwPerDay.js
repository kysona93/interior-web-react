import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    GET_ALL_GENERATE_KW_PER_DAY,
    GET_ALL_GENERATE_KW_PER_DAY_FAIL,
    GET_ALL_GENERATE_KW_PER_DAY_SUCCESS,
    GENERATE_KW_PER_DAY,
    GENERATE_KW_PER_DAY_FAIL,
    GENERATE_KW_PER_DAY_SUCCESS
} from '../../../actions/transaction/prepaid/kwPerDay';
import {
    getAllGenerateKwPerDayApi,
    generateKwPerDayApi
} from '../../../services/transaction/prepaid/kwPerDay';

function* getAllGenerateKwPerDaySaga(action){
    const { response, error } = yield call(getAllGenerateKwPerDayApi, action);
    if(response){
        yield put({type: GET_ALL_GENERATE_KW_PER_DAY_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_GENERATE_KW_PER_DAY_FAIL, error});
    }
}

function* generateKwPerDaySaga(action){
    const { response, error } = yield call(generateKwPerDayApi, action);
    if(response){
        yield put({type: GENERATE_KW_PER_DAY_SUCCESS, response});
    }else{
        yield put({type: GENERATE_KW_PER_DAY_FAIL, error});
    }
}

export default function* kwPerDaySaga() {
    yield fork(takeEvery, GET_ALL_GENERATE_KW_PER_DAY, getAllGenerateKwPerDaySaga);
    yield fork(takeEvery, GENERATE_KW_PER_DAY, generateKwPerDaySaga);
}