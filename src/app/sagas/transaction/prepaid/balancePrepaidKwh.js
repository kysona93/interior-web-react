import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_BALANCE_PREPAID_KWH,
    ADD_BALANCE_PREPAID_KWH_FAIL,
    ADD_BALANCE_PREPAID_KWH_SUCCESS,
    GET_ALL_BALANCE_PREPAID_KWH,
    GET_ALL_BALANCE_PREPAID_KWH_FAIL,
    GET_ALL_BALANCE_PREPAID_KWH_SUCCESS,
    GET_ALL_BILL_BALANCE_PREPAID,
    GET_ALL_BILL_BALANCE_PREPAID_FAIL,
    GET_ALL_BILL_BALANCE_PREPAID_SUCCESS,
    VOID_BALANCE_PREPAID_KW,
    VOID_BALANCE_PREPAID_KW_FAIL,
    VOID_BALANCE_PREPAID_KW_SUCCESS,
    GET_ALL_NEW_PREPAID_CUSTOMERS,
    GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL,
    GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS,
    SAVE_BALANCE_PREPAID_KWH_FAIL,
    SAVE_BALANCE_PREPAID_KWH_SUCCESS,
    SAVE_BALANCE_PREPAID_KWH,
    LIST_BALANCE_PREPAID_KW,
    LIST_BALANCE_PREPAID_KW_FAIL,
    LIST_BALANCE_PREPAID_KW_SUCCESS,
    UPDATE_BALANCE_PREPAID_KW,
    UPDATE_BALANCE_PREPAID_KW_FAIL,
    UPDATE_BALANCE_PREPAID_KW_SUCCESS
} from '../../../actions/transaction/prepaid/balancePrepaidKwh';
import {
    addBalancePrepaidKwhApi,
    getAllBalancePrepaidKwhApi,
    getAllBillBalancePrepaidApi,
    voidBalancePrepaidApi,
    getAllNewPrepaidCustomersApi,
    saveBalancePrepaidKwApi,
    listAllBalancePrepaidKwApi,
    updateBalancePrepaidKwApi
} from '../../../services/transaction/prepaid/balancePrepaidKwh';

function* addBalancePrepaidKwhSaga(action){
    const { response, error } = yield call(addBalancePrepaidKwhApi, action);
    if(response){
        yield put({type: ADD_BALANCE_PREPAID_KWH_SUCCESS, response});
    }else{
        yield put({type: ADD_BALANCE_PREPAID_KWH_FAIL, error});
    }
}

function* getAllBalancePrepaidKwhSaga(action){
    const { response, error } = yield call(getAllBalancePrepaidKwhApi, action);
    if(response){
        yield put({type: GET_ALL_BALANCE_PREPAID_KWH_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_BALANCE_PREPAID_KWH_FAIL, error});
    }
}

function* getAllBillBalancePrepaidSaga(action){
    const { response, error } = yield call(getAllBillBalancePrepaidApi, action);
    if(response){
        yield put({type: GET_ALL_BILL_BALANCE_PREPAID_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_BILL_BALANCE_PREPAID_FAIL, error});
    }
}

function* voidBalancePrepaidSaga(action){
    const { response, error } = yield call(voidBalancePrepaidApi, action);
    if(response){
        yield put({type: VOID_BALANCE_PREPAID_KW_SUCCESS, response});
    }else{
        yield put({type: VOID_BALANCE_PREPAID_KW_FAIL, error});
    }
}

function* getAllNewPrepaidCustomersSaga(action){
    const { response, error } = yield call(getAllNewPrepaidCustomersApi, action);
    if(response){
        yield put({type: GET_ALL_NEW_PREPAID_CUSTOMERS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_NEW_PREPAID_CUSTOMERS_FAIL, error});
    }
}

function* saveBalancePrepaidKwhSaga(action){
    const { response, error } = yield call(saveBalancePrepaidKwApi, action);
    if(response){
        yield put({type: SAVE_BALANCE_PREPAID_KWH_SUCCESS, response});
    }else{
        yield put({type: SAVE_BALANCE_PREPAID_KWH_FAIL, error});
    }
}

function* listAllBalancePrepaidKwhSaga(action){
    const { response, error } = yield call(listAllBalancePrepaidKwApi, action);
    if(response){
        yield put({type: LIST_BALANCE_PREPAID_KW_SUCCESS, response});
    }else{
        yield put({type: LIST_BALANCE_PREPAID_KW_FAIL, error});
    }
}

function* updateBalancePrepaidKwhSaga(action){
    const { response, error } = yield call(updateBalancePrepaidKwApi, action);
    if(response){
        yield put({type: UPDATE_BALANCE_PREPAID_KW_SUCCESS, response});
    }else{
        yield put({type: UPDATE_BALANCE_PREPAID_KW_FAIL, error});
    }
}


export default function* balancePrepaidKwhSaga() {
    yield fork(takeEvery, ADD_BALANCE_PREPAID_KWH, addBalancePrepaidKwhSaga);
    yield fork(takeEvery, GET_ALL_BALANCE_PREPAID_KWH, getAllBalancePrepaidKwhSaga);
    yield fork(takeEvery, GET_ALL_BILL_BALANCE_PREPAID, getAllBillBalancePrepaidSaga);
    yield fork(takeEvery, VOID_BALANCE_PREPAID_KW, voidBalancePrepaidSaga);
    yield fork(takeEvery, GET_ALL_NEW_PREPAID_CUSTOMERS, getAllNewPrepaidCustomersSaga);
    yield fork(takeEvery, SAVE_BALANCE_PREPAID_KWH, saveBalancePrepaidKwhSaga);

    yield fork(takeEvery, LIST_BALANCE_PREPAID_KW, listAllBalancePrepaidKwhSaga);
    yield fork(takeEvery, UPDATE_BALANCE_PREPAID_KW, updateBalancePrepaidKwhSaga);
}