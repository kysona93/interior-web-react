import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ISSUE_BILL_PREPAID,
    ISSUE_BILL_PREPAID_FAIL,
    ISSUE_BILL_PREPAID_SUCCESS,
    GET_ALL_RETURN_INVOICES,
    GET_ALL_RETURN_INVOICES_FAIL,
    GET_ALL_RETURN_INVOICES_SUCCESS,
    GENERATE_RETURN_INVOICES,
    GENERATE_RETURN_INVOICES_FAIL,
    GENERATE_RETURN_INVOICES_SUCCESS,
    GET_RETURN_INVOICE_BY_PREPAID_ID,
    GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL,
    GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS,
    UPDATE_RETURN_INVOICE_BY_PREPAID_ID,
    UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL,
    UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS
} from '../../../actions/transaction/prepaid/returnInvoice';
import {
    issueBillPrepaidApi,
    getAllReturnInvoicesApi,
    generateAllReturnInvoicesApi,
    getReturnInvoiceByPrepaidIdApi,
    updateReturnInvoiceByPrepaidIdApi
} from '../../../services/transaction/prepaid/returnInvoice';

function* addIssueBillPrepaidSaga(action){
    const { response, error } = yield call(issueBillPrepaidApi, action);
    if(response){
        yield put({type: ISSUE_BILL_PREPAID_SUCCESS, response});
    }else{
        yield put({type: ISSUE_BILL_PREPAID_FAIL, error});
    }
}

function* getAllReturnInvoicesSaga(action){
    const { response, error } = yield call(getAllReturnInvoicesApi, action);
    if(response){
        yield put({type: GET_ALL_RETURN_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_RETURN_INVOICES_FAIL, error});
    }
}

function* generateAllReturnInvoicesSaga(action){
    const { response, error } = yield call(generateAllReturnInvoicesApi, action);
    if(response){
        yield put({type: GENERATE_RETURN_INVOICES_SUCCESS, response});
    }else{
        yield put({type: GENERATE_RETURN_INVOICES_FAIL, error});
    }
}

function* getReturnInvoiceByPrepaidIdSaga(action){
    const { response, error } = yield call(getReturnInvoiceByPrepaidIdApi, action);
    if(response){
        yield put({type: GET_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS, response});
    }else{
        yield put({type: GET_RETURN_INVOICE_BY_PREPAID_ID_FAIL, error});
    }
}

function* updateReturnInvoiceByPrepaidIdSaga(action){
    const { response, error } = yield call(updateReturnInvoiceByPrepaidIdApi, action);
    if(response){
        yield put({type: UPDATE_RETURN_INVOICE_BY_PREPAID_ID_SUCCESS, response});
    }else{
        yield put({type: UPDATE_RETURN_INVOICE_BY_PREPAID_ID_FAIL, error});
    }
}

export default function* returnInvoicesSaga() {
    yield fork(takeEvery, ISSUE_BILL_PREPAID, addIssueBillPrepaidSaga);
    yield fork(takeEvery, GET_ALL_RETURN_INVOICES, getAllReturnInvoicesSaga);
    yield fork(takeEvery, GENERATE_RETURN_INVOICES, generateAllReturnInvoicesSaga);
    yield fork(takeEvery, GET_RETURN_INVOICE_BY_PREPAID_ID, getReturnInvoiceByPrepaidIdSaga);
    yield fork(takeEvery, UPDATE_RETURN_INVOICE_BY_PREPAID_ID, updateReturnInvoiceByPrepaidIdSaga);
}