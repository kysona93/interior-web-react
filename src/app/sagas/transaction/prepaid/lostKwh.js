import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_LOST_KWH,
    ADD_LOST_KWH_FAIL,
    ADD_LOST_KWH_SUCCESS,
    GET_ALL_LOST_KWH,
    GET_ALL_LOST_KWH_FAIL,
    GET_ALL_LOST_KWH_SUCCESS,
    GET_ALL_METER_IDS,
    GET_ALL_METER_IDS_FAIL,
    GET_ALL_METER_IDS_SUCCESS,
    GET_ALL_LOST_KWH_STATUS,
    GET_ALL_LOST_KWH_STATUS_FAIL,
    GET_ALL_LOST_KWH_STATUS_SUCCESS
} from '../../../actions/transaction/prepaid/lostKwh';
import {
    addLostKwhApi,
    getAllLostKwhApi,
    getAllMeterIdsApi,
    getAllLostKwhStatusApi
} from '../../../services/transaction/prepaid/lostKwh';

function* addLostKwhSaga(action){
    const { response, error } = yield call(addLostKwhApi, action);
    if(response){
        yield put({type: ADD_LOST_KWH_SUCCESS, response});
    }else{
        yield put({type: ADD_LOST_KWH_FAIL, error});
    }
}

function* getAllLostKwhSaga(action){
    const { response, error } = yield call(getAllLostKwhApi, action);
    if(response){
        yield put({type: GET_ALL_LOST_KWH_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_LOST_KWH_FAIL, error});
    }
}

function* getAllMeterIdsSaga(action){
    const { response, error } = yield call(getAllMeterIdsApi, action);
    if(response){
        yield put({type: GET_ALL_METER_IDS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_METER_IDS_FAIL, error});
    }
}

function* getAllLostKwhStatusSaga(action){
    const { response, error } = yield call(getAllLostKwhStatusApi, action);
    if(response){
        yield put({type: GET_ALL_LOST_KWH_STATUS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_LOST_KWH_STATUS_FAIL, error});
    }
}

export default function* lostKwhSaga() {
    yield fork(takeEvery, ADD_LOST_KWH, addLostKwhSaga);
    yield fork(takeEvery, GET_ALL_LOST_KWH, getAllLostKwhSaga);
    yield fork(takeEvery, GET_ALL_METER_IDS, getAllMeterIdsSaga);
    yield fork(takeEvery, GET_ALL_LOST_KWH_STATUS, getAllLostKwhStatusSaga);

}