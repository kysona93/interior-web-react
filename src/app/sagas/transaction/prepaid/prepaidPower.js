import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from './../../../actions/transaction/prepaid/prepaidPower';
import * as API from './../../../services/transaction/prepaid/prepaidPower';

function* getPrepaidPowersSaga(action){
    const { response, error } = yield call(API.getPrepaidPowersApi, action);
    if(response){
        yield put({type: ACTION.GET_PREPAID_POWERS_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PREPAID_POWERS_FAIL, error});
    }
}

function* addPrepaidPowerSaga(action){
    const { response, error } = yield call(API.addPrepaidPowerApi, action);
    if(response){
        yield put({type: ACTION.ADD_PREPAID_POWER_SUCCESS, response});
    }else{
        yield put({type: ACTION.ADD_PREPAID_POWER_FAIL, error});
    }
}

function* getAllPrepaidPowersSaga(action){
    const { response, error } = yield call(API.getAllPrepaidPowerApi, action);
    if(response){
        yield put({type: ACTION.GET_ALL_PREPAID_POWERS_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_ALL_PREPAID_POWERS_FAIL, error});
    }
}

function* getAllVerifyPrepaidEndPurchaseSaga(action){
    const { response, error } = yield call(API.getAllVerifyPrepaidEndPurchaseApi, action);
    if(response){
        yield put({type: ACTION.GET_ALL_PREPAID_END_PURCHASES_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_ALL_PREPAID_END_PURCHASES_FAIL, error});
    }
}

export default function* prepaidPowerSaga() {
    yield fork(takeEvery, ACTION.GET_PREPAID_POWERS, getPrepaidPowersSaga);
    yield fork(takeEvery, ACTION.ADD_PREPAID_POWER, addPrepaidPowerSaga);
    yield fork(takeEvery, ACTION.GET_ALL_PREPAID_POWERS, getAllPrepaidPowersSaga);
    yield fork(takeEvery, ACTION.GET_ALL_PREPAID_END_PURCHASES, getAllVerifyPrepaidEndPurchaseSaga);
}


