import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as REPORT_REF from './../../actions/report/reportRef';
import * as REPORT_REF_API from './../../services/report/reportRef';

/* report ref 0 kw */
function* getReportRefPostpaid0KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPostpaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_0_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_0_KW_FAIL, error});
    }
}
function* getReportRefPrepaid0KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_0_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_0_KW_FAIL, error});
    }
}

/* report ref 1 to 10 kw */
function* getReportRefPostpaid1to10KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPostpaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_1_TO_10_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_1_TO_10_KW_FAIL, error});
    }
}
function* getReportRefPrepaid1to10KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_1_TO_10_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_1_TO_10_KW_FAIL, error});
    }
}

/* report ref 11 to 50 kw */
function* getReportRefPostpaid11to50KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPostpaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_11_TO_50_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_POSTPAID_11_TO_50_KW_FAIL, error});
    }
}
/* report ref greater than 50 kw not sponsor */
function* getReportRefPostpaidGreaterThan50KwNotSponsorSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidGreaterThan50KwNotSponsorApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR_FAIL, error});
    }
}

/* report ref greater than 50 kw sponsor */
function* getReportRefPostpaidGreaterThan50KwSponsorSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidGreaterThan50KwSponsorApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR_FAIL, error});
    }
}
/* report ref greater than 50 kw residential */
function* getReportRefPostpaidGreaterThan50KwResidentialSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidGreaterThan50KwResidentialApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL_FAIL, error});
    }
}

function* getReportRefPrepaid11to50KwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportRefPrepaidKwApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_11_TO_50_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_REPORT_REF_PREPAID_11_TO_50_KW_FAIL, error});
    }
}
function* getHistory12MonthsSaga(action){
  const { error, response }= yield call(REPORT_REF_API.getHistory12MonthsApi, action);
    if(response){
        yield put({ type: REPORT_REF.GET_HISTORY_12_MONTHS_SUCCESS, response});
    }else{
        yield put({ type: REPORT_REF.GET_HISTORY_12_MONTHS_FAIL, error});
    }
}

function* getHistory12MonthsBusinessSaga(action){
    const { error, response }= yield call(REPORT_REF_API.getHistory12MonthsApi, action);
    if(response){
        yield put({ type: REPORT_REF.GET_HISTORY_12_MONTHS_SUCCESS, response});
    }else{
        yield put({ type: REPORT_REF.GET_HISTORY_12_MONTHS_FAIL, error});
    }
}

function* getAdditionSubsidiesSaga(action){
    const { error, response }= yield call(REPORT_REF_API.getAdditionSubsidiesApi, action);
    if(response){
        yield put({ type: REPORT_REF.GET_ADDITION_SUBSIDIES_SUCCESS, response});
    }else{
        yield put({ type: REPORT_REF.GET_ADDITION_SUBSIDIES_FAIL, error});
    }
}

function* getAdditionSubsidiesBusinessSaga(action){
    const { error, response }= yield call(REPORT_REF_API.getAdditionSubsidiesApi, action);
    if(response){
        yield put({ type: REPORT_REF.GET_ADDITION_SUBSIDIES_BUSINESS_SUCCESS, response});
    }else{
        yield put({ type: REPORT_REF.GET_ADDITION_SUBSIDIES_BUSINESS_FAIL, error});
    }
}

export default function* reportRefSaga() {
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_POSTPAID_0_KW, getReportRefPostpaid0KwSaga);
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_0_KW, getReportRefPrepaid0KwSaga);

    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_POSTPAID_1_TO_10_KW, getReportRefPostpaid1to10KwSaga);
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_1_TO_10_KW, getReportRefPrepaid1to10KwSaga);

    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_POSTPAID_11_TO_50_KW, getReportRefPostpaid11to50KwSaga);
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_11_TO_50_KW, getReportRefPrepaid11to50KwSaga);

    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_NOT_SPONSOR, getReportRefPostpaidGreaterThan50KwNotSponsorSaga);
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_SPONSOR, getReportRefPostpaidGreaterThan50KwSponsorSaga);
    yield fork(takeEvery, REPORT_REF.GET_REPORT_REF_PREPAID_GREATER_THAN_50_KW_RESIDENTIAL, getReportRefPostpaidGreaterThan50KwResidentialSaga);
    yield fork(takeEvery, REPORT_REF.GET_HISTORY_12_MONTHS, getHistory12MonthsSaga);
    yield fork(takeEvery, REPORT_REF.GET_HISTORY_12_MONTHS_BUSINESS, getHistory12MonthsBusinessSaga);

    yield fork(takeEvery, REPORT_REF.GET_ADDITION_SUBSIDIES, getAdditionSubsidiesSaga);
    yield fork(takeEvery, REPORT_REF.GET_ADDITION_SUBSIDIES_BUSINESS, getAdditionSubsidiesBusinessSaga);
}


