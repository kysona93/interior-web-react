import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as ACTION from './../../actions/report/icCardDailySale';
import * as API from './../../services/report/icCardDailySale';

function* getICCardDailySalesByCusIdSaga(action){
    const { response, error } = yield call(API.getICCardDailySalesByCusIdApi, action);
    if(response){
        yield put({type: ACTION.GET_IC_CARD_DAILY_SALES_BY_CUS_ID_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_IC_CARD_DAILY_SALES_BY_CUS_ID_FAIL, error});
    }
}

function* getICCardDailySalesFilterSaga(action){
    const { response, error } = yield call(API.getICCardDailySalesFilterApi, action);
    if(response){
        yield put({type: ACTION.GET_IC_CARD_DAILY_SALES_FILTER_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_IC_CARD_DAILY_SALES_FILTER_FAIL, error});
    }
}

function* getPrepaidPowerReportsSaga(action){
    const { response, error } = yield call(API.getPrepaidPowerReportsApi, action);
    if(response){
        yield put({type: ACTION.GET_PREPAID_POWER_REPORTS_SUCCESS, response});
    }else{
        yield put({type: ACTION.GET_PREPAID_POWER_REPORTS_FAIL, error});
    }
}

export default function* icCardDailySaleSaga() {
    yield fork(takeEvery, ACTION.GET_IC_CARD_DAILY_SALES_BY_CUS_ID, getICCardDailySalesByCusIdSaga);
    yield fork(takeEvery, ACTION.GET_IC_CARD_DAILY_SALES_FILTER, getICCardDailySalesFilterSaga);
    yield fork(takeEvery, ACTION.GET_PREPAID_POWER_REPORTS, getPrepaidPowerReportsSaga);
}


