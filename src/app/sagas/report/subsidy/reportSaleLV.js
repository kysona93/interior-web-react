import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as REPORT_REF from './../../../actions/report/subsidy/reportSaleLV';
import * as REPORT_REF_API from './../../../services/report/subsidy/reportSaleLV';
//import { getSubsidyPrepaidAmountSponserApi, getSubsidyPrepaidAmountApi } from "./../../../services/report/subsidy/reportSaleLV";

/* get Amount Subsidy Sponser page 03 */
function* getSubsidyPrepaidAmountSponserSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getSubsidyPrepaidAmountSponserApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER_FAIL, error});
    }
}

function* getSubsidyPrepaidAmountSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getSubsidyPrepaidAmountApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_FAIL, error});
    }
}
/* report subsidy page 2 */
function* getReportSubsidySaleLV0to2000kwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleLVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_2000_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_2000_KW_FAIL, error});
    }
}

function* getReportSubsidySaleLV0to10kwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleLVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_10_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_10_KW_FAIL, error});
    }
}

function* getReportSubsidySaleLV11to50kwSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleLVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_11_TO_50_KW_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_11_TO_50_KW_FAIL, error});
    }
}

function* getReportSubsidySaleLV51kwUpSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleLVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_51_KW_UP_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_51_KW_UP_FAIL, error});
    }
}

export default function* reportSubsidySaleLVSaga() {
    yield fork(takeEvery, REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID_SPONSER, getSubsidyPrepaidAmountSponserSaga);
    yield fork(takeEvery, REPORT_REF.GET_CALCULATE_SUBSIDY_AMOUNT_PREPAID, getSubsidyPrepaidAmountSaga);

    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_2000_KW, getReportSubsidySaleLV0to2000kwSaga);
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_LV_0_TO_10_KW, getReportSubsidySaleLV0to10kwSaga);
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_LV_11_TO_50_KW, getReportSubsidySaleLV11to50kwSaga);
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_LV_51_KW_UP, getReportSubsidySaleLV51kwUpSaga);
}


