import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as REPORT_REF from './../../../actions/report/subsidy/reportSaleMV';
import * as REPORT_REF_API from './../../../services/report/subsidy/reportSaleMV';

function* getReportSubsidySaleMVLicenseSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleMVLicenseApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_MV_LICENSE_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_MV_LICENSE_FAIL, error});
    }
}

function* getReportSubsidySaleMVSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleMVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_MV_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_MV_FAIL, error});
    }
}

function* getReportSubsidySaleLVKW2001UPSaga(action){
    const { response, error } = yield call(REPORT_REF_API.getReportSubsidySaleLVApi, action);
    if(response){
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_KW_2001_UP_SUCCESS, response});
    }else{
        yield put({type: REPORT_REF.GET_SUBSIDY_SALE_LV_KW_2001_UP_FAIL, error});
    }
}


export default function* reportSubsidySaleMVSaga() {
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_MV_LICENSE, getReportSubsidySaleMVLicenseSaga);
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_MV, getReportSubsidySaleMVSaga);
    yield fork(takeEvery, REPORT_REF.GET_SUBSIDY_SALE_LV_KW_2001_UP, getReportSubsidySaleLVKW2001UPSaga);
}


