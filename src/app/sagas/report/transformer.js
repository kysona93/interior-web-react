import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as REPORT_TYPE from './../../actions/report/transformer';
import * as REPORT_API from './../../services/report/transformer';

function* getTransformerReportsSaga(action){
    const { response, error } = yield call(REPORT_API.getTransformerReportsApi(), action);
    if(response){
        yield put({type: REPORT_TYPE.GET_TRANSFORMER_REPORTS_SUCCESS, response});
    } else{
        yield put({type: REPORT_TYPE.GET_TRANSFORMER_REPORTS_FAIL, error});
    }
}

function* getPoleReportsSaga(action){
    const { response, error } = yield call(REPORT_API.getPoleReportsApi(), action);
    if(response){
        yield put({type: REPORT_TYPE.GET_POLE_REPORTS_SUCCESS, response});
    } else{
        yield put({type: REPORT_TYPE.GET_POLE_REPORTS_FAIL, error});
    }
}

export default function* transformerReportSaga() {
    yield fork(takeEvery, REPORT_TYPE.GET_TRANSFORMER_REPORTS, getTransformerReportsSaga);
    yield fork(takeEvery, REPORT_TYPE.GET_POLE_REPORTS, getPoleReportsSaga);
}


