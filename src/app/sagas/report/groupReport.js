import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_GROUP_REPORT_SUCCESS,
    ADD_GROUP_REPORT_FAIL,
    ADD_GROUP_REPORT,
    GET_ALL_GROUP_REPORTS_SUCCESS,
    GET_ALL_GROUP_REPORTS_FAIL,
    GET_ALL_GROUP_REPORTS,
    UPDATE_GROUP_REPORT_FAIL,
    UPDATE_GROUP_REPORT_SUCCESS,
    UPDATE_GROUP_REPORT,
    DELETE_GROUP_REPORT,
    DELETE_GROUP_REPORT_FAIL,
    DELETE_GROUP_REPORT_SUCCESS
} from './../../actions/report/groupReport';
import {
    addGroupReportApi,
    listAllGroupReportsApi,
    updateGroupReportApi,
    deleteGroupReportApi
} from './../../services/report/groupReport';

function* addGroupReportSaga(action){
    const { response, error } = yield call(addGroupReportApi, action);
    if(response){
        yield put({type: ADD_GROUP_REPORT_SUCCESS, response});
    }else{
        yield put({type: ADD_GROUP_REPORT_FAIL, error});
    }
}

function* listAllGroupReportSaga(action){
    const { response, error } = yield call(listAllGroupReportsApi, action);
    if(response){
        yield put({type: GET_ALL_GROUP_REPORTS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_GROUP_REPORTS_FAIL, error});
    }
}

function* updateGroupReportSaga(action){
    const { response, error } = yield call(updateGroupReportApi, action);
    if(response){
        yield put({type: UPDATE_GROUP_REPORT_SUCCESS, response});
    }else{
        yield put({type: UPDATE_GROUP_REPORT_FAIL, error});
    }
}

function* deleteGroupReportSaga(action){
    const { response, error } = yield call(deleteGroupReportApi, action);
    if(response){
        yield put({type: DELETE_GROUP_REPORT_SUCCESS, response});
    }else{
        yield put({type: DELETE_GROUP_REPORT_FAIL, error});
    }
}

export default function* groupReportSaga() {
    yield fork(takeEvery, ADD_GROUP_REPORT, addGroupReportSaga);
    yield fork(takeEvery, GET_ALL_GROUP_REPORTS, listAllGroupReportSaga);
    yield fork(takeEvery, UPDATE_GROUP_REPORT, updateGroupReportSaga);
    yield fork(takeEvery, DELETE_GROUP_REPORT, deleteGroupReportSaga);
}


