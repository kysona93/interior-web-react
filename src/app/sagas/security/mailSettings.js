import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as MAIL_SETTINGS_ACTION from './../../actions/security/mailSettings';
import * as MAIL_SETTINGS_API from './../../services/security/mailSettings';

function* getAllMailSettingsSaga(action){
    const { response, error } = yield call(MAIL_SETTINGS_API.getAllMailSettingsApi, action);
    if(response){
        yield put({type: MAIL_SETTINGS_ACTION.GET_ALL_MAIL_SETTINGS_SUCCESS, response});
    }else{
        yield put({type: MAIL_SETTINGS_ACTION.GET_ALL_MAIL_SETTINGS_FAIL, error});
    }
}

function* addMailSettingsSaga(action){
    const { response, error } = yield call(MAIL_SETTINGS_API.addMailSettingsApi, action);
    if(response){
        yield put({type: MAIL_SETTINGS_ACTION.ADD_MAIL_SETTINGS_SUCCESS, response});
    }else{
        yield put({type: MAIL_SETTINGS_ACTION.ADD_MAIL_SETTINGS_FAIL, error});
    }
}

function* updateMailSettingsSaga(action){
    const { response, error } = yield call(MAIL_SETTINGS_API.updateMailSettingsApi, action);
    if(response){
        yield put({type: MAIL_SETTINGS_ACTION.UPDATE_MAIL_SETTINGS_SUCCESS, response});
    }else{
        yield put({type: MAIL_SETTINGS_ACTION.UPDATE_MAIL_SETTINGS_FAIL, error});
    }
}

function* deleteMailSettingsSaga(action){
    const { response, error } = yield call(MAIL_SETTINGS_API.deleteMailSettingApi, action);
    if(response){
        yield put({type: MAIL_SETTINGS_ACTION.DELETE_MAIL_SETTINGS_SUCCESS, response});
    }else{
        yield put({type: MAIL_SETTINGS_ACTION.DELETE_MAIL_SETTINGS_FAIL, error});
    }
}

export default function* mailSettingsSaga() {
    yield fork(takeEvery, MAIL_SETTINGS_ACTION.GET_ALL_MAIL_SETTINGS, getAllMailSettingsSaga);
    yield fork(takeEvery, MAIL_SETTINGS_ACTION.ADD_MAIL_SETTINGS, addMailSettingsSaga);
    yield fork(takeEvery, MAIL_SETTINGS_ACTION.UPDATE_MAIL_SETTINGS, updateMailSettingsSaga);
    yield fork(takeEvery, MAIL_SETTINGS_ACTION.DELETE_MAIL_SETTINGS, deleteMailSettingsSaga);
}


