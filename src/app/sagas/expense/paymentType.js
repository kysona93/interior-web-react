import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as PAYMENT_TYPE from '../actions/location';
import * as PAYMENT_API from '../services/location';

function* getAllPaymentTypeSaga(action){
    const response = yield call(PAYMENT_API.getAllLocationsApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.GET_ALL_LOCATIONS_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.GET_ALL_LOCATIONS_FAIL, response});
    }
}

function* addPaymentTypeSaga(action){
    const response = yield call(PAYMENT_API.addLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.ADD_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.ADD_LOCATION_FAIL, response});
    }
}

function* updatePaymentTypeSaga(action){
    const response = yield call(PAYMENT_API.updateLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.UPDATE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.UPDATE_LOCATION_FAIL, response});
    }
}

function* deletePaymentTypeSaga(action){
    const response = yield call(PAYMENT_API.deleteLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.DELETE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.DELETE_LOCATION_FAIL, response});
    }
}

export default function* PaymentTypeSaga() {
    yield fork(takeEvery, PAYMENT_TYPE.GET_ALL_LOCATIONS, getAllLocationsSaga);
    yield fork(takeEvery, PAYMENT_TYPE.ADD_LOCATION, addPaymentTypeSaga);
    yield fork(takeEvery, PAYMENT_TYPE.UPDATE_LOCATION, updatePaymentTypeSaga);
    yield fork(takeEvery, PAYMENT_TYPE.DELETE_LOCATION, deletePaymentTypeSaga);
}


