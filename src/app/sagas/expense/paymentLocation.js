import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as PAYMENT_TYPE from '../actions/location';
import * as PAYMENT_API from '../services/location';

function* getAllPaymentLocationSaga(action){
    const response = yield call(PAYMENT_API.getAllLocationsApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.GET_ALL_LOCATIONS_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.GET_ALL_LOCATIONS_FAIL, response});
    }
}

function* addPaymentLocationSaga(action){
    const response = yield call(PAYMENT_API.addLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.ADD_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.ADD_LOCATION_FAIL, response});
    }
}

function* updatePaymentLocationSaga(action){
    const response = yield call(PAYMENT_API.updateLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.UPDATE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.UPDATE_LOCATION_FAIL, response});
    }
}

function* deletePaymentLocationSaga(action){
    const response = yield call(PAYMENT_API.deleteLocationApi, action);
    if(!(response === undefined)){
        yield put({type: PAYMENT_TYPE.DELETE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: PAYMENT_TYPE.DELETE_LOCATION_FAIL, response});
    }
}

export default function* PaymentLocationSaga() {
    yield fork(takeEvery, PAYMENT_TYPE.GET_ALL_LOCATIONS, getAllLocationsSaga);
    yield fork(takeEvery, PAYMENT_TYPE.ADD_LOCATION, addPaymentLocationSaga);
    yield fork(takeEvery, PAYMENT_TYPE.UPDATE_LOCATION, updatePaymentLocationSaga);
    yield fork(takeEvery, PAYMENT_TYPE.DELETE_LOCATION, deletePaymentLocationSaga);
}


