import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_EXCHANGE_RATE,
    ADD_EXCHANGE_RATE_SUCCESS,
    ADD_EXCHANGE_RATE_FAIL,
    GET_ALL_EXCHANGE_RATE,
    GET_ALL_EXCHANGE_RATE_SUCCESS,
    GET_ALL_EXCHANGE_RATE_FAIL,
    GET_EXCHANGE_RATE,
    GET_EXCHANGE_RATE_SUCCESS,
    GET_EXCHANGE_RATE_FAIL,
    UPDATE_EXCHANGE_RATE,
    UPDATE_EXCHANGE_RATE_SUCCESS,
    UPDATE_EXCHANGE_RATE_FAIL,
    DELETE_EXCHANGE_RATE,
    DELETE_EXCHANGE_RATE_SUCCESS,
    DELETE_EXCHANGE_RATE_FAIL
} from './../../actions/setup/exchangeRate';
import {
    addExchangeRateApi,
    listAllExchangeRateApi,
    getExchangeRateApi,
    updateExchangeRateApi,
    deleteExchangeRateApi
} from './../../services/setup/exchangeRate';

/* ADD EXCHANGE RATE */
function* addExchangeRateSaga(action){
    const response = yield call(addExchangeRateApi, action);
    if(!(response === undefined)){
        yield put({type: ADD_EXCHANGE_RATE_SUCCESS, response});
    }else{
        yield put({type: ADD_EXCHANGE_RATE_FAIL, response});
    }
}

/* LIST ALL EXCHANGE RATE */
function* listAllExchangeRateSaga(action){
    const response = yield call(listAllExchangeRateApi, action);
    if(!(response === undefined)){
        yield put({type: GET_ALL_EXCHANGE_RATE_SUCCESS, response});
    }else {
        console.log("saga not work:");
        yield put({type: GET_ALL_EXCHANGE_RATE_FAIL, response});
    }
}

/* GET EXCHANGE RATE */
function* getExchangeRateSaga(action){
    const response = yield call(getExchangeRateApi, action);
    if(!(response === undefined)){
        yield put({type: GET_EXCHANGE_RATE_SUCCESS, response});
    }else{
        yield put({type: GET_EXCHANGE_RATE_FAIL, response});
    }
}

/* UPDATE EXCHANGE RATE */
function* updateExchangeRateSaga(action){
    const response = yield call(updateExchangeRateApi, action);
    if(!(response === undefined)){
        yield put({type: UPDATE_EXCHANGE_RATE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_EXCHANGE_RATE_FAIL, response});
    }
}

/* DELETE EXCHANGE RATE */
function* deleteExchangeRateSaga(action){
    const response = yield call(deleteExchangeRateApi, action);
    if(!(response === undefined)){
        yield put({type: DELETE_EXCHANGE_RATE_SUCCESS, response});
    }else{
        yield put({type: DELETE_EXCHANGE_RATE_FAIL, response});
    }
}

export default function* exchangeRateSaga() {
    yield fork(takeEvery, ADD_EXCHANGE_RATE, addExchangeRateSaga);
    yield fork(takeEvery, GET_ALL_EXCHANGE_RATE, listAllExchangeRateSaga);
    yield fork(takeEvery, GET_EXCHANGE_RATE, getExchangeRateSaga);
    yield fork(takeEvery, UPDATE_EXCHANGE_RATE, updateExchangeRateSaga);
    yield fork(takeEvery, DELETE_EXCHANGE_RATE, deleteExchangeRateSaga);
}