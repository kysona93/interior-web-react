import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as BREAKER_TYPE from '../../actions/setup/breaker';
import * as BREAKER_API from '../../services/setup/breaker';

function* getBreakersSaga(action){
    const {response, error} = yield call(BREAKER_API.getBreakersApi, action);
    if(response){
        yield put({type: BREAKER_TYPE.GET_BREAKERS_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE.GET_BREAKERS_FAIL, error});
    }
}

function* getAllBreakersSaga(){
    const {response, error} = yield call(BREAKER_API.getAllBreakersApi);
    if(response){
        yield put({type: BREAKER_TYPE.GET_ALL_BREAKERS_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE.GET_ALL_BREAKERS_FAIL, error});
    }
}

function* addBreakerSaga(action){
    const {response, error} = yield call(BREAKER_API.addBreakerApi, action);
    if(response){
        yield put({type: BREAKER_TYPE.ADD_BREAKER_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE.ADD_BREAKER_FAIL, error});
    }
}

function* updateBreakerSaga(action){
    const {response, error} = yield call(BREAKER_API.updateBreakerApi, action);
    if(response){
        yield put({type: BREAKER_TYPE.UPDATE_BREAKER_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE.UPDATE_BREAKER_FAIL, error});
    }
}

function* deleteBreakerSaga(action){
    const {response, error} = yield call(BREAKER_API.deleteBreakerApi, action);
    if(response){
        yield put({type: BREAKER_TYPE.DELETE_BREAKER_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE.DELETE_BREAKER_FAIL, error});
    }
}

export default function* breakerSaga() {
    yield fork(takeEvery, BREAKER_TYPE.GET_BREAKERS, getBreakersSaga);
    yield fork(takeEvery, BREAKER_TYPE.GET_ALL_BREAKERS, getAllBreakersSaga);
    yield fork(takeEvery, BREAKER_TYPE.ADD_BREAKER, addBreakerSaga);
    yield fork(takeEvery, BREAKER_TYPE.UPDATE_BREAKER, updateBreakerSaga);
    yield fork(takeEvery, BREAKER_TYPE.DELETE_BREAKER, deleteBreakerSaga);
}


