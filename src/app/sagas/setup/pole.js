import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as POLE_TYPE from '../../actions/setup/pole';
import * as POLE_API from '../../services/setup/pole';

function* getPolesSaga(action){
    const {response, error} = yield call(POLE_API.getPolesApi, action);
    if(response){
        yield put({type: POLE_TYPE.GET_POLES_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE.GET_POLES_FAIL, error});
    }
}

function* getAllPolesSaga(){
    const {response, error} = yield call(POLE_API.getAllPolesApi);
    if(response){
        yield put({type: POLE_TYPE.GET_ALL_POLES_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE.GET_ALL_POLES_FAIL, error});
    }
}

function* addPoleSaga(action){
    const {response, error} = yield call(POLE_API.addPoleApi, action);
    if(response){
        yield put({type: POLE_TYPE.ADD_POLE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE.ADD_POLE_FAIL, error});
    }
}

function* updatePoleSaga(action){
    const {response, error} = yield call(POLE_API.updatePoleApi, action);
    if(response){
        yield put({type: POLE_TYPE.UPDATE_POLE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE.UPDATE_POLE_FAIL, error});
    }
}

function* deletePoleSaga(action){
    const {response, error} = yield call(POLE_API.deletePoleApi, action);
    if(response){
        yield put({type: POLE_TYPE.DELETE_POLE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE.DELETE_POLE_FAIL, error});
    }
}

export default function* poleSaga() {
    yield fork(takeEvery, POLE_TYPE.GET_POLES, getPolesSaga);
    yield fork(takeEvery, POLE_TYPE.GET_ALL_POLES, getAllPolesSaga);
    yield fork(takeEvery, POLE_TYPE.ADD_POLE, addPoleSaga);
    yield fork(takeEvery, POLE_TYPE.UPDATE_POLE, updatePoleSaga);
    yield fork(takeEvery, POLE_TYPE.DELETE_POLE, deletePoleSaga);
}


