import {takeEvery} from 'redux-saga';
import { call, put,fork } from 'redux-saga/effects';
import * as AMPERE_API from '../../services/setup/ampere';
import * as AMPERE from '../../actions/setup/ampere';

export function* addAmpereSaga(action){
    const {response, error} = yield call(AMPERE_API.addAmpereApi, action);
    if(response){
        yield put({type: AMPERE.ADD_AMPERE_SUCCESS, response});
    }else{
        yield put({type: AMPERE.ADD_AMPERE_FAIL, error});
    }
}

export function* getAllAmperesSaga(action){
    const {response, error} = yield call(AMPERE_API.getAllAmperesApi, action);
    if(response){
        yield put({type: AMPERE.GET_ALL_AMPERES_SUCCESS, response});
    }else{
        yield put({type: AMPERE.GET_ALL_AMPERES_FAIL, error});
    }
}

export function* updateAmpereSaga(action){
    const {response, error} = yield call(AMPERE_API.updateAmpereApi, action);
    if(response){
        yield put({type: AMPERE.UPDATE_AMPERE_SUCCESS, response});
    }else{
        yield put({type: AMPERE.UPDATE_AMPERE_FAIL, error});
    }
}

export function* deleteAmpereSaga(action){
    const {response, error} = yield call(AMPERE_API.deleteAmpereApi, action);
    if(response){
        yield put({type: AMPERE.DELETE_AMPERE_SUCCESS, response});
    }else{
        yield put({type: AMPERE.DELETE_AMPERE_FAIL, error});
    }
}

export function* ampereSaga() {
    yield fork(takeEvery ,AMPERE.ADD_AMPERE,addAmpereSaga);
    yield fork(takeEvery ,AMPERE.GET_ALL_AMPERES, getAllAmperesSaga);
    yield fork(takeEvery ,AMPERE.UPDATE_AMPERE, updateAmpereSaga);
    yield fork(takeEvery ,AMPERE.DELETE_AMPERE, deleteAmpereSaga);
}