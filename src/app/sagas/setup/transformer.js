import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as TRANSFORMER_TYPE from '../../actions/setup/transformer';
import * as TRANSFORMER_API from '../../services/setup/transformer';

function* getAllTransformersSaga(action){
    const {response, error} = yield call(TRANSFORMER_API.getAllTransformersApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FAIL, error});
    }
}

function* addTransformerSaga(action){
    const {response, error} = yield call(TRANSFORMER_API.addTransformerApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE.ADD_TRANSFORMER_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE.ADD_TRANSFORMER_FAIL, error});
    }
}

function* updateTransformerSaga(action){
    const {response, error} = yield call(TRANSFORMER_API.updateTransformerApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE.UPDATE_TRANSFORMER_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE.UPDATE_TRANSFORMER_FAIL, error});
    }
}

function* deleteTransformerSaga(action){
    const {response, error} = yield call(TRANSFORMER_API.deleteTransformerApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE.DELETE_TRANSFORMER_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE.DELETE_TRANSFORMER_FAIL, error});
    }
}

function* getAllTransformerForKwhMonthlySaga(action){
    const {response, error} = yield call(TRANSFORMER_API.getAllTransformerForKwhMonthlyApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY_FAIL, error});
    }
}

export default function* transformerSaga() {
    yield fork(takeEvery, TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS, getAllTransformersSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE.ADD_TRANSFORMER, addTransformerSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE.UPDATE_TRANSFORMER, updateTransformerSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE.DELETE_TRANSFORMER, deleteTransformerSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE.GET_ALL_TRANSFORMERS_FOR_KWH_MONTHLY, getAllTransformerForKwhMonthlySaga);
}


