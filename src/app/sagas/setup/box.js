import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as BOX_TYPE from '../../actions/setup/box';
import * as BOX_API from '../../services/setup/box';

function* getBoxesSaga(action){
    const {response, error} = yield call(BOX_API.getBoxesApi, action);
    if(response){
        yield put({type: BOX_TYPE.GET_BOXES_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE.GET_BOXES_FAIL, error});
    }
}

function* getAllBoxesSaga(){
    const {response, error} = yield call(BOX_API.getAllBoxesApi);
    if(response){
        yield put({type: BOX_TYPE.GET_ALL_BOXES_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE.GET_ALL_BOXES_FAIL, error});
    }
}

function* addBoxSaga(action){
    const {response, error} = yield call(BOX_API.addBoxApi, action);
    if(response){
        yield put({type: BOX_TYPE.ADD_BOX_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE.ADD_BOX_FAIL, error});
    }
}

function* updateBoxSaga(action){
    const {response, error} = yield call(BOX_API.updateBoxApi, action);
    if(response){
        yield put({type: BOX_TYPE.UPDATE_BOX_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE.UPDATE_BOX_FAIL, error});
    }
}

function* deleteBoxSaga(action){
    const {response, error} = yield call(BOX_API.deleteBoxApi, action);
    if(response){
        yield put({type: BOX_TYPE.DELETE_BOX_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE.DELETE_BOX_FAIL, error});
    }
}

export default function* boxSaga() {
    yield fork(takeEvery, BOX_TYPE.GET_BOXES, getBoxesSaga);
    yield fork(takeEvery, BOX_TYPE.GET_ALL_BOXES, getAllBoxesSaga);
    yield fork(takeEvery, BOX_TYPE.ADD_BOX, addBoxSaga);
    yield fork(takeEvery, BOX_TYPE.UPDATE_BOX, updateBoxSaga);
    yield fork(takeEvery, BOX_TYPE.DELETE_BOX, deleteBoxSaga);
}


