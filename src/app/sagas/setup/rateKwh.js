import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as RATE_TYPE from './../../actions/setup/rateKwh';
import * as RATE_API from './../../services/setup/rateKwh';

function* addRatePerKhwSaga(action){
    const { response, error } = yield call(RATE_API.addRatePerKhwApi, action);
    if(response){
        yield put({type: RATE_TYPE.ADD_RATE_PER_KWH_SUCCESS, response});
    }else{
        yield put({type: RATE_TYPE.ADD_RATE_PER_KWH_FAIL, error});
    }
}

function* getAllRatePerKhwSaga(){
    const { response, error } = yield call(RATE_API.getAllRatePerKhwApi);
    if(response){
        yield put({type: RATE_TYPE.GET_ALL_RATE_PER_KWH_SUCCESS, response});
    }else{
        yield put({type: RATE_TYPE.GET_ALL_RATE_PER_KWH_FAIL, error});
    }
}

function* updateRatePerKhwSaga(action){
    const {response, error} = yield call(RATE_API.updateRatePerKhwApi, action);
    if(response){
        yield put({type: RATE_TYPE.UPDATE_RATE_PER_KWH_SUCCESS, response});
    }else{
        yield put({type: RATE_TYPE.UPDATE_RATE_PER_KWH_FAIL, error});
    }
}

function* deleteRatePerKhwSaga(action){
    const {response, error} = yield call(RATE_API.deleteRatePerKhwApi, action);
    if(response){
        yield put({type: RATE_TYPE.DELETE_RATE_PER_KWH_SUCCESS, response});
    }else{
        yield put({type: RATE_TYPE.DELETE_RATE_PER_KWH_FAIL, error});
    }
}

export default function* ratePerKhwSaga() {
    yield fork(takeEvery, RATE_TYPE.ADD_RATE_PER_KWH, addRatePerKhwSaga);
    yield fork(takeEvery, RATE_TYPE.GET_ALL_RATE_PER_KWH, getAllRatePerKhwSaga);
    yield fork(takeEvery, RATE_TYPE.UPDATE_RATE_PER_KWH, updateRatePerKhwSaga);
    yield fork(takeEvery, RATE_TYPE.DELETE_RATE_PER_KWH, deleteRatePerKhwSaga);
}