import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_RATE_RANGE,
    ADD_RATE_RANGE_FAIL,
    ADD_RATE_RANGE_SUCCESS,
    GET_ALL_RATE_RANGE,
    GET_ALL_RATE_RANGE_FAIL,
    GET_ALL_RATE_RANGE_SUCCESS,
    UPDATE_RATE_RANGE,
    UPDATE_RATE_RANGE_FAIL,
    UPDATE_RATE_RANGE_SUCCESS,
    DELETE_RATE_RANGE,
    DELETE_RATE_RANGE_FAIL,
    DELETE_RATE_RANGE_SUCCESS
} from './../../actions/setup/rateRange';
import {
    addRateRangeApi,
    getAllRateRangesApi,
    updateRateRangeApi,
    deleteRateRangeApi
} from './../../services/setup/rateRange';

export function* addRateRangeSaga(action){
    const { response, error } = yield call(addRateRangeApi, action);
    if(response){
        yield put({type: ADD_RATE_RANGE_SUCCESS, response});
    }else{
        yield put({type: ADD_RATE_RANGE_FAIL, error});
    }
}

export function* getAllRateRangesSaga(action){
    const { response, error } = yield call(getAllRateRangesApi, action);
    if(response){
        yield put({type: GET_ALL_RATE_RANGE_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_RATE_RANGE_FAIL, error});
    }
}

export function* updateRateRangeSaga(action){
    const { response, error } = yield call(updateRateRangeApi, action);
    if(response){
        yield put({type: UPDATE_RATE_RANGE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_RATE_RANGE_FAIL, error});
    }
}

export function* deleteRateRangeSaga(action){
    const { response, error } = yield call(deleteRateRangeApi, action);
    if(response){
        yield put({type: DELETE_RATE_RANGE_SUCCESS, response});
    }else{
        yield put({type: DELETE_RATE_RANGE_FAIL, error});
    }
}

export default function* rateRangesSaga() {
    yield fork(takeEvery, ADD_RATE_RANGE, addRateRangeSaga);
    yield fork(takeEvery, GET_ALL_RATE_RANGE, getAllRateRangesSaga);
    yield fork(takeEvery, UPDATE_RATE_RANGE, updateRateRangeSaga);
    yield fork(takeEvery, DELETE_RATE_RANGE, deleteRateRangeSaga);
}