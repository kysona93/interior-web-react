import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_SETTING,
    ADD_SETTING_SUCCESS,
    ADD_SETTING_FAIL,
    GET_ALL_SETTING,
    GET_ALL_SETTING_SUCCESS,
    GET_ALL_SETTING_FAIL,
    UPDATE_SETTING,
    UPDATE_SETTING_SUCCESS,
    UPDATE_SETTING_FAIL,
    DELETE_SETTING,
    DELETE_SETTING_SUCCESS,
    DELETE_SETTING_FAIL
} from './../../actions/setup/setting';
import {
    addSettingApi,
    listSettingApi,
    updateSettingApi,
    deleteSettingApi
} from './../../services/setup/setting';

/* ADD RATE */
function* addSettingSaga(action){
    const {response, error } = yield call(addSettingApi, action);
    if(response){
        yield put({type: ADD_SETTING_SUCCESS, response});
    }else{
        yield put({type: ADD_SETTING_FAIL, error});
    }
}

/* LIST ALL RATE */
function* listSettingSaga(action){
    const {response, error } = yield call(listSettingApi, action);
    if(response){
        yield put({type: GET_ALL_SETTING_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_SETTING_FAIL, error});
    }
}

/* UPDATE RATE */
function* updateSettingSaga(action){
    const {response, error } = yield call(updateSettingApi, action);
    if(response){
        yield put({type: UPDATE_SETTING_SUCCESS, response});
    }else{
        yield put({type: UPDATE_SETTING_FAIL, error});
    }
}

/* DELETE RATE */
function* deleteSettingSaga(action){
    const {response, error } = yield call(deleteSettingApi, action);
    if(response){
        yield put({type: DELETE_SETTING_SUCCESS, response});
    }else{
        yield put({type: DELETE_SETTING_FAIL, error});
    }
}

export default function* settingSaga() {
    yield fork(takeEvery, ADD_SETTING, addSettingSaga);
    yield fork(takeEvery, GET_ALL_SETTING, listSettingSaga);
    yield fork(takeEvery, UPDATE_SETTING, updateSettingSaga);
    yield fork(takeEvery, DELETE_SETTING, deleteSettingSaga);
}