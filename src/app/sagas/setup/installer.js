import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_INSTALLER,
    ADD_INSTALLER_SUCCESS,
    ADD_INSTALLER_FAIL,
    GET_ALL_INSTALLERS,
    GET_ALL_INSTALLERS_SUCCESS,
    GET_ALL_INSTALLERS_FAIL,
    UPDATE_INSTALLER,
    UPDATE_INSTALLER_SUCCESS,
    UPDATE_INSTALLER_FAIL,
    DELETE_INSTALLER,
    DELETE_INSTALLER_SUCCESS,
    DELETE_INSTALLER_FAIL
} from './../../actions/setup/installer';
import {
    addInstallerApi,
    listAllInstallersApi,
    getInstallerApi,
    updateInstallersApi,
    deleteInstallerApi
} from './../../services/setup/installer';

/* ADD INSTALLER */
export function* addInstallerSaga(action){
    const response = yield call(addInstallerApi, action);
    if(!(response == undefined)){
        yield put({type: ADD_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: ADD_INSTALLER_FAIL, response});
    }
}

/* LIST ALL INSTALLERS */
export function* listAllInstallersSaga(action){
    const response = yield call(listAllInstallersApi, action);
    if(!(response == undefined)){
        yield put({type: GET_ALL_INSTALLERS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_INSTALLERS_FAIL, response});
    }
}

/* UPDATE INSTALLER */
export function* updateInstallerSaga(action){
    const response = yield call(updateInstallersApi, action);
    if(!(response == undefined)){
        yield put({type: UPDATE_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: UPDATE_INSTALLER_FAIL, response});
    }
}

/* DELETE INSTALLER */
export function* deleteInstallerSaga(action){
    const response = yield call(deleteInstallerApi, action);
    if(!(response == undefined)){
        yield put({type: DELETE_INSTALLER_SUCCESS, response});
    }else{
        yield put({type: DELETE_INSTALLER_FAIL, response});
    }
}

export default function* installerSaga() {
    yield fork(takeEvery, ADD_INSTALLER, addInstallerSaga);
    yield fork(takeEvery, GET_ALL_INSTALLERS, listAllInstallersSaga);
    yield fork(takeEvery, UPDATE_INSTALLER, updateInstallerSaga);
    yield fork(takeEvery, DELETE_INSTALLER, deleteInstallerSaga);
}