import { call, put } from 'redux-saga/effects';
import {
    GET_ALL_JOBS_SUCCESS,
    GET_ALL_JOBS_FAIL
} from './../../actions/setup/job';
import {
    listAllJobsApi
} from './../../services/setup/job';

/* LIST ALL JOBS */
export function* listAllJobsSaga(action){
    const { response, error } = yield call(listAllJobsApi, action);
    if(response){
        yield put({type: GET_ALL_JOBS_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_JOBS_FAIL, error});
    }
}