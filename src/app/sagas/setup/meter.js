import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as METER_TYPE from '../../actions/setup/meter';
import * as METER_API from '../../services/setup/meter';
import { reuseMeterPostPaidAction } from "../../actions/setup/meter";

function* getMetersSaga(action){
    const { response, error } = yield call(METER_API.getMetersApi, action);
    if(response){
        yield put({type: METER_TYPE.GET_METERS_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.GET_METERS_FAIL, error});
    }
}

function* getAllMetersSaga(){
    const { response, error } = yield call(METER_API.getMetersApi);
    if(response){
        yield put({type: METER_TYPE.GET_ALL_METERS_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.GET_ALL_METERS_FAIL, error});
    }
}

function* addMeterSaga(action){
    const { response, error } = yield call(METER_API.addMeterApi, action);
    if(response){
        yield put({type: METER_TYPE.ADD_METER_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.ADD_METER_FAIL, error});
    }
}

function* updateMeterSaga(action){
    const { response, error } = yield call(METER_API.updateMeterApi, action);
    if(response){
        yield put({type: METER_TYPE.UPDATE_METER_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.UPDATE_METER_FAIL, error});
    }
}

function* deleteMeterSaga(action){
    const { response, error } = yield call(METER_API.deleteMeterApi, action);
    if(response){
        yield put({type: METER_TYPE.DELETE_METER_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.DELETE_METER_FAIL, error});
    }
}

function* getMeterByMeterSerialSaga(action){
    const { response, error } = yield call(METER_API.getMeterByMeterSerialApi, action);
    if(response){
        yield put({type: METER_TYPE.GET_METER_BY_METER_SERIAL_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.GET_METER_BY_METER_SERIAL_FAIL, error});
    }
}

function* reuseMeterPostPaidSaga(action){
    const { response, error } = yield call(METER_API.reuseMeterPostPaidApi, action);
    if(response){
        yield put({type: METER_TYPE.REUSE_METER_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE.REUSE_METER_FAIL, error});
    }
}

export default function* meterSaga() {
    yield fork(takeEvery, METER_TYPE.GET_METERS, getMetersSaga);
    yield fork(takeEvery, METER_TYPE.GET_ALL_METERS, getAllMetersSaga);
    yield fork(takeEvery, METER_TYPE.ADD_METER, addMeterSaga);
    yield fork(takeEvery, METER_TYPE.UPDATE_METER, updateMeterSaga);
    yield fork(takeEvery, METER_TYPE.DELETE_METER, deleteMeterSaga);
    yield fork(takeEvery, METER_TYPE.GET_METER_BY_METER_SERIAL, getMeterByMeterSerialSaga);
    yield fork(takeEvery, METER_TYPE.REUSE_METER, reuseMeterPostPaidSaga);
}


