import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as BANK_TYPE from './../../actions/setup/bank';
import * as BANK_API from './../../services/setup/bank';

function* addBankSaga(action){
    const { response, error } = yield call(BANK_API.addBankApi, action);
    if(response){
        yield put({type: BANK_TYPE.ADD_BANK_SUCCESS, response});
    }else{
        yield put({type: BANK_TYPE.ADD_BANK_FAIL, error});
    }
}

function* getAllBanksSaga(action){
    const { response, error } = yield call(BANK_API.getAllBanksApi, action);
    if(response){
        yield put({type: BANK_TYPE.GET_ALL_BANKS_SUCCESS, response});
    }else{
        yield put({type: BANK_TYPE.GET_ALL_BANKS_FAIL, error});
    }
}

function* updateBankSaga(action){
    const { response, error } = yield call(BANK_API.updateBankApi, action);
    if(response){
        yield put({type: BANK_TYPE.UPDATE_BANK_SUCCESS, response});
    }else{
        yield put({type: BANK_TYPE.UPDATE_BANK_FAIL, error});
    }
}

function* deleteBankSaga(action){
    const { response, error } = yield call(BANK_API.deleteBankApi, action);
    if(response){
        yield put({type: BANK_TYPE.DELETE_BANK_SUCCESS, response});
    }else{
        yield put({type: BANK_TYPE.DELETE_BANK_FAIL, error});
    }
}

export default function* banksSaga() {
    yield fork(takeEvery, BANK_TYPE.ADD_BANK, addBankSaga);
    yield fork(takeEvery, BANK_TYPE.GET_ALL_BANKS, getAllBanksSaga);
    yield fork(takeEvery, BANK_TYPE.UPDATE_BANK, updateBankSaga);
    yield fork(takeEvery, BANK_TYPE.DELETE_BANK, deleteBankSaga);
}