import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_METER_PHASE,
    ADD_METER_PHASE_SUCCESS,
    ADD_METER_PHASE_FAIL,
    GET_ALL_METER_PHASE,
    GET_ALL_METER_PHASE_SUCCESS,
    GET_ALL_METER_PHASE_FAIL,
    UPDATE_METER_PHASE,
    UPDATE_METER_PHASE_SUCCESS,
    UPDATE_METER_PHASE_FAIL,
    DELETE_METER_PHASE,
    DELETE_METER_PHASE_SUCCESS,
    DELETE_METER_PHASE_FAIL
} from './../../actions/setup/meterPhase';
import {
    addMeterPhaseApi,
    listAllMeterPhasesApi,
    updateMeterPhaseApi,
    deleteMeterPhaseApi
} from './../../services/setup/meterPhase';

/* ADD INSTALLER */
function* addMeterPhaseSaga(action){
    const response = yield call(addMeterPhaseApi, action);
    if(!(response == undefined)){
        yield put({type: ADD_METER_PHASE_SUCCESS, response});
    }else{
        yield put({type: ADD_METER_PHASE_FAIL, response});
    }
}

/* LIST ALL INSTALLERS */
function* listAllMeterPhasesSaga(action){
    const response = yield call(listAllMeterPhasesApi, action);
    if(!(response == undefined)){
        yield put({type: GET_ALL_METER_PHASE_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_METER_PHASE_FAIL, response});
    }
}

/* UPDATE INSTALLER */
function* updateMeterPhaseSaga(action){
    const response = yield call(updateMeterPhaseApi, action);
    if(!(response == undefined)){
        yield put({type: UPDATE_METER_PHASE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_METER_PHASE_FAIL, response});
    }
}

/* DELETE INSTALLER */
function* deleteMeterPhaseSaga(action){
    const response = yield call(deleteMeterPhaseApi, action);
    if(!(response == undefined)){
        yield put({type: DELETE_METER_PHASE_SUCCESS, response});
    }else{
        yield put({type: DELETE_METER_PHASE_FAIL, response});
    }
}

export default function* meterPhaseSaga() {
    yield fork(takeEvery, ADD_METER_PHASE, addMeterPhaseSaga);
    yield fork(takeEvery, GET_ALL_METER_PHASE, listAllMeterPhasesSaga);
    yield fork(takeEvery, UPDATE_METER_PHASE, updateMeterPhaseSaga);
    yield fork(takeEvery, DELETE_METER_PHASE, deleteMeterPhaseSaga);
}