import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as BOX_TYPE_TYPE from './../../../actions/setup/type/boxType';
import * as BOX_TYPE_API from './../../../services/setup/type/boxType';

function* addBoxTypeSaga(action){
    const { response, error } = yield call(BOX_TYPE_API.addBoxTypeApi, action);
    if(response){
        yield put({type: BOX_TYPE_TYPE.ADD_BOX_TYPE_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE_TYPE.ADD_BOX_TYPE_FAIL, error});
    }
}

function* getAllBoxTypesSaga(action){
    const response = yield call(BOX_TYPE_API.getAllBoxTypesApi, action);
    if(!(response == undefined)){
        yield put({type: BOX_TYPE_TYPE.GET_ALL_BOX_TYPES_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE_TYPE.GET_ALL_BOX_TYPES_FAIL, response});
    }
}

function* updateBoxTypeSaga(action){
    const { response, error } = yield call(BOX_TYPE_API.updateBoxTypeApi, action);
    if(response){
        yield put({type: BOX_TYPE_TYPE.UPDATE_BOX_TYPE_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE_TYPE.UPDATE_BOX_TYPE_FAIL, error});
    }
}

function* deleteBoxTypeSaga(action){
    const { response, error } = yield call(BOX_TYPE_API.deleteBoxTypeApi, action);
    if(response){
        yield put({type: BOX_TYPE_TYPE.DELETE_BOX_TYPE_SUCCESS, response});
    }else{
        yield put({type: BOX_TYPE_TYPE.DELETE_BOX_TYPE_FAIL, error});
    }
}

export default function* boxTypeSaga() {
    yield fork(takeEvery, BOX_TYPE_TYPE.ADD_BOX_TYPE, addBoxTypeSaga);
    yield fork(takeEvery, BOX_TYPE_TYPE.GET_ALL_BOX_TYPES, getAllBoxTypesSaga);
    yield fork(takeEvery, BOX_TYPE_TYPE.UPDATE_BOX_TYPE, updateBoxTypeSaga);
    yield fork(takeEvery, BOX_TYPE_TYPE.DELETE_BOX_TYPE, deleteBoxTypeSaga);
}