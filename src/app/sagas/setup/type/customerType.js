import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as CUSTOMER_TYPE_TYPE from '../../../actions/setup/type/customerType';
import * as CUSTOMER_TYPE_API from '../../../services/setup/type/customerType';

function* addCustomerTypeSaga(action) {
    const { response, error } = yield call(CUSTOMER_TYPE_API.addCustomerTypeApi, action);
    if(response){
        yield put({type: CUSTOMER_TYPE_TYPE.ADD_CUSTOMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE_TYPE.ADD_CUSTOMER_TYPE_FAIL, error});
    }
}

function* updateCustomerTypeSaga(action) {
    const { response, error } = yield call(CUSTOMER_TYPE_API.updateCustomerTypeApi, action);
    if(response) {
        yield put({type: CUSTOMER_TYPE_TYPE.UPDATE_CUSTOMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE_TYPE.UPDATE_CUSTOMER_TYPE_FAIL, error});
    }
}

function* deleteCustomerTypeSaga(action) {
    const { response, error } = yield call(CUSTOMER_TYPE_API.deleteCustomerTypeApi, action);
    if(response) {
        yield put({type: CUSTOMER_TYPE_TYPE.DELETE_CUSTOMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE_TYPE.DELETE_CUSTOMER_TYPE_FAIL, error});
    }
}

function* getAllCustomerTypesSaga() {
    const { response, error } = yield call(CUSTOMER_TYPE_API.getAllCustomerTypesApi);
    if(response) {
        yield put({type: CUSTOMER_TYPE_TYPE.GET_ALL_CUSTOMER_TYPES_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_TYPE_TYPE.GET_ALL_CUSTOMER_TYPES_FAIL, error});
    }
}

export default function* customerTypeSaga() {
    yield fork(takeEvery, CUSTOMER_TYPE_TYPE.ADD_CUSTOMER_TYPE, addCustomerTypeSaga);
    yield fork(takeEvery, CUSTOMER_TYPE_TYPE.GET_ALL_CUSTOMER_TYPES, getAllCustomerTypesSaga);
    yield fork(takeEvery, CUSTOMER_TYPE_TYPE.UPDATE_CUSTOMER_TYPE, updateCustomerTypeSaga);
    yield fork(takeEvery, CUSTOMER_TYPE_TYPE.DELETE_CUSTOMER_TYPE, deleteCustomerTypeSaga);
}