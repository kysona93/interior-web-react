import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as POLE_TYPE_TYPE from './../../../actions/setup/type/poleType';
import * as POLE_TYPE_API from './../../../services/setup/type/poleType';

function* addPoleTypeSaga(action){
    const {response, error} = yield call(POLE_TYPE_API.addPoleTypeApi, action);
    if(response){
        yield put({type: POLE_TYPE_TYPE.ADD_POLE_TYPE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE_TYPE.ADD_POLE_TYPE_FAIL, error});
    }
}

function* listAllPoleTypeSaga(action){
    const response = yield call(POLE_TYPE_API.getAllPoleTypesApi, action);
    if(!(response == undefined)){
        yield put({type: POLE_TYPE_TYPE.GET_ALL_POLE_TYPES_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE_TYPE.GET_ALL_POLE_TYPES_FAIL, response});
    }
}

function* updatePoleTypeSaga(action){
    const {response, error} = yield call(POLE_TYPE_API.updatePoleTypeApi, action);
    if(response){
        yield put({type: POLE_TYPE_TYPE.UPDATE_POLE_TYPE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE_TYPE.UPDATE_POLE_TYPE_FAIL, error});
    }
}

function* deletePoleTypeSaga(action){
    const {response, error} = yield call(POLE_TYPE_API.deletePoleTypeApi, action);
    if(response){
        yield put({type: POLE_TYPE_TYPE.DELETE_POLE_TYPE_SUCCESS, response});
    }else{
        yield put({type: POLE_TYPE_TYPE.DELETE_POLE_TYPE_FAIL, error});
    }
}

export default function* poleTypeSaga() {
    yield fork(takeEvery, POLE_TYPE_TYPE.ADD_POLE_TYPE, addPoleTypeSaga);
    yield fork(takeEvery, POLE_TYPE_TYPE.GET_ALL_POLE_TYPES, listAllPoleTypeSaga);
    yield fork(takeEvery, POLE_TYPE_TYPE.UPDATE_POLE_TYPE, updatePoleTypeSaga);
    yield fork(takeEvery, POLE_TYPE_TYPE.DELETE_POLE_TYPE, deletePoleTypeSaga);
}