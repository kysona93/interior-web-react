import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as VOLTAGE_TYPE from './../../../actions/setup/type/voltageType';
import * as VOLTAGE_API from './../../../services/setup/type/voltageType';

function* getAllVoltagesSaga(action){
    const {response, error} = yield call(VOLTAGE_API.getAllVoltagesApi, action);
    if(response){
        yield put({type: VOLTAGE_TYPE.GET_ALL_VOLTAGE_TYPES_SUCCESS, response});
    }else{
        yield put({type: VOLTAGE_TYPE.GET_ALL_VOLTAGE_TYPES_FAIL, error});
    }
}

function* addVoltageSaga(action){
    const {response, error} = yield call(VOLTAGE_API.addVoltageApi, action);
    if(response){
        yield put({type: VOLTAGE_TYPE.ADD_VOLTAGE_TYPE_SUCCESS, response});
    }else{
        yield put({type: VOLTAGE_TYPE.ADD_VOLTAGE_TYPE_FAIL, error});
    }
}

function* updateVoltageSaga(action){
    const {response, error} = yield call(VOLTAGE_API.updateVoltageApi, action);
    if(response){
        yield put({type: VOLTAGE_TYPE.UPDATE_VOLTAGE_TYPE_SUCCESS, response});
    }else{
        yield put({type: VOLTAGE_TYPE.UPDATE_VOLTAGE_TYPE_FAIL, error});
    }
}

function* deleteVoltageSaga(action){
    const {response, error} = yield call(VOLTAGE_API.deleteVoltageApi, action);
    if(response){
        yield put({type: VOLTAGE_TYPE.DELETE_VOLTAGE_TYPE_SUCCESS, response});
    }else{
        yield put({type: VOLTAGE_TYPE.DELETE_VOLTAGE_TYPE_FAIL, error});
    }
}

export default function* voltageSaga() {
    yield fork(takeEvery, VOLTAGE_TYPE.GET_ALL_VOLTAGE_TYPES, getAllVoltagesSaga);
    yield fork(takeEvery, VOLTAGE_TYPE.ADD_VOLTAGE_TYPE, addVoltageSaga);
    yield fork(takeEvery, VOLTAGE_TYPE.UPDATE_VOLTAGE_TYPE, updateVoltageSaga);
    yield fork(takeEvery, VOLTAGE_TYPE.DELETE_VOLTAGE_TYPE, deleteVoltageSaga);
}


