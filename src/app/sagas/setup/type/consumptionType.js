import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as CONSUMPTION_TYPE_TYPE from '../../../actions/setup/type/consumptionType';
import * as CONSUMPTION_TYPE_API from '../../../services/setup/type/consumptionType';

function* addConsumptionTypeSaga(action){
    const { response, error } = yield call(CONSUMPTION_TYPE_API.addConsumptionTypeApi, action);
    if(response){
        yield put({type: CONSUMPTION_TYPE_TYPE.ADD_CONSUMPTION_TYPE_SUCCESS, response});
    }else{
        yield put({type: CONSUMPTION_TYPE_TYPE.ADD_CONSUMPTION_TYPE_FAIL, error});
    }
}

function* getAllConsumptionTypesSaga(){
    const { response, error } = yield call(CONSUMPTION_TYPE_API.getAllConsumptionTypesApi);
    if(response){
        yield put({type: CONSUMPTION_TYPE_TYPE.GET_ALL_CONSUMPTION_TYPES_SUCCESS, response});
    }else{
        yield put({type: CONSUMPTION_TYPE_TYPE.GET_ALL_CONSUMPTION_TYPES_FAIL, error});
    }
}

function* updateConsumptionTypeSaga(action){
    const { response, error } = yield call(CONSUMPTION_TYPE_API.updateConsumptionTypeApi, action);
    if(response){
        yield put({type: CONSUMPTION_TYPE_TYPE.UPDATE_CONSUMPTION_TYPE_SUCCESS, response});
    }else{
        yield put({type: CONSUMPTION_TYPE_TYPE.UPDATE_CONSUMPTION_TYPE_FAIL, error});
    }
}

function* deleteConsumptionTypeSaga(action){
    const { response, error } = yield call(CONSUMPTION_TYPE_API.deleteConsumptionTypeApi, action);
    if(response){
        yield put({type: CONSUMPTION_TYPE_TYPE.DELETE_CONSUMPTION_TYPE_SUCCESS, response});
    }else{
        yield put({type: CONSUMPTION_TYPE_TYPE.DELETE_CONSUMPTION_TYPE_FAIL, error});
    }
}

export default function* consumptionTypeSaga() {
    yield fork(takeEvery, CONSUMPTION_TYPE_TYPE.ADD_CONSUMPTION_TYPE, addConsumptionTypeSaga);
    yield fork(takeEvery, CONSUMPTION_TYPE_TYPE.GET_ALL_CONSUMPTION_TYPES, getAllConsumptionTypesSaga);
    yield fork(takeEvery, CONSUMPTION_TYPE_TYPE.UPDATE_CONSUMPTION_TYPE, updateConsumptionTypeSaga);
    yield fork(takeEvery, CONSUMPTION_TYPE_TYPE.DELETE_CONSUMPTION_TYPE, deleteConsumptionTypeSaga);
}