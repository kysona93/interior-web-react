import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as CUSTOMER_GROUP from './../../../actions/setup/type/customerGroup';
import * as CUSTOMER_GROUP_API from '../../../services/setup/type/customerSaleType';

function* addCustomerGroupSaga(action){
    const { response, error } = yield call(CUSTOMER_GROUP_API.addCustomerGroupApi, action);
    if(response){
        yield put({type: CUSTOMER_GROUP.ADD_CUSTOMER_GROUP_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_GROUP.ADD_CUSTOMER_GROUP_FAIL, error});
    }
}

function* getAllCustomerGroupsSaga(action){
    const { response, error } = yield call(CUSTOMER_GROUP_API.getAllCustomerGroupsApi, action);
    if(response){
        yield put({type: CUSTOMER_GROUP.GET_ALL_CUSTOMER_GROUPS_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_GROUP.GET_ALL_CUSTOMER_GROUPS_FAIL, error});
    }
}

function* updateCustomerGroupSaga(action){
    const { response, error } = yield call(CUSTOMER_GROUP_API.updateCustomerGroupApi, action);
    if(response){
        yield put({type: CUSTOMER_GROUP.UPDATE_CUSTOMER_GROUP_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_GROUP.UPDATE_CUSTOMER_GROUP_FAIL, error});
    }
}

function* deleteCustomerGroupSaga(action){
    const { response, error } = yield call(CUSTOMER_GROUP_API.deleteCustomerGroupApi, action);
    if(response){
        yield put({type: CUSTOMER_GROUP.DELETE_CUSTOMER_GROUP_SUCCESS, response});
    }else{
        yield put({type: CUSTOMER_GROUP.DELETE_CUSTOMER_GROUP_FAIL, error});
    }
}

export default function* customerGroupSaga() {
    yield fork(takeEvery, CUSTOMER_GROUP.ADD_CUSTOMER_GROUP, addCustomerGroupSaga);
    yield fork(takeEvery, CUSTOMER_GROUP.GET_ALL_CUSTOMER_GROUPS, getAllCustomerGroupsSaga);
    yield fork(takeEvery, CUSTOMER_GROUP.UPDATE_CUSTOMER_GROUP, updateCustomerGroupSaga);
    yield fork(takeEvery, CUSTOMER_GROUP.DELETE_CUSTOMER_GROUP, deleteCustomerGroupSaga);
}