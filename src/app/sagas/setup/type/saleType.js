import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as SALE_TYPE from './../../../actions/setup/saleType';
import * as SALE_TYPE_API from './../../../services/setup/saleType';

/* ADD BOX TYPE */
function* addSaleTypeSaga(action){
    const response = yield call(SALE_TYPE_API.addSaleTypeApi, action);
    if(!(response == undefined)){
        yield put({type: SALE_TYPE.ADD_SALE_TYPE_SUCCESS, response});
    }else{
        yield put({type: SALE_TYPE.ADD_SALE_TYPE_FAIL, response});
    }
}

/* LIST ALL BOX TYPE */
function* getAllSaleTypeSaga(action){
    const response = yield call(SALE_TYPE_API.getAllSaleTypeApi, action);
    if(!(response == undefined)){
        yield put({type: SALE_TYPE.GET_ALL_SALE_TYPE_SUCCESS, response});
    }else{
        yield put({type: SALE_TYPE.GET_ALL_SALE_TYPE_FAIL, response});
    }
}

/* UPDATE BOX TYPE */
function* updateSaleTypeSaga(action){
    const response = yield call(SALE_TYPE_API.updateSaleTypeApi, action);
    if(!(response == undefined)){
        yield put({type: SALE_TYPE.UPDATE_SALE_TYPE_SUCCESS, response});
    }else{
        yield put({type: SALE_TYPE.UPDATE_SALE_TYPE_FAIL, response});
    }
}

/* DELETE BOX TYPE */
function* deleteSaleTypeSaga(action){
    const response = yield call(SALE_TYPE_API.deleteSaleTypeApi, action);
    if(!(response == undefined)){
        yield put({type: SALE_TYPE.DELETE_SALE_TYPE_SUCCESS, response});
    }else{
        yield put({type: SALE_TYPE.DELETE_SALE_TYPE_FAIL, response});
    }
}

export default function* saleTypeSaga() {
    yield fork(takeEvery, SALE_TYPE.ADD_SALE_TYPE, addSaleTypeSaga);
    yield fork(takeEvery, SALE_TYPE.GET_ALL_SALE_TYPE, getAllSaleTypeSaga);
    yield fork(takeEvery, SALE_TYPE.UPDATE_SALE_TYPE, updateSaleTypeSaga);
    yield fork(takeEvery, SALE_TYPE.DELETE_SALE_TYPE, deleteSaleTypeSaga);
}