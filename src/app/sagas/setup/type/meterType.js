import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as METER_TYPE_TYPE from './../../../actions/setup/type/meterType';
import * as METER_TYPE_API from './../../../services/setup/type/meterType';

function* addMeterTypeSaga(action){
    const response = yield call(METER_TYPE_API.addMeterTypeApi, action);
    if(!(response == undefined)){
        yield put({type: METER_TYPE_TYPE.ADD_METER_TYPE_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE_TYPE.ADD_METER_TYPE_FAIL, response});
    }
}

function* getAllMeterTypesSaga(action){
    const response = yield call(METER_TYPE_API.getAllMeterTypesApi, action);
    if(!(response == undefined)){
        yield put({type: METER_TYPE_TYPE.GET_ALL_METER_TYPES_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE_TYPE.GET_ALL_METER_TYPES_FAIL, response});
    }
}

function* updateMeterTypeSaga(action){
    const response = yield call(METER_TYPE_API.updateMeterTypeApi, action);
    if(!(response == undefined)){
        yield put({type: METER_TYPE_TYPE.UPDATE_METER_TYPE_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE_TYPE.UPDATE_METER_TYPE_FAIL, response});
    }
}

function* deleteMeterTypeSaga(action){
    const response = yield call(METER_TYPE_API.deleteMeterTypeApi, action);
    if(!(response == undefined)){
        yield put({type: METER_TYPE_TYPE.DELETE_METER_TYPE_SUCCESS, response});
    }else{
        yield put({type: METER_TYPE_TYPE.DELETE_METER_TYPE_FAIL, response});
    }
}

export default function* meterTypeSaga() {
    yield fork(takeEvery, METER_TYPE_TYPE.ADD_METER_TYPE, addMeterTypeSaga);
    yield fork(takeEvery, METER_TYPE_TYPE.GET_ALL_METER_TYPES, getAllMeterTypesSaga);
    yield fork(takeEvery, METER_TYPE_TYPE.UPDATE_METER_TYPE, updateMeterTypeSaga);
    yield fork(takeEvery, METER_TYPE_TYPE.DELETE_METER_TYPE, deleteMeterTypeSaga);
}