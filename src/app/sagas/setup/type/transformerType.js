import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as TRANSFORMER_TYPE_TYPE from './../../../actions/setup/type/transformerType';
import * as TRANSFORMER_TYPE_API from './../../../services/setup/type/transformerType';

/* ADD TRANSFORMER TYPE */
function* addTransformerTypeSaga(action){
    const { response, error } = yield call(TRANSFORMER_TYPE_API.addTransformerTypeApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE_TYPE.ADD_TRANSFORMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE_TYPE.ADD_TRANSFORMER_TYPE_FAIL, error});
    }
}

/* LIST ALL TRANSFORMER TYPE */
function* getAllTransformerTypesSaga(action){
    const response = yield call(TRANSFORMER_TYPE_API.getAllTransformerTypesApi, action);
    if(!(response === undefined)){
        yield put({type: TRANSFORMER_TYPE_TYPE.GET_ALL_TRANSFORMER_TYPES_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE_TYPE.GET_ALL_TRANSFORMER_TYPES_FAIL, response});
    }
}

/* UPDATE TRANSFORMER TYPE */
function* updateTransformerTypeSaga(action){
    const { response, error } = yield call(TRANSFORMER_TYPE_API.updateTransformerTypeApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE_TYPE.UPDATE_TRANSFORMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE_TYPE.UPDATE_TRANSFORMER_TYPE_FAIL, error});
    }
}

/* DELETE TRANSFORMER TYPE */
function* deleteTransformerTypeSaga(action){
    const { response, error } = yield call(TRANSFORMER_TYPE_API.deleteTransformerTypeApi, action);
    if(response){
        yield put({type: TRANSFORMER_TYPE_TYPE.DELETE_TRANSFORMER_TYPE_SUCCESS, response});
    }else{
        yield put({type: TRANSFORMER_TYPE_TYPE.DELETE_TRANSFORMER_TYPE_FAIL, error});
    }
}

export default function* transformerTypeSaga() {
    yield fork(takeEvery, TRANSFORMER_TYPE_TYPE.ADD_TRANSFORMER_TYPE, addTransformerTypeSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE_TYPE.GET_ALL_TRANSFORMER_TYPES, getAllTransformerTypesSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE_TYPE.UPDATE_TRANSFORMER_TYPE, updateTransformerTypeSaga);
    yield fork(takeEvery, TRANSFORMER_TYPE_TYPE.DELETE_TRANSFORMER_TYPE, deleteTransformerTypeSaga);
}

