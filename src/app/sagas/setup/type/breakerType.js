import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as BREAKER_TYPE_TYPE from './../../../actions/setup/type/breakerType';
import * as BREAKER_TYPE_API from './../../../services/setup/type/breakerType';

function* addBreakerTypeSaga(action){
    const { response, error } = yield call(BREAKER_TYPE_API.addBreakerTypeApi, action);
    if(response){
        yield put({type: BREAKER_TYPE_TYPE.ADD_BREAKER_TYPE_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE_TYPE.ADD_BREAKER_TYPE_FAIL, error});
    }
}

function* getAllBreakerTypesSaga(action){
    const response = yield call(BREAKER_TYPE_API.getAllBreakerTypesApi, action);
    if(!(response == undefined)){
        yield put({type: BREAKER_TYPE_TYPE.GET_ALL_BREAKER_TYPES_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE_TYPE.GET_ALL_BREAKER_TYPES_FAIL, response});
    }
}

function* updateBreakerTypeSaga(action){
    const { response, error } = yield call(BREAKER_TYPE_API.updateBreakerTypeApi, action);
    if(response){
        yield put({type: BREAKER_TYPE_TYPE.UPDATE_BREAKER_TYPE_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE_TYPE.UPDATE_BREAKER_TYPE_FAIL, error});
    }
}

function* deleteBreakerTypeSaga(action){
    const { response, error } = yield call(BREAKER_TYPE_API.deleteBreakerTypeApi, action);
    if(response){
        yield put({type: BREAKER_TYPE_TYPE.DELETE_BREAKER_TYPE_SUCCESS, response});
    }else{
        yield put({type: BREAKER_TYPE_TYPE.DELETE_BREAKER_TYPE_FAIL, error});
    }
}

export default function* breakerTypeSaga() {
    yield fork(takeEvery, BREAKER_TYPE_TYPE.ADD_BREAKER_TYPE, addBreakerTypeSaga);
    yield fork(takeEvery, BREAKER_TYPE_TYPE.GET_ALL_BREAKER_TYPES, getAllBreakerTypesSaga);
    yield fork(takeEvery, BREAKER_TYPE_TYPE.UPDATE_BREAKER_TYPE, updateBreakerTypeSaga);
    yield fork(takeEvery, BREAKER_TYPE_TYPE.DELETE_BREAKER_TYPE, deleteBreakerTypeSaga);
}