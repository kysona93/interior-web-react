import {takeEvery} from 'redux-saga';
import { call, put,fork } from 'redux-saga/effects';
import * as AREA_API from '../../services/setup/area';
import * as AREA from '../../actions/setup/area';

export function* addAreaSaga(action){
    const {response, error} = yield call(AREA_API.addAreaApi, action);
    if(response){
        yield put({type: AREA.ADD_AREA_SUCCESS, response});
    }else{
        yield put({type: AREA.ADD_AREA_FAIL, error});
    }
}

export function* getAllAreasSaga(){
    const {response, error} = yield call(AREA_API.getAllAreasApi);
    if(response){
        yield put({type: AREA.GET_ALL_AREAS_SUCCESS, response});
    }else{
        yield put({type: AREA.GET_ALL_AREAS_FAIL, error});
    }
}
export function* getAreasSaga(action){
    const {response, error} = yield call(AREA_API.getAreasApi, action);
    if(response){
        yield put({type: AREA.GET_AREAS_SUCCESS, response});
    }else{
        yield put({type: AREA.GET_AREAS_FAIL, error});
    }
}

export function* getAreaReportsSaga(){
    const {response, error} = yield call(AREA_API.getAreaReportsApi);
    if(response){
        yield put({type: AREA.GET_AREA_REPORTS_SUCCESS, response});
    }else{
        yield put({type: AREA.GET_AREA_REPORTS_FAIL, error});
    }
}

export function* updateAreaSaga(action){
    const {response, error} = yield call(AREA_API.updateAreaApi, action);
    if(response){
        yield put({type: AREA.UPDATE_AREA_SUCCESS, response});
    }else{
        yield put({type: AREA.UPDATE_AREA_FAIL, error});
    }
}

export function* deleteAreaSaga(action){
    const {response, error} = yield call(AREA_API.deleteAreaApi, action);
    if(response){
        yield put({type: AREA.DELETE_AREA_SUCCESS, response});
    }else{
        yield put({type: AREA.DELETE_AREA_FAIL, error});
    }
}

export default function* areaSaga() {
    yield fork(takeEvery ,AREA.GET_ALL_AREAS, getAllAreasSaga);
    yield fork(takeEvery ,AREA.GET_AREAS, getAreasSaga);
    yield fork(takeEvery ,AREA.GET_AREA_REPORTS, getAreaReportsSaga);
    yield fork(takeEvery ,AREA.ADD_AREA, addAreaSaga);
    yield fork(takeEvery ,AREA.UPDATE_AREA, updateAreaSaga);
    yield fork(takeEvery ,AREA.DELETE_AREA, deleteAreaSaga);
}