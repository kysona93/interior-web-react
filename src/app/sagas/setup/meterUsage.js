import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import {
    ADD_METER_USAGE,
    ADD_METER_USAGE_SUCCESS,
    ADD_METER_USAGE_FAIL,
    GET_ALL_METER_USAGES,
    GET_ALL_METER_USAGES_SUCCESS,
    GET_ALL_METER_USAGES_FAIL,
    UPDATE_METER_USAGE,
    UPDATE_METER_USAGE_SUCCESS,
    UPDATE_METER_USAGE_FAIL,
    DELETE_METER_USAGE,
    DELETE_METER_USAGE_SUCCESS,
    DELETE_METER_USAGE_FAIL
} from './../../actions/setup/meterUsage';
import {
    addMeterUsageApi,
    listAllMeterUsagesApi,
    updateMeterUsageApi,
    deleteMeterUsageApi
} from './../../services/setup/meterUsage';

function* addMeterUsageSaga(action){
    const response = yield call(addMeterUsageApi, action);
    if(!(response == undefined)){
        yield put({type: ADD_METER_USAGE_SUCCESS, response});
    }else{
        yield put({type: ADD_METER_USAGE_FAIL, response});
    }
}

function* listAllMeterUsagesSaga(action){
    const response = yield call(listAllMeterUsagesApi, action);
    if(!(response == undefined)){
        yield put({type: GET_ALL_METER_USAGES_SUCCESS, response});
    }else{
        yield put({type: GET_ALL_METER_USAGES_FAIL, response});
    }
}

function* updateMeterUsageSaga(action){
    const response = yield call(updateMeterUsageApi, action);
    if(!(response == undefined)){
        yield put({type: UPDATE_METER_USAGE_SUCCESS, response});
    }else{
        yield put({type: UPDATE_METER_USAGE_FAIL, response});
    }
}

function* deleteMeterUsageSaga(action){
    const response = yield call(deleteMeterUsageApi, action);
    if(!(response == undefined)){
        yield put({type: DELETE_METER_USAGE_SUCCESS, response});
    }else{
        yield put({type: DELETE_METER_USAGE_FAIL, response});
    }
}

export default function* meterUsageSaga() {
    yield fork(takeEvery, ADD_METER_USAGE, addMeterUsageSaga);
    yield fork(takeEvery, GET_ALL_METER_USAGES, listAllMeterUsagesSaga);
    yield fork(takeEvery, UPDATE_METER_USAGE, updateMeterUsageSaga);
    yield fork(takeEvery, DELETE_METER_USAGE, deleteMeterUsageSaga);
}