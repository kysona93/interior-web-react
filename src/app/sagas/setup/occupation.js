import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as OCCUPATION_TYPE from './../../actions/setup/occupation';
import * as OCCUPATION_API from './../../services/setup/occupation';

function* addOccupationSaga(action){
    const { response, error } = yield call(OCCUPATION_API.addOccupationApi, action);
    if(response){
        yield put({type: OCCUPATION_TYPE.ADD_OCCUPATION_SUCCESS, response});
    }else{
        yield put({type: OCCUPATION_TYPE.ADD_OCCUPATION_FAIL, error});
    }
}

function* listAllOccupationsSaga(action){
    const { response, error } = yield call(OCCUPATION_API.getAllOccupationsApi, action);
    if(response){
        yield put({type: OCCUPATION_TYPE.GET_ALL_OCCUPATIONS_SUCCESS, response});
    }else{
        yield put({type: OCCUPATION_TYPE.Get_ALL_OCCUPATIONS_FAIL, error});
    }
}

function* updateOccupationSaga(action){
    const { response, error } = yield call(OCCUPATION_API.updateOccupationApi, action);
    if(response){
        yield put({type: OCCUPATION_TYPE.UPDATE_OCCUPATION_SUCCESS, response});
    }else{
        yield put({type: OCCUPATION_TYPE.UPDATE_OCCUPATION_FAIL, error});
    }
}

function* deleteOccupationSaga(action){
    const { response, error } = yield call(OCCUPATION_API.deleteOccupationApi, action);
    if(response){
        yield put({type: OCCUPATION_TYPE.DELETE_OCCUPATION_SUCCESS, response});
    }else{
        yield put({type: OCCUPATION_TYPE.DELETE_OCCUPATION_FAIL, error});
    }
}

export default function* occupationSaga() {
    yield fork(takeEvery, OCCUPATION_TYPE.ADD_OCCUPATION, addOccupationSaga);
    yield fork(takeEvery, OCCUPATION_TYPE.GET_ALL_OCCUPATIONS, listAllOccupationsSaga);
    yield fork(takeEvery, OCCUPATION_TYPE.UPDATE_OCCUPATION, updateOccupationSaga);
    yield fork(takeEvery, OCCUPATION_TYPE.DELETE_OCCUPATION, deleteOccupationSaga);
}