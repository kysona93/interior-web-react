import { takeEvery } from "redux-saga";
import { call, put, fork } from 'redux-saga/effects';
import * as JOB_TITLE from '../actions/location';
import * as JOB_TITLE_API from '../services/location';

function* getAllJobTitleSaga(action){
    const response = yield call(JOB_TITLE_API.getAllLocationsApi, action);
    if(!(response === undefined)){
        yield put({type: JOB_TITLE.GET_ALL_LOCATIONS_SUCCESS, response});
    }else{
        yield put({type: JOB_TITLE.GET_ALL_LOCATIONS_FAIL, response});
    }
}

function* addJobTitleSaga(action){
    const response = yield call(JOB_TITLE_API.addLocationApi, action);
    if(!(response === undefined)){
        yield put({type: JOB_TITLE.ADD_LOCATION_SUCCESS, response});
    }else{
        yield put({type: JOB_TITLE.ADD_LOCATION_FAIL, response});
    }
}

function* updateJobTitleSaga(action){
    const response = yield call(JOB_TITLE_API.updateLocationApi, action);
    if(!(response === undefined)){
        yield put({type: JOB_TITLE.UPDATE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: JOB_TITLE.UPDATE_LOCATION_FAIL, response});
    }
}

function* deleteJobTitleSaga(action){
    const response = yield call(JOB_TITLE_API.deleteLocationApi, action);
    if(!(response === undefined)){
        yield put({type: JOB_TITLE.DELETE_LOCATION_SUCCESS, response});
    }else{
        yield put({type: JOB_TITLE.DELETE_LOCATION_FAIL, response});
    }
}

export default function* JobTitleSaga() {
    yield fork(takeEvery, JOB_TITLE.GET_ALL_LOCATIONS, getAllLocationsSaga);
    yield fork(takeEvery, JOB_TITLE.ADD_LOCATION, addJobTitleSaga);
    yield fork(takeEvery, JOB_TITLE.UPDATE_LOCATION, updateJobTitleSaga);
    yield fork(takeEvery, JOB_TITLE.DELETE_LOCATION, deleteJobTitleSaga);
}


