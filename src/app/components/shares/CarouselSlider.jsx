import React from 'react';
import {Carousel} from 'react-bootstrap';

class CarouselSlider extends React.Component{
    constructor(props){
        super(props);
    }

    render() {
        return (
            <Carousel >
                <Carousel.Item>
                    <img  style={{width:'100%', height:'600px'}} alt="900x500" src="images/slider1.jpg" />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img  style={{width:'100%', height:'600px'}} alt="900x500" src="images/slider2.jpg" />
                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img  style={{width:'100%', height:'600px'}} alt="900x500" src="images/slider3.jpg" />
                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img style={{width:'100%', height:'600px'}} alt="900x500" src="images/slider4.jpg" />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img  style={{width:'100%', height:'600px'}} alt="900x500" src="images/slider5.jpg" />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        );
    }
}
export default CarouselSlider;