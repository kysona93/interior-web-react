import React from 'react';
import {Image} from 'react-bootstrap';

class MenuPicture extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div>
                <Image src={this.props.name} style={{height:'800px', width:'100%'}} responsive />
            </div>
        )
    }
}

export default MenuPicture;