import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import './Styles.css';
const jobs = [];

function addJobs(quantity) {
    const startId = jobs.length;
    for (let i = 0; i < quantity; i++) {
        const id = startId + i;
        let priority = 'D';
        if (i % 2 === 0) priority = 'C';
        if (i % 5 === 0) priority = 'B';
        if (i % 7 === 0) priority = 'A';
        jobs.push({
            id: id,
            name: 'Item name ' + id,
            priority: priority,
            active: i%2 === 0 ? 'Y' : 'N'
        });
    }
}

addJobs(70);

function onRowSelect(row, isSelected) {
    alert(JSON.stringify(row));
    console.log(`selected: ${isSelected}`);
}

function onSelectAll(isSelected, rows) {
    alert(JSON.stringify(rows));
    console.log(`is select all: ${isSelected}`);
}

function onAfterSaveCell(row, cellName, cellValue) {
    alert(JSON.stringify(row))
}

function onAfterTableComplete() {
    console.log('Table render complete.');
}

function onAfterDeleteRow(rowKeys) {
    console.log('onAfterDeleteRow');
    console.log(rowKeys);
}

function onAfterInsertRow(row) {
    console.log('onAfterInsertRow');
    console.log(row);
}

const selectRowProp = {
    mode: 'checkbox',
    clickToSelect: true,
    selected: [], // default select on table
    bgColor: 'rgb(238, 193, 213)',
    onSelect: onRowSelect,
    onSelectAll: onSelectAll
};

const cellEditProp = {
    mode: 'click',
    blurToSave: true,
    beforeSaveCell: beforeSaveCell,
    afterSaveCell: onAfterSaveCell
};

const options = {
    paginationShowsTotal: true,
    sortName: 'name',  // default sort column name
    sortOrder: 'desc',  // default sort order
    afterTableComplete: onAfterTableComplete, // A hook for after table render complete.
    afterDeleteRow: onAfterDeleteRow,  // A hook for after droping rows.
    afterInsertRow: onAfterInsertRow   // A hook for after insert rows
};


function priorityFormatter(cell, row) {
    if (cell === 'A') return '<font color="red">' + cell + '</font>';
    else if (cell === 'B') return '<font color="orange">' + cell + '</font>';
    else return cell;
}

function trClassNameFormat(rowData, rIndex) {
    return rIndex % 2 === 0 ? 'third-tr' : '';
}
function nameValidator(value) {
    if (!value) {
        return 'Job Name is required!';
    } else if (value.length < 3) {
        return 'Job Name length must great 3 char';
    }
    return true;
}
function priorityValidator(value) {
    if (!value) {
        return 'Priority is required!';
    }
    return true;
}

function Test() {
    alert("Ok");
}

function beforeSaveCell(row, cellName, cellValue) {
    alert(JSON.stringify(row))
}

export default class TableBase extends React.Component {
    render() {
        return (
            <div className="container">
                <BootstrapTable
                    data={ jobs } //list of records
                    trClassName={trClassNameFormat}  //apply style on cell
                    headerStyle={{background: "red"}}
                    selectRow={ selectRowProp } //apply function to select record
                    cellEdit={ cellEditProp } // apply function to edit cell
                    options={ options } //
                    insertRow // apply button add on the top left
                    deleteRow // apply button delete on the top right
                    search // apply text-box search on the top right
                    hover // apply hover style on row
                    pagination // apply pagination
                >
                    <TableHeaderColumn dataField='id' width="10%" dataAlign='center' dataSort isKey autoValue><button onClick={Test} >Job ID</button></TableHeaderColumn>
                    <TableHeaderColumn dataField='name' width="55%" className='good' dataSort editable={ { type: 'textarea', validator: nameValidator } }>Job Name</TableHeaderColumn>
                    <TableHeaderColumn dataField='priority' width="20%" dataSort dataFormat={ priorityFormatter } editable={ {type: 'select', options: { values: [ 'A', 'B', 'C', 'D' ] }, validator: priorityValidator } }>Job Priority</TableHeaderColumn>
                    <TableHeaderColumn dataField='active' width="10%" editable={ { type: 'checkbox', options: { values: ' Y:N' } } }>Active</TableHeaderColumn>
                </BootstrapTable>
            </div>
        );
    }
}