import React from 'react';
//import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { push } from 'react-router-redux';
import { FormGroup, Radio } from 'react-bootstrap';
import './../index.css';
import { getMenuAction } from '../../../actions/menu';
import { getPagesByUsernameAction } from './../../../actions/page';
import { savePages } from './../../../../auth';
import { store } from "../../../../store.js";

let parentId1 = "";
let parentId2 = "";
let parentId3 = "";
let parentIndex1 = "";
class MenuLeft extends React.Component{
    constructor(props){
        super(props);
        this.state={
            type: 'fuzzy',
            option: 'id'
        };
        this.handleSearch = this.handleSearch.bind(this);
    }

    componentWillMount(){
        this.props.getMenuAction();
        this.props.getPagesByUsernameAction();
    }

    handleSelect(name, link){
        if(link !== null && link !== undefined && link !== ""){
            this.props.tab(
                link,
                name
            );
            store.dispatch(push(link));
        }
    }

    componentWillReceiveProps(data) {
        if(data.pagesByUsername.status === 200) {
            savePages(data.pagesByUsername.data.items);
        }

    }

    handleSearch(){
        const search = document.getElementById('srch-term').value;
        this.handleSelect(`Search(${search})`, `/app/customers?type=${this.state.type.trim()}&opt=${this.state.option.trim()}&search=${search}`);
    }

    handleSearchType(e) {
        this.setState({type: e.target.value});
    }
    handleSearchOption(e){
        this.setState({option: e.target.value});
    }
    render(){
        return (
            <div className="wrap-menu-left">
                <div className="col-lg-3 padding_10_0">
                    <div className="menu">
                        <div className="accordion">
                            {/*MenuList*/}
                            <div className="accordion-group">
                                <div className="accordion-heading area">
                                    <a className="accordion-toggle" data-toggle="collapse" href="#area1">
                                        <i className="fa fa-bars" aria-hidden="true"> </i>Menu List
                                    </a>
                                </div>
                                <div className="accordion-body collapse" id="area1">
                                    <div className="accordion-inner">
                                        <div className="accordion" id="equipamento1">
                                            {
                                                this.props.getMenus.status === 200 ?
                                                    this.props.getMenus.data.items.map((menu, index) => {
                                                        parentId1 = menu.id;
                                                        parentIndex1 = index;
                                                        return (
                                                            <div key={index}>
                                                                {
                                                                    (menu.parentId === 0 && menu.isLeaf === 0) ?
                                                                        <div className="accordion-group" >
                                                                            <div className="accordion-heading equipamento">
                                                                                <a className="accordion-toggle" data-parent="#equipamento1-1" data-toggle="collapse" href={"#ponto1-"+parentIndex1}>
                                                                                    <i className={"fa " + menu.icon}>{ menu.menuName }</i>
                                                                                </a>
                                                                                <div className="dropdown edit">
                                                                                    <a className="dropdown-toggle icon-pencil" data-toggle="dropdown" > </a>
                                                                                </div>
                                                                                <div className="accordion-body collapse" id={"ponto1-"+parentIndex1} >
                                                                                    <div className="accordion-inner">
                                                                                        <div className="accordion" id="servico1">
                                                                                            {
                                                                                                this.props.getMenus.data.items.map((submenu1, subindex1) => {
                                                                                                    parentId2 = submenu1.id;
                                                                                                    const linkTo = (url) => {
                                                                                                        console.log("url :", url)
                                                                                                        let currentUrl = window.location.pathname;
                                                                                                        store.dispatch(push(url == null ? currentUrl : url));
                                                                                                    };
                                                                                                    return (
                                                                                                        <div key={subindex1}>
                                                                                                            {
                                                                                                                parentId1 === submenu1.parentId ?
                                                                                                                    <div className="accordion-group" key={subindex1}>
                                                                                                                        <div className="accordion-heading ponto">
                                                                                                                            <a onClick={() => this.handleSelect(submenu1.menuName, submenu1.link)} className="accordion-toggle" data-parent={"#servico1-1-"+subindex1} data-toggle="collapse" href={"#servico1-1-"+subindex1}>
                                                                                                                               <i className={"fa " + submenu1.icon}/>{submenu1.menuName}
                                                                                                                            </a>
                                                                                                                        </div>
                                                                                                                        <div className="accordion-body collapse" id={"servico1-1-"+subindex1}>
                                                                                                                            {
                                                                                                                                this.props.getMenus.data.items.map((submenu2, subindex2) => {
                                                                                                                                    parentId3 = submenu2.id;
                                                                                                                                    const linkTo = (url) => {
                                                                                                                                        let currentUrl = window.location.pathname;
                                                                                                                                        store.dispatch(push(url === null ? currentUrl : url));
                                                                                                                                    };
                                                                                                                                    return (
                                                                                                                                        <div key={subindex2}>
                                                                                                                                            {
                                                                                                                                                parentId2 === submenu2.parentId ?
                                                                                                                                                    <div className="accordion-group" key={subindex2}>
                                                                                                                                                        <div className="accordion-heading ponto-2">
                                                                                                                                                            <a onClick={() => this.handleSelect(submenu2.menuName, submenu2.link)} className="accordion-toggle" data-parent={"#servico2-2-"+subindex2} data-toggle="collapse" href={"#servico2-2-"+subindex2}>
                                                                                                                                                                <i className={"fa " + submenu2.icon} />{submenu2.menuName}
                                                                                                                                                            </a>
                                                                                                                                                        </div>
                                                                                                                                                        <div className="accordion-body collapse" id={"servico2-2-"+subindex2}>
                                                                                                                                                            <div className="accordion-inner">
                                                                                                                                                                <ul className="nav nav-list">
                                                                                                                                                                    {
                                                                                                                                                                        this.props.getMenus.data.items.map((submenu3, subindex3) => {
                                                                                                                                                                            const linkTo = (url) => {
                                                                                                                                                                                let currentUrl = window.location.pathname;
                                                                                                                                                                                store.dispatch(push(url === null ? currentUrl : url));
                                                                                                                                                                            };
                                                                                                                                                                            return (
                                                                                                                                                                                <div key={subindex3}>
                                                                                                                                                                                {
                                                                                                                                                                                    parentId3 === submenu3.parentId ?
                                                                                                                                                                                        <div className="accordion-group" key={subindex3}>
                                                                                                                                                                                            <div className="accordion-heading ponto-3">
                                                                                                                                                                                                <a onClick={() => this.handleSelect(submenu3.menuName, submenu3.link)} >
                                                                                                                                                                                                    <i className={"fa " + submenu3.icon} />{submenu3.menuName}
                                                                                                                                                                                                </a>
                                                                                                                                                                                            </div>

                                                                                                                                                                                        </div>
                                                                                                                                                                                    : null
                                                                                                                                                                                }
                                                                                                                                                                            </div>
                                                                                                                                                                            )
                                                                                                                                                                        })
                                                                                                                                                                    }
                                                                                                                                                                </ul>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                    :
                                                                                                                                                    null
                                                                                                                                            }
                                                                                                                                        </div>
                                                                                                                                    )
                                                                                                                                })
                                                                                                                            }
                                                                                                                        </div>
                                                                                                                    </div>
                                                                                                                    :
                                                                                                                    null
                                                                                                            }
                                                                                                        </div>
                                                                                                    )
                                                                                                })
                                                                                            }
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        :
                                                                        null
                                                                }
                                                            </div>
                                                        )
                                                    })
                                                    :
                                                    <h1>Expired Token</h1>
                                            }
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/*End MenuList*/}

                            {/*Advance SearchList*/}
                            <div className="accordion-group">
                                <div className="accordion-heading area">
                                    <a className="accordion-toggle" data-toggle="collapse" href="#area2">
                                        <i className="fa fa-search"> </i>Advance Search
                                    </a>
                                </div>
                                <div className="accordion-body collapse in multi-search" id="area2">
                                    <div className="form-search">
                                        <form className="navbar-form" role="search">
                                            <div className="input-group add-on">
                                                <input className="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text"/>
                                                <div className="input-group-btn">
                                                    <a type="button" className="btn btn-default" style={{height: '34px'}} onClick={this.handleSearch}><i className="glyphicon glyphicon-search"> </i></a>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div className="option-search">
                                        <button type="button" className="btn btn-info search-option" data-toggle="collapse" data-target="#type">Search Type</button>
                                        <div id="type" className="collapse in option-search">
                                            <FormGroup>
                                                <Radio className="radio-button" name="radioSearchType" defaultChecked value="fuzzy" onClick ={this.handleSearchType.bind(this)}>
                                                    Fuzzy Search
                                                </Radio>
                                                {' '}
                                                <Radio className="radio-button" name="radioSearchType" value="match" onClick ={this.handleSearchType.bind(this)}>
                                                    Match Search
                                                </Radio>
                                            </FormGroup>
                                        </div>
                                    </div>
                                    <div className="option-search">
                                        <button type="button" className="btn btn-info search-option" data-toggle="collapse" data-target="#option">Search Option</button>
                                        <div id="option" className="collapse in option-search">
                                            <FormGroup>
                                                <br />
                                                <div style={{border: 'groove'}}>
                                                    <Radio className="radio-button" name="radioSearchOption" defaultChecked value="id" onClick ={this.handleSearchOption.bind(this)}>
                                                        Customer ID
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="name" onClick ={this.handleSearchOption.bind(this)}>
                                                        Customer Name
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="phone" onClick ={this.handleSearchOption.bind(this)}>
                                                        Phone Number
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="identity" onClick ={this.handleSearchOption.bind(this)}>
                                                        Identity Number
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="prepaid" onClick ={this.handleSearchOption.bind(this)}>
                                                        Prepaid ID
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="invoice" onClick ={this.handleSearchOption.bind(this)}>
                                                        Invoice No
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="box_serial" onClick ={this.handleSearchOption.bind(this)}>
                                                        Box Serial
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="pole_serial" onClick ={this.handleSearchOption.bind(this)}>
                                                        Pole Serial
                                                    </Radio>
                                                </div>
                                                <div style={{border: 'groove'}}>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="meter_info" onClick ={this.handleSearchOption.bind(this)}>
                                                        Meter Info
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="pole_info" onClick ={this.handleSearchOption.bind(this)}>
                                                        Pole Info
                                                    </Radio>
                                                    {' '}
                                                    <Radio className="radio-button" name="radioSearchOption" value="transformer_info" onClick ={this.handleSearchOption.bind(this)}>
                                                        Transformer Info
                                                    </Radio>
                                                </div>
                                            </FormGroup>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*End Advance SearchList*/}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        getMenus : state.menu.getMenus,
        pagesByUsername: state.page.getPagesByUsername
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getMenuAction, getPagesByUsernameAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(MenuLeft) ;