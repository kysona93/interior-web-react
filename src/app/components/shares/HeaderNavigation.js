import React from 'react';
import {Image} from 'react-bootstrap';
import {Link} from 'react-router';
import './index.css';

class HeaderNavigation extends React.Component{
    render(){
        return(
            <div style={{textAlign:'center'}}>
                <nav className="navbar" >
                    <div className="container-fluid">
                        <Link href="/" >
                            <Image style={{marginTop:'20px', marginBottom:'20px', width:'130px', height:'90px', float:"left"}}
                                   src="images/logo.jpg"/>
                        </Link>

                        <div style={{float:"right",marginTop:'50px', marginRight:"50px"}}>
                            <Link href="/">HOME</Link>
                            <Link href="/portfolio" style={{marginLeft:'30px'}}>PORTFOLIO</Link>
                            <Link href="/service" style={{marginLeft:'30px'}}>SERVICES</Link>
                            <Link href="/about-us" style={{marginLeft:'30px'}}>ABOUT US</Link>
                            <Link href="/contact-us" style={{marginLeft:'30px'}}>CONTACT US</Link>
                        </div>

                    </div>
                </nav>
            </div>
        )
    }
}
export default  HeaderNavigation;