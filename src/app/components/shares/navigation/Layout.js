// import React from 'react';
// import {Breadcrumb} from 'react-bootstrap';
// import MenuLeft from './../menu-left/MenuLeft';
//
// export default class Layout extends React.Component{
//     constructor(props) {
//         super(props);
//         this.state = {open: true};
//
//         this.openNav=this.openNav.bind(this);
//         this.closeNav=this.closeNav.bind(this);
//     }
//     openNav() {
//         document.getElementById("mySidenav").style.width = "300px";
//         document.getElementById("main").style.marginLeft = "285px";
//     }
//
//     closeNav() {
//         document.getElementById("mySidenav").style.width = "0";
//         document.getElementById("main").style.marginLeft= "0";
//     }
//     render(){
//          return(
//              <div className="container-fluid">
//                  <div className="row">
//                      <div className="col-lg-12 paddingLR">
//                          <nav className="navbar navbar-inverse">
//                              <div className="container-fluid">
//                                  <div className="navbar-header">
//                                      <a className="navbar-brand blog-menu" href=""><img src="images/icons/bnd_copyright.png"/> </a>
//                                      <span className="open-menu" style={{fontSize:'18px',cursor:'pointer'}} onClick={this.openNav}>&#9776;</span>
//                                  </div>
//                                  <ul className="nav navbar-nav navbar-right">
//                                      <li><a href="#"><span className="glyphicon glyphicon-user"> </span> Sign Up</a></li>
//                                      <li><a href="/login"><span className="glyphicon glyphicon-log-in"> </span> Login</a></li>
//                                  </ul>
//                              </div>
//                          </nav>
//                      </div>
//                  </div>
//                  <div className="row">
//                      <div className="col-lg-12 paddingLR">
//                          <Breadcrumb>
//                              <Breadcrumb.Item>
//                              </Breadcrumb.Item>
//                          </Breadcrumb>
//                          <div id="main">
//                              {this.props.children}
//                          </div>
//
//                      </div>
//                  </div>
//
//                  <div>
//                      <div id="mySidenav" className="sidenav">
//                          <img src="/images/icon/bnd_copyright.png"/>
//                          <a href="javascript:void(0)" className="closebtn" onClick={this.closeNav}>&times;</a>
//                          <MenuLeft/>
//                      </div>
//                  </div>
//              </div>
//
//          )
//      }
// }
