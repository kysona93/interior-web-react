import React from 'react';
import { Link, browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './../index.css';
import { getMenuAction } from '../../../actions/menu';
let parentId1 = "";
let parentId2 = "";
let parentId3 = "";
let parentIndex1 = "";
let parentIndex2 = "";
let parentIndex3 = "";

class SideBarLeft extends React.Component{
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.getMenuAction();
    }

    render(){
        return(
            <div className="wrap-menu-left">
                <div className="col-lg-3 padding_0_0">
                    <div className="menu">
                        <div className="accordion">
                            {/*MenuList*/}
                            <div className="accordion-group">
                                <div className="accordion-heading area">
                                    <a className="accordion-toggle" data-toggle="collapse" href="#area2"><i className="fa fa-bars" aria-hidden="true"> </i>Menu List</a>
                                </div>
                                <div className="accordion-body collapse" id="area2">
                                    <div className="accordion-inner">
                                        <div className="accordion" id="equipamento1">

                                            <div className="accordion-group">
                                                <div className="accordion-heading equipamento">
                                                    <a className="accordion-toggle" data-parent="#equipamento1-1" data-toggle="collapse" href="#ponto2-3"><i className="fa fa-user" aria-hidden="true"> </i>Administration</a>
                                                    <div className="dropdown edit">
                                                        <a className="dropdown-toggle icon-pencil" data-toggle="dropdown" href="#"> </a>
                                                    </div>
                                                </div>
                                                <div className="accordion-body collapse" id="ponto2-3">
                                                    <div className="accordion-inner">
                                                        <div className="accordion" id="servico1">

                                                            <div className="accordion-group">
                                                                <div className="accordion-heading ponto">
                                                                    <a className="accordion-toggle" data-parent="#servico2-2-3" data-toggle="collapse" href="#servico2-2-3"><i className="fa fa-cogs" aria-hidden="true"> </i> Set Up</a>
                                                                </div>

                                                                <div className="accordion-body collapse" id="servico2-2-3">
                                                                    <div className="accordion-group">
                                                                        <div className="accordion-heading ponto-1">
                                                                            <a className="accordion-toggle" data-parent="#servico2-2-4" data-toggle="collapse" href="#servico2-2-4"><i className="fa fa-folder-o" aria-hidden="true"> </i>Type</a>
                                                                        </div>
                                                                        <div className="accordion-body collapse" id="servico2-2-4">
                                                                            <div className="accordion-inner">
                                                                                <ul className="nav nav-list">
                                                                                    <li><Link to="/menu/administrator/setup/transformer"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Customer Type</Link></li>
                                                                                    <li><Link to="/menu/administrator/setup/box"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Box Type</Link></li>
                                                                                    <li><Link to="/menu/administrator/setup/pole"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Pole Type</Link></li>
                                                                                    <li><Link to="/menu/administrator/setup/transformer"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Transformer Type</Link></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    {/* box */}
                                                                    <div className="accordion-group">
                                                                        <div className="accordion-heading ponto-1">
                                                                            <a className="accordion-toggle" data-parent="#servico2-2-4-1" data-toggle="collapse" href="#servico2-2-4-1"><i className="fa fa-folder-o" aria-hidden="true"> </i>Box</a>
                                                                        </div>
                                                                        <div className="accordion-body collapse" id="servico2-2-4-1">
                                                                            <div className="accordion-inner">
                                                                                <ul className="nav nav-list">
                                                                                    <li><Link to="/menu/administrator/setup/box"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Add Box</Link></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {/* box */}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div className="accordion-group">
                                                <div className="accordion-heading equipamento">
                                                    <a className="accordion-toggle" data-parent="#equipamento1-1" data-toggle="collapse" href="#ponto3-3"><i className="fa fa-file-text" aria-hidden="true"> </i>Report</a>
                                                    <div className="dropdown edit">
                                                        <a className="dropdown-toggle icon-pencil" data-toggle="dropdown" href="#"></a>
                                                    </div>
                                                </div>
                                                <div className="accordion-body collapse" id="ponto3-3">
                                                    <div className="accordion-inner">
                                                        <div className="accordion" id="servico1">

                                                            <div className="accordion-group">
                                                                <div className="accordion-heading ponto">
                                                                    <a className="accordion-toggle" data-parent="#servico3-3-3" data-toggle="collapse" href="#servico3-3-3"><i className="fa fa-money" aria-hidden="true"> </i>Expend Report</a>
                                                                </div>

                                                                <div className="accordion-body collapse" id="servico3-3-3">
                                                                    <div className="accordion-group">
                                                                        <div className="accordion-heading ponto-1">
                                                                            <a className="accordion-toggle" data-parent="#servico3-3-4" data-toggle="collapse" href="#servico3-3-4"><i className="fa fa-folder-o" aria-hidden="true"> </i>Sub Search 2</a>
                                                                        </div>
                                                                        <div className="accordion-body collapse" id="servico3-3-4">
                                                                            <div className="accordion-inner">
                                                                                <ul className="nav nav-list">
                                                                                    <li><a href="#"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Sub Search 4</a></li>
                                                                                    <li><a href="#"><i className="fa fa-file-text-o" aria-hidden="true"> </i>Sub Search 4</a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/*End MenuList*/}

                            {/*Advance SearchList*/}
                            <div className="accordion-group">
                                <div className="accordion-heading area">
                                    <a className="accordion-toggle" data-toggle="collapse" href="#area3"><i className="fa fa-search" aria-hidden="true"></i>Advance SearchList</a>
                                </div>
                            </div>
                            {/*End Advance SearchList*/}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    return {
        getMenus : state.menu.getMenus
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({getMenuAction},dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(SideBarLeft) ;