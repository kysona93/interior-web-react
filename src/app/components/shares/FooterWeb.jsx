import React from 'react';
import { Row, Col } from 'react-bootstrap';
import './index.css';

class FooterWeb extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div className="container-fluid footer-bk-color">
                <div className="container footer-content">
                    <Row>
                        <Col xs={4} md={4}>
                            <h3>OUR COMPANY</h3>
                            <p>Address: Phnom Penh, Cambodia</p>
                            <p>Phone: 078 999 167</p>
                            <p>Email: trysenghay@yahoo.com</p>
                        </Col>
                        <Col xs={4} md={4}>
                            <h3>OPENING HOURS</h3>
                            <p>Mon - Sun: 8am - 5pm</p>​​
                        </Col>
                        <Col xs={4} md={4}>
                            <h3>FOLLOW US</h3>
                            <span>
                                <a href="https://www.facebook.com/ApsaraFurnitures/" target="_blank">
                                <i className="fa fa-facebook-square fa-2x" aria-hidden="true"></i></a>
                            </span>
                        </Col>
                    </Row>
                </div>
                <div className="container footer-content">
                    <p style={{textAlign:'center'}}>Copyright © 2018 APSARA HOME & FURNITURE</p>
                </div>
            </div>
        );
    }
}

export default FooterWeb;