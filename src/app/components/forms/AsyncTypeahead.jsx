import React, {PropTypes} from '../../../../node_modules/react';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import { AsyncTypeahead } from 'react-bootstrap-typeahead';
import './Styles.css';

export default class AsyncTypeaheadFilter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            options: [],
        };
    }

    render() {
        const { placeholder, labelKey, values, handleOnChange, handleRef, meta, defaultSelected, disabled } = this.props;

        const _renderMenuItemChildren = (option, props, index) => {
            return (
                <div key={option.id}>
                    <span>{option.name}</span>
                </div>
            );
        };

        const _handleSearch = query => {
            if (!query) {
                return;
            }

            const valuesFilters = values.filter((value) =>
                value.name.toLowerCase().indexOf(query.toLowerCase()) > -1
            );

            this.setState({options: valuesFilters});
        };

        return (
            <FormGroup validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
                <AsyncTypeahead
                    {...this.state}
                    clearButton
                    labelKey={labelKey}
                    ref={handleRef}
                    onSearch={_handleSearch}
                    placeholder={placeholder}
                    onChange={handleOnChange}
                    renderMenuItemChildren={_renderMenuItemChildren}
                    selected={defaultSelected}
                    disabled={disabled}
                    className="form_typeahead"
                />
                <FormControl.Feedback style={{left: 0, marginTop: 10, width: 30, height: 30}} autoComplete="off">
                    <i className="fa fa-search" />
                </FormControl.Feedback>
                <HelpBlock>
                    {meta.touched && meta.error ? meta.error : null}
                </HelpBlock>
            </FormGroup>
        );
    }
}

AsyncTypeaheadFilter.propTypes = {
    labelKey: PropTypes.string,
    placeholder: PropTypes.string,
    values: PropTypes.array,
    handleOnChange: PropTypes.func,
    handleRef: PropTypes.func,
    meta: PropTypes.object,
    defaultSelected: PropTypes.array
};