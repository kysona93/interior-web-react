import React, {PropTypes} from '../../../../node_modules/react';
import { FormGroup, HelpBlock } from "react-bootstrap";

export const CheckBox = ({input, name, label, check=false, meta, type, id}) => (
    <FormGroup validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
        <FormGroup>
            <input {...input} id={id} type={type} checked={check} value={check} /> &nbsp;
            <label>{label}</label>
        </FormGroup>
        <HelpBlock>
            {meta.touched && meta.error ? meta.error : null }
        </HelpBlock>
    </FormGroup>
);

CheckBox.propTypes = {
    meta: PropTypes.object,
    input: PropTypes.object,
    name: PropTypes.string,
    type: PropTypes.string
};