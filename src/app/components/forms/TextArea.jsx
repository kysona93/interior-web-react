import React, {PropTypes} from '../../../../node_modules/react';
import { FormGroup, FormControl, HelpBlock } from "react-bootstrap";

export const TextArea = ({className, input, label, height, meta,disabled}) => (
    <FormGroup className={className}
               validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
        <FormControl {...input} componentClass="textarea" disabled={disabled} placeholder={label} style={{paddingLeft: '30px', paddingRight: '5px', height: height}} />
        <FormControl.Feedback style={{left: 0, marginTop: '3px', width: '30px', height: '34px'}}>
            <i className="fa fa-keyboard-o">
            </i>
        </FormControl.Feedback>
        <HelpBlock>
            {meta.touched && meta.error ? meta.error : null}
        </HelpBlock>
    </FormGroup>
);

TextArea.propTypes = {
    meta: PropTypes.object,
    input: PropTypes.object,
    label: PropTypes.string,
    className: PropTypes.string
};