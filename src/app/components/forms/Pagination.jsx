/**
 * Using pagination just call <Pagination /> from shared folder
 * Include some properties :
 * totalCount = { value }
 *      value : total count of list object in Number
 * items = { value }
 *      value : pagination = [ limit: value , page: value , ... ]
 * callBackAction = { action }
 *      action : using action request api
 * 
 * ******* eg: ListMenu.jsx
 */
import React, {PropTypes} from '../../../../node_modules/react';
import { Row, Col, Pagination, FormControl } from "react-bootstrap";

export default class PaginationCls extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            activePage: 1,
            limitShow: 10
        };
    }

    render() {
        const { totalCount, items, callBackAction } = this.props;
        const handleShow = event => {
            this.setState({limitShow: Number(event.target.value)});
            items.limit = Number(event.target.value);
            callBackAction(items);
        };

        const handleSelect = eventKey => {
            this.setState({activePage: eventKey});
            items.page = eventKey;
            callBackAction(items);
        };

        const handleItems = total => {
            if (total <= this.state.limitShow) {
                return 1
            } else if (total % this.state.limitShow === 0) {
                return total / this.state.limitShow
            } else if (total % this.state.limitShow > 0) {
                return parseInt(total / this.state.limitShow) + 1
            }
        };

        return (
            totalCount >= 10 ?
                <Row>
                    <Col md={1}>
                        <FormControl 
                                onChange={handleShow}
                                componentClass="select" 
                                placeholder="select" 
                                style={{width: 70}}>
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="150">150</option>
                            <option value={totalCount}>All</option>
                        </FormControl>

                    </Col>
                    <Col md={5}>
                        <div style={{marginTop: 8}}>
                            Total {totalCount} Records
                        </div>
                    </Col>
                    <Col md={6}>
                        <Pagination 
                            style={{ float: 'right', margin: 0}}
                            prev
                            next
                            first
                            last
                            ellipsis
                            boundaryLinks
                            items={handleItems(totalCount)}
                            maxButtons={5}
                            activePage={this.state.activePage}
                            onSelect={handleSelect}
                        />
                    </Col>
                </Row>
            : null
        )
    }
}