import React, {PropTypes} from '../../../../node_modules/react';
import { FormGroup, HelpBlock, Button } from "react-bootstrap";

export const ButtonSubmit = ({className, error, invalid, submitting, label, icon}) => (
    <div>
        { error && <FormGroup validationState="error"><HelpBlock>{error}</HelpBlock></FormGroup> }
        <FormGroup className="submit">
            <Button bsStyle="primary" type="submit" className={className} block disabled={invalid || submitting}>
                {label}&nbsp;&nbsp;<i className={icon}> </i>
            </Button>
        </FormGroup>
    </div>
);

ButtonSubmit.propTypes = {
    error: PropTypes.string,
    invalid: PropTypes.bool,
    submitting: PropTypes.bool,
    className: PropTypes.string
};
