import React, {PropTypes} from '../../../../node_modules/react';
import { FormGroup, FormControl, HelpBlock } from 'react-bootstrap';
import { SortObjArray } from './../../utils/SortObjArray';

export default class SelectBox extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        const { input, type, placeholder, values, sortBy="none", disabled, icon="fa fa-keyboard-o", children, meta } = this.props;

        return(
            <FormGroup controlId="formControlsSelect" validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
                <FormControl {...input} componentClass={type} type={type} disabled={disabled} style={{paddingLeft: '25px', paddingRight: '5px', height: '34px'}}>
                    <option value=''>{placeholder}</option>
                    { sortBy === "none" ?
                        values.map((value, index) => {
                            return(
                                <option key={index} value={value.id}>{value.name}</option>
                            )
                        })
                        :
                        values.sort(SortObjArray(sortBy, false, function(a){return a.toUpperCase()})).map((value, index) => {
                            return(
                                <option key={index} value={value.id}>{value.name}</option>
                            )
                        })
                    }
                    {children}
                </FormControl>
                <FormControl.Feedback style={{left: 0, marginTop: '8px', width: '30px', height: '30px'}}>
                    <i className={icon}> </i>
                </FormControl.Feedback>
                <HelpBlock>
                    {meta.touched && meta.error ? meta.error : null}
                </HelpBlock>
            </FormGroup>
        );
    }
}
SelectBox.propTypes = {
    meta: PropTypes.object,
    input: PropTypes.object,
    type: PropTypes.string,
    icon: PropTypes.string,
    placeholder: PropTypes.string,
    values: PropTypes.array,
    sortBy: PropTypes.string
};
