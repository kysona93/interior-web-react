import React, {PropTypes} from '../../../../node_modules/react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import { FormGroup, FormControl, HelpBlock } from "react-bootstrap";
import './Styles.css';

export const DateTimePicker = ({input, dateFormat, placeholder, defaultDate, handleChange, clear, meta, disabled }) => (
    <FormGroup validationState={!meta.touched ? null : (meta.error ? 'error' : 'success')}>
        <DatePicker
            {...input}
            dateFormat="YYYY-MM-DD"
            placeholderText={placeholder}
            selected={defaultDate}
            onChange={handleChange}
            isClearable={clear}
            disabled={disabled}
            showYearDropdown
            dropdownMode="select"
            className="form_datepicker_style"
            highlightDates={[moment(), moment()]}
        />
        <FormControl.Feedback style={{left: 0, marginTop: '8px', width: '30px', height: '30px'}} autoComplete="off">
            <i className="fa fa-calendar">
            </i>
        </FormControl.Feedback>
        <HelpBlock>
            {meta.touched && meta.error ? meta.error : null}
        </HelpBlock>
    </FormGroup>
);

DateTimePicker.propTypes = {
    input: PropTypes.object,
    dateFormat: PropTypes.string,
    placeholder: PropTypes.string,
    defaultDate: PropTypes.object,
    handleChange: PropTypes.func,
    clear: PropTypes.bool,
    meta: PropTypes.object
};