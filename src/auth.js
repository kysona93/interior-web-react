import { browserHistory } from 'react-router';
/**
 * @function saveAuth: keep token into localstorage
 * @param {* user token object } state 
 */
export const saveAuth = (state) => {
    try {
        localStorage.removeItem('state');
        const serializedState = JSON.stringify(state);
        localStorage.setItem('state', serializedState);
    }
    catch (error) {
    }
};

export const clearAuth = () => {
    try {
        localStorage.removeItem('state');
        browserHistory.push('/');
    } catch (error) {
        browserHistory.push('/');
    }
};

export const loadAuth = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    }
    catch (error) {
        return undefined;
    }
};

/**
 * authObj convert Jwt token to JSON object.
 * return authObj() dot object name.
 */
export const authObj = () => {
    const base64Url = loadAuth().split('.')[1];
    const base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
};

export const checkIsLogged = (nextState, replace) => {
    try {
        if (loadAuth() === undefined) {
            replace('/');
        }
    } catch (error) {
        replace('/');
    }
};

export const checkHasToken = (nextState, replace) => {
    try {
        if (loadAuth()) {
            replace('/app');
        }
    } catch (error) {
        replace('/');
    }
};

export const savePages = (state) => {
    try {
        localStorage.removeItem('pageUrl');
        const serializedState = JSON.stringify(state);
        localStorage.setItem('pageUrl', serializedState);
    }
    catch (error) {
    }
};

export const clearPages = () => {
    try {
        localStorage.removeItem('pageUrl');
    } catch (error) {
    }
};

export const loadPages = () => {
    try {
        const serializedState = localStorage.getItem('pageUrl');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    }
    catch (error) {
        return undefined;
    }
};


/**
 * @function checkPermission: check user with page permission,
 * And if no permission, go throw to access denied page.
 * Using onEnter to handle check page permission on each <Route.../>
 * @param {*handle state checking after onEnter url} nextState 
 * @param {*handle route ot url access after checking} replace 
 * @method find: compare with page permisstion with request access page.
 */
export let canCreate = false;
export let canUpdate = false;
export let canDelete = false;

let parentRoute = "";
export const checkPermission = (nextState, replace) => {
    try {
        parentRoute = nextState.location.pathname;
        if(loadAuth() !== undefined && loadPages() !== undefined) {
            let isPermission = loadPages().find(route => route.link === parentRoute && route.canRead === true);
            if(!isPermission) {
                replace('/app/access-denied');
            }
            canCreate = isPermission.canCreate;
            canUpdate = isPermission.canUpdate;
            canDelete = isPermission.canDelete;
        }
    } catch (error) {console.log(error)}
};

export const checkCanCreate = (nextState, replace) => {
    try {
        let urlCreate = `${parentRoute}/add`;
        if(urlCreate && canCreate === false) {
            replace('/app/access-denied');
        }
    } catch (error) {console.log(error)}
};

export const checkCanUpdate = (nextState, replace) => {
    try {
        let urlUpdate = `${parentRoute}/edit/:id`;
        if(urlUpdate && canUpdate === false) {
            replace('/app/access-denied');
        }
    } catch (error) {console.log(error)}
};