import React from "react";
import { Router, Route, IndexRoute } from "react-router";
import { history } from "./store.js";
import { checkIsLogged, checkHasToken } from './auth';
import AccessDenied from './app/containers/error/AccessDenied';
import UnderConstruction from './app/containers/error/UnderContruction';
import App from './app/containers/App';
import LogIn from './app/containers/signin/LogIn';
import Home from './app/containers/Home';
import Portfolio from './app/containers/portfolio/Portfolio';
import Service from './app/containers/service/Service';
import AboutUs from './app/containers/aboutus/AboutUs';
import ContactUs from './app/containers/contactus/ContactUs';

/* menu
import AssignMenu from './app/containers/menu/AssignMenu';

import PrintInvoiceEDC from './app/containers/customer/finance/PrintInvoiceEDC';
import PrintInvoiceAddCharge from './app/containers/customer/finance/PrintInvoiceAddCharge';
import PrintInvoiceRentPole from './app/containers/administration/setup/license/rentPole/PrintInvoiceRentPole.jsx';
import PrintInvoiceOtherIncome from './app/containers/installation/step/OtherIncome';
import PrintNewConnection from './app/containers/installation/step/NewConnection';

import Return from './app/containers/report/prepaid/Return';
import InvoiceDailySale from './app/containers/report/prepaid/InvoiceDailySale';
import PrintInvoicePrepaid from './app/containers/customer/finance/PrintInvoicePrepaid';

import { security } from './app/route/security/index';
import { administration } from './app/route/administration/index';
import { customer } from './app/route/customer/index';
import { inventory } from './app/route/inventory/index';
import { installation } from './app/route/installation/index';
import { transaction } from './app/route/transaction/index';
import { accounting } from './app/route/accounting/index';
import { report } from './app/route/report/index';

import GroupReport from './app/containers/report/groupReport/GroupReport';

import NewConnection from './app/containers/installation/step/NewConnection';
import OtherIncome from './app/containers/installation/step/OtherIncome';
import PrintPlaning from './app/containers/inventory/plan/Print';
 */

const router = (
    <Router onUpdate={() => window.scrollTo(0, 0)} history={history}>
        <Route path="/" component={Home} />
        <Route path="/portfolio" component={Portfolio} />
        <Route path="/service" component={Service} />
        <Route path="/about-us" component={AboutUs} />
        <Route path="/contact-us" component={ContactUs} />

    </Router>
);

export {router};

/**
 <Route path="/" component={LogIn} onEnter={checkHasToken} />

 <Route path="/login" component={LogIn} onEnter={checkHasToken}/>
 <Route path="/app" component={App} onEnter={checkIsLogged} >
 <IndexRoute components={Home}/>
 { security }
 { administration }
 { customer }
 { inventory }
 { installation }
 { transaction }
 { accounting }
 { report }

 <Route path="reports">
 <Route path="group-reports" components={GroupReport} />
 </Route>
 <Route path="access-denied" component={AccessDenied}/>
 <Route path="under-contruction" component={UnderConstruction} />
 </Route>

 <Route path="/pub" component={App}>
 <Route path="assign-menu" component={AssignMenu} />
 </Route>
 <Route path="/print-edc-invoice/:invoiceId/:issueDate" component={PrintInvoiceEDC} />
 <Route path="/print-add-charge-invoice(/:invoiceId)" component={PrintInvoiceAddCharge} />
 <Route path="/print-rent-pole-invoice(/:invoiceId)" component={PrintInvoiceRentPole} />
 <Route path="/print-other-income" component={PrintInvoiceOtherIncome}/>
 <Route path="/print-new-connection" component={PrintNewConnection}/>

 <Route path="print-step-work-new-connection/:id/:no" components={NewConnection} />
 <Route path="print-step-work-other-income/:id/:no" components={OtherIncome} />
 <Route path="/print-planning/:planId" component={PrintPlaning} />

 <Route path="/return" component={Return}/>
 <Route path="/print-invoice-daily-sale/:cusId" component={InvoiceDailySale}/>
 <Route path="/print-prepaid-invoice" component={PrintInvoicePrepaid} />
 **/
